//
///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mollatech.coremobiletrust.rhb.user;
//
//import com.mollatech.axiom.nucleus.crypto.LoadSettings;
//import com.mollatech.rhb.user.RHBUserServiceInterfaceImpl;
//import com.mollatech.rhb.user.RHBUserServiceInterfaceImpl_Service;
//import com.mollatech.rhb.user.RhbUser;
//import java.net.URL;
//import java.security.KeyManagementException;
//import java.security.NoSuchAlgorithmException;
//import java.util.Date;
//import javax.net.ssl.HttpsURLConnection;
//import javax.net.ssl.SSLContext;
//import javax.net.ssl.TrustManager;
//import javax.xml.namespace.QName;
//import org.apache.log4j.Logger;
//
///**
// *
// * @author vikramsareen
// */
//public class RhbWrapper {
//
//    private RHBUserServiceInterfaceImpl_Service m_service = null;
//    private RHBUserServiceInterfaceImpl m_port = null;
//    private String m_channelid = null;
//    // public static final int PKI_SOFTWARE_TOKEN = 8;
//    private static Logger log = Logger.getLogger(RhbWrapper.class);
//    
//
//    public RhbWrapper() {
//        try {
//            String wsdlname = LoadSettings.g_RHBSettings.getProperty("rhbuser.wsdl.name");
//            String channelid = LoadSettings.g_RHBSettings.getProperty("rhbuser.channelid");
//            m_channelid = channelid;
//            String remotelogin = LoadSettings.g_RHBSettings.getProperty("rhbuser.remotelogin");
//            String password = LoadSettings.g_RHBSettings.getProperty("rhbuser.password");
//            String ipAddress = LoadSettings.g_RHBSettings.getProperty("rhbuser.ipaddress");
//            String strPort = LoadSettings.g_RHBSettings.getProperty("rhbuser.port");
//            String strSecured = LoadSettings.g_RHBSettings.getProperty("rhbuser.secured");
//
//            String strDebug = null;
//            try {
//                // strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                    Date d = new Date();
//                    log.info(d + ">>" + "channelid::" + channelid);
//                    log.info(d + ">>" + "remotelogin::" + remotelogin);
//                    log.info(d + ">>" + "password::" + password);
//                    log.info(d + ">>" + "ipAddress::" + ipAddress);
//                    log.info(d + ">>" + "strPort::" + strPort);
//                    log.info(d + ">>" + "strSecured::" + strSecured);
//                }
//            } catch (Exception ex) {
//            }
//
//            SSLContext sslContext = null;
//            try {
//                HttpsURLConnection.setDefaultHostnameVerifier(new AllVerifier());
//                try {
//                    sslContext = SSLContext.getInstance("TLS");
//                } catch (NoSuchAlgorithmException ex) {
//                    //Logger.getLogger(NewClass.class.getName()).log(Level.SEVERE, null, ex);
//                    ex.printStackTrace();
//                }
//                sslContext.init(null, new TrustManager[]{new AllTrustManager()}, null);
//                HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
//
//            } catch (KeyManagementException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//
////            RssUserCerdentials userObj = null;
//            java.lang.String sessionid = null;
//            try { // Call Web Service Operation
//
//                String wsdlUrl = "http://" + ipAddress + ":" + strPort + "/" + wsdlname + "/RHBUserServiceInterfaceImpl?wsdl";
//                if (strSecured != null && strSecured.compareToIgnoreCase("yes") == 0) {
//                    wsdlUrl = "https://" + ipAddress + ":" + strPort + "/" + wsdlname + "/RHBUserServiceInterfaceImpl?wsdl";
//                }
//
//                URL url = new URL(wsdlUrl);
//                QName qName = new QName("http://user.rhb.mollatech.com/", "RHBUserServiceInterfaceImpl");
//                //m_service = new MobileTrustInterfaceImplService(url, qName);
//                //m_port = m_service.getMobileTrustInterfaceImplPort();
//                m_service = new RHBUserServiceInterfaceImpl_Service(url, qName);
//                m_port = m_service.getRHBUserServiceInterfaceImplPort();
////                byte[] SHA1hash = UtilityFunctions.SHA1(channelid + remotelogin + password);
////                integritycheck = new String(Base64Coder.encode(SHA1hash));
////                m_sessionid = m_port.openSession(channelid, remotelogin, password, integritycheck);
//
//                //System.out.println("Result = " + sessionid);
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//
//    }
//    
//      public RhbUser checkRHBUser(String userId, String userName, String email ,String organisationUnit,
//              String organisation,String location,String country) {
//        try {
//            log.info("in check RHB User");
//            if (userId != null || userName !=null) {
//                RhbUser RhbUser = m_port.checkRHBUser(userId, userName, email, organisationUnit,
//                        organisation, location, country);
//                log.info("RhbUser ::" + RhbUser);
//                return RhbUser;
//            } else {
//                return null;
//            }
//        } catch (Exception ex) {
//            log.info(ex.toString());
//            ex.printStackTrace();
//            return null;
//        }
//
//    }
//
//
//}
