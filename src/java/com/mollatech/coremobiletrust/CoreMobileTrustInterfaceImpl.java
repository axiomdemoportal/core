///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.mollatech.coremobiletrust;
//
//
//import com.mollatech.axiom.bridge.web.service.AxiomPDFSignerWrapper;
//import com.mollatech.axiom.common.utils.UtilityFunctions;
//import static com.mollatech.axiom.common.utils.UtilityFunctions.SHA1;
//import com.mollatech.axiom.connector.communication.AXIOMStatus;
//import com.mollatech.axiom.connector.communication.RemoteSigningInfo;
//import com.mollatech.axiom.connector.mobiletrust.Location;
//import com.mollatech.axiom.connector.user.AuthUser;
//import com.mollatech.axiom.connector.user.AxiomChallengeResponse;
//import com.mollatech.axiom.connector.user.TokenStatusDetails;
//import com.mollatech.axiom.mobiletrust.crypto.CryptoManager;
//import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
//import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
//import com.mollatech.axiom.nucleus.crypto.LoadSettings;
//import com.mollatech.axiom.nucleus.db.Certificates;
//import com.mollatech.axiom.nucleus.db.Channels;
//import com.mollatech.axiom.nucleus.db.Countrylist;
//import com.mollatech.axiom.nucleus.db.Geofence;
//import com.mollatech.axiom.nucleus.db.Operators;
//import com.mollatech.axiom.nucleus.db.Otptokens;
//import com.mollatech.axiom.nucleus.db.Securephrase;
//import com.mollatech.axiom.nucleus.db.Sessions;
//import com.mollatech.axiom.nucleus.db.Templates;
//import com.mollatech.axiom.nucleus.db.Trusteddevice;
//import com.mollatech.axiom.nucleus.db.connector.GeoFenceUtils;
//import com.mollatech.axiom.nucleus.db.connector.GeoTrackingUtils;
//import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
//import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
//import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.ChallengeResponsemanagement;
//import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.CryptoManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.GeoLocationManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.LocationManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.MobileTrustManagment;
//import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
//import static com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement.TOKEN;
//import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.PDFSigningManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.SecurePhraseManagment;
//import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.TrustDeviceManagement;
//import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
//import com.mollatech.axiom.nucleus.db.operation.AxiomQuestionsAndAnswers;
//import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
//import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
//import com.mollatech.axiom.nucleus.settings.MobileTrustSettings;
//import com.mollatech.axiom.nucleus.settings.RootCertificateSettings;
//import com.mollatech.axiom.nucleus.settings.SendNotification;
//import com.mollatech.axiom.nucleus.settings.TokenSettings;
//import static com.mollatech.axiom.v2.core.all.CoreInterface.OTP_TOKEN_HARDWARE;
//import static com.mollatech.axiom.v2.core.all.CoreInterface.OTP_TOKEN_OUTOFBAND;
//import com.mollatech.axiom.v2.core.utils.AxiomCredentialDetails;
//import com.mollatech.axiom.v2.core.utils.AxiomData;
//import com.mollatech.axiom.v2.core.utils.AxiomException;
//import com.mollatech.axiom.v2.core.utils.AxiomStatus;
//import com.mollatech.axiom.v2.core.utils.AxiomUser;
//import com.mollatech.axiom.v2.core.utils.AxiomUserCerdentials;
//import com.mollatech.axiom.v2.core.utils.DocumentSigner;
//import com.mollatech.axiom.v2.core.utils.MobileTrustStatus;
//import com.mollatech.coremobiletrust.rhb.user.RhbWrapper;
//import com.mollatech.rhb.user.RhbUser;
//import java.io.ByteArrayInputStream;
//import java.io.File;
//import java.io.FileOutputStream;
//import java.security.KeyFactory;
//import java.security.KeyPair;
//import java.security.KeyStore;
//import java.security.PrivateKey;
//import java.security.PublicKey;
//import java.security.interfaces.RSAPrivateCrtKey;
//import java.security.spec.RSAPublicKeySpec;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.Enumeration;
//import java.util.List;
//
//import javax.annotation.Resource;
//import javax.jws.WebService;
//import javax.servlet.http.HttpServletRequest;
//import javax.xml.ws.WebServiceContext;
//import javax.xml.ws.handler.MessageContext;
//import org.bouncycastle.util.encoders.Base64;
//import org.hibernate.Session;
//import org.json.JSONObject;
//import org.apache.log4j.Logger;
//
///**
// *
// * @author Manoj Sherkhane
// */
//@WebService(endpointInterface = "com.mollatech.coremobiletrust.CoreMobileTrustInterface")
//public class CoreMobileTrustInterfaceImpl implements CoreMobileTrustInterface {
//
//    @Resource
//    WebServiceContext wsContext;
//    private final static int ANDROID = 1;
//    private final static int IOS = 2;
//    private final static int Webtrust = 3;
//
//    static Logger log = Logger.getLogger(CoreMobileTrustInterfaceImpl.class.getName());
//
////    static {
////        Provider p = Security.getProvider("BC");
////        if (p == null) {
////            Security.addProvider(new BouncyCastleProvider()); // add it
////        }
////    }
//    final String itemtype = "MOBILETRUST";
//
//    @Override
//
//    public String OpenSession(String channelid, String loginid, String loginpassword) throws AxiomException {
//
//        try {
//
//            String strDebug = null;
//            SettingsManagement setManagement = new SettingsManagement();
//            log.info("##OpenSession: Started");
//            log.info("##OpenSession: channelid = " + channelid);
//            log.info("##OpenSession: loginid = " + loginid);
//
//            try {
//
//                ChannelProfile channelprofileObj = null;
//                Object channelpobj = setManagement.getSettingInner(channelid, SettingsManagement.CHANNELPROFILE_SETTING, 1);
//
//                if (channelpobj == null) {
//                    LoadSettings.LoadChannelProfile(channelprofileObj);
//                } else {
//                    channelprofileObj = (ChannelProfile) channelpobj;
//                    LoadSettings.LoadChannelProfile(channelprofileObj);
//                }
//                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                    log.info("##OpenSession: Started");
//                    log.info("##OpenSession: channelid = " + channelid);
//                    log.info("##OpenSession: loginid = " + loginid);
//
//                }
//            } catch (Exception ex) {
//
//                log.error(String.format("##OpenSession :  - exception caught when calling OpenSession - ", ex.getMessage()));
//
//                throw new AxiomException("Invalid Credentials!!!");
//            }
//
//            int iResult = 0;
////            if (iResult != 0) {
////                System.out.println("OpenSession::License Error::" + iResult);
////                throw new AxiomException("Invalid License (" + iResult + ")");
////            }
//
//            ChannelManagement cManagement = new ChannelManagement();
//            Channels channel = cManagement.getChannelByID(channelid);
//            if (channel == null) {
//                log.error(String.format("##OpenSession :  - exception caught when calling Enhanced2FAEJBBean.sendIVRCall() - ", "Channel ID not found"));
//                throw new AxiomException("Channel ID not found (" + iResult + ")");
//            }
//
//            MessageContext mc = wsContext.getMessageContext();
//            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//            //System.out.println("Client IP = " + req.getRemoteAddr());
//            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//            if (ipobj != null) {
//                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                int checkIp = 1;
//                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                    log.debug("checkIp =" + checkIp);
//
//                } else {
//                    checkIp = 1;
//                }
//                log.debug("Ipstatus =" + iObj.ipstatus);
//                if (iObj.ipstatus == 0 && checkIp != 1) {
//                    if (iObj.ipalertstatus == 0) {
//                        try {
//                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                            Session sTemplate = suTemplate.openSession();
//                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                            SessionFactoryUtil suOperators = new SessionFactoryUtil(SessionFactoryUtil.operators);
//                            OperatorsManagement oManagement = new OperatorsManagement();
//                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                            log.debug("templatesObjstatus =" + templatesObj.getStatus());
//                            if (aOperator != null) {
//                                String[] emailList = new String[aOperator.length - 1];
//                                for (int i = 1; i < aOperator.length; i++) {
//                                    emailList[i - 1] = aOperator[i].getEmailid();
//                                }
//                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//
//                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                    String strsubject = templatesObj.getSubject();
//                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                    if (strmessageBody != null) {
//                                        // Date date = new Date();
//                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//
//                                    }
//
//                                    SendNotification send = new SendNotification();
//                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                                    log.debug("SendEmail result AxiomStatus =" + axStatus.strStatus + "" + axStatus.iStatus);
//
//                                }
//
//                                suTemplate.close();
//                                sTemplate.close();
//                                return null;
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
//                            return null;
//                        } catch (Exception e) {
//                            log.error(String.format("##OpenSession :  - exception caught when calling OpenSession() - ", e.getMessage()));
//                            throw e;
//                        }
//                    }
//                    throw new AxiomException("Invalid IP");
//                }
//            }
//
////            int result = setManagement.checkIP(channelid, req.getRemoteAddr());
////            if (checkIp != 1) {
////                return null;
////            }
//            String resultStr = "Failure";
//            int retValue = -1;
//            SessionManagement sManagement = new SessionManagement();
//            String sessionId = sManagement.OpenSessionForWS(channelid, loginid, loginpassword, req.getSession().getId());
//            log.debug("sessionId" + sessionId);
//            AuditManagement audit = new AuditManagement();
//            if (sessionId != null) {
//                retValue = 0;
//                resultStr = "Success";
//                audit.AddAuditTrail(sessionId, channelid, loginid,
//                        req.getRemoteAddr(),
//                        channel.getName(), loginid,
//                        loginid, new Date(), "Open Session", resultStr, retValue,
//                        "Login", "", "Open Session successfully with Session Id =" + sessionId, "SESSION",
//                        loginid);
//
//            } else if (sessionId == null) {
//                audit.AddAuditTrail(sessionId, channel.getChannelid(), loginid, req.getRemoteAddr(), channel.getName(), loginid,
//                        loginid, new Date(), "Open Session", resultStr, retValue,
//                        "Login", "", "Failed To Open Session", "SESSION",
//                        loginid);
//
//            }
//
//            return sessionId;
//        } catch (Exception e) {
//            log.error(String.format("##OpenSession :  - exception caught when calling OpenSession() - ", e.getMessage()));
//            e.printStackTrace();
//            throw new AxiomException(e.getMessage());
//        }
//
//    }
//
//    @Override
//    public AxiomStatus CloseSession(String sessionid) {
//        try {
//
//            String strDebug = null;
//            try {
//                log.info("##CloseSession: sessionid = " + sessionid);
//                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                    System.out.println("CloseSession::sessionid::" + sessionid);
//                }
//            } catch (Exception ex) {
//                log.error(String.format("##CloseSession :  - exception caught when calling OpenSession() - ", ex.getMessage()));
//            }
//
////            int iResult = AxiomProtect.ValidateLicense();
////            if (iResult != 0) {
////                AxiomStatus aStatus = new AxiomStatus();
////                aStatus.error = "Licence invalid";
////                aStatus.errorcode = -100;
////                return aStatus;
////            }
//            SessionManagement sManagement = new SessionManagement();
//            Sessions session = sManagement.getSessionById(sessionid);
//            MessageContext mc = wsContext.getMessageContext();
//            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "ERROR";
//            aStatus.errorcode = -2;
//
//            if (session != null) {
//
//                //System.out.println("Client IP = " + req.getRemoteAddr());        
//                SettingsManagement setManagement = new SettingsManagement();
////                int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
////                if (result != 1) {
////                    aStatus.error = "INVALID IP REQUEST";
////                    aStatus.errorcode = -8;
////                    //  return aStatus;
////                }
//                String channelid = session.getChannelid();
//                log.debug("#channelid # closesession" + "channelId" + channelid);
//                ChannelManagement cManagement = new ChannelManagement();
//                Channels channel = cManagement.getChannelByID(channelid);
//                if (channel == null) {
//                    aStatus.error = "Invalid Channel";
//                    aStatus.errorcode = -3;
//                    return aStatus;
//                }
//                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//                if (ipobj != null) {
//                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                    log.debug("##closesession ipstatus" + iObj.ipstatus);
//                    int checkIp = 1;
//                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                        log.debug("##closesession checkipd" + checkIp);
//
//                    } else {
//                        checkIp = 1;
//                    }
//                    if (iObj.ipstatus == 0 && checkIp != 1) {
//                        if (iObj.ipalertstatus == 0) {
//                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                            Session sTemplate = suTemplate.openSession();
//                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                            OperatorsManagement oManagement = new OperatorsManagement();
//                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                            if (aOperator != null) {
//                                String[] emailList = new String[aOperator.length - 1];
//                                for (int i = 1; i < aOperator.length; i++) {
//                                    emailList[i - 1] = aOperator[i].getEmailid();
//                                }
//                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                    String strsubject = templatesObj.getSubject();
//                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                    if (strmessageBody != null) {
//                                        // Date date = new Date();
//                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                    }
//
//                                    SendNotification send = new SendNotification();
//                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                                    log.debug("SendEmail result AxiomStatus =" + axStatus.strStatus + "" + axStatus.iStatus);
//                                }
//
//                                suTemplate.close();
//                                sTemplate.close();
//                                aStatus.error = "INVALID IP";
//                                aStatus.errorcode = -8;
//                                log.debug("##Opensession : INVALID IP" + aStatus.errorcode);
//
//                                return aStatus;
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
//                            aStatus.error = "INVALID IP";
//                            aStatus.errorcode = -8;
//                            return aStatus;
//                        }
//                        aStatus.error = "INVALID IP";
//                        aStatus.errorcode = -8;
//                        return aStatus;
//                    }
//                }
//                int retValue = sManagement.CloseSession(sessionid);
//                log.debug("##Opensession :retValue" + retValue);
//                aStatus.error = "ERROR";
//                aStatus.errorcode = retValue;
//                if (retValue == 0) {
//                    log.debug("##Opensession :AxiomStatus" + "Error Code" + aStatus.errorcode + "Error Message" + aStatus.error);
//                    aStatus.error = "SUCCESS";
//                    aStatus.errorcode = 0;
//                }
//
//            } else if (session == null) {
//                log.debug("##Opensession :session is " + "null");
//                return aStatus;
//            }
//
//            ChannelManagement cManagement = new ChannelManagement();
//            Channels channel = cManagement.getChannelByID(session.getChannelid());
//            log.debug("##Opensession :channelId " + channel.getChannelid());
//            if (channel == null) {
//                return aStatus;
//            }
//            AuditManagement audit = new AuditManagement();
//            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(),
//                    session.getLoginid(), new Date(), "Close Session", aStatus.error, aStatus.errorcode,
//                    "Login", "", "Close Session successfully with Session Id =" + sessionid, "SESSION",
//                    session.getLoginid());
//            return aStatus;
//        } catch (Exception e) {
//            e.printStackTrace();
//            log.error(String.format("##CloseSession :  - exception caught when calling CloseSession() - ", e.getMessage()));
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "General Exception::" + e.getMessage();
//            aStatus.errorcode = -99;
//            return aStatus;
//        }
//
//    }
//
//    @Override
//    public AxiomStatus CreateAPUser(String sessionid, AxiomUser aUser) {
//        String strDebug = null;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                log.info(String.format("##CreateAPUser :", "sessionid" + sessionid));
//                log.info(String.format("##CreateAPUser :", "username" + aUser.userName));
//                log.info(String.format("##CreateAPUser :", "phoneNo" + aUser.phoneNo));
//                log.info(String.format("##CreateAPUser :", "email" + aUser.email));
//                log.info(String.format("##CreateAPUser :", "userId" + aUser.userId));
//                log.info(String.format("##CreateAPUser :", "idType" + aUser.idType));
//                log.info(String.format("##CreateAPUser :", "location" + aUser.location));
//                log.info(String.format("##CreateAPUser :", "organisation" + aUser.organisation));
//                log.info(String.format("##CreateAPUser :", "organisationUnit" + aUser.organisationUnit));
//                log.info(String.format("##CreateAPUser :", "street" + aUser.street));
//                log.info(String.format("##CreateAPUser :", "userIdentity" + aUser.userIdentity));
//                log.info(String.format("##CreateAPUser :", "country" + aUser.country));
//                log.info(String.format("##CreateAPUser :", "designation" + aUser.designation));
//                System.out.println("CreateUser::sessionId::" + sessionid);
//                System.out.println("CreateUser::fullname::" + aUser.userName);
//                System.out.println("CreateUser::phone::" + aUser.phoneNo);
//                System.out.println("CreateUser::email::" + aUser.email);
//                System.out.println("CreateUser::userid::" + aUser.userId);
//            }
//        } catch (Exception ex) {
//            log.error(String.format("##CreateAPUser :  - exception caught when calling CreateAPUser() - ", ex.getMessage()));
//        }
//
////        int iResult = AxiomProtect.ValidateLicense();
////        if (iResult != 0) {
////            AxiomStatus aStatus = new AxiomStatus();
////            aStatus.error = "Licence is invalid";
////            aStatus.errorcode = -100;
////            return aStatus;
////        }
//        try {
//
//            SessionManagement sManagement = new SessionManagement();
//            AuditManagement audit = new AuditManagement();
//            MessageContext mc = wsContext.getMessageContext();
//            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//            Sessions session = sManagement.getSessionById(sessionid);
//
//            AxiomStatus aStatus = new AxiomStatus();
//
//            if (session == null) {
//                log.debug("Invalid Session");
//                aStatus.error = "Invalid Session";
//                aStatus.errorcode = -14;
//                return aStatus;
//            }
//
//            if (session == null || aUser.userName == null || aUser.phoneNo == null
//                    || sessionid.isEmpty() == true || aUser.userName.isEmpty() == true || aUser.phoneNo.isEmpty() == true
//                    || aUser.email == null || aUser.email.isEmpty() == true) {
//                log.debug("Invalid Data");
//                aStatus.error = "Invalid Data";
//                aStatus.errorcode = -11;
//                return aStatus;
//            }
//
//            if (aUser.userId != null && aUser.userId.isEmpty() == true) {
//                //AxiomStatus aStatus = new AxiomStatus();
//                aStatus.error = "Setting userid is not supported!!!";
//                aStatus.errorcode = -12;
//                return aStatus;
//            }
//
////            int iResult = AxiomProtect.ValidateLicense();
////            if (iResult != 0) {
////                AxiomStatus aStatus = new AxiomStatus();
////                aStatus.error = "Invalid License";
////                aStatus.errorcode = -10;
////                return aStatus;
////            }
//            UserManagement uManagement = new UserManagement();
//            //SessionManagement sManagement = new SessionManagement();
//            //MessageContext mc = wsContext.getMessageContext();
//            //HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//            //Sessions session = sManagement.getSessionById(sessionId);
//            //AxiomStatus aStatus = new AxiomStatus();
//            int retValue = -1;
//            aStatus.error = "ERROR";
//            aStatus.errorcode = retValue;
//
//            if (session != null) {
//
//                //System.out.println("Client IP = " + req.getRemoteAddr());        
//                SettingsManagement setManagement = new SettingsManagement();
//                String channelid = session.getChannelid();
//                log.debug("##CreateUser" + "ChannelId" + channelid);
//                ChannelManagement cManagement = new ChannelManagement();
//                Channels channel = cManagement.getChannelByID(channelid);
//                if (channel == null) {
//                    log.debug("Channel is Null");
//                    return null;
//                }
//                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//                if (ipobj != null) {
//                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                    int checkIp = 1;
//                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                        log.debug("CheckIpd" + checkIp);
//                    } else {
//                        checkIp = 1;
//                    }
//                    if (iObj.ipstatus == 0 && checkIp != 1) {
//                        if (iObj.ipalertstatus == 0) {
//                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                            Session sTemplate = suTemplate.openSession();
//                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                            OperatorsManagement oManagement = new OperatorsManagement();
//                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                            if (aOperator != null) {
//                                String[] emailList = new String[aOperator.length - 1];
//                                for (int i = 1; i < aOperator.length; i++) {
//                                    emailList[i - 1] = aOperator[i].getEmailid();
//                                }
//                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                    String strsubject = templatesObj.getSubject();
//                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                    if (strmessageBody != null) {
//                                        // Date date = new Date();
//                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                    }
//
//                                    SendNotification send = new SendNotification();
//                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                                    log.debug("##Createuser #SendEMail" + "Status" + axStatus.strStatus + axStatus.iStatus);
//                                }
//
//                                suTemplate.close();
//                                sTemplate.close();
//                                aStatus.error = "INVALID IP REQUEST";
//                                log.debug("INVALID IP REQUEST");
//                                aStatus.errorcode = -8;
//                                return aStatus;
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
//                            aStatus.error = "INVALID IP REQUEST";
//                            aStatus.errorcode = -8;
//                            return aStatus;
//                        }
//                        aStatus.error = "INVALID IP REQUEST";
//                        aStatus.errorcode = -8;
//                        return aStatus;
//                    }
//                }
//
//                //addition for license check enforcement
////                if (AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_PASSWORD) != 0) {
////                    aStatus.error = "This feature is not available in this license!!!";
////                    aStatus.errorcode = -100;
////                    return aStatus;
////                }
////                int iUserCount = uManagement.getCountOfLicenseUser(channel.getChannelid());
////                int licensecount = AxiomProtect.GetUsersAllowed(); //AxiomProtect Function here now default 0. 
////                if (licensecount == -998) {
////                    //unlimited licensing 
////                } else if (iUserCount >= licensecount) {
////                    aStatus.error = "User Addition already reached its limit as per this license!!!";
////                    aStatus.errorcode = -100;
////                    return aStatus;
////                }
//                //end of addition
////                AxiomUser user = checkUser(aUser.userName, "UID");
//                String checkRhbuser = LoadSettings.g_sSettings.getProperty("ca.user.validation");
//                RhbUser rhbUser = null;
//                if(checkRhbuser == null){
//                    checkRhbuser = "no";
//                }
//                
//                if (checkRhbuser.equals("yes")) {
//                    RhbWrapper rhbWraper = new RhbWrapper();
//                    rhbUser = rhbWraper.checkRHBUser(aUser.userId, aUser.userName, aUser.email, aUser.organisationUnit,
//                            aUser.organisation, aUser.location, aUser.country);
//                    if(rhbUser != null){
//                        retValue = uManagement.CreateUser(sessionid, session.getChannelid(), aUser.userName, aUser.phoneNo,
//                            aUser.email, aUser.userId, 0, aUser.organisation, aUser.userIdentity, aUser.idType,
//                            aUser.organisationUnit, aUser.country, aUser.location, aUser.street, aUser.designation);
//                            log.debug("##CreateUser user creation retvalue" + retValue);
//                    }else{
//                        aStatus.error = "ERROR :: RHB User Not Found";
//                        aStatus.errorcode = -1;
//                    }
//                } else {
//                    retValue = uManagement.CreateUser(sessionid, session.getChannelid(), aUser.userName, aUser.phoneNo,
//                            aUser.email, aUser.userId, 0, aUser.organisation, aUser.userIdentity, aUser.idType,
//                            aUser.organisationUnit, aUser.country, aUser.location, aUser.street, aUser.designation);
//                    log.debug("##CreateUser user creation retvalue" + retValue);
//                }
//
//                if (retValue == 0) {
//                    aStatus.error = "SUCCESS";
//                    aStatus.errorcode = 0;
//                } else if (retValue == 1) {
//                    //aStatus.error = "SUCCESS";
//                    //aStatus.errorcode = 0;
//                    aStatus.error = "User created succesfully but email could not be sent!!!";
//                    aStatus.errorcode = 0;
//                    log.debug("##CreateUser:User created succesfully but email could not be sent!!!" + retValue);
//
//                } else if (retValue == -244) {
//                    aStatus.error = "Username is Duplicate!!!";
//                    log.debug("##CreateUser:Username is Duplicate!!!" + retValue);
//
//                    aStatus.errorcode = retValue;
//                } else if (retValue == -241) {
//                    aStatus.error = "Userid is Duplicate!!!";
//                    log.debug("##CreateUser:Userid is Duplicate!!!" + retValue);
//                    aStatus.errorcode = retValue;
//                } else if (retValue == -242) {
//                    aStatus.error = "Phone Number is Duplicate!!!";
//                    log.debug("##CreateUser:Phone Number is Duplicate!!!" + retValue);
//                    aStatus.errorcode = retValue;
//                } else if (retValue == -240) {
//                    aStatus.error = "Email Id is Duplicate!!!";
//                    log.debug("##CreateUser:Email Id is Duplicate!!!" + retValue);
//                    aStatus.errorcode = retValue;
//                }
//                //return aStatus;
//            }
//
//            ChannelManagement cManagement = new ChannelManagement();
//            Channels channel = cManagement.getChannelByID(session.getChannelid());
//            //AuditManagement audit = new AuditManagement();
//            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                    req.getRemoteAddr(), channel.getName(),
//                    session.getLoginid(), session.getLoginid(),
//                    new Date(), "Create User",
//                    aStatus.error, aStatus.errorcode,
//                    "User Management", "",
//                    "Name=" + aUser.userName + ",Phone=" + aUser.phoneNo + ",Email=" + aUser.email + ",State=Inactive",
//                    "USER", "");
//            return aStatus;
//        } catch (Exception e) {
//            e.printStackTrace();
//            log.error(String.format("##CreateUser :  - exception caught when calling CreateUser() - ", e.getMessage()));
//            return null;
//        }
//    }
//
//    @Override
//    public AxiomUserCerdentials GetAPUserBy(String sessionid, int type, String searchFor) throws AxiomException {
//        String strDebug = null;
//        AxiomUserCerdentials AxiomUCred = new AxiomUserCerdentials();
//
//        System.out.println("Start::" + new Date());
//        try {
//            try {
//                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//
//                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                    log.info("##GetAPUserBy: Started");
//                    log.info("##GetAPUserBy: sessionid = " + sessionid);
//                    log.info("##GetAPUserBy: type = " + type);
//                    log.info("##GetAPUserBy: searchFor = " + searchFor);
//                    System.out.println("GetUserBy::sessionId::" + sessionid);
//                    System.out.println("GetUserBy::type::" + type);
//                    System.out.println("GetUserBy::searchFor::" + searchFor);
//                }
//            } catch (Exception ex) {
//                log.error(String.format("##GetAPUserBy :  - exception caught when calling GetAPUserBy() - ", ex.getMessage()));
//            }
////nilesh
////            int iResult = AxiomProtect.ValidateLicense();
////            if (iResult != 0) {
////                try {
////                    AxiomStatus aStatus = new AxiomStatus();
////                    AxiomUCred.errorMsg = aStatus.error = "Licence is invalid";
////                    AxiomUCred.errorCode = aStatus.errorcode = -100;
////
////                    throw new AxiomException("Licence is invalid");
////                } catch (AxiomException ex) {
////                    ex.printStackTrace();
////                }
////            }
//
//            AxiomStatus aStatus = new AxiomStatus();
//            String resultStr = "Failure";
//            int retValue = -1;
//
//            System.out.println("2::" + new Date());
//
//            UserManagement uManagement = new UserManagement();
//            SessionManagement sManagement = new SessionManagement();
//            AuditManagement audit = new AuditManagement();
//
//            MessageContext mc = wsContext.getMessageContext();
//            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//            Sessions session = sManagement.getSessionById(sessionid);
////            AxiomUser aUser = null;
//            if (sessionid == null) {
//                throw new Exception("Session ID is empty!!!");
//            }
//
//            if (session == null) {
//                log.debug("Session ID is invalid/expired!!!");
//                throw new Exception("Session ID is invalid/expired!!!");
//
//            }
//
//            ChannelManagement cManagement = new ChannelManagement();
//            Channels channel = cManagement.getChannelByID(session.getChannelid());
//            if (session != null) {
//                SettingsManagement setManagement = new SettingsManagement();
//                String channelid = session.getChannelid();
//                log.debug("#getApUser #channelid" + channelid);
//                System.out.println("3::" + new Date());
//                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
//                if (channelProfileObj != null) {
//                    int retVal = sManagement.getServerStatus(channelProfileObj);
//                    log.debug("#getApUser #getServerStatus" + retVal);
//                    if (retVal != 0) {
//                        log.debug("#getApUser #Server Down for maintainance,Please try later!!!" + retVal);
//                        throw new AxiomException("Server Down for maintainance,Please try later!!!");
//                    }
//                }
//                if (channel == null) {
//                    log.debug("#getApUser #Server Down for maintainance,Please try later!!!+Channel is null");
//                    throw new AxiomException("Server Down for maintainance,Please try later!!!");
//                }
//                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//                if (ipobj != null) {
//                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                    int checkIp = 1;
//                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                        log.debug("#getApUser #Checkip" + checkIp);
//                    } else {
//                        checkIp = 1;
//                    }
//                    if (iObj.ipstatus == 0 && checkIp != 1) {
//                        if (iObj.ipalertstatus == 0) {
//                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                            Session sTemplate = suTemplate.openSession();
//                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                            OperatorsManagement oManagement = new OperatorsManagement();
//                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                            if (aOperator != null) {
//                                String[] emailList = new String[aOperator.length - 1];
//                                for (int i = 1; i < aOperator.length; i++) {
//                                    emailList[i - 1] = aOperator[i].getEmailid();
//                                }
//                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                    String strsubject = templatesObj.getSubject();
//                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                    if (strmessageBody != null) {
//                                        // Date date = new Date();
//                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                    }
//
//                                    SendNotification send = new SendNotification();
//                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                                    log.debug("#getApUser #SendNotification #AXIOMStatus" + axStatus.strStatus);
//                                }
//
//                                suTemplate.close();
//                                sTemplate.close();
//                                log.debug("#getApUser:IP is not allowed!!!");
//                                throw new Exception("IP is not allowed!!!");
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
//                            throw new Exception("IP is not allowed!!!");
//                        }
//                        throw new Exception("IP is not allowed!!!");
//                    }
//                }
//
//                System.out.println("4::" + new Date());
//
//                String userFlag = LoadSettings.g_sSettings.getProperty("check.user");
//                log.debug("##getApUser: userFlag" + userFlag);
//                AuthUser authUser = null;
//                String USERID;
//                AxiomUser axiomUser = null;
//                //            RSSUserCerdentials axiomUserCred = null;
//                AxiomCredentialDetails[] atDetails = null;
////                TokenStatusDetails[] tDetails = null;//nilesh
//                Otptokens[] tDetails = null;
//                List<AxiomCredentialDetails> AlDetails = new ArrayList<AxiomCredentialDetails>();
//
//                if (userFlag.equals("1")) {
//                    authUser = uManagement.CheckUserByType(sessionid, session.getChannelid(), searchFor, type);
//                    log.debug("##getAPuser #CheckUserByType: userName" + authUser.userName);
//                    log.debug("##getAPuser #CheckUserByType: phoneNo" + authUser.phoneNo);
//                    log.debug("##getAPuser #CheckUserByType: userIdentity" + authUser.userIdentity);
//                    log.debug("##getAPuser #CheckUserByType: userId" + authUser.userId);
//                    log.debug("##getAPuser #CheckUserByType: idType" + authUser.idType);
//                    log.debug("##getAPuser #CheckUserByType: country" + authUser.country);
//                    log.debug("##getAPuser #CheckUserByType: designation" + authUser.designation);
//                    log.debug("##getAPuser #CheckUserByType: location" + authUser.location);
//                    log.debug("##getAPuser #CheckUserByType: street" + authUser.street);
//                } else {
//                    authUser = uManagement.CheckUserByType(sessionid, session.getChannelid(), searchFor, type);
//                    log.debug("##getAPuser #CheckUserByType: userName" + authUser.userName);
//                    log.debug("##getAPuser #CheckUserByType: phoneNo" + authUser.phoneNo);
//                    log.debug("##getAPuser #CheckUserByType: userIdentity" + authUser.userIdentity);
//                    log.debug("##getAPuser #CheckUserByType: userId" + authUser.userId);
//                    log.debug("##getAPuser #CheckUserByType: idType" + authUser.idType);
//                    log.debug("##getAPuser #CheckUserByType: country" + authUser.country);
//                    log.debug("##getAPuser #CheckUserByType: designation" + authUser.designation);
//                    log.debug("##getAPuser #CheckUserByType: location" + authUser.location);
//                    log.debug("##getAPuser #CheckUserByType: street" + authUser.street);
//                }
//
//                System.out.println("5::" + new Date());
//
//                if (authUser != null) {
//
//                    axiomUser = new AxiomUser();
//                    axiomUser.phoneNo = authUser.phoneNo;
//                    axiomUser.email = authUser.email;
//                    axiomUser.userName = authUser.userName;
//                    axiomUser.userId = authUser.userId;
//                    axiomUser.lLastAccessOn = authUser.lLastAccessOn;
//                    axiomUser.lCreatedOn = authUser.lCreatedOn;
//                    axiomUser.iAttempts = authUser.iAttempts;
//                    axiomUser.statePassword = authUser.statePassword;
//                    axiomUser.organisation = authUser.organisation;
//                    axiomUser.organisationUnit = authUser.organisationUnit;
//                    axiomUser.country = authUser.country;
//                    axiomUser.location = authUser.location;
//                    axiomUser.street = authUser.street;
//                    axiomUser.designation = authUser.designation;
//                    axiomUser.userIdentity = authUser.userIdentity;
//                    axiomUser.idType = authUser.idType;
//                    USERID = authUser.userId;
//
//                    if (USERID != null && USERID != "") {
//                        AxiomCredentialDetails PassDetails = new AxiomCredentialDetails();
//                        PassDetails.category = AxiomCredentialDetails.PASSWORD;
//                        PassDetails.subcategory = 0;
//                        PassDetails.Password = authUser.getPassword();
//                        PassDetails.attempts = authUser.iAttempts;
//                        PassDetails.createOn = authUser.lCreatedOn;
//                        PassDetails.lastaccessOn = authUser.lLastAccessOn;
//                        PassDetails.status = authUser.statePassword;
//                        AlDetails.add(PassDetails);
//
//                    }
//
//                    System.out.println("7::" + new Date());
//
//                    if (USERID != null && USERID != "") {
//
//                        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//
////                        tDetails = oManagement.getTokenList(sessionid, session.getChannelid(), USERID);//nilesh
//                        tDetails = oManagement.getTokenListv2(sessionid, session.getChannelid(), USERID);
//
//                        Object settings = setManagement.getSetting(sessionid, channelid, SettingsManagement.Token, SettingsManagement.PREFERENCE_ONE);
//                        TokenSettings tokenObj = null;
//                        if (settings != null) {
//                            tokenObj = (TokenSettings) settings;
//                        }
//                        if (tDetails != null) {
//                            for (int i = 0; i < tDetails.length; i++) {
//                                AxiomCredentialDetails TokenDetail = new AxiomCredentialDetails();
//                                int res = -1;
//                                if (tDetails[i].getStatus() == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
//                                    log.debug("Token Status" + "TOKEN_STATUS_LOCKEd");
////                                    res = oManagement.unlockTokenAfter(channelid, USERID, tokenObj, tDetails[i]);//nilesh
//                                    res = oManagement.unlockTokenAfterv2(channelid, USERID, tokenObj, tDetails[i]);
//                                }
//
//                                if (tDetails[i].getCategory() == OTP_TOKEN_SOFTWARE) {
//                                    TokenDetail.category = AxiomCredentialDetails.SOFTWARE_TOKEN;
//                                } else if (tDetails[i].getCategory() == OTP_TOKEN_HARDWARE) {
//                                    TokenDetail.category = AxiomCredentialDetails.HARDWARE_TOKEN;
//                                } else if (tDetails[i].getCategory() == OTP_TOKEN_OUTOFBAND) {
//                                    TokenDetail.category = AxiomCredentialDetails.OUTOFBOUND_TOKEN;
//                                }
//
//                                TokenDetail.subcategory = tDetails[i].getSubcategory();
//                                TokenDetail.Password = null;
//                                TokenDetail.attempts = tDetails[i].getAttempts();
//                                TokenDetail.createOn = tDetails[i].getCreationdatetime().getTime();
//                                TokenDetail.lastaccessOn = tDetails[i].getLastaccessdatetime().getTime();
//                                if (res == 0) {
//                                    TokenDetail.status = OTPTokenManagement.TOKEN_STATUS_ACTIVE;
//                                    log.debug("Token Status" + "TOKEN_STATUS_ACTIVE");
//                                } else {
//                                    TokenDetail.status = tDetails[i].getStatus();
//                                    log.debug("Token Status" + tDetails[i].getStatus());
//                                }
//                                TokenDetail.serialnumber = tDetails[i].getSrno();
//                                log.debug("Token serialnumber" + tDetails[i].getSrno());
//                                AlDetails.add(TokenDetail);
//
//                            }
//                        }
//
//                    }
//
//                    System.out.println("8::" + new Date());
//                    TokenStatusDetails[] tSDetails = null;
//                    if (USERID != null && USERID != "") {
//
//                        PKITokenManagement pManagement = new PKITokenManagement();
//                        log.info("getting  PKITokenManagement token");
//                        tSDetails = pManagement.getTokenList(sessionid, session.getChannelid(), USERID);
//
//                        if (tDetails != null) {
//                            for (int i = 0; i < tDetails.length; i++) {
//                                AxiomCredentialDetails TokenDetail = new AxiomCredentialDetails();
//                                if (tDetails[i].getCategory() == PKITokenManagement.SOFTWARE_TOKEN) {
//                                    log.debug("PKITokenManagement category" + PKITokenManagement.SOFTWARE_TOKEN);
//                                    TokenDetail.category = AxiomCredentialDetails.PKI_SOFTWARE_TOKEN;
//                                } else if (tDetails[i].getCategory() == PKITokenManagement.HARDWARE_TOKEN) {
//                                    TokenDetail.category = AxiomCredentialDetails.PKI_HARDWARE_TOKEN;
//                                    log.debug("PKITokenManagement category" + "" + PKITokenManagement.HARDWARE_TOKEN);
//                                }
//
//                                TokenDetail.subcategory = tDetails[i].getSubcategory();
//                                log.debug("PKITokenManagement subcategory" + "" + tDetails[i].getSubcategory());
//                                TokenDetail.Password = null;
//                                TokenDetail.attempts = tDetails[i].getAttempts();
//                                log.debug("PKITokenManagement attempts" + "" + tDetails[i].getAttempts());
//                                TokenDetail.createOn = tDetails[i].getCreationdatetime().getTime();
//                                TokenDetail.lastaccessOn = tDetails[i].getLastaccessdatetime().getTime();
//                                TokenDetail.status = tDetails[i].getStatus();
//                                log.debug("PKITokenManagement status" + "" + tDetails[i].getStatus());
//                                TokenDetail.serialnumber = tDetails[i].getSrno();
//                                log.debug("PKITokenManagement serialnumber" + "" + tDetails[i].getSrno());
//                                AlDetails.add(TokenDetail);
//
//                            }
//                        }
//
//                    }
//                    if (!"".equals(USERID)) {
//
//                        CertificateManagement certManagement = new CertificateManagement();
//                        Certificates cert = certManagement.getCertificate(sessionid, session.getChannelid(), USERID);
//
//                        if (cert != null) {
//
//                            AxiomCredentialDetails CertDetails = new AxiomCredentialDetails();
//                            CertDetails.category = AxiomCredentialDetails.CERTIFICATE;
//                            log.debug("##getAPuser:##getCertificate:category" + AxiomCredentialDetails.CERTIFICATE);
//                            CertDetails.Password = cert.getPfxpassword();
//                            CertDetails.createOn = cert.getCreationdatetime().getTime();
//                            CertDetails.serialnumber = cert.getSrno();
//                            log.debug("##getAPuser:##getCertificate:serialnumber" + cert.getSrno());
//                            CertDetails.status = cert.getStatus();
//                            log.debug("##getAPuser:##getCertificate:status" + cert.getStatus());
//                            CertDetails.base64Cert = cert.getCertificate();
//                            CertDetails.expiryDate = cert.getExpirydatetime().getTime();
//                            log.debug("##getAPuser:##getCertificate:expiryDate" + cert.getExpirydatetime().getTime());
//                            AlDetails.add(CertDetails);
//
//                        } else {
//                            log.debug("Certificate entry is null");
//                        }
//                    }
//                    if (!"".equals(USERID)) {
//
//                        AxiomCredentialDetails ChallengeResponseDetails = new AxiomCredentialDetails();
//                        ChallengeResponsemanagement ChalResp = new ChallengeResponsemanagement();
//                        AxiomQuestionsAndAnswers qanda = ChalResp.getUserQuestionsandAnswers(sessionid, session.getChannelid(), USERID);
//
//                        if (qanda != null) {
//                            ChallengeResponseDetails.qas = new AxiomChallengeResponse();
//                            ChallengeResponseDetails.qas.webQAndA = qanda.webQAndA;
//                            AlDetails.add(ChallengeResponseDetails);
//                        } else {
//                            log.info("AxiomQuestionsAndAnswers for this user not Available");
//                        }
//
//                    }
//                    System.out.println("9::" + new Date());
//
//                    atDetails = new AxiomCredentialDetails[AlDetails.size()];
//                    for (int i = 0; i < AlDetails.size(); i++) {
//                        atDetails[i] = AlDetails.get(i);
//                    }
//
//                    System.out.println("10::" + new Date());
//
//                    AxiomUCred = new AxiomUserCerdentials();
//                    AxiomUCred.axiomUser = axiomUser;
//                    AxiomUCred.tokenDetails = atDetails;
//
//                    if (AxiomUCred != null) {
//                        retValue = 0;
//                        resultStr = "Success";
//                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                                req.getRemoteAddr(), channel.getName(),
//                                session.getLoginid(), session.getLoginid(), new Date(),
//                                "GET USER", resultStr, retValue,
//                                "User Management",
//                                "Name=" + AxiomUCred.axiomUser.userName + ",Phone=" + AxiomUCred.axiomUser.phoneNo + ",Email=" + AxiomUCred.axiomUser.email,
//                                "", "USER", AxiomUCred.axiomUser.userId);
//                    } else if (AxiomUCred == null) {
//                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                                req.getRemoteAddr(), channel.getName(),
//                                session.getLoginid(), session.getLoginid(), new Date(),
//                                "GET USER", resultStr, retValue,
//                                "User Management", "", "", "USER", "");
//                    }
//
//                    System.out.println("exiting::" + new Date());
//                    log.info("#getApUser: Method Exit");
//                    return AxiomUCred;
//
//                }
//            }
//        } catch (Exception ex) {
//            log.error(String.format("##getApuser :  - exception caught when calling getApuser() - ", ex.getMessage()));
//
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//    @Override
//    public AxiomStatus ResetAPUser(String sessionid, String userid, int type) {
//
//        String strDebug = null;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//
//                log.info("##ResetAPUser: Started");
//                log.info("##ResetAPUser: sessionid = " + sessionid);
//                log.info("##ResetAPUser: type = " + type);
//                log.info("##ResetAPUser: userid = " + userid);
//
//                System.out.println("ResetUser::sessionId::" + sessionid);
//                System.out.println("ResetUser::userid::" + userid);
//                System.out.println("ResetUser::type::" + type);
//            }
//        } catch (Exception ex) {
//            log.error(String.format("##ResetUser :  - exception caught when calling ResetUser() - ", ex.getMessage()));
//        }
//
////        int iResult = 0;
////        if (iResult != 0) {
////            AxiomStatus aStatus = new AxiomStatus();
////            aStatus.error = "Licence invalid";
////            aStatus.errorcode = -100;
////            return aStatus;
////        }
//        try {
////           int iResult = 0;
////            if (iResult != 0) {
////                AxiomStatus aStatus = new AxiomStatus();
////                aStatus.error = "Licence Not Valid";
////                aStatus.errorcode = -10;
////                return aStatus;
////            }
//            SessionManagement sManagement = new SessionManagement();
//            Sessions session = sManagement.getSessionById(sessionid);
//            AuditManagement audit = new AuditManagement();
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "ERROR";
//            aStatus.errorcode = -9;
//
//            int retValue = -1;
//            String strCategory = "";
//            if (type == RESET_USER_TOKEN_SOFTWARE) {
//                strCategory = "SOFTWARE_TOKEN";
//            } else if (type == RESET_USER_TOKEN_HARDWARE) {
//                strCategory = "HARDWARE_TOKEN";
//            } else if (type == RESET_USER_TOKEN_OOB) {
//                strCategory = "OOB_TOKEN";
//            }
//            String strSubCategory = "";
//            if (session == null) {
//                aStatus.error = "Invalid Session";
//                log.debug("ResetAPUser Session is null");
//                aStatus.errorcode = -14;
//                return aStatus;
//            }
//
//            ChannelManagement cManagement = new ChannelManagement();
//            Channels channel = cManagement.getChannelByID(session.getChannelid());
//
//            if (channel == null) {
//                aStatus.error = "Invalid Channel";
//                log.debug("Channel is null");
//                aStatus.errorcode = -3;
//                return aStatus;
//            }
//            if (session != null) {
//                log.debug("ResetAPUser channel" + channel.getChannelid());
//                MessageContext mc = wsContext.getMessageContext();
//                HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//                //System.out.println("Client IP = " + req.getRemoteAddr());        
//                SettingsManagement setManagement = new SettingsManagement();
//                String channelid = session.getChannelid();
//
//                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//                if (ipobj != null) {
//                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                    int checkIp = 1;
//                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                        log.debug("##ResetAPUser:checkIp" + checkIp);
//                    } else {
//                        checkIp = 1;
//                    }
//                    if (iObj.ipstatus == 0 && checkIp != 1) {
//                        if (iObj.ipalertstatus == 0) {
//                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                            Session sTemplate = suTemplate.openSession();
//                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                            OperatorsManagement oManagement = new OperatorsManagement();
//                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//
//                            if (aOperator != null) {
//                                String[] emailList = new String[aOperator.length - 1];
//                                for (int i = 1; i < aOperator.length; i++) {
//                                    emailList[i - 1] = aOperator[i].getEmailid();
//                                }
//                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                    String strsubject = templatesObj.getSubject();
//                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                    if (strmessageBody != null) {
//                                        // Date date = new Date();
//                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                    }
//
//                                    SendNotification send = new SendNotification();
//                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                                    log.debug("##ResetAPUser SendEmail" + axStatus.strStatus);
//                                }
//
//                                suTemplate.close();
//                                sTemplate.close();
//                                aStatus.error = "INVALID IP";
//                                log.info("##ResetAPUser :INVALID IP");
//                                aStatus.errorcode = -8;
//                                return aStatus;
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
//                            aStatus.error = "INVALID IP";
//                            log.info("##ResetAPUser :INVALID IP");
//                            aStatus.errorcode = -8;
//                            return aStatus;
//                        }
//                        aStatus.error = "INVALID IP";
//                        log.info("##ResetAPUser :INVALID IP");
//                        aStatus.errorcode = -8;
//                        return aStatus;
//                    }
//                }
//                if (type == RESET_USER_PASSWORD) {
//                    UserManagement uManagement = new UserManagement();
//                    retValue = uManagement.resetPassword(sessionid, session.getChannelid(), userid);
//                    log.debug("##ResetAPUser :#resetPassword:retVal" + retValue);
//                    if (retValue == 0) {
//                        aStatus.error = "SUCCESS";
//                        aStatus.errorcode = 0;
//                        //return aStatus;
//                    } else {
//                        aStatus.errorcode = -20;
//                        aStatus.error = "Password Reset Failed (" + retValue + ")";
//                        log.debug("##ResetAPUser :#resetPassword:retVal" + retValue + "Password Reset Failed");
//                        // return aStatus;
//                    }
//
//                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                            req.getRemoteAddr(), channel.getName(),
//                            session.getLoginid(), session.getLoginid(),
//                            new Date(), "RESET",
//                            aStatus.error, aStatus.errorcode,
//                            "User Management",
//                            "Old Password = ******",
//                            "New Password = ******",
//                            "PASSWORD", userid);
//
//                    return aStatus;
//                } else if (type == RESET_USER_TOKEN_OOB) {
//
//                    OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//                    TokenStatusDetails[] tokens = oManagement.getTokenList(sessionid, session.getChannelid(), userid);
//                    TokenStatusDetails tokenSelected = null;
//                    for (int i = 0; i < tokens.length; i++) {
//                        if (tokens[i].Catrgory == OTPTokenManagement.OOB_TOKEN) {
//                            log.info("#ResetAPUser" + OTPTokenManagement.OOB_TOKEN);
//                            tokenSelected = tokens[i];
//                            break;
//                        }
//                    }
//
//                    if (tokenSelected == null) {
//                        aStatus.errorcode = -2;
//                        aStatus.error = "Desired OOB Token is not assigned";
//                        log.info("Desired OOB Token is not assigned");
//                        return aStatus;
//                    }
//
//                    if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_SMS) {
//                        strSubCategory = "OOB__SMS_TOKEN";
//                        log.debug("#ResetAPUser:SubCategory" + "OOB__SMS_TOKEN");
//                    } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_USSD) {
//                        log.debug("#ResetAPUser:SubCategory" + "OTP_TOKEN_OUTOFBAND_USSD");
//                        strSubCategory = "OOB__USSD_TOKEN";
//                    } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_VOICE) {
//                        log.debug("#ResetAPUser:SubCategory" + "OTP_TOKEN_OUTOFBAND_VOICE");
//                        strSubCategory = "OOB__VOICE_TOKEN";
//                    } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_EMAIL) {
//                        log.debug("#ResetAPUser:SubCategory" + "OTP_TOKEN_OUTOFBAND_EMAIL");
//                        strSubCategory = "OOB__EMAIL_TOKEN";
//                    }
//
//                    retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), userid, TOKEN_STATUS_UNASSIGNED, OTPTokenManagement.OOB_TOKEN, tokenSelected.SubCategory);
//                    log.debug("##ResetAPUser #ChangeStatus:retValue" + retValue);
//                    if (retValue == 0) {
//                        aStatus.error = "SUCCESS";
//                        aStatus.errorcode = 0;
//                    } else {
//                        aStatus.errorcode = -21;
//                        aStatus.error = "OOB Token Reset Failed (" + retValue + ")";
//                        //   return aStatus;
//                    }
//
//                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                            req.getRemoteAddr(), channel.getName(),
//                            session.getLoginid(), session.getLoginid(), new Date(),
//                            "RESET", aStatus.error, aStatus.errorcode,
//                            "Token Management",
//                            "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
//                            "New Status =" + strUNASSIGNED,
//                            "OOBTOKEN", userid);
//
//                    return aStatus;
//
//                } else if (type == RESET_USER_TOKEN_SOFTWARE) {
//
//                    OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//                    TokenStatusDetails[] tokens = oManagement.getTokenList(sessionid, session.getChannelid(), userid);
//                    TokenStatusDetails tokenSelected = null;
//                    for (int i = 0; i < tokens.length; i++) {
//                        if (tokens[i].Catrgory == OTPTokenManagement.SOFTWARE_TOKEN) {
//                            tokenSelected = tokens[i];
//                            break;
//                        }
//                    }
//
//                    if (tokenSelected == null) {
//                        //return ERROR;
//                        aStatus.errorcode = -2;
//                        aStatus.error = "Desired Software Token is not assigned";
//                        return aStatus;
//                    }
//
//                    if (tokenSelected.SubCategory == OTP_TOKEN_SOFTWARE_WEB) {
//                        strSubCategory = "SW_WEB_TOKEN";
//                    } else if (tokenSelected.SubCategory == OTP_TOKEN_SOFTWARE_MOBILE) {
//                        strSubCategory = "SW_MOBILE_TOKEN";
//                    } else if (tokenSelected.SubCategory == OTP_TOKEN_SOFTWARE_PC) {
//                        strSubCategory = "SW_PC_TOKEN";
//                    }
//
//                    retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), userid, TOKEN_STATUS_UNASSIGNED, OTPTokenManagement.SOFTWARE_TOKEN, tokenSelected.SubCategory);
//
//                    if (retValue == 0) {
//                        aStatus.error = "SUCCESS";
//                        aStatus.errorcode = 0;
//                    } else {
//                        aStatus.errorcode = -70;
//                        log.info("Software Token Reset Failed ");
//                        aStatus.error = "Software Token Reset Failed (" + retValue + ")";
//                        //  return aStatus;
//                    }
//
//                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                            req.getRemoteAddr(), channel.getName(),
//                            session.getLoginid(), session.getLoginid(), new Date(),
//                            "RESET", aStatus.error, aStatus.errorcode,
//                            "Token Management",
//                            "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
//                            "New Status =" + strUNASSIGNED,
//                            "SOFTWARETOKEN", userid);
//
////                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
////                            session.getLoginid(), session.getLoginid(), new Date(), "Change OTP Status", aStatus.error, aStatus.errorcode,
////                            "Token Management", "Category =" + strCategory + "SubCategory =" + strSubCategory + "Old Status =" + tokenSelected.Status,
////                            "Category =" + strCategory + "New Status =" + strUNASSIGNED,
////                            "RESET TOKEN", userid);
//                    return aStatus;
//                } else if (type == RESET_USER_TOKEN_HARDWARE) {
//                    log.info("RESET_USER_TOKEN_HARDWARE");
//                    OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//                    TokenStatusDetails[] tokens = oManagement.getTokenList(sessionid, session.getChannelid(), userid);
//                    TokenStatusDetails tokenSelected = null;
//                    for (int i = 0; i < tokens.length; i++) {
//                        if (tokens[i].Catrgory == OTPTokenManagement.HARDWARE_TOKEN) {
//                            tokenSelected = tokens[i];
//                            break;
//                        }
//                    }
//
//                    if (tokenSelected == null) {
//                        //return ERROR;
//                        aStatus.errorcode = -24;
//                        aStatus.error = "Desired Hardware Token is not assigned";
//                        log.info("Desired Hardware Token is not assigned");
//                        return aStatus;
//                    }
//
//                    if (tokenSelected.SubCategory == OTP_TOKEN_HARDWARE_CR) {
//                        strSubCategory = "HW_CR_TOKEN";
//                        log.info("#RESETApUser :Hardware Token Subcategory:HW_CR_TOKEN");
//                    } else if (tokenSelected.SubCategory == OTP_TOKEN_HARDWARE_MINI) {
//                        log.info("#RESETApUser :Hardware Token Subcategory:HW_MINI_TOKEN");
//                        strSubCategory = "HW_MINI_TOKEN";
//                    }
//                    retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), userid, TOKEN_STATUS_UNASSIGNED, OTPTokenManagement.HARDWARE_TOKEN, tokenSelected.SubCategory);
//                    log.debug("##RESETApUser #ChangeStatus:retValue" + retValue);
//                    if (retValue == 0) {
//                        aStatus.error = "SUCCESS";
//                        aStatus.errorcode = 0;
//                    } else {
//                        aStatus.errorcode = -71;
//                        aStatus.error = "Hardware Token Reset Failed (" + retValue + ")";
//                        log.info("#RESETApUser:Hardware Token Reset Failed");
//                        //  return aStatus;
//                    }
//
//                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                            req.getRemoteAddr(), channel.getName(),
//                            session.getLoginid(), session.getLoginid(), new Date(),
//                            "RESET", aStatus.error, aStatus.errorcode,
//                            "Token Management",
//                            "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
//                            "New Status =" + strUNASSIGNED,
//                            "HARDWARETOKEN", userid);
//
////                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
////                            session.getLoginid(), session.getLoginid(), new Date(), "Change OTP Status", aStatus.error, aStatus.errorcode,
////                            "Token Management", "Category =" + strCategory + "SubCategory =" + strSubCategory + "Old Status =" + tokenSelected.Status,
////                            "Category =" + strCategory + "New Status =" + strUNASSIGNED,
////                            "RESET TOKEN", userid);
//                    return aStatus;
//
//                } else if (type == RESET_USER_MOBILETRUST) {
//                    log.info("#RESETApUser:IN RESET_USER_MOBILETRUST");
//                    MobileTrustManagment mManagment = new MobileTrustManagment();
//
//                    retValue = mManagment.eraseMobileTrustCredential(sessionid, channel.getChannelid(), userid);
//                    log.debug("RESETApUser #eraseMobileTrustCredential" + retValue);
//                    if (retValue == 0) {
//                        aStatus.error = "SUCCESS";
//                        aStatus.errorcode = 0;
//                    } else {
//
//                        aStatus.errorcode = -72;
//                        aStatus.error = "Mobile Trust Acccount Reset Failed (" + retValue + ")";
//                        log.info("#RESETApUser:Mobile Trust Acccount Reset Failed");
//                        if (retValue == -3) {
//                            aStatus.error = "Mobile Trust Acccount was not present/active";
//                            log.info("#RESETApUser:Mobile Trust Acccount was not present/active");
//                            aStatus.errorcode = -73;
//                        }
//
//                        //  return aStatus;
//                    }
//
//                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                            req.getRemoteAddr(), channel.getName(),
//                            session.getLoginid(), session.getLoginid(), new Date(),
//                            "RESET", aStatus.error, aStatus.errorcode,
//                            "MOBILETRUST",
//                            "ALL TOKENS AND CERTIFICATE",
//                            "New Status =" + strUNASSIGNED,
//                            "MOBILETRUST", userid);
//
//                    return aStatus;
//
//                }
//            }
//
//            return aStatus;
//        } catch (Exception e) {
//            e.printStackTrace();
//            log.error(String.format("##resetAPUser :  - exception caught when calling resetAPUser() - ", e.getMessage()));
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "General Exception::" + e.getMessage();
//            aStatus.errorcode = -99;
//            return aStatus;
//        }
//
//    }
//
//    @Override
//    public AxiomStatus AssignMobileTrust(String sessionid, String userid) {
//        String strDebug = null;
//        int subtype = 2;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                log.info("##AssignMobileTrust: Started");
//                log.info("##AssignMobileTrust: sessionid = " + sessionid);
//
//                log.info("##AssignMobileTrust: userid = " + userid);
//                System.out.println("Assign::sessionId::" + sessionid);
//                System.out.println("Assign::userid::" + userid);
//                System.out.println("Assign::subtype::" + subtype);
//            }
//        } catch (Exception ex) {
//            log.error(String.format("##AssignMobileTrust :  - exception caught when calling AssignMobileTrust() - ", ex.getMessage()));
//        }
//
////        int iResult = 0;
////        if (iResult != 0) {
////            AxiomStatus aStatus = new AxiomStatus();
////            aStatus.error = "Licence invalid";
////            aStatus.errorcode = -100;
////            return aStatus;
////        }
////        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
////            AxiomStatus aStatus = new AxiomStatus();
////            aStatus.error = "OTP Token feature is not available in this license!!!";
////            aStatus.errorcode = -101;
////            return aStatus;
////        }
//        SessionManagement sManagement = new SessionManagement();
//        AuditManagement audit = new AuditManagement();
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        Sessions session = sManagement.getSessionById(sessionid);
//        AxiomStatus aStatus = new AxiomStatus();
//        int retValue = -1;
//        aStatus.error = "ERROR";
//        aStatus.errorcode = retValue;
//
//        if (session == null) {
//            aStatus.error = "Session Invalid!!!";
//            log.debug("Session Invalid!!!");
//            aStatus.errorcode = -14;
//            return aStatus;
//        }
//
//        String strCategory = "SOFTWARE_TOKEN";
//
//        String strSubCategory = "";
//        if (subtype == OTP_TOKEN_SOFTWARE_WEB) {
//            log.info("AssignMobileTrust:strSubCategory:OTP_TOKEN_SOFTWARE_WEB");
//            strSubCategory = "SW_WEB_TOKEN";
//        } else if (subtype == OTP_TOKEN_SOFTWARE_MOBILE) {
//            log.info("AssignMobileTrust:strSubCategory:OTP_TOKEN_SOFTWARE_MOBILE");
//            strSubCategory = "SW_MOBILE_TOKEN";
//        } else if (subtype == OTP_TOKEN_SOFTWARE_PC) {
//            log.info("AssignMobileTrust:strSubCategory:OTP_TOKEN_SOFTWARE_PC");
//            strSubCategory = "SW_PC_TOKEN";
//        }
//
//        ChannelManagement cManagement = new ChannelManagement();
//        Channels channel = cManagement.getChannelByID(session.getChannelid());
//        if (channel == null) {
//            aStatus.error = "Invalid Channel";
//            log.debug("AssignMobileTrust:Invalid Channel");
//            aStatus.errorcode = -3;
//            return aStatus;
//        }
//
//        if (session != null) {
//
//            SettingsManagement setManagement = new SettingsManagement();
//            String channelid = session.getChannelid();
//            log.debug("##AssignMobileTrust:channelid" + channelid);
//            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//            if (ipobj != null) {
//                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                int checkIp = 1;
//                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                    log.debug("##AssignMobileTrust:checkIp" + checkIp);
//                } else {
//                    checkIp = 1;
//                }
//                if (iObj.ipstatus == 0 && checkIp != 1) {
//                    if (iObj.ipalertstatus == 0) {
//                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                        Session sTemplate = suTemplate.openSession();
//                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                        OperatorsManagement oManagementObj = new OperatorsManagement();
//                        Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
//                        if (aOperator != null) {
//                            String[] emailList = new String[aOperator.length - 1];
//                            for (int i = 1; i < aOperator.length; i++) {
//                                emailList[i - 1] = aOperator[i].getEmailid();
//                            }
//                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                String strsubject = templatesObj.getSubject();
//                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                if (strmessageBody != null) {
//                                    // Date date = new Date();
//                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                }
//
//                                SendNotification send = new SendNotification();
//                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                                log.debug("##AssignMobileTrust:SendEmail axStatus:" + axStatus.strStatus);
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
//                            aStatus.error = "INVALID IP";
//                            log.info("INVALID IP");
//                            aStatus.errorcode = -8;
//                            return aStatus;
//                        }
//
//                        suTemplate.close();
//                        sTemplate.close();
//                        aStatus.error = "INVALID IP";
//                        log.info("INVALID IP");
//                        aStatus.errorcode = -8;
//                        return aStatus;
//                    }
//                    aStatus.error = "INVALID IP";
//                    log.info("INVALID IP");
//                    aStatus.errorcode = -8;
//                    return aStatus;
//                }
//            }
//
//            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//
//            //new addtion for license enforcement 
////            int tokenCountCurrentDBState = oManagement.getTokenCountForLicenseCheck(sessionid, channel.getChannelid(), OTPTokenManagement.SOFTWARE_TOKEN);
////            int licensecount = 0; // Axiom Protect fuction call here 
////            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
////                aStatus.error = "Software Token is not available in this license!!!";
////                aStatus.errorcode = -100;
////                return aStatus;
////            }
////            licensecount = AxiomProtect.GetSoftwareTokensAllowed();
////            if (licensecount == -998) {
////                //unlimited licensing 
////            } else {
////                if (tokenCountCurrentDBState >= licensecount) {
////                    aStatus.error = "Software Token already reached its limit as per this license!!!";
////                    aStatus.errorcode = -100;
////                    return aStatus;
////                }
////            }
//            //end of new addition
//            retValue = oManagement.AssignToken(sessionid, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE, subtype, userid);
//            log.debug("##AssignMobileTrust:AssignToken retValue:" + retValue);
//            if (retValue == 0) {
//                SessionFactoryUtil suSettings = new SessionFactoryUtil(SessionFactoryUtil.settings);
//                Session sSettings = suSettings.openSession();
//                MobileTrustSettings mSettings = null;
//                Object obj = setManagement.getSetting(sessionid, session.getChannelid(), SettingsManagement.MOBILETRUST_SETTING, 1);
//                if (obj != null) {
//                    if (obj instanceof MobileTrustSettings) {
//                        mSettings = (MobileTrustSettings) obj;
//                    }
//                }
//
//                sSettings.close();
//                suSettings.close();
//
//                if (mSettings == null) {
//                    log.debug("##AssignMobileTrust:Mobile Trust Setting is not configured!!!");
//                    aStatus.error = "Mobile Trust Setting is not configured!!!";
//                    aStatus.errorcode = -42;
//                    //aStatus.data = signData;
//                    return aStatus;
//                }
//
//                if (mSettings.bSilentCall == false) {
//
//                    //if (tSettings.isbSilentCall() == false) {
//                    String regCode = oManagement.generateRegistrationCode(sessionid, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE);
//                    SimpleDateFormat sdfExpiry = new SimpleDateFormat("hh:mm");
//                    if (regCode != null) {
//                        log.debug("##AssignMobileTrust:generateRegistrationCode: RegCode generated!!!");
//
////                        UserManagement userObj = new UserManagement();
////                        AuthUser user = null;
////                        user = userObj.getUser(sessionid, channel.getChannelid(), userid);
////
////                        Templates temp = null;
////                        TemplateManagement tObj = new TemplateManagement();
////                        temp = tObj.LoadbyName(sessionid, channel.getChannelid(), TemplateNames.MOBILE_SOFTWARE_TOKEN_REGISTER_TEMPLATE);
////                        ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
////                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
////                        Date d = new Date();
////                        String tmessage = (String) UtilityFunctions.deserializeFromObject(bais);
////                        tmessage = tmessage.replaceAll("#name#", user.getUserName());
////                        tmessage = tmessage.replaceAll("#channel#", channel.getName());
////                        tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
////                        tmessage = tmessage.replaceAll("#regcode#", regCode);
////                        tmessage = tmessage.replaceAll("#expiry#", "3 Mins");
////                        if (subtype == OTPTokenManagement.SW_WEB_TOKEN) {
////                            tmessage = tmessage.replaceAll("#tokentype#", "WEB");
////
////                        }
////                        if (subtype == OTPTokenManagement.SW_MOBILE_TOKEN) {
////                            tmessage = tmessage.replaceAll("#tokentype#", "MOBILE");
////
////                        }
////                        if (subtype == OTPTokenManagement.SW_PC_TOKEN) {
////                            tmessage = tmessage.replaceAll("#tokentype#", "PC");
////
////                        }
////
////                        SendNotification send = new SendNotification();
////                        //  AxiomStatus status = null;
////                        if (temp.getStatus() == tObj.ACTIVE_STATUS) {
////                            AXIOMStatus status = send.SendOnMobileNoWaiting(channel.getChannelid(), user.getPhoneNo(), tmessage, 1, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
////                        }
//                        retValue = 0;
//                        aStatus.error = "SUCCESS";
//                        aStatus.errorcode = 0;
//                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getChannelid(),
//                                req.getRemoteAddr(), channel.getName(),
//                                session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code",
//                                "success", 0, "Token Management", "", "Registration Code = ******" /*strRegCode*/, itemtype, userid);
//                    } else if (regCode == null) {
//                        aStatus.error = "Failed To generate registration code";
//                        aStatus.errorcode = -24;
//                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                                req.getRemoteAddr(), channel.getName(),
//                                session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code",
//                                "failed", -1, "Token Management", "", "Failed To generate Registration Code", itemtype, userid);
//                    }
//
//                } else {
//                    aStatus.error = "SUCCESS";
//                    aStatus.errorcode = 0;
//                }
//            }
//        }
//        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                session.getLoginid(), session.getLoginid(), new Date(), "Assign Token", aStatus.error, aStatus.errorcode,
//                "Token Management", "", "Category = " + strCategory + " Sub Category =" + strSubCategory,
//                itemtype, userid);
//        return aStatus;
//
//    }
//
//    @Override
//    public MobileTrustStatus ActivateMobileTrust(String sessionid, String userid, String clientPayload) {
//        String strDebug = null;
//        String certPass = null;
//        try {
//
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                log.info("##ActivateMobileTrust: Started");
//
//                log.info("##ActivateMobileTrust: sessionid = " + sessionid);
//                log.info("##ActivateMobileTrust: userid = " + userid);
//                log.info("##ActivateMobileTrust: userid = " + userid);
//                System.out.println("ActivateMobileTrust::clientPayload::" + clientPayload);
//                System.out.println("ActivateMobileTrust::clientPayload::" + clientPayload);
//                System.out.println("ActivateMobileTrust::SwTokenData::" + clientPayload);
//            }
//        } catch (Exception ex) {
//            log.error(String.format("##ActivateMobileTrust :  - exception caught when calling ActivateMobileTrust() - ", ex.getMessage()));
//        }
//
////        int iResult = 0;
////        if (iResult != 0) {
////            MobileTrustStatus aStatus = new MobileTrustStatus();
////            aStatus.error = "Licence Invalid";
////            aStatus.errorCode = -100;
////            aStatus.data = null;
////            return aStatus;
////        }
////        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.MOBILETRUST) != 0) {
////            MobileTrustStatus aStatus = new MobileTrustStatus();
////            aStatus.error = "OTP Token feature is not available in this license!!!";
////            aStatus.errorCode = -101;
////            return aStatus;
////        }
////       int iResult = 0;
////        if (iResult != 0) {
////            AxiomStatus aStatus = new AxiomStatus();
////            aStatus.error = "Licence Not Valid";
////            aStatus.errorcode = -10;
////            return aStatus;
////        }
//        MobileTrustStatus aStatus = new MobileTrustStatus();
//        aStatus.error = "ERROR";
//        aStatus.errorCode = -2;
//
//        SessionManagement sManagement = new SessionManagement();
//        Sessions session = sManagement.getSessionById(sessionid);
//
//        if (session == null) {
//
//            log.debug("Session Invalid!!!");
//            aStatus.error = "Session Invalid!!!";
//            aStatus.errorCode = -14;
//            return aStatus;
//        }
//
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        String signData = null;
//
//        String oldValue = "";
//        String strValue = "";
//        //String otp = null;
//        String cert = null;
//
//        if (session != null) {
//            try {
//                //System.out.println("Client IP = " + req.getRemoteAddr());
//                SettingsManagement setManagement = new SettingsManagement();
////                int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
////                if (result != 1) {
////                    aStatus.error = "INVALID IP REQUEST";
////                    aStatus.errorCode = -8;
////                    aStatus.data = signData;
////                    return aStatus;   //why return statement is commented
////                }
//
//                String channelid = session.getChannelid();
//                log.debug("#ActivateMobileTrust:channelid" + channelid);
//                ChannelManagement cManagement = new ChannelManagement();
//                Channels channel = cManagement.getChannelByID(channelid);
//                if (channel == null) {
//                    log.debug("Invalid Channel!!!");
//                    aStatus.error = "Invalid Channel!!!";
//                    aStatus.errorCode = -3;
//                    return aStatus;
//                }
//                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//                if (ipobj != null) {
//                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                    int checkIp = 1;
//                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                    } else {
//                        checkIp = 1;
//                    }
//                    if (iObj.ipstatus == 0 && checkIp != 1) {
//                        if (iObj.ipalertstatus == 0) {
//                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                            Session sTemplate = suTemplate.openSession();
//                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                            OperatorsManagement oManagement = new OperatorsManagement();
//                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                            if (aOperator != null) {
//                                String[] emailList = new String[aOperator.length - 1];
//                                for (int i = 1; i < aOperator.length; i++) {
//                                    emailList[i - 1] = aOperator[i].getEmailid();
//                                }
//                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                    String strsubject = templatesObj.getSubject();
//                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                    if (strmessageBody != null) {
//                                        // Date date = new Date();
//                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                    }
//
//                                    SendNotification send = new SendNotification();
//                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                                    log.debug("#ActivateMobileTrust #SendEmail:axStatus" + axStatus.strStatus);
//
//                                }
//
//                                suTemplate.close();
//                                sTemplate.close();
//                                aStatus.error = "INVALID IP";
//                                log.info("#ActivateMobileTrust:" + "INVALID IP");
//                                aStatus.errorCode = -8;
//                                return aStatus;
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
//                            aStatus.error = "INVALID IP";
//                            log.debug("#ActivateMobileTrust:" + "INVALID IP");
//                            aStatus.errorCode = -8;
//                            return aStatus;
//                        }
//                        aStatus.error = "INVALID IP";
//                        log.debug("#ActivateMobileTrust:" + "INVALID IP");
//                        aStatus.errorCode = -8;
//                        return aStatus;
//                    }
//                }
//                OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//
//                Otptokens oToken = oManagement.getOtpObjByUserId(sessionid, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE);
//                log.debug("#ActivateMobileTrust:getOtpObjByUserId" + "Called");
//                if (oToken == null) {
//                    log.debug("#ActivateMobileTrust:getOtpObjByUserId" + "oToken" + "Null");
//                    aStatus.errorCode = -20;
//                    aStatus.error = "Desired Token is not assigned";
//                    aStatus.data = signData;
//                    return aStatus;
//                }
//
//                if (oToken.getStatus() == TOKEN_STATUS_ACTIVE) {
//                    log.debug("#ActivateMobileTrust:getOtpObjByUserId" + "TokenStatus" + "TOKEN_STATUS_ACTIVE");
//                    oldValue = strACTIVE;
//
//                } else if (oToken.getStatus() == TOKEN_STATUS_SUSPENDED) {
//                    log.debug("#ActivateMobileTrust:getOtpObjByUserId" + "TokenStatus" + "TOKEN_STATUS_SUSPENDED");
//                    oldValue = strSUSPENDED;
//                } else if (oToken.getStatus() == TOKEN_STATUS_UNASSIGNED) {
//                    log.debug("#ActivateMobileTrust:getOtpObjByUserId" + "TokenStatus" + "TOKEN_STATUS_UNASSIGNED");
//                    oldValue = strUNASSIGNED;
//                }
//                //CryptoManagement cryManagement = new CryptoManagement();
//                strValue = strACTIVE;
//                //decrypt data here SwTokenData
//                //decrypt SwTokenData
//                String dSWData = clientPayload;
//                // dSWData
//
//                //String serverpublicKey = channel.getVisiblekey();
//                String serverPrivateKey = channel.getHiddenkey();
//                CryptoManager cm = new CryptoManager();
//                //which variable is userd for verifyneeded?
//                JSONObject jsonObj = new JSONObject(dSWData);
//                String device = jsonObj.getString("_deviceType");
//                int devicetype = 1;
//                if (device.equals("ANDROID")) {
//                    log.debug("#ActivateMobileTrust:" + "DeviceType" + "ANDROID");
//                    devicetype = ANDROID;
//
//                } else if (device.equals("IOS")) {
//                    log.debug("#ActivateMobileTrust:" + "DeviceType" + "IOS");
//                    devicetype = IOS;
//                }
//                if (device.equals("WebBrowser")) {
//                    devicetype = CryptoManager.webtrust;
//                    log.debug("#ActivateMobileTrust:" + "DeviceType" + "webtrust");
//                }
//                String jsonData = cm.ConsumeMobileTrust(dSWData, serverPrivateKey, true, CryptoManager.ACTIVATE,"");
//
//                JSONObject json = new JSONObject(jsonData);
//
//                String ip = json.getString("ip");
//                //String con = json.getString("con");
//                String longi = json.getString("longi");
//                String latti = json.getString("latti");
//                String mac = json.getString("macaddress");
//                String did = json.getString("did");
//                String vKey = json.getString("vKey");
//                String pKey = json.getString("pKey");
//                String os = json.getString("os");
//                String regcode = json.getString("regcode");
//                String pin = json.getString("pin");
//                String hiddenKey = pKey;
//                String visibleKey = vKey;
//
//                boolean isNewDevice = false;
//
//                UserManagement uManagement = new UserManagement();
//                String password = uManagement.GetPassword(sessionid, channelid, userid);
//                if (password != null) {
//                    if (pin.equals(password) == false) {
//                        log.debug("ActivateMobileTrust" + "Failed Due to password does not matched");
//                        aStatus.error = "Invalid User";
//                        aStatus.errorCode = -17;
//                        return aStatus;
//                    }
//                }
//
//                if (oToken.getStatus() == TOKEN_STATUS_ACTIVE) {
//                    log.debug("ActivateMobileTrust" + "Token Status" + "TOKEN_STATUS_ACTIVE");
//                    TrustDeviceManagement tManagement = new TrustDeviceManagement();
//                    Trusteddevice tDevice = tManagement.getTrusteddeviceByDeviceId(sessionid, channelid, userid, did);
//                    if (tDevice != null) {
//                        log.debug("#ActivateMobileTrust:Trusteddevice" + tDevice.getDeviceid());
//                        isNewDevice = true;
////                        aStatus.error = "Token is Already Active";
////                        aStatus.errorCode = -6;
////                        aStatus.data = signData;
////                        return aStatus;
//                    } else {
//                        log.debug("#ActivateMobileTrust:Trusteddevice" + "New device");
//                        isNewDevice = true;
//                    }
//
//                }
//
//                MobileTrustSettings mSettings = null;
//                int retValue = -1;
//                Object obj = setManagement.getSetting(sessionid, session.getChannelid(), SettingsManagement.MOBILETRUST_SETTING, 1);
//                if (obj != null) {
//                    if (obj instanceof MobileTrustSettings) {
//                        mSettings = (MobileTrustSettings) obj;
//                    }
//                }
//
//                RootCertificateSettings rcaSettings = (RootCertificateSettings) setManagement.getSetting(sessionid, session.getChannelid(), SettingsManagement.RootConfiguration, 1);
//                if (rcaSettings == null) {
//                    aStatus.error = "Certificate Setting is not configured!!!";
//                    log.error(String.format("##ActivateMobileTrust :  - exception caught when calling ActivateMobileTrust() - ", aStatus.error));
//                    aStatus.errorCode = -24;
//                    aStatus.data = signData;
//                    return aStatus;
//                }
//
//                if (mSettings == null) {
//                    aStatus.error = "Mobile Trust Setting is not configured!!!";
//                    log.error(String.format("##ActivateMobileTrust :  - exception caught when calling ActivateMobileTrust() - ", aStatus.error));
//                    aStatus.errorCode = -42;
//                    aStatus.data = signData;
//                    return aStatus;
//                }
//
//                TokenSettings tSettings = (TokenSettings) setManagement.getSetting(sessionid, session.getChannelid(), TOKEN, 1);
//                if (tSettings == null) {
//                    aStatus.error = "OTP Token Setting is not configured!!!";
//                    aStatus.errorCode = -25;
//                    log.error(String.format("##ActivateMobileTrust :  - exception caught when calling ActivateMobileTrust() - ", aStatus.error));
//                    aStatus.data = signData;
//                    return aStatus;
//                }
//
//                if (mSettings.bSilentCall == false) {
//
//                    if (regcode.compareTo(oToken.getRegcode()) != 0) {
//                        aStatus.error = "Invalid Registration Code";
//                        aStatus.errorCode = -17;
//                        log.error(String.format("##ActivateMobileTrust :  - exception caught when calling ActivateMobileTrust() - ", aStatus.error));
//                        aStatus.data = signData;
//                        return aStatus;
//                    }
//
//                    Date dExpiryTime = oToken.getExpirytime();
//                    Date dCurrent = new Date();
//
//                    if (dExpiryTime.before(dCurrent) == true) {
//                        aStatus.error = "Regitration code has expired.";
//                        log.error(String.format("##ActivateMobileTrust :  - exception caught when calling ActivateMobileTrust() - ", aStatus.error));
//                        aStatus.errorCode = -18;
//                        aStatus.data = signData;
//                        return aStatus;
//                    }
//                }
//
////                if (mSettings.bSilentCall == false ) {
////                    if (regcode.compareTo(oToken.getRegcode()) != 0) {
////                        aStatus.error = "Invalid Regitration Code";
////                        aStatus.errorCode = -17;
////                        aStatus.data = signData;
////                        return aStatus;
////                    }
////                }
////                Date dExpiryTime = oToken.getExpirytime();
////                Date dCurrent = new Date();
////
////                if (dExpiryTime.before(dCurrent) == true) {
////                    aStatus.error = "Regitration code has expired.";
////                    aStatus.errorCode = -18;
////                    aStatus.data = signData;
////                    return aStatus;
////                }
//                MobileTrustManagment mManagment = new MobileTrustManagment();
//                String strAxiomid = userid + mac + did + os + vKey; //add all device info and get new unique number
//                String axiomid = new String(Base64.encode(SHA1(strAxiomid)));
//                log.debug("axiomid" + axiomid);
////                String con = "";
////                String city = "";
////                String state = "";
////                String country = "";
////                String zipcode = "";
////
////                Location srvLoc = mManagment.getLocationByLongAndLat(longi, latti);
////                if (srvLoc != null) {
////                    con = srvLoc.country;
////                    city = srvLoc.city;
////                    state = srvLoc.state;
////                    country = con;
////                    zipcode = srvLoc.zipcode;
////                } else {
////                    aStatus.error = "Geo Address could not found for (" + longi + "," + latti + ").";
////                    aStatus.errorCode = -37;
////                    //aStatus.data = signData;
////                }
////
////                //changed from -1 to 0 for webtrust
////                int result = -1;
////                LocationManagement gLocation = new LocationManagement();
////                Countrylist clist = gLocation.getCountriesByid(Integer.parseInt(mSettings.homeCountryCode));
////                String countryName = null;
////                if (clist != null) {
////                    countryName = clist.getCountyName();
////                }
//////needd to chage changed for geolocaton offline
////                if (srvLoc.country.equalsIgnoreCase(countryName)) {
////                    result = 0;
////                } else {
////                    aStatus.error = "Home Country Is Different";
////                    aStatus.errorCode = -21;
////                    aStatus.data = signData;
////                    result = -21;
////                    AuditManagement audit = new AuditManagement();
////
////                    String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
////                    audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
////                            session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorCode,
////                            "Token Management", errorLocation, null, itemtype,
////                            session.getLoginid());
////
////                    return aStatus;
////                }
///////////////////////////////
//                //   String pubKey = null;
//                int result = 0;
//                String con = "INDIA";
////                  String con = "";
//                String city = "Pune";
//                String state = "MH";
//                String country = "INDIA";
//                String zipcode = "411001";
//                String prKey = null;
//                if (result == 0) {
//                    if (isNewDevice == false) {
//                        if (devicetype == IOS) {
//                            KeyPair kp = CryptoManagement.getAxiomKeyPair(pKey);
//                            PrivateKey pk = kp.getPrivate();
//                            PublicKey pubk = kp.getPublic();
//
//                            byte[] s = Base64.encode(pubk.getEncoded());
//                            vKey = new String(s);
//
//                            s = Base64.encode(pk.getEncoded());
//                            pKey = new String(s);
//                            cert = mManagment.AddCertificate(sessionid, session.getChannelid(), userid, oToken.getSrno(), vKey, pKey);
//                            log.debug("#ActivateMobileTrust: #Adding certificate");
//                        } else {
//                            cert = mManagment.AddCertificate(sessionid, session.getChannelid(), userid, oToken.getSrno(), vKey, pKey);
//                            log.debug("#ActivateMobileTrust: #Adding certificate");
//                        }
//                        if (password == null) {
//
//                            uManagement.AssignPassword(sessionid, channelid, userid, pin);
//                            log.debug("#ActivateMobileTrust: #Assigning passwords");
//                        }
//
//                    } else {
//                        CertificateManagement certManagement = new CertificateManagement();
//
//                        Certificates certObj = certManagement.getCertificate(sessionid, channelid, userid);
//
//                        if (devicetype == IOS) {
//                            KeyPair kp = CryptoManagement.getAxiomKeyPair(pKey);
//                            PrivateKey pk = kp.getPrivate();
//                            PublicKey pubk = kp.getPublic();
//
//                            byte[] s = Base64.encode(pubk.getEncoded());
//                            visibleKey = new String(s);
//
//                            s = Base64.encode(pk.getEncoded());
//                            hiddenKey = new String(s);
//
//                        }
//                        if (certObj != null) {
//                            cert = certObj.getCertificate();
//                            String filepath = certManagement.getPFXFile(sessionid, channelid, userid);
//                            log.debug("#ActivateMobileTrust: certificate#filepath" + filepath);
//                            byte[] pfxAsBytes = Base64.decode(certObj.getPfx());
//                            ByteArrayInputStream fis = new ByteArrayInputStream(pfxAsBytes);
//                            certPass = certObj.getPfxpassword();
//
////                            // supported KeyStore types  (JDK1.4): PKCS12 and JKS (native Sun)
//                            KeyStore ks = KeyStore.getInstance("PKCS12");
//                            ks.load(fis, certObj.getPfxpassword().toCharArray());
//                            for (Enumeration num = ks.aliases(); num.hasMoreElements();) {
//                                String alias = (String) num.nextElement();
//                                if (ks.isKeyEntry(alias)) {
//                                    PrivateKey priKey = (PrivateKey) ks.getKey(alias, certObj.getPfxpassword().toCharArray());
//                                    prKey = new String(Base64.encode(priKey.getEncoded()));
//
//                                    RSAPrivateCrtKey privk = (RSAPrivateCrtKey) priKey;
//
//                                    RSAPublicKeySpec publicKeySpec = new java.security.spec.RSAPublicKeySpec(privk.getModulus(), privk.getPublicExponent());
//
//                                    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
//                                    PublicKey myPublicKey = keyFactory.generatePublic(publicKeySpec);
//                                    vKey = new String(Base64.encode(myPublicKey.getEncoded()));
//                                }
//                            }
//                        }
//                    }
//
//                }
////                cert = "MIIECzCCA3SgAwIBAgIKAwJGtjNbPYhEDzANBgkqhkiG9w0BAQUFADCB8jEYMBYGA1UECwwPUHJvZHVjdCBTdXBwb3J0MRswGQYDVQQDDBJhZGZzLm1vbGxhdGVjaC5jb20xIjAgBgoJkiaJk/IsZAEBDBJhZGZzLm1vbGxhdGVjaC5jb20xJDAiBgkqhkiG9w0BCQEWFXN1cHBvcnRAbW9sbGF0ZWNoLmNvbTEVMBMGA1UEFBMMKzE0MDg3NTcwMDM2MQswCQYDVQQGEwJOWTEbMBkGA1UECgwSTW9sbGEgVGVjaG5vbG9naWVzMQswCQYDVQQHDAJOWTERMA8GA1UECAwITmV3IFlvcmsxDjAMBgNVBBEMBTEwOTg5MB4XDTE1MDExMDE2NTMyNFoXDTE3MDEwOTE2NTQyNFowggEVMR0wGwYDVQQLDBRCdXNpbmVzcyBEZXZlbG9wbWVudDEPMA0GA1UEAwwGc2FjaGluMTgwNgYKCZImiZPyLGQBAQwoODU0ZTA0ZTU3M2M5ZTQ0NWNlNTZhYzgzNTY2ODg0OTE3ZTBlN2RlODEjMCEGCSqGSIb3DQEJARYUc2FjaGluQG1vbGxhdGVjaC5jb20xFjAUBgNVBBQTDSs5MTg5ODMzNzI0NTcxFTATBgNVBAYTDEt1YWxhIEx1bXB1cjEbMBkGA1UECgwSTW9sbGEgVGVjaG5vbG9naWVzMRUwEwYDVQQHDAxLdWFsYSBMdW1wdXIxETAPBgNVBAgMCFNlbGFuZ29yMQ4wDAYDVQQRDAU1MDQ4MDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKdgNBAn1hcXc5A7xPEQMMsm/PcSrXc9oyIfcMjFxllq7lLJkpqbXk8mS1VuZSCIHa/cSDKJDe6cXxoVSIuJMmkB5cOckb7922YT6CRNtmYMM0g1PAIdrRSLrQeSKsvTxAUTXWD8g2ZlO7b1hA6GMj0p56ZMlbkMwo2QMdVarFD0pHkCAWfe/zi8noSA56FHoP8Uamz+up8pYppJbex0H55mKkJWjqt5U1VqOz4PRsMQlG/kprmDiLRMJeqEL5bN/wIyBKlupdqaKo33ZXpIpVSp5mtVE58L/GWIHH29ej3u1HfVp2guzNMmPgamcx3jeN3BNcrgxlvreOvQYWjtqwMCAwEAATANBgkqhkiG9w0BAQUFAAOBgQBrSE3YCHOQL7wvQrMWYWpSOIe03cLujppnJMtksrPCPqsgs7uVYfMkwJFoq6ZD95ZYoHzaWLdsPYzD6SqeKbr1Ah2+7msIWf85JTqe6Y6vkXfjsQ+0tHUBR5rBWqrTvznB6z2whKo4EQJxHEVeNmdP/kJsJLoddMGEqQ0xeUvdsg==";
//                if (cert == null && isNewDevice == false) {
//                    aStatus.error = "Digital Certificate Issuance Failed";
//                    log.error(String.format("##ActivateMobileTrust :  - exception caught when calling ActivateMobileTrust() - ", aStatus.error));
//                    aStatus.errorCode = -41;
//                    aStatus.data = signData;
//                }
//
//                if (result == 0 && cert != null) {
//                    retValue = mManagment.addTrustedDevice(sessionid, session.getChannelid(), userid, axiomid, did, mac, os, true, vKey, new Date(), new Date());
//                    log.debug("##ActivateMobileTrust:addTrustedDevice:retValue" + retValue);
//                    if (retValue == 0) {
//                        if (isNewDevice == false) {
//                            LocationManagement lManagement = new LocationManagement();
//                            Countrylist conObj = lManagement.getCountriesByName(con);
//                            if (conObj != null) {
//                                con = "" + conObj.getCountryid();
//                                log.debug("##ActivateMobileTrust:Countryid:" + con);
//                            } else {
//                                con = mSettings.homeCountryCode;
//                                log.debug("##ActivateMobileTrust::homeCountryCode" + mSettings.homeCountryCode);
//                            }
//                            retValue = mManagment.addGeoFence(sessionid, session.getChannelid(), userid, null, null, con, MobileTrustManagment.ROAMING_DISABLE, null, null);
//                            log.debug("##ActivateMobileTrust::addGeoFence:retValue:" + retValue);
//                        }
//                        if (retValue == 0) {
//                            retValue = mManagment.addGeoTrack(sessionid, session.getChannelid(), userid, longi, ip, latti,
//                                    city, state, country, zipcode,
//                                    MobileTrustManagment.REGISTER, new Date());
//                            log.debug("##ActivateMobileTrust::addGeoTrack:retValue:" + retValue);
//                        }
//
//                        if (retValue == 0 && isNewDevice == false) {
//                            retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), userid, TOKEN_STATUS_ACTIVE, OTP_TOKEN_SOFTWARE, oToken.getSubcategory());
//                            log.debug("##ActivateMobileTrust::ChangeStatus:retValue:" + retValue);
//                        }
//                        if (retValue == 0) {
//                            oToken = oManagement.getOtpObjByUserId(sessionid, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE);
//                            String secret = oToken.getSecret();;
//                            if (oToken != null && isNewDevice == false) {
//
//                                retValue = mManagment.AddPKIToken(sessionid, session.getChannelid(), userid, oToken.getSrno(), oToken.getCategory(), oToken.getRegcode());
//
//                                if (retValue == 0) {
//
//                                    signData = mManagment.confirmation(session.getChannelid(), userid, did, secret, cert, certPass, mac, vKey, serverPrivateKey, devicetype,oToken.getSrno());
//                                    log.debug("##ActivateMobileTrust::confirmation:signData:" + signData);
//                                    if (signData == null) {
//                                        aStatus.error = "Confirmation Message Could Not Be Generated.";
//                                        log.error(String.format("##ActivateMobileTrust :  - exception caught when calling ActivateMobileTrust() - ", aStatus.error));
//                                        aStatus.errorCode = -7;
//                                        //aStatus.data = signData;
//                                    }
//                                } else {
//                                    mManagment.eraseMobileTrustCredential(sessionid, channel.getChannelid(), userid);
//                                    aStatus.error = "PKI Token Assignment Failed!!!";
//                                    log.error(String.format("##ActivateMobileTrust :  - exception caught when calling ActivateMobileTrust() - ", aStatus.error));
//                                    aStatus.errorCode = -26;
//                                    aStatus.data = null;
//                                    //end
//                                }
//                                // String cert = cManagement.g
//                            } else {
//                                signData =  ""; //mManagment.reConfirmation(session.getChannelid(), userid, did, secret, cert, certPass, mac, visibleKey, serverPrivateKey, devicetype, hiddenKey);
//                                log.debug("##ActivateMobileTrust::reConfirmation:signData:" + signData);
//                                if (signData == null) {
//                                    aStatus.error = "Confirmation Message Could Not Be Generated.";
//                                    log.error(String.format("##ActivateMobileTrust :  - exception caught when calling ActivateMobileTrust() - ", aStatus.error));
//                                    aStatus.errorCode = -7;
//                                    //aStatus.data = signData;
//                                }
//
//                            }
//                        }
//
//                    }
//
//                }
//
//                if (retValue == 0) {
//                    aStatus.error = "SUCCESS";
//                    aStatus.errorCode = 0;
//                    aStatus.data = signData;
//                }
//
//                // return aStatus;
//            } catch (Exception ex) {
//                ex.printStackTrace();
//                aStatus.error = "General Exception::" + ex.getMessage();
//                log.error(String.format("##ActivateMobileTrust :  - exception caught when calling ActivateMobileTrust() - ", aStatus.error));
//                aStatus.errorCode = -99;
//                aStatus.data = signData;
//            }
//        }
//        ChannelManagement cManagement = new ChannelManagement();
//        Channels channel = cManagement.getChannelByID(session.getChannelid());
//        AuditManagement audit = new AuditManagement();
//        audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
//                session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorCode,
//                "Token Management", "Old Status :" + oldValue, "New Status :" + strValue, itemtype,
//                session.getLoginid());
//        log.info("##ACtivateMobileTrust Exited");
//        return aStatus;
//
//    }
//
//    @Override
//    public String GetTimeStamp(String sessionid, String userid) throws AxiomException {
//
//        String strDebug = null;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            log.info("##ActivateMobileTrust: sessionid = " + sessionid);
//            log.info("##ActivateMobileTrust: userid = " + userid);
//
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                System.out.println("GetTimeStamp::sessionid::" + sessionid);
//                System.out.println("GetTimeStamp::userid::" + userid);
//            }
//        } catch (Exception ex) {
//
//        }
//
//        if (sessionid == null) {
//            throw new AxiomException("Session ID is empty!!!");
//        }
//
////        int iResult = AxiomProtect.ValidateLicense();
////        if (iResult != 0) {
////            //return null;
////            throw new AxiomException("Invalid License!!!");
////        }
////        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.MOBILETRUST) != 0) {
////            throw new AxiomException("Mobile Trust Feature is not present in this license!!!");
////        }
//        SessionManagement sManagement = new SessionManagement();
//        Sessions session = sManagement.getSessionById(sessionid);
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        AxiomStatus aStatus = new AxiomStatus();
//        aStatus.error = "ERROR";
//        aStatus.errorcode = -2;
//        String oldValue = "";
//        String strValue = "";
//
//        if (session == null) {
//            throw new AxiomException("Session is invlaid/null!!!");
//        }
//
//        try {
//            SettingsManagement setManagement = new SettingsManagement();
//
//            String channelid = session.getChannelid();
//            ChannelManagement cManagement = new ChannelManagement();
//            Channels channel = cManagement.getChannelByID(channelid);
//            if (channel == null) {
//                throw new AxiomException("Invalid Channel!!!");
//            }
//            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//            if (ipobj != null) {
//                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                int checkIp = 1;
//                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                } else {
//                    checkIp = 1;
//                }
//                if (iObj.ipstatus == 0 && checkIp != 1) {
//                    if (iObj.ipalertstatus == 0) {
//                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                        Session sTemplate = suTemplate.openSession();
//                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                        suTemplate.close();
//                        sTemplate.close();
//
//                        OperatorsManagement oManagement = new OperatorsManagement();
//                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                        if (aOperator != null) {
//                            String[] emailList = new String[aOperator.length - 1];
//                            for (int i = 1; i < aOperator.length; i++) {
//                                emailList[i - 1] = aOperator[i].getEmailid();
//                            }
//                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                String strsubject = templatesObj.getSubject();
//                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                if (strmessageBody != null) {
//                                    // Date date = new Date();
//                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                }
//
//                                SendNotification send = new SendNotification();
//                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                            }
//                        }
//                    }
//                    throw new AxiomException("Invalid IP!!!");
//                }
//            }
//            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//            TokenStatusDetails tokenSelected = oManagement.getToken(sessionid, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE);
//
//            if (tokenSelected == null) {
////                    aStatus.errorcode = -2;
////                    aStatus.error = "Desired Token is not assigned";
//                throw new AxiomException("Desired Token is not assigned!!!");
//                //return null;
//            }
//
//            if (tokenSelected.Status == TOKEN_STATUS_ACTIVE) {
//                oldValue = strACTIVE;
//            } else if (tokenSelected.Status == TOKEN_STATUS_SUSPENDED) {
//                oldValue = strSUSPENDED;
//            } else if (tokenSelected.Status == TOKEN_STATUS_UNASSIGNED) {
//                oldValue = strUNASSIGNED;
//            }
//
//            strValue = strACTIVE;
//
//            MobileTrustManagment mManagment = new MobileTrustManagment();
//
//            String timeStamp = mManagment.GetTimeStamp(sessionid, session.getChannelid(), userid);
//
//            if (timeStamp != null) {
//                aStatus.error = "SUCCESS";
//                aStatus.errorcode = 0;
//            }
//
//            //ChannelManagement cManagement = new ChannelManagement();
//            //Channels channel = cManagement.getChannelByID(session.getChannelid());
//            AuditManagement audit = new AuditManagement();
//            audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
//                    session.getLoginid(), new Date(), "Get TimeStamp", aStatus.error, aStatus.errorcode,
//                    "MobileTrust Management", "", "New Session :" + strValue, itemtype,
//                    session.getLoginid());
//
//            if (timeStamp == null) {
//                throw new AxiomException("Failed to generate desired timestamp for this user!!!");
//            }
//
//            return timeStamp;
//            // return aStatus;
//        } catch (AxiomException ex) {
//            ex.printStackTrace();
//            //throw new AxiomException("Failed to generate desired timestamp for this user!!!");
//            throw ex;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            //throw new AxiomException("Failed to generate desired timestamp for this user!!!");
//            throw new AxiomException("General Exception::" + ex.getMessage());
//        }
//    }
//
//    @Override
//    public AxiomData VerifyCredentialAndSignDocuments(String sessionid, String userid, String clientPayload, String signature, DocumentSigner dSigner, boolean bNotifyUser) {
//        String strDebug = null;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                log.info("##VerifyCredentialAndSignDocuments:Method Started");
//                log.info("##VerifyCredentialAndSignDocuments: sessionid = " + sessionid);
//                log.info("##VerifyCredentialAndSignDocuments: userid = " + userid);
//
//                log.debug("VerifyCredentialAndSignDocuments::userid::" + userid);
//                log.debug("VerifyCredentialAndSignDocuments::data::" + clientPayload);
//                log.debug("VerifyCredentialAndSignDocuments::signature::" + signature);
//                log.debug("VerifyCredentialAndSignDocuments::bNotifyUser::" + bNotifyUser);
//
//            }
//        } catch (Exception ex) {
//            log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", ex.getMessage()));
//        }
//
////        int iResult = AxiomProtect.ValidateLicense();
////        if (iResult != 0) {
////            AxiomData aStatus = new AxiomData();
////            aStatus.sErrorMessage = "Licence Invalid";
////            aStatus.iErrorCode = -100;
////            return aStatus;
////        }
//        //OTPTokenManagement oManagement = new OTPTokenManagement();
//        CryptoManager crypto = new CryptoManager();
//        SessionManagement sManagement = new SessionManagement();
//        AuditManagement audit = new AuditManagement();
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        Sessions session = sManagement.getSessionById(sessionid);
//        AxiomData aStatus = new AxiomData();
//        int retValue = -1;
//        aStatus.sErrorMessage = "ERROR";
//        aStatus.iErrorCode = retValue;
//        if (session == null) {
//            aStatus.sErrorMessage = "Invalid Session";
//            log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", aStatus.sErrorMessage));
//            aStatus.iErrorCode = -14;
//            return aStatus;
//        }
//        ChannelManagement cManagement = new ChannelManagement();
//        Channels channel = cManagement.getChannelByID(session.getChannelid());
//        if (channel == null) {
//            aStatus.sErrorMessage = "Invalid Channel";
//            log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", aStatus.sErrorMessage));
//            aStatus.iErrorCode = -3;
//            return aStatus;
//        }
//        if (session != null) {
//
//            //System.out.println("Client IP = " + req.getRemoteAddr());        
//            SettingsManagement setManagement = new SettingsManagement();
////            int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
////            if (result != 1) {
////                aStatus.error = "INVALID IP REQUEST";
////                aStatus.errorcode = -8;
////                return aStatus;
////            }
//
//            String channelid = session.getChannelid();
//            log.debug("#VerifyCredentialAndSignDocuments:channelId" + channelid);
//            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//            if (ipobj != null) {
//                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                int checkIp = 1;
//                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                    log.debug("VerifyCredentialAndSignDocuments" + "checkIp" + checkIp);
//                } else {
//                    checkIp = 1;
//                }
//                if (iObj.ipstatus == 0 && checkIp != 1) {
//                    if (iObj.ipalertstatus == 0) {
//                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                        Session sTemplate = suTemplate.openSession();
//                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                        OperatorsManagement oManagement = new OperatorsManagement();
//                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                        if (aOperator != null) {
//                            String[] emailList = new String[aOperator.length - 1];
//                            for (int i = 1; i < aOperator.length; i++) {
//                                emailList[i - 1] = aOperator[i].getEmailid();
//                            }
//                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                String strsubject = templatesObj.getSubject();
//                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                if (strmessageBody != null) {
//                                    // Date date = new Date();
//                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                }
//
//                                SendNotification send = new SendNotification();
//                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                                log.debug("VerifyCredentialAndSignDocuments:SendEmail()" + axStatus.strStatus);
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
//                            aStatus.sErrorMessage = "INVALID IP";
//                            log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", aStatus.sErrorMessage));
//                            aStatus.iErrorCode = -8;
//                            return aStatus;
//                        }
//
//                        suTemplate.close();
//                        sTemplate.close();
//                        aStatus.sErrorMessage = "INVALID IP";
//                        log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", aStatus.sErrorMessage));
//                        aStatus.iErrorCode = -8;
//                        return aStatus;
//                    }
//                    aStatus.sErrorMessage = "INVALID IP";
//                    log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", aStatus.sErrorMessage));
//                    aStatus.iErrorCode = -8;
//                    return aStatus;
//                }
//            }
//            int devicetype = 1;
//            JSONObject jsonObj = null;
//            try {
//                jsonObj = new JSONObject(clientPayload);
//                String device = jsonObj.getString("_deviceType");
//                log.debug("##VerifyCredentialAndSignDocuments:device" + device);
//                if (device.equals("ANDROID")) {
//                    devicetype = ANDROID;
//                } else if (device.equals("IOS")) {
//                    devicetype = IOS;
//                }
//            } catch (Exception ex) {
//                log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", ex.getMessage()));
//                ex.printStackTrace();
//            }
//            MobileTrustManagment mManagment = new MobileTrustManagment();
//            String data = null;
//            if (dSigner != null) {
//                log.debug("DataToSign" + dSigner.dataToSign);
//                data = dSigner.dataToSign;
//            }
//            retValue = mManagment.CheckPlusComponentForSign(channel, userid, sessionid, data, signature, clientPayload, devicetype);
//            log.debug("##VerifyCredentialAndSignDocuments:CheckPlusComponentForSign:retValue" + retValue);
//            if (retValue == 0) {
//                aStatus.sErrorMessage = "Verify Signature Successfully";
//
//                aStatus.iErrorCode = 0;
//                String referenceid = null;
//
//                PDFSigningManagement pManagement = new PDFSigningManagement();
//                String plain = userid + session.getChannelid() + new Date();
//                String archiveid = UtilityFunctions.Bas64SHA1(plain);
//                //  referenceid="REF102030401";
//                if (dSigner.referenceId == null) {
//                    String refData = plain + archiveid + new Date();
//                    referenceid = UtilityFunctions.Bas64SHA1(refData);
//
//                } else {
//                    referenceid = dSigner.referenceId;
//                    log.debug("referenceId" + dSigner.referenceId);
//                }
//
//                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                        session.getLoginid(), session.getLoginid(), new Date(), "Verify Digital Signature ", aStatus.sErrorMessage, aStatus.iErrorCode,
//                        "Verify Digital Signature", "Signature = ******", " Signature = ******",
//                        itemtype, userid);
////                    PDFSigningManagement pManagement = new PDFSigningManagement();
////                String plain = userid + session.getChannelid() + new Date();
////                String archiveid = UtilityFunctions.Bas64SHA1(plain);
//                if (retValue == 0) {
//                    CertificateManagement certManagement = new CertificateManagement();
//                    Certificates cert = certManagement.getCertificate(sessionid, session.getChannelid(), userid);
//
//                    if (cert == null) {
//                        aStatus.sErrorMessage = "Certificate Not Issued!!";
//                        log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", aStatus.sErrorMessage));
//                        aStatus.iErrorCode = -27;
//
//                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                                session.getLoginid(), session.getLoginid(), new Date(), "Verify Signature And Sign Transaction ", aStatus.sErrorMessage, aStatus.iErrorCode,
//                                "Verify Signature And Sign Transaction Management", "data = ******, questions = ******", " signdata = ******,questions = ******",
//                                "Verify Signature And Sign Transaction", userid);
//                        return aStatus;
//                    }
//
//                    String pfxFile = cert.getPfx();
//                    String pfxPassword = cert.getPfxpassword();
//                    CryptoManagement cyManagement = new CryptoManagement();
//                    // String pfxFile = certManagement.sendPFXFile(sessionid, session.getChannelid(), userid);
//                    //String pfxPassword = certManagement.getPFXPassWord(sessionid, session.getChannelid(), userid);
//                    //call signData() here 
//
//                    boolean sResult = false;
//                    String sign = null;
//                    RemoteSigningInfo rsInfo = new RemoteSigningInfo();
//                    rsInfo.bAddTimestamp = dSigner.bAddTimestamp;
//                    rsInfo.bReturnSignedDocument = dSigner.bReturnSignedDocument;
//                    rsInfo.bNotifyUser = dSigner.bNotifyUser;
//                    rsInfo.clientLocation = dSigner.clientLocation;
//                    rsInfo.dataToSign = dSigner.dataToSign;
//                    rsInfo.designation = dSigner.designation;
//                    rsInfo.name = dSigner.name;
//                    rsInfo.reason = dSigner.reason;
//                    rsInfo.referenceId = dSigner.referenceId;
//                    rsInfo.type = dSigner.type;
//                    String pdfUrl = LoadSettings.g_sSettings.getProperty("pdf.show.url");
//                    pdfUrl = pdfUrl + "?_userid=" + userid + "&_archiveid=" + archiveid + "&_referenceid=" + referenceid;
//                    rsInfo.qrCodeData = pdfUrl;
//                    int result = -1;
//                    if (dSigner.type == RemoteSigningInfo.RAW_DATA) {
//                        sign = cyManagement.SignData(dSigner.dataToSign, pfxFile, pfxPassword); // signData();
//
//                        if (sign != null) {
//                            result = 0;
//                        }
//
//                        retValue = pManagement.addRemoteSignature(sessionid, session.getChannelid(), userid, archiveid, signature, retValue, new Date(), dSigner.type, dSigner.dataToSign, signature, result, new Date(), referenceid);
//
//                        if (retValue != 0) {
//
//                        }
//                    } else if (dSigner.type == RemoteSigningInfo.PDF) {
//                        AxiomPDFSignerWrapper apWrapper = new AxiomPDFSignerWrapper();
//                        try {
//                            Date d = new Date();
//                            String sourceFileName = userid + d.getTime();
//                            String filepath = LoadSettings.g_sSettings.getProperty("remote.sign.archive") + sourceFileName + ".pdf";
//                            log.debug("filepath" + filepath);
//                            sResult = pManagement.Base64ToPDF(dSigner.dataToSign, filepath);
//
//                            String destFileName = LoadSettings.g_sSettings.getProperty("remote.sign.archive") + sourceFileName + "-sign.pdf";
//                            log.debug("destFileName" + destFileName);
//                            String imagepath = LoadSettings.g_sSettings.getProperty("remote.sign.archive") + "imagefile.png";
//                            byte[] imageData = Base64.decode(dSigner.base64ImageData);
//                            try {
//                                FileOutputStream fos = new FileOutputStream(imagepath);
//                                fos.write(imageData);
//                                fos.close();
//                            } catch (Exception ex) {
//                                ex.printStackTrace();
//                            }
//                            sResult = true; //apWrapper.signpdf(userid, filepath, destFileName, pfxFile, pfxPassword, rsInfo, imagepath);
//                            log.debug("##VerifyCredentialAndSignDocuments AxiomPdfSigner Called signpdf() SResult:" + sResult);
//                            if (sResult == true) {
//                                signature = pManagement.fileToBase64(destFileName);
//                                retValue = 0;
//                                //for RHB to delete the file after singing 
//                                boolean source=false, destinaton=false;
//                                try{
//                                    source=new File(filepath).delete();        
//                                    log.debug("##VerifyCredentialAndSignDocuments source file delete:" + source);
//                                }catch(Exception ex){
//                                    log.error("##VerifyCredentialAndSignDocuments source file delete:" + ex);
//                                    
//                                }try{
//                                      destinaton=new File(destFileName).delete();
//                                      log.debug("##VerifyCredentialAndSignDocuments  destination file delete:" + destinaton);
//                                }catch(Exception ex){
//                                    log.error("##VerifyCredentialAndSignDocuments  destination file delete:" + ex);
//                                }
//                                //end of addition
//                                
//                            }
//
//                            //    sResult = pManagement.signPdf(userid, filepath, destFileName, pfxFile, pfxPassword, rsInfo, imagepath);
////                            if (sResult == true) {
////                                sign = pManagement.fileToBase64(destFileName);
////                            }
//                            int result1 = pManagement.addRemoteSignature(sessionid, session.getChannelid(), userid, archiveid, signature, retValue, new Date(), dSigner.type, filepath, destFileName, result, new Date(), referenceid);
//                            log.debug("##VerifyCredentialAndSignDocuments #addRemoteSignature():" + result1);
//                            if (retValue == 0) {
//
//                                aStatus.archiveid = archiveid;
//                                aStatus.signature = signature;
//                                aStatus.referenceid = referenceid;
//                                aStatus.sErrorMessage = "SUCCESS";
//                                aStatus.iErrorCode = 0;
//                                log.debug("VerifyCredentialAndSignDocuments:archiveid" + archiveid);
//                                log.debug("VerifyCredentialAndSignDocuments:referenceid" + referenceid);
//                                log.debug("VerifyCredentialAndSignDocuments:sErrorMessage" + "Success");
//                            }
//
//                        } catch (Exception ex) {
//                            log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", ex.getMessage()));
//                        }
//
//                    }
//
//                } else {
//
//                    pManagement.addRemoteSignature(sessionid, session.getChannelid(), userid, archiveid, signature, retValue, new Date(), dSigner.type, dSigner.dataToSign, null, -1, null, referenceid);
//
//                }
//
//                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                        session.getLoginid(), session.getLoginid(), new Date(), "Validate User And Sign Transaction ", aStatus.sErrorMessage, aStatus.iErrorCode,
//                        "Validate User And Sign Transaction Management", "data = ******, questions = ******", " signdata = ******,questions = ******",
//                        "Validate User TOKEN And Sign Transaction", userid);
//
////            SendNotification sNotification = new SendNotification();
////            PushNotificationDeviceManagement pManagement = new PushNotificationDeviceManagement();
////            String pushMessage = null;
////            if (aStatus.iErrorCode == 0) {
////                pushMessage = "You have transfered amount from your account " + VerifyRequest.signingData[1] + "  to  beneficiary's account : " + verifyRequest.signingData[2] + " for amount of " + verifyRequest.signingData[0];
////            } else {
////                pushMessage = "Your transtion has failed from your account " + verifyRequest.signingData[1] + "  to  beneficiary's account : " + verifyRequest.signingData[2] + " for amount of " + verifyRequest.signingData[0];
////                //pushMessage = "You are transferring amount from the specified account no : " + verifyRequest.signingData[1] + "  to the destination account : " + verifyRequest.signingData[2] + " and the amount you specified for transaction is :  " + verifyRequest.signingData[0] + " is failed to transferred. ";;
////            }
////
////            Registerdevicepush rDevice = pManagement.GetRegisterPushDeviceByUserID(sessionid, channelid, userid);
////            if (rDevice != null) {
////                sNotification.SendOnPush(channelid, rDevice.getGoogleregisterid(), rDevice.getGooglemeailid(), pushMessage, ANDROID);
////            }
////            return aStatus;
//            } else {
//                aStatus.iErrorCode = retValue;
//                if (aStatus.iErrorCode == -14) {
//                    aStatus.sErrorMessage = "Mobile Trust Setting is not configured!!!";
//                    log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", aStatus.sErrorMessage));
//                    aStatus.iErrorCode = -42;
//                } else if (aStatus.iErrorCode == -6) {
//                    aStatus.sErrorMessage = "Trusted Device not found!!!";
//                    log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", aStatus.sErrorMessage));
//                    aStatus.iErrorCode = -43;
//                } else if (aStatus.iErrorCode == -17) {
//                    aStatus.sErrorMessage = "Trusted Device is marked suspended";
//                    log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", aStatus.sErrorMessage));
//                    aStatus.iErrorCode = -44;
//                } else if (aStatus.iErrorCode == -5) {
//                    aStatus.sErrorMessage = "Trust Device mismatch!!!";
//                    log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", aStatus.sErrorMessage));
//                    aStatus.iErrorCode = -45;
//                } else if (aStatus.iErrorCode == -4) {
//                    aStatus.sErrorMessage = "Verification failed!!!";
//                    log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", aStatus.sErrorMessage));
//                    aStatus.iErrorCode = -46;
//                } else if (aStatus.iErrorCode == -42) {
//                    aStatus.sErrorMessage = "Digtial Signature mismatch!!!";
//                    log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", aStatus.sErrorMessage));
//                    aStatus.iErrorCode = -47;
//                } else if (aStatus.iErrorCode == -7) {
//                    aStatus.sErrorMessage = "Home Country is not defined!!!";
//                    log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", aStatus.sErrorMessage));
//                    aStatus.iErrorCode = -48;
//                } else if (aStatus.iErrorCode == -28) {
//                    aStatus.sErrorMessage = "TimeStamp is not issued!!";
//                    log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", aStatus.sErrorMessage));
//                    aStatus.iErrorCode = -49;
//                } else if (aStatus.iErrorCode == -27) {
//                    aStatus.sErrorMessage = "TimeStamp mistmatch!!!";
//                    log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", aStatus.sErrorMessage));
//                    aStatus.iErrorCode = -60;
//                } else if (aStatus.iErrorCode == -29) {
//                    aStatus.sErrorMessage = "TimeStamp has expired!!!";
//                    log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", aStatus.sErrorMessage));
//                    aStatus.iErrorCode = -61;
//                } else if (aStatus.iErrorCode == -10) {
//                    aStatus.sErrorMessage = "Invalid Location, out of home country!!!";
//                    log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", aStatus.sErrorMessage));
//                    aStatus.iErrorCode = -62;
//                } else if (aStatus.iErrorCode == -11) {
//                    aStatus.sErrorMessage = "Invalid Location, roaming country is not allowed!!!";
//                    log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", aStatus.sErrorMessage));
//                    aStatus.iErrorCode = -63;
//                } else if (aStatus.iErrorCode == -12) {
//                    aStatus.sErrorMessage = "Invalid Location, roaming is not configured properly!!!";
//                    log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", aStatus.sErrorMessage));
//                    aStatus.iErrorCode = -64;
//                }
//
//                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                        session.getLoginid(), session.getLoginid(), new Date(), "Verify Digital Signature ", aStatus.sErrorMessage, aStatus.iErrorCode,
//                        "Verify Digital Signature", "Signature = ******", " Signature = ******",
//                        itemtype, userid);
//            }
//        }
//        log.info("##VerifyCredentialAndSignDocuments:Method Ends");
//        return aStatus;
//    }
//
//    @Override
//    public AxiomStatus ALertMTEvent(String sessionid, String signData) {
//
//        String strDebug = null;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                System.out.println("ALertEvent::sessionid::" + sessionid);
//                System.out.println("ALertEvent::signData::" + signData);
//            }
//        } catch (Exception ex) {
//        }
//
////        int iResult = 0;
////        if (iResult != 0) {
////            AxiomStatus aStatus = new AxiomStatus();
////            aStatus.error = "Licence invalid";
////            aStatus.errorcode = -100;
////            return aStatus;
////        }
////        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.MOBILETRUST) != 0) {
////            AxiomStatus aStatus = new AxiomStatus();
////            aStatus.error = "Mobile Trust feature is not available in this license!!!";
////            aStatus.errorcode = -101;
////            return aStatus;
////        }
//        //int iResult = 0;
////        if (iResult != 0) {
////            AxiomStatus aStatus = new AxiomStatus();
////            aStatus.error = "Licence Not Valid";
////            aStatus.errorcode = -10;
////            return aStatus;
////        }
//        //OTPTokenManagement oManagement = new OTPTokenManagement();
//        SessionManagement sManagement = new SessionManagement();
//        AuditManagement audit = new AuditManagement();
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        Sessions session = sManagement.getSessionById(sessionid);
//        AxiomStatus aStatus = new AxiomStatus();
//        int retValue = -1;
//        aStatus.error = "ERROR";
//        aStatus.errorcode = retValue;
//        if (session == null) {
//            aStatus.error = "Invalid Session";
//            aStatus.errorcode = -14;
//            return aStatus;
//        }
//        ChannelManagement cManagement = new ChannelManagement();
//        Channels channel = cManagement.getChannelByID(session.getChannelid());
//        if (channel == null) {
//            aStatus.error = "Invalid Channel";
//            aStatus.errorcode = -3;
//            return aStatus;
//        }
//        if (session != null) {
//            try {
//
//                SettingsManagement setManagement = new SettingsManagement();
//
//                String channelid = session.getChannelid();
//
//                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//                if (ipobj != null) {
//                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                    int checkIp = 1;
//                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                    } else {
//                        checkIp = 1;
//                    }
//                    if (iObj.ipstatus == 0 && checkIp != 1) {
//                        if (iObj.ipalertstatus == 0) {
//                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                            Session sTemplate = suTemplate.openSession();
//                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                            OperatorsManagement oManagement = new OperatorsManagement();
//                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                            if (aOperator != null) {
//                                String[] emailList = new String[aOperator.length - 1];
//                                for (int i = 1; i < aOperator.length; i++) {
//                                    emailList[i - 1] = aOperator[i].getEmailid();
//                                }
//                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                    String strsubject = templatesObj.getSubject();
//                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                    if (strmessageBody != null) {
//                                        // Date date = new Date();
//                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                    }
//
//                                    SendNotification send = new SendNotification();
//                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                                }
//
//                                suTemplate.close();
//                                sTemplate.close();
//                                aStatus.error = "INVALID IP";
//                                aStatus.errorcode = -8;
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
//                            aStatus.error = "INVALID IP";
//                            aStatus.errorcode = -8;
//                        }
//                        aStatus.error = "INVALID IP";
//                        aStatus.errorcode = -8;
//                    }
//                }
//                MobileTrustManagment mManagment = new MobileTrustManagment();
//                retValue = mManagment.CheckForAlert(channel, sessionid, signData);
//                if (retValue == 0) {
//                    aStatus.error = "SUCCESS";
//                    aStatus.errorcode = 0;
//                } else {
//                    if (aStatus.errorcode == -2) {
//                        aStatus.error = "Session is not active";
//                    } else if (aStatus.errorcode == -6) {
//                        aStatus.error = "Trusted Device not found!!!";
//                    } else if (aStatus.errorcode == -17) {
//                        aStatus.error = "Trusted Device is marked suspended";
//                    } else if (aStatus.errorcode == -110) {
//                        aStatus.error = "Adding Abuse Alert Failed!!!";
//                    } else if (aStatus.errorcode == -60) {
//                        aStatus.error = "User not Found!!!";
//                    } else if (aStatus.errorcode == -30) {
//                        aStatus.error = "Trust Device mismatch!!!";
//                    }
//                }
//            } catch (Exception ex) {
//                ex.printStackTrace();
//                aStatus.error = "General Exception::" + ex.getMessage();
//                aStatus.errorcode = -99;
//            }
//
//        }
//        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                session.getLoginid(), session.getLoginid(), new Date(), "Alert User ", aStatus.error, aStatus.errorcode,
//                "Alert Management", "", " Successfully Destroy",
//                itemtype, session.getLoginid());
//
//        return aStatus;
//
//    }
//
//    @Override
//    public AxiomStatus VerifyOTPPlus(String sessionid, String userid, int type, String otp, String plusDetails) {
//
//        String strDebug = null;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//
//                log.info("VerifyOTPPlus:Method Started");
//                log.debug("VerifyOTPPlus::sessionid::" + sessionid);
//                log.debug("VerifyOTPPlus::userid::" + userid);
//                log.debug("VerifyOTPPlus::type::" + type);
//                log.debug("VerifyOTPPlus::otp::" + otp);
//                log.debug("VerifyOTPPlus::plusDetails::" + plusDetails);
//            }
//        } catch (Exception ex) {
//            log.error(String.format("##VerifyCredentialAndSignDocuments :  - exception caught when calling VerifyCredentialAndSignDocuments() - ", ex.getMessage()));
//        }
//
////        int iResult = AxiomProtect.ValidateLicense();
////        if (iResult != 0) {
////            AxiomStatus aStatus = new AxiomStatus();
////            aStatus.error = "Licence invalid";
////            aStatus.errorcode = -100;
////            return aStatus;
////        }
////        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.MOBILETRUST) != 0) {
////            AxiomStatus aStatus = new AxiomStatus();
////            aStatus.error = "Mobile Trust feature is not available in this license!!!";
////            aStatus.errorcode = -101;
////            return aStatus;
////        }
////        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_OOB) != 0
////                && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_HARDWARE) != 0
////                && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
////            AxiomStatus aStatus = new AxiomStatus();
////            aStatus.error = "OTP Token feature is not available in this license!!!";
////            aStatus.errorcode = -102;
////            return aStatus;
////        }
//        try {
//
//            int retValue = -10;
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "Invalid Parameters!!!";
//            aStatus.errorcode = retValue;
//
//            if (sessionid == null || userid == null || otp == null) {
//                log.error("SessionId  is null");
//                return aStatus;
//            }
//
//            SessionManagement sManagement = new SessionManagement();
//            AuditManagement audit = new AuditManagement();
//            MessageContext mc = wsContext.getMessageContext();
//            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//            Sessions session = sManagement.getSessionById(sessionid);
//            aStatus.error = "ERROR";
//            aStatus.errorcode = retValue;
//            if (session == null) {
//                aStatus.error = "Invalid Session";
//                log.error("Invalid Session");
//                aStatus.errorcode = -104;
//                return aStatus;
//            }
//            ChannelManagement cManagement = new ChannelManagement();
//            Channels channel = cManagement.getChannelByID(session.getChannelid());
//            if (channel == null) {
//                aStatus.error = "Invalid Channel";
//                log.error(aStatus.error);
//                aStatus.errorcode = -3;
//                return aStatus;
//            }
//
//            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//            if (session != null) {
//
//                SettingsManagement setManagement = new SettingsManagement();
//                String channelid = session.getChannelid();
//
//                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//                if (ipobj != null) {
//                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                    int checkIp = 1;
//                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                        log.debug("##VerifyOTPPlus:checkip" + checkIp);
//                    } else {
//                        checkIp = 1;
//                    }
//                    if (iObj.ipstatus == 0 && checkIp != 1) {
//                        if (iObj.ipalertstatus == 0) {
//                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                            Session sTemplate = suTemplate.openSession();
//                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                            OperatorsManagement oManagementObj = new OperatorsManagement();
//                            Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
//                            if (aOperator != null) {
//                                String[] emailList = new String[aOperator.length - 1];
//                                for (int i = 1; i < aOperator.length; i++) {
//                                    emailList[i - 1] = aOperator[i].getEmailid();
//                                }
//                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                    String strsubject = templatesObj.getSubject();
//                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                    if (strmessageBody != null) {
//                                        // Date date = new Date();
//                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                    }
//
//                                    SendNotification send = new SendNotification();
//                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                                    log.debug("##VerifyOTPPlus:SendEmail" + axStatus.strStatus);
//                                }
//
//                                suTemplate.close();
//                                sTemplate.close();
//                                aStatus.error = "INVALID IP";
//                                log.error(aStatus.error);
//                                aStatus.errorcode = -108;
//                                return aStatus;
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
//                            aStatus.error = "INVALID IP";
//                            log.error(aStatus.error);
//                            aStatus.errorcode = -108;
//                            return aStatus;
//                        }
//                        aStatus.error = "INVALID IP";
//                        log.error(aStatus.error);
//                        aStatus.errorcode = -108;
//                        return aStatus;
//                    }
//                }
//
//                MobileTrustManagment mManagment = new MobileTrustManagment();
//                JSONObject jsonObj = new JSONObject(plusDetails);
//                String device = jsonObj.getString("_deviceType");
//                log.debug("Verifyotp:device" + device);
//                int devicetype = 1;
//                if (device.equals("ANDROID")) {
//                    devicetype = ANDROID;
//                } else if (device.equals("IOS")) {
//                    devicetype = IOS;
//                }
//                if (plusDetails != null && !otp.contains(":")) {
//                    retValue = mManagment.CheckPlusComponent(channel, userid, sessionid, otp, plusDetails, devicetype);
//                    log.debug("##VerifyOTPPlus:CheckPlusComponent:retvalue:" + retValue);
//                    System.out.println("CheckPlusComponent returned::" + retValue);
//                    //added for error handling 
//                    if (retValue == -6) {
//                        aStatus.error = "Trusted device not found";
//                        log.error(aStatus.error);
//                        aStatus.errorcode = -6;
//                    } else if (retValue == -4) {
//                        aStatus.error = "OTP is invalid";
//                        log.error(aStatus.error);
//                        aStatus.errorcode = -4;
//                    } else if (retValue == -5) {
//                        aStatus.error = "Trusted Device mismatch";
//                        log.error(aStatus.error);
//                        aStatus.errorcode = -5;
//                    } else if (retValue == -7) {
//                        aStatus.error = "Invalid Location, out of home country!!!";
//                        log.error(aStatus.error);
//                        aStatus.errorcode = -7;
//                    } else if (retValue == -8) {
//                        aStatus.error = "Mobile Trust Setting is not configured!!!";
//                        log.error(aStatus.error);
//                        aStatus.errorcode = -118;
//                    } else if (retValue == -9) {
//                        aStatus.error = "Timestamp is null";
//                        log.error(aStatus.error);
//                        aStatus.errorcode = -9;
//                    } else if (retValue == -10) {
//                        aStatus.error = "TimeStamp mistmatch!!!";
//                        log.error(aStatus.error);
//                        aStatus.errorcode = -10;
//                    } else if (retValue == -11) {
//                        aStatus.error = "TimeStamp has expired!!!";
//                        log.error(aStatus.error);
//                        aStatus.errorcode = -11;
//                    } else if (retValue == -12) {
//                        aStatus.error = "Invalid Location, out of home country!!!";
//                        log.error(aStatus.error);
//                        aStatus.errorcode = -12;
//                    } else if (retValue == -13) {
//                        aStatus.error = "Invalid Location, roaming country is not allowed!!!";
//                        log.error(aStatus.error);
//                        aStatus.errorcode = -13;
//                    } else if (retValue == -14) {
//                        aStatus.error = "Roaming duration is over";
//                        log.error(aStatus.error);
//                        aStatus.errorcode = -15;
//                    } else if (retValue == -15) {
//                        //aStatus.error = "TimeStamp Updated failed";
//                        aStatus.error = "SUCCESS";
//                        aStatus.errorcode = 0;
//                    } else if (retValue == -2) {
//                        aStatus.error = "Session has expired";
//                        log.error(aStatus.error);
//                        aStatus.errorcode = -2;
//                    } else if (retValue == -1) {
//                        aStatus.error = "General Exception";
//                        log.error(aStatus.error);
//                        aStatus.errorcode = -16;
//                    }
//                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                            session.getLoginid(), session.getLoginid(), new Date(), "Mobile Trust PLUS Check", aStatus.error, aStatus.errorcode,
//                            "Token Management", aStatus.error, " ",
//                            itemtype, userid);
//
//                    //return aStatus;
//                    //end of addition
//                } else if (otp.contains(":")) {
//                    retValue = 0;
//                }
//                if (retValue == 0) {
//                    if (otp.contains(":")) {
//                        UtilityFunctions utility = new UtilityFunctions();
//                        String oldLatiLong = otp.substring(0, otp.indexOf(":"));
//                        String[] latilong = utility.DecodeGEOCode(oldLatiLong);
//                        String newOtp = otp.substring(otp.indexOf(":") + 1, otp.length());
//                        Location srvLoc = mManagment.getLocationByLongAndLat(latilong[1], latilong[0]);
//                        SessionFactoryUtil geouSession = new SessionFactoryUtil(SessionFactoryUtil.geotrack);
//                        Session geoSession = geouSession.openSession();
//                        GeoTrackingUtils geoUtil = new GeoTrackingUtils(geouSession, geoSession);
//                        SessionFactoryUtil guSession = new SessionFactoryUtil(SessionFactoryUtil.geofence);
//                        Session gSession = guSession.openSession();
//                        GeoFenceUtils gUtil = new GeoFenceUtils(guSession, gSession);
//                        try {
//                            //Location srvLocip = getLocationByIpAddress(ip);
//                            int val = geoUtil.addGeotrack(channel.getChannelid(), userid, latilong[1], latilong[0], req.getRemoteAddr(), srvLoc.city, srvLoc.state, srvLoc.country, srvLoc.zipcode, LOGIN, new Date());
//                            log.debug("##VerifyOTPPlus:addGeotrack result" + val);
//                            Geofence gFence = gUtil.getGeofence(userid, channel.getChannelid());
//                            log.debug("##VerifyOTPPlus:getGeofence");
//                            if (gFence == null) {
//                                aStatus.errorcode = -7;
//                                log.error("Geofence is null");
//
//                            }
//
//                            boolean b3434 = false;
//                            String locationUser = null;
//                            if (gFence != null) {
//                                //b3434 = gFence.getRoamingAllowed();
//                                b3434 = (gFence.getRoamingAllowed() != 0);
//                                locationUser = gFence.getHomeCountry();
//                                log.debug("##VerifyOTPPlus:HomeCountry" + locationUser);
//                            }
//
//                            if (b3434 == false) {
//                                if (srvLoc.country.equalsIgnoreCase(locationUser)) {
//                                    retValue = 0;
//                                } else {
//                                    retValue = -12;
//                                }
//                            } else {
//
//                                String[] foreignCountryList = null;
//                                if (gFence.getForeignCountry() != null) {
//                                    foreignCountryList = gFence.getForeignCountry().split(",");
//                                }
//                                if (foreignCountryList != null && foreignCountryList.length > 0) {
//                                    for (int i = 0; i < foreignCountryList.length; i++) {
//                                        if (srvLoc.country.equalsIgnoreCase(foreignCountryList[i])) {
//
//                                            long nowDate = new Date().getTime();
//                                            //System.out.println("Current Time::"+nowDate);
//                                            //System.out.println("Start Time::"+gFence.getStartDateForRoming().getTime() );
//                                            //System.out.println("End Time::"+gFence.getEndDateForRoming().getTime() );
//
//                                            Long startDiff = nowDate - gFence.getStartDateForRoming().getTime();
//                                            Long endDiff = gFence.getEndDateForRoming().getTime() - nowDate;
//                                            if (startDiff < 0 || endDiff < 0) {
//                                                retValue = -11;
//                                            }
//                                            retValue = 0;
//                                        }
//                                    }
//                                } else {
//                                    retValue = -13;
//                                }
//
////                                if (gFence.getForeignCountry().equalsIgnoreCase(srvLoc.country)) {
////                                    Long startDiff = new Date().getTime() - gFence.getStartDateForRoming().getTime();
////                                    Long endDiff = gFence.getEndDateForRoming().getTime() - new Date().getTime();
////                                    if (startDiff < 0 || endDiff < 0) {
////                                        retValue = -11;
////                                    }
////                                    retValue = 0;
////                                } else {
////                                    retValue = -12;
////                                }
//                            }
//                        } finally {
//                            geoSession.close();
//                            geouSession.close();
//                            gSession.close();
//                            guSession.close();
//                        }
//                        if (retValue == 0) {
//                            retValue = oManagement.VerifyOTP(session.getChannelid(), userid, sessionid, newOtp);
//                            log.debug("##VerifyOTPPlus:VerifyOTP resultvalue" + retValue);
//                        }
//                    } else {
//                        retValue = oManagement.VerifyOTP(session.getChannelid(), userid, sessionid, otp);
//                        log.debug("##VerifyOTPPlus:VerifyOTP resultvalue" + retValue);
//                    }
//                }
//
//                if (retValue == 0) {
//                    aStatus.error = "SUCCESS";
//                    aStatus.errorcode = 0;
//                } else if (retValue == -22) {
//                    aStatus.error = "OTP Token is locked";
//                    log.error("Geofence is null");
//                    aStatus.errorcode = -22;
//                } else if (retValue == -8) {
//                    aStatus.error = "OTP is already consumed";
//                    log.error("Geofence is null");
//                    aStatus.errorcode = -8;
//                } else if (retValue == -17) {
//                    aStatus.error = "OTP Token not found ";
//                    log.error("Geofence is null");
//                    aStatus.errorcode = -17;
//                } else if (retValue == -9) {
//                    aStatus.error = "OTP is expired";
//                    log.error("Geofence is null");
//                    aStatus.errorcode = -9;
//                } else if (retValue == -2) {
//                    aStatus.error = "Session has expired";
//                    log.error("Geofence is null");
//                    aStatus.errorcode = -2;
//                } else if (retValue == -1) {
//                    aStatus.error = "General Error";
//                    log.error("Geofence is null");
//                    aStatus.errorcode = -1;
//                }
//            }
//            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                    session.getLoginid(), session.getLoginid(), new Date(), "Verify OTP ", aStatus.error, aStatus.errorcode,
//                    "Token Management", "OTP = ******", " OTP = ******",
//                    itemtype, userid);
//            return aStatus;
//
//        } catch (Exception e) {
//            log.error("##VerifyOTPPlus" + e.getMessage());
//            e.printStackTrace();
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "General Exception::" + e.getMessage();
//            aStatus.errorcode = -99;
//            return aStatus;
//        }
//    }
//
//    @Override
//    public AxiomStatus VerifySignatureOTPPlus(String sessionid, String userid, int type, String[] data, String sotp, String sotpPlus) {
//
//        String strDebug = null;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                log.info("VerifySignatureOTPPlus::Method Started::");
//                log.info("VerifySignatureOTPPlus::sessionid::" + sessionid);
//                log.info("VerifySignatureOTPPlus::userid::" + userid);
//                log.info("VerifySignatureOTPPlus::type::" + type);
//                log.info("VerifySignatureOTPPlus::sotp::" + sotp);
//                log.info("VerifySignatureOTPPlus::sotpPlus::" + sotpPlus);
//                log.info("VerifySignatureOTPPlus::data.length::" + data.length);
//                for (int i = 0; i < data.length; i++) {
//                    System.out.println("VerifySignatureOTPPlus::data[" + i + "]::" + data[i]);
//                }
//            }
//        } catch (Exception ex) {
//            log.error(String.format("##VerifySignatureOTPPlus :  - exception caught when calling VerifySignatureOTPPlus - ", ex.getMessage()));
//        }
//
////        int iResult = AxiomProtect.ValidateLicense();
////        if (iResult != 0) {
////            AxiomStatus aStatus = new AxiomStatus();
////            aStatus.error = "Licence invalid";
////            aStatus.errorcode = -100;
////            return aStatus;
////        }
////        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.MOBILETRUST) != 0) {
////            AxiomStatus aStatus = new AxiomStatus();
////            aStatus.error = "Mobile Trust feature is not available in this license!!!";
////            aStatus.errorcode = -101;
////            return aStatus;
////        }
////
////        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_OOB) != 0
////                && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_HARDWARE) != 0
////                && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
////            AxiomStatus aStatus = new AxiomStatus();
////            aStatus.error = "OTP Token feature is not available in this license!!!";
////            aStatus.errorcode = -102;
////            return aStatus;
////        }
//        // int iResult = AxiomProtect.ValidateLicense();
////        if (iResult != 0) {
////            AxiomStatus aStatus = new AxiomStatus();
////            aStatus.error = "Licence Not Valid";
////            aStatus.errorcode = -10;
////            return aStatus;
////        }
//        int retValue = -10;
//        AxiomStatus aStatus = new AxiomStatus();
//        aStatus.error = "Invalid Parameters!!!";
//        aStatus.errorcode = retValue;
//
//        if (sessionid == null || userid == null || sotp == null) {
//            return aStatus;
//        }
//        SessionManagement sManagement = new SessionManagement();
//        AuditManagement audit = new AuditManagement();
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        Sessions session = sManagement.getSessionById(sessionid);
//
//        if (session == null) {
//
//            aStatus.error = "Invalid Session";
//            aStatus.errorcode = -104;
//            log.error(String.format("##VerifySignatureOTPPlus :  - exception caught when calling VerifySignatureOTPPlus - ", aStatus.error));
//
//            return aStatus;
//        }
//        ChannelManagement cManagement = new ChannelManagement();
//        Channels channel = cManagement.getChannelByID(session.getChannelid());
//        if (channel == null) {
//            aStatus.error = "Invalid Channel";
//            log.error(String.format("##VerifySignatureOTPPlus :  - exception caught when calling VerifySignatureOTPPlus - ", aStatus.error));
//            aStatus.errorcode = -3;
//            return aStatus;
//        }
//
//        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//        if (session != null) {
//
//            SettingsManagement setManagement = new SettingsManagement();
//            String channelid = session.getChannelid();
//            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//            if (ipobj != null) {
//                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                int checkIp = 1;
//                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                    log.debug("checkIP" + checkIp);
//                } else {
//                    checkIp = 1;
//                }
//                if (iObj.ipstatus == 0 && checkIp != 1) {
//                    if (iObj.ipalertstatus == 0) {
//                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                        Session sTemplate = suTemplate.openSession();
//                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                        OperatorsManagement oManagementObj = new OperatorsManagement();
//                        Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
//                        if (aOperator != null) {
//                            String[] emailList = new String[aOperator.length - 1];
//                            for (int i = 1; i < aOperator.length; i++) {
//                                emailList[i - 1] = aOperator[i].getEmailid();
//                            }
//                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                String strsubject = templatesObj.getSubject();
//                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                if (strmessageBody != null) {
//                                    // Date date = new Date();
//                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                }
//
//                                SendNotification send = new SendNotification();
//                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                                log.debug("##SendEmail" + axStatus.strStatus);
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
//                            aStatus.error = "INVALID IP";
//                            log.error(aStatus.error);
//                            aStatus.errorcode = -108;
//                            return aStatus;
//                        }
//
//                        suTemplate.close();
//                        sTemplate.close();
//                        aStatus.error = "INVALID IP";
//                        log.error(aStatus.error);
//                        aStatus.errorcode = -108;
//                        return aStatus;
//                    }
//                    aStatus.error = "INVALID IP";
//                    log.error(aStatus.error);
//                    aStatus.errorcode = -108;
//                    return aStatus;
//                }
//            }
//            MobileTrustManagment mManagment = new MobileTrustManagment();
//            int check = -9;
//            JSONObject jsonObj = null;
//            int devicetype = 1;
//            try {
//                jsonObj = new JSONObject(sotpPlus);
//
//                String device = jsonObj.getString("_deviceType");
//                log.debug("DeviceType" + device);
//                if (device.equals("ANDROID")) {
//                    devicetype = ANDROID;
//                } else if (device.equals("IOS")) {
//                    devicetype = IOS;
//                }
//            } catch (Exception ex) {
//                log.error(String.format("##VerifySignatureOTPPlus :  - exception caught when calling VerifySignatureOTPPlus - ", ex.getMessage()));
//                ex.printStackTrace();
//            }
//
//            if (sotpPlus != null && !sotp.contains(":")) {
//                check = mManagment.CheckPlusComponent(channel, userid, sessionid, sotp, sotpPlus, devicetype);
//                System.out.println("CheckPlusComponent returned::" + check);
//                //added for error handling
//                if (retValue == -6) {
//                    aStatus.error = "Trusted device not found";
//                    log.error(String.format("##VerifySignatureOTPPlus :  - exception caught when calling VerifySignatureOTPPlus - ", aStatus.error));
//                    aStatus.errorcode = -6;
//                } else if (retValue == -4) {
//                    aStatus.error = "OTP is invalid";
//                    log.error(String.format("##VerifySignatureOTPPlus :  - exception caught when calling VerifySignatureOTPPlus - ", aStatus.error));
//                    aStatus.errorcode = -4;
//                } else if (retValue == -5) {
//                    aStatus.error = "Trusted Device mismatch";
//                    log.error(String.format("##VerifySignatureOTPPlus :  - exception caught when calling VerifySignatureOTPPlus - ", aStatus.error));
//                    aStatus.errorcode = -5;
//
//                } else if (retValue == -7) {
//                    aStatus.error = "Invalid Location, out of home country!!!";
//                    log.error(String.format("##VerifySignatureOTPPlus :  - exception caught when calling VerifySignatureOTPPlus - ", aStatus.error));
//                    aStatus.errorcode = -7;
//                } else if (retValue == -8) {
//                    aStatus.error = "Mobile Trust Setting is not configured!!!";
//                    aStatus.errorcode = -118;
//                } else if (retValue == -9) {
//                    aStatus.error = "Timestamp is null";
//                    log.error(String.format("##VerifySignatureOTPPlus :  - exception caught when calling VerifySignatureOTPPlus - ", aStatus.error));
//                    aStatus.errorcode = -9;
//                } else if (retValue == -10) {
//                    aStatus.error = "TimeStamp mistmatch!!!";
//                    log.error(String.format("##VerifySignatureOTPPlus :  - exception caught when calling VerifySignatureOTPPlus - ", aStatus.error));
//                    aStatus.errorcode = -10;
//                } else if (retValue == -11) {
//                    aStatus.error = "TimeStamp has expired!!!";
//                    log.error(String.format("##VerifySignatureOTPPlus :  - exception caught when calling VerifySignatureOTPPlus - ", aStatus.error));
//                    aStatus.errorcode = -11;
//                } else if (retValue == -12) {
//                    aStatus.error = "Invalid Location, out of home country!!!";
//                    log.error(String.format("##VerifySignatureOTPPlus :  - exception caught when calling VerifySignatureOTPPlus - ", aStatus.error));
//                    aStatus.errorcode = -12;
//                } else if (retValue == -13) {
//                    aStatus.error = "Invalid Location, roaming country is not allowed!!!";
//                    log.error(String.format("##VerifySignatureOTPPlus :  - exception caught when calling VerifySignatureOTPPlus - ", aStatus.error));
//                    aStatus.errorcode = -13;
//                } else if (retValue == -14) {
//                    aStatus.error = "Roaming duration is over";
//                    log.error(String.format("##VerifySignatureOTPPlus :  - exception caught when calling VerifySignatureOTPPlus - ", aStatus.error));
//                    aStatus.errorcode = -15;
//                } else if (retValue == -15) {
//                    //aStatus.error = "TimeStamp Updated failed";
//                    aStatus.error = "SUCCESS";
//                    aStatus.errorcode = 0;
//                } else if (retValue == -2) {
//                    aStatus.error = "Session has expired";
//                    log.error(String.format("##VerifySignatureOTPPlus :  - exception caught when calling VerifySignatureOTPPlus - ", aStatus.error));
//                    aStatus.errorcode = -2;
//                } else if (retValue == -1) {
//                    aStatus.error = "General Exception";
//                    log.error(String.format("##VerifySignatureOTPPlus :  - exception caught when calling VerifySignatureOTPPlus - ", aStatus.error));
//                    aStatus.errorcode = -16;
//                }
//                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                        session.getLoginid(), session.getLoginid(), new Date(), "Mobile Trust PLUS Check", aStatus.error, aStatus.errorcode,
//                        "Token Management", aStatus.error, " ",
//                        itemtype, userid);
//
//                //return aStatus;
//                //end of addition
//            } else if (sotp.contains(":")) {
//                check = 0;
//            }
//            if (check == 0) {
//                if (sotp.contains(":")) {
//                    UtilityFunctions utility = new UtilityFunctions();
//                    String oldLatiLong = sotp.substring(0, sotp.indexOf(":"));
//                    String[] latilong = utility.DecodeGEOCode(oldLatiLong);
//                    String newOtp = sotp.substring(sotp.indexOf(":") + 1, sotp.length());
//                    Location srvLoc = mManagment.getLocationByLongAndLat(latilong[1], latilong[0]);
//                    SessionFactoryUtil geouSession = new SessionFactoryUtil(SessionFactoryUtil.geotrack);
//                    Session geoSession = geouSession.openSession();
//                    GeoTrackingUtils geoUtil = new GeoTrackingUtils(geouSession, geoSession);
//                    SessionFactoryUtil guSession = new SessionFactoryUtil(SessionFactoryUtil.geofence);
//                    Session gSession = guSession.openSession();
//                    GeoFenceUtils gUtil = new GeoFenceUtils(guSession, gSession);
//                    try {
//                        //Location srvLocip = getLocationByIpAddress(ip);
//                        int val = geoUtil.addGeotrack(channel.getChannelid(), userid, latilong[1], latilong[0], req.getRemoteAddr(), srvLoc.city, srvLoc.state, srvLoc.country, srvLoc.zipcode, LOGIN, new Date());
//                        Geofence gFence = gUtil.getGeofence(userid, channel.getChannelid());
//                        log.debug("##VerifySignatureOTPPlus:addGeotrack():Result" + val);
//
//                        log.debug("##VerifySignatureOTPPlus:getGeofence()");
//                        if (gFence == null) {
//                            aStatus.errorcode = -7;
//
//                        }
//                        boolean b3434 = false;
//                        String locationUser = null;
//                        if (gFence != null) {
//                            //b3434 = gFence.getRoamingAllowed();
//                            b3434 = (gFence.getRoamingAllowed() != 0);
//                            locationUser = gFence.getHomeCountry();
//                            log.debug("##VerifySignatureOTPPlus:locationUser()+HomeCountry" + locationUser);
//                        }
//
//                        if (b3434 == false) {
//                            if (srvLoc.country.equalsIgnoreCase(locationUser)) {
//
//                                retValue = 0;
//                            } else {
//                                retValue = -12;
//                            }
//                        } else {
//                            String[] foreignCountryList = null;
//                            if (gFence.getForeignCountry() != null) {
//                                foreignCountryList = gFence.getForeignCountry().split(",");
//                            }
//                            if (foreignCountryList != null && foreignCountryList.length > 0) {
//                                for (int i = 0; i < foreignCountryList.length; i++) {
//                                    if (srvLoc.country.equalsIgnoreCase(foreignCountryList[i])) {
//
//                                        long nowDate = new Date().getTime();
//                                        //System.out.println("Current Time::"+nowDate);
//                                        //System.out.println("Start Time::"+gFence.getStartDateForRoming().getTime() );
//                                        //System.out.println("End Time::"+gFence.getEndDateForRoming().getTime() );
//
//                                        Long startDiff = nowDate - gFence.getStartDateForRoming().getTime();
//                                        Long endDiff = gFence.getEndDateForRoming().getTime() - nowDate;
//                                        if (startDiff < 0 || endDiff < 0) {
//                                            retValue = -11;
//                                        }
//                                        retValue = 0;
//                                    }
//                                }
//                            } else {
//                                retValue = -13;
//                            }
////                            if (gFence.getForeignCountry().equalsIgnoreCase(srvLoc.country)) {
////                                Long startDiff = new Date().getTime() - gFence.getStartDateForRoming().getTime();
////                                Long endDiff = gFence.getEndDateForRoming().getTime() - new Date().getTime();
////                                if (startDiff < 0 || endDiff < 0) {
////                                    retValue = -11;
////                                }
////                                retValue = 0;
////                            } else {
////                                retValue = -12;
////                            }
//                        }
//                    } finally {
//                        geoSession.close();
//                        geouSession.close();
//                        gSession.close();
//                        guSession.close();
//                    }
//                    if (retValue == 0) {
//                        retValue = oManagement.VerifySignatureOTP(session.getChannelid(), userid, sessionid, data, newOtp);
//                        log.debug("##VerifySignatureOTPPlus:VerifySignatureOTP() return value" + retValue);
//                    }
//                } else {
//                    retValue = oManagement.VerifySignatureOTP(session.getChannelid(), userid, sessionid, data, sotp);
//                    log.debug("##VerifySignatureOTPPlus:VerifySignatureOTP() return value" + retValue);
//                }
//            }
//
//            if (retValue == 0) {
//                aStatus.error = "SUCCESS";
//                aStatus.errorcode = 0;
//            } else if (retValue == -22) {
//                aStatus.error = "Signature OTP Token is locked";
//                log.error(String.format("##VerifySignatureOTPPlus :  - exception caught when calling VerifySignatureOTPPlus - ", aStatus.error));
//                aStatus.errorcode = -22;
//            } else if (retValue == -8) {
//                aStatus.error = "Signature OTP is already consumed";
//                log.error(String.format("##VerifySignatureOTPPlus :  - exception caught when calling VerifySignatureOTPPlus - ", aStatus.error));
//                aStatus.errorcode = -8;
//            } else if (retValue == -17) {
//                aStatus.error = "User/Token is not found";
//                log.error(String.format("##VerifySignatureOTPPlus :  - exception caught when calling VerifySignatureOTPPlus - ", aStatus.error));
//                aStatus.errorcode = -17;
//            } else if (retValue == -9) {
//                aStatus.error = "Signature OTP is expired";
//                log.error(String.format("##VerifySignatureOTPPlus :  - exception caught when calling VerifySignatureOTPPlus - ", aStatus.error));
//                aStatus.errorcode = -9;
//            } else if (retValue == -2) {
//                aStatus.error = "Session has expired";
//                log.error(String.format("##VerifySignatureOTPPlus :  - exception caught when calling VerifySignatureOTPPlus - ", aStatus.error));
//                aStatus.errorcode = -2;
//            } else if (retValue == -1) {
//                aStatus.error = "General Error";
//                log.error(String.format("##VerifySignatureOTPPlus :  - exception caught when calling VerifySignatureOTPPlus - ", aStatus.error));
//                aStatus.errorcode = -1;
//            }
//
//        }
//        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                session.getLoginid(), session.getLoginid(), new Date(), "Verify SOTP ", aStatus.error, aStatus.errorcode,
//                "Token Management", "SOTP = ******", " SOTP = ******",
//                itemtype, userid);
//        return aStatus;
//    }
//
//    @Override
//    public AxiomStatus VerifyDigitalSignaturePlus(String sessionid, String userid, String data, int type, String signature, String signaturePlus) {
//
//        String strDebug = null;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                System.out.println("VerifyDigitalSignaturePlus::sessionid::" + sessionid);
//                System.out.println("VerifyDigitalSignaturePlus::userid::" + userid);
//                System.out.println("VerifyDigitalSignaturePlus::data::" + data);
//                System.out.println("VerifyDigitalSignaturePlus::type::" + type);
//                System.out.println("VerifyDigitalSignaturePlus::signature::" + signature);
//                System.out.println("VerifyDigitalSignaturePlus::signaturePlus::" + signaturePlus);
//            }
//        } catch (Exception ex) {
//        }
//
//        int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "Licence Invalid";
//            aStatus.errorcode = -100;
//            return aStatus;
//        }
//
//        //OTPTokenManagement oManagement = new OTPTokenManagement();
//        CryptoManager crypto = new CryptoManager();
//        SessionManagement sManagement = new SessionManagement();
//        AuditManagement audit = new AuditManagement();
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        Sessions session = sManagement.getSessionById(sessionid);
//        AxiomStatus aStatus = new AxiomStatus();
//        int retValue = -1;
//        aStatus.error = "ERROR";
//        aStatus.errorcode = retValue;
//        if (session == null) {
//            aStatus.error = "Invalid Session";
//            aStatus.errorcode = -14;
//            return aStatus;
//        }
//        ChannelManagement cManagement = new ChannelManagement();
//        Channels channel = cManagement.getChannelByID(session.getChannelid());
//        if (channel == null) {
//            aStatus.error = "Invalid Channel";
//            aStatus.errorcode = -3;
//            return aStatus;
//        }
//        if (session != null) {
//
//            //System.out.println("Client IP = " + req.getRemoteAddr());        
//            SettingsManagement setManagement = new SettingsManagement();
////            int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
////            if (result != 1) {
////                aStatus.error = "INVALID IP REQUEST";
////                aStatus.errorcode = -8;
////                return aStatus;
////            }
//
//            String channelid = session.getChannelid();
//
//            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//            if (ipobj != null) {
//                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                int checkIp = 1;
//                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                } else {
//                    checkIp = 1;
//                }
//                if (iObj.ipstatus == 0 && checkIp != 1) {
//                    if (iObj.ipalertstatus == 0) {
//                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                        Session sTemplate = suTemplate.openSession();
//                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                        OperatorsManagement oManagement = new OperatorsManagement();
//                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                        if (aOperator != null) {
//                            String[] emailList = new String[aOperator.length - 1];
//                            for (int i = 1; i < aOperator.length; i++) {
//                                emailList[i - 1] = aOperator[i].getEmailid();
//                            }
//                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                String strsubject = templatesObj.getSubject();
//                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                if (strmessageBody != null) {
//                                    // Date date = new Date();
//                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                }
//
//                                SendNotification send = new SendNotification();
//                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
//                            aStatus.error = "INVALID IP";
//                            aStatus.errorcode = -8;
//                            return aStatus;
//                        }
//
//                        suTemplate.close();
//                        sTemplate.close();
//                        aStatus.error = "INVALID IP";
//                        aStatus.errorcode = -8;
//                        return aStatus;
//                    }
//                    aStatus.error = "INVALID IP";
//                    aStatus.errorcode = -8;
//                    return aStatus;
//                }
//            }
//            int devicetype = 1;
//            JSONObject jsonObj = null;
//            try {
//                jsonObj = new JSONObject(signaturePlus);
//                String device = jsonObj.getString("_deviceType");
//                if (device.equals("ANDROID")) {
//                    devicetype = ANDROID;
//                } else if (device.equals("IOS")) {
//                    devicetype = IOS;
//                }
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//            MobileTrustManagment mManagment = new MobileTrustManagment();
//            retValue = mManagment.CheckPlusComponentForSign(channel, userid, sessionid, data, signature, signaturePlus, devicetype);
//
//            if (retValue == 0) {
//                aStatus.error = "SUCCESS";
//                aStatus.errorcode = 0;
//            } else {
//                aStatus.errorcode = retValue;
//                if (aStatus.errorcode == -14) {
//                    aStatus.error = "Mobile Trust Setting is not configured!!!";
//                } else if (aStatus.errorcode == -6) {
//                    aStatus.error = "Trusted Device not found!!!";
//                } else if (aStatus.errorcode == -17) {
//                    aStatus.error = "Trusted Device is marked suspended";
//                } else if (aStatus.errorcode == -5) {
//                    aStatus.error = "Trust Device mismatch!!!";
//                } else if (aStatus.errorcode == -4) {
//                    aStatus.error = "Verification failed!!!";
//                } else if (aStatus.errorcode == -42) {
//                    aStatus.error = "Digtial Signature mismatch!!!";
//                } else if (aStatus.errorcode == -7) {
//                    aStatus.error = "Home Country is not defined!!!";
//                } else if (aStatus.errorcode == -28) {
//                    aStatus.error = "TimeStamp is not issued!!";
//                } else if (aStatus.errorcode == -27) {
//                    aStatus.error = "TimeStamp mistmatch!!!";
//                } else if (aStatus.errorcode == -29) {
//                    aStatus.error = "TimeStamp has expired!!!";
//                } else if (aStatus.errorcode == -10) {
//                    aStatus.error = "Invalid Location, out of home country!!!";
//                } else if (aStatus.errorcode == -11) {
//                    aStatus.error = "Invalid Location, roaming country is not allowed!!!";
//                } else if (aStatus.errorcode == -12) {
//                    aStatus.error = "Invalid Location, roaming is not configured properly!!!";
//                }
//            }
//
//        }
//        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                session.getLoginid(), session.getLoginid(), new Date(), "Verify Digital Signature ", aStatus.error, aStatus.errorcode,
//                "Verify Digital Signature", "Signature = ******", " Signature = ******",
//                itemtype, userid);
//
//        return aStatus;
//    }
//
//    @Override
//    public AxiomStatus ChangeUserDetailsPlus(String sessionid, AxiomUser aUser) {
//
//        String strDebug = null;
//        String userid = aUser.userId;
//        String fullname = aUser.userName;
//        String phone = aUser.phoneNo;
//        String email = aUser.email;
//        String organisation = aUser.organisation;
//        String organisationUnit = aUser.organisationUnit;
//        String country = aUser.country;
//        String location = aUser.location;
//        String street = aUser.street;
//        String userIdentity = aUser.userIdentity;
//        String idType = aUser.idType;
//        String designation = aUser.designation;
//
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                System.out.println("ChangeUserDetails::sessionId::" + sessionid);
//                System.out.println("ChangeUserDetails::userid::" + userid);
//                System.out.println("ChangeUserDetails::fullname::" + fullname);
//                System.out.println("ChangeUserDetails::phone::" + phone);
//                System.out.println("ChangeUserDetails::Country::" + country);
//                System.out.println("ChangeUserDetails::location::" + location);
//                System.out.println("ChangeUserDetails::street::" + street);
//                System.out.println("ChangeUserDetails::idType::" + idType);
//                System.out.println("ChangeUserDetails::userIdentity::" + userIdentity);
//                System.out.println("ChangeUserDetails::organisation::" + organisation);
//                System.out.println("ChangeUserDetails::organisationUnit::" + organisationUnit);
//            }
//        } catch (Exception ex) {
//        }
//
////        int iResult = 0;
////        if (iResult != 0) {
////            AxiomStatus aStatus = new AxiomStatus();
////            aStatus.error = "Licence Invalid";
////            aStatus.errorcode = -100;
////            return aStatus;
////        }
//        SessionManagement sManagement = new SessionManagement();
//        AuditManagement audit = new AuditManagement();
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        Sessions session = sManagement.getSessionById(sessionid);
//        if (session == null) {
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "Invalid Session";
//            aStatus.errorcode = -14;
//            return aStatus;
//            //throw  new AxiomException("");
//        }
//
//        try {
//            UserManagement uManagement = new UserManagement();
//            AxiomStatus aStatus = new AxiomStatus();
//            int retValue = -1;
//            aStatus.error = "ERROR";
//            aStatus.errorcode = retValue;
//            AuthUser oldUser = null;
//            if (session != null) {
//
//                //System.out.println("Client IP = " + req.getRemoteAddr());        
//                SettingsManagement setManagement = new SettingsManagement();
//                String channelid = session.getChannelid();
//                ChannelManagement cManagement = new ChannelManagement();
//                Channels channel = cManagement.getChannelByID(channelid);
//                if (channel == null) {
//                    aStatus.error = "Invalid Channel";
//                    aStatus.errorcode = -3;
//                    return aStatus;
//                }
//                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//                if (ipobj != null) {
//                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                    int checkIp = 1;
//                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                    } else {
//                        checkIp = 1;
//                    }
//                    if (iObj.ipstatus == 0 && checkIp != 1) {
//                        if (iObj.ipalertstatus == 0) {
//                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                            Session sTemplate = suTemplate.openSession();
//                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                            OperatorsManagement oManagement = new OperatorsManagement();
//                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                            if (aOperator != null) {
//                                String[] emailList = new String[aOperator.length - 1];
//                                for (int i = 1; i < aOperator.length; i++) {
//                                    emailList[i - 1] = aOperator[i].getEmailid();
//                                }
//                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                    String strsubject = templatesObj.getSubject();
//                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                    if (strmessageBody != null) {
//                                        // Date date = new Date();
//                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                    }
//
//                                    SendNotification send = new SendNotification();
//                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                                }
//
//                                suTemplate.close();
//                                sTemplate.close();
//                                aStatus.error = "INVALID IP";
//                                aStatus.errorcode = -8;
//                                return aStatus;
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
//                            aStatus.error = "INVALID IP";
//                            aStatus.errorcode = -8;
//                            return aStatus;
//                        }
//                        aStatus.error = "INVALID IP";
//                        aStatus.errorcode = -8;
//                        return aStatus;
//                    }
//                }
//
//                retValue = uManagement.EditUser(sessionid, session.getChannelid(), userid, fullname, phone, email, 0, organisation, userIdentity, idType, organisationUnit, country, location, street, designation);
//                if (retValue == 0) {
//                    aStatus.error = "SUCCESS";
//                    aStatus.errorcode = 0;
//                } else if (retValue == 1) {
//                    aStatus.error = "User Details Changed Succesfully but email could not be sent!!!";
//                    aStatus.errorcode = 0;
//                } else if (retValue == -5 || retValue == -1) {
//                    aStatus.error = "General Error (" + retValue + ")";
//                    aStatus.errorcode = -5;
//                } else if (retValue == -244) {
//                    aStatus.error = "Username is Duplicate!!!";
//                    aStatus.errorcode = retValue;
//                } else if (retValue == -241) {
//                    aStatus.error = "Userid is Duplicate!!!";
//                    aStatus.errorcode = retValue;
//                } else if (retValue == -242) {
//                    aStatus.error = "Phone Number is Duplicate!!!";
//                    aStatus.errorcode = retValue;
//                } else if (retValue == -240) {
//                    aStatus.error = "Email Id is Duplicate!!!";
//                    aStatus.errorcode = retValue;
//                } else if (retValue == -2) {
//                    aStatus.error = "User Not found!!!";
//                    aStatus.errorcode = retValue;
//                }
//
//                //return aStatus;
//                oldUser = uManagement.getUser(sessionid, session.getChannelid(), userid);
//            }
//
//            String oldUserName = "";
//            String oldPhoneNo = "";
//            String oldEmailID = "";
//            String oldPasswordState = "";
//
//            if (oldUser != null) {
//                oldUserName = oldUser.getUserName();
//                oldPhoneNo = oldUser.getPhoneNo();
//                oldEmailID = oldUser.getEmail();
//                oldPasswordState = "" + oldUser.getStatePassword();
//            }
//
//            String itemtype = "USERPASSWORD";
//
//            ChannelManagement cManagement = new ChannelManagement();
//            Channels channel = cManagement.getChannelByID(session.getChannelid());
//            //AuditManagement audit = new AuditManagement();
//            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                    session.getLoginid(), session.getLoginid(), new Date(), "Edit User", aStatus.error, aStatus.errorcode,
//                    "User Management", "User Name = " + oldUserName + "User Phone =" + oldPhoneNo + "User Email =" + oldEmailID + "State Of Password =" + oldPasswordState,
//                    "User Name = " + fullname + "User Phone =" + phone + "User Email =" + email + "State Of Password =" + -1,
//                    itemtype, "");
//
//            return aStatus;
//        } catch (Exception e) {
//            e.printStackTrace();
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "General Exception::" + e.getMessage();
//            aStatus.errorcode = -99;
//            return aStatus;
//        }
//
//    }
//
//    @Override
//    public AxiomStatus ChangeUserCredentials(String sessionid, AxiomUserCerdentials credentials, String integrityCheckString, String trustPayLoad) {
//
//        String strDebug = null;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                log.info("ChangeTokenType::Started");
//                log.info("ChangeTokenType::sessionid::" + sessionid);
//                log.info("ChangeTokenType::userid::" + credentials.axiomUser.userId);
//                for (int i = 0; i < credentials.tokenDetails.length; i++) {
//                    log.info("Categorey" + credentials.tokenDetails[i].category);
//                    log.info("SubCategory" + credentials.tokenDetails[i].subcategory);
//                }
//            }
//        } catch (Exception ex) {
//            log.debug(ex.getMessage());
//        }
////nilesh
////        int iResult = AxiomProtect.ValidateLicense();
////        if (iResult != 0) {
////            AxiomStatus aStatus = new AxiomStatus();
////            aStatus.error = "Licence is invalid";
////            aStatus.errorcode = -100;
////            return aStatus;
////        }
//        AxiomStatus aStatus = new AxiomStatus();
//
//        byte[] SHA1hash = null;
//        if (trustPayLoad == null) {
//            SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.axiomUser.email + credentials.axiomUser.userId + credentials.axiomUser.phoneNo + credentials.axiomUser.userName);
//
//        } else {
//            SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.axiomUser.email + credentials.axiomUser.userId + credentials.axiomUser.phoneNo + credentials.axiomUser.userName + trustPayLoad);
//        }
//        String integritycheck = new String(Base64.encode(SHA1hash));
//
//        if (integrityCheckString != null) {
//            if (integrityCheckString.equals(integritycheck) != true) {
//                aStatus.error = "Input Data is Tampered!!!";
//                log.error(String.format("##ChangeUserCredentials :  - exception caught when calling ChangeUserCredentials - ", aStatus.error));
//                aStatus.errorcode = -76;
//                return aStatus;
//
//            }
//        }
//
//        SessionManagement sManagement = new SessionManagement();
//        AuditManagement audit = new AuditManagement();
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        Sessions session = sManagement.getSessionById(sessionid);
//        if (session == null) {
//
//            aStatus.error = "Invalid Session";
//            log.error(String.format("##ChangeUserCredentials :  - exception caught when calling ChangeUserCredentials - ", aStatus.error));
//            aStatus.errorcode = -14;
//            return aStatus;
//
//        }
//
////        SessionManagement sManagement = new SessionManagement();
////        Sessions session = sManagement.getSessionById(sessionid);
////        MessageContext mc = wsContext.getMessageContext();
////        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        aStatus.error = "ERROR";
//
//        aStatus.errorcode = -2;
//        String oldValue = "";
//        String strValue = "";
//        String UserId = credentials.axiomUser.userId;
//        int retValue = -1;
////        if (session == null) {
////            return aStatus;
////        }
//        if (session != null) {
//
//            //System.out.println("Client IP = " + req.getRemoteAddr());        
//            SettingsManagement setManagement = new SettingsManagement();
////            int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
////            if (result != 1) {
////                aStatus.error = "INVALID IP REQUEST";
////                aStatus.errorcode = -8;
////                //  return aStatus;
////            }
//
//            String channelid = session.getChannelid();
//
//            ChannelManagement cManagement = new ChannelManagement();
//            Channels channel = cManagement.getChannelByID(channelid);
//            if (channel == null) {
//                log.error(String.format("# channel is null"));
//                return null;
//            }
//
//            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
//            if (channelProfileObj != null) {
//                int retVal = sManagement.getServerStatus(channelProfileObj);
//                if (retVal != 0) {
//
//                    aStatus = new AxiomStatus();
//                    aStatus.error = "Server Down for maintainance,Please try later!!!";
//                    log.error(String.format("##ChangeUserCredentials :  - exception caught when calling ChangeUserCredentials - ", aStatus.error));
//                    aStatus.errorcode = -48;
//                    return aStatus;
//                }
//            }
//
//            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//            if (ipobj != null) {
//                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                int checkIp = 1;
//                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                    log.debug("CheckIp" + checkIp);
//                } else {
//                    checkIp = 1;
//                }
//                if (iObj.ipstatus == 0 && checkIp != 1) {
//                    if (iObj.ipalertstatus == 0) {
//                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                        Session sTemplate = suTemplate.openSession();
//                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                        OperatorsManagement oManagement = new OperatorsManagement();
//                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                        if (aOperator != null) {
//                            String[] emailList = new String[aOperator.length - 1];
//                            for (int i = 1; i < aOperator.length; i++) {
//                                emailList[i - 1] = aOperator[i].getEmailid();
//                            }
//                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                String strsubject = templatesObj.getSubject();
//                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                if (strmessageBody != null) {
//                                    // Date date = new Date();
//                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                }
//
//                                SendNotification send = new SendNotification();
//                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                                log.debug("#ChangeUserCredentials:SendEmail:Status" + axStatus.strStatus);
//
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
//                            aStatus.error = "INVALID IP REQUEST";
//                            log.error(String.format("##ChangeUserCredentials :  - exception caught when calling ChangeUserCredentials - ", aStatus.error));
//                            aStatus.errorcode = -8;
//                            return aStatus;
//                        }
//
//                        suTemplate.close();
//                        sTemplate.close();
//                        aStatus.error = "INVALID IP REQUEST";
//                        log.error(String.format("##ChangeUserCredentials :  - exception caught when calling ChangeUserCredentials - ", aStatus.error));
//                        aStatus.errorcode = -8;
//                        return aStatus;
//                    }
//                    aStatus.error = "INVALID IP REQUEST";
//                    log.error(String.format("##ChangeUserCredentials :  - exception caught when calling ChangeUserCredentials - ", aStatus.error));
//                    aStatus.errorcode = -8;
//                    return aStatus;
//                }
//            }
//            boolean bSentToUser = true;
//            int subcategory = 1;
//            if (trustPayLoad != null) {
//                MobileTrustSettings mSettings = null;
//
//                Object obj = setManagement.getSetting(sessionid, channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
//                if (obj != null) {
//                    if (obj instanceof MobileTrustSettings) {
//                        mSettings = (MobileTrustSettings) obj;
//                    }
//                }
//
//                if (mSettings == null) {
//                    aStatus.error = "Mobile Trust Setting is not configured!!!";
//                    log.error(String.format("##ChangeUserCredentials :  - exception caught when calling ChangeUserCredentials - ", aStatus.error));
//                    aStatus.errorcode = -42;
////                  
//                    return aStatus;
//                }
//
//                if (mSettings.bGeoFencing == true) {
//
////                    TrustManagement tManagement = new TrustManagement();
////                    int res = tManagement.checkLocationAndUpdateWMI(sessionid, channelid, credentials.axiomUser.userId, trustPayLoad);
////                    if (res != 0) {
////                        if (res == -14) {
////                            aStatus.errorcode = -14;
////                            aStatus.error = "Mobile Trust Setting is not configured!!!";
////
////                        } else if (res == -4) {
////                            aStatus.errorcode = 4;
////                            aStatus.error = "Verification failed!!!";
////                        } else if (res == -7) {
////                            aStatus.errorcode = -7;
////                            aStatus.error = "Home Country is not defined!!!";
////                        } else if (res == -10) {
////                            aStatus.errorcode = -10;
////                            aStatus.error = "Invalid Location, out of home country!!!";
////                        } else if (res == -11) {
////                            aStatus.errorcode = -11;
////                            aStatus.error = "Invalid Location, roaming country is not allowed!!!";
////                        } else if (res == -12) {
////                            aStatus.errorcode = -12;
////                            aStatus.error = "Invalid Location, roaming is not configured properly!!!";
////                        } else if (res == -13) {
////                            aStatus.errorcode = -13;
////                            aStatus.error = "Roaming Contry is not Configured!!!";
////                        } else if (res == -1) {
////                            aStatus.errorcode = -1;
////                            aStatus.error = "Internal Error!!!";
////                        } else if (res == -2) {
////                            aStatus.errorcode = -2;
////                            aStatus.error = "Session has Expired!!!";
////                        }
//                    GeoLocationManagement gManagement = new GeoLocationManagement();
//                    AXIOMStatus status = gManagement.validateGeoLocation(sessionid, channelid, credentials.axiomUser.userId, trustPayLoad);
//                    if (status == null) {
//                        aStatus.error = "Location Not Found!!!";
//                        log.error(String.format("##ChangeUserCredentials :  - exception caught when calling ChangeUserCredentials - ", aStatus.error));
//                        aStatus.errorcode = -115;
//                    } else if (status.iStatus != 0) {
//
//                        aStatus.error = status.strStatus;
//                        aStatus.errorcode = status.iStatus;
//                        //     String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
//                        audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
//                                session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorcode,
//                                "Token Management", "Invalid Location", null, itemtype,
//                                session.getLoginid());
//
//                        return aStatus;
//                    }
//                }
//            }
//            for (int i = 0; i < credentials.tokenDetails.length; i++) {
//                if (credentials.tokenDetails[i] != null) {
//                    if (credentials.tokenDetails[i].category == AxiomCredentialDetails.PASSWORD) {
////                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.REMOTESIGNSERVICEWITH_OTP) != 0) {
////                            aStatus = new AxiomStatus();
////                            aStatus.error = "This feature is not available in this license!!!";
////                            aStatus.errorcode = -101;
////                            return aStatus;
////                        }
//                        UserManagement um = new UserManagement();
//                        retValue = um.AssignPassword(sessionid, session.getChannelid(), UserId, credentials.tokenDetails[i].Password);
//                        log.debug("##ChangeUserCredentials:AssignPassword:retvalue" + retValue);
//                        if (retValue == 0) {
//                            if (bSentToUser == true) {
//                                SendNotification send = new SendNotification();
//                                AuthUser aUser = um.getUser(sessionid, session.getChannelid(), UserId);
//                                Templates temp = null;
//                                TemplateManagement tObj = new TemplateManagement();
//                                if (subcategory == 1) // sms
//                                {
//                                    temp = tObj.LoadbyName(sessionid, session.getChannelid(), TemplateNames.MOBILE_USER_PASSWORD_TEMPLATE);
//                                }
//                                if (temp == null) {
//                                    aStatus.error = "Message Template is missing";
//                                    log.error(String.format("##ChangeUserCredentials :  - exception caught when calling ChangeUserCredentials - ", aStatus.error));
//                                    aStatus.errorcode = -2;
//                                    //return aStatus;
//                                }
//
//                                ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
//                                String templatebody = (String) TemplateUtils.deserializeFromObject(bais);
//
//                                templatebody = templatebody.replaceAll("#name#", aUser.userName);
//                                String date = String.valueOf(new Date());
//                                templatebody = templatebody.replaceAll("#channel#", channel.getName());
//                                templatebody = templatebody.replaceAll("#password#", credentials.tokenDetails[i].Password);
//                                templatebody = templatebody.replaceAll("#datetime#", date);
//
//                                com.mollatech.axiom.connector.communication.AXIOMStatus axiomStatus = null;
//
//                                int iProductType = Integer.valueOf(LoadSettings.g_sSettings.getProperty("product.type")).intValue();
//
//                                if (temp.getStatus() == tObj.ACTIVE_STATUS) {
//                                    axiomStatus = send.SendOnMobile(session.getChannelid(), aUser.phoneNo, templatebody, subcategory, iProductType);
//                                }
//                                if (axiomStatus != null) {
//                                    aStatus.error = axiomStatus.strStatus;
//                                    log.error(String.format("##ChangeUserCredentials :  - exception caught when calling ChangeUserCredentials - ", aStatus.error));
//                                    aStatus.errorcode = axiomStatus.iStatus;
//                                }
//                            } else {
//                                aStatus.error = "SUCCESS";
//                                aStatus.errorcode = 0;
//                            }
//
//                        }
//
//                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.CERTIFICATE) {
//
//                        //CertificateManagement cerMgmt = new CertificateManagement();
//                        CertificateManagement cerMgmt = new CertificateManagement();
//                        int res = -1;
//                        if (credentials.tokenDetails[i].status == CertificateManagement.CERT_STATUS_REVOKED) {
//                            res = cerMgmt.RevokeCertificate(sessionid, channelid, UserId, "");
//                            log.debug("##RevokeCertificate:res" + res);
//                        }
//
//                        if (res == 0) {
//                            res = cerMgmt.DeleteCert(sessionid, channelid, UserId);
//                            log.debug("##DeleteCert:res" + res);
//                        }
//                        String resultString = null;
//                        if (res == 0) {
//                            aStatus.error = "SUCCESS";
//                            aStatus.errorcode = 0;
//                            resultString = "Success";
//                            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                                    req.getRemoteAddr(),
//                                    //                    /ipaddress, 
//                                    channel.getName(),
//                                    session.getLoginid(), session.getLoginid(), new Date(), "Delete Certificate", resultString, retValue,
//                                    "Certificate Management", "", " ",
//                                    "PKITOKEN", UserId);
//                        } else {
//                            aStatus.error = "error";
//                            aStatus.errorcode = retValue;
//                            resultString = "Failure";
//                            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                                    req.getRemoteAddr(),
//                                    //ipaddress, 
//                                    channel.getName(),
//                                    session.getLoginid(), session.getLoginid(), new Date(), "Delete Certificate", resultString, retValue,
//                                    "Certificate Management", "", " ",
//                                    "PKITOKEN", UserId);
//
//                        }
//
//                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.CHALLAEGE_RESPONSE) {
//                        // AxiomQuestionsAndAnswers axiomQandA = new AxiomQuestionsAndAnswers();
////                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.REMOTESIGNSERVICEWITH_CHALLENGERESPONSE) != 0) {
////                            aStatus = new AxiomStatus();
////                            aStatus.error = "This feature is not available in this license!!!";
////                            aStatus.errorcode = -101;
////                            return aStatus;
////                        }
//                        AxiomQuestionsAndAnswers axiomQAndA = new AxiomQuestionsAndAnswers();
//                        axiomQAndA.webQAndA = credentials.tokenDetails[i].qas.webQAndA;
//                        ChallengeResponsemanagement chManagment = new ChallengeResponsemanagement();
//                        retValue = chManagment.UpdateRegisterUser(sessionid, session.getChannelid(), UserId, axiomQAndA);
//                        log.debug("##UpdateRegisterUser:retValue" + retValue);
//                        //  retValue = uManagement.CreateUser(sessionId, session.getChannelid(), fullname, phone, email);
//
//                        if (retValue == 0) {
//                            aStatus.error = "SUCCESS";
//                            aStatus.errorcode = 0;
//                        }
//
//                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.SOFTWARE_TOKEN) {
////                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
////                            aStatus = new AxiomStatus();
////                            aStatus.error = "This feature is not available in this license!!!";
////                            aStatus.errorcode = -101;
////                            return aStatus;
////                        }
//
//                        int cat = OTP_TOKEN_SOFTWARE;
//                        log.debug("Category:" + "OTP_TOKEN_SOFTWARE");
//                        int subcat = 0;
//                        if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_SOFTWARE_MOBILE) {
//                            subcat = OTP_TOKEN_SOFTWARE;
//                            log.debug("Subcategory:" + "OTP_TOKEN_SOFTWARE");
//                        }
//                        if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_SOFTWARE_WEB) {
//                            subcat = OTP_TOKEN_SOFTWARE_WEB;
//                            log.debug("Subcategory:" + "OTP_TOKEN_SOFTWARE_WEB");
//                        }
//                        if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_SOFTWARE_PC);
//                        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//                        TokenStatusDetails tokenSelected = oManagement.getToken(sessionid, session.getChannelid(), UserId, cat);
//
//                        if (tokenSelected == null) {
//                            aStatus.errorcode = -2;
//                            aStatus.error = "Desired Token is not assigned";
//                            return aStatus;
//                        } else {
//                            if (tokenSelected.Status == TOKEN_STATUS_ACTIVE) {
//                                oldValue = strACTIVE;
//                            } else if (tokenSelected.Status == TOKEN_STATUS_SUSPENDED) {
//                                oldValue = strSUSPENDED;
//                            } else if (tokenSelected.Status == TOKEN_STATUS_UNASSIGNED) {
//                                oldValue = strUNASSIGNED;
//                            }
//
//                            if (credentials.tokenDetails[i].status == TOKEN_STATUS_ACTIVE) {
//                                strValue = strACTIVE;
//                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_SUSPENDED) {
//                                strValue = strSUSPENDED;
//                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_UNASSIGNED) {
//                                strValue = strUNASSIGNED;
//                            }
//
//                            retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), UserId, credentials.tokenDetails[i].status, cat, subcat);
//
//                            if (retValue == 0) {
//                                aStatus.error = "SUCCESS";
//                                aStatus.errorcode = 0;
//                            } else if (retValue == -6) {
//                                aStatus.error = "Status your trying to change is same as current status";
//                                aStatus.errorcode = -6;
//                            }
//
//                            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
//                                    channel.getName(),
//                                    session.getLoginid(), session.getLoginid(),
//                                    new Date(),
//                                    "CHANGE STATUS", aStatus.error, aStatus.errorcode,
//                                    "Token Management",
//                                    "Current Status=" + oldValue,
//                                    "New Status=" + strValue,
//                                    "OTPTOKENS", UserId);
//
//                        }
//
//                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.HARDWARE_TOKEN) {
//
////                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_HARDWARE) != 0) {
////                            aStatus = new AxiomStatus();
////                            aStatus.error = "This feature is not available in this license!!!";
////                            aStatus.errorcode = -101;
////                            return aStatus;
////                        }
//                        int cat = OTP_TOKEN_HARDWARE;
//                        int subcat = 0;
//                        if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_HARDWARE_CR) {
//                            subcat = OTP_TOKEN_HARDWARE_CR;
//                        } else {
//                            subcat = OTP_TOKEN_HARDWARE_MINI;
//                        }
//                        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//                        TokenStatusDetails tokenSelected = oManagement.getToken(sessionid, session.getChannelid(), UserId, cat);
//
//                        if (tokenSelected == null) {
//                            aStatus.errorcode = -2;
//                            aStatus.error = "Desired Token is not assigned";
//                            return aStatus;
//                        } else {
//                            if (tokenSelected.Status == TOKEN_STATUS_ACTIVE) {
//                                oldValue = strACTIVE;
//                            } else if (tokenSelected.Status == TOKEN_STATUS_SUSPENDED) {
//                                oldValue = strSUSPENDED;
//                            } else if (tokenSelected.Status == TOKEN_STATUS_UNASSIGNED) {
//                                oldValue = strUNASSIGNED;
//                            }
//
//                            if (credentials.tokenDetails[i].status == TOKEN_STATUS_ACTIVE) {
//                                strValue = strACTIVE;
//                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_SUSPENDED) {
//                                strValue = strSUSPENDED;
//                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_UNASSIGNED) {
//                                strValue = strUNASSIGNED;
//                            }
//
//                            retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), UserId, credentials.tokenDetails[i].status, cat, credentials.tokenDetails[i].subcategory);
//
//                            if (retValue == 0) {
//                                aStatus.error = "SUCCESS";
//                                aStatus.errorcode = 0;
//                            } else if (retValue == -6) {
//                                aStatus.error = "Status your trying to change is same as current status";
//                                aStatus.errorcode = -6;
//                            }
//
//                            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
//                                    channel.getName(),
//                                    session.getLoginid(), session.getLoginid(),
//                                    new Date(),
//                                    "CHANGE STATUS", aStatus.error, aStatus.errorcode,
//                                    "Token Management",
//                                    "Current Status=" + oldValue,
//                                    "New Status=" + strValue,
//                                    "OTPTOKENS", UserId);
//                        }
//
//                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.OUTOFBOUND_TOKEN) {
////                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_OOB) != 0) {
////                            aStatus = new AxiomStatus();
////                            aStatus.error = "This feature is not available in this license!!!";
////                            aStatus.errorcode = -101;
////                            return aStatus;
////                        }
//                        int cat = OTP_TOKEN_OUTOFBAND;
//                        int subcat = 0;
//                        if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_OUTOFBAND_SMS) {
//                            subcat = OTP_TOKEN_OUTOFBAND_SMS;
//                        }
//                        if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_OUTOFBAND_EMAIL) {
//                            subcat = OTP_TOKEN_OUTOFBAND_EMAIL;
//                        }
//                        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//                        TokenStatusDetails tokenSelected = oManagement.getToken(sessionid, session.getChannelid(), UserId, cat);
//
//                        if (tokenSelected == null) {
//                            aStatus.errorcode = -2;
//                            aStatus.error = "Desired Token is not assigned";
//                            return aStatus;
//                        } else {
//                            if (tokenSelected.Status == TOKEN_STATUS_ACTIVE) {
//                                oldValue = strACTIVE;
//                            } else if (tokenSelected.Status == TOKEN_STATUS_SUSPENDED) {
//                                oldValue = strSUSPENDED;
//                            } else if (tokenSelected.Status == TOKEN_STATUS_UNASSIGNED) {
//                                oldValue = strUNASSIGNED;
//                            }
//
//                            if (credentials.tokenDetails[i].status == TOKEN_STATUS_ACTIVE) {
//                                strValue = strACTIVE;
//                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_SUSPENDED) {
//                                strValue = strSUSPENDED;
//                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_UNASSIGNED) {
//                                strValue = strUNASSIGNED;
//                            }
//
//                            retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), UserId, credentials.tokenDetails[i].status, cat, credentials.tokenDetails[i].subcategory);
//
//                            if (retValue == 0) {
//                                aStatus.error = "SUCCESS";
//                                aStatus.errorcode = 0;
//                            } else if (retValue == -6) {
//                                aStatus.error = "Status your trying to change is same as current status";
//                                aStatus.errorcode = -6;
//                            }
//
//                            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
//                                    channel.getName(),
//                                    session.getLoginid(), session.getLoginid(),
//                                    new Date(),
//                                    "CHANGE STATUS", aStatus.error, aStatus.errorcode,
//                                    "Token Management",
//                                    "Current Status=" + oldValue,
//                                    "New Status=" + strValue,
//                                    "OTPTOKENS", UserId);
//                        }
//
//                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.PKI_SOFTWARE_TOKEN) {
////                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.PKI_SOFTWARE) != 0) {
////                            aStatus = new AxiomStatus();
////                            aStatus.error = "This feature is not available in this license!!!";
////                            aStatus.errorcode = -101;
////                            return aStatus;
////                        }
//
//                        int cat = PKITokenManagement.SOFTWARE_TOKEN;
//                        int subcat = 0;
//
//                        PKITokenManagement pManagement = new PKITokenManagement();
//                        TokenStatusDetails tokenSelected = pManagement.getToken(sessionid, session.getChannelid(), UserId, cat);
//
//                        if (tokenSelected == null) {
//                            aStatus.errorcode = -2;
//                            aStatus.error = "Desired Token is not assigned";
//                            return aStatus;
//                        } else {
//                            if (tokenSelected.Status == TOKEN_STATUS_ACTIVE) {
//                                oldValue = strACTIVE;
//                            } else if (tokenSelected.Status == TOKEN_STATUS_SUSPENDED) {
//                                oldValue = strSUSPENDED;
//                            } else if (tokenSelected.Status == TOKEN_STATUS_UNASSIGNED) {
//                                oldValue = strUNASSIGNED;
//                            }
//
//                            if (credentials.tokenDetails[i].status == TOKEN_STATUS_ACTIVE) {
//                                strValue = strACTIVE;
//                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_SUSPENDED) {
//                                strValue = strSUSPENDED;
//                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_UNASSIGNED) {
//                                strValue = strUNASSIGNED;
//                            }
//
//                            retValue = pManagement.ChangeStatus(sessionid, session.getChannelid(), UserId, credentials.tokenDetails[i].status, cat);
//
//                            if (retValue == 0) {
//                                aStatus.error = "SUCCESS";
//                                aStatus.errorcode = 0;
//                            } else if (retValue == -6) {
//                                aStatus.error = "Status your trying to change is same as current status";
//                                aStatus.errorcode = -6;
//                            }
//
//                            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
//                                    channel.getName(),
//                                    session.getLoginid(), session.getLoginid(),
//                                    new Date(),
//                                    "CHANGE STATUS", aStatus.error, aStatus.errorcode,
//                                    "PKI Token Management",
//                                    "Current Status=" + oldValue,
//                                    "New Status=" + strValue,
//                                    "PKITOKEN", UserId);
//
//                        }
//
//                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.PKI_HARDWARE_TOKEN) {
////                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.PKI_HARDWARE) != 0) {
////                            aStatus = new AxiomStatus();
////                            aStatus.error = "This feature is not available in this license!!!";
////                            aStatus.errorcode = -101;
////                            return aStatus;
////                        }
//
//                        int cat = PKITokenManagement.HARDWARE_TOKEN;
//                        int subcat = 0;
//
//                        PKITokenManagement pManagement = new PKITokenManagement();
//                        TokenStatusDetails tokenSelected = pManagement.getToken(sessionid, session.getChannelid(), UserId, cat);
//
//                        if (tokenSelected == null) {
//                            aStatus.errorcode = -2;
//                            aStatus.error = "Desired Token is not assigned";
//                            return aStatus;
//                        } else {
//                            if (tokenSelected.Status == TOKEN_STATUS_ACTIVE) {
//                                oldValue = strACTIVE;
//                            } else if (tokenSelected.Status == TOKEN_STATUS_SUSPENDED) {
//                                oldValue = strSUSPENDED;
//                            } else if (tokenSelected.Status == TOKEN_STATUS_UNASSIGNED) {
//                                oldValue = strUNASSIGNED;
//                            }
//
//                            if (credentials.tokenDetails[i].status == TOKEN_STATUS_ACTIVE) {
//                                strValue = strACTIVE;
//                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_SUSPENDED) {
//                                strValue = strSUSPENDED;
//                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_UNASSIGNED) {
//                                strValue = strUNASSIGNED;
//                            }
//
//                            retValue = pManagement.ChangeStatus(sessionid, session.getChannelid(), UserId, credentials.tokenDetails[i].status, cat);
//
//                            if (retValue == 0) {
//                                aStatus.error = "SUCCESS";
//                                aStatus.errorcode = 0;
//                            } else if (retValue == -6) {
//                                aStatus.error = "Status your trying to change is same as current status";
//                                aStatus.errorcode = -6;
//                            }
//
//                            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
//                                    channel.getName(),
//                                    session.getLoginid(), session.getLoginid(),
//                                    new Date(),
//                                    "CHANGE STATUS", aStatus.error, aStatus.errorcode,
//                                    "PKI Token Management",
//                                    "Current Status=" + oldValue,
//                                    "New Status=" + strValue,
//                                    "PKITOKEN", UserId);
//
//                        }
//
//                    } //nilesh
//                    else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.SECURE_PHRASE) {
//                        SecurePhraseManagment sMngt = new SecurePhraseManagment();
//
//                        Securephrase Securephrase = sMngt.GetSecurePhrase(sessionid, session.getChannelid(), UserId);
////                        byte[] _securephraseID = UtilityFunctions.SHA1("" + credentials.tokenDetails[i].securePhraseImage + credentials.tokenDetails[i].xCoordinate + credentials.tokenDetails[i].yCoordinate);
////                        String securephraseID = new String(Base64.encode(_securephraseID));
//                        if (Securephrase == null) {
//                            retValue = sMngt.AddSecurePhrase(sessionid, session.getChannelid(), UserId,
//                                    credentials.tokenDetails[i].securePhraseImage, credentials.tokenDetails[i].xCoordinate, credentials.tokenDetails[i].yCoordinate, credentials.tokenDetails[i].securePhraseID);
//                        } else {
//                            retValue = sMngt.EditSecurePhrase(sessionid, session.getChannelid(), UserId,
//                                    credentials.tokenDetails[i].securePhraseImage, credentials.tokenDetails[i].xCoordinate, credentials.tokenDetails[i].yCoordinate, credentials.tokenDetails[i].securePhraseID);
//                        }
//                        if (retValue == 0) {
//                            aStatus.error = "SUCCESS";
//                            aStatus.errorcode = 0;
//                        } else if (retValue == -1) {
//                            aStatus.error = "Failed to change security phrase";
//                            aStatus.errorcode = -1;
//                        }
//                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
//                                channel.getName(),
//                                session.getLoginid(), session.getLoginid(),
//                                new Date(),
//                                "CHANGE SECURITY PHRASE", aStatus.error, aStatus.errorcode,
//                                "SECURITY PHRASE Management",
//                                "Current Status=" + oldValue,
//                                "New Status=" + strValue,
//                                "SECURITYPHRASE", UserId);
//
//                    }
//                    //nilesh
//
//                }
//            }
//
//        }
//        return aStatus;
//
//    }
//
////    @Override
////    public AxiomStatus UpdateUserCredentialWallet(String sessionid, String userid, int type, String clientPayload) {
////       String strDebug = null;
////        try {
////            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
////
////            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
////                System.out.println("ChangeTokenType::sessionid::" + sessionid);
////                System.out.println("ChangeTokenType::userid::" +userid);
////            
////              
////            }
////        } catch (Exception ex) {
////        }
//////nilesh
////        int iResult = AxiomProtect.ValidateLicense();
////        if (iResult != 0) {
////            AxiomStatus aStatus = new AxiomStatus();
////            aStatus.error = "Licence is invalid";
////            aStatus.errorcode = -100;
////            return aStatus;
////             }
////           AxiomStatus aStatus = new AxiomStatus();
////
////               byte[] SHA1hash = null;
////       
//////        String integritycheck = new String(Base64.encode(SHA1hash));
//////
//////        if (integrityCheckString != null) {
//////            if (integrityCheckString.equals(integritycheck) != true) {
//////                aStatus.error = "Input Data is Tampered!!!";
//////                aStatus.errorcode = -76;
//////                return aStatus;
//////
//////            }
//////        }
////
////        SessionManagement sManagement = new SessionManagement();
////        AuditManagement audit = new AuditManagement();
////        MessageContext mc = wsContext.getMessageContext();
////        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
////        Sessions session = sManagement.getSessionById(sessionid);
////        if (session == null) {
////
////            aStatus.error = "Invalid Session";
////            aStatus.errorcode = -14;
////            return aStatus;
////        }
////
//////        SessionManagement sManagement = new SessionManagement();
//////        Sessions session = sManagement.getSessionById(sessionid);
//////        MessageContext mc = wsContext.getMessageContext();
//////        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
////        aStatus.error = "ERROR";
////
////        aStatus.errorcode = -2;
////        String oldValue = "";
////        String strValue = "";
////        
////        int retValue = -1;
//////        if (session == null) {
//////            return aStatus;
//////        }
////        if (session != null) {
////
////            //System.out.println("Client IP = " + req.getRemoteAddr());        
////            SettingsManagement setManagement = new SettingsManagement();
//////            int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
//////            if (result != 1) {
//////                aStatus.error = "INVALID IP REQUEST";
//////                aStatus.errorcode = -8;
//////                //  return aStatus;
//////            }
////
////            String channelid = session.getChannelid();
////            ChannelManagement cManagement = new ChannelManagement();
////            Channels channel = cManagement.getChannelByID(channelid);
////            if (channel == null) {
////                return null;
////            }
////
////            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
////            if (channelProfileObj != null) {
////                int retVal = sManagement.getServerStatus(channelProfileObj);
////                if (retVal != 0) {
////
////                    aStatus = new AxiomStatus();
////                    aStatus.error = "Server Down for maintainance,Please try later!!!";
////                    aStatus.errorcode = -48;
////                    return aStatus;
////                }
////            }
////
////            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
////            if (ipobj != null) {
////                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
////                int checkIp = 1;
////                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
////                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
////                } else {
////                    checkIp = 1;
////                }
////                if (iObj.ipstatus == 0 && checkIp != 1) {
////                    if (iObj.ipalertstatus == 0) {
////                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
////                        Session sTemplate = suTemplate.openSession();
////                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
////                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
////                        OperatorsManagement oManagement = new OperatorsManagement();
////                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
////                        if (aOperator != null) {
////                            String[] emailList = new String[aOperator.length - 1];
////                            for (int i = 1; i < aOperator.length; i++) {
////                                emailList[i - 1] = aOperator[i].getEmailid();
////                            }
////                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
////                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
////                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
////                                String strsubject = templatesObj.getSubject();
////                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
////                                if (strmessageBody != null) {
////                                    // Date date = new Date();
////                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
////                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
////                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
////                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
////                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
////                                }
////
////                                SendNotification send = new SendNotification();
////                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
////                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
////                            }
////
////                            suTemplate.close();
////                            sTemplate.close();
////                            aStatus.error = "INVALID IP REQUEST";
////                            aStatus.errorcode = -8;
////                            return aStatus;
////                        }
////
////                        suTemplate.close();
////                        sTemplate.close();
////                        aStatus.error = "INVALID IP REQUEST";
////                        aStatus.errorcode = -8;
////                        return aStatus;
////                    }
////                    aStatus.error = "INVALID IP REQUEST";
////                    aStatus.errorcode = -8;
////                    return aStatus;
////                }
////            }
////            boolean bSentToUser = true;
////            int subcategory = 1;
////            if (clientPayload != null) {
////                MobileTrustSettings mSettings = null;
////
////                Object obj = setManagement.getSetting(sessionid, channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
////                if (obj != null) {
////                    if (obj instanceof MobileTrustSettings) {
////                        mSettings = (MobileTrustSettings) obj;
////                    }
////                }
////
////                if (mSettings == null) {
////                    aStatus.error = "Mobile Trust Setting is not configured!!!";
////                    aStatus.errorcode = -23;
//////                  
////                    return aStatus;
////                }
////
////                if (mSettings.bGeoFencing == true) {
////
//////                    TrustManagement tManagement = new TrustManagement();
//////                    int res = tManagement.checkLocationAndUpdateWMI(sessionid, channelid, credentials.axiomUser.userId, trustPayLoad);
//////                    if (res != 0) {
//////                        if (res == -14) {
//////                            aStatus.errorcode = -14;
//////                            aStatus.error = "Mobile Trust Setting is not configured!!!";
//////
//////                        } else if (res == -4) {
//////                            aStatus.errorcode = 4;
//////                            aStatus.error = "Verification failed!!!";
//////                        } else if (res == -7) {
//////                            aStatus.errorcode = -7;
//////                            aStatus.error = "Home Country is not defined!!!";
//////                        } else if (res == -10) {
//////                            aStatus.errorcode = -10;
//////                            aStatus.error = "Invalid Location, out of home country!!!";
//////                        } else if (res == -11) {
//////                            aStatus.errorcode = -11;
//////                            aStatus.error = "Invalid Location, roaming country is not allowed!!!";
//////                        } else if (res == -12) {
//////                            aStatus.errorcode = -12;
//////                            aStatus.error = "Invalid Location, roaming is not configured properly!!!";
//////                        } else if (res == -13) {
//////                            aStatus.errorcode = -13;
//////                            aStatus.error = "Roaming Contry is not Configured!!!";
//////                        } else if (res == -1) {
//////                            aStatus.errorcode = -1;
//////                            aStatus.error = "Internal Error!!!";
//////                        } else if (res == -2) {
//////                            aStatus.errorcode = -2;
//////                            aStatus.error = "Session has Expired!!!";
//////                        }
////                GeoLocationManagement gManagement = new GeoLocationManagement();
////           AXIOMStatus status = gManagement.validateGeoLocation(sessionid, channelid,userid, clientPayload);
////                    if (status == null) {
////                        aStatus.error = "Location Not Found!!!";
////                        aStatus.errorcode = -115;
////                    } else if (status.iStatus != 0) {
////
////                        aStatus.error = status.strStatus;
////                        aStatus.errorcode = status.iStatus;
////                        //     String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
////                        audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
////                                session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorcode,
////                                "Token Management", "Invalid Location", null, itemtype,
////                                session.getLoginid());
////
////                        return aStatus;
////                    }
////                }
////            }
////        
////        
////        
////        
////      return null;  
////    }
////        return  null;
////    }
//    public AxiomUserCerdentials[] GetAllUsers(String sessionid, int type, String searchFor[]) {
//        String strDebug = null;
//        List<AxiomUserCerdentials> axiomUserList = new ArrayList<AxiomUserCerdentials>();
//        AxiomUserCerdentials[] aUCredentials = null;
//        AxiomUserCerdentials AxiomUCred = new AxiomUserCerdentials();
//
//        System.out.println("Start::" + new Date());
//        try {
//            try {
//                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//
//                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                    System.out.println("GetUserBy::sessionId::" + sessionid);
//                    System.out.println("GetUserBy::type::" + type);
//                    System.out.println("GetUserBy::searchFor::" + searchFor);
//                }
//            } catch (Exception ex) {
//            }
////nilesh
////            int iResult = AxiomProtect.ValidateLicense();
////            if (iResult != 0) {
////                try {
////                    AxiomStatus aStatus = new AxiomStatus();
////                    AxiomUCred.errorMsg = aStatus.error = "Licence is invalid";
////                    AxiomUCred.errorCode = aStatus.errorcode = -100;
////
////                    throw new AxiomException("Licence is invalid");
////                } catch (AxiomException ex) {
////                    ex.printStackTrace();
////                }
////            }
//
//            AxiomStatus aStatus = new AxiomStatus();
//            String resultStr = "Failure";
//            int retValue = -1;
//
//            System.out.println("2::" + new Date());
//
//            UserManagement uManagement = new UserManagement();
//            SessionManagement sManagement = new SessionManagement();
//            AuditManagement audit = new AuditManagement();
//
//            MessageContext mc = wsContext.getMessageContext();
//            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//            Sessions session = sManagement.getSessionById(sessionid);
////            AxiomUser aUser = null;
//            if (sessionid == null) {
//                throw new Exception("Session ID is empty!!!");
//            }
//
//            if (session == null) {
//                throw new Exception("Session ID is invalid/expired!!!");
//            }
//
//            ChannelManagement cManagement = new ChannelManagement();
//            Channels channel = cManagement.getChannelByID(session.getChannelid());
//            if (session != null) {
//                SettingsManagement setManagement = new SettingsManagement();
//                String channelid = session.getChannelid();
//                System.out.println("3::" + new Date());
//                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
//                if (channelProfileObj != null) {
//                    int retVal = sManagement.getServerStatus(channelProfileObj);
//                    if (retVal != 0) {
//                        throw new AxiomException("Server Down for maintainance,Please try later!!!");
//                    }
//                }
//                if (channel == null) {
//                    throw new AxiomException("Server Down for maintainance,Please try later!!!");
//                }
//                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//                if (ipobj != null) {
//                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                    int checkIp = 1;
//                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                    } else {
//                        checkIp = 1;
//                    }
//                    if (iObj.ipstatus == 0 && checkIp != 1) {
//                        if (iObj.ipalertstatus == 0) {
//                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                            Session sTemplate = suTemplate.openSession();
//                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                            OperatorsManagement oManagement = new OperatorsManagement();
//                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                            if (aOperator != null) {
//                                String[] emailList = new String[aOperator.length - 1];
//                                for (int i = 1; i < aOperator.length; i++) {
//                                    emailList[i - 1] = aOperator[i].getEmailid();
//                                }
//                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                    String strsubject = templatesObj.getSubject();
//                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                    if (strmessageBody != null) {
//                                        // Date date = new Date();
//                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                    }
//
//                                    SendNotification send = new SendNotification();
//                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                                }
//
//                                suTemplate.close();
//                                sTemplate.close();
//                                throw new Exception("IP is not allowed!!!");
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
//                            throw new Exception("IP is not allowed!!!");
//                        }
//                        throw new Exception("IP is not allowed!!!");
//                    }
//                }
//
//                System.out.println("4::" + new Date());
//
//                String userFlag = LoadSettings.g_sSettings.getProperty("check.user");
//                AuthUser authUser = null;
//                String USERID;
//                AxiomUser axiomUser = null;
//                //            RSSUserCerdentials axiomUserCred = null;
//                AxiomCredentialDetails[] atDetails = null;
////                TokenStatusDetails[] tDetails = null;//nilesh
//                Otptokens[] tDetails = null;
//                List<AxiomCredentialDetails> AlDetails = new ArrayList<AxiomCredentialDetails>();
//
//                List<AuthUser> aUserList = new ArrayList<AuthUser>();
//                for (int i = 0; i < searchFor.length; i++) {
//                    if (userFlag.equals("1")) {
//                        authUser = uManagement.CheckUserByType(sessionid, session.getChannelid(), searchFor[i], type);
//                        if (authUser != null) {
//                            aUserList.add(authUser);
//                        }
//                    } else {
//                        authUser = uManagement.CheckUserByType(sessionid, session.getChannelid(), searchFor[i], type);
//                        if (authUser != null) {
//                            aUserList.add(authUser);
//                        }
//                    }
//                }
//                System.out.println("5::" + new Date());
//                for (int j = 0; j < aUserList.size(); j++) {
//                    if (aUserList.get(j) != null) {
//
//                        axiomUser = new AxiomUser();
//                        axiomUser.phoneNo = aUserList.get(j).phoneNo;
//                        axiomUser.email = aUserList.get(j).email;
//                        axiomUser.userName = aUserList.get(j).userName;
//                        axiomUser.userId = aUserList.get(j).userId;
//                        axiomUser.lLastAccessOn = aUserList.get(j).lLastAccessOn;
//                        axiomUser.lCreatedOn = aUserList.get(j).lCreatedOn;
//                        axiomUser.iAttempts = aUserList.get(j).iAttempts;
//                        axiomUser.statePassword = aUserList.get(j).statePassword;
//                        axiomUser.organisation = aUserList.get(j).organisation;
//                        axiomUser.organisationUnit = aUserList.get(j).organisationUnit;
//                        axiomUser.country = aUserList.get(j).country;
//                        axiomUser.location = aUserList.get(j).location;
//                        axiomUser.street = aUserList.get(j).street;
//
//                        axiomUser.designation = aUserList.get(j).designation;
//                        axiomUser.userIdentity = aUserList.get(j).userIdentity;
//                        axiomUser.idType = aUserList.get(j).idType;
//
//                        USERID = aUserList.get(j).userId;
//
//                        if (USERID != null && USERID != "") {
//                            AxiomCredentialDetails PassDetails = new AxiomCredentialDetails();
//                            PassDetails.category = AxiomCredentialDetails.PASSWORD;
//                            PassDetails.subcategory = 0;
//                            PassDetails.Password = aUserList.get(j).getPassword();
//                            PassDetails.attempts = aUserList.get(j).iAttempts;
//                            PassDetails.createOn = aUserList.get(j).lCreatedOn;
//                            PassDetails.lastaccessOn = aUserList.get(j).lLastAccessOn;
//                            PassDetails.status = aUserList.get(j).statePassword;
//                            AlDetails.add(PassDetails);
//
//                        }
//
//                        System.out.println("7::" + new Date());
//
//                        if (USERID != null && USERID != "") {
//
//                            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//
////                        tDetails = oManagement.getTokenList(sessionid, session.getChannelid(), USERID);//nilesh
//                            tDetails = oManagement.getTokenListv2(sessionid, session.getChannelid(), USERID);
//                            Object settings = setManagement.getSetting(sessionid, channelid, SettingsManagement.Token, SettingsManagement.PREFERENCE_ONE);
//                            TokenSettings tokenObj = null;
//                            if (settings != null) {
//                                tokenObj = (TokenSettings) settings;
//                            }
//                            if (tDetails != null) {
//                                for (int i = 0; i < tDetails.length; i++) {
//                                    AxiomCredentialDetails TokenDetail = new AxiomCredentialDetails();
//                                    int res = -1;
//                                    if (tDetails[i].getStatus() == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
////                                    res = oManagement.unlockTokenAfter(channelid, USERID, tokenObj, tDetails[i]);//nilesh
//                                        res = oManagement.unlockTokenAfterv2(channelid, USERID, tokenObj, tDetails[i]);
//                                    }
//
//                                    if (tDetails[i].getCategory() == OTP_TOKEN_SOFTWARE) {
//                                        TokenDetail.category = AxiomCredentialDetails.SOFTWARE_TOKEN;
//                                    } else if (tDetails[i].getCategory() == OTP_TOKEN_HARDWARE) {
//                                        TokenDetail.category = AxiomCredentialDetails.HARDWARE_TOKEN;
//                                    } else if (tDetails[i].getCategory() == OTP_TOKEN_OUTOFBAND) {
//                                        TokenDetail.category = AxiomCredentialDetails.OUTOFBOUND_TOKEN;
//                                    }
//
//                                    TokenDetail.subcategory = tDetails[i].getSubcategory();
//                                    TokenDetail.Password = null;
//                                    TokenDetail.attempts = tDetails[i].getAttempts();
//                                    TokenDetail.createOn = tDetails[i].getCreationdatetime().getTime();
//                                    TokenDetail.lastaccessOn = tDetails[i].getLastaccessdatetime().getTime();
//                                    if (res == 0) {
//                                        TokenDetail.status = OTPTokenManagement.TOKEN_STATUS_ACTIVE;
//                                    } else {
//                                        TokenDetail.status = tDetails[i].getStatus();
//                                    }
//                                    TokenDetail.serialnumber = tDetails[i].getSrno();
//                                    AlDetails.add(TokenDetail);
//
//                                }
//                            }
//
//                        }
//
//                        System.out.println("8::" + new Date());
//                        TokenStatusDetails[] tSDetails = null;
//                        if (USERID != null && USERID != "") {
//
//                            PKITokenManagement pManagement = new PKITokenManagement();
//
//                            tSDetails = pManagement.getTokenList(sessionid, session.getChannelid(), USERID);
//
//                            if (tDetails != null) {
//                                for (int i = 0; i < tDetails.length; i++) {
//                                    AxiomCredentialDetails TokenDetail = new AxiomCredentialDetails();
//                                    if (tDetails[i].getCategory() == PKITokenManagement.SOFTWARE_TOKEN) {
//                                        TokenDetail.category = AxiomCredentialDetails.PKI_SOFTWARE_TOKEN;
//                                    } else if (tDetails[i].getCategory() == PKITokenManagement.HARDWARE_TOKEN) {
//                                        TokenDetail.category = AxiomCredentialDetails.PKI_HARDWARE_TOKEN;
//                                    }
//
//                                    TokenDetail.subcategory = tDetails[i].getSubcategory();
//                                    TokenDetail.Password = null;
//                                    TokenDetail.attempts = tDetails[i].getAttempts();
//                                    TokenDetail.createOn = tDetails[i].getCreationdatetime().getTime();
//                                    TokenDetail.lastaccessOn = tDetails[i].getLastaccessdatetime().getTime();
//                                    TokenDetail.status = tDetails[i].getStatus();
//                                    TokenDetail.serialnumber = tDetails[i].getSrno();
//                                    AlDetails.add(TokenDetail);
//
//                                }
//                            }
//
//                        }
//                        if (!"".equals(USERID)) {
//
//                            CertificateManagement certManagement = new CertificateManagement();
//                            Certificates cert = certManagement.getCertificate(sessionid, session.getChannelid(), USERID);
//
//                            if (cert != null) {
//
//                                AxiomCredentialDetails CertDetails = new AxiomCredentialDetails();
//                                CertDetails.category = AxiomCredentialDetails.CERTIFICATE;
//                                CertDetails.Password = cert.getPfxpassword();
//                                CertDetails.createOn = cert.getCreationdatetime().getTime();
//                                CertDetails.serialnumber = cert.getSrno();
//                                CertDetails.status = cert.getStatus();
//                                CertDetails.base64Cert = cert.getCertificate();
//                                CertDetails.expiryDate = cert.getExpirydatetime().getTime();
//                                AlDetails.add(CertDetails);
//
//                            }
//                        }
//                        if (!"".equals(USERID)) {
//
//                            AxiomCredentialDetails ChallengeResponseDetails = new AxiomCredentialDetails();
//                            ChallengeResponsemanagement ChalResp = new ChallengeResponsemanagement();
//                            AxiomQuestionsAndAnswers qanda = ChalResp.getUserQuestionsandAnswers(sessionid, session.getChannelid(), USERID);
//
//                            if (qanda != null) {
//                                ChallengeResponseDetails.qas = new AxiomChallengeResponse();
//                                ChallengeResponseDetails.qas.webQAndA = qanda.webQAndA;
//                                AlDetails.add(ChallengeResponseDetails);
//                            }
//                        }
//                        System.out.println("9::" + new Date());
//
//                        atDetails = new AxiomCredentialDetails[AlDetails.size()];
//                        for (int i = 0; i < AlDetails.size(); i++) {
//                            atDetails[i] = AlDetails.get(i);
//                        }
//
//                        System.out.println("10::" + new Date());
//
//                        AxiomUCred = new AxiomUserCerdentials();
//                        AxiomUCred.axiomUser = axiomUser;
//                        AxiomUCred.tokenDetails = atDetails;
//
//                        if (AxiomUCred != null) {
//                            retValue = 0;
//                            resultStr = "Success";
//                            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                                    req.getRemoteAddr(), channel.getName(),
//                                    session.getLoginid(), session.getLoginid(), new Date(),
//                                    "GET USER", resultStr, retValue,
//                                    "User Management",
//                                    "Name=" + AxiomUCred.axiomUser.userName + ",Phone=" + AxiomUCred.axiomUser.phoneNo + ",Email=" + AxiomUCred.axiomUser.email,
//                                    "", "USER", AxiomUCred.axiomUser.userId);
//                        } else if (AxiomUCred == null) {
//                            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                                    req.getRemoteAddr(), channel.getName(),
//                                    session.getLoginid(), session.getLoginid(), new Date(),
//                                    "GET USER", resultStr, retValue,
//                                    "User Management", "", "", "USER", "");
//                        }
//
//                    }
//                    if (AxiomUCred != null) {
//                        axiomUserList.add(AxiomUCred);
//                    }
//                }
//                System.out.println("exiting::" + new Date());
//                if (axiomUserList != null || axiomUserList.size() > 0) {
//                    aUCredentials = new AxiomUserCerdentials[axiomUserList.size()];
//
//                    for (int i = 0; i < axiomUserList.size(); i++) {
//
//                        aUCredentials[i] = axiomUserList.get(i);
//
//                    }
//                }
//
//                return aUCredentials;
//
//            }
//        } catch (Exception ex) {
////            Logger.getLogger(RSSCoreInterfaceImpl.class.getName()).log(Level.SEVERE, null, ex);
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//    @Override
//    public MobileTrustStatus GetUserCredentialWallet(String sessionid, String userid, String did) {
//        String strDebug = null;
//        String hiddenKey = "";
//        String visibleKey = "";
//
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                log.info("GetUserCredentialWallet::sessionid::" + sessionid);
//                log.info("GetUserCredentialWallet:Started"
//                        + "::");
//                log.info("GetUserCredentialWallet::userid::" + userid);
//                log.info("GetUserCredentialWallet::Device Id::" + did);
//
//            }
//        } catch (Exception ex) {
//            log.error(String.format("##GetUserCredentialWallet :  - exception caught when calling GetUserCredentialWallet - ", ex.getMessage()));
//        }
//
////        int iResult = 0;
////        if (iResult != 0) {
////            MobileTrustStatus aStatus = new MobileTrustStatus();
////            aStatus.error = "Licence Invalid";
////            aStatus.errorCode = -100;
////            aStatus.data = null;
////            return aStatus;
////        }
////        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.MOBILETRUST) != 0) {
////            MobileTrustStatus aStatus = new MobileTrustStatus();
////            aStatus.error = "OTP Token feature is not available in this license!!!";
////            aStatus.errorCode = -101;
////            return aStatus;
////        }
////       int iResult = 0;
////        if (iResult != 0) {
////            AxiomStatus aStatus = new AxiomStatus();
////            aStatus.error = "Licence Not Valid";
////            aStatus.errorcode = -10;
////            return aStatus;
////        }
//        MobileTrustStatus aStatus = new MobileTrustStatus();
//        aStatus.error = "ERROR";
//        aStatus.errorCode = -2;
//
//        SessionManagement sManagement = new SessionManagement();
//        Sessions session = sManagement.getSessionById(sessionid);
//
//        if (session == null) {
//            aStatus.error = "Session Invalid!!!";
//            log.error(String.format("##GetUserCredentialWallet :  - exception caught when calling GetUserCredentialWallet - ", aStatus.error));
//            aStatus.errorCode = -14;
//            return aStatus;
//        }
//
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        String signData = null;
//
//        String oldValue = "";
//        String strValue = "";
//        //String otp = null;
//        String cert = null;
//
//        if (session != null) {
//            try {
//                SettingsManagement setManagement = new SettingsManagement();
//                String channelid = session.getChannelid();
//                ChannelManagement cManagement = new ChannelManagement();
//                Channels channel = cManagement.getChannelByID(channelid);
//                if (channel == null) {
//                    aStatus.error = "Invalid Channel!!!";
//                    log.error(String.format("##GetUserCredentialWallet :  - exception caught when calling GetUserCredentialWallet - ", aStatus.error));
//                    aStatus.errorCode = -3;
//                    return aStatus;
//                }
//                String serverPrivateKey = channel.getHiddenkey();
//                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//                if (ipobj != null) {
//                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                    int checkIp = 1;
//                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                        log.debug("##GetUserCredentialWallet:checkIp" + checkIp);
//
//                    } else {
//                        checkIp = 1;
//                    }
//                    if (iObj.ipstatus == 0 && checkIp != 1) {
//                        if (iObj.ipalertstatus == 0) {
//                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                            Session sTemplate = suTemplate.openSession();
//                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                            OperatorsManagement oManagement = new OperatorsManagement();
//                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                            if (aOperator != null) {
//                                String[] emailList = new String[aOperator.length - 1];
//                                for (int i = 1; i < aOperator.length; i++) {
//                                    emailList[i - 1] = aOperator[i].getEmailid();
//                                }
//                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                    String strsubject = templatesObj.getSubject();
//                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                    if (strmessageBody != null) {
//                                        // Date date = new Date();
//                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                    }
//
//                                    SendNotification send = new SendNotification();
//                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                                    log.debug("GetUserCredentialWalletSendEmail Status" + axStatus.strStatus);
//                                }
//
//                                suTemplate.close();
//                                sTemplate.close();
//                                aStatus.error = "INVALID IP";
//                                log.error(String.format("##GetUserCredentialWallet :  - exception caught when calling GetUserCredentialWallet - ", aStatus.error));
//                                aStatus.errorCode = -8;
//                                return aStatus;
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
//                            aStatus.error = "INVALID IP";
//                            log.error(String.format("##GetUserCredentialWallet :  - exception caught when calling GetUserCredentialWallet - ", aStatus.error));
//                            aStatus.errorCode = -8;
//                            return aStatus;
//                        }
//                        aStatus.error = "INVALID IP";
//                        log.error(String.format("##GetUserCredentialWallet :  - exception caught when calling GetUserCredentialWallet - ", aStatus.error));
//                        aStatus.errorCode = -8;
//                        return aStatus;
//                    }
//                }
//                OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//                Otptokens oToken = oManagement.getOtpObjByUserId(sessionid, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE);
//
//                if (oToken == null) {
//                    aStatus.errorCode = -20;
//                    aStatus.error = "Desired Token is not assigned";
//                    log.error(String.format("##GetUserCredentialWallet :  - exception caught when calling GetUserCredentialWallet - ", aStatus.error));
//                    aStatus.data = signData;
//                    return aStatus;
//                }
//
//                if (oToken.getStatus() == TOKEN_STATUS_ACTIVE) {
//                    oldValue = strACTIVE;
//                    log.debug("##GetUserCredentialWalletToken Staus" + "TOKEN_STATUS_ACTIVE");
//                } else if (oToken.getStatus() == TOKEN_STATUS_SUSPENDED) {
//                    log.debug("##GetUserCredentialWalletToken Staus" + "TOKEN_STATUS_SUSPENDED");
//                    oldValue = strSUSPENDED;
//                } else if (oToken.getStatus() == TOKEN_STATUS_UNASSIGNED) {
//                    log.debug("##GetUserCredentialWalletToken Staus" + "TOKEN_STATUS_UNASSIGNED");
//                    oldValue = strUNASSIGNED;
//                }
//
//                strValue = strACTIVE;
//
//                UserManagement uManagement = new UserManagement();
//                String password = uManagement.GetPassword(sessionid, channelid, userid);
////                if (password == null) {
////
////                    aStatus.error = "Invalid User";
////                    aStatus.errorCode = -46;
////
////                }
//
//                MobileTrustSettings mSettings = null;
//                int retValue = -1;
//                Object obj = setManagement.getSetting(sessionid, session.getChannelid(), SettingsManagement.MOBILETRUST_SETTING, 1);
//                if (obj != null) {
//                    if (obj instanceof MobileTrustSettings) {
//                        mSettings = (MobileTrustSettings) obj;
//                    }
//                }
//
//                RootCertificateSettings rcaSettings = (RootCertificateSettings) setManagement.getSetting(sessionid, session.getChannelid(), SettingsManagement.RootConfiguration, 1);
//                if (rcaSettings == null) {
//                    aStatus.error = "Certificate Setting is not configured!!!";
//                    log.error(String.format("##GetUserCredentialWallet :  - exception caught when calling GetUserCredentialWallet - ", aStatus.error));
//                    aStatus.errorCode = -65;
//                    aStatus.data = signData;
//                    return aStatus;
//                }
//
//                if (mSettings == null) {
//                    aStatus.error = "Mobile Trust Setting is not configured!!!";
//                    log.error(String.format("##GetUserCredentialWallet :  - exception caught when calling GetUserCredentialWallet - ", aStatus.error));
//                    aStatus.errorCode = -42;
//                    aStatus.data = signData;
//                    return aStatus;
//                }
//
//                TokenSettings tSettings = (TokenSettings) setManagement.getSetting(sessionid, session.getChannelid(), TOKEN, 1);
//                if (tSettings == null) {
//                    aStatus.error = "OTP Token Setting is not configured!!!";
//                    log.error(String.format("##GetUserCredentialWallet :  - exception caught when calling GetUserCredentialWallet - ", aStatus.error));
//                    aStatus.errorCode = -66;
//                    aStatus.data = signData;
//                    return aStatus;
//                }
//
//                // CertificateManagement certManagement = new CertificateManagement();
////                Certificates certObj = certManagement.getCertificate(sessionid, channelid, userid);
////                if (certObj != null) {
////                    cert = certObj.getCertificate();
//////                            String filepath = certManagement.getPFXFile(sessionid, channelid, userid);
////                    byte[] pfxAsBytes = Base64.decode(certObj.getPfx());
////                    ByteArrayInputStream fis = new ByteArrayInputStream(Base64.decode(pfxAsBytes));
////
////                    // supported KeyStore types (JDK1.4): PKCS12 and JKS (native Sun)
////                    KeyStore ks = KeyStore.getInstance("PKCS12");
////                    ks.load(fis, certObj.getPfxpassword().toCharArray());
////                    for (Enumeration num = ks.aliases(); num.hasMoreElements();) {
////                        String alias = (String) num.nextElement();
////                        if (ks.isKeyEntry(alias)) {
////                            PrivateKey priKey = (PrivateKey) ks.getKey(alias, certObj.getPfxpassword().toCharArray());
////                            hiddenKey = new String(Base64.encode(priKey.getEncoded()));
////
////                            RSAPrivateCrtKey privk = (RSAPrivateCrtKey) priKey;
////
////                            RSAPublicKeySpec publicKeySpec = new java.security.spec.RSAPublicKeySpec(privk.getModulus(), privk.getPublicExponent());
////
////                            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
////                            PublicKey myPublicKey = keyFactory.generatePublic(publicKeySpec);
////                            visibleKey = new String(Base64.encode(myPublicKey.getEncoded()));
////                        }
////                    }
////                }
//                oToken = oManagement.getOtpObjByUserId(sessionid, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE);
//                String secret = oToken.getSecret();
//
////                if (cert == null) {
////                    aStatus.error = "Digital Certificate Issuance Failed";
////                    aStatus.errorCode = -22;
////                    aStatus.data = signData;
////                    return aStatus;
////
////                }
//                MobileTrustManagment Managment1 = new MobileTrustManagment();
//
//                TrustDeviceManagement trustMgmt = new TrustDeviceManagement();
//                Trusteddevice device = null;
//                if (did != null) {
//                    device = trustMgmt.getTrusteddeviceByDeviceId(sessionid, channelid, userid, did);
//
//                }
//
//                if (device == null) {
//                    aStatus.error = "Failed to get user wallet(Device Not Found!!)";
//                    log.error(String.format("##GetUserCredentialWallet :  - exception caught when calling GetUserCredentialWallet - ", aStatus.error));
//                    aStatus.errorCode = -152;
//                    return aStatus;
//                }
//
//                signData = Managment1.getWalletDetails(sessionid, session.getChannelid(), userid, device.getDeviceid(), secret, cert, "", visibleKey, serverPrivateKey, 2, hiddenKey,oToken.getSrno());
//
//                if (signData == null) {
//                    aStatus.error = "Failed to get user wallet";
//                    log.error(String.format("##GetUserCredentialWallet :  - exception caught when calling GetUserCredentialWallet - ", aStatus.error));
//                    aStatus.errorCode = -67;
//                    return aStatus;
//                } else {
//                    aStatus.error = "Success";
//                    aStatus.errorCode = 0;
//                    aStatus.data = signData;
//                }
//
//            } catch (Exception ex) {
//                log.error(String.format("##GetUserCredentialWallet :  - exception caught when calling GetUserCredentialWallet - ", ex.getMessage()));
//                ex.printStackTrace();
//            }
//        }
//
//        return aStatus;
//
//    }
//
//    @Override
//    public AxiomStatus UpdateUserCredentialWallet(String sessionid, String userid, int type, String clientPayload) {
//        String strDebug = null;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                log.info("UpdateUserCredentialWallet::sessionid::" + sessionid);
//                log.info("UpdateUserCredentialWallet::userid::" + userid);
//                log.info("UpdateUserCredentialWallet::data::" + clientPayload);
//                log.info("UpdateUserCredentialWallet::type::" + type);
//            }
//        } catch (Exception ex) {
//            log.error(String.format("##UpdateUserCredentialWallet :  - exception caught when calling UpdateUserCredentialWallet - ", ex.getMessage()));
//        }
//
////        int iResult = AxiomProtect.ValidateLicense();
////        if (iResult != 0) {
////            AxiomStatus aStatus = new AxiomStatus();
////            aStatus.error = "Licence Invalid";
////            aStatus.errorcode = -100;
////            return aStatus;
////        }
//        //OTPTokenManagement oManagement = new OTPTokenManagement();
//        CryptoManager crypto = new CryptoManager();
//        SessionManagement sManagement = new SessionManagement();
//        AuditManagement audit = new AuditManagement();
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        Sessions session = sManagement.getSessionById(sessionid);
//        AxiomStatus aStatus = new AxiomStatus();
//        int retValue = -1;
//        aStatus.error = "ERROR";
//        aStatus.errorcode = retValue;
//        if (session == null) {
//            aStatus.error = "Invalid Session";
//            log.error(String.format("##UpdateUserCredentialWallet :  - exception caught when calling UpdateUserCredentialWallet - ", aStatus.error));
//            aStatus.errorcode = -14;
//            return aStatus;
//        }
//        ChannelManagement cManagement = new ChannelManagement();
//        Channels channel = cManagement.getChannelByID(session.getChannelid());
//        if (channel == null) {
//            aStatus.error = "Invalid Channel";
//            log.error(String.format("##UpdateUserCredentialWallet :  - exception caught when calling UpdateUserCredentialWallet - ", aStatus.error));
//            aStatus.errorcode = -3;
//            return aStatus;
//        }
//        if (session != null) {
//
//            //System.out.println("Client IP = " + req.getRemoteAddr());        
//            SettingsManagement setManagement = new SettingsManagement();
////            int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
////            if (result != 1) {
////                aStatus.error = "INVALID IP REQUEST";
////                aStatus.errorcode = -8;
////                return aStatus;
////            }
//
//            String channelid = session.getChannelid();
//
//            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//            if (ipobj != null) {
//                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                int checkIp = 1;
//                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                    log.debug("CheckIpd" + checkIp);
//                } else {
//                    checkIp = 1;
//                }
//                if (iObj.ipstatus == 0 && checkIp != 1) {
//                    if (iObj.ipalertstatus == 0) {
//                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                        Session sTemplate = suTemplate.openSession();
//                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                        OperatorsManagement oManagement = new OperatorsManagement();
//                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                        if (aOperator != null) {
//                            String[] emailList = new String[aOperator.length - 1];
//                            for (int i = 1; i < aOperator.length; i++) {
//                                emailList[i - 1] = aOperator[i].getEmailid();
//                            }
//                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                String strsubject = templatesObj.getSubject();
//                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                if (strmessageBody != null) {
//                                    // Date date = new Date();
//                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                }
//
//                                SendNotification send = new SendNotification();
//                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
//                            aStatus.error = "INVALID IP";
//                            log.error(String.format("##UpdateUserCredentialWallet :  - exception caught when calling UpdateUserCredentialWallet - ", aStatus.error));
//                            aStatus.errorcode = -8;
//                            return aStatus;
//                        }
//
//                        suTemplate.close();
//                        sTemplate.close();
//                        aStatus.error = "INVALID IP";
//                        log.error(String.format("##UpdateUserCredentialWallet :  - exception caught when calling UpdateUserCredentialWallet - ", aStatus.error));
//                        aStatus.errorcode = -8;
//                        return aStatus;
//                    }
//                    aStatus.error = "INVALID IP";
//                    log.error(String.format("##UpdateUserCredentialWallet :  - exception caught when calling UpdateUserCredentialWallet - ", aStatus.error));
//                    aStatus.errorcode = -8;
//                    return aStatus;
//                }
//            }
//            int devicetype = 1;
//            JSONObject jsonObj = null;
//            try {
//                jsonObj = new JSONObject(clientPayload);
//                String device = jsonObj.getString("_deviceType");
//                if (device.equals("ANDROID")) {
//                    devicetype = ANDROID;
//                } else if (device.equals("IOS")) {
//                    devicetype = IOS;
//                }
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//            MobileTrustManagment mManagment = new MobileTrustManagment();
//            AXIOMStatus aXStatus = mManagment.CheckPlusComponentTOReset(channel, userid, sessionid, type, clientPayload, devicetype);
//            retValue = aXStatus.iStatus;
//            if (retValue == 0) {
//                aStatus.error = aXStatus.strStatus;
//                aStatus.errorcode = aXStatus.iStatus;
//
//            } else {
//                aStatus.errorcode = retValue;
//                if (aStatus.errorcode == -14) {
//
//                    aStatus.errorcode = -42;
//                    aStatus.error = "Mobile Trust Setting is not configured!!!";
//                    log.error(String.format("##UpdateUserCredentialWallet :  - exception caught when calling UpdateUserCredentialWallet - ", aStatus.error));
//
//                } else if (aStatus.errorcode == -6) {
//                    aStatus.error = "Trusted Device not found!!!";
//                    log.error(String.format("##UpdateUserCredentialWallet :  - exception caught when calling UpdateUserCredentialWallet - ", aStatus.error));
//                } else if (aStatus.errorcode == -17) {
//                    aStatus.error = "Trusted Device is marked suspended";
//                    log.error(String.format("##UpdateUserCredentialWallet :  - exception caught when calling UpdateUserCredentialWallet - ", aStatus.error));
//                } else if (aStatus.errorcode == -5) {
//                    aStatus.error = "Trust Device mismatch!!!";
//                    log.error(String.format("##UpdateUserCredentialWallet :  - exception caught when calling UpdateUserCredentialWallet - ", aStatus.error));
//                } else if (aStatus.errorcode == -4) {
//                    aStatus.error = "Verification failed!!!";
//                    log.error(String.format("##UpdateUserCredentialWallet :  - exception caught when calling UpdateUserCredentialWallet - ", aStatus.error));
//                } else if (aStatus.errorcode == -42) {
//                    aStatus.error = "Digtial Signature mismatch!!!";
//
//                } else if (aStatus.errorcode == -7) {
//                    aStatus.error = "Home Country is not defined!!!";
//                    log.error(String.format("##UpdateUserCredentialWallet :  - exception caught when calling UpdateUserCredentialWallet - ", aStatus.error));
//                } else if (aStatus.errorcode == -28) {
//                    aStatus.error = "TimeStamp is not issued!!";
//                    log.error(String.format("##UpdateUserCredentialWallet :  - exception caught when calling UpdateUserCredentialWallet - ", aStatus.error));
//                } else if (aStatus.errorcode == -27) {
//                    aStatus.error = "TimeStamp mistmatch!!!";
//                    log.error(String.format("##UpdateUserCredentialWallet :  - exception caught when calling UpdateUserCredentialWallet - ", aStatus.error));
//                } else if (aStatus.errorcode == -29) {
//                    aStatus.error = "TimeStamp has expired!!!";
//                    log.error(String.format("##UpdateUserCredentialWallet :  - exception caught when calling UpdateUserCredentialWallet - ", aStatus.error));
//                } else if (aStatus.errorcode == -10) {
//                    aStatus.error = "Invalid Location, out of home country!!!";
//                    log.error(String.format("##UpdateUserCredentialWallet :  - exception caught when calling UpdateUserCredentialWallet - ", aStatus.error));
//                } else if (aStatus.errorcode == -11) {
//                    aStatus.error = "Invalid Location, roaming country is not allowed!!!";
//                    log.error(String.format("##UpdateUserCredentialWallet :  - exception caught when calling UpdateUserCredentialWallet - ", aStatus.error));
//                } else if (aStatus.errorcode == -12) {
//                    aStatus.error = "Invalid Location, roaming is not configured properly!!!";
//                    log.error(String.format("##UpdateUserCredentialWallet :  - exception caught when calling UpdateUserCredentialWallet - ", aStatus.error));
//                } else {
//                    aStatus.errorcode = aXStatus.iStatus;
//                    aStatus.error = aXStatus.strStatus;
//                }
//            }
//
//        }
//        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                session.getLoginid(), session.getLoginid(), new Date(), "Verify Digital Signature ", aStatus.error, aStatus.errorcode,
//                "Verify Digital Signature", "Signature = ******", " Signature = ******",
//                itemtype, userid);
//        log.debug("UpdateUserCredentialWallet exited");
//        return aStatus;
//    }
//
//    @Override
//    public AxiomStatus DeleteUser(String sessionId, String userid) {
//        try {
//
////            int iResult = AxiomProtect.ValidateLicense();
////            if (iResult != 0) {
////                AxiomStatus aStatus = new AxiomStatus();
////                aStatus.error = "Licence Invalid";
////                aStatus.errorcode = -100;
////                return aStatus;
//////                AxiomData aData = new AxiomData();
//////                aData.sErrorMessage = "Licence is invalid";
//////                aData.iErrorCode = -100;
//////                return aData;                
////                //throw new AxiomException("Licence is invalid");
////            }
//            String strDebug = null;
//            try {
//                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                    System.out.println("DeleteUser::sessionId::" + sessionId);
//                    System.out.println("DeleteUser::userid::" + userid);
//
//                }
//            } catch (Exception ex) {
//            }
//
////            int iResult = AxiomProtect.ValidateLicense();
////            if (iResult != 0) {
////                AxiomStatus aStatus = new AxiomStatus();
////                aStatus.error = "Licence is invalid";
////                aStatus.errorcode = -100;
////                return aStatus;
//////                AxiomData aData = new AxiomData();
//////                aData.sErrorMessage = "Licence is invalid";
//////                aData.iErrorCode = -100;
//////                return aData;                
////                //throw new AxiomException("Licence is invalid");
////            }
//            SessionManagement sManagement = new SessionManagement();
//            AuditManagement audit = new AuditManagement();
//            MessageContext mc = wsContext.getMessageContext();
//            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//            Sessions session = sManagement.getSessionById(sessionId);
//            if (session == null) {
//                AxiomStatus aStatus = new AxiomStatus();
//                aStatus.error = "Invalid Session";
//                aStatus.errorcode = -14;
//                return aStatus;
//                //throw  new AxiomException("");
//            }
//
//            UserManagement uManagement = new UserManagement();
////            SessionManagement sManagement = new SessionManagement();
////            MessageContext mc = wsContext.getMessageContext();
////            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
////            Sessions session = sManagement.getSessionById(sessionId);
//            AxiomStatus aStatus = new AxiomStatus();
//            int retValue = -1;
//            aStatus.error = "ERROR";
//            aStatus.errorcode = retValue;
//            AuthUser oldUser = null;
//            if (session != null) {
//
//                //System.out.println("Client IP = " + req.getRemoteAddr());        
//                SettingsManagement setManagement = new SettingsManagement();
//                String channelid = session.getChannelid();
//                ChannelManagement cManagement = new ChannelManagement();
//                Channels channel = cManagement.getChannelByID(channelid);
//                if (channel == null) {
//                    aStatus.error = "Invalid Channel";
//                    aStatus.errorcode = -3;
//                    return aStatus;
//                }
//                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//                if (ipobj != null) {
//                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                    int checkIp = 1;
//                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                    } else {
//                        checkIp = 1;
//                    }
//                    if (iObj.ipstatus == 0 && checkIp != 1) {
//                        if (iObj.ipalertstatus == 0) {
//                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                            Session sTemplate = suTemplate.openSession();
//                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                            OperatorsManagement oManagement = new OperatorsManagement();
//                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                            if (aOperator != null) {
//                                String[] emailList = new String[aOperator.length - 1];
//                                for (int i = 1; i < aOperator.length; i++) {
//                                    emailList[i - 1] = aOperator[i].getEmailid();
//                                }
//                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                    String strsubject = templatesObj.getSubject();
//                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                    if (strmessageBody != null) {
//                                        // Date date = new Date();
//                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                    }
//
//                                    SendNotification send = new SendNotification();
//                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                                }
//
//                                suTemplate.close();
//                                sTemplate.close();
//                                aStatus.error = "INVALID IP";
//                                aStatus.errorcode = -8;
//                                return aStatus;
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
//                            aStatus.error = "INVALID IP";
//                            aStatus.errorcode = -8;
//                            return aStatus;
//                        }
//                        aStatus.error = "INVALID IP";
//                        aStatus.errorcode = -8;
//                        return aStatus;
//                    }
//                }
//
//                retValue = uManagement.DeleteUser(sessionId, session.getChannelid(), userid);
//                if (retValue == 0) {
//                    aStatus.error = "SUCCESS";
//                    aStatus.errorcode = 0;
//                }
//                //return aStatus;
//                oldUser = uManagement.getUser(sessionId, session.getChannelid(), userid);
//            }
//            String oldUserName = "";
//            String oldPhoneNo = "";
//            String oldEmailID = "";
//            String oldPasswordState = "";
//
//            if (oldUser != null) {
//                oldUserName = oldUser.getUserName();
//                oldPhoneNo = oldUser.getPhoneNo();
//                oldEmailID = oldUser.getEmail();
//                oldPasswordState = "" + oldUser.getStatePassword();
//            }
//
//            String itemtype = "USERPASSWORD";
//            ChannelManagement cManagement = new ChannelManagement();
//            Channels channel = cManagement.getChannelByID(session.getChannelid());
////            AuditManagement audit = new AuditManagement();
//            if (retValue == 0) {
//                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                        session.getLoginid(), session.getLoginid(), new Date(), "Delete User", aStatus.error, aStatus.errorcode,
//                        "User Management", "User Name = " + oldUserName + "User Phone =" + oldPhoneNo + "User Email =" + oldEmailID + "State Of Password =" + oldPasswordState,
//                        "User Deleted Successfully",
//                        itemtype, "");
//                return aStatus;
//            } else {
//                String reason = "User Deletion Failed";
//                if (retValue == -10) {
//                    reason += "User is assigned token/certificate, therefore cannot be removed!!!";
//                    aStatus.error = reason;
//                    aStatus.errorcode = -68;
//                } else {
//                    reason += "!!!";
//                    aStatus.error = reason;
//                    aStatus.errorcode = -69;
//                }
//                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                        session.getLoginid(), session.getLoginid(), new Date(), "Delete User", aStatus.error, aStatus.errorcode,
//                        "User Management", "User Name = " + oldUserName + "User Phone =" + oldPhoneNo + "User Email =" + oldEmailID + "State Of Password =" + oldPasswordState,
//                        reason,
//                        itemtype, "");
//
//                return aStatus;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "General Exception::" + e.getMessage();
//            aStatus.errorcode = -99;
//            return aStatus;
//
//        }
//    }
//
//}
