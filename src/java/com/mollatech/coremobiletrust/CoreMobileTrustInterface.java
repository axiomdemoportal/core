/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mollatech.coremobiletrust;

import com.mollatech.axiom.v2.core.utils.AxiomData;
import com.mollatech.axiom.v2.core.utils.AxiomException;
import com.mollatech.axiom.v2.core.utils.AxiomStatus;
import com.mollatech.axiom.v2.core.utils.AxiomUser;
import com.mollatech.axiom.v2.core.utils.AxiomUserCerdentials;
import com.mollatech.axiom.v2.core.utils.DocumentSigner;
import com.mollatech.axiom.v2.core.utils.MobileTrustStatus;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author Manoj Sherkhane
 */



@WebService
public interface CoreMobileTrustInterface {
    
        public int GETUSER_BY_USERID = 1;
    public int GETUSER_BY_PHONENUMBER = 2;
    public int GETUSER_BY_EMAILID = 3;
    public int GETUSER_BY_FULLNAME = 4;

    public int RESET_USER_PASSWORD = 1;
    public int RESET_USER_TOKEN_OOB = 2;
    public int RESET_USER_TOKEN_SOFTWARE = 3;
    public int RESET_USER_TOKEN_HARDWARE = 4;

    public int OTP_TOKEN_SOFTWARE = 1;
    public int OTP_TOKEN_SOFTWARE_WEB = 1;
    public int OTP_TOKEN_SOFTWARE_MOBILE = 2;
    public int OTP_TOKEN_SOFTWARE_PC = 3;

    public int TOKEN_STATUS_UNASSIGNED = -10;
    public int TOKEN_STATUS_ACTIVE = 1;
    public int TOKEN_STATUS_SUSPENDED = -2;
    public int TOKEN_STATUS_PENDING = 0;

    public String strACTIVE = "ACTIVE";
    public String strSUSPENDED = "SUSPENDED";
    public String strPENDING = "PENDING";
    public String strUNASSIGNED = "UNASSIGNED";

    public int WRONG_ATTEMPT_DESTROY = 1;
    public int INTENTIONALLY_DESTROY = 2;
    public int FORCE_DESTROY = 3;

    public String USER_ASSIGN_PASSWORD_TEMPLATE_NAME = "User Password";

    public int SEND_MESSAGE_PENDING_STATE = 2;
    public String SEND_MESSAGE_PENDING_STATE_STRING = "PENDING";

    public int SEND_MESSAGE_BLOCKED_STATE = -5;
    public String SEND_MESSAGE_BLOCKED_STATE_STRING = "BLOCKED";
    public int SEND_MESSAGE_INVALID_DATA = -4;
    public String SEND_MESSAGE_INVALID_DATA_STRING = "INVALIDDATA";
    public final int LOGIN = 2;
    // AUTHENTICATE = 1
    public final int TRANSACTION = 3;
 // AUTHORIZE = 2
    
    
    public int RESET_USER_MOBILETRUST = 5;
    public int OTP_TOKEN_OUTOFBAND_SMS = 1;
    public int OTP_TOKEN_OUTOFBAND_VOICE = 2;
    public int OTP_TOKEN_OUTOFBAND_USSD = 3;
    public int OTP_TOKEN_OUTOFBAND_EMAIL = 4;
    public int OTP_TOKEN_HARDWARE_MINI = 1;
    public int OTP_TOKEN_HARDWARE_CR = 2;

    
    
      @WebMethod
    public String OpenSession(@WebParam(name = "channelid") String channelid,@WebParam(name = "loginid") String loginid, @WebParam(name = "loginpassword") String loginpassword) throws AxiomException ;

    @WebMethod
    public AxiomStatus CloseSession( @WebParam(name = "sessionid")  String sessionid);

    @WebMethod
    public AxiomStatus CreateAPUser(@WebParam(name = "sessionid")  String sessionid,@WebParam(name = "aUser")AxiomUser aUser);// needs to change in implementation

    @WebMethod
    public AxiomUserCerdentials GetAPUserBy(@WebParam(name = "sessionid")  String sessionid, @WebParam(name = "type") int type, @WebParam(name = "searchFor")String searchFor) throws AxiomException;// needs to change in implementation

    @WebMethod
    public AxiomStatus ResetAPUser(@WebParam(name = "sessionid")  String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type);

    @WebMethod
    public AxiomStatus AssignMobileTrust(@WebParam(name = "sessionid")  String sessionid,  @WebParam(name = "userid") String userid);//soft Mobile otp,soft mobile pki,certificate

    @WebMethod
    public MobileTrustStatus ActivateMobileTrust(@WebParam(name = "sessionid")  String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "clientPayload") String clientPayload);// needs to change in implementation

    @WebMethod
    public String GetTimeStamp(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid) throws AxiomException ; // client encrypt(server sign(timestamp, expiry time, session id,channel id, client hardware profile))
    @WebMethod
    public AxiomStatus VerifyOTPPlus(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type, @WebParam(name = "otp") String otp, @WebParam(name = "otpPlus") String otpPlus);

    @WebMethod
    public AxiomStatus VerifySignatureOTPPlus(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type, @WebParam(name = "data")  String[] data, @WebParam(name = "sotp")  String sotp, @WebParam(name = "sotpPlus")  String sotpPlus);
    
    @WebMethod
    public AxiomStatus VerifyDigitalSignaturePlus(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "data") String data, @WebParam(name = "type") int type, @WebParam(name = "signature") String signature, @WebParam(name = "signaturePlus") String signaturePlus);

    @WebMethod
    public AxiomData VerifyCredentialAndSignDocuments(@WebParam(name = "sessionid")  String sessionid, @WebParam(name = "userid") String userid,@WebParam(name = "clientPayload") String clientPayload,@WebParam(name = "signature") String signature,@WebParam(name = "dSigner")DocumentSigner dSigner,@WebParam(name = "bNotifyUser") boolean bNotifyUser);// needs to change in implementation // newly added

    @WebMethod
    public AxiomStatus ALertMTEvent( @WebParam(name = "sessionid")  String sessionid,@WebParam(name = "signData")   String signData);
        
    @WebMethod
    public AxiomStatus ChangeUserDetailsPlus(@WebParam(name = "sessionid")  String sessionid,@WebParam(name = "aUser") AxiomUser aUser);// needs to change in implementation // newly added

    @WebMethod 
    public AxiomStatus ChangeUserCredentials(@WebParam(name="sessionid") String sessionid,@WebParam(name="AxiomUserCredential") AxiomUserCerdentials credentials,@WebParam(name = "integrityCheckString")String integrityCheckString, @WebParam(name = "trustPayLoad")String trustPayLoad);
    
//    @WebMethod
//    public AxiomStatus UpdateUserCredentialWallet(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type,  @WebParam(name = "clientPayload") String clientPayload);
    
    @WebMethod
    public AxiomUserCerdentials[] GetAllUsers(@WebParam(name = "sessionid")  String sessionid, @WebParam(name = "type") int type, @WebParam(name = "searchFor")String[] searchFor);

    @WebMethod
    public MobileTrustStatus GetUserCredentialWallet(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid,@WebParam(name = "devid") String did);
    
    @WebMethod
    public AxiomStatus UpdateUserCredentialWallet(@WebParam(name = "sessionid") String sessionid, @WebParam(name = "userid") String userid, @WebParam(name = "type") int type,  @WebParam(name = "clientPayload") String clientPayload);
    
     @WebMethod
    public AxiomStatus DeleteUser(@WebParam(name = "sessionid")  String sessionid, @WebParam(name = "userid") String userid);
    
    
    
}
