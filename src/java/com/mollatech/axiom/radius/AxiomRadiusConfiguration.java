/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.radius;

public class AxiomRadiusConfiguration {
    public boolean accountEnabled;
    public String authIp;
    public int authPort;
    public boolean authEnabled;
    public boolean ldapValidate;
    public boolean axiomValidate;
    public String ldapSearchPath;
    public String accountIp;
    public int accountPort;
    public String ldapServerIp;
    public int ldapServerPort;
    public String ldapServerUsername;
    public String ldapServerPassword;
    public AxiomRadiusClient[] radiusClient;
    public String sessionId;
     public String statusTimeinterval;
     public String ldapSearchInitial;
}
