/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.mollatech.axiom.common.utils.UtilityFunctions
 *  com.mollatech.axiom.connector.communication.AXIOMStatus
 *  com.mollatech.axiom.connector.communication.RemoteSigningInfo
 *  com.mollatech.axiom.connector.epin.QuestionsAndAnswers
 *  com.mollatech.axiom.connector.user.AuthUser
 *  com.mollatech.axiom.connector.user.AxiomChallengeResponse
 *  com.mollatech.axiom.connector.user.AxiomQuestionAndAnswer
 *  com.mollatech.axiom.connector.user.TokenStatusDetails
 *  com.mollatech.axiom.nucleus.crypto.ChannelProfile
 *  com.mollatech.axiom.nucleus.crypto.LoadSettings
 *  com.mollatech.axiom.nucleus.db.Bulkmsfg
 *  com.mollatech.axiom.nucleus.db.Certificates
 *  com.mollatech.axiom.nucleus.db.Channellogs
 *  com.mollatech.axiom.nucleus.db.Channels
 *  com.mollatech.axiom.nucleus.db.Operators
 *  com.mollatech.axiom.nucleus.db.Otptokens
 *  com.mollatech.axiom.nucleus.db.Pkitokens
 *  com.mollatech.axiom.nucleus.db.Questionsandanswers
 *  com.mollatech.axiom.nucleus.db.Remoteaccess
 *  com.mollatech.axiom.nucleus.db.Sessions
 *  com.mollatech.axiom.nucleus.db.Templates
 *  com.mollatech.axiom.nucleus.db.connector.OtpTokensUtils
 *  com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils
 *  com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil
 *  com.mollatech.axiom.nucleus.db.connector.SettingsUtil
 *  com.mollatech.axiom.nucleus.db.connector.TemplateUtils
 *  com.mollatech.axiom.nucleus.db.connector.management.AuditManagement
 *  com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement
 *  com.mollatech.axiom.nucleus.db.connector.management.ChallengeResponsemanagement
 *  com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement
 *  com.mollatech.axiom.nucleus.db.connector.management.CryptoManagement
 *  com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement
 *  com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement
 *  com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement
 *  com.mollatech.axiom.nucleus.db.connector.management.QuestionsManagement
 *  com.mollatech.axiom.nucleus.db.connector.management.RegistrationCodeTrailManagement
 *  com.mollatech.axiom.nucleus.db.connector.management.SessionManagement
 *  com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement
 *  com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement
 *  com.mollatech.axiom.nucleus.db.connector.management.UserManagement
 *  com.mollatech.axiom.nucleus.db.operation.AxiomMessage
 *  com.mollatech.axiom.nucleus.db.operation.AxiomQuestionsAndAnswers
 *  com.mollatech.axiom.nucleus.settings.ConnectorStatusInternal
 *  com.mollatech.axiom.nucleus.settings.GlobalChannelSettings
 *  com.mollatech.axiom.nucleus.settings.PasswordPolicySetting
 *  com.mollatech.axiom.nucleus.settings.SendNotification
 *  com.mollatech.axiom.nucleus.settings.TokenSettings
 *  com.mollatech.axiom.nucleus.settings.emailsetting
 *  com.mollatech.axiom.nucleus.settings.smssetting
 *  com.mollatech.axiom.nucleus.settings.ussdsetting
 *  com.mollatech.axiom.nucleus.settings.voicesetting
 *  com.mollatech.axiom.v2.core.AxiomCoreInterface
 *  com.mollatech.axiom.v2.core.ConnectorStatusListener
 *  com.mollatech.axiom.v2.core.utils.AxiomData
 *  com.mollatech.axiom.v2.core.utils.AxiomException
 *  com.mollatech.axiom.v2.core.utils.AxiomMessage
 *  com.mollatech.axiom.v2.core.utils.AxiomStatus
 *  com.mollatech.axiom.v2.core.utils.AxiomTokenDetails
 *  com.mollatech.axiom.v2.core.utils.AxiomUser
 *  com.mollatech.axiom.v2.core.utils.ConnectorStatus
 *  com.mollatech.axiom.v2.core.utils.MessageReport
 *  com.mollatech.dictum.management.BulkMSGManagement
 *  javax.servlet.http.HttpServletRequest
 *  javax.servlet.http.HttpSession
 *  org.apache.log4j.Logger
 *  org.bouncycastle.util.encoders.Base64
 *  org.hibernate.Session
 */
package com.mollatech.axiom.v2.core;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.communication.RemoteSigningInfo;
import com.mollatech.axiom.connector.epin.QuestionsAndAnswers;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.connector.user.AxiomChallengeResponse;
import com.mollatech.axiom.connector.user.AxiomQuestionAndAnswer;
import com.mollatech.axiom.connector.user.TokenStatusDetails;
import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Bulkmsg;
import com.mollatech.axiom.nucleus.db.Certificates;
import com.mollatech.axiom.nucleus.db.Channellogs;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Otptokens;
import com.mollatech.axiom.nucleus.db.Pkitokens;
import com.mollatech.axiom.nucleus.db.Questionsandanswers;
import com.mollatech.axiom.nucleus.db.Remoteaccess;
import com.mollatech.axiom.nucleus.db.Sessions;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.OtpTokensUtils;
import com.mollatech.axiom.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.SettingsUtil;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ChallengeResponsemanagement;
import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
import com.mollatech.axiom.nucleus.db.connector.management.CryptoManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement;
import com.mollatech.axiom.nucleus.db.connector.management.QuestionsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.RegistrationCodeTrailManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.db.operation.AxiomQuestionsAndAnswers;
import com.mollatech.axiom.nucleus.settings.ConnectorStatusInternal;
import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
import com.mollatech.axiom.nucleus.settings.PasswordPolicySetting;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import com.mollatech.axiom.nucleus.settings.TokenSettings;
import com.mollatech.axiom.nucleus.settings.emailsetting;
import com.mollatech.axiom.nucleus.settings.smssetting;
import com.mollatech.axiom.nucleus.settings.ussdsetting;
import com.mollatech.axiom.nucleus.settings.voicesetting;
import com.mollatech.axiom.v2.core.AxiomCoreInterface;
import com.mollatech.axiom.v2.core.ConnectorStatusListener;
import com.mollatech.axiom.v2.core.utils.AxiomData;
import com.mollatech.axiom.v2.core.utils.AxiomException;
import com.mollatech.axiom.v2.core.utils.AxiomMessage;
import com.mollatech.axiom.v2.core.utils.AxiomStatus;
import com.mollatech.axiom.v2.core.utils.AxiomTokenDetails;
import com.mollatech.axiom.v2.core.utils.AxiomUser;
import com.mollatech.axiom.v2.core.utils.ConnectorStatus;
import com.mollatech.axiom.v2.core.utils.MessageReport;
import com.mollatech.dictum.management.BulkMSGManagement;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.security.MessageDigest;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import org.apache.log4j.Logger;
import org.bouncycastle.util.encoders.Base64;
import org.hibernate.Session;

@WebService(endpointInterface = "com.mollatech.axiom.v2.core.AxiomCoreInterface")
public class AxiomCoreInterfaceImpl
        implements AxiomCoreInterface {

    static Logger log = Logger.getLogger((String) AxiomCoreInterfaceImpl.class.getName());
    @Resource
    WebServiceContext wsContext;
    final String itemtype = "MOBILETRUST";
    private static final int ANDROID = 1;
    private static final int IOS = 2;
    private int GETUSER_BY_USERID = 1;
    private int GETUSER_BY_PHONENUMBER = 2;
    private int GETUSER_BY_EMAILID = 3;
    private int GETUSER_BY_FULLNAME = 4;
    private int RESET_USER_PASSWORD = 1;
    private int RESET_USER_TOKEN_OOB = 2;
    private int RESET_USER_TOKEN_SOFTWARE = 3;
    private int RESET_USER_TOKEN_HARDWARE = 4;
    private int RESET_USER_CERT = 5;
    private int RESET_USER_PKI_TOKEN_SOFTWARE = 6;
    private int RESET_USER_PKI_TOKEN_HARDWARE = 7;
    private int OTP_TOKEN_OUTOFBAND = 3;
    public int OTP_TOKEN_SOFTWARE = 1;
    private int OTP_TOKEN_HARDWARE = 2;
    private int OTP_TOKEN_OUTOFBAND_SMS = 1;
    private int OTP_TOKEN_OUTOFBAND_VOICE = 2;
    private int OTP_TOKEN_OUTOFBAND_USSD = 3;
    private int OTP_TOKEN_OUTOFBAND_EMAIL = 4;
    private int OTP_TOKEN_SOFTWARE_MOBILE = 2;
    private int OTP_TOKEN_SOFTWARE_PC = 3;
    private int OTP_TOKEN_SOFTWARE_WEB = 1;
    private int OTP_TOKEN_HARDWARE_MINI = 1;
    private int OTP_TOKEN_HARDWARE_CR = 2;
    private int TOKEN_STATUS_UNASSIGNED = -10;
    private int TOKEN_STATUS_ACTIVE = 1;
    private int TOKEN_STATUS_SUSPENDED = -2;
    private String strACTIVE = "ACTIVE";
    private String strSUSPENDED = "SUSPENDED";
    private String strUNASSIGNED = "UNASSIGNED";
    private int SEND_MESSAGE_PENDING_STATE = 2;
    private String SEND_MESSAGE_PENDING_STATE_STRING = "PENDING";
    private int SEND_MESSAGE_BLOCKED_STATE = -5;
    private String SEND_MESSAGE_BLOCKED_STATE_STRING = "BLOCKED";
    private int SEND_MESSAGE_INVALID_DATA = -4;
    private String SEND_MESSAGE_INVALID_DATA_STRING = "INVALIDDATA";
    private int INVALID_IP = -3;
    private int INVALID_REMOTEUSER = -4;
    private int INVALID_LICENSE = -5;
    private int EPINSMS = 1;
    private int EPINIVR = 2;
    public static final int OTP_TOKEN = 1;
    public static final int PKI_TOKEN = 2;

    public String OpenSession(String channelid, String loginid, String loginpassword) {
        log.debug("Started");
        try {

            String strDebug = null;
            SettingsManagement setManagement = new SettingsManagement();
            try {
                ChannelProfile channelprofileObj = null;
                Object channelpobj = setManagement.getSettingInner(channelid, 20, 1);
                if (channelpobj == null) {
                    LoadSettings.LoadChannelProfile((ChannelProfile) channelprofileObj);
                } else {
                    channelprofileObj = (ChannelProfile) channelpobj;
                    LoadSettings.LoadChannelProfile((ChannelProfile) channelprofileObj);
                }
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    log.debug((Object) ("OpenSession::loginpassword::" + loginpassword));
                    log.debug((Object) ("OpenSession::channelid::" + channelid));
                    log.debug((Object) ("OpenSession::loginid::" + loginid));
                }
            } catch (Exception channelprofileObj) {
                // empty catch block
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(channelid);
            if (channel == null) {
                return null;
            }
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
//            Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
//            if (ipobj != null) {
//                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                int checkIp = 1;
//                checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
//                if (iObj.ipstatus == 0 && checkIp != 1) {
//                    if (iObj.ipalertstatus == 0) {
//                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
//                        Session sTemplate = suTemplate.openSession();
//                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                        Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
//                        SessionFactoryUtil suOperators = new SessionFactoryUtil(3);
//                        OperatorsManagement oManagement = new OperatorsManagement();
//                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                        if (aOperator != null) {
//                            String[] emailList = new String[aOperator.length - 1];
//                            for (int i = 1; i < aOperator.length; ++i) {
//                                emailList[i - 1] = aOperator[i].getEmailid();
//                            }
//                            if (templatesObj.getStatus() == 1) {
//                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
//                                String strsubject = templatesObj.getSubject();
//                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                if (strmessageBody != null) {
//                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                }
//                                SendNotification send = new SendNotification();
//                                AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                            }
//                            suTemplate.close();
//                            sTemplate.close();
//                            return null;
//                        }
//                        suTemplate.close();
//                        sTemplate.close();
//                        return null;
//                    }
//                    return null;
//                }
//            }
            String resultStr = "Failure";
            int retValue = -1;
            log.debug("OpenSession :: OpenSessionForWS() called");
            SessionManagement sManagement = new SessionManagement();
            String sessionId = sManagement.OpenSessionForWS(channelid, loginid, loginpassword, req.getSession().getId());
             log.debug("sessionId"+sessionId);
            log.debug("OpenSession :: OpenSessionForWS() ended");
            AuditManagement audit = new AuditManagement();
            if (sessionId != null) {
                retValue = 0;
                resultStr = "Success";

                audit.AddAuditTrail(sessionId, channelid, loginid, req.getRemoteAddr(), channel.getName(), loginid, loginid, new Date(), "Open Session", resultStr, retValue, "Login", "", "Open Session successfully with Session Id =" + sessionId, "SESSION", loginid);
            } else if (sessionId == null) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), loginid, req.getRemoteAddr(), channel.getName(), loginid, loginid, new Date(), "Open Session", resultStr, retValue, "Login", "", "Failed To Open Session", "SESSION", loginid);
            }
            log.debug("Ended");
            return sessionId;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public AxiomStatus CloseSession(String sessionid) {
        try {
            String strDebug = null;
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    log.debug((Object) ("CloseSession::sessionid::" + sessionid));
                }
            } catch (Exception var3_4) {
                // empty catch block
            }
            SessionManagement sManagement = new SessionManagement();
            Sessions session = sManagement.getSessionById(sessionid);
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                ChannelManagement cManagement = new ChannelManagement();
                String channelid = session.getChannelid();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    return null;
                }
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }
                log.debug("CloseSession :: CloseSession() called");
                int retValue = sManagement.CloseSession(sessionid);
                log.debug("CloseSession :: CloseSession() ended");
                aStatus.error = "ERROR";
                aStatus.errorcode = retValue;
                if (retValue == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                }
            } else if (session == null) {
                return aStatus;
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                return aStatus;
            }
            AuditManagement audit = new AuditManagement();
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "Close Session", aStatus.error, aStatus.errorcode, "Login", "", "Close Session successfully with Session Id =" + sessionid, "SESSION", session.getLoginid());
            return aStatus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public AxiomStatus CreateUser(String sessionId, String fullname, String phone, String email, String userid) {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug((Object) ("CreateUser::sessionId::" + sessionId));
                log.debug((Object) ("CreateUser::fullname::" + fullname));
                log.debug((Object) ("CreateUser::phone::" + phone));
                log.debug((Object) ("CreateUser::email::" + email));
                log.debug((Object) ("CreateUser::userid::" + userid));
            }
        } catch (Exception var7_7) {
            // empty catch block
        }
        try {
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionId);
            AxiomStatus aStatus = new AxiomStatus();
            if (session == null) {
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                return aStatus;
            }
            if (sessionId == null || fullname == null || phone == null || sessionId.isEmpty() || fullname.isEmpty() || phone.isEmpty() || email == null || email.isEmpty()) {
                aStatus.error = "Invalid Data";
                aStatus.errorcode = -11;
                return aStatus;
            }
            if (userid != null && userid.isEmpty()) {
                aStatus.error = "Setting userid is not supported!!!";
                aStatus.errorcode = -12;
                return aStatus;
            }
            UserManagement uManagement = new UserManagement();
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                ChannelManagement cManagement = new ChannelManagement();
                String channelid = session.getChannelid();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    return null;
                }
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }
                if ((retValue = uManagement.CreateUser(sessionId, session.getChannelid(), fullname, phone, email, userid, 0, "MSC Trustgate.com Sdn Bhd.", "140541211211", "Passport", "Business Development", "MY", "KL", "Mont Kiara", "Manager")) == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                } else if (retValue == 1) {
                    aStatus.error = "User created succesfully but email could not be sent!!!";
                    aStatus.errorcode = 0;
                } else if (retValue == -1) {
                    aStatus.error = "Duplicate user entry!!!";
                    aStatus.errorcode = 0;
                }
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "Create User", aStatus.error, aStatus.errorcode, "User Management", "", "Name=" + fullname + ",Phone=" + phone + ",Email=" + email + ",State=Inactive", "USER", "");
            return aStatus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public AxiomUser GetUserBy(String sessionId, int type, String searchFor) throws AxiomException {
        log.debug("Started");
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug((Object) ("GetUserBy::sessionId::" + sessionId));
                log.debug((Object) ("GetUserBy::type::" + type));
                log.debug((Object) ("GetUserBy::searchFor::" + searchFor));
            }
        } catch (Exception var5_5) {
            // empty catch block
        }
        try {
            UserManagement uManagement = new UserManagement();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            String resultStr = "Failure";
            int retValue = -1;
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionId);
            AxiomUser aUser = null;
            if (sessionId == null) {
                throw new Exception("Session ID is empty!!!");
            }
            if (session == null) {
                throw new Exception("Session ID is invalid/expired!!!");
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                if (channel == null) {
                    return null;
                }
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                throw new Exception("IP is not allowed!!!");
                            }
                            suTemplate.close();
                            sTemplate.close();
                            throw new Exception("IP is not allowed!!!");
                        }
                        throw new Exception("IP is not allowed!!!");
                    }
                }
                String userFlag = LoadSettings.g_sSettings.getProperty("check.user");
                AuthUser authUser = null;
                log.debug("GetUserBy :: CheckUserByType() called");
                authUser = userFlag.equals("1") ? uManagement.CheckUserByType(sessionId, session.getChannelid(), searchFor, 4) : uManagement.CheckUserByType(sessionId, session.getChannelid(), searchFor, type);
               
                log.debug(" GetUserBy :: CheckUserByType() ended");
                if (authUser != null) {
                    aUser = new AxiomUser();
                    aUser.email = authUser.email;
                    aUser.iAttempts = authUser.iAttempts;
                    aUser.lCreatedOn = authUser.lCreatedOn;
                    aUser.lLastAccessOn = authUser.lLastAccessOn;
                    aUser.phoneNo = authUser.phoneNo;
                    aUser.statePassword = authUser.statePassword;
                    aUser.userId = authUser.userId;
                    aUser.userName = authUser.userName;
                  
                }
                
            }
            if (aUser != null) {
                retValue = 0;
                resultStr = "Success";
                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "GET USER", resultStr, retValue, "User Management", "Name=" + aUser.userName + ",Phone=" + aUser.phoneNo + ",Email=" + aUser.email, "", "USER", aUser.userId);
            } else if (aUser == null) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "GET USER", resultStr, retValue, "User Management", "", "", "USER", "");
                 }
            log.debug("ended");
            return aUser;
        } catch (Exception e) {
            e.printStackTrace();
            throw new AxiomException(e.getMessage());
        }
    }

    public AxiomStatus ResetUser(String sessionId, String userid, int type) {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug((Object) ("ResetUser::sessionId::" + sessionId));
                log.debug((Object) ("ResetUser::userid::" + userid));
                log.debug((Object) ("ResetUser::type::" + type));
            }
        } catch (Exception var5_5) {
            // empty catch block
        }
        try {
            SessionManagement sManagement = new SessionManagement();
            Sessions session = sManagement.getSessionById(sessionId);
            AuditManagement audit = new AuditManagement();
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "ERROR";
            aStatus.errorcode = -9;
            int retValue = -1;
            String strCategory = "";
            if (type == this.RESET_USER_TOKEN_SOFTWARE) {
                strCategory = "SOFTWARE_TOKEN";
            } else if (type == this.RESET_USER_TOKEN_HARDWARE) {
                strCategory = "HARDWARE_TOKEN";
            } else if (type == this.RESET_USER_TOKEN_OOB) {
                strCategory = "OOB_TOKEN";
            }
            String strSubCategory = "";
            if (session == null) {
                return aStatus;
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (session != null) {
                OTPTokenManagement oManagement;
                MessageContext mc = this.wsContext.getMessageContext();
                HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                if (channel == null) {
                    return null;
                }
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement2 = new OperatorsManagement();
                            Operators[] aOperator = oManagement2.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }
                if (type == this.RESET_USER_PASSWORD) {
                    UserManagement uManagement = new UserManagement();
                    retValue = uManagement.resetPassword(sessionId, session.getChannelid(), userid);
                    if (retValue == 0) {
                        aStatus.error = "SUCCESS";
                        aStatus.errorcode = 0;
                    } else {
                        aStatus.errorcode = -1;
                    }
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "RESET", aStatus.error, aStatus.errorcode, "User Management", "Old Password = ******", "New Password = ******", "PASSWORD", userid);
                    return aStatus;
                }
                if (type == this.RESET_USER_TOKEN_OOB) {
                    oManagement = new OTPTokenManagement(session.getChannelid());
                    TokenStatusDetails[] tokens = oManagement.getTokenList(sessionId, session.getChannelid(), userid);
                    TokenStatusDetails tokenSelected = null;
                    for (int i = 0; i < tokens.length; ++i) {
                        if (tokens[i].Catrgory != 3) {
                            continue;
                        }
                        tokenSelected = tokens[i];
                        break;
                    }
                    if (tokenSelected == null) {
                        aStatus.errorcode = -2;
                        aStatus.error = "Desired OOB Token is not assigned";
                        return aStatus;
                    }
                    if (tokenSelected.SubCategory == this.OTP_TOKEN_OUTOFBAND_SMS) {
                        strSubCategory = "OOB__SMS_TOKEN";
                    } else if (tokenSelected.SubCategory == this.OTP_TOKEN_OUTOFBAND_USSD) {
                        strSubCategory = "OOB__USSD_TOKEN";
                    } else if (tokenSelected.SubCategory == this.OTP_TOKEN_OUTOFBAND_VOICE) {
                        strSubCategory = "OOB__VOICE_TOKEN";
                    } else if (tokenSelected.SubCategory == this.OTP_TOKEN_OUTOFBAND_EMAIL) {
                        strSubCategory = "OOB__EMAIL_TOKEN";
                    }
                    retValue = oManagement.ChangeStatus(sessionId, userid, session.getChannelid(), this.TOKEN_STATUS_UNASSIGNED, 3, tokenSelected.SubCategory);
                    if (retValue == 0) {
                        aStatus.error = "SUCCESS";
                        aStatus.errorcode = 0;
                    } else {
                        aStatus.errorcode = -2;
                    }
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "RESET", aStatus.error, aStatus.errorcode, "Token Management", "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status, "New Status =" + this.strUNASSIGNED, "OOBTOKEN", userid);
                } else if (type == this.RESET_USER_TOKEN_SOFTWARE) {
                    oManagement = new OTPTokenManagement(session.getChannelid());
                    TokenStatusDetails[] tokens = oManagement.getTokenList(sessionId, session.getChannelid(), userid);
                    TokenStatusDetails tokenSelected = null;
                    for (int i = 0; i < tokens.length; ++i) {
                        if (tokens[i].Catrgory != 1) {
                            continue;
                        }
                        tokenSelected = tokens[i];
                        break;
                    }
                    if (tokenSelected == null) {
                        aStatus.errorcode = -2;
                        aStatus.error = "Desired Software Token is not assigned";
                        return aStatus;
                    }
                    if (tokenSelected.SubCategory == this.OTP_TOKEN_SOFTWARE_WEB) {
                        strSubCategory = "SW_WEB_TOKEN";
                    } else if (tokenSelected.SubCategory == this.OTP_TOKEN_SOFTWARE_MOBILE) {
                        strSubCategory = "SW_MOBILE_TOKEN";
                    } else if (tokenSelected.SubCategory == this.OTP_TOKEN_SOFTWARE_PC) {
                        strSubCategory = "SW_PC_TOKEN";
                    }
                    retValue = oManagement.ChangeStatus(sessionId, userid, session.getChannelid(), this.TOKEN_STATUS_UNASSIGNED, 1, tokenSelected.SubCategory);
                    if (retValue == 0) {
                        aStatus.error = "SUCCESS";
                        aStatus.errorcode = 0;
                    } else {
                        aStatus.errorcode = -2;
                    }
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "RESET", aStatus.error, aStatus.errorcode, "Token Management", "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status, "New Status =" + this.strUNASSIGNED, "SOFTWARETOKEN", userid);
                } else if (type == this.RESET_USER_TOKEN_HARDWARE) {
                    oManagement = new OTPTokenManagement(session.getChannelid());
                    TokenStatusDetails[] tokens = oManagement.getTokenList(sessionId, session.getChannelid(), userid);
                    TokenStatusDetails tokenSelected = null;
                    for (int i = 0; i < tokens.length; ++i) {
                        if (tokens[i].Catrgory != 2) {
                            continue;
                        }
                        tokenSelected = tokens[i];
                        break;
                    }
                    if (tokenSelected == null) {
                        aStatus.errorcode = -2;
                        aStatus.error = "Desired Hardware Token is not assigned";
                        return aStatus;
                    }
                    if (tokenSelected.SubCategory == this.OTP_TOKEN_HARDWARE_CR) {
                        strSubCategory = "HW_CR_TOKEN";
                    } else if (tokenSelected.SubCategory == this.OTP_TOKEN_HARDWARE_MINI) {
                        strSubCategory = "HW_MINI_TOKEN";
                    }
                    retValue = oManagement.ChangeStatus(sessionId, userid, session.getChannelid(), this.TOKEN_STATUS_UNASSIGNED, 2, tokenSelected.SubCategory);
                    if (retValue == 0) {
                        aStatus.error = "SUCCESS";
                        aStatus.errorcode = 0;
                    } else {
                        aStatus.errorcode = -2;
                    }
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "RESET", aStatus.error, aStatus.errorcode, "Token Management", "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status, "New Status =" + this.strUNASSIGNED, "HARDWARETOKEN", userid);
                }
            }
            return aStatus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public AxiomStatus AssignPassword(String sessionId, String userid, String password, boolean bSentToUser, int subcategory) {
        AxiomStatus aStatus;
        block29:
        {
            String strDebug = null;
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    log.debug((Object) ("AssignPassword::sessionId::" + sessionId));
                    log.debug((Object) ("AssignPassword::userid::" + userid));
                    log.debug((Object) ("AssignPassword::password::" + password));
                    log.debug((Object) ("AssignPassword::bSentToUser::" + bSentToUser));
                    log.debug((Object) ("AssignPassword::subcategory::" + subcategory));
                }
            } catch (Exception var7_7) {
                // empty catch block
            }
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionId);
            aStatus = new AxiomStatus();
            if (session == null) {
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                return aStatus;
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            if (channel == null) {
                return aStatus;
            }
            try {
                UserManagement uManagement = new UserManagement();
                if (session == null) {
                    AxiomStatus axiomStatus = aStatus;
                    return axiomStatus;
                }
                if (session == null) {
                    break block29;
                }
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                if (channel == null) {
                    AxiomStatus axiomStatus = null;
                    return axiomStatus;
                }
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                AxiomStatus baisobj = aStatus;
                                return baisobj;
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            AxiomStatus emailList = aStatus;
                            return emailList;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        AxiomStatus suTemplate = aStatus;
                        return suTemplate;
                    }
                }
                if ((retValue = uManagement.AssignPassword(sessionId, session.getChannelid(), userid, password)) != 0) {
                    break block29;
                }
                if (bSentToUser) {
                    SendNotification send = new SendNotification();
                    AuthUser aUser = uManagement.getUser(sessionId, session.getChannelid(), userid);
                    Templates temp = null;
                    TemplateManagement tObj = new TemplateManagement();
                    if (subcategory == 1) {
                        temp = tObj.LoadbyName(sessionId, session.getChannelid(), "mobile.user.password");
                    }
                    if (temp == null) {
                        aStatus.error = "Message Template is missing";
                        aStatus.errorcode = -2;
                    }
                    ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
                    String templatebody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) bais);
                    templatebody = templatebody.replaceAll("#name#", aUser.userName);
                    String date = String.valueOf(new Date());
                    templatebody = templatebody.replaceAll("#channel#", channel.getName());
                    templatebody = templatebody.replaceAll("#password#", password);
                    templatebody = templatebody.replaceAll("#datetime#", date);
                    AXIOMStatus axiomStatus = null;
                    int iProductType = Integer.valueOf(LoadSettings.g_sSettings.getProperty("product.type"));
                    tObj.getClass();
                    if (temp.getStatus() == 1) {
                        axiomStatus = send.SendOnMobile(session.getChannelid(), aUser.phoneNo, templatebody, subcategory, iProductType);
                    }
                    if (axiomStatus != null) {
                        aStatus.error = axiomStatus.strStatus;
                        aStatus.errorcode = axiomStatus.iStatus;
                    }
                    break block29;
                }
                aStatus.error = "SUCCESS";
                aStatus.errorcode = 0;
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "Assign Password", aStatus.error, aStatus.errorcode, "User Management", "Old Password=******", "New Password=******", "PASSWORD", userid);
            }
        }
        return aStatus;
    }

    public AxiomStatus VerifyPassword(String sessionId, String userid, String password) {
        log.debug("started");
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug((Object) ("VerifyPassword::sessionId::" + sessionId));
                log.debug((Object) ("VerifyPassword::userid::" + userid));
                log.debug((Object) ("VerifyPassword::password::" + password));
            }
        } catch (Exception var5_5) {
            // empty catch block
        }
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = this.wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
        Sessions session = sManagement.getSessionById(sessionId);
        AxiomStatus aStatus = new AxiomStatus();
        if (session == null) {
            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
            return aStatus;
        }
        UserManagement uManagement = new UserManagement();
        int retValue = -1;
        aStatus.error = "ERROR";
        aStatus.errorcode = retValue;
        if (session == null) {
            return aStatus;
        }
        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(session.getChannelid());
        if (channel == null) {
            return aStatus;
        }
        if (session != null) {
            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            if (channel == null) {
                return null;
            }
            Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                        OperatorsManagement oManagement = new OperatorsManagement();
                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; ++i) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == 1) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }
                                SendNotification send = new SendNotification();
                                AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            log.debug("ended");
                            return aStatus;
                        }
                        suTemplate.close();
                        sTemplate.close();
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        log.debug("ended");
                        return aStatus;
                    }
                    aStatus.error = "INVALID IP REQUEST";
                    aStatus.errorcode = -8;
                    log.debug("ended");
                    return aStatus;
                }
            }
            AuthUser aUser = uManagement.verifyPassword(sessionId, session.getChannelid(), userid, password);
            if (retValue == 0) {
                aStatus.error = "SUCCESS";
                aStatus.errorcode = 0;
            }
        }
        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "Verify Password", aStatus.error, aStatus.errorcode, "User Management", "", "", "PASSWORD", userid);
        log.debug("ended");
        return aStatus;
    }

    public AxiomStatus AssignToken(String sessionId, String userid, int type, int subtype) {
        log.debug("started");
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug((Object) ("AssignToken::sessionId::" + sessionId));
                log.debug((Object) ("AssignToken::userid::" + userid));
                log.debug((Object) ("AssignToken::type::" + type));
                log.debug((Object) ("AssignToken::subtype::" + subtype));
            }
        } catch (Exception var6_6) {
            // empty catch block
        }
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = this.wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
        Sessions session = sManagement.getSessionById(sessionId);
        AxiomStatus aStatus = new AxiomStatus();
        if (session == null) {
            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
            return aStatus;
        }
        if (sessionId == null || userid == null || sessionId.isEmpty() || userid.isEmpty() || type < 0 || subtype < 0) {
            aStatus.error = "Invalid Data";
            aStatus.errorcode = -11;
            return aStatus;
        }
        String strCategory = "";
        if (type == this.OTP_TOKEN_SOFTWARE) {
            strCategory = "SOFTWARE_TOKEN";
        } else if (type == this.OTP_TOKEN_HARDWARE) {
            strCategory = "HARDWARE_TOKEN";
        } else if (type == this.OTP_TOKEN_OUTOFBAND) {
            strCategory = "OOB_TOKEN";
        }
        if (type != this.OTP_TOKEN_SOFTWARE && type != this.OTP_TOKEN_HARDWARE && type != this.OTP_TOKEN_OUTOFBAND) {
            aStatus.error = "Invalid category!!!";
            aStatus.errorcode = -12;
            return aStatus;
        }
        String strSubCategory = "";
        if (subtype == this.OTP_TOKEN_OUTOFBAND_SMS) {
            strSubCategory = "OOB__SMS_TOKEN";
        } else if (subtype == this.OTP_TOKEN_OUTOFBAND_USSD) {
            strSubCategory = "OOB__USSD_TOKEN";
        } else if (subtype == this.OTP_TOKEN_OUTOFBAND_VOICE) {
            strSubCategory = "OOB__VOICE_TOKEN";
        } else if (subtype == this.OTP_TOKEN_OUTOFBAND_EMAIL) {
            strSubCategory = "OOB__EMAIL_TOKEN";
        } else if (subtype == this.OTP_TOKEN_SOFTWARE_WEB) {
            strSubCategory = "SW_WEB_TOKEN";
        } else if (subtype == this.OTP_TOKEN_SOFTWARE_MOBILE) {
            strSubCategory = "SW_MOBILE_TOKEN";
        } else if (subtype == this.OTP_TOKEN_SOFTWARE_PC) {
            strSubCategory = "SW_PC_TOKEN";
        } else if (subtype == this.OTP_TOKEN_HARDWARE_CR) {
            strSubCategory = "HW_CR_TOKEN";
        } else if (subtype == this.OTP_TOKEN_HARDWARE_MINI) {
            strSubCategory = "HW_MINI_TOKEN";
        } else {
            aStatus.error = "Invalid subcategory!!!";
            aStatus.errorcode = -13;
            return aStatus;
        }
        int retValue = -1;
        aStatus.error = "ERROR";
        aStatus.errorcode = retValue;
        if (session == null) {
            aStatus.error = "Invalid/Expired Session";
            aStatus.errorcode = -14;
            return aStatus;
        }
        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(session.getChannelid());
        if (channel == null) {
            aStatus.error = "Invalid Channel";
            aStatus.errorcode = -15;
            return aStatus;
        }
        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
        if (session != null) {
            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            if (channel == null) {
                return null;
            }
            Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                        OperatorsManagement oManagementObj = new OperatorsManagement();
                        Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; ++i) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == 1) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }
                                SendNotification send = new SendNotification();
                                AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        suTemplate.close();
                        sTemplate.close();
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                    aStatus.error = "INVALID IP REQUEST";
                    aStatus.errorcode = -8;
                    return aStatus;
                }
            }
            if ((retValue = oManagement.AssignToken(sessionId, session.getChannelid(), userid, type, subtype, null)) == 0) {
                aStatus.error = "SUCCESS";
                aStatus.errorcode = 0;
            } else if (retValue == -4) {
                aStatus.error = "Token already assigned!!!";
                aStatus.errorcode = -4;
            } else if (retValue == -3) {
                aStatus.error = "Empty Serial Number!!!";
                aStatus.errorcode = -3;
            } else if (retValue == -2) {
                aStatus.error = "Invalid Serial Number!!!";
                aStatus.errorcode = -3;
            } else if (retValue == -6) {
                aStatus.error = "Hardware Token could not be assigned!!!";
                aStatus.errorcode = -6;
            }
        }
        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "Assign OTP Token", aStatus.error, aStatus.errorcode, "Token Management", "", "Category=" + strCategory + ",Subcategory=" + strSubCategory, "OTPTOKENS", userid);
        log.debug("ended");
        return aStatus;
    }

    public AxiomTokenDetails[] GetUserTokens(String sessionId, String userid) throws AxiomException {
        log.debug("started");
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug((Object) ("GetUserTokens::sessionId::" + sessionId));
                log.debug((Object) ("GetUserTokens::userid::" + userid));
            }
        } catch (Exception var4_4) {
            // empty catch block
        }
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = this.wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
        Sessions session = sManagement.getSessionById(sessionId);
        if (session == null) {
            throw new AxiomException("Invalid Session!!!");
        }
        TokenStatusDetails[] tDetails = null;
        AxiomTokenDetails[] atDetails = null;
        String resultStr = "Failure";
        int retValue = -1;
        ChannelManagement cManagement = new ChannelManagement();
        log.debug("getChannelByID :Strted");
        Channels channel = cManagement.getChannelByID(session.getChannelid());
          log.debug("getChannelByID :Ended");
        if (channel == null) {
            throw new AxiomException("Invalid Channel!!!");
        }
        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
        log.debug("");
         if (session != null) {
//            SettingsManagement setManagement = new SettingsManagement();
           // String channelid = session.getChannelid();
//            Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
//            if (ipobj != null) {
//                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                int checkIp = 1;
//                checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
//                if (iObj.ipstatus == 0 && checkIp != 1) {
//                    if (iObj.ipalertstatus == 0) {
//                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
//                        Session sTemplate = suTemplate.openSession();
//                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                        Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
//                        OperatorsManagement oManagementObj = new OperatorsManagement();
//                        Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
//                        if (aOperator != null) {
//                            String[] emailList = new String[aOperator.length - 1];
//                            for (int i = 1; i < aOperator.length; ++i) {
//                                emailList[i - 1] = aOperator[i].getEmailid();
//                            }
//                            if (templatesObj.getStatus() == 1) {
//                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
//                                String strsubject = templatesObj.getSubject();
//                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                if (strmessageBody != null) {
//                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                }
//                                SendNotification send = new SendNotification();
//                                AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                            }
//                            suTemplate.close();
//                            sTemplate.close();
//                            throw new AxiomException("Invalid IP!!!");
//                        }
//                        suTemplate.close();
//                        sTemplate.close();
//                        throw new AxiomException("Invalid IP!!!");
//                    }
//                    throw new AxiomException("Invalid IP!!!");
//                }
//            }
            log.debug("GetUserTokens :: getTokenList() called");
            if ((tDetails = oManagement.getTokenList(sessionId, session.getChannelid(), userid)) != null && tDetails.length > 0) {
                atDetails = new AxiomTokenDetails[tDetails.length];
                for (int i = 0; i < tDetails.length; ++i) {
                    atDetails[i] = new AxiomTokenDetails();
                    atDetails[i].attempts = tDetails[i].Attempts;
                    atDetails[i].category = tDetails[i].Catrgory;
                    atDetails[i].status = tDetails[i].Status;
                    atDetails[i].createOn = tDetails[i].createOn;
                    atDetails[i].subcategory = tDetails[i].SubCategory;
                    atDetails[i].lastaccessOn = tDetails[i].lastaccessOn;
                    atDetails[i].serialnumber = tDetails[i].serialnumber;
                }
            }
            log.debug("GetUserTokens :: getTokenList() ended");
        }
        if (atDetails != null) {
            retValue = 0;
            resultStr = "Success";
        } else if (atDetails == null) {
            // empty if block
        }
        log.debug("ended");
        return atDetails;
    }

    public AxiomStatus SendOTP(String sessionId, String userid) {
        log.debug("started");
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug((Object) ("SendOTP::sessionId::" + sessionId));
                log.debug((Object) ("SendOTP::userid::" + userid));
            }
        } catch (Exception var4_4) {
            // empty catch block
        }
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = this.wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
        Sessions session = sManagement.getSessionById(sessionId);
        if (session == null) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
            return aStatus;
        }
        if (sessionId == null || userid == null || sessionId.isEmpty() || userid.isEmpty()) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Invalid Data";
            aStatus.errorcode = -11;
            return aStatus;
        }
        AxiomStatus aStatus = new AxiomStatus();
        String strCategory = "OOB_TOKEN";
        String strSubCategory = "";
        int retValue = -1;
        aStatus.error = "ERROR";
        aStatus.errorcode = retValue;
        ChannelManagement cManagement = new ChannelManagement();
        log.debug("getChannelByID Started");
        Channels channel = cManagement.getChannelByID(session.getChannelid());
         log.debug("getChannelByID Ended");
        if (channel == null) {
            aStatus.error = "Invalid Channel";
            aStatus.errorcode = -15;
            return aStatus;
        }
        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
        if (session != null) {
//            SettingsManagement setManagement = new SettingsManagement();
//            String channelid = session.getChannelid();
//            Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
//            if (ipobj != null) {
//                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                int checkIp = 1;
//                checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
//                if (iObj.ipstatus == 0 && checkIp != 1) {
//                    if (iObj.ipalertstatus == 0) {
//                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
//                        Session sTemplate = suTemplate.openSession();
//                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                        Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
//                        OperatorsManagement oManagementObj = new OperatorsManagement();
//                        Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
//                        if (aOperator != null) {
//                            String[] emailList = new String[aOperator.length - 1];
//                            for (int i = 1; i < aOperator.length; ++i) {
//                                emailList[i - 1] = aOperator[i].getEmailid();
//                            }
//                            if (templatesObj.getStatus() == 1) {
//                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
//                                String strsubject = templatesObj.getSubject();
//                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                if (strmessageBody != null) {
//                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                }
//                                SendNotification send = new SendNotification();
//                                AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                            }
//                            suTemplate.close();
//                            sTemplate.close();
//                            aStatus.error = "INVALID IP REQUEST";
//                            aStatus.errorcode = -8;
//                            return aStatus;
//                        }
//                        suTemplate.close();
//                        sTemplate.close();
//                        aStatus.error = "INVALID IP REQUEST";
//                        aStatus.errorcode = -8;
//                        return aStatus;
//                    }
//                    aStatus.error = "INVALID IP REQUEST";
//                    aStatus.errorcode = -8;
//                    return aStatus;
//                }
//            }
             log.debug("SendOTP :: getTokenList() called ");
            TokenStatusDetails[] tokens = oManagement.getTokenList(sessionId, session.getChannelid(), userid);
             log.debug("SendOTP :: getTokenList() ended ");
            TokenStatusDetails tokenSelected = null;
            if (tokens == null) {
                aStatus.error = "No Token Assigned to this user!!!";
                aStatus.errorcode = -2;
                return aStatus;
            }
            for (int i = 0; i < tokens.length; ++i) {
                if (tokens[i].Catrgory != 3) {
                    continue;
                }
                tokenSelected = tokens[i];
                break;
            }
            if (tokenSelected == null) {
                aStatus.errorcode = -12;
                aStatus.error = "Desired OOB Token is not assigned";
                return aStatus;
            }
            if (tokenSelected.SubCategory == this.OTP_TOKEN_OUTOFBAND_SMS) {
                strSubCategory = "OOB__SMS_TOKEN";
            } else if (tokenSelected.SubCategory == this.OTP_TOKEN_OUTOFBAND_USSD) {
                strSubCategory = "OOB__USSD_TOKEN";
            } else if (tokenSelected.SubCategory == this.OTP_TOKEN_OUTOFBAND_VOICE) {
                strSubCategory = "OOB__VOICE_TOKEN";
            } else if (tokenSelected.SubCategory == this.OTP_TOKEN_OUTOFBAND_EMAIL) {
                strSubCategory = "OOB__EMAIL_TOKEN";
            }
            log.debug("SendOTP :: SendOTP() called ");
            AXIOMStatus aStatusResponse = oManagement.SendOTP(sessionId, session.getChannelid(), userid, this.OTP_TOKEN_OUTOFBAND, tokenSelected.SubCategory);
            log.debug("SendOTP :: SendOTP() ended ");
            aStatus.error = aStatusResponse.strStatus;
            aStatus.errorcode = aStatusResponse.iStatus;
        }
        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "SEND OTP", aStatus.error, aStatus.errorcode, "Token Management", "", "Category=" + strCategory + ",Subcategory=" + strSubCategory, "OTPTOKENS", userid);
        log.debug("ended");
        return aStatus;
    }

    public AxiomStatus VerifyOTP(String sessionId, String userid, String otp) {
        log.debug("started");
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug((Object) ("VerifyOTP::sessionId::" + sessionId));
                log.debug((Object) ("VerifyOTP::userid::" + userid));
                log.debug((Object) ("VerifyOTP::otp::" + otp));
            }
        } catch (Exception var5_5) {
            // empty catch block
        }
        try {
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                return aStatus;
            }
            if (sessionId == null || userid == null || sessionId.isEmpty() || userid.isEmpty() || otp == null || otp.isEmpty()) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Data";
                aStatus.errorcode = -11;
                return aStatus;
            }
            AxiomStatus aStatus = new AxiomStatus();
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            ChannelManagement cManagement = new ChannelManagement();
            log.debug("getChannelByID Started");
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            log.debug("getChannelByID Ended");
            if (channel == null) {
                aStatus.error = "Invalid Channel";
                aStatus.errorcode = -15;
                return aStatus;
            }
            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
            if (session != null) {
//                SettingsManagement setManagement = new SettingsManagement();
//                String channelid = session.getChannelid();
//                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
//                if (ipobj != null) {
//                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                    int checkIp = 1;
//                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
//                    if (iObj.ipstatus == 0 && checkIp != 1) {
//                        if (iObj.ipalertstatus == 0) {
//                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
//                            Session sTemplate = suTemplate.openSession();
//                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
//                            OperatorsManagement oManagementObj = new OperatorsManagement();
//                            Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
//                            if (aOperator != null) {
//                                String[] emailList = new String[aOperator.length - 1];
//                                for (int i = 1; i < aOperator.length; ++i) {
//                                    emailList[i - 1] = aOperator[i].getEmailid();
//                                }
//                                if (templatesObj.getStatus() == 1) {
//                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
//                                    String strsubject = templatesObj.getSubject();
//                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                    if (strmessageBody != null) {
//                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                    }
//                                    SendNotification send = new SendNotification();
//                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                                }
//                                suTemplate.close();
//                                sTemplate.close();
//                                aStatus.error = "INVALID IP REQUEST";
//                                aStatus.errorcode = -8;
//                                return aStatus;
//                            }
//                            suTemplate.close();
//                            sTemplate.close();
//                            aStatus.error = "INVALID IP REQUEST";
//                            aStatus.errorcode = -8;
//                            return aStatus;
//                        }
//                        aStatus.error = "INVALID IP REQUEST";
//                        aStatus.errorcode = -8;
//                        return aStatus;
//                    }
//                }
                log.debug("VerifyOTP :: VerifyOTP() called");
                if ((retValue = oManagement.VerifyOTP(session.getChannelid(), userid, sessionId, otp)) == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                    log.debug((Object) ("otp:" + otp + " :Verified Sucessfully"));
                } else if (retValue == -9) {
                    aStatus.error = "One Time Password is expired...";
                    aStatus.errorcode = -9;
                } else if (retValue == -8) {
                    aStatus.error = "One Time Password is already consumed!!!";
                    aStatus.errorcode = -16;
                } else if (retValue == -17) {
                    aStatus.error = "User/Token is not found!!!";
                    aStatus.errorcode = -17;
                } else if (retValue == -22) {
                    aStatus.error = "Token is locked!!!";
                    aStatus.errorcode = -22;
                }
                 log.debug("VerifyOTP :: VerifyOTP() ended");
            }
            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "VERIFY OTP", aStatus.error, aStatus.errorcode, "Token Management", "OTP=" + otp, "", "OTPTOKENS", userid);
            log.debug("ended");
            return aStatus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public AxiomStatus SendSignatureOTP(String sessionId, String userid, String[] data) {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug((Object) ("SendSignatureOTP::sessionId::" + sessionId));
                log.debug((Object) ("SendSignatureOTP::userid::" + userid));
                log.debug((Object) ("SendSignatureOTP::data::" + data));
                log.debug((Object) ("SendSignatureOTP::data.length::" + data.length));
                for (int i = 0; i < data.length; ++i) {
                    log.debug((Object) ("SendSignatureOTP::data[" + i + "]::" + data[i]));
                }
            }
        } catch (Exception i) {
            // empty catch block
        }
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = this.wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
        Sessions session = sManagement.getSessionById(sessionId);
        if (session == null) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
            return aStatus;
        }
        if (sessionId == null || userid == null || sessionId.isEmpty() || userid.isEmpty() || data == null || data.length <= 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Invalid Data";
            aStatus.errorcode = -11;
            return aStatus;
        }
        AxiomStatus aStatus = new AxiomStatus();
        int retValue = -1;
        aStatus.error = "ERROR";
        aStatus.errorcode = retValue;
        String strCategory = "OOB_TOKEN";
        String strSubCategory = "";
        aStatus.error = "ERROR";
        aStatus.errorcode = retValue;
        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(session.getChannelid());
        if (channel == null) {
            aStatus.error = "Invalid Channel";
            aStatus.errorcode = -15;
            return aStatus;
        }
        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
        if (session != null) {
            TokenStatusDetails[] tokens;
            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                        OperatorsManagement oManagementObj = new OperatorsManagement();
                        Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; ++i) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == 1) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }
                                SendNotification send = new SendNotification();
                                AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        suTemplate.close();
                        sTemplate.close();
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                    aStatus.error = "INVALID IP REQUEST";
                    aStatus.errorcode = -8;
                    return aStatus;
                }
            }
            if ((tokens = oManagement.getTokenList(sessionId, session.getChannelid(), userid)) == null) {
                aStatus.error = "No Token Assigned to this user!!!";
                aStatus.errorcode = -2;
                return aStatus;
            }
            TokenStatusDetails tokenSelected = null;
            for (int i = 0; i < tokens.length; ++i) {
                if (tokens[i].Catrgory != 3) {
                    continue;
                }
                tokenSelected = tokens[i];
                break;
            }
            if (tokenSelected == null) {
                aStatus.errorcode = -12;
                aStatus.error = "Desired OOB Token is not assigned!!!";
                return aStatus;
            }
            if (tokenSelected.SubCategory == this.OTP_TOKEN_OUTOFBAND_SMS) {
                strSubCategory = "OOB__SMS_TOKEN";
            } else if (tokenSelected.SubCategory == this.OTP_TOKEN_OUTOFBAND_USSD) {
                strSubCategory = "OOB__USSD_TOKEN";
            } else if (tokenSelected.SubCategory == this.OTP_TOKEN_OUTOFBAND_VOICE) {
                strSubCategory = "OOB__VOICE_TOKEN";
            } else if (tokenSelected.SubCategory == this.OTP_TOKEN_OUTOFBAND_EMAIL) {
                strSubCategory = "OOB__EMAIL_TOKEN";
            }
            retValue = oManagement.SendSignatureOTP(session.getChannelid(), userid, sessionId, data, this.OTP_TOKEN_OUTOFBAND, tokenSelected.SubCategory);
            if (retValue == 0 || retValue == 2) {
                aStatus.error = "SUCCESS";
                aStatus.errorcode = 0;
            } else if (retValue == -22) {
                aStatus.error = "Token is locked!!!";
                aStatus.errorcode = retValue;
            } else {
                aStatus.error = "Signautre OTP message could not sent...";
                aStatus.errorcode = retValue;
            }
        }
        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "SEND SIGNATURE OTP", aStatus.error, aStatus.errorcode, "Token Management", "", "Category=" + strCategory + ",Subcategory=" + strSubCategory, "OTPTOKENS", userid);
        return aStatus;
    }

    public AxiomStatus VerifySignatureOTP(String sessionId, String userid, String[] data, String otp) {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug((Object) ("VerifySignatureOTP::sessionId::" + sessionId));
                log.debug((Object) ("VerifySignatureOTP::userid::" + userid));
                log.debug((Object) ("VerifySignatureOTP::otp::" + otp));
                log.debug((Object) ("VerifySignatureOTP::data::" + data));
                log.debug((Object) ("VerifySignatureOTP::data.length::" + data.length));
                for (int i = 0; i < data.length; ++i) {
                    log.debug((Object) ("VerifySignatureOTP::data[" + i + "]::" + data[i]));
                }
            }
        } catch (Exception i) {
            // empty catch block
        }
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = this.wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
        Sessions session = sManagement.getSessionById(sessionId);
        if (session == null) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
            return aStatus;
        }
        if (sessionId == null || userid == null || sessionId.isEmpty() || userid.isEmpty() || data == null || data.length <= 0 || otp == null || otp.isEmpty()) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Invalid Data";
            aStatus.errorcode = -11;
            return aStatus;
        }
        AxiomStatus aStatus = new AxiomStatus();
        int retValue = -1;
        aStatus.error = "ERROR";
        aStatus.errorcode = retValue;
        if (session == null) {
            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
            return aStatus;
        }
        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(session.getChannelid());
        if (channel == null) {
            aStatus.error = "Invalid Channel";
            aStatus.errorcode = -15;
            return aStatus;
        }
        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
        if (session != null) {
            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                        OperatorsManagement oManagementObj = new OperatorsManagement();
                        Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; ++i) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == 1) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }
                                SendNotification send = new SendNotification();
                                AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        suTemplate.close();
                        sTemplate.close();
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                    aStatus.error = "INVALID IP REQUEST";
                    aStatus.errorcode = -8;
                    return aStatus;
                }
            }
            if ((retValue = oManagement.VerifySignatureOTP(session.getChannelid(), userid, sessionId, data, otp)) == 0) {
                aStatus.error = "SUCCESS";
                aStatus.errorcode = 0;
            } else if (retValue == -9) {
                aStatus.error = "Signature One Time Password is expired";
                aStatus.errorcode = -9;
            } else if (retValue == -2) {
                aStatus.error = "Internal Error";
                aStatus.errorcode = -2;
            } else if (retValue == -14) {
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
            } else if (retValue == -8) {
                aStatus.error = "One Time Password is already consumed!!!";
                aStatus.errorcode = -16;
            } else if (retValue == -17) {
                aStatus.error = "User/Token is not found!!!";
                aStatus.errorcode = -17;
            } else if (retValue == -22) {
                aStatus.error = "Token is locked!!!";
                aStatus.errorcode = -22;
            }
        }
        SessionFactoryUtil suSettings = new SessionFactoryUtil(9);
        Session sSettings = suSettings.openSession();
        SettingsUtil setUtil = new SettingsUtil(suSettings, sSettings);
        TokenSettings tSettings = (TokenSettings) setUtil.getSetting(session.getChannelid(), 7, 1);
        sSettings.close();
        suSettings.close();
        String txFrom = null;
        String txTo = null;
        if (tSettings.isbEnforceMasking()) {
            txTo = UtilityFunctions.MaskData((String) data[1]);
            txFrom = UtilityFunctions.MaskData((String) data[2]);
        } else {
            txTo = data[1];
            txFrom = data[2];
        }
        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "VERIFY SIGNATURE OTP", aStatus.error, aStatus.errorcode, "Token Management", "SOTP=" + otp + ",From=" + txFrom + ",To=" + txTo, "", "OTPTOKENS", userid);
        return aStatus;
    }

    public AxiomStatus ChangeOTPTokenStatus(String sessionid, String userid, int category, int value) {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug((Object) ("ChangeOTPTokenStatus::sessionid::" + sessionid));
                log.debug((Object) ("ChangeOTPTokenStatus::userid::" + userid));
                log.debug((Object) ("ChangeOTPTokenStatus::category::" + category));
                log.debug((Object) ("ChangeOTPTokenStatus::value::" + value));
            }
        } catch (Exception var6_6) {
            // empty catch block
        }
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = this.wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
        Sessions session = sManagement.getSessionById(sessionid);
        if (session == null) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
            return aStatus;
        }
        AxiomStatus aStatus = new AxiomStatus();
        aStatus.error = "ERROR";
        aStatus.errorcode = -2;
        String oldValue = "";
        String strValue = "";
        if (session != null) {
            OTPTokenManagement oManagement;
            TokenStatusDetails tokenSelected;
            SettingsManagement setManagement = new SettingsManagement();
            ChannelManagement cManagement = new ChannelManagement();
            String channelid = session.getChannelid();
            Channels channel = cManagement.getChannelByID(channelid);
            if (channel == null) {
                return null;
            }
            Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                        OperatorsManagement oManagement2 = new OperatorsManagement();
                        Operators[] aOperator = oManagement2.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; ++i) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == 1) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }
                                SendNotification send = new SendNotification();
                                AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        suTemplate.close();
                        sTemplate.close();
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                    aStatus.error = "INVALID IP REQUEST";
                    aStatus.errorcode = -8;
                    return aStatus;
                }
            }
            if ((tokenSelected = (oManagement = new OTPTokenManagement(session.getChannelid())).getToken(sessionid, session.getChannelid(), userid, category)) == null) {
                aStatus.errorcode = -2;
                aStatus.error = "Desired Token is not assigned";
                return aStatus;
            }
            if (tokenSelected.Status == this.TOKEN_STATUS_ACTIVE) {
                oldValue = this.strACTIVE;
            } else if (tokenSelected.Status == this.TOKEN_STATUS_SUSPENDED) {
                oldValue = this.strSUSPENDED;
            } else if (tokenSelected.Status == this.TOKEN_STATUS_UNASSIGNED) {
                oldValue = this.strUNASSIGNED;
            }
            if (value == this.TOKEN_STATUS_ACTIVE) {
                strValue = this.strACTIVE;
            } else if (value == this.TOKEN_STATUS_SUSPENDED) {
                strValue = this.strSUSPENDED;
            } else if (value == this.TOKEN_STATUS_UNASSIGNED) {
                strValue = this.strUNASSIGNED;
            }
            int retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), userid, value, category, tokenSelected.SubCategory);
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            if (retValue == 0) {
                aStatus.error = "SUCCESS";
                aStatus.errorcode = 0;
            } else if (retValue == -6) {
                aStatus.error = "Status your trying to change is same as current status";
                aStatus.errorcode = -6;
            }
        }
        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(session.getChannelid());
        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "CHANGE STATUS", aStatus.error, aStatus.errorcode, "Token Management", "Current Status=" + oldValue, "New Status=" + strValue, "OTPTOKENS", userid);
        return aStatus;
    }

    public AxiomMessage[] SendMessages(String sessionId, AxiomMessage[] msgs, boolean bCheckContent, int speedType, int type) throws AxiomException {
        int SMS = 1;
        int USSD = 2;
        int VOICE = 3;
        int EMAIL = 4;
        try {
            if (msgs == null || msgs.length == 0) {
                throw new AxiomException("Invalid Messages!!!");
            }
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                throw new AxiomException("Invalid Session");
            }
            BulkMSGManagement bMSGManagement = new BulkMSGManagement();
            int check = 0;
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                throw new AxiomException("Invalid Channel");
            }
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                throw new AxiomException("Invalid IP");
                            }
                            suTemplate.close();
                            sTemplate.close();
                            throw new AxiomException("Invalid IP");
                        }
                        throw new AxiomException("Invalid IP");
                    }
                }
                int result = -1;
                int speed = 0;
                Object settingsObj = setManagement.getSetting(sessionId, session.getChannelid(), 10, 1);
                if (settingsObj != null) {
                    GlobalChannelSettings globalObj = (GlobalChannelSettings) settingsObj;
                    if (speedType == 1) {
                        if (type == EMAIL) {
                            speed = globalObj.emailsettingobj._slowPaceEMAIL;
                        } else if (type == SMS) {
                            speed = globalObj.smssettingobj._slowPaceSMS;
                        } else if (type == VOICE) {
                            speed = globalObj.voicesettingobj._slowPaceVOICE;
                        } else if (type == USSD) {
                            speed = globalObj.ussdsettingobj._slowPaceUSSD;
                        }
                    } else if (speedType == 2) {
                        if (type == EMAIL) {
                            speed = globalObj.emailsettingobj._normalPaceEMAIL;
                        } else if (type == SMS) {
                            speed = globalObj.smssettingobj._normalPaceSMS;
                        } else if (type == VOICE) {
                            speed = globalObj.voicesettingobj._normalPaceVOICE;
                        } else if (type == USSD) {
                            speed = globalObj.ussdsettingobj._normalPaceUSSD;
                        }
                    } else if (speedType == 3) {
                        if (type == EMAIL) {
                            speed = globalObj.emailsettingobj._fastPaceEMAIL;
                        } else if (type == SMS) {
                            speed = globalObj.smssettingobj._fastPaceSMS;
                        } else if (type == VOICE) {
                            speed = globalObj.voicesettingobj._fastPaceVOICE;
                        } else if (type == USSD) {
                            speed = globalObj.ussdsettingobj._fastPaceUSSD;
                        }
                    } else if (speedType == 4) {
                        if (type == EMAIL) {
                            speed = globalObj.emailsettingobj._hyperPaceEMAIL;
                        } else if (type == SMS) {
                            speed = globalObj.smssettingobj._hyperPaceSMS;
                        } else if (type == VOICE) {
                            speed = globalObj.voicesettingobj._hyperPaceVOICE;
                        } else if (type == USSD) {
                            speed = globalObj.ussdsettingobj._hyperPaceUSSD;
                        }
                    }
                } else {
                    throw new AxiomException("Configuraiton Missing!!!");
                }
                com.mollatech.axiom.nucleus.db.operation.AxiomMessage[] aMsg = new com.mollatech.axiom.nucleus.db.operation.AxiomMessage[msgs.length];
                String res = null;
                String uniqueid = req.getSession().getId() + channel.getChannelid() + new Date().getTime() + sessionId;
                String uniqueidforbulk = new String(Base64.encode((byte[]) this.SHA1(uniqueid)));
                for (int i = 0; i < msgs.length; ++i) {
                    if ((msgs[i] == null || msgs[i].message == null || msgs[i].number == null || msgs[i].message.isEmpty() || msgs[i].number.isEmpty()) && (msgs[i] == null || msgs[i].message == null || msgs[i].emailid == null || msgs[i].subject == null || msgs[i].message.isEmpty() || msgs[i].emailid.isEmpty() || msgs[i].subject.isEmpty())) {
                        msgs[i].status = new AxiomStatus();
                        msgs[i].status.error = this.SEND_MESSAGE_INVALID_DATA_STRING;
                        msgs[i].status.errorcode = this.SEND_MESSAGE_INVALID_DATA;
                        check = -1;
                        continue;
                    }
                    if (bCheckContent) {
                        if (settingsObj != null) {
                            GlobalChannelSettings globalObj = (GlobalChannelSettings) settingsObj;
                            res = setManagement.checkcontent(session.getChannelid(), msgs[i].message, globalObj);
                        } else {
                            res = null;
                        }
                        if (res == null) {
                            try {
                                aMsg[i] = new com.mollatech.axiom.nucleus.db.operation.AxiomMessage();
                                aMsg[i].phone = msgs[i].number;
                                aMsg[i].message = msgs[i].message;
                                aMsg[i].emailid = msgs[i].emailid;
                                aMsg[i].subject = msgs[i].subject;
                                aMsg[i].bcc = msgs[i].bcc;
                                aMsg[i].cc = msgs[i].cc;
                                aMsg[i].filenames = msgs[i].filenames;
                                aMsg[i].mimetypes = msgs[i].mimetypes;
                                msgs[i].status = new AxiomStatus();
                                msgs[i].status.error = this.SEND_MESSAGE_PENDING_STATE_STRING;
                                msgs[i].status.errorcode = 0;
                                int retValue = bMSGManagement.ADDAxiomMessage(sessionId, session.getChannelid(), null, aMsg[i], this.SEND_MESSAGE_PENDING_STATE, new Date(), new Date(), type, speed, uniqueidforbulk);
                                msgs[i].msgid = "" + retValue;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            continue;
                        }
                        try {
                            aMsg[i] = new com.mollatech.axiom.nucleus.db.operation.AxiomMessage();
                            aMsg[i].phone = msgs[i].number;
                            aMsg[i].message = msgs[i].message;
                            aMsg[i].emailid = msgs[i].emailid;
                            aMsg[i].subject = msgs[i].subject;
                            aMsg[i].bcc = msgs[i].bcc;
                            aMsg[i].cc = msgs[i].cc;
                            aMsg[i].filenames = msgs[i].filenames;
                            aMsg[i].mimetypes = msgs[i].mimetypes;
                            msgs[i].status = new AxiomStatus();
                            msgs[i].status.error = this.SEND_MESSAGE_BLOCKED_STATE_STRING;
                            msgs[i].status.errorcode = this.SEND_MESSAGE_BLOCKED_STATE;
                            int retValue = bMSGManagement.ADDAxiomMessage(sessionId, session.getChannelid(), null, aMsg[i], this.SEND_MESSAGE_BLOCKED_STATE, new Date(), new Date(), type, speed, uniqueidforbulk);
                            msgs[i].msgid = "" + retValue;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        continue;
                    }
                    try {
                        aMsg[i] = new com.mollatech.axiom.nucleus.db.operation.AxiomMessage();
                        aMsg[i].phone = msgs[i].number;
                        aMsg[i].message = msgs[i].message;
                        aMsg[i].emailid = msgs[i].emailid;
                        aMsg[i].subject = msgs[i].subject;
                        aMsg[i].bcc = msgs[i].bcc;
                        aMsg[i].cc = msgs[i].cc;
                        aMsg[i].filenames = msgs[i].filenames;
                        aMsg[i].mimetypes = msgs[i].mimetypes;
                        msgs[i].status = new AxiomStatus();
                        msgs[i].status.error = this.SEND_MESSAGE_PENDING_STATE_STRING;
                        msgs[i].status.errorcode = 0;
                        int retValue = bMSGManagement.ADDAxiomMessage(sessionId, session.getChannelid(), null, aMsg[i], this.SEND_MESSAGE_PENDING_STATE, new Date(), new Date(), type, speed, uniqueidforbulk);
                        msgs[i].msgid = "" + retValue;
                        continue;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (check == 0) {
                    result = bMSGManagement.SendMSG(sessionId, session.getChannelid(), type, speed, uniqueidforbulk);
                }
                String strTYPE = null;
                if (type == 1) {
                    strTYPE = "SMS";
                } else if (type == 2) {
                    strTYPE = "USSD";
                } else if (type == 3) {
                    strTYPE = "VOICE";
                } else if (type == 4) {
                    strTYPE = "EMAIL";
                }
                String resultstr = "Failure";
                if (result == 0) {
                    resultstr = "Success";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "BULK SEND", resultstr, result, "Messages", "Total Count=" + msgs.length, "", strTYPE, channel.getChannelid());
                } else if (result != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "BULK SEND", resultstr, result, "Messages", "Total Count=" + msgs.length, "", strTYPE, channel.getChannelid());
                }
            }
            return msgs;
        } catch (Exception e) {
            throw new AxiomException("System Internal Exception::" + e.getMessage());
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public ConnectorStatus GetConnectorsStatus(String channelid, String loginid, String password) {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug((Object) ("GetConnectorsStatus::channelid::" + channelid));
                log.debug((Object) ("GetConnectorsStatus::loginid::" + loginid));
                log.debug((Object) ("GetConnectorsStatus::password::" + password));
            }
        } catch (Exception var5_5) {
            // empty catch block
        }
        SessionFactoryUtil remoteAccess = new SessionFactoryUtil(5);
        Session remote = remoteAccess.openSession();
        ConnectorStatus cStatus = new ConnectorStatus();
        try {
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            SettingsManagement setManagement = new SettingsManagement();
            int result = setManagement.checkIP(channelid, req.getRemoteAddr());
            if (result != 1) {
                cStatus.CertificateConnector = this.INVALID_IP;
                cStatus.EMAILPrimary = this.INVALID_IP;
                cStatus.EMAILSecondary = this.INVALID_IP;
                cStatus.SMSPrimary = this.INVALID_IP;
                cStatus.SMSSecondary = this.INVALID_IP;
                cStatus.USSDPrimary = this.INVALID_IP;
                cStatus.USSDSecondary = this.INVALID_IP;
                cStatus.VOICEPrimary = this.INVALID_IP;
                cStatus.VOICESecondary = this.INVALID_IP;
                cStatus.channelid = channelid;
                ConnectorStatus connectorStatus = cStatus;
                return connectorStatus;
            }
            RemoteAccessUtils rUtil = new RemoteAccessUtils(remoteAccess, remote);
            ConnectorStatusListener cStatusListener = new ConnectorStatusListener();
            ConnectorStatusInternal cInternal = cStatusListener.CheckConnectorStatus(channelid);
            Remoteaccess rAccess = rUtil.getRemoteaccess(loginid, channelid);
            if (rAccess == null) {
                cStatus.CertificateConnector = this.INVALID_REMOTEUSER;
                cStatus.EMAILPrimary = this.INVALID_REMOTEUSER;
                cStatus.EMAILSecondary = this.INVALID_REMOTEUSER;
                cStatus.SMSPrimary = this.INVALID_REMOTEUSER;
                cStatus.SMSSecondary = this.INVALID_REMOTEUSER;
                cStatus.USSDPrimary = this.INVALID_REMOTEUSER;
                cStatus.USSDSecondary = this.INVALID_REMOTEUSER;
                cStatus.VOICEPrimary = this.INVALID_REMOTEUSER;
                cStatus.VOICESecondary = this.INVALID_REMOTEUSER;
                cStatus.channelid = channelid;
                ConnectorStatus connectorStatus = cStatus;
                return connectorStatus;
            }
            if (!rAccess.getPassword().equals(password)) {
                cStatus.CertificateConnector = this.INVALID_REMOTEUSER;
                cStatus.EMAILPrimary = this.INVALID_REMOTEUSER;
                cStatus.EMAILSecondary = this.INVALID_REMOTEUSER;
                cStatus.SMSPrimary = this.INVALID_REMOTEUSER;
                cStatus.SMSSecondary = this.INVALID_REMOTEUSER;
                cStatus.USSDPrimary = this.INVALID_REMOTEUSER;
                cStatus.USSDSecondary = this.INVALID_REMOTEUSER;
                cStatus.VOICEPrimary = this.INVALID_REMOTEUSER;
                cStatus.VOICESecondary = this.INVALID_REMOTEUSER;
                cStatus.channelid = channelid;
                ConnectorStatus connectorStatus = cStatus;
                return connectorStatus;
            }
            if (cInternal == null) {
                ConnectorStatus connectorStatus = cStatus;
                return connectorStatus;
            }
            cStatus.CertificateConnector = cInternal.CertificateConnector;
            cStatus.EMAILPrimary = cInternal.EMAILPrimary;
            cStatus.EMAILSecondary = cInternal.EMAILSecondary;
            cStatus.SMSPrimary = cInternal.SMSPrimary;
            cStatus.SMSSecondary = cInternal.SMSSecondary;
            cStatus.USSDPrimary = cInternal.USSDPrimary;
            cStatus.USSDSecondary = cInternal.USSDSecondary;
            cStatus.VOICEPrimary = cInternal.VOICEPrimary;
            cStatus.VOICESecondary = cInternal.VOICESecondary;
            cStatus.channelid = channelid;
            ConnectorStatus connectorStatus = cStatus;
            return connectorStatus;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            remoteAccess.close();
            remote.close();
        }
        return cStatus;
    }

    public AxiomStatus AssignTokenAndGenerateCertificate(String sessionId, String userid, int type, int subtype, String srno) {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug((Object) ("AssignTokenAndGenerateCertificate::sessionId::" + sessionId));
                log.debug((Object) ("AssignTokenAndGenerateCertificate::userid::" + userid));
                log.debug((Object) ("AssignTokenAndGenerateCertificate::type::" + type));
                log.debug((Object) ("AssignTokenAndGenerateCertificate::subtype::" + subtype));
                log.debug((Object) ("AssignTokenAndGenerateCertificate::srno::" + srno));
            }
        } catch (Exception var7_7) {
            // empty catch block
        }
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = this.wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
        Sessions session = sManagement.getSessionById(sessionId);
        if (session == null) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
            return aStatus;
        }
        AxiomStatus aStatus = new AxiomStatus();
        int retValue = -1;
        aStatus.error = "ERROR";
        aStatus.errorcode = retValue;
        String strCategory = "";
        String strSubCategory = "";
        if (type == this.RESET_USER_TOKEN_SOFTWARE) {
            strCategory = "SOFTWARE_TOKEN";
            if (subtype == this.OTP_TOKEN_SOFTWARE_WEB) {
                strSubCategory = "SW_WEB_TOKEN";
            } else if (subtype == this.OTP_TOKEN_SOFTWARE_MOBILE) {
                strSubCategory = "SW_MOBILE_TOKEN";
            } else if (subtype == this.OTP_TOKEN_SOFTWARE_PC) {
                strSubCategory = "SW_PC_TOKEN";
            } else if (session == null) {
                return aStatus;
            }
        } else if (type == this.RESET_USER_TOKEN_HARDWARE) {
            strCategory = "HARDWARE_TOKEN";
            if (subtype == this.OTP_TOKEN_HARDWARE_CR) {
                strSubCategory = "HW_CR_TOKEN";
            } else if (subtype == this.OTP_TOKEN_HARDWARE_MINI) {
                strSubCategory = "HW_MINI_TOKEN";
            }
        } else if (type == this.RESET_USER_TOKEN_OOB) {
            strCategory = "OOB_TOKEN";
            if (subtype == this.OTP_TOKEN_OUTOFBAND_SMS) {
                strSubCategory = "OOB__SMS_TOKEN";
            } else if (subtype == this.OTP_TOKEN_OUTOFBAND_USSD) {
                strSubCategory = "OOB__USSD_TOKEN";
            } else if (subtype == this.OTP_TOKEN_OUTOFBAND_VOICE) {
                strSubCategory = "OOB__VOICE_TOKEN";
            } else if (subtype == this.OTP_TOKEN_OUTOFBAND_EMAIL) {
                strSubCategory = "OOB__EMAIL_TOKEN";
            }
        }
        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(session.getChannelid());
        if (channel == null) {
            return aStatus;
        }
        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
        if (session != null) {
            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                        OperatorsManagement oManagementObj = new OperatorsManagement();
                        Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; ++i) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == 1) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }
                                SendNotification send = new SendNotification();
                                AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        suTemplate.close();
                        sTemplate.close();
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                    aStatus.error = "INVALID IP REQUEST";
                    aStatus.errorcode = -8;
                    return aStatus;
                }
            }
            if ((retValue = oManagement.AssignTokenAndIssueCert(sessionId, session.getChannelid(), userid, type, subtype, srno)) == 0) {
                aStatus.error = "SUCCESS";
                aStatus.errorcode = 0;
            } else if (retValue == -7) {
                aStatus.error = "Assign OTP Token failed!!!";
                aStatus.errorcode = -7;
            } else if (retValue == -8) {
                aStatus.error = "Certificate issuance failed!!!";
                aStatus.errorcode = -8;
            }
        }
        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "Assign", aStatus.error, aStatus.errorcode, "Token Management", "", "Category = " + strCategory + " Sub Category =" + strSubCategory, "OTPTOKENS", userid);
        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "Issue", aStatus.error, aStatus.errorcode, "Certificate Management", "", "Category = " + strCategory + " Sub Category =" + strSubCategory, "CERTIFICATE", userid);
        return aStatus;
    }

    public AxiomData VerifySignature(String sessionid, String userid, String SignedData, String Signature, int docType) {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug((Object) ("VerifySignature::sessionid::" + sessionid));
                log.debug((Object) ("VerifySignature::userid::" + userid));
                log.debug((Object) ("VerifySignature::SignedData::" + SignedData));
                log.debug((Object) ("VerifySignature::Signature::" + Signature));
                log.debug((Object) ("VerifySignature::docType::" + docType));
            }
        } catch (Exception var7_7) {
            // empty catch block
        }
        AxiomData aData = new AxiomData();
        try {
            SessionManagement sManagement = new SessionManagement();
            UserManagement uManagement = new UserManagement();
            CryptoManagement cyManagement = new CryptoManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionid);
            int retValue = -1;
            aData.sErrorMessage = "ERROR";
            aData.iErrorCode = retValue;
            if (session == null) {
                aData.sErrorMessage = "Invalid Session";
                aData.iErrorCode = -14;
                return aData;
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                aData.sErrorMessage = "Invalid Channel";
                aData.iErrorCode = -15;
                return aData;
            }
            if (session != null) {
                Certificates cert;
                CertificateManagement certManagement;
                AuthUser aUser = uManagement.getUser(sessionid, channel.getChannelid(), userid);
                if (aUser == null) {
                    aData.sErrorMessage = "INVALID USER";
                    aData.iErrorCode = -10;
                    return aData;
                }
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                AxiomData aStatus = new AxiomData();
                                aStatus.sErrorMessage = "INVALID IP REQUEST";
                                aStatus.iErrorCode = -8;
                                return aStatus;
                            }
                            suTemplate.close();
                            sTemplate.close();
                            AxiomData aStatus = new AxiomData();
                            aStatus.sErrorMessage = "INVALID IP REQUEST";
                            aStatus.iErrorCode = -8;
                            return aStatus;
                        }
                        AxiomData aStatus = new AxiomData();
                        aStatus.sErrorMessage = "INVALID IP REQUEST";
                        aStatus.iErrorCode = -8;
                        return aStatus;
                    }
                }
                if ((cert = (certManagement = new CertificateManagement()).getCertificate(sessionid, session.getChannelid(), userid)) == null) {
                    aData.sErrorMessage = "Certificate Not Issued!!";
                    aData.iErrorCode = -7;
                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "Get Certificate", aData.sErrorMessage, aData.iErrorCode, "Certificate Management", "", "Certificate Not Found", "Get Certificate", userid);
                    return aData;
                }
                retValue = cyManagement.VerifySignature1(SignedData, Signature, cert.getCertificate());
                if (retValue == 0) {
                    aData.sErrorMessage = "Success";
                    aData.iErrorCode = retValue;
                }
            }
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "Verify Signature ", aData.sErrorMessage, aData.iErrorCode, "Verify Signature", "signature = ******", " signature = ******", "Verify Signature", userid);
            return aData;
        } catch (Exception e) {
            e.printStackTrace();
            aData.sErrorMessage = "Exception::" + e.getMessage();
            aData.iErrorCode = -20;
            return aData;
        }
    }

    public MessageReport[] FetchStatus(String sessionId, String phonNo, String strstartDate, String strendDate) throws AxiomException {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug((Object) ("FetchStatus::sessionId::" + sessionId));
                log.debug((Object) ("FetchStatus::phonNo::" + phonNo));
                log.debug((Object) ("FetchStatus::strstartDate::" + strstartDate));
                log.debug((Object) ("FetchStatus::strendDate::" + strendDate));
            }
        } catch (Exception var6_6) {
            // empty catch block
        }
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = this.wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
        Sessions session = sManagement.getSessionById(sessionId);
        if (session == null) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
            throw new AxiomException("Invalid Session!!!");
        }
        try {
            if (phonNo == null || strstartDate == null || strendDate == null) {
                throw new AxiomException("Invalid Details!!!");
            }
            BulkMSGManagement bMSGManagement = new BulkMSGManagement();
            boolean check = false;
            MessageReport[] mReport = null;
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                throw new AxiomException("Invalid Channel");
            }
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                throw new AxiomException("Invalid IP!!!");
                            }
                            suTemplate.close();
                            sTemplate.close();
                            throw new AxiomException("Invalid IP!!!");
                        }
                        throw new AxiomException("Invalid IP!!!");
                    }
                }
                BulkMSGManagement bulk = new BulkMSGManagement();
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy/hh:mm:ss");
                Date startDate = null;
                if (strstartDate != null && !strstartDate.isEmpty()) {
                    startDate = formatter.parse(strstartDate);
                }
                Date endDate = null;
                if (strendDate != null && !strendDate.isEmpty()) {
                    endDate = formatter.parse(strendDate);
                }
                Channellogs[] cObj = bulk.FetchStatus(session.getChannelid(), phonNo, startDate, endDate);
                mReport = new MessageReport[cObj.length];
                for (int i = 0; i < cObj.length; ++i) {
                    mReport[i] = new MessageReport();
                    mReport[i].number = cObj[i].getPhone();
                    mReport[i].msgid = cObj[i].getMsgid();
                    mReport[i].status = new AxiomStatus();
                    mReport[i].status.error = cObj[i].getErrorstatus();
                    mReport[i].status.errorcode = cObj[i].getStatus();
                    mReport[i].sentDate = cObj[i].getSentUtctime().getTime();
                }
                return mReport;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new AxiomException(ex.getMessage());
        }
        return null;
    }

    public AxiomStatus ChangeUserDetails(String sessionId, String userid, String fullname, String phone, String email) {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug((Object) ("ChangeUserDetails::sessionId::" + sessionId));
                log.debug((Object) ("ChangeUserDetails::userid::" + userid));
                log.debug((Object) ("ChangeUserDetails::fullname::" + fullname));
                log.debug((Object) ("ChangeUserDetails::phone::" + phone));
                log.debug((Object) ("ChangeUserDetails::email::" + email));
            }
        } catch (Exception var7_7) {
            // empty catch block
        }
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = this.wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
        Sessions session = sManagement.getSessionById(sessionId);
        if (session == null) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
            return aStatus;
        }
        try {
            UserManagement uManagement = new UserManagement();
            AxiomStatus aStatus = new AxiomStatus();
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            AuthUser oldUser = null;
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                ChannelManagement cManagement = new ChannelManagement();
                String channelid = session.getChannelid();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    aStatus.error = "INVALID CHANNEL";
                    aStatus.errorcode = -3;
                    return aStatus;
                }
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }
                if ((retValue = uManagement.EditUser(sessionId, session.getChannelid(), userid, fullname, phone, email, 0, null, null, null, null, null, null, null, null)) == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                } else if (retValue == 1) {
                    aStatus.error = "User Details Changed Succesfully but email could not be sent!!!";
                    aStatus.errorcode = 0;
                }
                oldUser = uManagement.getUser(sessionId, session.getChannelid(), userid);
            }
            String oldUserName = "";
            String oldPhoneNo = "";
            String oldEmailID = "";
            String oldPasswordState = "";
            if (oldUser != null) {
                oldUserName = oldUser.getUserName();
                oldPhoneNo = oldUser.getPhoneNo();
                oldEmailID = oldUser.getEmail();
                oldPasswordState = "" + oldUser.getStatePassword();
            }
            String itemtype = "USERPASSWORD";
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "Edit User", aStatus.error, aStatus.errorcode, "User Management", "User Name = " + oldUserName + "User Phone =" + oldPhoneNo + "User Email =" + oldEmailID + "State Of Password =" + oldPasswordState, "User Name = " + fullname + "User Phone =" + phone + "User Email =" + email + "State Of Password =" + -1, itemtype, "");
            return aStatus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public AxiomStatus DeleteUser(String sessionId, String userid) {
        try {
            String strDebug = null;
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    log.debug((Object) ("DeleteUser::sessionId::" + sessionId));
                    log.debug((Object) ("DeleteUser::userid::" + userid));
                }
            } catch (Exception var4_5) {
                // empty catch block
            }
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                return aStatus;
            }
            UserManagement uManagement = new UserManagement();
            AxiomStatus aStatus = new AxiomStatus();
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            AuthUser oldUser = null;
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                ChannelManagement cManagement = new ChannelManagement();
                String channelid = session.getChannelid();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    aStatus.error = "INVALID CHANNEL";
                    aStatus.errorcode = -3;
                    return aStatus;
                }
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }
                if ((retValue = uManagement.DeleteUser(sessionId, session.getChannelid(), userid)) == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                }
                oldUser = uManagement.getUser(sessionId, session.getChannelid(), userid);
            }
            String oldUserName = "";
            String oldPhoneNo = "";
            String oldEmailID = "";
            String oldPasswordState = "";
            if (oldUser != null) {
                oldUserName = oldUser.getUserName();
                oldPhoneNo = oldUser.getPhoneNo();
                oldEmailID = oldUser.getEmail();
                oldPasswordState = "" + oldUser.getStatePassword();
            }
            String itemtype = "USERPASSWORD";
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (retValue == 0) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "Delete User", aStatus.error, aStatus.errorcode, "User Management", "User Name = " + oldUserName + "User Phone =" + oldPhoneNo + "User Email =" + oldEmailID + "State Of Password =" + oldPasswordState, "User Deleted Successfully", itemtype, "");
                return aStatus;
            }
            String reason = "User Deletion Failed";
            reason = retValue == -10 ? reason + "User is assigned token/certificate, therefore cannot be removed!!!" : reason + "!!!";
            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "Delete User", aStatus.error, aStatus.errorcode, "User Management", "User Name = " + oldUserName + "User Phone =" + oldPhoneNo + "User Email =" + oldEmailID + "State Of Password =" + oldPasswordState, reason, itemtype, "");
            return aStatus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public AxiomStatus IsUserRegistered(String sessionid, String userid) {
        try {
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionid);
            if (session == null) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                return aStatus;
            }
            UserManagement uManagement = new UserManagement();
            AxiomStatus aStatus = new AxiomStatus();
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            if (session != null) {
                ChallengeResponsemanagement chManagment;
                Questionsandanswers qa;
                SettingsManagement setManagement = new SettingsManagement();
                ChannelManagement cManagement = new ChannelManagement();
                String channelid = session.getChannelid();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    aStatus.error = "INVALID CHANNEL";
                    aStatus.errorcode = -3;
                    return aStatus;
                }
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }
                if ((qa = (chManagment = new ChallengeResponsemanagement()).getRegisterUser(sessionid, session.getChannelid(), userid)) != null) {
                    if (qa.getStatus() == 1) {
                        aStatus.error = "Status is Active";
                        aStatus.errorcode = 0;
                    } else if (qa.getStatus() == 0) {
                        aStatus.error = "Status is Suspended";
                        aStatus.errorcode = -1;
                    } else if (qa.getStatus() == -1) {
                        aStatus.error = "Status is Locked";
                        aStatus.errorcode = -3;
                    }
                }
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "IsUserRegistered", aStatus.error, aStatus.errorcode, "ChallengeResponse Management", "", "", "CHECKREGISTERUSER", "");
            return aStatus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public AxiomChallengeResponse getQuestionsForRegistration(String sessionid, int noOfQuestion) throws AxiomException {
        try {
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionid);
            if (session == null) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                throw new AxiomException("Invalid Session!!!");
            }
            UserManagement uManagement = new UserManagement();
            AxiomStatus aStatus = new AxiomStatus();
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            AxiomQuestionsAndAnswers questions = null;
            AxiomChallengeResponse webQuestions = null;
            if (session != null) {
                QuestionsManagement qManagment;
                SettingsManagement setManagement = new SettingsManagement();
                ChannelManagement cManagement = new ChannelManagement();
                String channelid = session.getChannelid();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    throw new AxiomException("Invalid Channel!!!");
                }
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                throw new AxiomException("Invalid Ip!!!");
                            }
                            suTemplate.close();
                            sTemplate.close();
                            throw new AxiomException("Invalid IP!!!");
                        }
                        throw new AxiomException("Invalid IP!!!");
                    }
                }
                if ((questions = (qManagment = new QuestionsManagement()).getRandomQuestions(sessionid, session.getChannelid(), noOfQuestion)) != null) {
                    webQuestions = new AxiomChallengeResponse();
                    webQuestions.webQAndA = questions.webQAndA;
                    retValue = 0;
                }
                if (retValue == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                }
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "GetQuestionForRegistration", aStatus.error, aStatus.errorcode, "ChallengeResponse Management", "", "", "GetRandomQuestions", "");
            return webQuestions;
        } catch (Exception e) {
            e.printStackTrace();
            throw new AxiomException(e.getMessage());
        }
    }

    public AxiomStatus registerAnswersForUser(String sessionid, String userid, AxiomChallengeResponse QAndA) {
        try {
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionid);
            if (session == null) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                return aStatus;
            }
            UserManagement uManagement = new UserManagement();
            AxiomStatus aStatus = new AxiomStatus();
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            Object questions = null;
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                ChannelManagement cManagement = new ChannelManagement();
                String channelid = session.getChannelid();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    aStatus.error = "INVALID CHANNEL";
                    aStatus.errorcode = -3;
                    return aStatus;
                }
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }
                ChallengeResponsemanagement chManagment = new ChallengeResponsemanagement();
                AxiomQuestionsAndAnswers axiomQAndA = new AxiomQuestionsAndAnswers();
                axiomQAndA.webQAndA = QAndA.webQAndA;
                Questionsandanswers qA = chManagment.getRegisterUser(sessionid, session.getChannelid(), userid);
                if (qA != null) {
                    aStatus.error = "User already Registered...";
                    aStatus.errorcode = -6;
                    retValue = -6;
                } else {
                    retValue = chManagment.RegisterUser(sessionid, session.getChannelid(), userid, axiomQAndA);
                }
                if (retValue == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                }
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "RegisterUser", aStatus.error, aStatus.errorcode, "ChallengeResponse Management", "", "userid = " + userid + ",QuestionsAndAnswers = *********", "REGISTERUSER", "");
            return aStatus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public AxiomStatus registerAnswersForUserAndGenerateCertificate(String sessionid, String userid, AxiomChallengeResponse QAndA) {
        try {
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionid);
            if (session == null) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                return aStatus;
            }
            AxiomStatus aStatus = new AxiomStatus();
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            Object questions = null;
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                ChannelManagement cManagement = new ChannelManagement();
                String channelid = session.getChannelid();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    aStatus.error = "INVALID CHANNEL";
                    aStatus.errorcode = -3;
                    return aStatus;
                }
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }
                ChallengeResponsemanagement chManagment = new ChallengeResponsemanagement();
                AxiomQuestionsAndAnswers axiomQAndA = new AxiomQuestionsAndAnswers();
                axiomQAndA.webQAndA = QAndA.webQAndA;
                Questionsandanswers qA = chManagment.getRegisterUser(sessionid, session.getChannelid(), userid);
                if (qA != null) {
                    aStatus.error = "User already Registered...";
                    aStatus.errorcode = -6;
                    retValue = -6;
                } else {
                    retValue = chManagment.RegisterUser(sessionid, session.getChannelid(), userid, axiomQAndA);
                }
                if (retValue == 0) {
                    CertificateManagement certManagement = new CertificateManagement();
                    String cert = certManagement.GenerateCertificate(sessionid, session.getChannelid(), userid);
                    if (cert != null) {
                        aStatus.error = "SUCCESS";
                        aStatus.errorcode = 0;
                    } else {
                        aStatus.error = "Failed to issue Certificate";
                        aStatus.errorcode = -9;
                    }
                }
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "RegisterUserandgenrateCertificate", aStatus.error, aStatus.errorcode, "ChallengeResponse Management", "", "userid = " + userid + ",QuestionsAndAnswers = *********", "REGISTERUSER&GENRATECERTIFICATE", "");
            return aStatus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public AxiomChallengeResponse getQuestionsForValidation(String sessionid, String userid) throws AxiomException {
        try {
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionid);
            if (session == null) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                throw new AxiomException("Invalid Session!!!");
            }
            UserManagement uManagement = new UserManagement();
            AxiomStatus aStatus = new AxiomStatus();
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            AxiomQuestionsAndAnswers questions = null;
            AxiomChallengeResponse webQAndA = null;
            if (session != null) {
                ChallengeResponsemanagement chManagment;
                SettingsManagement setManagement = new SettingsManagement();
                ChannelManagement cManagement = new ChannelManagement();
                String channelid = session.getChannelid();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    return null;
                }
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                throw new AxiomException("Invalid IP!!!");
                            }
                            suTemplate.close();
                            sTemplate.close();
                            throw new AxiomException("Invalid IP!!!");
                        }
                        throw new AxiomException("Invalid IP!!!");
                    }
                }
                if ((questions = (chManagment = new ChallengeResponsemanagement()).getUserQueAndAns(sessionid, session.getChannelid(), userid)) != null) {
                    webQAndA = new AxiomChallengeResponse();
                    webQAndA.webQAndA = questions.webQAndA;
                    retValue = 0;
                }
                if (retValue == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                }
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "GetQuestionForValidations", aStatus.error, aStatus.errorcode, "ChallengeResponse Management", "QuestionsAndAnswers = *********", "QuestionsAndAnswers = *********", "GetUserQuestionsAndAnswers", "");
            return webQAndA;
        } catch (Exception e) {
            e.printStackTrace();
            throw new AxiomException(e.getMessage());
        }
    }

    public AxiomStatus ValidateUserAnswers(String sessionid, String userid, AxiomChallengeResponse QAndA) {
        try {
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionid);
            if (session == null) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                return aStatus;
            }
            UserManagement uManagement = new UserManagement();
            AxiomStatus aStatus = new AxiomStatus();
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            Object questions = null;
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                ChannelManagement cManagement = new ChannelManagement();
                String channelid = session.getChannelid();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    return null;
                }
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }
                AxiomQuestionsAndAnswers axiomQandA = new AxiomQuestionsAndAnswers();
                axiomQandA.webQAndA = QAndA.webQAndA;
                ChallengeResponsemanagement chManagment = new ChallengeResponsemanagement();
                retValue = chManagment.ValidateUserAnswers(sessionid, session.getChannelid(), userid, axiomQandA);
                if (retValue == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                }
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "ValidateUserAnswers", aStatus.error, aStatus.errorcode, "ChallengeResponse Management", "userid =" + userid, "userid = " + userid, "ValidateUserAnswers", "");
            return aStatus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public AxiomStatus changeAnswersForUser(String sessionid, String userid, AxiomChallengeResponse QAndA) {
        try {
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionid);
            if (session == null) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                return aStatus;
            }
            UserManagement uManagement = new UserManagement();
            AxiomStatus aStatus = new AxiomStatus();
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            Object questions = null;
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                ChannelManagement cManagement = new ChannelManagement();
                String channelid = session.getChannelid();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    aStatus.error = "INVALID CHANNEL";
                    aStatus.errorcode = -3;
                    return aStatus;
                }
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }
                AxiomQuestionsAndAnswers axiomQandA = new AxiomQuestionsAndAnswers();
                axiomQandA.webQAndA = QAndA.webQAndA;
                ChallengeResponsemanagement chManagment = new ChallengeResponsemanagement();
                retValue = chManagment.UpdateRegisterUser(sessionid, session.getChannelid(), userid, axiomQandA);
                if (retValue == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                }
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "changesAnswersForUser", aStatus.error, aStatus.errorcode, "ChallengeResponse Management", "userid =" + userid, "userid = " + userid, "ChangeUsersAnswers", "");
            return aStatus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public AxiomStatus ValidateUserAnswersAndResetUser(String sessionid, String userid, AxiomChallengeResponse QAndA, int type) {
        try {
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionid);
            if (session == null) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                return aStatus;
            }
            UserManagement uManagement = new UserManagement();
            AxiomStatus aStatus = new AxiomStatus();
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            Object questions = null;
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                ChannelManagement cManagement = new ChannelManagement();
                String channelid = session.getChannelid();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    aStatus.error = "INVALID CHANNEL";
                    aStatus.errorcode = -3;
                    return aStatus;
                }
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }
                AxiomQuestionsAndAnswers axiomQandA = new AxiomQuestionsAndAnswers();
                axiomQandA.webQAndA = QAndA.webQAndA;
                ChallengeResponsemanagement chManagment = new ChallengeResponsemanagement();
                retValue = chManagment.ValidateUserAnswers(sessionid, session.getChannelid(), userid, axiomQandA);
                if (retValue == 0) {
                    aStatus = this.ResetUser(sessionid, userid, type);
                    if (aStatus.errorcode == 0) {
                        retValue = 0;
                    }
                }
                if (retValue == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                }
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "ValidateUserAnswersAndResetUser", aStatus.error, aStatus.errorcode, "ChallengeResponse Management", "userid =" + userid, "userid = " + userid, "ValidateUserAnswersAndResetUser", "");
            return aStatus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public AxiomStatus ValidateUserAnswersAndChangePassword(String sessionid, String userid, String password, AxiomChallengeResponse QAndA, boolean bSendOnMobile, int type) {
        try {
            UserManagement uManagement = new UserManagement();
            SessionManagement sManagement = new SessionManagement();
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionid);
            AxiomStatus aStatus = new AxiomStatus();
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            Object questions = null;
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                ChannelManagement cManagement = new ChannelManagement();
                String channelid = session.getChannelid();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    aStatus.error = "INVALID CHANNEL";
                    aStatus.errorcode = -3;
                    return aStatus;
                }
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }
                AxiomQuestionsAndAnswers axiomQandA = new AxiomQuestionsAndAnswers();
                axiomQandA.webQAndA = QAndA.webQAndA;
                ChallengeResponsemanagement chManagment = new ChallengeResponsemanagement();
                retValue = chManagment.ValidateUserAnswers(sessionid, session.getChannelid(), userid, axiomQandA);
                if (retValue == 0) {
                    Object obj = setManagement.getSetting(session.getChannelid(), 21, 1);
                    PasswordPolicySetting pSettings = null;
                    if (obj != null) {
                        pSettings = (PasswordPolicySetting) obj;
                    }
                    if (pSettings != null && pSettings.allowCRQAforResetandReissue) {
                        aStatus = this.AssignPassword(sessionid, userid, password, bSendOnMobile, type);
                        if (aStatus.errorcode == 0) {
                            retValue = 0;
                        }
                    } else {
                        retValue = -4;
                        aStatus.error = "Password Policy enforcement not allowed";
                        aStatus.errorcode = -4;
                    }
                }
                if (retValue == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                }
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            AuditManagement audit = new AuditManagement();
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "ValidateUserAnswersAndChangePassword", aStatus.error, aStatus.errorcode, "ChallengeResponse Management", "userid =" + userid, "userid = " + userid, "ValidateUserAnswersAndChangePassword", "");
            return aStatus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public AxiomStatus ValidateRegistration(String sessionId, String userid, String challenge, String response) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private byte[] SHA1(String message) {
        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            byte[] array = sha1.digest(message.getBytes());
            return array;
        } catch (Exception e) {
            return null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public AxiomStatus AddTokens(String channelid, String userid, String regcode, String secrete, String serialno) {
        SessionFactoryUtil sUtil = new SessionFactoryUtil(4);
        Session session = sUtil.openSession();
        OtpTokensUtils oUtil = new OtpTokensUtils(sUtil, session);
        AxiomStatus aStatus = new AxiomStatus();
        aStatus.error = "Failed";
        aStatus.errorcode = -55;
        try {
            int result = oUtil.addOtpTokens(channelid, userid, serialno, secrete, 1, "OATH-TOTP", "OCRA-1:HOTP-SHA1-6:QN08", "OCRA-1:HOTP-SHA1-8:QA08", "OCRA-1:HOTP-SHA1-8:QA08", 0, new Date(), new Date(), 0, 1, 2, regcode, 0);
            if (result == 0) {
                aStatus.error = "Success";
                aStatus.errorcode = 0;
            }
            AxiomStatus axiomStatus = aStatus;
            return axiomStatus;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            sUtil.close();
            session.close();
        }
        return aStatus;
    }

    public AxiomStatus initiateRequestEPIN(String sessionId, String userid, int inChannel) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void AddTrailRA(String sessionid, String userid, String source, int type, String[][] txDetails, String ip, String[][] requestheaders, String[][] extendsDetails) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int IsAdditionalAuthenticationNeededRA(String sessionid, String userid) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public QuestionsAndAnswers GetQuestionsEPIN(String sessionid, String userid) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public AxiomStatus verifyAnswersAndSendEPIN(String sessionid, String userid, QuestionsAndAnswers QandA) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String EnforceSecurity(String sessionId, String userid, String data) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String ConsumeSecurity(String sessionId, String userid, String data) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public AxiomStatus ChangeTokenType(String sessionid, String userid, int type, int subType) {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug((Object) ("ChangeTokenType::sessionid::" + sessionid));
                log.debug((Object) ("ChangeTokenType::userid::" + userid));
                log.debug((Object) ("ChangeTokenType::category::" + type));
                log.debug((Object) ("ChangeTokenType::subCategory::" + subType));
            }
        } catch (Exception var6_6) {
            // empty catch block
        }
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = this.wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
        Sessions session = sManagement.getSessionById(sessionid);
        if (session == null) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
            return aStatus;
        }
        AxiomStatus aStatus = new AxiomStatus();
        aStatus.error = "ERROR";
        aStatus.errorcode = -2;
        String oldValue = "";
        String strValue = "";
        if (session != null) {
            OTPTokenManagement oManagement;
            TokenStatusDetails tokenSelected;
            SettingsManagement setManagement = new SettingsManagement();
            ChannelManagement cManagement = new ChannelManagement();
            String channelid = session.getChannelid();
            Channels channel = cManagement.getChannelByID(channelid);
            if (channel == null) {
                return null;
            }
            Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                        OperatorsManagement oManagement2 = new OperatorsManagement();
                        Operators[] aOperator = oManagement2.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; ++i) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == 1) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }
                                SendNotification send = new SendNotification();
                                AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        suTemplate.close();
                        sTemplate.close();
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                    aStatus.error = "INVALID IP REQUEST";
                    aStatus.errorcode = -8;
                    return aStatus;
                }
            }
            if ((tokenSelected = (oManagement = new OTPTokenManagement(session.getChannelid())).getToken(sessionid, session.getChannelid(), userid, type)) == null) {
                aStatus.errorcode = -2;
                aStatus.error = "Desired Token is not assigned";
                return aStatus;
            }
            if (tokenSelected.Catrgory == this.OTP_TOKEN_SOFTWARE) {
                oldValue = "SOFTWARE_TOKEN";
                if (tokenSelected.SubCategory == this.OTP_TOKEN_SOFTWARE_WEB) {
                    oldValue = "SW_WEB_TOKEN";
                } else if (tokenSelected.SubCategory == this.OTP_TOKEN_SOFTWARE_MOBILE) {
                    oldValue = "SW_MOBILE_TOKEN";
                } else if (tokenSelected.SubCategory == this.OTP_TOKEN_SOFTWARE_PC) {
                    oldValue = "SW_PC_TOKEN";
                } else if (session == null) {
                    return aStatus;
                }
            } else if (tokenSelected.Catrgory == this.OTP_TOKEN_HARDWARE) {
                oldValue = "HARDWARE_TOKEN";
                if (tokenSelected.SubCategory == this.OTP_TOKEN_HARDWARE_CR) {
                    oldValue = "HW_CR_TOKEN";
                } else if (tokenSelected.SubCategory == this.OTP_TOKEN_HARDWARE_MINI) {
                    oldValue = "HW_MINI_TOKEN";
                }
            } else if (tokenSelected.Catrgory == this.OTP_TOKEN_OUTOFBAND) {
                oldValue = "OOB_TOKEN";
                if (tokenSelected.SubCategory == this.OTP_TOKEN_OUTOFBAND_SMS) {
                    oldValue = "OOB__SMS_TOKEN";
                } else if (tokenSelected.SubCategory == this.OTP_TOKEN_OUTOFBAND_USSD) {
                    oldValue = "OOB__USSD_TOKEN";
                } else if (tokenSelected.SubCategory == this.OTP_TOKEN_OUTOFBAND_VOICE) {
                    oldValue = "OOB__VOICE_TOKEN";
                } else if (tokenSelected.SubCategory == this.OTP_TOKEN_OUTOFBAND_EMAIL) {
                    oldValue = "OOB__EMAIL_TOKEN";
                }
            }
            if (type == this.OTP_TOKEN_SOFTWARE) {
                strValue = "SOFTWARE_TOKEN";
                if (subType == this.OTP_TOKEN_SOFTWARE_WEB) {
                    strValue = "SW_WEB_TOKEN";
                } else if (subType == this.OTP_TOKEN_SOFTWARE_MOBILE) {
                    strValue = "SW_MOBILE_TOKEN";
                } else if (subType == this.OTP_TOKEN_SOFTWARE_PC) {
                    strValue = "SW_PC_TOKEN";
                } else if (session == null) {
                    return aStatus;
                }
            } else if (type == this.OTP_TOKEN_HARDWARE) {
                strValue = "HARDWARE_TOKEN";
                if (subType == this.OTP_TOKEN_HARDWARE_CR) {
                    strValue = "HW_CR_TOKEN";
                } else if (subType == this.OTP_TOKEN_HARDWARE_MINI) {
                    strValue = "HW_MINI_TOKEN";
                }
            } else if (type == this.OTP_TOKEN_OUTOFBAND) {
                strValue = "OOB_TOKEN";
                if (subType == this.OTP_TOKEN_OUTOFBAND_SMS) {
                    strValue = "OOB__SMS_TOKEN";
                } else if (subType == this.OTP_TOKEN_OUTOFBAND_USSD) {
                    strValue = "OOB__USSD_TOKEN";
                } else if (subType == this.OTP_TOKEN_OUTOFBAND_VOICE) {
                    strValue = "OOB__VOICE_TOKEN";
                } else if (subType == this.OTP_TOKEN_OUTOFBAND_EMAIL) {
                    strValue = "OOB__EMAIL_TOKEN";
                }
            }
            log.debug("  ChangeTokenType :: ChangeTokenType() called");
            int retValue = oManagement.ChangeTokenType(sessionid, session.getChannelid(), userid, type, subType);
             log.debug("  ChangeTokenType :: ChangeTokenType() ended");
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            if (retValue == 0) {
                aStatus.error = "SUCCESS";
                aStatus.errorcode = 0;
            } else {
                aStatus.error = "Error";
                aStatus.errorcode = -1;
            }
        }
        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(session.getChannelid());
        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "CHANGE Token Type", aStatus.error, aStatus.errorcode, "Token Management", "Current Token Type=" + oldValue, "New Token Type=" + strValue, "OTPTOKENS", userid);
        return aStatus;
    }

    public AxiomStatus AssignTokenV2(String sessionid, String userid, String serialNo, int type, int subtype) {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                Date d = new Date();
                log.debug((Object) (d + ">>AssignTokenV2::systemSessionId::" + sessionid));
                log.debug((Object) (d + ">>AssignTokenV2::userid::" + userid));
                log.debug((Object) (d + ">>AssignTokenV2::serialNo::" + serialNo));
            }
        } catch (Exception d) {
            // empty catch block
        }
        String strException = "";
        try {
            if (sessionid == null || userid == null || sessionid.isEmpty() || userid.isEmpty()) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Data";
                aStatus.errorcode = -11;
                return aStatus;
            }
            String strCategory = "";
            if (type == this.OTP_TOKEN_SOFTWARE) {
                strCategory = "SOFTWARE_TOKEN";
            } else if (type == this.OTP_TOKEN_HARDWARE) {
                strCategory = "HARDWARE_TOKEN";
            } else if (type == this.OTP_TOKEN_OUTOFBAND) {
                strCategory = "OOB_TOKEN";
            }
            String strSubCategory = "";
            if (subtype == this.OTP_TOKEN_OUTOFBAND_SMS) {
                strSubCategory = "OOB__SMS_TOKEN";
            } else if (subtype == this.OTP_TOKEN_OUTOFBAND_USSD) {
                strSubCategory = "OOB__USSD_TOKEN";
            } else if (subtype == this.OTP_TOKEN_OUTOFBAND_VOICE) {
                strSubCategory = "OOB__VOICE_TOKEN";
            } else if (subtype == this.OTP_TOKEN_OUTOFBAND_EMAIL) {
                strSubCategory = "OOB__EMAIL_TOKEN";
            } else if (subtype == this.OTP_TOKEN_SOFTWARE_WEB) {
                strSubCategory = "SW_WEB_TOKEN";
            } else if (subtype == this.OTP_TOKEN_SOFTWARE_MOBILE) {
                strSubCategory = "SW_MOBILE_TOKEN";
            } else if (subtype == this.OTP_TOKEN_SOFTWARE_PC) {
                strSubCategory = "SW_PC_TOKEN";
            } else if (subtype == this.OTP_TOKEN_HARDWARE_CR) {
                strSubCategory = "HW_CR_TOKEN";
            } else if (subtype == this.OTP_TOKEN_HARDWARE_MINI) {
                strSubCategory = "HW_MINI_TOKEN";
            } else {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid subcategory!!!";
                aStatus.errorcode = -13;
                return aStatus;
            }
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionid);
            AxiomStatus aStatus = new AxiomStatus();
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            if (session == null) {
                aStatus.error = "Invalid/Expired Session";
                aStatus.errorcode = -14;
                return aStatus;
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channels = cManagement.getChannelByID(session.getChannelid());
            if (channels == null) {
                aStatus.error = "Invalid Channel";
                aStatus.errorcode = -15;
                return aStatus;
            }
            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
            
                
            if (session != null) {
                TokenSettings token;
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                ChannelProfile channelprofileObj = null;
                Object channelpobj = setManagement.getSettingInner(session.getChannelid(), 20, 1);
                if (channelpobj == null) {
                    LoadSettings.LoadChannelProfile((ChannelProfile) channelprofileObj);
                } else {
                    channelprofileObj = (ChannelProfile) channelpobj;
                    LoadSettings.LoadChannelProfile((ChannelProfile) channelprofileObj);
                }
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagementObj = new OperatorsManagement();
                            Operators[] aOperator = oManagementObj.getAdminOperator(channels.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channels.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channels.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }
                
                if ((token = (TokenSettings) setManagement.getSetting(sessionid, channels.getChannelid(), 7, 1)) == null) {
                    aStatus.error = "OTP Token Settings is not configured!!!";
                    aStatus.errorcode = -17;
                    return aStatus;
                }
                 UserManagement uManagement = new UserManagement();
                AuthUser aUser = uManagement.getUser(sessionid, channels.getChannelid(), userid);
                if(aUser==null)
                {   aStatus.error = "User not found!!!";
                    aStatus.errorcode = -66;
                    return aStatus;
                
                
                }
                retValue = oManagement.AssignToken(sessionid, session.getChannelid(), userid, type, subtype, serialNo);
                try {
                    if (retValue == 0 && type == 1 && subtype == 2) {
                        String regCode = oManagement.generateRegistrationCode(sessionid, session.getChannelid(), userid, this.OTP_TOKEN_SOFTWARE);
                        SimpleDateFormat sdfExpiry = new SimpleDateFormat("hh:mm");
                        Calendar cal = Calendar.getInstance();
                        cal.add(12, token.getRegistrationValidity());
                        Date expiry = cal.getTime();
                        if (regCode != null) {
                            UserManagement userObj = new UserManagement();
                            AuthUser user = null;
                            user = userObj.getUser(sessionid, channels.getChannelid(), userid);
                            Templates temp = null;
                            TemplateManagement tObj = new TemplateManagement();
                            temp = tObj.LoadbyName(sessionid, channels.getChannelid(), "mobile.software.token.register");
                            ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            Date d = new Date();
                            String tmessage = (String) UtilityFunctions.deserializeFromObject((ByteArrayInputStream) bais);
                            tmessage = tmessage.replaceAll("#name#", user.getUserName());
                            tmessage = tmessage.replaceAll("#channel#", channels.getName());
                            tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                            if (subtype == 2) {
                                tmessage = tmessage.replaceAll("#tokentype#", "MOBILE");
                                String strSWOTPType = LoadSettings.g_sSettings.getProperty("sw.otp.type");
                                if (strSWOTPType != null && strSWOTPType.compareToIgnoreCase("simple") == 0) {
                                    regCode = "1" + regCode;
                                    log.debug((Object) ("user name = " + user.getUserName() + " with reg code as " + regCode));
                                }
                            }
                            tmessage = tmessage.replaceAll("#regcode#", regCode);
                            tmessage = tmessage.replaceAll("#expiry#", sdfExpiry.format(expiry));
                            if (subtype == 1) {
                                tmessage = tmessage.replaceAll("#tokentype#", "WEB");
                            }
                            if (subtype == 2) {
                                tmessage = tmessage.replaceAll("#tokentype#", "MOBILE");
                            }
                            if (subtype == 3) {
                                tmessage = tmessage.replaceAll("#tokentype#", "PC");
                            }
                            SendNotification send = new SendNotification();
                            AXIOMStatus status = null;
                            tObj.getClass();
                            if (temp.getStatus() == 1) {
                                status = send.SendOnMobileNoWaiting(channels.getChannelid(), user.getPhoneNo(), tmessage, 1, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }
                            if (status != null && status.iStatus == this.SEND_MESSAGE_PENDING_STATE) {
                                RegistrationCodeTrailManagement rManagement = new RegistrationCodeTrailManagement();
                                String registrationcode = UtilityFunctions.Bas64SHA1((String) regCode);
                                rManagement.addRegCodeTrail(channelid, userid, registrationcode, 0, 1);
                            }
                            retValue = 0;
                            aStatus.error = "SUCCESS";
                            aStatus.errorcode = 0;
                            audit.AddAuditTrail(sessionid, channels.getChannelid(), session.getChannelid(), req.getRemoteAddr(), channels.getName(), session.getChannelid(), session.getChannelid(), new Date(), "Generate & Send Registration Code", "success", 0, "Token Management", "", "Registration Code = ******", "OTPTOKENS", userid);
                        } else if (regCode == null) {
                            aStatus.error = "Failed To generate Registration Code";
                            aStatus.errorcode = -120;
                            audit.AddAuditTrail(sessionid, channels.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channels.getName(), session.getChannelid(), session.getChannelid(), new Date(), "Generate & Send Registration Code", "failed", -1, "Token Management", "", "Failed To generate Registration Code", "OTPTOKENS", userid);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (retValue == 0 && (retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), userid, 1, type, subtype)) != 0) {
                    retValue = -15;
                    aStatus.error = "Failed in Activation!!!";
                    aStatus.errorcode = retValue;
                }
                if (retValue == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                } else if (retValue == -4) {
                    aStatus.error = "Token already assigned!!!";
                    aStatus.errorcode = -4;
                } else if (retValue == -3) {
                    aStatus.error = "Empty Serial Number!!!";
                    aStatus.errorcode = -3;
                } else if (retValue == -2) {
                    aStatus.error = "Invalid Serial Number!!!";
                    aStatus.errorcode = -2;
                } else if (retValue == -6) {
                    aStatus.error = "Token could not be assigned!!!";
                    aStatus.errorcode = -6;
                } else if (retValue == -20) {
                    aStatus.error = "Failed to issue registration code!!!";
                    aStatus.errorcode = -7;
                }
            }
            audit.AddAuditTrail(sessionid, channels.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channels.getName(), session.getLoginid(), session.getLoginid(), new Date(), "Assign OTP Token", aStatus.error, aStatus.errorcode, "Token Management", "", "Category=" + strCategory + ",Subcategory=" + strSubCategory, "OTPTOKENS", userid);
            return aStatus;
        } catch (Exception ex) {
            ex.printStackTrace();
            strException = ex.getMessage();
            AxiomStatus aStatus1 = new AxiomStatus();
            aStatus1.error = strException;
            aStatus1.errorcode = -99;
            return aStatus1;
        }
    }

    public AxiomStatus SendRegistrationCode(String sessionid, String userid, int type) {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug((Object) ("SendRegistrationCode::sessionId::" + sessionid));
                log.debug((Object) ("SendRegistrationCode::userid::" + userid));
                log.debug((Object) ("SendRegistrationCode::type::" + type));
            }
        } catch (Exception var5_5) {
            // empty catch block
        }
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = this.wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
        Sessions session = sManagement.getSessionById(sessionid);
        AxiomStatus aStatus = new AxiomStatus();
        int retValue = -1;
        aStatus.error = "ERROR";
        aStatus.errorcode = retValue;
        if (session == null) {
            aStatus.error = "Session Invalid!!!";
            aStatus.errorcode = -14;
            return aStatus;
        }
        String strCategory = "SOFTWARE_TOKEN";
        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(session.getChannelid());
        if (channel == null) {
            aStatus.error = "Invalid Channel";
            aStatus.errorcode = -3;
            return aStatus;
        }
        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
        if (session != null) {
            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                        OperatorsManagement oManagementObj = new OperatorsManagement();
                        Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; ++i) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == 1) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }
                                SendNotification send = new SendNotification();
                                AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        suTemplate.close();
                        sTemplate.close();
                        aStatus.error = "INVALID IP";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                    aStatus.error = "INVALID IP";
                    aStatus.errorcode = -8;
                    return aStatus;
                }
            }
            String regCode = null;
            Otptokens oToken = null;
            Pkitokens pToken = null;
            int subtype = 0;
            PKITokenManagement pManagement = new PKITokenManagement();
            TokenSettings token = (TokenSettings) setManagement.getSetting(sessionid, channelid, 7, 1);
            if (token == null) {
                aStatus.error = "OTP Token Settings is not configured!!!";
                aStatus.errorcode = -17;
                return aStatus;
            }
            SimpleDateFormat sdfExpiry = new SimpleDateFormat("hh:mm");
            Calendar cal = Calendar.getInstance();
            cal.add(12, token.getRegistrationValidity());
            Date expiry = cal.getTime();
            if (type == 1) {
                oToken = oManagement.getOtpObjByUserId(sessionid, channelid, userid, 1);
                if (oToken == null) {
                    aStatus.error = "Desired Software Token is not assigned!!!";
                    aStatus.errorcode = -22;
                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "Send Registration Code ", aStatus.error, aStatus.errorcode, "Token Management", "", "Category = " + strCategory + "regcode =******", "MOBILETRUST", userid);
                    return aStatus;
                }
                subtype = oToken.getSubcategory();
                regCode = oManagement.generateRegistrationCode(sessionid, session.getChannelid(), userid, this.OTP_TOKEN_SOFTWARE);
                String strSWOTPType = LoadSettings.g_sSettings.getProperty("sw.otp.type");
                if (strSWOTPType != null && strSWOTPType.compareToIgnoreCase("simple") == 0) {
                    regCode = "1" + regCode;
                }
            } else if (type == 2) {
                pToken = pManagement.getPkiObjByUserid(channelid, userid, 1);
                if (pToken == null) {
                    aStatus.error = "Desired Software Token is not assigned!!!";
                    aStatus.errorcode = -22;
                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "Send Registration Code ", aStatus.error, aStatus.errorcode, "PKI Token Management", "", "Category = " + strCategory + "regcode =******", "MOBILETRUST", userid);
                    return aStatus;
                }
                subtype = pToken.getCategory();
                regCode = pManagement.generateRegistrationCode(sessionid, session.getChannelid(), userid, 1);
            }
            if (regCode != null) {
                System.out.println("Registration Code" + regCode);
                UserManagement userObj = new UserManagement();
                AuthUser user = null;
                user = userObj.getUser(sessionid, channel.getChannelid(), userid);
                Templates temp = null;
                TemplateManagement tObj = new TemplateManagement();
                temp = tObj.LoadbyName(sessionid, channel.getChannelid(), "mobile.software.token.register");
                ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                Date d = new Date();
                String tmessage = (String) UtilityFunctions.deserializeFromObject((ByteArrayInputStream) bais);
                tmessage = tmessage.replaceAll("#name#", user.getUserName());
                tmessage = tmessage.replaceAll("#channel#", channel.getName());
                tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                tmessage = tmessage.replaceAll("#regcode#", regCode);
                tmessage = tmessage.replaceAll("#expiry#", sdfExpiry.format(expiry));
                System.out.println("Registation code Message" + tmessage);
                if (subtype == 1) {
                    tmessage = tmessage.replaceAll("#tokentype#", "WEB");
                }
                if (type == 1) {
                    if (subtype == 2) {
                        tmessage = tmessage.replaceAll("#tokentype#", "MOBILE");
                    }
                    if (subtype == 3) {
                        tmessage = tmessage.replaceAll("#tokentype#", "PC");
                    }
                } else {
                    tmessage = tmessage.replaceAll("#tokentype#", "");
                }
                SendNotification send = new SendNotification();
                AXIOMStatus status = null;
                tObj.getClass();
                if (temp.getStatus() == 1) {
                    status = send.SendOnMobileNoWaiting(channel.getChannelid(), user.getPhoneNo(), tmessage, 1, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                }
                if (status != null && status.iStatus == this.SEND_MESSAGE_PENDING_STATE) {
                    RegistrationCodeTrailManagement rManagement = new RegistrationCodeTrailManagement();
                    String registrationcode = UtilityFunctions.Bas64SHA1((String) regCode);
                    rManagement.addRegCodeTrail(channelid, userid, registrationcode, 0, 1);
                }
                retValue = 0;
                aStatus.error = "SUCCESS";
                aStatus.errorcode = 0;
                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getChannelid(), req.getRemoteAddr(), channel.getName(), session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code", "success", 0, "Token Management", "", "Registration Code = ******", "MOBILETRUST", userid);
            } else if (regCode == null) {
                aStatus.error = "Failed To generate registration code";
                aStatus.errorcode = -24;
                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code", "failed", -1, "Token Management", "", "Failed To generate Registration Code", "MOBILETRUST", userid);
            }
        } else {
            aStatus.error = "SUCCESS";
            aStatus.errorcode = 0;
        }
        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "Send Registration Code", aStatus.error, aStatus.errorcode, "Token Management", "", "Category = " + strCategory + "regcode =******", "MOBILETRUST", userid);
        return aStatus;
    }

    public AxiomData VerifyOTPAndSignTransaction(String sessionid, String userid, String otp, RemoteSigningInfo rsInfo, boolean bNotifyUser) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public AxiomData ValidateUserAnswersAndSignTransaction(String sessionid, String userid, AxiomChallengeResponse QAndA, RemoteSigningInfo rsInfo, boolean bNotifyUser) throws AxiomException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public AxiomStatus AddOperators(String operatorid, String channelid, String name, String phone, String emailid, String passsword, int roleid, int status, int currentAttempts) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public AxiomMessage GetArchivedMessage(String sessionid, int messageId) throws AxiomException {
        AxiomMessage amessage = new AxiomMessage();
        try {
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionid);
            if (session == null) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                throw new AxiomException("Invalid Session");
            }
            BulkMSGManagement bMSGManagement = new BulkMSGManagement();
            boolean check = false;
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                throw new AxiomException("Invalid Channel");
            }
            if (session != null) {
                int f;
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                suTemplate.close();
                                sTemplate.close();
                                throw new AxiomException("Invalid IP");
                            }
                            suTemplate.close();
                            sTemplate.close();
                            throw new AxiomException("Invalid IP");
                        }
                        throw new AxiomException("Invalid IP");
                    }
                }
                int result = -1;
                BulkMSGManagement bmsg = new BulkMSGManagement();
                Bulkmsg msgs = bmsg.getBulkMessage(messageId);
                if (msgs == null) {
                    throw new AxiomException("Message id is not present (" + messageId + ")");
                }
                amessage.emailid = msgs.getEmailid();
                amessage.message = msgs.getMsgbody();
                amessage.subject = msgs.getEmailsubject();
                if (msgs.getBcc() != null && !msgs.getBcc().isEmpty()) {
                    if (!msgs.getBcc().contains(",")) {
                        amessage.bcc = new String[1];
                        amessage.bcc[0] = msgs.getBcc();
                    } else {
                        String[] bccs = msgs.getBcc().split(",");
                        amessage.bcc = new String[bccs.length];
                        for (f = 0; f < bccs.length; ++f) {
                            amessage.bcc[f] = bccs[f];
                        }
                    }
                }
                if (msgs.getCc() != null && !msgs.getCc().isEmpty()) {
                    if (!msgs.getCc().contains(",")) {
                        amessage.cc = new String[1];
                        amessage.cc[0] = msgs.getCc();
                    } else {
                        String[] ccs = msgs.getCc().split(",");
                        amessage.cc = new String[ccs.length];
                        for (f = 0; f < ccs.length; ++f) {
                            amessage.cc[f] = ccs[f];
                        }
                    }
                }
                if (msgs.getFilename() != null && !msgs.getFilename().isEmpty()) {
                    if (msgs.getFilename().contains(",")) {
                        amessage.filenames = new String[1];
                        amessage.filenames[0] = msgs.getFilename();
                    } else {
                        String[] files = msgs.getFilename().split(",");
                        amessage.filenames = new String[files.length];
                        for (f = 0; f < files.length; ++f) {
                            amessage.filenames[f] = files[f];
                        }
                    }
                }
                amessage.mimetypes = new String[3];
                amessage.mimetypes[0] = "html/text";
                amessage.mimetypes[1] = "application/pdf";
                amessage.mimetypes[2] = "application/zip";
                amessage.status = new AxiomStatus();
                amessage.status.error = this.SEND_MESSAGE_PENDING_STATE_STRING;
                String resultstr = "Failure";
                if (amessage != null) {
                    resultstr = "SUCCESS";
                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "BULK SEND", resultstr, result, "Messages", "Total Count=", "", "Email", channel.getChannelid());
                } else if (result != 0) {
                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "BULK SEND", resultstr, result, "Messages", "Total Count=", "", "Email", channel.getChannelid());
                    throw new AxiomException("Message id is not present (" + messageId + ")");
                }
            }
            return amessage;
        } catch (Exception e) {
            e.printStackTrace();
            return amessage;
        }
    }

    public AxiomMessage[] SendArchivedMessage(String sessionid, AxiomMessage[] msgs, boolean bCheckContent, int speedType, int type) throws AxiomException {
        int SMS = 1;
        int USSD = 2;
        int VOICE = 3;
        int EMAIL = 4;
        try {
            if (msgs == null || msgs.length == 0) {
                throw new AxiomException("Invalid Messages!!!");
            }
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            Sessions session = sManagement.getSessionById(sessionid);
            if (session == null) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                throw new AxiomException("Invalid Session");
            }
            BulkMSGManagement bMSGManagement = new BulkMSGManagement();
            int check = 0;
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                throw new AxiomException("Invalid Channel");
            }
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus aXIOMStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                throw new AxiomException("Invalid IP");
                            }
                            suTemplate.close();
                            sTemplate.close();
                            throw new AxiomException("Invalid IP");
                        }
                        throw new AxiomException("Invalid IP");
                    }
                }
                int result = -1;
                int speed = 0;
                Object settingsObj = setManagement.getSetting(sessionid, session.getChannelid(), 10, 1);
                if (settingsObj != null) {
                    GlobalChannelSettings globalObj = (GlobalChannelSettings) settingsObj;
                    if (speedType == 1) {
                        if (type == EMAIL) {
                            speed = globalObj.emailsettingobj._slowPaceEMAIL;
                        } else if (type == SMS) {
                            speed = globalObj.smssettingobj._slowPaceSMS;
                        } else if (type == VOICE) {
                            speed = globalObj.voicesettingobj._slowPaceVOICE;
                        } else if (type == USSD) {
                            speed = globalObj.ussdsettingobj._slowPaceUSSD;
                        }
                    } else if (speedType == 2) {
                        if (type == EMAIL) {
                            speed = globalObj.emailsettingobj._normalPaceEMAIL;
                        } else if (type == SMS) {
                            speed = globalObj.smssettingobj._normalPaceSMS;
                        } else if (type == VOICE) {
                            speed = globalObj.voicesettingobj._normalPaceVOICE;
                        } else if (type == USSD) {
                            speed = globalObj.ussdsettingobj._normalPaceUSSD;
                        }
                    } else if (speedType == 3) {
                        if (type == EMAIL) {
                            speed = globalObj.emailsettingobj._fastPaceEMAIL;
                        } else if (type == SMS) {
                            speed = globalObj.smssettingobj._fastPaceSMS;
                        } else if (type == VOICE) {
                            speed = globalObj.voicesettingobj._fastPaceVOICE;
                        } else if (type == USSD) {
                            speed = globalObj.ussdsettingobj._fastPaceUSSD;
                        }
                    } else if (speedType == 4) {
                        if (type == EMAIL) {
                            speed = globalObj.emailsettingobj._hyperPaceEMAIL;
                        } else if (type == SMS) {
                            speed = globalObj.smssettingobj._hyperPaceSMS;
                        } else if (type == VOICE) {
                            speed = globalObj.voicesettingobj._hyperPaceVOICE;
                        } else if (type == USSD) {
                            speed = globalObj.ussdsettingobj._hyperPaceUSSD;
                        }
                    }
                } else {
                    throw new AxiomException("Configuraiton Missing!!!");
                }
                com.mollatech.axiom.nucleus.db.operation.AxiomMessage[] aMsg = new com.mollatech.axiom.nucleus.db.operation.AxiomMessage[msgs.length];
                String res = null;
                String uniqueid = req.getSession().getId() + channel.getChannelid() + new Date().getTime() + sessionid;
                String uniqueidforbulk = new String(Base64.encode((byte[]) this.SHA1(uniqueid)));
                Object files = null;
                String filepath = null;
                for (int i = 0; i < msgs.length; ++i) {
                    int retValue;
                    byte[] zipa;
                    int k;
                    if ((msgs[i] == null || msgs[i].message == null || msgs[i].number == null || msgs[i].message.isEmpty() || msgs[i].number.isEmpty()) && (msgs[i] == null || msgs[i].message == null || msgs[i].emailid == null || msgs[i].subject == null || msgs[i].message.isEmpty() || msgs[i].emailid.isEmpty() || msgs[i].subject.isEmpty())) {
                        msgs[i].status = new AxiomStatus();
                        msgs[i].status.error = this.SEND_MESSAGE_INVALID_DATA_STRING;
                        msgs[i].status.errorcode = this.SEND_MESSAGE_INVALID_DATA;
                        check = -1;
                        continue;
                    }
                    if (bCheckContent) {
                        File file;
                        FileOutputStream fout;
                        if (settingsObj != null) {
                            GlobalChannelSettings globalObj = (GlobalChannelSettings) settingsObj;
                            res = setManagement.checkcontent(session.getChannelid(), msgs[i].message, globalObj);
                        } else {
                            res = null;
                        }
                        if (res == null) {
                            try {
                                aMsg[i] = new com.mollatech.axiom.nucleus.db.operation.AxiomMessage();
                                aMsg[i].phone = msgs[i].number;
                                aMsg[i].message = msgs[i].message;
                                aMsg[i].emailid = msgs[i].emailid;
                                if (msgs[i].mimetypes != null || msgs[i].mimetypes.length != 0) {
                                    aMsg[i].mimetypes = new String[msgs[i].mimetypes.length];
                                    for (int m = 0; m < msgs[i].mimetypes.length; ++m) {
                                        aMsg[i].mimetypes[m] = msgs[i].mimetypes[m];
                                    }
                                }
                                if (msgs[i].filenames != null || msgs[i].filenames.length != 0) {
                                    aMsg[i].filenames = new String[msgs[i].filenames.length];
                                    for (k = 0; k < msgs[i].filenames.length; ++k) {
                                        zipa = Base64.decode((byte[]) msgs[i].filenames[k].getBytes());
                                        file = this.generateTempFile(".zip");
                                        filepath = this.generateTempFile(".zip").getAbsolutePath();
                                        System.err.println(" zip filepath name is  :" + filepath);
                                        fout = new FileOutputStream(filepath);
                                        fout.write(zipa);
                                        fout.close();
                                        aMsg[i].filenames[k] = filepath;
                                    }
                                }
                                aMsg[i].subject = msgs[i].subject;
                                msgs[i].status = new AxiomStatus();
                                msgs[i].status.error = this.SEND_MESSAGE_PENDING_STATE_STRING;
                                msgs[i].status.errorcode = 0;
                                retValue = bMSGManagement.ADDAxiomMessage(sessionid, session.getChannelid(), null, aMsg[i], this.SEND_MESSAGE_PENDING_STATE, new Date(), new Date(), type, speed, uniqueidforbulk);
                                msgs[i].msgid = "" + retValue;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            continue;
                        }
                        try {
                            aMsg[i] = new com.mollatech.axiom.nucleus.db.operation.AxiomMessage();
                            aMsg[i].phone = msgs[i].number;
                            aMsg[i].message = msgs[i].message;
                            aMsg[i].emailid = msgs[i].emailid;
                            aMsg[i].subject = msgs[i].subject;
                            if (msgs[i].mimetypes != null || msgs[i].mimetypes.length != 0) {
                                aMsg[i].mimetypes = new String[msgs[i].mimetypes.length];
                                for (int m = 0; m < msgs[i].mimetypes.length; ++m) {
                                    aMsg[i].mimetypes[m] = msgs[i].mimetypes[m];
                                }
                            }
                            if (msgs[i].filenames != null || msgs[i].filenames.length != 0) {
                                aMsg[i].filenames = new String[msgs[i].filenames.length];
                                for (k = 0; k < msgs[i].filenames.length; ++k) {
                                    zipa = Base64.decode((byte[]) msgs[i].filenames[k].getBytes());
                                    file = this.generateTempFile(".zip");
                                    filepath = this.generateTempFile(".zip").getAbsolutePath();
                                    System.err.println(" zip filepath name is  :" + filepath);
                                    fout = new FileOutputStream(filepath);
                                    fout.write(zipa);
                                    fout.close();
                                    aMsg[i].filenames[k] = filepath;
                                }
                            }
                            msgs[i].status = new AxiomStatus();
                            msgs[i].status.error = this.SEND_MESSAGE_BLOCKED_STATE_STRING;
                            msgs[i].status.errorcode = this.SEND_MESSAGE_BLOCKED_STATE;
                            retValue = bMSGManagement.ADDAxiomMessage(sessionid, session.getChannelid(), null, aMsg[i], this.SEND_MESSAGE_BLOCKED_STATE, new Date(), new Date(), type, speed, uniqueidforbulk);
                            msgs[i].msgid = "" + retValue;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        continue;
                    }
                    try {
                        aMsg[i] = new com.mollatech.axiom.nucleus.db.operation.AxiomMessage();
                        aMsg[i].phone = msgs[i].number;
                        aMsg[i].message = msgs[i].message;
                        aMsg[i].emailid = msgs[i].emailid;
                        aMsg[i].subject = msgs[i].subject;
                        if (msgs[i].mimetypes != null || msgs[i].mimetypes.length != 0) {
                            aMsg[i].mimetypes = new String[msgs[i].mimetypes.length];
                            for (int m = 0; m < msgs[i].mimetypes.length; ++m) {
                                aMsg[i].mimetypes[m] = msgs[i].mimetypes[m];
                            }
                        }
                        if (msgs[i].filenames != null || msgs[i].filenames.length != 0) {
                            aMsg[i].filenames = new String[msgs[i].filenames.length];
                            for (k = 0; k < msgs[i].filenames.length; ++k) {
                                zipa = Base64.decode((byte[]) msgs[i].filenames[k].getBytes());
                                filepath = this.generateTempFile(".zip").getAbsolutePath();
                                System.err.println(" zip filepath name is  :" + filepath);
                                FileOutputStream fout = new FileOutputStream(filepath);
                                fout.write(zipa);
                                fout.close();
                                aMsg[i].filenames[k] = filepath;
                            }
                        }
                        msgs[i].status = new AxiomStatus();
                        msgs[i].status.error = this.SEND_MESSAGE_PENDING_STATE_STRING;
                        msgs[i].status.errorcode = 0;
                        retValue = bMSGManagement.ADDAxiomMessage(sessionid, session.getChannelid(), null, aMsg[i], this.SEND_MESSAGE_PENDING_STATE, new Date(), new Date(), type, speed, uniqueidforbulk);
                        msgs[i].msgid = "" + retValue;
                        continue;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (check == 0) {
                    result = bMSGManagement.SendMSG(sessionid, session.getChannelid(), type, speed, uniqueidforbulk);
                }
                String strTYPE = null;
                if (type == 1) {
                    strTYPE = "SMS";
                } else if (type == 2) {
                    strTYPE = "USSD";
                } else if (type == 3) {
                    strTYPE = "VOICE";
                } else if (type == 4) {
                    strTYPE = "EMAIL";
                }
                String resultstr = "Failure";
                if (result == 0) {
                    resultstr = "Success";
                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "BULK SEND", resultstr, result, "Messages", "Total Count=" + msgs.length, "", strTYPE, channel.getChannelid());
                } else if (result != 0) {
                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "BULK SEND", resultstr, result, "Messages", "Total Count=" + msgs.length, "", strTYPE, channel.getChannelid());
                }
            }
            return msgs;
        } catch (Exception e) {
            throw new AxiomException("System Internal Exception::" + e.getMessage());
        }
    }

    private File generateTempFile(String strExtension) throws IOException {
        return File.createTempFile("axiom", strExtension);
    }

}
