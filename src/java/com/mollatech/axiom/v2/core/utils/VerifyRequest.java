/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.core.utils;

import com.mollatech.axiom.connector.user.AxiomChallengeResponse;

/**
 *
 * @author Manoj Sherkhane
 */
public class VerifyRequest {

    public static int Password = 1;
    public static int OTP = 2;
    public static int SignatureOTP = 3;
    public static int ChallengeAndResponse = 4;    
    public static int verifyData = 6;
    public static int BioMetrix = 5;
    

    public int category;
    public int subcategory;
    public String credential;
    public String[] signingData;
    public AxiomChallengeResponse qas;

    public static int GEOFENCING = 7;
    public GeoData geoData;
    
     public String pkisignData;
     public String pkiplainData;
    
    

}
