package com.mollatech.axiom.v2.core.utils;

public class AxiomMessage {
    public String number;
    public String message;         
    public AxiomStatus status;
    public String msgid;    
    public String  emailid;
    public String  subject;
    
    public String name;
    public String[] cc;
    public String[] bcc;
    public String[] filenames;
    public String[] mimetypes;
    
}
