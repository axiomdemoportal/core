/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.core.utils;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Sessions;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.CRC32;
import java.util.zip.Checksum;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import org.hibernate.Session;

/**
 *
 * @author pramod
 */
public class RSSUtils {

    public AxiomStatus checkRiskContraint(String sessionid, WebServiceContext wsContext) {
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Licence is invalid";
            aStatus.errorcode = -100;
            return aStatus;
        }

        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);
        AxiomStatus aStatus = new AxiomStatus();
        if (session == null) {
            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
            return aStatus;
        }

        if (sessionid == null || sessionid.isEmpty() == true) {
            //AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Invalid Data";
            aStatus.errorcode = -11;
            return aStatus;
        }

        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(session.getChannelid());
        if (channel == null) {
            aStatus.error = "Invalid Channel";
            aStatus.errorcode = -15;
            return aStatus;
        }

        if (session != null) {

            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();

            if (channel == null) {
                return null;
            }
            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        OperatorsManagement oManagementObj = new OperatorsManagement();
                        Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }

                        suTemplate.close();
                        sTemplate.close();
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                    aStatus.error = "INVALID IP REQUEST";
                    aStatus.errorcode = -8;
                    return aStatus;
                }
            }
        }
        return null;
    }

    public long CRC32CheckSum(byte[] data) {
        Checksum checksum = new CRC32();
        checksum.update(data, 0, data.length);
        long lngChecksum = checksum.getValue();
        
        return lngChecksum;
    }
    
    public String MaskingString(String strToMask, int CountToHide) {
        if (strToMask != null) {
            char[] mask = new char[strToMask.length() - 4];
            for (int i = 0; i < mask.length; i++) {
                mask[i] = 'X';
            }
            String maskString = new String(mask);
            return strToMask.replace(strToMask.substring(2, strToMask.length() - 2), maskString);
        }
        return "Not-Available";
    }

}
