/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.core.utils;

import com.mollatech.axiom.connector.user.AxiomChallengeResponse;
import java.util.Date;

/**
 *
 * @author Manoj Sherkhane
 */
public class AxiomCredentialDetails {

    public static final int PASSWORD = 1;
    public static final int SOFTWARE_TOKEN = 2;
    public static final int HARDWARE_TOKEN = 3;
    public static final int OUTOFBOUND_TOKEN = 4;
    public static final int CERTIFICATE = 5;
    public static final int CHALLAEGE_RESPONSE = 6;
    public static final int BIOMETRIX = 7;
     public static final int PKI_SOFTWARE_TOKEN = 8;
    public static final int PKI_HARDWARE_TOKEN = 9;
    public static final int SECURE_PHRASE = 10;
    public static final int SECURE_PHRASE_VALIDIMAGE = 1;
    public static final int SECURE_PHRASE_INVALIDIMAGE = 2;
    public static final int SECURE_PHRASE_OLDIMAGE = 0;
    public static final int GEOFENCING = 11;
    
    public static final int OTP_TOKEN_OUTOFBAND_SMS = 1;
    public static final int OTP_TOKEN_OUTOFBAND_VOICE = 3;
    public static final int OTP_TOKEN_OUTOFBAND_USSD = 2;
    public static final int OTP_TOKEN_OUTOFBAND_EMAIL = 4;
    public static final int OTP_TOKEN_SOFTWARE_MOBILE = 2;
    public static final int OTP_TOKEN_SOFTWARE_PC = 3;
    public static final int OTP_TOKEN_SOFTWARE_WEB = 1;
    public static final int OTP_TOKEN_HARDWARE_MINI = 1;
    public static final int OTP_TOKEN_HARDWARE_CR = 2;
    public int category;
    public int subcategory;
    public int attempts;
    public int status;
    public long createOn;
    public long lastaccessOn;
    public String serialnumber;
    public String base64Cert;
    public String Password;
    public String regcodeMessage;
    public AxiomChallengeResponse qas;
    public byte[] securePhraseImage;
    public int xCoordinate;
    public int yCoordinate;
    public String securePhraseID;
    public String securePhraseColor;
    public String securePhrase;
     public long expiryDate;
    public long certificateExpiryDate; 
}
