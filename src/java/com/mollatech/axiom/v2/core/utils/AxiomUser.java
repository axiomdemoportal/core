/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.core.utils;

/**
 *
 * @author vikramsareen
 */
public class AxiomUser {
    public String organisation;
    public String userIdentity;
    public String organisationUnit;
    public String country;
    public String location;
//    public String passport;
//    public String identitytype;
    public String idType;
    public String street;
    public String userName;
    public String surName;
    public String userId;
    public String email;
    public String phoneNo;
    public int statePassword;
    public int iAttempts;
    public long lCreatedOn;
    public long lLastAccessOn;
    public String designation;   
    public int status;
    public String expiryDateForcCertMigration;    
    
    public String toString() {
        return "AxiomUser{" + "userId=" + userId + 
                ", userName=" + userName + 
                ", email=" + email + 
                ", phoneNo=" + phoneNo + 
                ", statePassword=" + statePassword + 
                ", organisation=" + organisation+  
                ", userIdentity="+userIdentity+
                ", organisationUnit="+organisationUnit+
                ", country="+country+
                ", location="+location+
                ", idType="+idType+
                ", street="+street+
                ", surName="+surName+
                ", iAttempts="+iAttempts+
                ", lCreatedOn="+lCreatedOn+
                ", lLastAccessOn="+lLastAccessOn+
                ", designation="+designation+
                ", status="+status+
                ", status="+expiryDateForcCertMigration+                
                '}';
    }
}

