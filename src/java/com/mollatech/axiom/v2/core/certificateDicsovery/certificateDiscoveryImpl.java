/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.core.certificateDicsovery;

import certdiscovery.CertificateDiscovery;
import certdiscovery.CertificateManagerReports;
import certdiscovery.CertificatesReport;
import certdiscovery.DeleteCertDetailsByArchiveId;
import certdiscovery.GenerateAcno;
import certdiscovery.IPAddress;
import certdiscovery.NetUtils;
import certdiscovery.certDiscoverythread;
import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Sessions;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import com.mollatech.axiom.v2.core.utils.AxiomStatus;
import com.mollatech.axiom.v2.core.utils.CertificateManagerDelete;
import com.mollatech.axiom.v2.core.utils.CertificateManagerStatus;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import org.hibernate.Session;

/**
 *
 * @author bluebricks
 */
@WebService(endpointInterface = "com.mollatech.axiom.v2.core.certificateDicsovery.CertDiscovery")
public class certificateDiscoveryImpl implements CertDiscovery {

    final String itemtype = "CERTDISCOVERY";
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(certificateDiscoveryImpl.class.getName());
    @Resource
    WebServiceContext wsContext;

    @Override
    public String OpenSession(String channelid, String loginid, String loginpassword) {
        try {

            String strDebug = null;
            SettingsManagement setManagement = new SettingsManagement();
            try {

                ChannelProfile channelprofileObj = null;
                Object channelpobj = setManagement.getSettingInner(channelid, SettingsManagement.CHANNELPROFILE_SETTING, 1);

                if (channelpobj == null) {
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                } else {
                    channelprofileObj = (ChannelProfile) channelpobj;
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                }
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    log.debug("OpenSession::loginpassword::" + loginpassword);
                    log.debug("OpenSession::channelid::" + channelid);
                    log.debug("OpenSession::loginid::" + loginid);
                }
            } catch (Exception ex) {
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(channelid);
            if (channel == null) {
                return null;
            }

            MessageContext mc = wsContext.getMessageContext();

            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            //log.debug("Client IP = " + req.getRemoteAddr());
            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        SessionFactoryUtil suOperators = new SessionFactoryUtil(SessionFactoryUtil.operators);
                        OperatorsManagement oManagement = new OperatorsManagement();
                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            return null;
                        }

                        suTemplate.close();
                        sTemplate.close();
                        return null;
                    }
                    return null;
                }
            }

//            int result = setManagement.checkIP(channelid, req.getRemoteAddr());
//            if (checkIp != 1) {
//                return null;
//            }
            String resultStr = "Failure";
            int retValue = -1;
            SessionManagement sManagement = new SessionManagement();
            String sessionId = sManagement.OpenSessionForWS(channelid, loginid, loginpassword, req.getSession().getId());

            AuditManagement audit = new AuditManagement();
            if (sessionId != null) {
                retValue = 0;
                resultStr = "Success";
                audit.AddAuditTrail(sessionId, channelid, loginid,
                        req.getRemoteAddr(),
                        channel.getName(), loginid,
                        loginid, new Date(), "Open Session", resultStr, retValue,
                        "Login", "", "Open Session successfully with Session Id =" + sessionId, "SESSION",
                        loginid);

            } else if (sessionId == null) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), loginid, req.getRemoteAddr(), channel.getName(), loginid,
                        loginid, new Date(), "Open Session", resultStr, retValue,
                        "Login", "", "Failed To Open Session", "SESSION",
                        loginid);

            }

            return sessionId;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public AxiomStatus CloseSession(String sessionid) {
        try {

            String strDebug = null;
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    log.debug("CloseSession::sessionid::" + sessionid);
                }
            } catch (Exception ex) {
            }

//            int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                AxiomStatus aStatus = new AxiomStatus();
//                aStatus.error = "Licence is invalid";
//                aStatus.errorcode = -100;
//                return aStatus;
//            }
            SessionManagement sManagement = new SessionManagement();
            Sessions session = sManagement.getSessionById(sessionid);
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;

            if (session != null) {

                //log.debug("Client IP = " + req.getRemoteAddr());        
                SettingsManagement setManagement = new SettingsManagement();
//                int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
//                if (result != 1) {
//                    aStatus.error = "INVALID IP REQUEST";
//                    aStatus.errorcode = -8;
//                    //  return aStatus;
//                }
                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    return null;
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }
                int retValue = sManagement.CloseSession(sessionid);

                aStatus.error = "ERROR";
                aStatus.errorcode = retValue;
                if (retValue == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                }

            } else if (session == null) {
                return aStatus;
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                return aStatus;
            }
            AuditManagement audit = new AuditManagement();
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                    session.getLoginid(), new Date(), "Close Session", aStatus.error, aStatus.errorcode,
                    "Login", "", "Close Session successfully with Session Id =" + sessionid, "SESSION",
                    session.getLoginid());
            return aStatus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public CertificateManagerStatus ScanAllNodes(String sessionid, String ip, String[] port) {
        int result = 0;
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug("ScanAllNodes::sessionId::" + sessionid);
                log.debug("ScanAllNodes::ip::" + ip);
                log.debug("ScanAllNodes::port::" + port);
            }
        } catch (Exception ex) {
        }
        CertificateManagerStatus archiveId = new CertificateManagerStatus();
        
        if (ip.isEmpty() ) {
            archiveId.error = "Ip Address/host name can not be blank!!!";
//            archiveId.errorcode = -14;
            return archiveId;
        }else if (port == null || port.length==0 || port[0].equals("") ) {
            archiveId.error = "Port can not be blank!!!";
//            archiveId.errorcode = -14;
            return archiveId;
        }
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);
//        AxiomStatus aStatus = new AxiomStatus();
        int retValue = -1;
        archiveId.error = "ERROR";
        archiveId.errorcode = retValue;

        if (session == null) {
            archiveId.error = "Session Invalid!!!";
            archiveId.errorcode = -14;
            return archiveId;
        }

        String strCategory = "SOFTWARE_TOKEN";

        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(session.getChannelid());
        if (channel == null) {
            archiveId.error = "Invalid Channel";
            archiveId.errorcode = -3;
            return archiveId;
        }
        if (session != null) {
            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();

            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        OperatorsManagement oManagementObj = new OperatorsManagement();
                        Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            archiveId.error = "INVALID IP";
                            archiveId.errorcode = -8;
                            return archiveId;
                        }

                        suTemplate.close();
                        sTemplate.close();
                        archiveId.error = "INVALID IP";
                        archiveId.errorcode = -8;
                        return archiveId;
                    }
                    archiveId.error = "INVALID IP";
                    archiveId.errorcode = -8;
                    return archiveId;
                }
            }
            //added

            List<String> ips = new ArrayList();
            if (ip.contains("/")) {

                String[] str = ip.split("/");
                List<String> list = null;
                try {
                    list = IPAddress.getIPAddresses(str[0]);
                } catch (IOException ex) {
                    Logger.getLogger(certificateDiscoveryImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
                ips.addAll(list);
            } else {
                ips.add(ip);
            }

            //
            List<String> port1 = new ArrayList();
            for (int p = 0; p < port.length; p++) {
                port1.add(port[p]);

            }
            NetUtils n = new NetUtils();
            String regcode = null;
            for (String ip1 : ips) {
                for (String ports : port1) {
                    boolean isConnect = NetUtils.isConnectable(ip1, ports);
                    if (isConnect == true) {
                        regcode = GenerateAcno.generateAckno();

                    } else {
                        System.out.println("Unable To connect");
                        archiveId.error = "Unable To connect!!!";
//            archiveId.errorcode = -14;
                     return archiveId;
                        
                    }

                }
            }
            archiveId.archiveId = regcode;
            CertificateDiscovery certDis = new CertificateDiscovery();
            Runnable r = new certDiscoverythread(ip, port, regcode);
            new Thread(r).start();
//           certDiscoverythread certDetails=new certDiscoverythread(ip,port,regcode);
//           certDetails.run();

            archiveId.archiveId = regcode;

            archiveId.error = "SUCCESS";

            if (result == 0 || result == 3) {

                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getChannelid(),
                        req.getRemoteAddr(), channel.getName(),
                        session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code",
                        "success", 0, "Certificate Discovery", "", "Registration Code = ******" /*strRegCode*/, itemtype, sessionid);
            }
        } else {
            archiveId.error = "Failed To generate registration code";
            archiveId.errorcode = -24;
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                    req.getRemoteAddr(), channel.getName(),
                    session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code",
                    "failed", -1, "Certificate Discovery", "", "Failed To generate Registration Code", itemtype, sessionid);
        }

        archiveId.errorcode = result;
        return archiveId;
    }

    @Override
    public List<CertificatesReport> getReports(String sessionid, String archiveid, String reportType, String repoortCategory) {

        //added
        int result = -1;
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug("getReports::sessionId::" + sessionid);
                log.debug("getReports::archiveid::" + archiveid);

            }
        } catch (Exception ex) {
        }
        // CertificateManagerDelete CertStatus=new CertificateManagerDelete();
        CertificatesReport CertStatus = new CertificatesReport();
        List<CertificatesReport> list11 = new ArrayList<CertificatesReport>();
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);
//        AxiomStatus aStatus = new AxiomStatus();
        int retValue = -1;
        CertStatus.setErrormessage("ERROR");
//        CertStatus.errorcode = retValue;

        if (session == null) {
            CertStatus.setErrormessage("Session Invalid!!!");
            list11.add(CertStatus);
            return list11;
        }

        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(session.getChannelid());
        if (channel == null) {
            CertStatus.setErrormessage("Invalid Channel!!");
            list11.add(CertStatus);
            return list11;
//            CertStatus.error = "Invalid Channel";
//            CertStatus.errorcode = -3;
//            return CertStatus;
        }
        if (session != null) {
            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();

            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        OperatorsManagement oManagementObj = new OperatorsManagement();
                        Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();

                            CertStatus.setErrormessage("INVALID IP!!!");
                            list11.add(CertStatus);
                            return list11;
//                            CertStatus.error = "INVALID IP";
//                            CertStatus.errorcode = -8;
//                            return CertStatus;
                        }

                        suTemplate.close();
                        sTemplate.close();
                         CertStatus.setErrormessage("INVALID IP!!!");
                            list11.add(CertStatus);
                            return list11;
//                        CertStatus.error = "INVALID IP";
//                        CertStatus.errorcode = -8;
//                        return CertStatus;
                    }
                     CertStatus.setErrormessage("INVALID IP!!!");
                            list11.add(CertStatus);
                            return list11;
//                    CertStatus.error = "INVALID IP";
//                    CertStatus.errorcode = -8;
//                    return CertStatus;
                }
            }

            //end add
            CertificateManagerReports certReports = new CertificateManagerReports();
//        CertificatesReport[] CertificatesReport= null;
          //  List<CertificatesReport> list11 = null;
            try {
                list11 = certReports.getReports(archiveid, reportType, repoortCategory);

            } catch (Exception ex) {
                Logger.getLogger(certificateDiscoveryImpl.class.getName()).log(Level.SEVERE, null, ex);
            }

            //added
             CertStatus.setErrormessage("SUCCESS");
                            list11.add(CertStatus);
                            
//            CertStatus.errorcode = result;

//            CertStatus.error = "SUCCESS";

            if (result == 0 || result == 3) {

                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getChannelid(),
                        req.getRemoteAddr(), channel.getName(),
                        session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code",
                        "success", 0, "Certificate Discovery", "", "Registration Code = ******" /*strRegCode*/, itemtype, sessionid);
            }
        } else {
//            CertStatus.error = "Failed To generate registration code";
//            CertStatus.errorcode = -24;
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                    req.getRemoteAddr(), channel.getName(),
                    session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code",
                    "failed", -1, "Certificate Discovery", "", "Failed To generate Registration Code", itemtype, sessionid);
        }

//        CertStatus.errorcode = result;
        //end
        return list11;
    }

    @Override
    public CertificateManagerDelete deleteRecords(String sessionid, String archiveid) {

        int result = -1;
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.debug("deleteRecords::sessionId::" + sessionid);
                log.debug("deleteRecords::archiveid::" + archiveid);

            }
        } catch (Exception ex) {
        }
        CertificateManagerDelete CertStatus = new CertificateManagerDelete();
       if (archiveid == null) {
            CertStatus.error = "archive id can not be blank!!!";
//            CertStatus.errorcode = -14;
            return CertStatus;
        }
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);
//        AxiomStatus aStatus = new AxiomStatus();
        int retValue = -1;
        CertStatus.error = "ERROR";
        CertStatus.errorcode = retValue;

        if (session == null) {
            CertStatus.error = "Session Invalid!!!";
            CertStatus.errorcode = -14;
            return CertStatus;
        }

        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(session.getChannelid());
        if (channel == null) {
            CertStatus.error = "Invalid Channel";
            CertStatus.errorcode = -3;
            return CertStatus;
        }
        if (session != null) {
            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();

            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        OperatorsManagement oManagementObj = new OperatorsManagement();
                        Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            CertStatus.error = "INVALID IP";
                            CertStatus.errorcode = -8;
                            return CertStatus;
                        }

                        suTemplate.close();
                        sTemplate.close();
                        CertStatus.error = "INVALID IP";
                        CertStatus.errorcode = -8;
                        return CertStatus;
                    }
                    CertStatus.error = "INVALID IP";
                    CertStatus.errorcode = -8;
                    return CertStatus;
                }
            }

             DeleteCertDetailsByArchiveId d = new DeleteCertDetailsByArchiveId();
            try {
                result = d.DeleteCertDetailsByArchiveId(archiveid);
            } catch (Exception ex) {
                Logger.getLogger(certificateDiscoveryImpl.class.getName()).log(Level.SEVERE, null, ex);
            }

            CertStatus.errorcode = result;

            CertStatus.error = "SUCCESS";

            if (result == 0) {
                CertStatus.error = "Record Deleted Successfully!!";

                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getChannelid(),
                        req.getRemoteAddr(), channel.getName(),
                        session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code",
                        "success", 0, "Certificate Discovery", "", "Registration Code = ******" /*strRegCode*/, itemtype, sessionid);
            }else{
                CertStatus.error = "Archive Id is Not Present";
            }
        } else {
            CertStatus.error = "Archive Id is Not Present";
            CertStatus.errorcode = -24;
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                    req.getRemoteAddr(), channel.getName(),
                    session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code",
                    "failed", -1, "Certificate Discovery", "", "Failed To generate Registration Code", itemtype, sessionid);
        }

        CertStatus.errorcode = result;
        return CertStatus;

    }

}
