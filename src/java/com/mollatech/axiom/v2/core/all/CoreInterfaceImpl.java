/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.axiom.v2.core.all;

import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.communication.RemoteSigningInfo;
import com.mollatech.axiom.connector.epin.QuestionsAndAnswers;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.connector.user.TokenStatusDetails;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Sessions;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.RegistrationCodeTrailManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import com.mollatech.axiom.nucleus.settings.TokenSettings;
import com.mollatech.axiom.v2.core.utils.AxiomData;
import com.mollatech.axiom.v2.core.utils.AxiomException;
import com.mollatech.axiom.v2.core.utils.AxiomMessage;
import com.mollatech.axiom.v2.core.utils.AxiomStatus;
import com.mollatech.axiom.v2.core.utils.AxiomTokenDetails;
import com.mollatech.axiom.v2.core.utils.AxiomUser;
import com.mollatech.axiom.v2.core.utils.ConnectorStatus;
import com.mollatech.axiom.v2.core.utils.MessageReport;
import com.mollatech.axiom.v2.core.utils.MobileTrustStatus;
import com.mollatech.ftress.mapper.AuthenticationResponse;
import com.mollatech.ftress.mapper.Response;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import org.hibernate.Session;

/**
 *
 * @author Human
 */
@WebService(endpointInterface = "com.mollatech.axiom.v2.core.all.CoreInterface")
public class CoreInterfaceImpl implements CoreInterface {

    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CoreInterfaceImpl.class.getName());
    @Resource
    WebServiceContext wsContext;

    private static Sessions gs_session = null;
    private static Channels gs_channels = null;

    @Override
    public String OpenSessionAP(String channelid, String loginid, String loginpassword) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String OpenSessionMT(String channelid, String loginid, String loginpassword) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus CloseSessionAP(String sessionid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus CloseSessionMT(String sessionid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus CreateUserAP(String sessionid, String fullname, String phone, String email, String userid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus CreateUserMT(String sessionid, String fullname, String phone, String email, String userid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomUser GetUserByAP(String sessionid, int type, String searchFor) throws AxiomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomUser GetUserByMT(String sessionid, int type, String searchFor) throws AxiomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus ResetUserAP(String sessionid, String userid, int type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus ResetUserMT(String sessionid, String userid, int type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus AssignPasswordAP(String sessionid, String userid, String password, boolean bSentToUser, int subcategory) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus VerifyPasswordAP(String sessionid, String userid, String password) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus AssignTokenAP(String sessionid, String userid, int type, int subtype) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomTokenDetails[] GetUserTokensAP(String sessionid, String userid) throws AxiomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus SendOTPAP(String sessionid, String userid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus VerifyOTPAP(String sessionid, String userid, String otp) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus SendSignatureOTPAP(String sessionid, String userid, String[] data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus VerifySignatureOTPAP(String sessionid, String userid, String[] data, String otp) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomMessage[] SendMessagesAP(String sessionid, AxiomMessage[] msgs, boolean bCheckContent, int speed, int type) throws AxiomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus ChangeOTPTokenStatusAP(String sessionid, String userid, int category, int value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ConnectorStatus GetConnectorsStatusAP(String channelid, String loginid, String password) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus AssignTokenAndGenerateCertificateAP(String sessionid, String userid, int type, int subtype, String srno) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomData VerifyOTPAndSignTransactionAP(String sessionid, String userid, String otp, RemoteSigningInfo rsInfo, boolean bNotifyUser) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomData VerifySignatureAP(String sessionid, String userid, String dataToSign, String envelopedData, int docType) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus ValidateRegistrationAP(String sessionid, String userid, String challenge, String response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public MessageReport[] FetchStatusAP(String sessionid, String phonNo, String startDate, String endDate) throws AxiomException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus AssignMT(String sessionid, String userid, int subtype) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public MobileTrustStatus ActivateMT(String sessionid, String userid, String SwTokenData) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String GetTimeStampMT(String sessionid, String userid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus VerifyOTPPlusMT(String sessionid, String userid, int type, String otp, String otpPlus) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus VerifySignatureOTPPlusMT(String sessionid, String userid, int type, String[] data, String sotp, String sotpPlus) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus VerifyDigitalSignatureMT(String sessionid, String userid, String data, int type, String signature) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus VerifyDigitalSignaturePlusMT(String sessionid, String userid, String data, int type, String signature, String signaturePlus) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus ALertEventMT(String sessionid, String signData) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus initiateRequestEPIN(String sessionid, String userid, int inChannel) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void AddTrailRA(String sessionid, String userid, String source, int type, String[][] txDetails, String ip, String[][] requestheaders, String[][] extendsDetails) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int IsAdditionalAuthenticationNeededRA(String sessionid, String userid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public QuestionsAndAnswers GetQuestionsEPIN(String sessionid, String userid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus verifyAnswersAndSendEPIN(String sessionid, String userid, QuestionsAndAnswers QandA) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus DeleteUserAP(String sessionid, String userid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AxiomStatus ChangeUserDetailsAP(String sessionid, String userid, String fullname, String phone, String email) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String EnforceSecurityMT(String sessionid, String userid, String data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String ConsumeSecurityMT(String sessionid, String userid, String data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AuthenticationResponse verifyEBankingPassword(String systemSessionId, String userid, String password, int fds_options, String channel, String domain) {

        String strDebug = null;
        try {
            log.info("Entered verifyEBankingPassword()");
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                Date d = new Date();
                log.info(d + ">>" + "verifyEBankingPassword::systemSessionId::" + systemSessionId);
                log.info(d + ">>" + "verifyEBankingPassword::userid::" + userid);
                log.info(d + ">>" + "verifyEBankingPassword::fds_options::" + fds_options);
                log.info(d + ">>" + "verifyEBankingPassword::password::" + password);
                log.info(d + ">>" + "verifyEBankingPassword::channel::" + channel);
                log.info(d + ">>" + "verifyEBankingPassword::domain::" + domain);
            }
        } catch (Exception ex) {
        }

        String strException = "";

        try {

            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AuthenticationResponse aStatus = new AuthenticationResponse();
                log.debug("verifyEBankingPassword:: " + " Licence Not Valid");
                log.debug("" + -100);
                log.debug(systemSessionId);
                aStatus.errorMessage = "Licence Not Valid";
                aStatus.status = "" + -100;
                aStatus.session = systemSessionId;
                return aStatus;
            }

            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_PASSWORD) != 0) {
                //throw new AxiomException("Digital Certificate feature is not available in this license!!!");                    
                AuthenticationResponse aStatus = new AuthenticationResponse();
                log.debug("verifyEBankingPassword:: " + "Password feature is not available in this license!!!");
                aStatus.errorMessage = "Password feature is not available in this license!!!";
                aStatus.status = "" + -101;
                aStatus.session = systemSessionId;
                return aStatus;
            }

            UserManagement uManagement = new UserManagement();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(systemSessionId);
            AuthenticationResponse aStatus = new AuthenticationResponse();
            int retValue = -1;
            log.debug("verifyEBankingPassword::" + "ERROR");
            aStatus.errorMessage = "ERROR";
            aStatus.status = "" + retValue;
            aStatus.session = systemSessionId;

            if (session == null) {
                return aStatus;
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channels = cManagement.getChannelByID(session.getChannelid());
            if (channels == null) {
                return aStatus;
            }

            if (session != null) {

                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();

                //new addition
                ChannelProfile channelprofileObj = null;
                Object channelpobj = setManagement.getSettingInner(channelid,
                        SettingsManagement.CHANNELPROFILE_SETTING, 1);

                if (channelpobj == null) {
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                } else {
                    channelprofileObj = (ChannelProfile) channelpobj;
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                }
                //end of new addition

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channels.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channels.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channels.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                log.debug("verifyEBankingPassword::" + "INVALID IP REQUEST");
                                log.debug("verifyEBankingPassword::" + "" + -8);
                                log.debug("verifyEBankingPassword::" + systemSessionId);
                                aStatus.errorMessage = "INVALID IP REQUEST";
                                aStatus.status = "" + -8;
                                aStatus.session = systemSessionId;
                                return aStatus;

                            }

                            suTemplate.close();
                            sTemplate.close();
                            log.debug("verifyEBankingPassword::" + "INVALID IP REQUEST");
                            log.debug("verifyEBankingPassword::" + "" + -8);
                            log.debug("verifyEBankingPassword::" + systemSessionId);
                            aStatus.errorMessage = "INVALID IP REQUEST";
                            aStatus.status = "" + -8;
                            aStatus.session = systemSessionId;
                            return aStatus;
                        }
                        log.debug("verifyEBankingPassword::" + "INVALID IP REQUEST");
                        log.debug("verifyEBankingPassword::" + "" + -8);
                        log.debug("verifyEBankingPassword::" + systemSessionId);
                        aStatus.errorMessage = "INVALID IP REQUEST";
                        aStatus.status = "" + -8;
                        aStatus.session = systemSessionId;
                        return aStatus;
                    }
                }

                AuthUser aUser = uManagement.verifyPassword(systemSessionId, session.getChannelid(), userid, password);

                if (aUser != null) {
                    log.debug("verifyEBankingPassword::" + "SUCCESS");
                    log.debug("verifyEBankingPassword::" + "" + 0);
                    log.debug("verifyEBankingPassword::" + systemSessionId);
                    aStatus.errorMessage = "SUCCESS";
                    aStatus.status = "" + 0;
                    aStatus.session = systemSessionId;

                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                            channels.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(),
                            "Verify Password", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "User Management",
                            "",
                            "Verify Password Successful",
                            "USERPASSWORD", userid);
                } else {
                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                            channels.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(),
                            "Verify Password", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "User Management",
                            "",
                            "Failed To Validate Password",
                            "USERPASSWORD", userid);
                }

            }
            return aStatus;
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            strException = e.getMessage();
        }
        AuthenticationResponse aStatus1 = new AuthenticationResponse();
        int retValue = -99;
        log.debug("verifyEBankingPassword::" + strException);
        log.debug("verifyEBankingPassword::" + retValue);
        log.debug("verifyEBankingPassword::" + systemSessionId);
        aStatus1.errorMessage = strException;
        aStatus1.status = "" + retValue;
        aStatus1.session = systemSessionId;
        return aStatus1;
    }

    @Override
    public Response resetUser(String systemSessionId, String authenticationType, String userid, String channel, String domain) {

        String strDebug = null;
        try {
            log.info("Entered resetUser()");
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                Date d = new Date();
                log.info(d + ">>" + "resetUser::systemSessionId::" + systemSessionId);
                log.info(d + ">>" + "resetUser::userid::" + userid);
                log.info(d + ">>" + "resetUser::authenticationType::" + authenticationType);
                log.info(d + ">>" + "resetUser::channel::" + channel);
                log.info(d + ">>" + "resetUser::domain::" + domain);
            }
        } catch (Exception ex) {
        }

        String strException = "";

        try {

            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                Response aStatus = new Response();
                log.debug("resetUser" + "Licence Not Valid");
                log.debug("resetUser" + "" + -100);
                aStatus.errorMessage = "Licence Not Valid";
                aStatus.status = "" + -100;
                return aStatus;
            }

            SessionManagement sManagement = new SessionManagement();

            Sessions session = sManagement.getSessionById(systemSessionId);
            AuditManagement audit = new AuditManagement();
            Response aStatus = new Response();
            log.debug("resetUser" + "Error");
            log.debug("resetUser" + "" + -9);

            aStatus.errorMessage = "ERROR";
            aStatus.status = "" + -9;
            int type = RESET_USER_PASSWORD;
            if (authenticationType.equals("SOFTTOKEN") == true || authenticationType.equals("Soft Token") == true) {
                type = RESET_USER_TOKEN_SOFTWARE;
            } else if (authenticationType.equals("HARDTOKEN") == true || authenticationType.equals("Hard Token") == true) {
                type = RESET_USER_TOKEN_HARDWARE;
            } else if (authenticationType.equals("OOBTOKEN") == true || authenticationType.equals("OTP") == true || authenticationType.equals("One Time Password") == true) {
                type = RESET_USER_TOKEN_OOB;
            } else if (authenticationType.equals("Challenge Questions") == true) {
                log.debug("Challenge Questions is disabled!!!");
                log.debug("" + -5);
                aStatus.errorMessage = "Challenge Questions is disabled!!!";
                aStatus.status = "" + -5;
                return aStatus;
            } else {
                try {
                    type = Integer.parseInt(authenticationType);
                } catch (Exception e) {
                    log.debug("resetUser" + "Invalid Authentication Type!!!");
                    log.debug("resetUser" + "" + -6);
                    aStatus.errorMessage = "Invalid Authentication Type!!!";
                    aStatus.status = "" + -6;
                    return aStatus;
                }
            }
            int retValue = -1;
            String strCategory = "";
            if (type == RESET_USER_TOKEN_SOFTWARE) {
                strCategory = "SOFTWARE_TOKEN";
            } else if (type == RESET_USER_TOKEN_HARDWARE) {
                strCategory = "HARDWARE_TOKEN";
            } else if (type == RESET_USER_TOKEN_OOB) {
                strCategory = "OOB_TOKEN";
            }
            String strSubCategory = "";
            if (session == null) {
                return aStatus;
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channels = cManagement.getChannelByID(session.getChannelid());
//        if (channels == null) {
//            return aStatus;
//        }
            String check = LoadSettings.g_sSettings.getProperty("user.reset");

            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            //System.out.println("Client IP = " + req.getRemoteAddr());        
            SettingsManagement setManagement = new SettingsManagement();

            //new addition
            ChannelProfile channelprofileObj = null;
            Object channelpobj = setManagement.getSettingInner(session.getChannelid(),
                    SettingsManagement.CHANNELPROFILE_SETTING, 1);

            if (channelpobj == null) {
                LoadSettings.LoadChannelProfile(channelprofileObj);
            } else {
                channelprofileObj = (ChannelProfile) channelpobj;
                LoadSettings.LoadChannelProfile(channelprofileObj);
            }
            //end of new addition

            int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
            String channelid = session.getChannelid();

            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        OperatorsManagement oManagement = new OperatorsManagement();
                        Operators[] aOperator = oManagement.getAdminOperator(channels.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channels.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channels.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            log.debug("resetUser" + "INVALID IP REQUEST!!!");
                            log.debug("resetUser" + "" + -8);
                            aStatus.errorMessage = "INVALID IP REQUEST!!!";
                            aStatus.status = "" + -8;
                            return aStatus;
                        }

                        suTemplate.close();
                        sTemplate.close();
                        log.debug("resetUser" + "INVALID IP REQUEST!!!");
                        log.debug("resetUser" + "" + -8);
                        aStatus.errorMessage = "INVALID IP REQUEST!!!";
                        aStatus.status = "" + -8;
                        return aStatus;
                    }
                    log.debug("resetUser" + "INVALID IP REQUEST!!!");
                    log.debug("resetUser" + "" + -8);
                    aStatus.errorMessage = "INVALID IP REQUEST!!!";
                    aStatus.status = "" + -8;
                    return aStatus;
                }
            }
            if (type == RESET_USER_PASSWORD) {
                UserManagement uManagement = new UserManagement();
                retValue = uManagement.resetPassword(systemSessionId, session.getChannelid(), userid);

                if (retValue == 0) {
                    log.debug("resetUser" + "SUCCESS");
                    log.debug("resetUser" + "" + 0);
                    aStatus.errorMessage = "SUCCESS";
                    aStatus.status = "" + 0;
                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channels.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(), "Reset Password",
                            aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "User Management",
                            "Old Password = ******",
                            "New Password = ******",
                            "USERPASSWORD", userid);

                    //return aStatus;
                } else {
                    log.debug("resetUser" + "" + -1);
                    aStatus.status = "" + -1;
                    // return aStatus;
                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channels.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(), "Reset Password",
                            aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "User Management",
                            "Old Password = ******",
                            "Failed to reset password",
                            "USERPASSWORD", userid);
                }

                return aStatus;
            } else if (type == RESET_USER_TOKEN_OOB) {

                //OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                TokenStatusDetails[] tokens = oManagement.getTokenList(systemSessionId, session.getChannelid(), userid);
                TokenStatusDetails tokenSelected = null;
                for (int i = 0; i < tokens.length; i++) {
                    if (tokens[i].Catrgory == OTPTokenManagement.OOB_TOKEN) {
                        tokenSelected = tokens[i];
                        break;
                    }
                }

                if (tokenSelected == null) {
                    //return ERROR;
                    log.debug("resetUser" + "" + -2);
                    log.debug("resetUser" + "Desired OOB Token is not assigned");
                    aStatus.status = "" + -2;
                    aStatus.errorMessage = "Desired OOB Token is not assigned";
                    return aStatus;
                }

                //added by vikram for removing old tokens from the system as acleda IB does not do that so token gets lost forever.   
//                try {
//                    for (int i = 0; i < tokens.length; i++) {
//                        //unassign the token
//                        retValue = oManagement.ChangeStatus(systemSessionId, session.getChannelid(), userid, TOKEN_STATUS_UNASSIGNED, tokens[i].Catrgory, tokens[i].SubCategory);
//                        if (retValue == 0) {
//                            aStatus.errorMessage = "SUCCESS";
//                            aStatus.status = "" + 0;
//                            audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
//                                    req.getRemoteAddr(), channels.getName(),
//                                    session.getLoginid(), session.getLoginid(), new Date(),
//                                    "Change Status", aStatus.errorMessage, Integer.parseInt(aStatus.status),
//                                    "OTPToken Management",
//                                    "Category=" + tokens[i].Catrgory + ",Subcategory=" + tokens[i].SubCategory,
//                                    "New Status =" + strUNASSIGNED,
//                                    "OTPTOKENS", userid);
//                        }
//                    }
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
                //end of addition
                if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_SMS) {
                    strSubCategory = "OOB__SMS_TOKEN";
                } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_USSD) {
                    strSubCategory = "OOB__USSD_TOKEN";
                } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_VOICE) {
                    strSubCategory = "OOB__VOICE_TOKEN";
                } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_EMAIL) {
                    strSubCategory = "OOB__EMAIL_TOKEN";
                }
                String strStatus = "";
                if (check.equals("1")) {
                    strStatus = strACTIVE;
                    retValue = oManagement.ChangeStatus(systemSessionId, session.getChannelid(), userid, TOKEN_STATUS_ACTIVE, OTPTokenManagement.OOB_TOKEN, tokenSelected.SubCategory);
                } else {
                    strStatus = strUNASSIGNED;
                    retValue = oManagement.ChangeStatus(systemSessionId, session.getChannelid(), userid, TOKEN_STATUS_UNASSIGNED, OTPTokenManagement.OOB_TOKEN, tokenSelected.SubCategory);
                }

                if (retValue == 0) {

                    aStatus.errorMessage = "SUCCESS";
                    aStatus.status = "" + 0;
                    log.debug("resetUser" + "" + 0);
                    log.debug("resetUser" + "SUCCESS");
                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channels.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(),
                            "Change Status", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "OTPToken Management",
                            "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                            "New Status =" + strUNASSIGNED,
                            "OTPTOKENS", userid);
                } else if (retValue == -6) {

                    log.debug("resetUser" + "Token is already active!!!");
                    log.debug("resetUser" + "" + -6);
                    aStatus.errorMessage = "Token is already active!!!";
                    aStatus.status = "" + -6;
                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channels.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(),
                            "Change Status", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "OTPToken Management",
                            "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                            aStatus.errorMessage,
                            "OTPTOKENS", userid);
                } else if (retValue == -3) {
                    log.debug("resetUser" + "Token not found!!!");
                    log.debug("resetUser" + "" + -3);
                    aStatus.errorMessage = "Token not found!!!";
                    aStatus.status = "" + -3;
                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channels.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(),
                            "Change Status", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "OTPToken Management",
                            "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                            aStatus.errorMessage,
                            "OTPTOKENS", userid);
                } else {
                    log.debug("resetUser" + "Invalid Session used.");
                    log.debug("resetUser" + "" + -2);
                    aStatus.status = "" + -2;
                    aStatus.errorMessage = "Invalid Session used.";
                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channels.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(),
                            "Change Status", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "OTPToken Management",
                            "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                            aStatus.errorMessage,
                            "OTPTOKENS", userid);
                    //  return aStatus;
                }

            } else if (type == RESET_USER_TOKEN_SOFTWARE) {

                //OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                TokenStatusDetails[] tokens = oManagement.getTokenList(systemSessionId, session.getChannelid(), userid);
                TokenStatusDetails tokenSelected = null;
                for (int i = 0; i < tokens.length; i++) {
                    if (tokens[i].Catrgory == OTPTokenManagement.SOFTWARE_TOKEN) {
                        tokenSelected = tokens[i];
                        break;
                    }
                }

                if (tokenSelected == null) {
                    //return ERROR;
                    log.debug("resetUser" + "Desired Software Token is not assigned");
                    log.debug("resetUser" + "" + -2);
                    aStatus.status = "" + -2;
                    aStatus.errorMessage = "Desired Software Token is not assigned";
                    return aStatus;
                }

                //added by vikram for removing old tokens from the system as acleda IB does not do that so token gets lost forever.   
//                try {
//                    for (int i = 0; i < tokens.length; i++) {
//                        //unassign the token
//                        retValue = oManagement.ChangeStatus(systemSessionId, session.getChannelid(), userid, TOKEN_STATUS_UNASSIGNED, tokens[i].Catrgory, tokens[i].SubCategory);
//                        if (retValue == 0) {
//                            aStatus.errorMessage = "SUCCESS";
//                            aStatus.status = "" + 0;
//                            audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
//                                    req.getRemoteAddr(), channels.getName(),
//                                    session.getLoginid(), session.getLoginid(), new Date(),
//                                    "Change Status", aStatus.errorMessage, Integer.parseInt(aStatus.status),
//                                    "OTPToken Management",
//                                    "Category=" + tokens[i].Catrgory + ",Subcategory=" + tokens[i].SubCategory,
//                                    "New Status =" + strUNASSIGNED,
//                                    "OTPTOKENS", userid);
//                        }
//                    }
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
                //end of addition
                if (tokenSelected.SubCategory == OTP_TOKEN_SOFTWARE_WEB) {
                    strSubCategory = "SW_WEB_TOKEN";
                } else if (tokenSelected.SubCategory == OTP_TOKEN_SOFTWARE_MOBILE) {
                    strSubCategory = "SW_MOBILE_TOKEN";
                } else if (tokenSelected.SubCategory == OTP_TOKEN_SOFTWARE_PC) {
                    strSubCategory = "SW_PC_TOKEN";
                }

                if (check.equals("1")) {
                    retValue = oManagement.ChangeStatus(systemSessionId, session.getChannelid(), userid, TOKEN_STATUS_ACTIVE, OTPTokenManagement.SOFTWARE_TOKEN, tokenSelected.SubCategory);
                } else {
                    retValue = oManagement.ChangeStatus(systemSessionId, session.getChannelid(), userid, TOKEN_STATUS_UNASSIGNED, OTPTokenManagement.SOFTWARE_TOKEN, tokenSelected.SubCategory);
                }

                if (retValue == 0) {
                    log.debug("resetUser" + "SUCCESS");
                    log.debug("resetUser" + "" + 0);
                    aStatus.errorMessage = "SUCCESS";
                    aStatus.status = "" + 0;
                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channels.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(),
                            "Change Status", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "OTPToken Management",
                            "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                            "New Status =" + strUNASSIGNED,
                            "OTPTOKENS", userid);
                } else if (retValue == -6) {
                    log.debug("resetUser" + "Token is already active!!!");
                    log.debug("resetUser" + "" + -6);
                    aStatus.errorMessage = "Token is already active!!!";
                    aStatus.status = "" + -6;
                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channels.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(),
                            "Change Status", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "OTPToken Management",
                            "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                            aStatus.errorMessage,
                            "OTPTOKENS", userid);
                } else if (retValue == -3) {
                    log.debug("resetUser" + "Token not found!!!");
                    log.debug("resetUser" + "" + -3);
                    aStatus.errorMessage = "Token not found!!!";
                    aStatus.status = "" + -3;
                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channels.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(),
                            "Change Status", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "OTPToken Management",
                            "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                            aStatus.errorMessage,
                            "OTPTOKENS", userid);
                } else {
                    log.debug("resetUser" + "Invalid Session used.");
                    log.debug("resetUser" + "" + -2);
                    aStatus.status = "" + -2;
                    aStatus.errorMessage = "Invalid Session used.";
                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channels.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(),
                            "Change Status", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "OTPToken Management",
                            "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                            aStatus.errorMessage,
                            "OTPTOKENS", userid);
                    //  return aStatus;
                }

            } else if (type == RESET_USER_TOKEN_HARDWARE) {

                //OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                TokenStatusDetails[] tokens = oManagement.getTokenList(systemSessionId, session.getChannelid(), userid);
                TokenStatusDetails tokenSelected = null;
                for (int i = 0; i < tokens.length; i++) {
                    if (tokens[i].Catrgory == OTPTokenManagement.HARDWARE_TOKEN) {
                        tokenSelected = tokens[i];
                        break;
                    }
                }

                if (tokenSelected == null) {
                    //return ERROR;
                    log.debug("resetUser" + "Desired Hardware Token is not assigned");
                    log.debug("resetUser" + "" + -2);
                    aStatus.status = "" + -2;
                    aStatus.errorMessage = "Desired Hardware Token is not assigned";
                    return aStatus;
                }

                //added by vikram for removing old tokens from the system as acleda IB does not do that so token gets lost forever.   
//                try {
//                    for (int i = 0; i < tokens.length; i++) {
//                        //unassign the token
//                        retValue = oManagement.ChangeStatus(systemSessionId, session.getChannelid(), userid, TOKEN_STATUS_UNASSIGNED, tokens[i].Catrgory, tokens[i].SubCategory);
//                        if (retValue == 0) {
//                            aStatus.errorMessage = "SUCCESS";
//                            aStatus.status = "" + 0;
//                            audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
//                                    req.getRemoteAddr(), channels.getName(),
//                                    session.getLoginid(), session.getLoginid(), new Date(),
//                                    "Change Status", aStatus.errorMessage, Integer.parseInt(aStatus.status),
//                                    "OTPToken Management",
//                                    "Category=" + tokens[i].Catrgory + ",Subcategory=" + tokens[i].SubCategory,
//                                    "New Status =" + strUNASSIGNED,
//                                    "OTPTOKENS", userid);
//                        }
//                    }
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
                //end of addition
                if (tokenSelected.SubCategory == OTP_TOKEN_HARDWARE_CR) {
                    strSubCategory = "HW_CR_TOKEN";
                } else if (tokenSelected.SubCategory == OTP_TOKEN_HARDWARE_MINI) {
                    strSubCategory = "HW_MINI_TOKEN";
                }

                if (check.equals("1")) {
                    retValue = oManagement.ChangeStatus(systemSessionId, session.getChannelid(), userid, TOKEN_STATUS_ACTIVE, OTPTokenManagement.HARDWARE_TOKEN, tokenSelected.SubCategory);
                } else {
                    retValue = oManagement.ChangeStatus(systemSessionId, session.getChannelid(), userid, TOKEN_STATUS_UNASSIGNED, OTPTokenManagement.HARDWARE_TOKEN, tokenSelected.SubCategory);
                }

                if (retValue == 0) {
                    log.debug("resetUser" + "SUCCESS");
                    log.debug("resetUser" + "" + 0);
                    aStatus.errorMessage = "SUCCESS";
                    aStatus.status = "" + 0;
                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channels.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(),
                            "RESET", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "OTPToken Management",
                            "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                            "New Status =" + strUNASSIGNED,
                            "OTPTOKENS", userid);

                } else if (retValue == -6) {
                    log.debug("resetUser" + "Token is already active!!!");
                    log.debug("resetUser" + "" + -6);
                    aStatus.errorMessage = "Token is already active!!!";
                    aStatus.status = "" + -6;
                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channels.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(),
                            "RESET", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "OTPToken Management",
                            "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                            aStatus.errorMessage,
                            "OTPTOKENS", userid);
                } else if (retValue == -3) {
                    log.debug("resetUser" + "Token not found!!!");
                    log.debug("resetUser" + "" + -3);
                    aStatus.errorMessage = "Token not found!!!";
                    aStatus.status = "" + -3;
                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channels.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(),
                            "RESET", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "OTPToken Management",
                            "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                            aStatus.errorMessage,
                            "OTPTOKENS", userid);
                } else {
                    log.debug("resetUser::" + "Invalid Session used.");
                    log.debug("resetUser::" + "" + -2);
                    aStatus.status = "" + -2;
                    aStatus.errorMessage = "Invalid Session used.";
                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channels.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(),
                            "RESET", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "OTPToken Management",
                            "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                            aStatus.errorMessage,
                            "OTPTOKENS", userid);
                    //  return aStatus;
                }

                audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                        req.getRemoteAddr(), channels.getName(),
                        session.getLoginid(), session.getLoginid(), new Date(),
                        "RESET",
                        aStatus.errorMessage, Integer.parseInt(aStatus.status),
                        "Token Management",
                        "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                        "New Status =" + strUNASSIGNED,
                        "HARDWARETOKEN", userid);

            }

            return aStatus;
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            strException = e.getMessage();
        }
        Response aStatus1 = new Response();
        int retValue = -99;
        log.debug("resetUser::" + strException);
        log.debug("resetUser::" + "" + retValue);
        aStatus1.errorMessage = strException;
        aStatus1.status = "" + retValue;
        return aStatus1;
    }

    @Override
    public AuthenticationResponse changeEbankingPassword(String systemSessionId, String userid, String oldpassword, String newpassword, String channel, String domain) {

        String strDebug = null;
        try {
            log.info("Entered changeEbankingPassword()");
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                Date d = new Date();
                log.info(d + ">>" + "changeEbankingPassword::systemSessionId::" + systemSessionId);
                log.info(d + ">>" + "changeEbankingPassword::userid::" + userid);
                // System.out.println(d + ">>" + "changeEbankingPassword::oldpassword::" + oldpassword);
                // System.out.println(d + ">>" + "changeEbankingPassword::newpassword::" + newpassword);
                log.info(d + ">>" + "changeEbankingPassword::channel::" + channel);
                log.info(d + ">>" + "changeEbankingPassword::domain::" + domain);
            }
        } catch (Exception ex) {
        }

        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AuthenticationResponse aStatus = new AuthenticationResponse();
            log.debug("changeEbankingPassword::" + "Licence is invalid");
            log.debug("changeEbankingPassword::" + "" + -100);
            log.debug("changeEbankingPassword::" + systemSessionId);
            aStatus.errorMessage = "Licence is invalid";
            aStatus.status = "" + -100;
            aStatus.session = systemSessionId;
            return aStatus;
        }

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_PASSWORD) != 0) {
            AuthenticationResponse aStatus = new AuthenticationResponse();
            log.debug("changeEbankingPassword::" + "Password feature is not available in this license!!!");
            log.debug("changeEbankingPassword::" + "" + -101);
            log.debug("changeEbankingPassword::" + systemSessionId);

            aStatus.errorMessage = "Password feature is not available in this license!!!";
            aStatus.status = "" + -101;
            aStatus.session = systemSessionId;
            return aStatus;
        }

        String strException = "";
        try {

            if (systemSessionId == null || userid == null
                    //|| oldpassword == null 
                    || newpassword == null
                    || systemSessionId.isEmpty() == true
                    || userid.isEmpty() == true
                    //|| oldpassword.isEmpty() == true 
                    || newpassword.isEmpty() == true) {
                AuthenticationResponse aStatus = new AuthenticationResponse();
                log.debug("changeEbankingPassword::" + "Invalid Data");
                log.debug("changeEbankingPassword::" + "" + -11);
                log.debug("changeEbankingPassword::" + systemSessionId);
                aStatus.errorMessage = "Invalid Data";
                aStatus.status = "" + -11;
                aStatus.session = systemSessionId;
                return aStatus;
            }

            UserManagement uManagement = new UserManagement();
            SessionManagement sManagement = new SessionManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(systemSessionId);
            AuthenticationResponse aStatus = new AuthenticationResponse();
            int retValue = -1;
            log.debug("changeEbankingPassword::" + "ERROR");
            log.debug("changeEbankingPassword::" + "" + retValue);
            log.debug("changeEbankingPassword::" + systemSessionId);
            aStatus.errorMessage = "ERROR";
            aStatus.status = "" + retValue;
            aStatus.session = systemSessionId;

            if (session == null) {
                log.debug("changeEbankingPassword::" + "INVALID SESSION ID");
                log.debug("changeEbankingPassword::" + "" + -11);
                log.debug("changeEbankingPassword::" + systemSessionId);
                aStatus.errorMessage = "INVALID SESSION ID";
                aStatus.status = "" + -11;
                aStatus.session = systemSessionId;
                return aStatus;
            }

            AuditManagement audit = new AuditManagement();
            ChannelManagement cManagement = new ChannelManagement();
            Channels channels = cManagement.getChannelByID(session.getChannelid());

            if (session != null) {

                //System.out.println("Client IP = " + req.getRemoteAddr());        
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();

                if (channel == null) {
                    return null;
                }

                //new addition
                ChannelProfile channelprofileObj = null;
                Object channelpobj = setManagement.getSettingInner(session.getChannelid(),
                        SettingsManagement.CHANNELPROFILE_SETTING, 1);

                if (channelpobj == null) {
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                } else {
                    channelprofileObj = (ChannelProfile) channelpobj;
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                }
                //end of new addition

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channels.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channels.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channels.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                log.debug("changeEbankingPassword::" + "INVALID IP REQUEST");
                                log.debug("changeEbankingPassword::" + "" + -8);
                                log.debug("changeEbankingPassword::" + systemSessionId);
                                aStatus.errorMessage = "INVALID IP REQUEST";
                                aStatus.status = "" + -8;
                                aStatus.session = systemSessionId;
                                return aStatus;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            log.debug("changeEbankingPassword::" + "INVALID IP REQUEST");
                            log.debug("changeEbankingPassword::" + "" + -8);
                            log.debug("changeEbankingPassword::" + systemSessionId);
                            aStatus.errorMessage = "INVALID IP REQUEST";
                            aStatus.status = "" + -8;
                            aStatus.session = systemSessionId;
                            return aStatus;
                        }
                        log.debug("changeEbankingPassword::" + "INVALID IP REQUEST");
                        log.debug("changeEbankingPassword::" + "" + -8);
                        log.debug("changeEbankingPassword::" + systemSessionId);
                        aStatus.errorMessage = "INVALID IP REQUEST";
                        aStatus.status = "" + -8;
                        aStatus.session = systemSessionId;
                        return aStatus;
                    }
                }
                //retValue = uManagement.changePassword(systemSessionId, session.getChannelid(), userid, oldpassword, newpassword);                
                retValue = uManagement.AssignPassword(systemSessionId, session.getChannelid(), userid, newpassword);
                if (retValue == 0) {
                    log.debug("changeEbankingPassword::" + "SUCCESS");
                    log.debug("changeEbankingPassword::" + "" + 0);
                    log.debug("changeEbankingPassword::" + systemSessionId);
                    aStatus.errorMessage = "SUCCESS";
                    aStatus.status = "" + 0;
                    aStatus.session = systemSessionId;
                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channels.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(), "Change Password",
                            aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "User Management", "",
                            "OldPassword=***** ,newPassword=*****,Userid=" + userid,
                            "USERPASSWORD", userid);

                } else {
                    //aStatus.errorMessage = "Failed to change Password/Old Password is Wrong";
                    log.debug("changeEbankingPassword::" + "Failed to Change New Password");
                    log.debug("changeEbankingPassword::" + "" + retValue);
                    log.debug("changeEbankingPassword::" + systemSessionId);
                    aStatus.errorMessage = "Failed to Change New Password";
                    aStatus.status = "" + retValue;
                    aStatus.session = systemSessionId;
                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channels.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(), "Change Password",
                            aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "User Management", "",
                            "Failed to Change passsword",
                            "USERPASSWORD", userid);
                }
                //return aStatus;
            }

            return aStatus;
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            strException = e.getMessage();
        }

        AuthenticationResponse aStatus1 = new AuthenticationResponse();
        int retValue = -99;
        log.debug("changeEbankingPassword::" + strException);
        log.debug("changeEbankingPassword::" + "" + retValue);
        log.debug("changeEbankingPassword::" + systemSessionId);
        aStatus1.errorMessage = strException;
        aStatus1.status = "" + retValue;
        aStatus1.session = systemSessionId;
        return aStatus1;
    }

    @Override
    public Response assignPwdAuthenticationType(String systemSessionId, String userid, String password, String channel, String domain) {

        String strDebug = null;
        try {
            log.info("Entered assignPwdAuthenticationType()");
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                Date d = new Date();
                log.info(d + ">>" + "assignPwdAuthenticationType::systemSessionId::" + systemSessionId);
                log.info(d + ">>" + "assignPwdAuthenticationType::userid::" + userid);
                // System.out.println(d + ">>" + "assignPwdAuthenticationType::password::" + password);
                log.info(d + ">>" + "assignPwdAuthenticationType::channel::" + channel);
                log.info(d + ">>" + "assignPwdAuthenticationType::domain::" + domain);
            }
        } catch (Exception ex) {
        }

        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            Response aStatus = new Response();
            log.debug("assignPwdAuthenticationType::" + "Licence Not Valid");
            log.debug("assignPwdAuthenticationType::" + "" + -100);

            aStatus.errorMessage = "Licence Not Valid";
            aStatus.status = "" + -100;
            return aStatus;
        }

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_PASSWORD) != 0) {
            Response aStatus = new Response();
            log.debug("assignPwdAuthenticationType::" + "Password feature is not available in this license!!!");
            log.debug("assignPwdAuthenticationType::" + "" + -101);
            aStatus.errorMessage = "Password feature is not available in this license!!!";
            aStatus.status = "" + -101;
            return aStatus;

        }

        String strException = "";

        try {

            AuditManagement audit = new AuditManagement();
            ChannelManagement cManagement = new ChannelManagement();
            SessionManagement sManagement = new SessionManagement();
            Sessions session = sManagement.getSessionById(systemSessionId);
            Channels channels = cManagement.getChannelByID(session.getChannelid());
            Response aStatus = new Response();
            int retValue = -1;
            log.debug("assignPwdAuthenticationType::" + "ERROR");
            log.debug("assignPwdAuthenticationType::" + "" + retValue);
            aStatus.errorMessage = "ERROR";
            aStatus.status = "" + retValue;
            if (channels == null) {
                return aStatus;
            }

            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);

            try {
                UserManagement uManagement = new UserManagement();

                if (session == null) {
                    return aStatus;
                }

                if (session != null) {

                    //System.out.println("Client IP = " + req.getRemoteAddr());        
                    SettingsManagement setManagement = new SettingsManagement();
                    String channelid = session.getChannelid();

                    //new addition
                    ChannelProfile channelprofileObj = null;
                    Object channelpobj = setManagement.getSettingInner(session.getChannelid(),
                            SettingsManagement.CHANNELPROFILE_SETTING, 1);

                    if (channelpobj == null) {
                        LoadSettings.LoadChannelProfile(channelprofileObj);
                    } else {
                        channelprofileObj = (ChannelProfile) channelpobj;
                        LoadSettings.LoadChannelProfile(channelprofileObj);
                    }
                    //end of new addition

                    Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                    if (ipobj != null) {
                        GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                        int checkIp = 1;
                        if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                            checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                        } else {
                            checkIp = 1;
                        }
                        if (iObj.ipstatus == 0 && checkIp != 1) {
                            if (iObj.ipalertstatus == 0) {
                                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                                Session sTemplate = suTemplate.openSession();
                                TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                                Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                                OperatorsManagement oManagement = new OperatorsManagement();
                                Operators[] aOperator = oManagement.getAdminOperator(channels.getChannelid());
                                if (aOperator != null) {
                                    String[] emailList = new String[aOperator.length - 1];
                                    for (int i = 1; i < aOperator.length; i++) {
                                        emailList[i - 1] = aOperator[i].getEmailid();
                                    }
                                    if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                        ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                        String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                        String strsubject = templatesObj.getSubject();
                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                        if (strmessageBody != null) {
                                            // Date date = new Date();
                                            strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                            strmessageBody = strmessageBody.replaceAll("#channel#", channels.getName());
                                            strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                            strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                            strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                        }

                                        SendNotification send = new SendNotification();
                                        AXIOMStatus axStatus = send.SendEmail(channels.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                                emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                    }

                                    suTemplate.close();
                                    sTemplate.close();
                                    log.debug("assignPwdAuthenticationType::" + "INVALID IP REQUEST");
                                    log.debug("assignPwdAuthenticationType::" + "" + -8);
                                    aStatus.errorMessage = "INVALID IP REQUEST";
                                    aStatus.status = "" + -8;
                                    return aStatus;

                                }

                                suTemplate.close();
                                sTemplate.close();
                                log.debug("assignPwdAuthenticationType::" + "INVALID IP REQUEST");
                                log.debug("assignPwdAuthenticationType::" + "" + -8);
                                aStatus.errorMessage = "INVALID IP REQUEST";
                                aStatus.status = "" + -8;
                                return aStatus;

                            }
                            log.debug("assignPwdAuthenticationType::" + "INVALID IP REQUEST");
                            log.debug("assignPwdAuthenticationType::" + "" + -8);
                            aStatus.errorMessage = "INVALID IP REQUEST";
                            aStatus.status = "" + -8;
                            return aStatus;

                        }
                    }

                    retValue = uManagement.AssignPassword(systemSessionId, session.getChannelid(), userid, password);

                    if (retValue == 0) {
                        boolean bSentToUser = false; // commented for ACLEDA DEMO
                        if (bSentToUser == true) {
                            SendNotification send = new SendNotification();
                            AuthUser aUser = uManagement.getUser(systemSessionId, session.getChannelid(), userid);
                            Templates temp = null;
                            TemplateManagement tObj = new TemplateManagement();
                            temp = tObj.LoadbyName(systemSessionId, session.getChannelid(), TemplateNames.MOBILE_USER_PASSWORD_TEMPLATE);

                            if (temp == null) {
                                log.debug("assignPwdAuthenticationType::" + "Message Template is missing");
                                log.debug("assignPwdAuthenticationType::" + "" + -2);
                                aStatus.errorMessage = "Message Template is missing";
                                aStatus.status = "" + -2;
                                //return aStatus;
                            }

                            ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
                            String templatebody = (String) TemplateUtils.deserializeFromObject(bais);

                            templatebody = templatebody.replaceAll("#name#", aUser.userName);
                            String date = String.valueOf(new Date());
                            //templatebody = templatebody.replaceAll("#Sender#", "Axiom");
                            templatebody = templatebody.replaceAll("#date#", date);

                            com.mollatech.axiom.connector.communication.AXIOMStatus axiomStatus = null;

                            int iProductType = Integer.valueOf(LoadSettings.g_sSettings.getProperty("product.type")).intValue();

                            if (temp.getStatus() == tObj.ACTIVE_STATUS) {
                                axiomStatus = send.SendOnMobile(session.getChannelid(), aUser.phoneNo, templatebody, SendNotification.SMS, iProductType);
                            }
                            if (axiomStatus != null) {
                                log.debug("assignPwdAuthenticationType::" + axiomStatus.strStatus);
                                log.debug("assignPwdAuthenticationType::" + axiomStatus.iStatus);
                                aStatus.errorMessage = axiomStatus.strStatus;
                                aStatus.status = "" + axiomStatus.iStatus;
                            }
                        } else {
                            log.debug("assignPwdAuthenticationType::" + "SUCCESS");
                            log.debug("assignPwdAuthenticationType::" + "" + 0);
                            aStatus.errorMessage = "SUCCESS";
                            aStatus.status = "" + 0;
                        }

                        audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                channels.getName(),
                                session.getLoginid(), session.getLoginid(),
                                new Date(),
                                "Assign Password", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                                "User Management",
                                "Old Password=******",
                                "New Password=******",
                                "USERPASSWORD", userid);

                    } else {
                        audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                channels.getName(),
                                session.getLoginid(), session.getLoginid(),
                                new Date(),
                                "Assign Password", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                                "User Management",
                                "Old Password=******",
                                "Failed To Assign Password...!!!",
                                "USERPASSWORD", userid);
                    }
                }
            } catch (java.lang.Exception ex) {
                ex.printStackTrace();
            } finally {
            }

            return aStatus;

        } catch (Exception ex) {
            strException = ex.getMessage();
            ex.printStackTrace();
        }
        Response aStatus = new Response();
        int retValue = -99;
        log.debug("assignPwdAuthenticationType::" + strException);
        log.debug("assignPwdAuthenticationType::" + "" + retValue);
        aStatus.errorMessage = strException;
        aStatus.status = "" + retValue;
        return aStatus;
    }

    @Override
    public AuthenticationResponse verifyEBankingOTP(String systemSessionId, String userid, String otptoken, String password, String channel, String domain) {
        String strException = "";

        String strDebug = null;
        try {
            log.info("Entered verifyEBankingOTP()");
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                Date d = new Date();
                log.info(d + ">>" + "verifyEBankingOTP::systemSessionId::" + systemSessionId);
                log.info(d + ">>" + "verifyEBankingOTP::userid::" + userid);
                log.info(d + ">>" + "verifyEBankingOTP::otptoken::" + otptoken);
                log.info(d + ">>" + "verifyEBankingOTP::password::" + password);
                log.info(d + ">>" + "verifyEBankingOTP::channel::" + channel);
                log.info(d + ">>" + "verifyEBankingOTP::domain::" + domain);
            }
        } catch (Exception ex) {
        }

        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AuthenticationResponse aStatus = new AuthenticationResponse();
            log.debug("verifyEBankingOTP::" + "Licence Not Valid");
            log.debug("verifyEBankingOTP::" + "" + -100);
            aStatus.errorMessage = "Licence Not Valid";
            aStatus.status = "" + -100;
            aStatus.session = systemSessionId;
            return aStatus;
        }

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_PASSWORD) != 0) {
            AuthenticationResponse aStatus = new AuthenticationResponse();
            log.debug("verifyEBankingOTP::" + "Password feature is not available in this license!!!");
            log.debug("verifyEBankingOTP::" + "" + -101);
            log.debug("verifyEBankingOTP::" + "" + systemSessionId);
            aStatus.errorMessage = "Password feature is not available in this license!!!";
            aStatus.status = "" + -101;
            aStatus.session = systemSessionId;
            return aStatus;
        }

        try {

            //added for acleda
            int iTokenType = 0;
            if (otptoken == null || otptoken.isEmpty() == true) {
                iTokenType = -1; // check all tokens of user.
            } else if (otptoken.compareToIgnoreCase("HARDOTP") == 0) {
                iTokenType = 2; //Integer.parseInt(otptoken); //1=SW, 2=HW, 3=OOB
            } else if (otptoken.compareToIgnoreCase("SOFTOTP") == 0) {
                iTokenType = 1; //Integer.parseInt(otptoken); //1=SW, 2=HW, 3=OOB
            } else if (otptoken.compareToIgnoreCase("OOBOTP") == 0) {
                iTokenType = 3; //Integer.parseInt(otptoken); //1=SW, 2=HW, 3=OOB
            } else {
                //error
                AuthenticationResponse aStatus = new AuthenticationResponse();
                log.debug("verifyEBankingOTP::" + "Invalid Token Type!!!");
                log.debug("verifyEBankingOTP::" + "" + -23);
                log.debug("verifyEBankingOTP::" + "" + systemSessionId);
                aStatus.errorMessage = "Invalid Token Type!!!";
                aStatus.status = "" + -23;
                aStatus.session = systemSessionId;
                return aStatus;
            }

            otptoken = password;
            //end of addition

            if (systemSessionId == null || userid == null
                    || systemSessionId.isEmpty() == true || userid.isEmpty() == true
                    || otptoken == null || otptoken.isEmpty() == true) {
                AuthenticationResponse aStatus = new AuthenticationResponse();
                log.debug("verifyEBankingOTP::" + "Invalid Data");
                log.debug("verifyEBankingOTP::" + "" + -11);
                log.debug("verifyEBankingOTP::" + "" + systemSessionId);
                aStatus.errorMessage = "Invalid Data";
                aStatus.status = "" + -11;
                aStatus.session = systemSessionId;
                return aStatus;
            }

            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);

            Sessions session = null;

            if (gs_session == null) {
                SessionManagement sManagement = new SessionManagement();
                session = sManagement.getSessionById(systemSessionId);
                gs_session = session;

                ChannelManagement cManagement = new ChannelManagement();
                gs_channels = cManagement.getChannelByID(gs_session.getChannelid());
                if (gs_channels == null) {
                    AuthenticationResponse aStatus = new AuthenticationResponse();
                    log.debug("verifyEBankingOTP::" + "Invalid Channel");
                    log.debug("verifyEBankingOTP::" + "" + -15);
                    log.debug("verifyEBankingOTP::" + "" + systemSessionId);
                    aStatus.errorMessage = "Invalid Channel";
                    aStatus.status = "" + -15;
                    aStatus.session = systemSessionId;
                    return aStatus;
                }
            }

            AuthenticationResponse aStatus = new AuthenticationResponse();
            int retValue = -1;
            log.debug("verifyEBankingOTP::" + "ERROR");
            log.debug("verifyEBankingOTP::" + "" + retValue);
            log.debug("verifyEBankingOTP::" + "" + systemSessionId);
            aStatus.errorMessage = "ERROR";
            aStatus.status = "" + retValue;
            aStatus.session = systemSessionId;
            if (gs_session == null) {
                log.debug("verifyEBankingOTP::" + "Invalid Session");
                log.debug("verifyEBankingOTP::" + "" + -14);
                log.debug("verifyEBankingOTP::" + "" + systemSessionId);
                aStatus.errorMessage = "Invalid Session";
                aStatus.status = "" + -14;
                aStatus.session = systemSessionId;
                return aStatus;
            } else {
                session = gs_session;
            }

            //OTPTokenManagement oManagement = new OTPTokenManagement(gs_session.getChannelid());
            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());

            Channels channels = gs_channels;

            SettingsManagement setManagement = new SettingsManagement();

            String channelid = session.getChannelid();

            //new addition
            ChannelProfile channelprofileObj = null;
            Object channelpobj = setManagement.getSettingInner(session.getChannelid(),
                    SettingsManagement.CHANNELPROFILE_SETTING, 1);

            if (channelpobj == null) {
                LoadSettings.LoadChannelProfile(channelprofileObj);
            } else {
                channelprofileObj = (ChannelProfile) channelpobj;
                LoadSettings.LoadChannelProfile(channelprofileObj);
            }
            //end of new addition

            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        OperatorsManagement oManagementObj = new OperatorsManagement();
                        Operators[] aOperator = oManagementObj.getAdminOperator(channels.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channels.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channels.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            log.debug("verifyEBankingOTP::" + "INVALID IP REQUEST");
                            log.debug("verifyEBankingOTP::" + "" + -8);
                            log.debug("verifyEBankingOTP::" + "" + systemSessionId);
                            aStatus.errorMessage = "INVALID IP REQUEST";
                            aStatus.status = "" + -8;
                            aStatus.session = systemSessionId;
                            return aStatus;
                        }

                        suTemplate.close();
                        sTemplate.close();
                        log.debug("verifyEBankingOTP::" + "INVALID IP REQUEST");
                        log.debug("verifyEBankingOTP::" + "" + -8);
                        log.debug("verifyEBankingOTP::" + "" + systemSessionId);
                        aStatus.errorMessage = "INVALID IP REQUEST";
                        aStatus.status = "" + -8;
                        aStatus.session = systemSessionId;
                        return aStatus;
                    }
                    log.debug("verifyEBankingOTP::" + "INVALID IP REQUEST");
                    log.debug("verifyEBankingOTP::" + "" + -8);
                    log.debug("verifyEBankingOTP::" + "" + systemSessionId);
                    aStatus.errorMessage = "INVALID IP REQUEST";
                    aStatus.status = "" + -8;
                    aStatus.session = systemSessionId;
                    return aStatus;
                }
            }
            //System.out.println("Before VerifyOTP >> " + otp);
            if (iTokenType == -1) {
                retValue = oManagement.VerifyOTP(gs_session.getChannelid(), userid, systemSessionId, otptoken);
            } else {
                retValue = oManagement.VerifyOTPByType(gs_session.getChannelid(), userid, systemSessionId, otptoken, iTokenType);
            }

            AuditManagement audit = new AuditManagement();

            if (retValue == 0) {
                log.debug("verifyEBankingOTP::" + "SUCCESS");
                log.debug("verifyEBankingOTP::" + "" + 0);
                log.debug("verifyEBankingOTP::" + "" + systemSessionId);
                aStatus.errorMessage = "SUCCESS";
                aStatus.status = "" + 0;
                aStatus.session = systemSessionId;
                audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                        channels.getName(),
                        session.getLoginid(), session.getLoginid(),
                        new Date(),
                        "VERIFY OTP", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                        "Token Management",
                        "",
                        "Verify OTP Successful...!!!",
                        "OTPTOKENS", userid);

            } else if (retValue == -9) {
                log.debug("verifyEBankingOTP::" + "One Time Password is expired...");
                log.debug("verifyEBankingOTP::" + "" + -9);
                log.debug("verifyEBankingOTP::" + "" + systemSessionId);
                aStatus.errorMessage = "One Time Password is expired...";
                aStatus.status = "" + -9;
                aStatus.session = systemSessionId;
                log.debug("verifyEBankingOTP::" + "One Time Password is expired...");
                log.debug("verifyEBankingOTP::" + "" + -9);
                log.debug("verifyEBankingOTP::" + "" + systemSessionId);
                aStatus.errorMessage = "One Time Password is expired...";
                aStatus.status = "" + -9;
                aStatus.session = systemSessionId;
                audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                        channels.getName(),
                        session.getLoginid(), session.getLoginid(),
                        new Date(),
                        "VERIFY OTP", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                        "Token Management",
                        "",
                        aStatus.errorMessage,
                        "OTPTOKENS", userid);
            } else if (retValue == -8) {
                log.debug("verifyEBankingOTP::" + "One Time Password is already consumed!!!");
                log.debug("verifyEBankingOTP::" + "" + -16);
                log.debug("verifyEBankingOTP::" + "" + systemSessionId);
                aStatus.errorMessage = "One Time Password is already consumed!!!";
                aStatus.status = "" + -16;
                aStatus.session = systemSessionId;
                audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                        channels.getName(),
                        session.getLoginid(), session.getLoginid(),
                        new Date(),
                        "VERIFY OTP", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                        "Token Management",
                        "",
                        aStatus.errorMessage,
                        "OTPTOKENS", userid);
            } else if (retValue == -17) {
                log.debug("verifyEBankingOTP::" + "User/Token is not found!!!");
                log.debug("verifyEBankingOTP::" + "" + -17);
                log.debug("verifyEBankingOTP::" + "" + systemSessionId);
                aStatus.errorMessage = "User/Token is not found!!!";
                aStatus.status = "" + -17;
                aStatus.session = systemSessionId;
                audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                        channels.getName(),
                        session.getLoginid(), session.getLoginid(),
                        new Date(),
                        "VERIFY OTP", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                        "Token Management",
                        "",
                        aStatus.errorMessage,
                        "OTPTOKENS", userid);
            } else if (retValue == -22) {
                log.debug("verifyEBankingOTP::" + "Token is locked!!!");
                log.debug("verifyEBankingOTP::" + "" + -22);
                log.debug("verifyEBankingOTP::" + "" + systemSessionId);
                aStatus.errorMessage = "Token is locked!!!";
                aStatus.status = "" + -22;
                aStatus.session = systemSessionId;
                audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                        channels.getName(),
                        session.getLoginid(), session.getLoginid(),
                        new Date(),
                        "VERIFY OTP", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                        "Token Management",
                        "",
                        aStatus.errorMessage,
                        "OTPTOKENS", userid);
            } else {
                log.debug("verifyEBankingOTP::" + "ERROR");
                log.debug("verifyEBankingOTP::" + "" + -1);
                log.debug("verifyEBankingOTP::" + "" + systemSessionId);
                aStatus.errorMessage = "ERROR";
                aStatus.status = "" + -1;
                aStatus.session = systemSessionId;
                audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                        channels.getName(),
                        session.getLoginid(), session.getLoginid(),
                        new Date(),
                        "VERIFY OTP", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                        "Token Management",
                        "",
                        aStatus.errorMessage,
                        "OTPTOKENS", userid);
            }

            try {
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("verifyEBankingOTP result::" + retValue + " for OTP::" + password);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return aStatus;

        } catch (Exception e) {
            e.printStackTrace();
            strException = e.getMessage();
        }
        AuthenticationResponse aStatus = new AuthenticationResponse();
        log.debug("verifyEBankingOTP::" + strException);
        log.debug("verifyEBankingOTP::" + "" + -99);
        log.debug("verifyEBankingOTP::" + "" + systemSessionId);
        aStatus.errorMessage = strException;
        aStatus.status = "" + -99;
        aStatus.session = systemSessionId;
        return aStatus;
    }

    @Override
    public Response createUser(String systemSessionId, String userid, String firstname, String lastname, String postcode, String address1, String address2, String city, String mobileno, String channel, String domain) {
        String strException = "";
        try {
            log.info("Entered createUser()");
            String strDebug = null;
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    Date d = new Date();
                    log.info(d + ">>" + "createUser::systemSessionId::" + systemSessionId);
                    log.info(d + ">>" + "createUser::userid::" + userid);
                    log.info(d + ">>" + "createUser::firstname::" + firstname);
                    log.info(d + ">>" + "createUser::lastname::" + lastname);
                    log.info(d + ">>" + "createUser::postcode::" + postcode);
                    log.info(d + ">>" + "createUser::address1::" + address1);
                    log.info(d + ">>" + "createUser::address2::" + address2);
                    log.info(d + ">>" + "createUser::city::" + city);
                    log.info(d + ">>" + "createUser::mobileno::" + mobileno);
                    log.info(d + ">>" + "createUser::channel::" + channel);
                    log.info(d + ">>" + "createUser::domain::" + domain);
                }
            } catch (Exception ex) {
            }

            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                Response aStatus = new Response();
                log.debug("createUser::" + "Licence Not Valid");
                log.debug("createUser::" + "" + -100);

                aStatus.errorMessage = "Licence Not Valid";
                aStatus.status = "" + -100;
                return aStatus;
            }

            String fullname = userid;
            if (firstname != null && firstname.isEmpty() == false) {
                fullname = firstname;
                if (lastname != null && lastname.isEmpty() == false) {
                    fullname += " " + lastname;
                }
            }
            if (mobileno == null) {
                mobileno = userid;
            }
            //end of comment

            if (systemSessionId == null || fullname == null
                    || systemSessionId.isEmpty() == true || fullname.isEmpty() == true) {
                Response aStatus = new Response();
                log.debug("createUser::" + "Invalid Data");
                log.debug("createUser::" + "" + -11);
                aStatus.errorMessage = "Invalid Data";
                aStatus.status = "" + -11;
                return aStatus;
            }

            if (userid != null && userid.isEmpty() == true) {
                Response aStatus = new Response();
                log.debug("createUser::" + "Setting userid is not supported!!!");
                log.debug("createUser::" + "" + -12);
                aStatus.errorMessage = "Setting userid is not supported!!!";
                aStatus.status = "" + -12;
                return aStatus;
            }

            UserManagement uManagement = new UserManagement();
            SessionManagement sManagement = new SessionManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(systemSessionId);
            Response aStatus = new Response();
            int retValue = -1;
            log.debug("createUser::" + "ERROR");
            log.debug("createUser::" + "" + retValue);
            aStatus.errorMessage = "ERROR";
            aStatus.status = "" + retValue;

            ChannelManagement cManagement = new ChannelManagement();
            Channels channels = cManagement.getChannelByID(session.getChannelid());
            AuditManagement audit = new AuditManagement();

            if (session != null) {

                //System.out.println("Client IP = " + req.getRemoteAddr());        
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();

                //new addition
                ChannelProfile channelprofileObj = null;
                Object channelpobj = setManagement.getSettingInner(session.getChannelid(),
                        SettingsManagement.CHANNELPROFILE_SETTING, 1);

                if (channelpobj == null) {
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                } else {
                    channelprofileObj = (ChannelProfile) channelpobj;
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                }
                //end of new addition

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channels.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channels.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channels.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                log.debug("createUser::" + "INVALID IP REQUEST");
                                log.debug("createUser::" + "" + -8);
                                aStatus.errorMessage = "INVALID IP REQUEST";
                                aStatus.status = "" + -8;
                                return aStatus;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            log.debug("createUser::" + "INVALID IP REQUEST");
                            log.debug("createUser::" + "" + -8);
                            aStatus.errorMessage = "INVALID IP REQUEST";
                            aStatus.status = "" + -8;
                            return aStatus;
                        }
                        log.debug("createUser::" + "INVALID IP REQUEST");
                        log.debug("createUser::" + "" + -8);
                        aStatus.errorMessage = "INVALID IP REQUEST";
                        aStatus.status = "" + -8;
                        return aStatus;
                    }
                }

                //addition for license check enforcement
                if (AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_PASSWORD) != 0) {
                    log.debug("createUser::" + "This feature is not available in this license!!!");
                    log.debug("createUser::" + "" + -100);
                    aStatus.errorMessage = "This feature is not available in this license!!!";
                    aStatus.status = "" + -100;
//                    aStatus.error = "This feature is not available in this license!!!";
//                    aStatus.errorcode = -100;
                    return aStatus;
                }

                int iUserCount = uManagement.getCountOfLicenseUser(channel);
                int licensecount = AxiomProtect.GetUsersAllowed(); //AxiomProtect Function here now default 0. 
                if (licensecount == -998) {
                    //unlimited licensing 
                } else if (iUserCount >= licensecount) {
//                    aStatus.error = "User Addition already reached its limit as per this license!!!";
//                    aStatus.errorcode = -100;
                    log.debug("createUser::" + "User Addition already reached its limit as per this license!!!");
                    log.debug("createUser::" + "" + -100);
                    aStatus.errorMessage = "User Addition already reached its limit as per this license!!!";
                    aStatus.status = "" + -100;
                    return aStatus;
                }
                //end of addition

//                retValue = uManagement.CreateUser(systemSessionId, session.getChannelid(), fullname, mobileno, null, userid,0);
                retValue = uManagement.CreateUser(systemSessionId, session.getChannelid(), fullname, mobileno, null, userid, 0, null, null, null, null, null, null, null, null);
                if (retValue == 0) {
                    log.debug("createUser::" + "SUCCESS");
                    log.debug("createUser::" + "" + 0);
                    aStatus.errorMessage = "SUCCESS";
                    aStatus.status = "" + 0;
                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channels.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(), "Create User",
                            aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "User Management", "",
                            "Name=" + fullname + ",Phone=" + mobileno + ",Userid=" + userid + ",State=Inactive",
                            "USERPASSWORD", session.getLoginid());
                } else {
                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channels.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(), "Create User",
                            "ERROR", -1,
                            "User Management", "",
                            "Name=" + fullname + ",Phone=" + mobileno + ",Userid=" + userid + ",State=Inactive",
                            "USERPASSWORD", session.getLoginid());
                }
                //return aStatus;
            }

            return aStatus;
        } catch (java.lang.Exception e) {
            strException = e.getMessage();
            e.printStackTrace();
        }
        Response aStatus = new Response();
        log.debug("createUser::" + strException);
        log.debug("createUser::" + "" + -99);
        aStatus.errorMessage = strException;
        aStatus.status = "" + -99;
        return aStatus;
    }

    @Override
    public Response systemLogout(String systemSessionId, String channel, String domain) {
        String strException = "";
        try {
            log.info("Entered systemLogout()");
            String strDebug = null;
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    Date d = new Date();
                    log.info(d + ">>" + "systemLogout::systemSessionId::" + systemSessionId);
                    log.info(d + ">>" + "systemLogout::channel::" + channel);
                    log.info(d + ">>" + "systemLogout::domain::" + domain);
                }
            } catch (Exception ex) {
            }

            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                Response aStatus = new Response();
                log.debug("systemLogout::" + "Licence Not Valid");
                log.debug("systemLogout::" + "" + -100);
                aStatus.errorMessage = "Licence Not Valid";
                aStatus.status = "" + -100;
                return aStatus;
            }

            SessionManagement sManagement = new SessionManagement();
            Sessions session = sManagement.getSessionById(systemSessionId);
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Response aResponse = new Response();
            log.debug("systemLogout::" + "ERROR");
            log.debug("systemLogout::" + "" + -2);
            aResponse.errorMessage = "ERROR";
            aResponse.status = "" + -2;

            ChannelManagement cManagement = new ChannelManagement();
            Channels channels = cManagement.getChannelByID(session.getChannelid());
            if (channels == null) {
                return aResponse;
            }
            AuditManagement audit = new AuditManagement();

            if (session != null) {

                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();

                //new addition
                ChannelProfile channelprofileObj = null;
                Object channelpobj = setManagement.getSettingInner(session.getChannelid(),
                        SettingsManagement.CHANNELPROFILE_SETTING, 1);

                if (channelpobj == null) {
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                } else {
                    channelprofileObj = (ChannelProfile) channelpobj;
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                }
                //end of new addition

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channels.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channels.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channels.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                log.debug("systemLogout::" + "INVALID IP REQUEST");
                                log.debug("systemLogout::" + "" + -8);
                                aResponse.errorMessage = "INVALID IP REQUEST";
                                aResponse.status = "" + -8;
                                return aResponse;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            log.debug("systemLogout::" + "INVALID IP REQUEST");
                            log.debug("systemLogout::" + "" + -8);
                            aResponse.errorMessage = "INVALID IP REQUEST";
                            aResponse.status = "" + -8;
                            return aResponse;
                        }
                        log.debug("systemLogout::" + "INVALID IP REQUEST");
                        log.debug("systemLogout::" + "" + -8);
                        aResponse.errorMessage = "INVALID IP REQUEST";
                        aResponse.status = "" + -8;
                        return aResponse;
                    }
                }
                int retValue = sManagement.CloseSession(systemSessionId);
                log.debug("systemLogout::" + "ERROR");
                log.debug("systemLogout::" + "" + retValue);
                aResponse.errorMessage = "ERROR";
                aResponse.status = "" + retValue;

                if (retValue == 0) {
                    log.debug("systemLogout::" + "SUCCESS");
                    log.debug("systemLogout::" + "" + 0);
                    aResponse.errorMessage = "SUCCESS";
                    aResponse.status = "" + 0;
                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(),
                            channels.getName(), session.getLoginid(),
                            session.getLoginid(), new Date(),
                            "Close Session",
                            aResponse.errorMessage, retValue,
                            "Session Management",
                            "Session Opend with sessionId=" + systemSessionId,
                            "Session Close Successfully",
                            "LOGIN",
                            session.getLoginid());

                } else {
                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(),
                            channels.getName(), session.getLoginid(),
                            session.getLoginid(), new Date(),
                            "Close Session",
                            aResponse.errorMessage, retValue,
                            "Session Managment",
                            "",
                            "Session Close Failed",
                            "LOGIN",
                            session.getLoginid());
                }
                //return aStatus;
            } else if (session == null) {
                return aResponse;
            }

            return aResponse;
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            strException = e.getMessage();

        }

        Response aStatus1 = new Response();
        log.debug("systemLogout::" + strException);
        log.debug("systemLogout::" + "" + -99);
        aStatus1.errorMessage = strException;
        aStatus1.status = "" + -99;
        return aStatus1;
    }

    //@Override
    //public AuthenticationResponse systemLogin(String userid, String password, String channel, String domain) {
    @Override
    public AuthenticationResponse systemLogin(String userid, String password, String channel, String domain) {
        String strException = "";
        log.info("Entered systemLogin()");
        try {
            String strDebug = null;

            SettingsManagement setManagement = new SettingsManagement();
            try {

                ChannelProfile channelprofileObj = null;
                Object channelpobj = setManagement.getSettingInner(channel, SettingsManagement.CHANNELPROFILE_SETTING, 1);

                if (channelpobj == null) {
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                } else {
                    channelprofileObj = (ChannelProfile) channelpobj;
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                }

                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    Date d = new Date();
                    log.info(d + ">>" + "systemlogin::userid::" + userid);
                    log.info(d + ">>" + "systemlogin::password::" + password);
                    log.info(d + ">>" + "systemlogin::channel::" + channel);
                    log.info(d + ">>" + "systemlogin::domain::" + domain);
                }
            } catch (Exception ex) {
            }

            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AuthenticationResponse aStatus = new AuthenticationResponse();
                log.debug("systemlogin::" + "Licence Not Valid");
                log.debug("systemlogin::" + "" + -100);
                aStatus.errorMessage = "Licence Not Valid";
                aStatus.status = "" + -100;
                aStatus.session = null;
                return aStatus;
            }

            AuthenticationResponse aResponse = new AuthenticationResponse();
            log.debug("systemlogin::" + "ERROR");
            log.debug("systemlogin::" + "" + -1);
            aResponse.errorMessage = "ERROR";
            aResponse.status = "-1";
            aResponse.session = null;

            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            //System.out.println("Client IP = " + req.getRemoteAddr());

            ChannelManagement cManagement = new ChannelManagement();
            Channels channels = cManagement.getChannelByID(channel);
            AuditManagement audit = new AuditManagement();

            int result = setManagement.checkIP(channel, req.getRemoteAddr());
            String resultStr = "Failure";
            int retValue = -1;
            String channelid = channels.getChannelid();

            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        OperatorsManagement oManagement = new OperatorsManagement();
                        Operators[] aOperator = oManagement.getAdminOperator(channels.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channels.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channels.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            log.debug("systemlogin::" + "INVALID IP REQUEST");
                            log.debug("systemlogin::" + "" + -8);
                            aResponse.errorMessage = "INVALID IP REQUEST";
                            aResponse.status = "-8";
                            aResponse.session = null;

                            audit.AddAuditTrail("INVALIDIP", channel, userid,
                                    req.getRemoteAddr(),
                                    channels.getName(), userid,
                                    userid, new Date(),
                                    "Open Session",
                                    resultStr,
                                    result,
                                    "Session Management",
                                    "",
                                    "Failed to Open Session",
                                    "LOGIN",
                                    userid);
                            return aResponse;
                        }

                        suTemplate.close();
                        sTemplate.close();
                        log.debug("systemlogin::" + "INVALID IP REQUEST");
                        log.debug("systemlogin::" + "" + -8);
                        aResponse.errorMessage = "INVALID IP REQUEST";
                        aResponse.status = "-8";
                        aResponse.session = null;

                        audit.AddAuditTrail("INVALIDIP", channel, userid,
                                req.getRemoteAddr(),
                                channels.getName(), userid,
                                userid, new Date(),
                                "Open Session",
                                resultStr,
                                result,
                                "Session Management",
                                "",
                                "Failed to Open Session",
                                "LOGIN",
                                userid);
                        return aResponse;
                    }
                    log.debug("systemlogin::" + "INVALID IP REQUEST");
                    log.debug("systemlogin::" + "" + -8);
                    aResponse.errorMessage = "INVALID IP REQUEST";
                    aResponse.status = "-8";
                    aResponse.session = null;

                    audit.AddAuditTrail("INVALIDIP", channel, userid,
                            req.getRemoteAddr(),
                            channels.getName(), userid,
                            userid, new Date(),
                            "Open Session",
                            resultStr,
                            result,
                            "Session Management",
                            "",
                            "Failed to Open Session",
                            "LOGIN",
                            userid);
                    return aResponse;
                }
            }

            SessionManagement sManagement = new SessionManagement();
            String sessionId = sManagement.OpenSessionForWS(channel, userid, password, req.getSession().getId());

            if (sessionId != null) {
                retValue = 0;
                resultStr = "success";
                audit.AddAuditTrail(sessionId, channel, userid,
                        req.getRemoteAddr(),
                        channels.getName(), userid,
                        userid, new Date(),
                        "Open Session",
                        resultStr, retValue,
                        "Session Management",
                        "",
                        "Session open with Session ID=" + sessionId,
                        "LOGIN",
                        userid);
                log.debug("systemlogin::" + "SUCCESS");
                log.debug("systemlogin::" + "" + 0);
                log.debug("systemlogin::" + sessionId);
                aResponse.errorMessage = "SUCCESS";
                aResponse.status = "0";
                aResponse.session = sessionId;
            } else if (sessionId == null) {
                log.debug("systemlogin::" + "Session Creation Failed!!!");
                log.debug("systemlogin::" + "" + -4);
                log.debug("systemlogin::" + sessionId);
                aResponse.errorMessage = "Session Creation Failed!!!";
                aResponse.status = "" + -4;
                aResponse.session = sessionId;
            }

            return aResponse;
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            //return null;
            strException = e.getMessage();
        }
        AuthenticationResponse aStatus1 = new AuthenticationResponse();
        log.debug("systemlogin::" + strException);
        log.debug("systemlogin::" + "" + -4);
        log.debug("systemlogin::" + null);
        aStatus1.errorMessage = strException;
        aStatus1.status = "" + -4;
        aStatus1.session = null;
        return aStatus1;
    }

//    @Override
//    public Response assignOtpAuthenticationType(String systemSessionId, String userid, String otptoken, String channel, String domain) {
//
//        String strDebug = null;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                Date d = new Date();
//                System.out.println(d + ">>" + "assignOtpAuthenticationType::systemSessionId::" + systemSessionId);
//                System.out.println(d + ">>" + "assignOtpAuthenticationType::userid::" + userid);
//                System.out.println(d + ">>" + "assignOtpAuthenticationType::otptoken::" + otptoken);
//                System.out.println(d + ">>" + "assignOtpAuthenticationType::channel::" + channel);
//                System.out.println(d + ">>" + "assignOtpAuthenticationType::domain::" + domain);
//            }
//        } catch (Exception ex) {
//        }
//
//        int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//            Response aStatus = new Response();
//            aStatus.errorMessage = "Licence Not Valid";
//            aStatus.status = "" + -100;
//            return aStatus;
//        }
//
//        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_OOB) != 0
//                && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_HARDWARE) != 0
//                && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
//            Response aStatus = new Response();
//            aStatus.errorMessage = "OTP Token feature is not available in this license!!!";
//            aStatus.status = "" + -101;
//            return aStatus;
//        }
//
//        String strException = "";
//
//        try {
//
//            if (systemSessionId == null || userid == null
//                    || systemSessionId.isEmpty() == true || userid.isEmpty() == true) {
//                Response aStatus = new Response();
//                aStatus.errorMessage = "Invalid Data";
//                aStatus.status = "" + -11;
//                return aStatus;
//            }
//            int type = 0;
//            int subtype = 0;
//
//            if (otptoken == null || otptoken.isEmpty() == true) {
//                type = 1;   //software 1
//                subtype = 2; //mobile 2
//            } else {
//                type = 2;   //hardware
//                subtype = 1;    //mini
//            }
//
//            String strCategory = "";
//            if (type == RESET_USER_TOKEN_SOFTWARE) {
//                strCategory = "SOFTWARE_TOKEN";
//            } else if (type == RESET_USER_TOKEN_HARDWARE) {
//                strCategory = "HARDWARE_TOKEN";
//            } else if (type == RESET_USER_TOKEN_OOB) {
//                strCategory = "OOB_TOKEN";
//            }
//
//            String strSubCategory = "";
//            if (subtype == OTP_TOKEN_OUTOFBAND_SMS) {
//                strSubCategory = "OOB__SMS_TOKEN";
//            } else if (subtype == OTP_TOKEN_OUTOFBAND_USSD) {
//                strSubCategory = "OOB__USSD_TOKEN";
//            } else if (subtype == OTP_TOKEN_OUTOFBAND_VOICE) {
//                strSubCategory = "OOB__VOICE_TOKEN";
//            } else if (subtype == OTP_TOKEN_OUTOFBAND_EMAIL) {
//                strSubCategory = "OOB__EMAIL_TOKEN";
//            } else if (subtype == OTP_TOKEN_SOFTWARE_WEB) {
//                strSubCategory = "SW_WEB_TOKEN";
//            } else if (subtype == OTP_TOKEN_SOFTWARE_MOBILE) {
//                strSubCategory = "SW_MOBILE_TOKEN";
//            } else if (subtype == OTP_TOKEN_SOFTWARE_PC) {
//                strSubCategory = "SW_PC_TOKEN";
//            } else if (subtype == OTP_TOKEN_HARDWARE_CR) {
//                strSubCategory = "HW_CR_TOKEN";
//            } else if (subtype == OTP_TOKEN_HARDWARE_MINI) {
//                strSubCategory = "HW_MINI_TOKEN";
//            } else {
//                Response aStatus = new Response();
//                aStatus.errorMessage = "Invalid subcategory!!!";
//                aStatus.status = "" + -13;
//                return aStatus;
//            }
//
//            SessionManagement sManagement = new SessionManagement();
//            AuditManagement audit = new AuditManagement();
//            MessageContext mc = wsContext.getMessageContext();
//            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//            Sessions session = sManagement.getSessionById(systemSessionId);
//            Response aStatus = new Response();
//            int retValue = -1;
//            aStatus.errorMessage = "ERROR";
//            aStatus.status = "" + retValue;
//
//            if (session == null) {
//                aStatus.errorMessage = "Invalid/Expired Session";
//                aStatus.status = "" + -14;
//                return aStatus;
//            }
//            ChannelManagement cManagement = new ChannelManagement();
//            Channels channels = cManagement.getChannelByID(session.getChannelid());
//            if (channels == null) {
//                aStatus.errorMessage = "Invalid Channel";
//                aStatus.status = "" + -15;
//                return aStatus;
//            }
//
//            //OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//            if (session != null) {
//
//                SettingsManagement setManagement = new SettingsManagement();
//                String channelid = session.getChannelid();
//
//                //new addition
//                ChannelProfile channelprofileObj = null;
//                Object channelpobj = setManagement.getSettingInner(session.getChannelid(),
//                        SettingsManagement.CHANNELPROFILE_SETTING, 1);
//
//                if (channelpobj == null) {
//                    LoadSettings.LoadChannelProfile(channelprofileObj);
//                } else {
//                    channelprofileObj = (ChannelProfile) channelpobj;
//                    LoadSettings.LoadChannelProfile(channelprofileObj);
//                }
//                //end of new addition
//
//                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//                if (ipobj != null) {
//                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                    int checkIp = 1;
//                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                    } else {
//                        checkIp = 1;
//                    }
//                    if (iObj.ipstatus == 0 && checkIp != 1) {
//                        if (iObj.ipalertstatus == 0) {
//                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                            Session sTemplate = suTemplate.openSession();
//                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                            OperatorsManagement oManagementObj = new OperatorsManagement();
//                            Operators[] aOperator = oManagementObj.getAdminOperator(channels.getChannelid());
//                            if (aOperator != null) {
//                                String[] emailList = new String[aOperator.length - 1];
//                                for (int i = 1; i < aOperator.length; i++) {
//                                    emailList[i - 1] = aOperator[i].getEmailid();
//                                }
//                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                    String strsubject = templatesObj.getSubject();
//                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                    if (strmessageBody != null) {
//                                        // Date date = new Date();
//                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                        strmessageBody = strmessageBody.replaceAll("#channel#", channels.getName());
//                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                    }
//
//                                    SendNotification send = new SendNotification();
//                                    AXIOMStatus axStatus = send.SendEmail(channels.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                                }
//
//                                suTemplate.close();
//                                sTemplate.close();
//                                aStatus.errorMessage = "INVALID IP REQUEST";
//                                aStatus.status = "" + -8;
//                                return aStatus;
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
//                            aStatus.errorMessage = "INVALID IP REQUEST";
//                            aStatus.status = "" + -8;
//                            return aStatus;
//                        }
//                        aStatus.errorMessage = "INVALID IP REQUEST";
//                        aStatus.status = "" + -8;
//                        return aStatus;
//                    }
//                }
//
//                //addition for mobile token registration code
//                TokenSettings token = (TokenSettings) setManagement.getSetting(systemSessionId, channels.getChannelid(), SettingsManagement.Token, SettingsManagement.PREFERENCE_ONE);
//                if (token == null) {
//                    aStatus.errorMessage = "OTP Token Settings is not configured!!!";
//                    aStatus.status = "" + -17;
//                    return aStatus;
//                }
//                //end of addition
//
//                retValue = oManagement.AssignToken(systemSessionId, session.getChannelid(), userid, type, subtype, otptoken);
//                //added for mobile token registration code sending
//                try {
//                    if (retValue == 0 && type == OTPTokenManagement.SOFTWARE_TOKEN && subtype == OTPTokenManagement.SW_MOBILE_TOKEN) {
//                        //if (tSettings.isbSilentCall() == false) {
//                        String regCode = oManagement.generateRegistrationCode(systemSessionId, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE);
//                        SimpleDateFormat sdfExpiry = new SimpleDateFormat("hh:mm");
//                        Calendar cal = Calendar.getInstance();
//                        cal.add(Calendar.MINUTE, token.getRegistrationValidity());
//                        Date expiry = cal.getTime();
//                        if (regCode != null) {
//                            UserManagement userObj = new UserManagement();
//                            AuthUser user = null;
//                            user = userObj.getUser(systemSessionId, channels.getChannelid(), userid);
//
//                            Templates temp = null;
//                            TemplateManagement tObj = new TemplateManagement();
//                            temp = tObj.LoadbyName(systemSessionId, channels.getChannelid(), TemplateNames.MOBILE_SOFTWARE_TOKEN_REGISTER_TEMPLATE);
//                            ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
//                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                            Date d = new Date();
//                            String tmessage = (String) UtilityFunctions.deserializeFromObject(bais);
//                            tmessage = tmessage.replaceAll("#name#", user.getUserName());
//                            tmessage = tmessage.replaceAll("#channel#", channels.getName());
//                            tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
//                            if (subtype == OTPTokenManagement.SW_MOBILE_TOKEN) {
//                                tmessage = tmessage.replaceAll("#tokentype#", "MOBILE");
//                                String strSWOTPType = LoadSettings.g_sSettings.getProperty("sw.otp.type");
//                                if (strSWOTPType != null && strSWOTPType.compareToIgnoreCase("simple") == 0) { // we are forcing it to be time based for mobile token.
//                                    regCode = "1" + regCode;
//                                    System.out.println("user name = " + user.getUserName() + " with reg code as " + regCode);
//                                }
//                            }
//                            tmessage = tmessage.replaceAll("#regcode#", regCode);
//                            tmessage = tmessage.replaceAll("#expiry#", sdfExpiry.format(expiry));
//                            if (subtype == OTPTokenManagement.SW_WEB_TOKEN) {
//                                tmessage = tmessage.replaceAll("#tokentype#", "WEB");
//
//                            }
//                            if (subtype == OTPTokenManagement.SW_MOBILE_TOKEN) {
//                                tmessage = tmessage.replaceAll("#tokentype#", "MOBILE");
//
//                            }
//                            if (subtype == OTPTokenManagement.SW_PC_TOKEN) {
//                                tmessage = tmessage.replaceAll("#tokentype#", "PC");
//
//                            }
//
//                            SendNotification send = new SendNotification();
//                            //  AxiomStatus status = null;
//                            AXIOMStatus status = null;
//                            if (temp.getStatus() == tObj.ACTIVE_STATUS) {
//                                status = send.SendOnMobileNoWaiting(channels.getChannelid(), user.getPhoneNo(), tmessage, 1, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                            }
//                            if (status != null) {
//                                if (status.iStatus == SEND_MESSAGE_PENDING_STATE) {
//                                    RegistrationCodeTrailManagement rManagement = new RegistrationCodeTrailManagement();
//                                    String registrationcode = UtilityFunctions.Bas64SHA1(regCode);
//                                    rManagement.addRegCodeTrail(channelid, userid, registrationcode, RegistrationCodeTrailManagement.SENT, RegistrationCodeTrailManagement.OTPTOKEN);
//                                }
//                            }
//                            retValue = 0;
//                            aStatus.errorMessage = "SUCCESS";
//                            aStatus.status = "" + 0;
//
//                            audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getChannelid(),
//                                    req.getRemoteAddr(), channels.getName(),
//                                    session.getChannelid(), session.getChannelid(), new Date(), "Generate & Send Registration Code",
//                                    "success", 0, "Token Management", "", "Registration Code = ******" /*strRegCode*/, "OTPTOKENS", userid);
//
//                            //add trail code here
//                        } else if (regCode == null) {
//                            aStatus.errorMessage = "Failed To generate Registration Code";
//                            aStatus.status = "" + -8;
//
//                            audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
//                                    req.getRemoteAddr(), channels.getName(),
//                                    session.getChannelid(), session.getChannelid(), new Date(), "Generate & Send Registration Code",
//                                    "failed", -1, "Token Management", "", "Failed To generate Registration Code", "OTPTOKENS", userid);
//                        }
//
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                //end of addition
//                if (retValue == 0) {
//                    //removed for acleda
//                    retValue = oManagement.ChangeStatus(systemSessionId, session.getChannelid(), userid, OTPTokenManagement.TOKEN_STATUS_ACTIVE, type, subtype);
//                    if (retValue != 0) {
//                        retValue = -15;
//                        aStatus.errorMessage = "Failed in Activation!!!";
//                        aStatus.status = "" + retValue;
//                    }
//                    //end of addition
//
//                }
//                if (retValue == 0) {
//                    aStatus.errorMessage = "SUCCESS";
//                    aStatus.status = "" + 0;
//                } else {
//                    if (retValue == -4) {
//                        aStatus.errorMessage = "Token already assigned!!!";
//                        aStatus.status = "" + -4;
//                    } else if (retValue == -3) {
//                        aStatus.errorMessage = "Empty Serial Number!!!";
//                        aStatus.status = "" + -3;
//                    } else if (retValue == -2) {
//                        aStatus.errorMessage = "Invalid Serial Number!!!";
//                        aStatus.status = "" + -2;
//                    } else if (retValue == -6) {
//                        aStatus.errorMessage = "Token could not be assigned!!!";
//                        aStatus.status = "" + -6;
//                    } else if (retValue == -20) {
//                        aStatus.errorMessage = "Failed to issue registration code!!!";
//                        aStatus.status = "" + -7;
//                    }
//                }
//
//            }
//
//            audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
//                    channels.getName(),
//                    session.getLoginid(), session.getLoginid(),
//                    new Date(),
//                    "Assign OTP Token", aStatus.errorMessage, Integer.parseInt(aStatus.status),
//                    "Token Management",
//                    "",
//                    "Category=" + strCategory + ",Subcategory=" + strSubCategory,
//                    "OTPTOKENS", userid);
//
//            return aStatus;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            strException = ex.getMessage();
//        }
//
//        Response aStatus1 = new Response();
//        aStatus1.errorMessage = strException;
//        aStatus1.status = "" + -99;
//        return aStatus1;
//    }
//
    @Override
    public Response ping() {
        log.debug("ping Entered");
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                Date d = new Date();
                log.debug(d + ">>" + "ping::" + new Date());
            }
        } catch (Exception ex) {
        }
        Response response = new Response();
        log.debug("ping::" + "SUCCESS");
        log.debug("ping::" + "" + 0);

        response.errorMessage = "SUCCESS";
        response.status = "" + 0;
        return response;

    }

    @Override
    public Response assignOtpAuthenticationType(String systemSessionId, String userid, String otptoken, String channel, String domain) {

        String strDebug = null;
        try {
            log.info("Entered assignOtpAuthenticationType()");
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                Date d = new Date();
                log.info(d + ">>" + "assignOtpAuthenticationType::systemSessionId::" + systemSessionId);
                log.info(d + ">>" + "assignOtpAuthenticationType::userid::" + userid);
                log.info(d + ">>" + "assignOtpAuthenticationType::otptoken::" + otptoken);
                log.info(d + ">>" + "assignOtpAuthenticationType::channel::" + channel);
                log.info(d + ">>" + "assignOtpAuthenticationType::domain::" + domain);
            }
        } catch (Exception ex) {
        }

        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            Response aStatus = new Response();
            log.debug("assignOtpAuthenticationType::" + "Licence Not Valid");
            log.debug("assignOtpAuthenticationType::" + "" + -100);
            aStatus.errorMessage = "Licence Not Valid";
            aStatus.status = "" + -100;
            return aStatus;
        }

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_OOB) != 0
                && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_HARDWARE) != 0
                && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
            Response aStatus = new Response();
            log.debug("assignOtpAuthenticationType::" + "OTP Token feature is not available in this license!!!");
            log.debug("assignOtpAuthenticationType::" + "" + -101);
            aStatus.errorMessage = "OTP Token feature is not available in this license!!!";
            aStatus.status = "" + -101;
            return aStatus;
        }

        String strException = "";

        try {

            if (systemSessionId == null || userid == null
                    || systemSessionId.isEmpty() == true || userid.isEmpty() == true) {
                Response aStatus = new Response();
                log.debug("assignOtpAuthenticationType::" + "Invalid Data");
                log.debug("assignOtpAuthenticationType::" + "" + -11);
                aStatus.errorMessage = "Invalid Data";
                aStatus.status = "" + -11;
                return aStatus;
            }
            int type = 0;
            int subtype = 0;

            if (otptoken == null || otptoken.isEmpty() == true) {
                type = 1;   //software 1
                subtype = 2; //mobile 2
            } else if (otptoken.contains("AP") == true) {
                type = 1;   //software 1
                subtype = 2; //mobile 2
            } else {
                type = 2;   //hardware
                subtype = 1;
            }//mini

            String strCategory = "";
            if (type == OTP_TOKEN_SOFTWARE) {
                strCategory = "SOFTWARE_TOKEN";
            } else if (type == OTP_TOKEN_HARDWARE) {
                strCategory = "HARDWARE_TOKEN";
            } else if (type == OTP_TOKEN_OUTOFBAND) {
                strCategory = "OOB_TOKEN";
            }

            String strSubCategory = "";
            if (subtype == OTP_TOKEN_OUTOFBAND_SMS) {
                strSubCategory = "OOB__SMS_TOKEN";
            } else if (subtype == OTP_TOKEN_OUTOFBAND_USSD) {
                strSubCategory = "OOB__USSD_TOKEN";
            } else if (subtype == OTP_TOKEN_OUTOFBAND_VOICE) {
                strSubCategory = "OOB__VOICE_TOKEN";
            } else if (subtype == OTP_TOKEN_OUTOFBAND_EMAIL) {
                strSubCategory = "OOB__EMAIL_TOKEN";
            } else if (subtype == OTP_TOKEN_SOFTWARE_WEB) {
                strSubCategory = "SW_WEB_TOKEN";
            } else if (subtype == OTP_TOKEN_SOFTWARE_MOBILE) {
                strSubCategory = "SW_MOBILE_TOKEN";
            } else if (subtype == OTP_TOKEN_SOFTWARE_PC) {
                strSubCategory = "SW_PC_TOKEN";
            } else if (subtype == OTP_TOKEN_HARDWARE_CR) {
                strSubCategory = "HW_CR_TOKEN";
            } else if (subtype == OTP_TOKEN_HARDWARE_MINI) {
                strSubCategory = "HW_MINI_TOKEN";
            } else {
                Response aStatus = new Response();
                log.debug("assignOtpAuthenticationType::" + "Invalid subcategory!!!");
                log.debug("assignOtpAuthenticationType::" + "" + -13);
                aStatus.errorMessage = "Invalid subcategory!!!";
                aStatus.status = "" + -13;
                return aStatus;
            }

            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(systemSessionId);
            Response aStatus = new Response();
            int retValue = -1;
            log.debug("assignOtpAuthenticationType::" + "ERROR");
            log.debug("assignOtpAuthenticationType::" + "" + retValue);
            aStatus.errorMessage = "ERROR";
            aStatus.status = "" + retValue;

            if (session == null) {
                log.debug("assignOtpAuthenticationType::" + "Invalid/Expired Session");
                log.debug("assignOtpAuthenticationType::" + "" + -14);
                aStatus.errorMessage = "Invalid/Expired Session";
                aStatus.status = "" + -14;
                return aStatus;
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channels = cManagement.getChannelByID(session.getChannelid());
            if (channels == null) {
                log.debug("assignOtpAuthenticationType::" + "Invalid Channel");
                log.debug("assignOtpAuthenticationType::" + "" + -15);
                aStatus.errorMessage = "Invalid Channel";
                aStatus.status = "" + -15;
                return aStatus;
            }

            //OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
            if (session != null) {

                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();

                //new addition
                ChannelProfile channelprofileObj = null;
                Object channelpobj = setManagement.getSettingInner(session.getChannelid(),
                        SettingsManagement.CHANNELPROFILE_SETTING, 1);

                if (channelpobj == null) {
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                } else {
                    channelprofileObj = (ChannelProfile) channelpobj;
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                }
                //end of new addition

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagementObj = new OperatorsManagement();
                            Operators[] aOperator = oManagementObj.getAdminOperator(channels.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channels.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channels.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                log.debug("assignOtpAuthenticationType::" + "INVALID IP REQUEST");
                                log.debug("assignOtpAuthenticationType::" + "" + -8);
                                aStatus.errorMessage = "INVALID IP REQUEST";
                                aStatus.status = "" + -8;
                                return aStatus;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            log.debug("assignOtpAuthenticationType::" + "INVALID IP REQUEST");
                            log.debug("assignOtpAuthenticationType::" + "" + -8);
                            aStatus.errorMessage = "INVALID IP REQUEST";
                            aStatus.status = "" + -8;
                            return aStatus;
                        }
                        log.debug("assignOtpAuthenticationType::" + "INVALID IP REQUEST");
                        log.debug("assignOtpAuthenticationType::" + "" + -8);
                        aStatus.errorMessage = "INVALID IP REQUEST";
                        aStatus.status = "" + -8;
                        return aStatus;
                    }
                }

                //addition for mobile token registration code
                TokenSettings token = (TokenSettings) setManagement.getSetting(systemSessionId, channels.getChannelid(), SettingsManagement.Token, SettingsManagement.PREFERENCE_ONE);
                if (token == null) {
                    log.debug("assignOtpAuthenticationType::" + "OTP Token Settings is not configured!!!");
                    log.debug("assignOtpAuthenticationType::" + "" + -17);
                    aStatus.errorMessage = "OTP Token Settings is not configured!!!";
                    aStatus.status = "" + -17;
                    return aStatus;
                }

                //end of addition
                //new addtion for license enforcement 
                int tokenCountCurrentDBState = oManagement.getTokenCountForLicenseCheck(systemSessionId, channels.getChannelid(), type);
                int licensecount = 0; // Axiom Protect fuction call here 
                if (type == OTP_TOKEN_OUTOFBAND) {

                    if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_OOB) != 0) {
                        log.debug("assignOtpAuthenticationType::" + "OOB Token is not available in this license!!!");
                        log.debug("assignOtpAuthenticationType::" + "" + -100);
                        aStatus.errorMessage = "OOB Token is not available in this license!!!";
                        aStatus.status = "" + -100;
//                    aStatus.error = "OOB Token is not available in this license!!!";
//                    aStatus.errorcode = -100;
                        return aStatus;

                    }

                    licensecount = AxiomProtect.GetOOBTokensAllowed();

                    if (licensecount == -998) {
                        //unlimited licensing 
                    } else if (tokenCountCurrentDBState >= licensecount) {
                        log.debug("assignOtpAuthenticationType::" + "OOB Token already reached its limit as per this license!!!");
                        log.debug("assignOtpAuthenticationType::" + "" + -100);
                        aStatus.errorMessage = "OOB Token already reached its limit as per this license!!!";
                        aStatus.status = "" + -100;
                        //aStatus.error = "OOB Token already reached its limit as per this license!!!";
                        //aStatus.errorcode = -100;
                        return aStatus;
                    }

                } else if (type == OTP_TOKEN_SOFTWARE) {
                    if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
                        log.debug("assignOtpAuthenticationType::" + "Software Token is not available in this license!!!");
                        log.debug("assignOtpAuthenticationType::" + "" + -100);
                        aStatus.errorMessage = "Software Token is not available in this license!!!";
                        aStatus.status = "" + -100;
                        return aStatus;
                    }
                    licensecount = AxiomProtect.GetSoftwareTokensAllowed();
                    if (licensecount == -998) {
                        //unlimited licensing 
                    } else if (tokenCountCurrentDBState >= licensecount) {
                        log.debug("assignOtpAuthenticationType::" + "SOFTWARE Token already reached its limit as per this license!!!");
                        log.debug("assignOtpAuthenticationType::" + "" + -100);
                        aStatus.errorMessage = "SOFTWARE Token already reached its limit as per this license!!!";
                        aStatus.status = "" + -100;
                        return aStatus;
                    }
                } else if (type == OTP_TOKEN_HARDWARE) {
                    if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_HARDWARE) != 0) {
                        log.debug("assignOtpAuthenticationType::" + "Hardware Token is not available in this license!!!");
                        log.debug("assignOtpAuthenticationType::" + "" + -100);
                        aStatus.errorMessage = "Hardware Token is not available in this license!!!";
                        aStatus.status = "" + -100;
                        return aStatus;
                    }
                    licensecount = AxiomProtect.GetHardwareTokensAllowed();
                    if (licensecount == -998) {
                        //unlimited licensing 
                    } else if (tokenCountCurrentDBState >= licensecount) {
                        log.debug("assignOtpAuthenticationType::" + "Hardware Token already reached its limit as per this license!!!");
                        log.debug("assignOtpAuthenticationType::" + "" + -100);
                        aStatus.errorMessage = "Hardware Token already reached its limit as per this license!!!";
                        aStatus.status = "" + -100;
                        return aStatus;
                    }
                }
                //end of new addition

                retValue = oManagement.AssignToken(systemSessionId, session.getChannelid(), userid, type, subtype, otptoken);
                //added for mobile token registration code sending
                try {
                    if (retValue == 0 && type == OTPTokenManagement.SOFTWARE_TOKEN && subtype == OTPTokenManagement.SW_MOBILE_TOKEN) {
                        //if (tSettings.isbSilentCall() == false) {
                        String regCode = oManagement.generateRegistrationCode(systemSessionId, session.getChannelid(), userid, OTP_TOKEN_SOFTWARE);
                        SimpleDateFormat sdfExpiry = new SimpleDateFormat("hh:mm");
                        Calendar cal = Calendar.getInstance();
                        cal.add(Calendar.MINUTE, token.getRegistrationValidity());
                        Date expiry = cal.getTime();
                        if (regCode != null) {
                            UserManagement userObj = new UserManagement();
                            AuthUser user = null;
                            user = userObj.getUser(systemSessionId, channels.getChannelid(), userid);

                            Templates temp = null;
                            TemplateManagement tObj = new TemplateManagement();
                            temp = tObj.LoadbyName(systemSessionId, channels.getChannelid(), TemplateNames.MOBILE_SOFTWARE_TOKEN_REGISTER_TEMPLATE);
                            ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            Date d = new Date();
                            String tmessage = (String) UtilityFunctions.deserializeFromObject(bais);
                            tmessage = tmessage.replaceAll("#name#", user.getUserName());
                            tmessage = tmessage.replaceAll("#channel#", channels.getName());
                            tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                            if (subtype == OTPTokenManagement.SW_MOBILE_TOKEN) {
                                tmessage = tmessage.replaceAll("#tokentype#", "MOBILE");
                                String strSWOTPType = LoadSettings.g_sSettings.getProperty("sw.otp.type");
                                if (strSWOTPType != null && strSWOTPType.compareToIgnoreCase("simple") == 0) { // we are forcing it to be time based for mobile token.
                                    regCode = "1" + regCode;
                                    System.out.println("user name = " + user.getUserName() + " with reg code as " + regCode);
                                }
                            }
                            tmessage = tmessage.replaceAll("#regcode#", regCode);
                            tmessage = tmessage.replaceAll("#expiry#", sdfExpiry.format(expiry));
                            if (subtype == OTPTokenManagement.SW_WEB_TOKEN) {
                                tmessage = tmessage.replaceAll("#tokentype#", "WEB");

                            }
                            if (subtype == OTPTokenManagement.SW_MOBILE_TOKEN) {
                                tmessage = tmessage.replaceAll("#tokentype#", "MOBILE");

                            }
                            if (subtype == OTPTokenManagement.SW_PC_TOKEN) {
                                tmessage = tmessage.replaceAll("#tokentype#", "PC");

                            }

                            SendNotification send = new SendNotification();
                            //  AxiomStatus status = null;
                            AXIOMStatus status = null;
                            if (temp.getStatus() == tObj.ACTIVE_STATUS) {
                                status = send.SendOnMobileNoWaiting(channels.getChannelid(), user.getPhoneNo(), tmessage, 1, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }
                            if (status != null) {
                                if (status.iStatus == SEND_MESSAGE_PENDING_STATE) {
                                    RegistrationCodeTrailManagement rManagement = new RegistrationCodeTrailManagement();
                                    String registrationcode = UtilityFunctions.Bas64SHA1(regCode);
                                    rManagement.addRegCodeTrail(channelid, userid, registrationcode, RegistrationCodeTrailManagement.SENT, RegistrationCodeTrailManagement.OTPTOKEN);
                                }
                            }
                            retValue = 0;
                            log.debug("assignOtpAuthenticationType::" + "SUCCESS");
                            log.debug("assignOtpAuthenticationType::" + "" + 0);
                            aStatus.errorMessage = "SUCCESS";
                            aStatus.status = "" + 0;

                            audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getChannelid(),
                                    req.getRemoteAddr(), channels.getName(),
                                    session.getChannelid(), session.getChannelid(), new Date(), "Generate & Send Registration Code",
                                    "success", 0, "Token Management", "", "Registration Code = ******" /*strRegCode*/, "OTPTOKENS", userid);

                            //add trail code here
                        } else if (regCode == null) {
                            log.debug("assignOtpAuthenticationType::" + "Failed To generate Registration Code");
                            log.debug("assignOtpAuthenticationType::" + "" + -8);
                            aStatus.errorMessage = "Failed To generate Registration Code";
                            aStatus.status = "" + -8;

                            audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                                    req.getRemoteAddr(), channels.getName(),
                                    session.getChannelid(), session.getChannelid(), new Date(), "Generate & Send Registration Code",
                                    "failed", -1, "Token Management", "", "Failed To generate Registration Code", "OTPTOKENS", userid);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //end of addition
                if (retValue == 0) {
                    //removed for acleda
                    retValue = oManagement.ChangeStatus(systemSessionId, session.getChannelid(), userid, OTPTokenManagement.TOKEN_STATUS_ACTIVE, type, subtype);
                    if (retValue != 0) {
                        retValue = -15;
                        log.debug("assignOtpAuthenticationType::" + "Failed in Activation!!!");
                        log.debug("assignOtpAuthenticationType::" + "" + retValue);
                        aStatus.errorMessage = "Failed in Activation!!!";
                        aStatus.status = "" + retValue;
                    }
                    //end of addition

                }
                if (retValue == 0) {
                    log.debug("assignOtpAuthenticationType::" + "SUCCESS");
                    log.debug("assignOtpAuthenticationType::" + "" + 0);
                    aStatus.errorMessage = "SUCCESS";
                    aStatus.status = "" + 0;
                } else if (retValue == -4) {
//                        aStatus.errorMessage = "Token already assigned!!!";
//                        aStatus.status = "" + -4;
                    if (type == OTP_TOKEN_SOFTWARE) {
                        log.debug("assignOtpAuthenticationType::" + "Success");
                        log.debug("assignOtpAuthenticationType::" + "" + 0);
                        aStatus.errorMessage = "Success";
                        aStatus.status = "" + 0;
                    } else {
                        log.debug("assignOtpAuthenticationType::" + "Token already assigned!!!");
                        log.debug("assignOtpAuthenticationType::" + "" + -4);
                        aStatus.errorMessage = "Token already assigned!!!";
                        aStatus.status = "" + -4;
                    }

                } else if (retValue == -3) {
                    log.debug("assignOtpAuthenticationType::" + "Empty Serial Number!!!");
                    log.debug("assignOtpAuthenticationType::" + "" + -3);
                    aStatus.errorMessage = "Empty Serial Number!!!";
                    aStatus.status = "" + -3;
                } else if (retValue == -2) {
                    log.debug("assignOtpAuthenticationType::" + "Invalid Serial Number!!!");
                    log.debug("assignOtpAuthenticationType::" + "" + -2);
                    aStatus.errorMessage = "Invalid Serial Number!!!";
                    aStatus.status = "" + -2;
                } else if (retValue == -6) {
                    log.debug("assignOtpAuthenticationType::" + "Token could not be assigned!!!");
                    log.debug("assignOtpAuthenticationType::" + "" + -6);
                    aStatus.errorMessage = "Token could not be assigned!!!";
                    aStatus.status = "" + -6;
                } else if (retValue == -20) {
                    log.debug("assignOtpAuthenticationType::" + "Failed to issue registration code!!!");
                    log.debug("assignOtpAuthenticationType::" + "" + -7);
                    aStatus.errorMessage = "Failed to issue registration code!!!";
                    aStatus.status = "" + -7;
                } else if (retValue == 57) {
                    log.debug("assignOtpAuthenticationType::" + "SUCCESS");
                    log.debug("assignOtpAuthenticationType::" + "" + 0);
                    aStatus.errorMessage = "SUCCESS";
                    aStatus.status = "" + 0;
                } else if (retValue == -57) {
                    log.debug("assignOtpAuthenticationType::" + "Failed To assign Token On Bridge");
                    log.debug("assignOtpAuthenticationType::" + "" + -57);
                    aStatus.errorMessage = "Failed To assign Token On Bridge";
                    aStatus.status = "" + -57;
                }

            }

            audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                    channels.getName(),
                    session.getLoginid(), session.getLoginid(),
                    new Date(),
                    "Assign OTP Token", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                    "Token Management",
                    "",
                    "Category=" + strCategory + ",Subcategory=" + strSubCategory,
                    "OTPTOKENS", userid);

            return aStatus;
        } catch (Exception ex) {
            ex.printStackTrace();
            strException = ex.getMessage();
        }

        Response aStatus1 = new Response();
        log.debug("assignOtpAuthenticationType::" + strException);
        log.debug("assignOtpAuthenticationType::" + "" + -99);
        aStatus1.errorMessage = strException;
        aStatus1.status = "" + -99;
        return aStatus1;
    }

    @Override
    public AxiomStatus ResyncToken(String sessionid, String userid, int catgory, String otp1, String otp2) {

        try {

            log.info("Entered ResyncToken()");

            String strDebug = null;

            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    log.info("CloseSession::sessionid::" + sessionid);
                     log.info("CloseSession::userid::" + userid);
                      log.info("CloseSession::catgory::" + catgory);
                      log.info("CloseSession::otp1::" + otp1);
                       log.info("CloseSession::otp2::" + otp2);
                }
            } catch (Exception ex) {
       log.error(ex.getMessage());
            }

            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                //     ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//                aStatus.error = "Licence is invalid";
                log.debug("ResyncToken::" + "Licence is invalid");
                log.debug("ResyncToken::" + -100);
                aStatus.errorcode = -100;
                //   Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = "Licence is invalid";
                return aStatus;
            }

            SessionManagement sManagement = new SessionManagement();
            Sessions session = sManagement.getSessionById(sessionid);
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
            //log.debug("ResyncToken::" + "ERROR");
            //log.debug("ResyncToken::" + -2);
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();

            if (session == null) {
                //AxiomStatus aStatus = new AxiomStatus();
                log.debug("ResyncToken::" + "session is invalid");
                log.debug("ResyncToken::" + -114);
                aStatus.error = "session is invalid";
                aStatus.errorcode = -114;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }

            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(channelid);
            if (channel == null) {
//                aStatus.error = "channel is null";

                log.debug("ResyncToken::" + "" + -102);
                aStatus.errorcode = -102;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }

            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj == null) {
                log.debug("ResyncToken::" + "Channel Global Setting is null");
                log.debug("ResyncToken::" + "" + -80);
                aStatus.error = "Channel Global Setting is null";

                aStatus.errorcode = -80;
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();

                return aStatus;
            }

//            AxiomStatus as = CheckIPAndSendAlert(channel, req.getRemoteAddr());
//            if (as.errorcode != 0) {
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
//                as.error = errmsg.getUsermessage();
//                throw new AxiomException(as.error);
//            }
            OTPTokenManagement otptkmgmt = new OTPTokenManagement(channelid);
            int result = otptkmgmt.resyncOTP(sessionid, channelid, userid, catgory, otp1, otp2);
            if (result == 0) {
                log.debug("ResyncToken::" + "Token has been Synchronized successfully");
                log.debug("ResyncToken::" + "" + 0);
                aStatus.errorcode = 0;
                aStatus.error = "Token has been Synchronized successfully";
                try {
                    AuditManagement audit = new AuditManagement();
                    String cip = req.getRemoteAddr();

                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                            cip, channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(),
                            "RESYNC", "SUCCESS", aStatus.errorcode,
                            "Token Management",
                            "",
                            aStatus.error + " for userid=" + userid + " of token type=" + catgory,
                            "OTPTOKENS", userid);
                    audit = null;
                } catch (Exception e) {
                }
                return aStatus;
            } else {
                log.debug("ResyncToken::" + "Token synchronization failed");
                log.debug("ResyncToken::" + "" + -201);
                aStatus.errorcode = -201;
                aStatus.error = "Token synchronization failed";
//                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//                aStatus.error = errmsg.getUsermessage();
                try {
                    AuditManagement audit = new AuditManagement();
                    String cip = req.getRemoteAddr();
                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                            cip, channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(),
                            "RESYNC", "ERROR", aStatus.errorcode,
                            "Token Management",
                            "",
                            aStatus.error + " for userid=" + userid + " of token type=" + catgory,
                            "OTPTOKENS", userid);
                    audit = null;

                } catch (Exception e) {
                }

                return aStatus;

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        System.err.println("Exited ResyncToken()");

        return null;
    }

}
