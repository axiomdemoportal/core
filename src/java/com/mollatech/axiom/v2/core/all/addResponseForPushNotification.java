package com.mollatech.axiom.v2.core.all;

import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PushbasedauthenticationManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.json.JSONObject;

public class addResponseForPushNotification extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            String _response = request.getParameter("_response");
            String _userid = request.getParameter("_userid");
            String _nid = request.getParameter("_nid");
            

            String result = "success";
            String message = "Response Successfully Added !!";

            JSONObject json = new JSONObject();
            String serialno = null;
            if (_response == null
                    && _response.isEmpty() == true
                    && _userid == null
                    && _nid == null
                    && _userid.isEmpty() == true
                    && _nid.isEmpty() == true
                    ) {
                result = "error";
                message = "Data is invalid!!!";

                try {
                    json.put("_result", result);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                out.print(json);
                out.flush();
                return;

            }

            /* TODO output your page here. You may use following sample code. */
            SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
            Session sChannel = suChannel.openSession();
            ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);

            String _channelName = this.getServletContext().getContextPath();
            //System.out.println("Channel Core Name >>" + _channelName);
            //_channelName = "/ibank2core";
            _channelName = _channelName.replaceAll("/", "");
            if (_channelName.compareTo("core") != 0) {
                _channelName = _channelName.replaceAll("core", "");
            } else {
                _channelName = "face";
            }

            Channels channel = cUtil.getChannel(_channelName);
            if (channel == null) {
                result = "error";
                message = "Channel Details could not be found>>" + _channelName;

                try {
                    json.put("_result", result);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                out.print(json);
                out.flush();
                return;
            }

            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                result = "error";
                message = "Licence Not Valid";

                try {
                    json.put("_result", result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                out.print(json);
                out.flush();
                return;
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channels = cManagement.getChannelByID(channel.getChannelid());

            SettingsManagement setManagement = new SettingsManagement();
//                int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
//                if (result != 1) {
//                    aStatus.errorMessage = "INVALID IP REQUEST";
//                    aStatus.status = "" + -8;
//                    aStatus.session = systemSessionId;
//                    //  return aStatus;
//                }

            String channelid = channel.getChannelid();

            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (request.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, request.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        OperatorsManagement oManagement = new OperatorsManagement();
                        Operators[] aOperator = oManagement.getAdminOperator(channels.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channels.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", request.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channels.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            result = "error";
                            message = "INVALID IP REQUEST";

                            try {
                                json.put("_result", result);
                                json.put("_message", message);
                                json.put("_serialno", serialno);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            out.print(json);
                            out.flush();
                            return;

                        }

                        suTemplate.close();
                        sTemplate.close();
                        result = "error";
                        message = "INVALID IP REQUEST";

                        try {
                            json.put("_result", result);
                            json.put("_message", message);
                            json.put("_serialno", serialno);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        out.print(json);
                        out.flush();
                        return;
                    }
                    result = "error";
                    message = "INVALID IP REQUEST";

                    try {
                        json.put("_result", result);
                        json.put("_message", message);
                        json.put("_serialno", serialno);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    out.print(json);
                    out.flush();
                    return;
                }
            }
            
            int res = -1;
            PushbasedauthenticationManagement pnauth = new PushbasedauthenticationManagement();
            if ( _response != null ) { 
                int iResponse = (new Integer(_response)).intValue();
                res = pnauth.UpdateResponse( channelid, _userid, _nid,iResponse);
            }


            if (res == 0) {

                result = "success";
                message = "Successfully Updated!!!";

                try {
                    json.put("_result", result);
                    json.put("_message", message);
                    json.put("_serialno", serialno);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                out.print(json);
                out.flush();
                return;

            } else {
                result = "error";
                message = "Error in updating your resposne" + res + ")!!!";

                try {
                    json.put("_result", result);
                    json.put("_message", message);
                    json.put("_serialno", serialno);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                out.print(json);
                out.flush();
                return;
            }

        } catch (java.lang.Exception e) {
            e.printStackTrace();

        }

    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
