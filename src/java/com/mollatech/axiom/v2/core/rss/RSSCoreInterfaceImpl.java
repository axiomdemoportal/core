package com.mollatech.axiom.v2.core.rss;

import static com.google.common.io.BaseEncoding.base32;
import com.lowagie.text.pdf.PdfReader;
import com.mollatech.axiom.bridge.web.service.AxiomPDFSignerWrapper;
import com.mollatech.axiom.common.utils.InitTransactionPackage;
import com.mollatech.axiom.common.utils.Phrase;
import com.mollatech.axiom.common.utils.ResourceDetails;
import com.mollatech.axiom.common.utils.SSOData;
import com.mollatech.axiom.common.utils.SSOResponse;
import com.mollatech.axiom.common.utils.TransactionStatus;
import com.mollatech.axiom.common.utils.UtilityFunctions;
import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.communication.RemoteSigningInfo;
import com.mollatech.axiom.connector.mobiletrust.Location;
import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.connector.user.AxiomChallengeResponse;
import com.mollatech.axiom.connector.user.TokenStatusDetails;
import com.mollatech.axiom.mobiletrust.crypto.CryptoManager;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.crypto.ChannelProfile;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import static com.mollatech.axiom.nucleus.crypto.LoadSettings.g_sSettings;
import com.mollatech.axiom.nucleus.db.ApEasylogin;
import com.mollatech.axiom.nucleus.db.ApEasyloginsession;
import com.mollatech.axiom.nucleus.db.Certificates;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Errormessages;
import com.mollatech.axiom.nucleus.db.Operators;
import com.mollatech.axiom.nucleus.db.Otptokens;
import com.mollatech.axiom.nucleus.db.Pkitokens;
import com.mollatech.axiom.nucleus.db.Questionsandanswers;
import com.mollatech.axiom.nucleus.db.Registerdevicepush;
import com.mollatech.axiom.nucleus.db.Remotesignature;
import com.mollatech.axiom.nucleus.db.Securephrase;
import com.mollatech.axiom.nucleus.db.Sessions;
import com.mollatech.axiom.nucleus.db.Srtracking;
import com.mollatech.axiom.nucleus.db.Ssodetails;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.Trusteddevice;
import com.mollatech.axiom.nucleus.db.Twowayauth;
import com.mollatech.axiom.nucleus.db.Txdetails;
import com.mollatech.axiom.nucleus.db.Webresource;
import com.mollatech.axiom.nucleus.db.connector.ChannelsUtils;
import com.mollatech.axiom.nucleus.db.connector.GeoTrackingUtils;
import com.mollatech.axiom.nucleus.db.connector.RemoteSignatureUtils;
import com.mollatech.axiom.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.axiom.nucleus.db.connector.SettingsUtil;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.CertificateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ChallengeResponsemanagement;
import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
import com.mollatech.axiom.nucleus.db.connector.management.CryptoManagement;
import com.mollatech.axiom.nucleus.db.connector.management.EasyLoginManagement;
import com.mollatech.axiom.nucleus.db.connector.management.EasyLoginSessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ErrorMessageManagement;
import com.mollatech.axiom.nucleus.db.connector.management.GeoLocationManagement;
import com.mollatech.axiom.nucleus.db.connector.management.MobileTrustManagment;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PDFSigningManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PKITokenManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PasswordTrailManagement;
import com.mollatech.axiom.nucleus.db.connector.management.PushNotificationDeviceManagement;
import com.mollatech.axiom.nucleus.db.connector.management.QuestionsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.RegistrationCodeTrailManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ResourceManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SSOManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SecurePhraseManagment;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SignReqTrackingManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TrustDeviceManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TwowayauthManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TxManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.db.operation.AxiomQuestionsAndAnswers;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.GlobalChannelSettings;
import com.mollatech.axiom.nucleus.settings.ImageSettings;
import com.mollatech.axiom.nucleus.settings.MobileTrustSettings;
import com.mollatech.axiom.nucleus.settings.PINDeliverySetting;
import com.mollatech.axiom.nucleus.settings.PasswordPolicySetting;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import com.mollatech.axiom.nucleus.settings.TokenSettings;
import com.mollatech.axiom.v2.core.utils.AuthenticationPackage;
import com.mollatech.axiom.v2.core.utils.AxiomCredentialDetails;
import com.mollatech.axiom.v2.core.utils.AxiomData;
import com.mollatech.axiom.v2.core.utils.AxiomException;
import com.mollatech.axiom.v2.core.utils.AxiomMessage;
import com.mollatech.axiom.v2.core.utils.AxiomStatus;
import com.mollatech.axiom.v2.core.utils.AxiomTokenData;
import com.mollatech.axiom.v2.core.utils.EasyCheckIn;
import com.mollatech.axiom.v2.core.utils.EasyCheckInSessions;
import com.mollatech.axiom.v2.core.utils.RSSUserCerdentials;
import com.mollatech.axiom.v2.core.utils.RSSUserDetails;
import com.mollatech.axiom.v2.core.utils.RSSUtils;
import com.mollatech.axiom.v2.core.utils.TwoWayAuthStatus;
import com.mollatech.axiom.v2.core.utils.VerifyRequest;
import com.mollatech.dictum.management.BulkMSGManagement;
import com.mollatech.ecopin.management.EPINManagement;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import org.bouncycastle.openssl.PEMWriter;
import org.bouncycastle.util.encoders.Base64;
import org.hibernate.Session;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import qrcodegenerator.QrcodeGenerator;

@WebService(endpointInterface = "com.mollatech.axiom.v2.core.rss.RSSCoreInterface")
public class RSSCoreInterfaceImpl implements RSSCoreInterface {

    @Resource
    WebServiceContext wsContext;
    final String itemtype = "MOBILETRUST";
    private final static int ANDROID = 1;
    private final static int IOS = 2;
//    private final int GETUSER_BY_USERID = 1;
//    private final int GETUSER_BY_PHONENUMBER = 2;
//    private final int GETUSER_BY_EMAILID = 3;
//    private final int GETUSER_BY_FULLNAME = 4;
//    private final int RESET_USER_PASSWORD = 1;
//    private final int RESET_USER_TOKEN_OOB = 2;
//    private final int RESET_USER_TOKEN_SOFTWARE = 3;
//    private final int RESET_USER_TOKEN_HARDWARE = 4;
//    private final int RESET_USER_CERT = 5;
//    private final int RESET_USER_PKI_TOKEN_SOFTWARE = 6;
//    private final int RESET_USER_PKI_TOKEN_HARDWARE = 7;

    private final int OTP_TOKEN_OUTOFBAND = 3;
    public final int OTP_TOKEN_SOFTWARE = 1;
    private final int OTP_TOKEN_HARDWARE = 2;
    private final int OTP_TOKEN_OUTOFBAND_SMS = 1;
    private final int OTP_TOKEN_OUTOFBAND_VOICE = 2;
    private final int OTP_TOKEN_OUTOFBAND_USSD = 3;
    private final int OTP_TOKEN_OUTOFBAND_EMAIL = 4;
    private final int OTP_TOKEN_SOFTWARE_MOBILE = 2;
    private final int OTP_TOKEN_SOFTWARE_PC = 3;
    private final int OTP_TOKEN_SOFTWARE_WEB = 1;
    private final int OTP_TOKEN_HARDWARE_MINI = 1;
    private final int OTP_TOKEN_HARDWARE_CR = 2;
//    private int MESSAGE_AS_SMS = 1;
//    private int MESSAGE_AS_VOICE = 2;
//    private int MESSAGE_AS_USSD = 3;
//    private int MESSAGE_AS_MAIL = 4;
    private final int TOKEN_STATUS_UNASSIGNED = -10;
    private final int TOKEN_STATUS_ACTIVE = 1;
    private final int TOKEN_STATUS_SUSPENDED = -2;
    //private int TOKEN_STATUS_PENDING = 0;
    //public int MESSAGES_BLOCKED= -5;
    private final String strACTIVE = "ACTIVE";
    private final String strSUSPENDED = "SUSPENDED";
    //private String strPENDING = "PENDING";
    //public String strBLOCKED = "BLOCKED";    
    private final String strUNASSIGNED = "UNASSIGNED";
    //private String USER_ASSIGN_PASSWORD_TEMPLATE_NAME = "User Password";
    private final int SEND_MESSAGE_PENDING_STATE = 2;
    private final String SEND_MESSAGE_PENDING_STATE_STRING = "PENDING";
    private final int SEND_MESSAGE_BLOCKED_STATE = -5;
    private final String SEND_MESSAGE_BLOCKED_STATE_STRING = "BLOCKED";
    private final int SEND_MESSAGE_INVALID_DATA = -4;
    private final String SEND_MESSAGE_INVALID_DATA_STRING = "INVALIDDATA";
//    private final int INVALID_IP = -3;
//    private final int INVALID_REMOTEUSER = -4;
//    private final int INVALID_LICENSE = -5;
//    private final int EPINSMS = 1;
//    private final int EPINIVR = 2;

    public static final int OTP_TOKEN = 1;
    public static final int PKI_TOKEN = 2;
    public static final int IMG_AUTH = 3;

    public static final int USER_PASSWORD = 1;
    public static final int OTP_TOKEN_REGCODE = 2;
    public static final int OTP_TOKEN_OTP = 3;
    public static final int OTP_TOKEN_SOTP = 4;
    public static final int PKI_TOKEN_REGCODE = 5;
    public static final int PFX_PASSWORD = 6;
    public static final int PFX_FILE = 7;
    public static final int CERT_FILE = 8;
    public static final int IMG_AUTH_REGCODE = 9;
    public static final int EMAIL_PDF_SIGNING_TEMPLATE = 10;
    public static final int EPIN = 18;

    public static final int PUSH = 1;
    public static final int SMS = 2;
    public static final int VOICE = 3;
    public static final int MISSEDCALL = 4;
    public static final int PENDING = 2;
    public static final int SEND = 0;
    public static final int RESET = 1;

//     easyloginsession status
    public static final int ELS_EXPIRED_STATUS = -2;
    public static final int ELS_REJECTED_STATUS = -1;
    public static final int ELS_NA_STATUS = 0;
    public static final int ELS_APPROVED_STATUS = 1;

    //  easylogin status
    public static final int EL_EXPIRED_STATUS = -1;
    public static final int EL_PENDING_STATUS = 0;
    public static final int EL_ACTIVE_STATUS = 1;

    static Logger log = Logger.getLogger(RSSCoreInterfaceImpl.class.getName());

    public static final int CHALLAEGE_RESPONSE = 4;

    @Override
    public AxiomStatus CreateRSSUser(String sessionid, RSSUserDetails rssUser, String integrityCheckString, String trustPayLoad) {
        String strDebug = null;
        try {

            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            log.info("##strDebug: Started" + strDebug);
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.info("##CreateUser::sessionId::" + sessionid);
                log.info("##CreateUser::fullname::" + rssUser.userName);
                log.info("##CreateUser::phone::" + rssUser.phoneNumber);
                log.info("##CreateUser::email::" + rssUser.emailid);
                log.info("##CreateUser::userid::" + rssUser.userId);
                log.info("##CreateUser::Loaction::" + rssUser.location);
                log.info("##CreateUser::Organisation::" + rssUser.organisation);
                log.info("##CreateUser::OrganisationUnit::" + rssUser.organisationUnit);
                log.info("##CreateUser::Street::" + rssUser.street);
                log.info("##CreateUser::UserIdentity::" + rssUser.userIdentity);
            }
        } catch (Exception ex) {
        }
//nilesh
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
//            log.info("##AxiomProtect.ValidateLicense()" , "Invalid Licensce");
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "Licence is invalid";
//            aStatus.errorcode = -100;
//            return aStatus;
        }

        try {

            SessionManagement sManagement = new SessionManagement();
            AxiomStatus aStatus = new AxiomStatus();
            byte[] SHA1hash = null;
            if (trustPayLoad == null) {
                SHA1hash = UtilityFunctions.SHA1(sessionid + rssUser.emailid + rssUser.userName + rssUser.phoneNumber);
            } else {
                SHA1hash = UtilityFunctions.SHA1(sessionid + rssUser.emailid + rssUser.userName + rssUser.phoneNumber + trustPayLoad);
            }

//            String integritycheck = new String(Base64.encode(SHA1hash));
            String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//            String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java
            if (integrityCheckString != null) {
                if (integrityCheckString.equals(integritycheck) != true) {
                    aStatus.error = "Input Data is Tampered!!!";
                    aStatus.errorcode = -76;
                    return aStatus;
                }
            }
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionid);

            if (session == null) {
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                return aStatus;
            }
//nilesh
//            if (sessionid == null || rssUser.userName == null || rssUser.phoneNumber == null
//                    || sessionid.isEmpty() == true || rssUser.userName.isEmpty() == true || rssUser.phoneNumber.isEmpty() == true
//                    || rssUser.emailid == null || rssUser.emailid.isEmpty() == true
//                    || rssUser.location == null || rssUser.location.isEmpty()
//                    || rssUser.organisation == null || rssUser.organisation.isEmpty()
//                    || rssUser.street == null || rssUser.organisation.isEmpty()
//                    || rssUser.userIdentity == null || rssUser.organisation.isEmpty()
//                    || rssUser.organisationUnit == null || rssUser.organisation.isEmpty()) {
//                //AxiomStatus aStatus = new AxiomStatus();
//                aStatus.error = "Invalid Data";
//                aStatus.errorcode = -11;
//                return aStatus;
//            }
            //nilesh

            if (sessionid == null || rssUser.userName == null || rssUser.userId == null) {
                //AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Data";
                aStatus.errorcode = -11;
                return aStatus;
            }

            if (rssUser.userId != null && rssUser.userId.isEmpty() == true) {
                //AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Setting userid is not supported!!!";
                aStatus.errorcode = -12;
                return aStatus;
            }

//            int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                AxiomStatus aStatus = new AxiomStatus();
//                aStatus.error = "Invalid License";
//                aStatus.errorcode = -10;
//                return aStatus;
//            }
            UserManagement uManagement = new UserManagement();
            //SessionManagement sManagement = new SessionManagement();
            //MessageContext mc = wsContext.getMessageContext();
            //HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            //Sessions session = sManagement.getSessionById(sessionId);
            //AxiomStatus aStatus = new AxiomStatus();
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;

            if (session != null) {

                //System.out.println("Client IP = " + req.getRemoteAddr());        
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    return null;
                }
                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {

                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
                        return aStatus;
                    }
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }

                //addition for license check enforcement
                if (AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_PASSWORD) != 0) {
                    aStatus.error = "This feature is not available in this license!!!";
                    aStatus.errorcode = -100;
                    return aStatus;
                }

                int iUserCount = uManagement.getCountOfLicenseUser(channel.getChannelid());
                int licensecount = AxiomProtect.GetUsersAllowed(); //AxiomProtect Function here now default 0. 
                if (licensecount == -998) {
                    //unlimited licensing 
                } else if (iUserCount >= licensecount) {
                    aStatus.error = "User Addition already reached its limit as per this license!!!";
                    aStatus.errorcode = -100;
                    return aStatus;
                }
                //end of addition
                String userFlag = LoadSettings.g_sSettings.getProperty("check.user");
//                retValue = uManagement.CreateUser(sessionid, session.getChannelid(), rssUser.userName, rssUser.phoneNumber, rssUser.emailid, rssUser.userId, rssUser.groupid);
                retValue = uManagement.CreateUser(sessionid, session.getChannelid(), rssUser.userName, rssUser.phoneNumber, rssUser.emailid, rssUser.userId, rssUser.groupid, rssUser.organisation, rssUser.userIdentity, "Passport", rssUser.organisationUnit, rssUser.country, rssUser.location, "Mont Kiara", "Manager");
                if (retValue == 0) {
                    AuthUser authUser = null;
                    authUser = uManagement.getUser(session.getChannelid(), rssUser.userId);
                                        
//                    if (authUser != null) {
//                        TrustManagement tManagement = new TrustManagement();
//                        if (trustPayLoad != null) {
//                            int res = tManagement.addGeoFenceForRSS(sessionid, channelid, authUser.getUserId(), trustPayLoad);
//                        }
//                    }
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                    int bCertificateSuccessful = -1;    
                    String cip = req.getRemoteAddr();
                    // certificate addition for newly created user
                    if (authUser != null) {
                        CertificateManagement cerManagement = new CertificateManagement();
                        String certificate = cerManagement.GenerateCertificateFace(sessionid, session.getChannelid(), authUser.getUserId());
                        if (certificate != null) {
                            retValue = 0;                                                                                    
                            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                    cip, channel.getName(),
                                    session.getLoginid(), session.getLoginid(),
                                    new Date(), "Assign",
                                    "Certificate assign successfully", aStatus.errorcode,
                                    "Certificate Management",
                                    "",
                                    "Certicate assigned successfully to user " + authUser.getUserName(),
                                    "PKITOKEN",
                                    authUser.getUserId());
                            bCertificateSuccessful = 0;
                        }
                    }
                    if (bCertificateSuccessful == -2) {
                        //this means that suer was created but certificate issuance failed 
                        int retValue1 = uManagement.DeleteUser(sessionid, channelid, authUser.getUserId());                        
                        if (retValue1 == 0) {
                            AuditManagement ad = new AuditManagement();
                            ad.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                    cip, channel.getName(),
                                    session.getLoginid(), session.getLoginid(),
                                    new Date(),
                                    "Delete User(roolback)",
                                    "SUCCESS", aStatus.errorcode,
                                    "User Management",
                                    "Name=" + authUser.getUserName() + ",userid=" + authUser.getUserId(),
                                    "Deleted user to rollback as failed certificate issance. userid was " + authUser.getUserId(),
                                    "USERPASSWORD",
                                    authUser.getUserId());
                        } else {
                            AuditManagement ad = new AuditManagement();
                            ad.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                    cip, channel.getName(),
                                    session.getLoginid(), session.getLoginid(),
                                    new Date(),
                                    "Delete User(roolback)",
                                    "ERROR",
                                    retValue1,
                                    "User Management",
                                    "Name=" + authUser.getUserName() + ",userid=" + authUser.getUserId(),
                                    "Deleted user to rollback as failed certificate issuance. userid is " + authUser.getUserId(),
                                    "USERPASSWORD",
                                    authUser.getUserId());
                        }
                        aStatus.error = "ERROR";
                        aStatus.errorcode = -1;
                    }
                } else if (retValue == 1) {
                    //aStatus.error = "SUCCESS";
                    //aStatus.errorcode = 0;
                    aStatus.error = "User created succesfully but email could not be sent!!!";
                    aStatus.errorcode = 0;

                } else if (retValue == -1) {
                    aStatus.error = "Duplicate user entry!!!";
                    aStatus.errorcode = -59;
                } else if(retValue == -242){
                    aStatus.error = "Phone number already used!!!";
                    aStatus.errorcode = -242;
                }else if(retValue == -240){
                    aStatus.error = "Email ID already used!!!";
                    aStatus.errorcode = -240;
                }else if(retValue == -241){
                     aStatus.error = "UserID already used!!!";
                    aStatus.errorcode = -241;
                }
                //return aStatus;
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            //AuditManagement audit = new AuditManagement();
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                    req.getRemoteAddr(), channel.getName(),
                    session.getLoginid(), session.getLoginid(),
                    new Date(), "Create User",
                    aStatus.error, aStatus.errorcode,
                    "User Management", "",
                    "Name=" + rssUser.userName + ",Phone=" + rssUser.phoneNumber + ",Email=" + rssUser.emailid + ",State=Inactive",
                    "USER", "");
            return aStatus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public AxiomStatus ChangeRSSUserDetails(String sessionid, String rssUserID, RSSUserDetails rssUser, String integrityCheckString, String trustPayLoad) {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                System.out.println("ChangeUserDetails::sessionId::" + sessionid);
                System.out.println("ChangeUserDetails::userid::" + rssUser.userId);
                System.out.println("ChangeUserDetails::fullname::" + rssUser.userName);
                System.out.println("ChangeUserDetails::phone::" + rssUser.phoneNumber);
                System.out.println("ChangeUserDetails::email::" + rssUser.location);
                System.out.println("ChangeUserDetails::email::" + rssUser.organisation);
                System.out.println("ChangeUserDetails::email::" + rssUser.organisationUnit);
                System.out.println("ChangeUserDetails::email::" + rssUser.userIdentity);
                System.out.println("ChangeUserDetails::email::" + rssUser.country);
                System.out.println("ChangeUserDetails::email::" + rssUser.street);

            }
        } catch (Exception ex) {
        }

        AxiomStatus aStatus = new AxiomStatus();
//nilesh
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {

            aStatus.error = "Licence is invalid";
            aStatus.errorcode = -100;
            return aStatus;
//                AxiomData aData = new AxiomData();
//                aData.sErrorMessage = "Licence is invalid";
//                aData.iErrorCode = -100;
//                return aData;                
            //throw new AxiomException("Licence is invalid");
        }

        byte[] SHA1hash = null;
        if (trustPayLoad == null) {
            SHA1hash = UtilityFunctions.SHA1(sessionid + rssUserID + rssUser.emailid + rssUser.userName + rssUser.phoneNumber);
        } else {
            SHA1hash = UtilityFunctions.SHA1(sessionid + rssUserID + rssUser.emailid + rssUser.userName + rssUser.phoneNumber + trustPayLoad);
        }
//        String integritycheck = new String(Base64.encode(SHA1hash));
        String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//        String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java
        if (integrityCheckString != null) {
            if (integrityCheckString.equals(integritycheck) != true) {
                aStatus.error = "Input Data is Tampered!!!";
                aStatus.errorcode = -76;
                return aStatus;
            }
        }
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);
        if (session == null) {

            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
            return aStatus;
            //throw  new AxiomException("");
        }

        try {
//            int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                AxiomStatus aStatus = new AxiomStatus();
//                aStatus.error = "Licence Not Valid";
//                aStatus.errorcode = -10;
//                return aStatus;
//            }
            UserManagement uManagement = new UserManagement();
            //SessionManagement sManagement = new SessionManagement();
            //MessageContext mc = wsContext.getMessageContext();
//            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//            Sessions session = sManagement.getSessionById(sessionId);

            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            AuthUser oldUser = null;
            if (session != null) {

                //System.out.println("Client IP = " + req.getRemoteAddr());        
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    aStatus.error = "INVALID CHANNEL";
                    aStatus.errorcode = -3;
                    return aStatus;
                }

                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {

                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
                        return aStatus;
                    }
                }

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }
                if (trustPayLoad != null) {
                    MobileTrustSettings mSettings = null;

                    Object obj = setManagement.getSetting(sessionid, channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
                    if (obj != null) {
                        if (obj instanceof MobileTrustSettings) {
                            mSettings = (MobileTrustSettings) obj;
                        }
                    }

                    if (mSettings == null) {
                        aStatus.error = "Mobile Trust Setting is not configured!!!";
                        aStatus.errorcode = -23;
//                  
                        return aStatus;
                    }

                    if (mSettings.bGeoFencing == true) {

//                        TrustManagement tManagement = new TrustManagement();
//                        int res = tManagement.checkLocationAndUpdateWMI(sessionid, channelid, rssUserID, trustPayLoad);
                        GeoLocationManagement gManagement = new GeoLocationManagement();
                        AXIOMStatus status = gManagement.validateGeoLocation(sessionid, channelid, rssUserID, trustPayLoad);
                        if (status == null) {
                            aStatus.error = "Location Not Found!!!";
                            aStatus.errorcode = -115;
                        } else if (status.iStatus != 0) {
//                            if (res == -14) {
//                                aStatus.errorcode = -14;
//                                aStatus.error = "Mobile Trust Setting is not configured!!!";
//                            } else if (res == -4) {
//                                aStatus.errorcode = 4;
//                                aStatus.error = "Verification failed!!!";
//                            } else if (res == -7) {
//                                aStatus.errorcode = -7;
//                                aStatus.error = "Home Country is not defined!!!";
//                            } else if (res == -10) {
//                                aStatus.errorcode = -10;
//                                aStatus.error = "Invalid Location, out of home country!!!";
//                            } else if (res == -11) {
//                                aStatus.errorcode = -11;
//                                aStatus.error = "Invalid Location, roaming country is not allowed!!!";
//                            } else if (res == -12) {
//                                aStatus.errorcode = -12;
//                                aStatus.error = "Invalid Location, roaming is not configured properly!!!";
//                            } else if (res == -13) {
//                                aStatus.errorcode = -13;
//                                aStatus.error = "Roaming Contry is not Configured!!!";
//                            } else if (res == -1) {
//                                aStatus.errorcode = -1;
//                                aStatus.error = "Internal Error!!!";
//                            } else if (res == -2) {
//                                aStatus.errorcode = -2;
//                                aStatus.error = "Session has Expired!!!";
//                            }

                            aStatus.error = status.strStatus;
                            aStatus.errorcode = status.iStatus;
                            //     String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
                            audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                                    session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorcode,
                                    "Token Management", "Invalid Location", null, itemtype,
                                    session.getLoginid());

                            return aStatus;
                        }
                    }
                }

//                retValue = uManagement.EditUser(sessionid, session.getChannelid(), rssUser.userId, rssUser.userName, rssUser.phoneNumber, rssUser.emailid, rssUser.groupid);
                retValue = uManagement.EditUser(sessionid, session.getChannelid(), rssUser.userId, rssUser.userName, rssUser.phoneNumber, rssUser.emailid, rssUser.groupid, null, null, null, null, null, null, null, null);
                if (retValue == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                } else if (retValue == 1) {
                    aStatus.error = "User Details Changed Succesfully but email could not be sent!!!";
                    aStatus.errorcode = 1;
                } else if (retValue == 9) {
                    aStatus.error = "User Details Changed Succesfully but failed to send alert!!!";
                    aStatus.errorcode = 9;
                }
                //return aStatus;
                oldUser = uManagement.getUser(sessionid, session.getChannelid(), rssUser.userId);
            }

            String oldUserName = "";
            String oldPhoneNo = "";
            String oldEmailID = "";
            String oldPasswordState = "";

            if (oldUser != null) {
                oldUserName = oldUser.getUserName();
                oldPhoneNo = oldUser.getPhoneNo();
                oldEmailID = oldUser.getEmail();
                oldPasswordState = "" + oldUser.getStatePassword();
            }

            String itemtype = "USERPASSWORD";

            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            //AuditManagement audit = new AuditManagement();
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                    session.getLoginid(), session.getLoginid(), new Date(), "Edit User", aStatus.error, aStatus.errorcode,
                    "User Management", "User Name = " + oldUserName + "User Phone =" + oldPhoneNo + "User Email =" + oldEmailID + "State Of Password =" + oldPasswordState,
                    "User Name = " + rssUser.userName + "User Phone =" + rssUser.phoneNumber + "User Email =" + rssUser.emailid + "State Of Password =" + -1,
                    itemtype, "");
            return aStatus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public RSSUserCerdentials GetRSSUser(String sessionid, String keyword, int type, String integrityCheckString, String trustPayLoad) {
        String strDebug = null;
        RSSUserCerdentials RSSUCred = new RSSUserCerdentials();
        TwoWayAuthStatus authStatus = new TwoWayAuthStatus();
        System.out.println("Start::" + new Date());
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");

                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("GetUserBy::sessionId::" + sessionid);
                    System.out.println("GetUserBy::type::" + type);
                    System.out.println("GetUserBy::searchFor::" + keyword);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
//                try {
//                    AxiomStatus aStatus = new AxiomStatus();
//                    RSSUCred.errorMsg = aStatus.error = "Licence is invalid";
//                    RSSUCred.errorCode = aStatus.errorcode = -100;
//
//                    throw new AxiomException("Licence is invalid");
//                } catch (AxiomException ex) {
//                    Logger.getLogger(RSSCoreInterfaceImpl.class.getName()).log(Level.SEVERE, null, ex);
//                }
            }

            AxiomStatus aStatus = new AxiomStatus();
            try {
                byte[] SHA1hash = null;
                if (trustPayLoad == null) {
                    SHA1hash = UtilityFunctions.SHA1(sessionid + keyword + type);
                } else {
                    SHA1hash = UtilityFunctions.SHA1(sessionid + keyword + type + trustPayLoad);
                }
//                String integritycheck = new String(Base64.encode(SHA1hash));
                String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//                String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java
                if (integrityCheckString != null) {
                    if (integrityCheckString.equals(integritycheck) != true) {
                        aStatus.error = "Input Data is Tampered!!!";
                        aStatus.errorcode = -76;
                        RSSUCred.errorMsg = aStatus.error = "Input Data is Tampered!!!";
                        RSSUCred.errorCode = aStatus.errorcode = -76;
                        throw new AxiomException("Input Data is Tampered!!!");
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            String resultStr = "Failure";
            int retValue = -1;

            System.out.println("2::" + new Date());

            UserManagement uManagement = new UserManagement();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();

            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionid);
//            AxiomUser aUser = null;
            if (sessionid == null) {
                throw new Exception("Session ID is empty!!!");
            }            
            AuthenticationPackage authPackage = new AuthenticationPackage();
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                System.out.println("3::" + new Date());
                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {
                        throw new AxiomException("Server Down for maintainance,Please try later!!!");
                    }
                }
                if (channel == null) {
                    throw new AxiomException("Server Down for maintainance,Please try later!!!");
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                        authPackage.IpCheck = SecurePhraseManagment.SUCCESS_AUTH_PACKAGE;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                authPackage.IpCheck = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                                throw new Exception("IP is not allowed!!!");
                            }

                            suTemplate.close();
                            sTemplate.close();
                            authPackage.IpCheck = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                            throw new Exception("IP is not allowed!!!");
                        }
                        authPackage.IpCheck = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                        throw new Exception("IP is not allowed!!!");
                    }
                }

                System.out.println("4::" + new Date());

                String userFlag = LoadSettings.g_sSettings.getProperty("check.user");
                AuthUser authUser = null;
                String USERID;
                RSSUserDetails rssUser = null;
                //            RSSUserCerdentials rssUserCred = null;
                AxiomCredentialDetails[] atDetails = null;
//                TokenStatusDetails[] tDetails = null;//nilesh
                Otptokens[] tDetails = null;
                List<AxiomCredentialDetails> AlDetails = new ArrayList<AxiomCredentialDetails>();

                if (userFlag != null) {
                    if (userFlag.equals("1")) {
                        authUser = uManagement.CheckUserByType(sessionid, session.getChannelid(), keyword, type);

                    } else {
                        authUser = uManagement.CheckUserByType(sessionid, session.getChannelid(), keyword, type);
                    }
                } else {
                    authUser = uManagement.CheckUserByType(sessionid, session.getChannelid(), keyword, type);
                }
                OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                //nilesh esigner 29-10-15
                if (authUser == null) {
                    if (type == 99) {//type == 99 esigner get user by regcode
                        String userId = oManagement.getUserIdByRegistrationCode(channelid, keyword);
                        authUser = uManagement.getUser(sessionid, channelid, userId);
                    }
                }

                //end nilesh esigner 29-10-15
                System.out.println("5::" + new Date());

                if (authUser != null) {
                    //ashis 28-5-15
                    if (trustPayLoad != null) {
                        MobileTrustSettings mSettings = null;

                        Object obj = setManagement.getSetting(sessionid, channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
                        if (obj != null) {
                            if (obj instanceof MobileTrustSettings) {
                                mSettings = (MobileTrustSettings) obj;
                            }
                        }

                        if (mSettings == null) {
                            aStatus.error = "Mobile Trust Setting is not configured!!!";
                            aStatus.errorcode = -23;
                            throw new Exception(aStatus.error);
//                        return aStatus;
                        }

                        if (mSettings.bGeoFencing == true) {
                            GeoLocationManagement gManagement = new GeoLocationManagement();
                            AXIOMStatus status = gManagement.validateGeoLocation(sessionid, channelid, authUser.userId, trustPayLoad);
                            if (status == null) {                                
                                aStatus.error = "Location Not Found!!!";
                                aStatus.errorcode = -115;
                                authPackage.GeoCheck = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                            } else if (status.iStatus != 0) {
                                authPackage.GeoCheck = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                                aStatus.error = status.strStatus;
                                aStatus.errorcode = status.iStatus;
                                //     String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                                        session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorcode,
                                        "Token Management", "Invalid Location", "Invalid Location", itemtype,
                                        session.getLoginid());

                                throw new Exception(aStatus.error);
                            }else if(status.iStatus == 0){
                                authPackage.GeoCheck = SecurePhraseManagment.SUCCESS_AUTH_PACKAGE;
                            }
                        }
                    } //end ashis 28-5-15

                    rssUser = new RSSUserDetails();
                    rssUser.phoneNumber = authUser.phoneNo;
                    rssUser.emailid = authUser.email;
                    rssUser.userName = authUser.userName;
                    rssUser.userId = authUser.userId;
                    rssUser.lastaccessOn = authUser.lLastAccessOn;
                    rssUser.createOn = authUser.lCreatedOn;
                    USERID = authUser.userId;

                    if (USERID != null && USERID != "") {
                        AxiomCredentialDetails PassDetails = new AxiomCredentialDetails();
                        PassDetails.category = AxiomCredentialDetails.PASSWORD;
                        PassDetails.subcategory = 0;
                        PassDetails.Password = authUser.getPassword();
                        PassDetails.attempts = authUser.iAttempts;
                        PassDetails.createOn = authUser.lCreatedOn;
                        PassDetails.lastaccessOn = authUser.lLastAccessOn;
                        PassDetails.status = authUser.statePassword;
                        AlDetails.add(PassDetails);

                    }
//ashish 28-5-15
                    if (USERID != null && USERID != "") {
                        authStatus.TWOWAYPUSH = -1;
                        authStatus.TWOWAYSMS = -1;
                        authStatus.TWOWAYVOICECALLBACK = -1;
                        authStatus.TWOWAYVOICEMISSEDCALL = -1;
                        Twowayauth twowayauth = new TwowayauthManagement().getAuthDetailsByUserId(channelid, USERID, sessionid);
                        if (twowayauth != null && twowayauth.getType()!= null) {
                            JSONObject json1 = new JSONObject(twowayauth.getType());
                            if(json1 != null){
                                String authtype = "";
                                if(json1.has("Push")){
                                    authtype = json1.getString("Push");
                                    if (!authtype.equalsIgnoreCase("Suspended")) {
                                        authStatus.TWOWAYPUSH = 0;
                                    }
                                }
                                if(json1.has("SMS")){
                                    authtype = json1.getString("SMS");
                                    if (!authtype.equalsIgnoreCase("Suspended")) {
                                        authStatus.TWOWAYSMS = 0;
                                    }
                                }
                                if(json1.has("Callback")){
                                    authtype = json1.getString("Callback");
                                    if (!authtype.equalsIgnoreCase("Suspended")) {
                                        authStatus.TWOWAYVOICECALLBACK = 0;
                                    }
                                }
                                if(json1.has("Missed")){
                                    authtype = json1.getString("Missed");
                                    if (!authtype.equalsIgnoreCase("Suspended")) {
                                        authStatus.TWOWAYVOICEMISSEDCALL = 0;
                                    }
                                }
                            }
                        }
                    }
                    //end ashish 28-5-15
                    System.out.println("7::" + new Date());
                    Object settings = setManagement.getSetting(sessionid, channelid, SettingsManagement.Token, SettingsManagement.PREFERENCE_ONE);
                    TokenSettings tokenObj = null;
                    if (settings != null) {
                        tokenObj = (TokenSettings) settings;
                    }

                    if (USERID != null && USERID != "") {
//                        tDetails = oManagement.getTokenList(sessionid, session.getChannelid(), USERID);//nilesh
                        tDetails = oManagement.getTokenListv2(sessionid, session.getChannelid(), USERID);

                        if (tDetails != null) {
                            for (int i = 0; i < tDetails.length; i++) {
                                AxiomCredentialDetails TokenDetail = new AxiomCredentialDetails();
                                int res = -1;
                                if (tDetails[i].getStatus() == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
//                                    res = oManagement.unlockTokenAfter(channelid, USERID, tokenObj, tDetails[i]);//nilesh
                                    res = oManagement.unlockTokenAfterv2(channelid, USERID, tokenObj, tDetails[i]);
                                }

                                if (tDetails[i].getCategory() == OTP_TOKEN_SOFTWARE) {
                                    TokenDetail.category = AxiomCredentialDetails.SOFTWARE_TOKEN;
                                } else if (tDetails[i].getCategory() == OTP_TOKEN_HARDWARE) {
                                    TokenDetail.category = AxiomCredentialDetails.HARDWARE_TOKEN;
                                } else if (tDetails[i].getCategory() == OTP_TOKEN_OUTOFBAND) {
                                    TokenDetail.category = AxiomCredentialDetails.OUTOFBOUND_TOKEN;
                                }

                                TokenDetail.subcategory = tDetails[i].getSubcategory();
                                TokenDetail.Password = null;
                                TokenDetail.attempts = tDetails[i].getAttempts();
                                TokenDetail.createOn = tDetails[i].getCreationdatetime().getTime();
                                TokenDetail.lastaccessOn = tDetails[i].getLastaccessdatetime().getTime();
                                if (res == 0) {
                                    TokenDetail.status = OTPTokenManagement.TOKEN_STATUS_ACTIVE;
                                } else {
                                    TokenDetail.status = tDetails[i].getStatus();
                                }
                                TokenDetail.serialnumber = tDetails[i].getSrno();
                                AlDetails.add(TokenDetail);

                            }
                        }

                    }

                    System.out.println("8::" + new Date());

                    if (USERID != null && USERID != "") {

                        PKITokenManagement pManagement = new PKITokenManagement();
//                        OTPTokenManagement oManagement = new OTPTokenManagement(channelid);
                        TokenStatusDetails[] tDetailPKI = pManagement.getTokenList(sessionid, session.getChannelid(), USERID);

                        if (tDetailPKI != null) {
                            for (int i = 0; i < tDetailPKI.length; i++) {
                                AxiomCredentialDetails TokenDetail = new AxiomCredentialDetails();

                                int res = -1;
                                if (tDetailPKI[i].Status == OTPTokenManagement.TOKEN_STATUS_LOCKEd) {
//                                    res = oManagement.unlockTokenAfter(channelid, USERID, tokenObj, tDetails[i]);//nilesh
                                    res = pManagement.unlockPKIToken(sessionid, channelid, USERID, tokenObj, tDetailPKI[i]);
                                }

                                if (tDetailPKI[i].Catrgory == PKITokenManagement.SOFTWARE_TOKEN) {
                                    TokenDetail.category = AxiomCredentialDetails.PKI_SOFTWARE_TOKEN;
                                } else if (tDetailPKI[i].Catrgory == PKITokenManagement.HARDWARE_TOKEN) {
                                    TokenDetail.category = AxiomCredentialDetails.PKI_HARDWARE_TOKEN;
                                }

                                TokenDetail.subcategory = tDetailPKI[i].SubCategory;
                                TokenDetail.Password = null;
                                TokenDetail.attempts = tDetailPKI[i].Attempts;
                                TokenDetail.createOn = tDetailPKI[i].createOn;
                                TokenDetail.lastaccessOn = tDetailPKI[i].lastaccessOn;
//                                TokenDetail.status = tDetailss[i].Status;
                                if (res == 0) {
                                    TokenDetail.status = PKITokenManagement.TOKEN_STATUS_ACTIVE;
                                } else {
                                    TokenDetail.status = tDetailPKI[i].Status;
                                }
                                TokenDetail.serialnumber = tDetailPKI[i].serialnumber;
                                AlDetails.add(TokenDetail);

                            }
                        }

                    }
                    if (!"".equals(USERID)) {

                        CertificateManagement certManagement = new CertificateManagement();
                        Certificates cert = certManagement.getCertificate(sessionid, session.getChannelid(), USERID);

                        if (cert != null) {

                            AxiomCredentialDetails CertDetails = new AxiomCredentialDetails();
                            CertDetails.category = AxiomCredentialDetails.CERTIFICATE;
                            CertDetails.Password = cert.getPfxpassword();
                            CertDetails.createOn = cert.getCreationdatetime().getTime();
                            CertDetails.serialnumber = cert.getSrno();
                            CertDetails.status = cert.getStatus();
                            CertDetails.base64Cert = cert.getCertificate();
                            CertDetails.securePhrase = cert.getPfx();
                            CertDetails.certificateExpiryDate = cert.getExpirydatetime().getTime();
                            AlDetails.add(CertDetails);

                        }
                    }
                    if (!"".equals(USERID)) {

                        AxiomCredentialDetails ChallengeResponseDetails = new AxiomCredentialDetails();
                        ChallengeResponsemanagement ChalResp = new ChallengeResponsemanagement();
                        AxiomQuestionsAndAnswers qanda = ChalResp.getUserQuestionsandAnswers(sessionid, session.getChannelid(), USERID);

                        if (qanda != null) {
                            ChallengeResponseDetails.qas = new AxiomChallengeResponse();
                            ChallengeResponseDetails.qas.webQAndA = qanda.webQAndA;
                            ChallengeResponseDetails.category = AxiomCredentialDetails.CHALLAEGE_RESPONSE;

                            AlDetails.add(ChallengeResponseDetails);
                        }
                    }
                    System.out.println("9::" + new Date());

                    //nilesh
                    if (!"".equals(USERID)) {
                        SecurePhraseManagment securePhrase = new SecurePhraseManagment();

                        Securephrase sPhrase = securePhrase.GetSecurePhrase(sessionid, channelid, USERID);
//                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.IMAGE_AUTH) != 0) {
//                            sPhrase = null;
//                        }

                        if (sPhrase != null) {
                            AxiomCredentialDetails phraseDetails = new AxiomCredentialDetails();
                            phraseDetails.category = AxiomCredentialDetails.SECURE_PHRASE;
                            phraseDetails.securePhraseImage = sPhrase.getImageentry();
                            phraseDetails.securePhraseID = sPhrase.getSecurephraseid();
                            int _xcoordinate = 0;
                            int _ycoordinate = 0;
                            if (sPhrase.getXcoordinate() != null) {
                                phraseDetails.xCoordinate = sPhrase.getXcoordinate();
                            } else {
                                phraseDetails.xCoordinate = _xcoordinate;
                            }
                            if (sPhrase.getYcoordinate() != null) {
                                phraseDetails.yCoordinate = sPhrase.getYcoordinate();
                            } else {
                                phraseDetails.yCoordinate = _ycoordinate;
                            }

                            AlDetails.add(phraseDetails);

                            securePhrase.AddSecureTrap(sessionid, channelid, USERID,
                                    -1, -1, null, SecurePhraseManagment.IS_REGISTERED);

                        } else {
                            AxiomCredentialDetails phraseDetails = new AxiomCredentialDetails();
                            phraseDetails.category = AxiomCredentialDetails.SECURE_PHRASE;
                            phraseDetails.securePhraseImage = null;
                            phraseDetails.securePhraseID = null;
                            phraseDetails.xCoordinate = -1;
                            phraseDetails.yCoordinate = -1;
                            AlDetails.add(phraseDetails);
//                            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.IMAGE_AUTH) != 0) {
//                                phraseDetails.securePhraseID = "This feature is not available in this license!!!";
//                            } else {
                                securePhrase.AddSecureTrap(sessionid, channelid, USERID,
                                        -1, -1, null, SecurePhraseManagment.IS_NOT_REGISTERED);
                            //}
                        }
                    }//nilesh

                    atDetails = new AxiomCredentialDetails[AlDetails.size()];
                    for (int i = 0; i < AlDetails.size(); i++) {
                        atDetails[i] = AlDetails.get(i);
                    }

                    System.out.println("10::" + new Date());

                    RSSUCred = new RSSUserCerdentials();
                    RSSUCred.rssUser = rssUser;
                    RSSUCred.tokenDetails = atDetails;
                    RSSUCred.twoWayAuthStatus = authStatus;
                    RSSUCred.authPackage= authPackage;
                    if (RSSUCred != null) {
                        retValue = 0;
                        resultStr = "Success";
                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                req.getRemoteAddr(), channel.getName(),
                                session.getLoginid(), session.getLoginid(), new Date(),
                                "GET USER", resultStr, retValue,
                                "User Management",
                                "Name=" + RSSUCred.rssUser.userName + ",Phone=" + RSSUCred.rssUser.phoneNumber + ",Email=" + RSSUCred.rssUser.emailid,
                                "", "USER", RSSUCred.rssUser.userId);
                    } else if (RSSUCred == null) {
                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                req.getRemoteAddr(), channel.getName(),
                                session.getLoginid(), session.getLoginid(), new Date(),
                                "GET USER", resultStr, retValue,
                                "User Management", "", "", "USER", "");
                    }

                    System.out.println("exiting::" + new Date());
                    return RSSUCred;

                }
            }
        } catch (Exception ex) {
//            Logger.getLogger(RSSCoreInterfaceImpl.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public AxiomStatus ResetCredential(String sessionid, RSSUserCerdentials credentials, String integrityCheckString, String trustPayLoad) {

        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                System.out.println("ResetUser::sessionId::" + sessionid);
                System.out.println("ResetUser::userid::" + credentials.rssUser.userId);

            }
        } catch (Exception ex) {
        }
//nilesh
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "Licence is invalid";
//            aStatus.errorcode = -100;
//            return aStatus;
        }

        try {
//            int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                AxiomStatus aStatus = new AxiomStatus();
//                aStatus.error = "Licence Not Valid";
//                aStatus.errorcode = -10;
//                return aStatus;
//            }
            AxiomStatus aStatus = new AxiomStatus();

            byte[] SHA1hash = null;
            if (trustPayLoad == null) {
                SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.rssUser.emailid + credentials.rssUser.userId + credentials.rssUser.phoneNumber + credentials.rssUser.userName);
            } else {
                SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.rssUser.emailid + credentials.rssUser.userId + credentials.rssUser.phoneNumber + credentials.rssUser.userName + trustPayLoad);
            }
//            String integritycheck = new String(Base64.encode(SHA1hash));
            String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//            String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java
            if (integrityCheckString != null) {
                if (integrityCheckString.equals(integritycheck) != true) {
                    aStatus.error = "Input Data is Tampered!!!";
                    aStatus.errorcode = -76;
                    return aStatus;

                }
            }
            SessionManagement sManagement = new SessionManagement();
            Sessions session = sManagement.getSessionById(sessionid);
            AuditManagement audit = new AuditManagement();

            aStatus.error = "ERROR";
            aStatus.errorcode = -9;

            int retValue = -1;
            String strCategory = "";
//            if (type == RESET_USER_TOKEN_SOFTWARE) {
//                strCategory = "SOFTWARE_TOKEN";
//            } else if (type == RESET_USER_TOKEN_HARDWARE) {
//                strCategory = "HARDWARE_TOKEN";
//            } else if (type == RESET_USER_TOKEN_OOB) {
//                strCategory = "OOB_TOKEN";
//            }
            String strSubCategory = "";
            if (session == null) {
                return aStatus;
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
//        if (channel == null) {
//            return aStatus;
//        }
            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
            if (channelProfileObj != null) {
                int retVal = sManagement.getServerStatus(channelProfileObj);
                if (retVal != 0) {

                    aStatus = new AxiomStatus();
                    aStatus.error = "Server Down for maintainance,Please try later!!!";
                    aStatus.errorcode = -48;
                    return aStatus;
                }
            }

            String check = LoadSettings.g_sSettings.getProperty("user.reset");
            if (session != null) {
                MessageContext mc = wsContext.getMessageContext();
                HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
                //System.out.println("Client IP = " + req.getRemoteAddr());        

                if (channel == null) {
                    return null;
                }

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }

                if (trustPayLoad != null) {
                    MobileTrustSettings mSettings = null;

                    Object obj = setManagement.getSetting(sessionid, channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
                    if (obj != null) {
                        if (obj instanceof MobileTrustSettings) {
                            mSettings = (MobileTrustSettings) obj;
                        }
                    }

                    if (mSettings == null) {
                        aStatus.error = "Mobile Trust Setting is not configured!!!";
                        aStatus.errorcode = -23;
//                  
                        return aStatus;
                    }

                    if (mSettings.bGeoFencing == true) {
//
//                        TrustManagement tManagement = new TrustManagement();
//                        int res = tManagement.checkLocationAndUpdateWMI(sessionid, channelid, credentials.rssUser.userId, trustPayLoad);
//                        if (res != 0) {
//                            if (res == -14) {
//                                aStatus.errorcode = -14;
//                                aStatus.error = "Mobile Trust Setting is not configured!!!";
//                            } else if (res == -4) {
//                                aStatus.errorcode = 4;
//                                aStatus.error = "Verification failed!!!";
//                            } else if (res == -7) {
//                                aStatus.errorcode = -7;
//                                aStatus.error = "Home Country is not defined!!!";
//                            } else if (res == -10) {
//                                aStatus.errorcode = -10;
//                                aStatus.error = "Invalid Location, out of home country!!!";
//                            } else if (res == -11) {
//                                aStatus.errorcode = -11;
//                                aStatus.error = "Invalid Location, roaming country is not allowed!!!";
//                            } else if (res == -12) {
//                                aStatus.errorcode = -12;
//                                aStatus.error = "Invalid Location, roaming is not configured properly!!!";
//                            } else if (res == -13) {
//                                aStatus.errorcode = -13;
//                                aStatus.error = "Roaming Contry is not Configured!!!";
//                            } else if (res == -1) {
//                                aStatus.errorcode = -1;
//                                aStatus.error = "Internal Error!!!";
//                            } else if (res == -2) {
//                                aStatus.errorcode = -2;
//                                aStatus.error = "Session has Expired!!!";
//                            }

                        GeoLocationManagement gManagement = new GeoLocationManagement();
                        AXIOMStatus status = gManagement.validateGeoLocation(sessionid, channelid, credentials.rssUser.userId, trustPayLoad);
                        if (status == null) {
                            aStatus.error = "Location Not Found!!!";
                            aStatus.errorcode = -115;
                        } else if (status.iStatus != 0) {

                            aStatus.error = status.strStatus;
                            aStatus.errorcode = status.iStatus;
                            //     String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
                            audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                                    session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorcode,
                                    "Token Management", "Invalid Location", null, itemtype,
                                    session.getLoginid());

                            return aStatus;
                        }
                    }
                }

                for (int i = 0; i < credentials.tokenDetails.length; i++) {
                    if (credentials.tokenDetails[i] != null) {
                        if (credentials.tokenDetails[i].category == AxiomCredentialDetails.PASSWORD) {
                            UserManagement uManagement = new UserManagement();
                            retValue = uManagement.resetPassword(sessionid, session.getChannelid(), credentials.rssUser.userId);

                            if (retValue == 0) {
                                aStatus.error = "SUCCESS";
                                aStatus.errorcode = 0;
                                //return aStatus;
                            } else {
                                aStatus.errorcode = -1;
                                // return aStatus;
                            }

                            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                    req.getRemoteAddr(), channel.getName(),
                                    session.getLoginid(), session.getLoginid(),
                                    new Date(), "RESET",
                                    aStatus.error, aStatus.errorcode,
                                    "User Management",
                                    "Old Password = ******",
                                    "New Password = ******",
                                    "PASSWORD", credentials.rssUser.userId);
                            return aStatus;
                        } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.OUTOFBOUND_TOKEN) {

                            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                            TokenStatusDetails[] tokens = oManagement.getTokenList(sessionid, session.getChannelid(), credentials.rssUser.userId);
                            TokenStatusDetails tokenSelected = null;
                            if (tokens != null && tokens.length > 0) {
                                for (int j = 0; j < tokens.length; j++) {
                                    if (tokens[j].Catrgory == OTPTokenManagement.OOB_TOKEN) {
                                        tokenSelected = tokens[i];
                                        break;
                                    }
                                }
                            }

                            if (tokenSelected == null) {
                                //return ERROR;
                                aStatus.errorcode = -2;
                                aStatus.error = "Desired OOB Token is not assigned";
                                return aStatus;
                            }

                            if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_SMS) {
                                strSubCategory = "OOB__SMS_TOKEN";
                            } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_USSD) {
                                strSubCategory = "OOB__USSD_TOKEN";
                            } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_VOICE) {
                                strSubCategory = "OOB__VOICE_TOKEN";
                            } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_EMAIL) {
                                strSubCategory = "OOB__EMAIL_TOKEN";
                            }

                            String strStatus = "";
                            if (check.equals("1")) {
                                strStatus = strACTIVE;
                                retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), credentials.rssUser.userId, TOKEN_STATUS_ACTIVE, OTPTokenManagement.OOB_TOKEN, tokenSelected.SubCategory);
                            } else {
                                strStatus = strUNASSIGNED;
                                retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), credentials.rssUser.userId, TOKEN_STATUS_UNASSIGNED, OTPTokenManagement.OOB_TOKEN, tokenSelected.SubCategory);
                            }
                            if (retValue == 0) {
                                aStatus.error = "SUCCESS";
                                aStatus.errorcode = 0;
                                audit.AddAuditTrail(sessionid, session.getChannelid(), session.getLoginid(),
                                        req.getRemoteAddr(), channel.getName(),
                                        session.getLoginid(), session.getLoginid(), new Date(),
                                        "Change Status", aStatus.error, aStatus.errorcode,
                                        "OTPToken Management",
                                        "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                                        "New Status =" + strUNASSIGNED,
                                        "OTPTOKENS", credentials.rssUser.userId);
                            } else if (retValue == -6) {
                                aStatus.error = "Token is already active!!!";
                                aStatus.errorcode = -6;
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                        req.getRemoteAddr(), channel.getName(),
                                        session.getLoginid(), session.getLoginid(), new Date(),
                                        "Change Status", aStatus.error, aStatus.errorcode,
                                        "OTPToken Management",
                                        "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                                        aStatus.error,
                                        "OTPTOKENS", credentials.rssUser.userId);
                            } else if (retValue == -3) {
                                aStatus.error = "Token not found!!!";
                                aStatus.errorcode = -3;
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                        req.getRemoteAddr(), channel.getName(),
                                        session.getLoginid(), session.getLoginid(), new Date(),
                                        "Change Status", aStatus.error, aStatus.errorcode,
                                        "OTPToken Management",
                                        "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                                        aStatus.error,
                                        "OTPTOKENS", credentials.rssUser.userId);
                            } else {
                                aStatus.errorcode = -2;
                                aStatus.error = "Invalid Session used.";
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                        req.getRemoteAddr(), channel.getName(),
                                        session.getLoginid(), session.getLoginid(), new Date(),
                                        "Change Status", aStatus.error, aStatus.errorcode,
                                        "OTPToken Management",
                                        "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                                        aStatus.error,
                                        "OTPTOKENS", credentials.rssUser.userId);
                                //  return aStatus;
                            }

                            return aStatus;

                        } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.SOFTWARE_TOKEN) {

                            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                            TokenStatusDetails[] tokens = oManagement.getTokenList(sessionid, session.getChannelid(), credentials.rssUser.userId);
                            TokenStatusDetails tokenSelected = null;
                            if (tokens != null && tokens.length > 0) {
                                for (int j = 0; j < tokens.length; j++) {
                                    if (tokens[j].Catrgory == OTPTokenManagement.SOFTWARE_TOKEN) {
                                        tokenSelected = tokens[j];
                                        break;
                                    }
                                }
                            }

                            if (tokenSelected == null) {
                                //return ERROR;
                                aStatus.errorcode = -2;
                                aStatus.error = "Desired Software Token is not assigned";
                                return aStatus;
                            }

                            if (tokenSelected.SubCategory == OTP_TOKEN_SOFTWARE_WEB) {
                                strSubCategory = "SW_WEB_TOKEN";
                            } else if (tokenSelected.SubCategory == OTP_TOKEN_SOFTWARE_MOBILE) {
                                strSubCategory = "SW_MOBILE_TOKEN";
                            } else if (tokenSelected.SubCategory == OTP_TOKEN_SOFTWARE_PC) {
                                strSubCategory = "SW_PC_TOKEN";
                            }

                            String strStatus = "";
//                            if (check.equals("1")) {
//                                strStatus = strACTIVE;
//                                retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), credentials.rssUser.userId, TOKEN_STATUS_ACTIVE, OTPTokenManagement.SOFTWARE_TOKEN, tokenSelected.SubCategory);
//                            } else {
//                                strStatus = strUNASSIGNED;
//                                retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), credentials.rssUser.userId, TOKEN_STATUS_UNASSIGNED, OTPTokenManagement.SOFTWARE_TOKEN, tokenSelected.SubCategory);
                            //}
                            retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), credentials.rssUser.userId, credentials.tokenDetails[i].status, OTPTokenManagement.SOFTWARE_TOKEN, tokenSelected.SubCategory);

                            if (retValue == 0) {
                                aStatus.error = "SUCCESS";
                                aStatus.errorcode = 0;
                            } else {
                                aStatus.errorcode = -2;
                                //  return aStatus;
                            }

                            if (retValue == 0) {
                                aStatus.error = "SUCCESS";
                                aStatus.errorcode = 0;
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                        req.getRemoteAddr(), channel.getName(),
                                        session.getLoginid(), session.getLoginid(), new Date(),
                                        "Change Status", aStatus.error, aStatus.errorcode,
                                        "OTPToken Management",
                                        "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                                        "New Status =" + strUNASSIGNED,
                                        "OTPTOKENS", credentials.rssUser.userId);
                            } else if (retValue == -6) {
                                aStatus.error = "Token is already active!!!";
                                aStatus.errorcode = -6;
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                        req.getRemoteAddr(), channel.getName(),
                                        session.getLoginid(), session.getLoginid(), new Date(),
                                        "Change Status", aStatus.error, aStatus.errorcode,
                                        "OTPToken Management",
                                        "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                                        aStatus.error,
                                        "OTPTOKENS", credentials.rssUser.userId);
                            } else if (retValue == -3) {
                                aStatus.error = "Token not found!!!";
                                aStatus.errorcode = -3;
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                        req.getRemoteAddr(), channel.getName(),
                                        session.getLoginid(), session.getLoginid(), new Date(),
                                        "Change Status", aStatus.error, aStatus.errorcode,
                                        "OTPToken Management",
                                        "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                                        aStatus.error,
                                        "OTPTOKENS", credentials.rssUser.userId);
                            } else {
                                aStatus.errorcode = -2;
                                aStatus.error = "Invalid Session used.";
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                        req.getRemoteAddr(), channel.getName(),
                                        session.getLoginid(), session.getLoginid(), new Date(),
                                        "Change Status", aStatus.error, aStatus.errorcode,
                                        "OTPToken Management",
                                        "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                                        aStatus.error,
                                        "OTPTOKENS", credentials.rssUser.userId);
                                //  return aStatus;
                            }
//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                            session.getLoginid(), session.getLoginid(), new Date(), "Change OTP Status", aStatus.error, aStatus.errorcode,
//                            "Token Management", "Category =" + strCategory + "SubCategory =" + strSubCategory + "Old Status =" + tokenSelected.Status,
//                            "Category =" + strCategory + "New Status =" + strUNASSIGNED,
//                            "RESET TOKEN", userid);
                        } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.HARDWARE_TOKEN) {

                            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                            TokenStatusDetails[] tokens = oManagement.getTokenList(sessionid, session.getChannelid(), credentials.rssUser.userId);
                            TokenStatusDetails tokenSelected = null;
                            if (tokens != null && tokens.length > 0) {
                                for (int j = 0; j < tokens.length; j++) {
                                    if (tokens[j].Catrgory == OTPTokenManagement.HARDWARE_TOKEN) {
                                        tokenSelected = tokens[i];
                                        break;
                                    }
                                }
                            }

                            if (tokenSelected == null) {
                                //return ERROR;
                                aStatus.errorcode = -2;
                                aStatus.error = "Desired Hardware Token is not assigned";
                                return aStatus;
                            }

                            if (tokenSelected.SubCategory == OTP_TOKEN_HARDWARE_CR) {
                                strSubCategory = "HW_CR_TOKEN";
                            } else if (tokenSelected.SubCategory == OTP_TOKEN_HARDWARE_MINI) {
                                strSubCategory = "HW_MINI_TOKEN";
                            }

                            String strStatus = "";
//                            if (check.equals("1")) {
//                                strStatus = strACTIVE;
//                                retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), credentials.rssUser.userId, TOKEN_STATUS_ACTIVE, OTPTokenManagement.HARDWARE_TOKEN, tokenSelected.SubCategory);
//                            } else {
//                                strStatus = strUNASSIGNED;
//                                retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), credentials.rssUser.userId, TOKEN_STATUS_UNASSIGNED, OTPTokenManagement.HARDWARE_TOKEN, tokenSelected.SubCategory);
//                            }

                            if (credentials.tokenDetails[i].status == TOKEN_STATUS_ACTIVE) {
                                strStatus = strACTIVE;
                                retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), credentials.rssUser.userId, TOKEN_STATUS_ACTIVE, OTPTokenManagement.HARDWARE_TOKEN, tokenSelected.SubCategory);
                            }
                            if (credentials.tokenDetails[i].status == TOKEN_STATUS_UNASSIGNED) {
                                strStatus = strUNASSIGNED;
                                retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), credentials.rssUser.userId, TOKEN_STATUS_UNASSIGNED, OTPTokenManagement.HARDWARE_TOKEN, tokenSelected.SubCategory);
                            }
                            if (credentials.tokenDetails[i].status == TOKEN_STATUS_SUSPENDED) {
                                strStatus = strSUSPENDED;
                                retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), credentials.rssUser.userId, TOKEN_STATUS_SUSPENDED, OTPTokenManagement.HARDWARE_TOKEN, tokenSelected.SubCategory);
                            }

                            if (retValue == 0) {
                                aStatus.error = "SUCCESS";
                                aStatus.errorcode = 0;
                            } else {
                                aStatus.errorcode = -2;
                                //  return aStatus;
                            }

                            if (retValue == 0) {
                                aStatus.error = "SUCCESS";
                                aStatus.errorcode = 0;
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                        req.getRemoteAddr(), channel.getName(),
                                        session.getLoginid(), session.getLoginid(), new Date(),
                                        "Change Status", aStatus.error, aStatus.errorcode,
                                        "OTPToken Management",
                                        "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                                        "New Status =" + strUNASSIGNED,
                                        "OTPTOKENS", credentials.rssUser.userId);
                            } else if (retValue == -6) {
                                aStatus.error = "Token is already active!!!";
                                aStatus.errorcode = -6;
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                        req.getRemoteAddr(), channel.getName(),
                                        session.getLoginid(), session.getLoginid(), new Date(),
                                        "Change Status", aStatus.error, aStatus.errorcode,
                                        "OTPToken Management",
                                        "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                                        aStatus.error,
                                        "OTPTOKENS", credentials.rssUser.userId);
                            } else if (retValue == -3) {
                                aStatus.error = "Token not found!!!";
                                aStatus.errorcode = -3;
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                        req.getRemoteAddr(), channel.getName(),
                                        session.getLoginid(), session.getLoginid(), new Date(),
                                        "Change Status", aStatus.error, aStatus.errorcode,
                                        "OTPToken Management",
                                        "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                                        aStatus.error,
                                        "OTPTOKENS", credentials.rssUser.userId);
                            } else {
                                aStatus.errorcode = -2;
                                aStatus.error = "Invalid Session used.";
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                        req.getRemoteAddr(), channel.getName(),
                                        session.getLoginid(), session.getLoginid(), new Date(),
                                        "Change Status", aStatus.error, aStatus.errorcode,
                                        "OTPToken Management",
                                        "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                                        aStatus.error,
                                        "OTPTOKENS", credentials.rssUser.userId);
                                //  return aStatus;
                            }

                        } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.CERTIFICATE) {
                            CertificateManagement Cmanagement = new CertificateManagement();

                            Certificates cf = Cmanagement.getCertificate(sessionid, session.getChannelid(), credentials.rssUser.userId);
                            if (cf == null) {
                                aStatus.errorcode = -2;
                                aStatus.error = "Certificate not assigned";
                                return aStatus;
                            }

                            String strStatus = "";
                            if (check.equals("1")) {
                                strStatus = strACTIVE;
//                            retValue = Cmanagement.RevokeCertificate(sessionid, channelid, credentials.rssUser.userId, "");
//                            if (retValue == 0) {
//                                String cer = Cmanagement.GenerateCertificate(sessionid, session.getChannelid(), credentials.rssUser.userId);
//                                if (cer != "") {
//                                    retValue = 0;
//                                } else {
//                                    retValue = -4;
//                                }
                                retValue = Cmanagement.DeleteCert(sessionid, channel.getChannelid(), credentials.rssUser.userId);
                                int certificateRenew = -1;
                                if (retValue == 0) {

                                    String cer = Cmanagement.GenerateCertificate(sessionid, session.getChannelid(), credentials.rssUser.userId);
                                    //certificateRenew = Cmanagement.RenewCertificate(sessionid, channel.getChannelid(), credentials.rssUser.userId);
                                    if (cer.compareTo("") != 0) {
                                        retValue = 0;
                                    } else {
                                        retValue = -4;
                                    }

                                    if (retValue == 0) {
                                        if (retValue == 0) {
                                            aStatus.error = "SUCCESS";
                                            aStatus.errorcode = 0;
                                            audit.AddAuditTrail(sessionid, session.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                                    channel.getName(),
                                                    session.getLoginid(), session.getLoginid(),
                                                    new Date(),
                                                    "Issue",
                                                    aStatus.error,
                                                    aStatus.errorcode,
                                                    "Certificate Management", "",
                                                    "Category = " + strCategory + " Sub Category =" + strSubCategory,
                                                    "CERTIFICATE", credentials.rssUser.userId);

                                        } else if (retValue == -4) {
                                            aStatus.error = "Certificate not Generated";
                                            aStatus.errorcode = -4;
                                            audit.AddAuditTrail(sessionid, session.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                                    channel.getName(),
                                                    session.getLoginid(), session.getLoginid(),
                                                    new Date(),
                                                    "Issue",
                                                    aStatus.error,
                                                    aStatus.errorcode,
                                                    "Certificate Management", "",
                                                    "Category = " + strCategory + " Sub Category =" + strSubCategory,
                                                    "CERTIFICATE", credentials.rssUser.userId);

                                        }
                                    } else {

                                    }

                                }

                            } else {
                                strStatus = strUNASSIGNED;
                                String cer = Cmanagement.GenerateCertificate(sessionid, session.getChannelid(), credentials.rssUser.userId);
                                if (cer != null) {
                                    retValue = 0;
                                } else {
                                    retValue = -7;
                                }

                                if (retValue == 0) {
                                    aStatus.error = "SUCCESS";
                                    aStatus.errorcode = 0;
                                    audit.AddAuditTrail(sessionid, session.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                            channel.getName(),
                                            session.getLoginid(), session.getLoginid(),
                                            new Date(),
                                            "Issue",
                                            aStatus.error,
                                            aStatus.errorcode,
                                            "Certificate Management", "",
                                            "Category = " + strCategory + " Sub Category =" + strSubCategory,
                                            "CERTIFICATE", credentials.rssUser.userId);

                                } else if (retValue == -7) {
                                    aStatus.error = "Certificate not generated";
                                    aStatus.errorcode = -7;
                                    audit.AddAuditTrail(sessionid, session.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                            channel.getName(),
                                            session.getLoginid(), session.getLoginid(),
                                            new Date(),
                                            "Issue",
                                            aStatus.error,
                                            aStatus.errorcode,
                                            "Certificate Management", "",
                                            "Category = " + strCategory + " Sub Category =" + strSubCategory,
                                            "CERTIFICATE", credentials.rssUser.userId);

                                }

                            }

                        } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.PKI_SOFTWARE_TOKEN) {

                            PKITokenManagement pManagement = new PKITokenManagement();
                            TokenStatusDetails[] tokens = pManagement.getTokenList(sessionid, session.getChannelid(), credentials.rssUser.userId);
                            TokenStatusDetails tokenSelected = null;
                            if (tokens != null && tokens.length > 0) {
                                for (int j = 0; j < tokens.length; j++) {
                                    if (tokens[j].Catrgory == PKITokenManagement.SOFTWARE_TOKEN) {
                                        tokenSelected = tokens[i];
                                        break;
                                    }
                                }
                            }

                            if (tokenSelected == null) {
                                //return ERROR;
                                aStatus.errorcode = -2;
                                aStatus.error = "Desired PKI Software Token is not assigned";
                                return aStatus;
                            }

                            String strStatus = "";
//                            if (check.equals("1")) {
//                                strStatus = strACTIVE;
//                                retValue = pManagement.ChangeStatus(sessionid, session.getChannelid(), credentials.rssUser.userId, TOKEN_STATUS_ACTIVE, PKITokenManagement.SOFTWARE_TOKEN);
//                            } else {
//                                strStatus = strUNASSIGNED;
//                                retValue = pManagement.ChangeStatus(sessionid, session.getChannelid(), credentials.rssUser.userId, TOKEN_STATUS_UNASSIGNED, PKITokenManagement.SOFTWARE_TOKEN);
//                            }

                            retValue = pManagement.ChangeStatus(sessionid, session.getChannelid(), credentials.rssUser.userId, credentials.tokenDetails[i].status, PKITokenManagement.SOFTWARE_TOKEN);

                            if (retValue == 0) {
                                aStatus.error = "SUCCESS";
                                aStatus.errorcode = 0;
                            } else {
                                aStatus.errorcode = -2;
                                //  return aStatus;
                            }

                            if (retValue == 0) {
                                aStatus.error = "SUCCESS";
                                aStatus.errorcode = 0;
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                        req.getRemoteAddr(), channel.getName(),
                                        session.getLoginid(), session.getLoginid(), new Date(),
                                        "Change Status", aStatus.error, aStatus.errorcode,
                                        "PKI Token Management",
                                        "Category=" + strCategory + ",Current Status=" + tokenSelected.Status,
                                        "New Status =" + strUNASSIGNED,
                                        "PKITOKEN", credentials.rssUser.userId);
                            } else if (retValue == -6) {
                                aStatus.error = "Token is already active!!!";
                                aStatus.errorcode = -6;
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                        req.getRemoteAddr(), channel.getName(),
                                        session.getLoginid(), session.getLoginid(), new Date(),
                                        "Change Status", aStatus.error, aStatus.errorcode,
                                        "PKITOKEN Management",
                                        "Category=" + strCategory + ",Current Status=" + tokenSelected.Status,
                                        aStatus.error,
                                        "PKITOKEN", credentials.rssUser.userId);
                            } else if (retValue == -3) {
                                aStatus.error = "Token not found!!!";
                                aStatus.errorcode = -3;
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                        req.getRemoteAddr(), channel.getName(),
                                        session.getLoginid(), session.getLoginid(), new Date(),
                                        "Change Status", aStatus.error, aStatus.errorcode,
                                        "PKITOKEN Management",
                                        "Category=" + strCategory + ",Current Status=" + tokenSelected.Status,
                                        aStatus.error,
                                        "PKITOKEN", credentials.rssUser.userId);
                            } else {
                                aStatus.errorcode = -2;
                                aStatus.error = "Invalid Session used.";
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                        req.getRemoteAddr(), channel.getName(),
                                        session.getLoginid(), session.getLoginid(), new Date(),
                                        "Change Status", aStatus.error, aStatus.errorcode,
                                        "PKITOKEN Management",
                                        "Category=" + strCategory + ",Current Status=" + tokenSelected.Status,
                                        aStatus.error,
                                        "PKITOKEN", credentials.rssUser.userId);
                                //  return aStatus;
                            }

                        } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.PKI_HARDWARE_TOKEN) {

                            PKITokenManagement pManagement = new PKITokenManagement();
                            TokenStatusDetails[] tokens = pManagement.getTokenList(sessionid, session.getChannelid(), credentials.rssUser.userId);
                            TokenStatusDetails tokenSelected = null;
                            if (tokens != null && tokens.length > 0) {
                                for (int j = 0; j < tokens.length; j++) {
                                    if (tokens[j].Catrgory == PKITokenManagement.HARDWARE_TOKEN) {
                                        tokenSelected = tokens[i];
                                        break;
                                    }
                                }
                            }

                            if (tokenSelected == null) {
                                //return ERROR;
                                aStatus.errorcode = -2;
                                aStatus.error = "Desired PKI Hardware Token is not assigned";
                                return aStatus;
                            }

                            String strStatus = "";
//                            if (check.equals("1")) {
//                                strStatus = strACTIVE;
//                                retValue = pManagement.ChangeStatus(sessionid, session.getChannelid(), credentials.rssUser.userId, TOKEN_STATUS_ACTIVE, PKITokenManagement.HARDWARE_TOKEN);
//                            } else {
//                                strStatus = strUNASSIGNED;
//                                retValue = pManagement.ChangeStatus(sessionid, session.getChannelid(), credentials.rssUser.userId, TOKEN_STATUS_UNASSIGNED, PKITokenManagement.HARDWARE_TOKEN);
//                            }

                            retValue = pManagement.ChangeStatus(sessionid, session.getChannelid(), credentials.rssUser.userId, credentials.tokenDetails[i].status, PKITokenManagement.HARDWARE_TOKEN);

                            if (retValue == 0) {
                                aStatus.error = "SUCCESS";
                                aStatus.errorcode = 0;
                            } else {
                                aStatus.errorcode = -2;
                                //  return aStatus;
                            }

                            if (retValue == 0) {
                                aStatus.error = "SUCCESS";
                                aStatus.errorcode = 0;
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                        req.getRemoteAddr(), channel.getName(),
                                        session.getLoginid(), session.getLoginid(), new Date(),
                                        "Change Status", aStatus.error, aStatus.errorcode,
                                        "PKI Token Management",
                                        "Category=" + strCategory + ",Current Status=" + tokenSelected.Status,
                                        "New Status =" + strUNASSIGNED,
                                        "PKITOKEN", credentials.rssUser.userId);
                            } else if (retValue == -6) {
                                aStatus.error = "Token is already active!!!";
                                aStatus.errorcode = -6;
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                        req.getRemoteAddr(), channel.getName(),
                                        session.getLoginid(), session.getLoginid(), new Date(),
                                        "Change Status", aStatus.error, aStatus.errorcode,
                                        "PKITOKEN Management",
                                        "Category=" + strCategory + ",Current Status=" + tokenSelected.Status,
                                        aStatus.error,
                                        "PKITOKEN", credentials.rssUser.userId);
                            } else if (retValue == -3) {
                                aStatus.error = "Token not found!!!";
                                aStatus.errorcode = -3;
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                        req.getRemoteAddr(), channel.getName(),
                                        session.getLoginid(), session.getLoginid(), new Date(),
                                        "Change Status", aStatus.error, aStatus.errorcode,
                                        "PKITOKEN Management",
                                        "Category=" + strCategory + ",Current Status=" + tokenSelected.Status,
                                        aStatus.error,
                                        "PKITOKEN", credentials.rssUser.userId);
                            } else {
                                aStatus.errorcode = -2;
                                aStatus.error = "Invalid Session used.";
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                        req.getRemoteAddr(), channel.getName(),
                                        session.getLoginid(), session.getLoginid(), new Date(),
                                        "Change Status", aStatus.error, aStatus.errorcode,
                                        "PKITOKEN Management",
                                        "Category=" + strCategory + ",Current Status=" + tokenSelected.Status,
                                        aStatus.error,
                                        "PKITOKEN", credentials.rssUser.userId);
                                //  return aStatus;
                            }

                        }
                    }

                }
            }

            return aStatus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public AxiomStatus ChangeCredential(String sessionid, RSSUserCerdentials credentials, String integrityCheckString, String trustPayLoad) {

        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");

            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                System.out.println("ChangeCredential::sessionid::" + sessionid);
                System.out.println("ChangeCredential::userid::" + credentials.rssUser.userId);
                for (int i = 0; i < credentials.tokenDetails.length; i++) {
                    System.out.println("Categorey" + credentials.tokenDetails[i].category);
                    System.out.println("SubCategory" + credentials.tokenDetails[i].subcategory);
                }
            }
        } catch (Exception ex) {
        }
//nilesh
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "Licence is invalid";
//            aStatus.errorcode = -100;
//            return aStatus;
        }
        AxiomStatus aStatus = new AxiomStatus();

        byte[] SHA1hash = null;
        if (trustPayLoad == null) {
            SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.rssUser.emailid + credentials.rssUser.userId + credentials.rssUser.phoneNumber + credentials.rssUser.userName);
        } else {
            SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.rssUser.emailid + credentials.rssUser.userId + credentials.rssUser.phoneNumber + credentials.rssUser.userName + trustPayLoad);
        }
//        String integritycheck = new String(Base64.encode(SHA1hash));
        String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//        String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java
        if (integrityCheckString != null) {
            if (integrityCheckString.equals(integritycheck) != true) {
                aStatus.error = "Input Data is Tampered!!!";
                aStatus.errorcode = -76;
                return aStatus;

            }
        }

        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);
        if (session == null) {

            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
            return aStatus;
        }

        aStatus.error = "ERROR";

        aStatus.errorcode = -2;
        String oldValue = "";
        String strValue = "";
        String UserId = credentials.rssUser.userId;
        int retValue = -1;
//        if (session == null) {
//            return aStatus;
//        }
        if (session != null) {

            //System.out.println("Client IP = " + req.getRemoteAddr());        
            SettingsManagement setManagement = new SettingsManagement();

            String channelid = session.getChannelid();
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(channelid);
            if (channel == null) {
                return null;
            }

            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
            if (channelProfileObj != null) {
                int retVal = sManagement.getServerStatus(channelProfileObj);
                if (retVal != 0) {

                    aStatus = new AxiomStatus();
                    aStatus.error = "Server Down for maintainance,Please try later!!!";
                    aStatus.errorcode = -48;
                    return aStatus;
                }
            }

            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        OperatorsManagement oManagement = new OperatorsManagement();
                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }

                        suTemplate.close();
                        sTemplate.close();
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                    aStatus.error = "INVALID IP REQUEST";
                    aStatus.errorcode = -8;
                    return aStatus;
                }
            }
            boolean bSentToUser = true;
            int subcategory = 1;
            int geoCheck = 1;
            ImageSettings iSettings = null;
            Object obj1 = setManagement.getSetting(sessionid, channelid, SettingsManagement.IMAGE_SETTINGS, SettingsManagement.PREFERENCE_ONE);
            if (obj1 != null) {
                if (obj1 instanceof ImageSettings) {
                    iSettings = (ImageSettings) obj1;
                    if (iSettings != null) {
                        if (iSettings._geoCheck != 1) {//1 - Active ; 0 - InActive
                            geoCheck = 0;
                        }
                    }
                }
            }
            if (geoCheck == 1) {
                if (trustPayLoad != null) {
                    MobileTrustSettings mSettings = null;

                    Object obj = setManagement.getSetting(sessionid, channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
                    if (obj != null) {
                        if (obj instanceof MobileTrustSettings) {
                            mSettings = (MobileTrustSettings) obj;
                        }
                    }

                    if (mSettings == null) {
                        aStatus.error = "Mobile Trust Setting is not configured!!!";
                        aStatus.errorcode = -23;
//                  
                        return aStatus;
                    }

                    if (mSettings.bGeoFencing == true) {

                        GeoLocationManagement gManagement = new GeoLocationManagement();
                        AXIOMStatus status = gManagement.validateGeoLocation(sessionid, channelid, credentials.rssUser.userId, trustPayLoad);
                        if (status == null) {
                            aStatus.error = "Location Not Found!!!";
                            aStatus.errorcode = -115;
                        } else if (status.iStatus != 0) {

                            aStatus.error = status.strStatus;
                            aStatus.errorcode = status.iStatus;
                            //     String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
                            audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                                    session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorcode,
                                    "Token Management", "Invalid Location", "", itemtype,
                                    session.getLoginid());

                            return aStatus;
                        }
                    }
                }
            }
            for (int i = 0; i < credentials.tokenDetails.length; i++) {
                if (credentials.tokenDetails[i] != null) {
                    if (credentials.tokenDetails[i].category == AxiomCredentialDetails.PASSWORD) {
                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.REMOTESIGNSERVICEWITH_OTP) != 0) {
                            aStatus = new AxiomStatus();
                            aStatus.error = "This feature is not available in this license!!!";
                            aStatus.errorcode = -101;
                            return aStatus;
                        }
                        UserManagement um = new UserManagement();
                        retValue = um.AssignPassword(sessionid, session.getChannelid(), UserId, credentials.tokenDetails[i].Password);
                        if (retValue == 0) {
                            if (bSentToUser == true) {
                                SendNotification send = new SendNotification();
                                AuthUser aUser = um.getUser(sessionid, session.getChannelid(), UserId);
                                Templates temp = null;
                                TemplateManagement tObj = new TemplateManagement();
                                if (subcategory == 1) // sms
                                {
                                    temp = tObj.LoadbyName(sessionid, session.getChannelid(), TemplateNames.MOBILE_USER_PASSWORD_TEMPLATE);
                                }
                                if (temp == null) {
                                    aStatus.error = "Message Template is missing";
                                    aStatus.errorcode = -2;
                                    //return aStatus;
                                }

                                ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
                                String templatebody = (String) TemplateUtils.deserializeFromObject(bais);

                                templatebody = templatebody.replaceAll("#name#", aUser.userName);
                                String date = String.valueOf(new Date());
                                templatebody = templatebody.replaceAll("#channel#", channel.getName());
                                templatebody = templatebody.replaceAll("#password#", credentials.tokenDetails[i].Password);
                                templatebody = templatebody.replaceAll("#datetime#", date);

                                com.mollatech.axiom.connector.communication.AXIOMStatus axiomStatus = null;

                                int iProductType = Integer.valueOf(LoadSettings.g_sSettings.getProperty("product.type")).intValue();

                                if (temp.getStatus() == tObj.ACTIVE_STATUS) {
                                    axiomStatus = send.SendOnMobile(session.getChannelid(), aUser.phoneNo, templatebody, subcategory, iProductType);
                                }
                                if (axiomStatus != null) {
                                    aStatus.error = axiomStatus.strStatus;
                                    aStatus.errorcode = axiomStatus.iStatus;
                                }
                            } else {
                                aStatus.error = "SUCCESS";
                                aStatus.errorcode = 0;
                            }

                        }

                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.CERTIFICATE) {

                        //CertificateManagement cerMgmt = new CertificateManagement();
                        CertificateManagement cerMgmt = new CertificateManagement();
                        int res = -1;
                        if (credentials.tokenDetails[i].status == CertificateManagement.CERT_STATUS_REVOKED) {
                            res = cerMgmt.RevokeCertificate(sessionid, channelid, UserId, "");
                        }

                        if (res == 0) {
                            res = cerMgmt.DeleteCert(sessionid, channelid, UserId);
                        }
                        String resultString = null;
                        if (res == 0) {
                            aStatus.error = "SUCCESS";
                            aStatus.errorcode = 0;
                            resultString = "Success";
                            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                    req.getRemoteAddr(),
                                    //                    /ipaddress, 
                                    channel.getName(),
                                    session.getLoginid(), session.getLoginid(), new Date(), "Delete Certificate", resultString, retValue,
                                    "Certificate Management", "", " ",
                                    "PKITOKEN", UserId);
                        } else {
                            aStatus.error = "error";
                            aStatus.errorcode = retValue;
                            resultString = "Failure";
                            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                    req.getRemoteAddr(),
                                    //ipaddress, 
                                    channel.getName(),
                                    session.getLoginid(), session.getLoginid(), new Date(), "Delete Certificate", resultString, retValue,
                                    "Certificate Management", "", " ",
                                    "PKITOKEN", UserId);

                        }

                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.CHALLAEGE_RESPONSE) {
                        // AxiomQuestionsAndAnswers axiomQandA = new AxiomQuestionsAndAnswers();
                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.REMOTESIGNSERVICEWITH_CHALLENGERESPONSE) != 0) {
                            aStatus = new AxiomStatus();
                            aStatus.error = "This feature is not available in this license!!!";
                            aStatus.errorcode = -101;
                            return aStatus;
                        }
                        AxiomQuestionsAndAnswers axiomQAndA = new AxiomQuestionsAndAnswers();
                        axiomQAndA.webQAndA = credentials.tokenDetails[i].qas.webQAndA;
                        ChallengeResponsemanagement chManagment = new ChallengeResponsemanagement();
                        retValue = chManagment.UpdateRegisterUser(sessionid, session.getChannelid(), UserId, axiomQAndA);
                        //  retValue = uManagement.CreateUser(sessionId, session.getChannelid(), fullname, phone, email);

                        if (retValue == 0) {
                            aStatus.error = "SUCCESS";
                            aStatus.errorcode = 0;
                        }

                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.SOFTWARE_TOKEN) {
                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
                            aStatus = new AxiomStatus();
                            aStatus.error = "This feature is not available in this license!!!";
                            aStatus.errorcode = -101;
                            return aStatus;
                        }

                        int cat = OTP_TOKEN_SOFTWARE;
                        int subcat = 0;
                        if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_SOFTWARE_MOBILE) {
                            subcat = OTP_TOKEN_SOFTWARE;
                        }
                        if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_SOFTWARE_WEB) {
                            subcat = OTP_TOKEN_SOFTWARE_WEB;
                        }
                        if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_SOFTWARE_PC);
                        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                        TokenStatusDetails tokenSelected = oManagement.getToken(sessionid, session.getChannelid(), UserId, cat);

                        if (tokenSelected == null) {
                            aStatus.errorcode = -2;
                            aStatus.error = "Desired Token is not assigned";
                            return aStatus;
                        } else {
                            if (tokenSelected.Status == TOKEN_STATUS_ACTIVE) {
                                oldValue = strACTIVE;
                            } else if (tokenSelected.Status == TOKEN_STATUS_SUSPENDED) {
                                oldValue = strSUSPENDED;
                            } else if (tokenSelected.Status == TOKEN_STATUS_UNASSIGNED) {
                                oldValue = strUNASSIGNED;
                            }

                            if (credentials.tokenDetails[i].status == TOKEN_STATUS_ACTIVE) {
                                strValue = strACTIVE;
                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_SUSPENDED) {
                                strValue = strSUSPENDED;
                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_UNASSIGNED) {
                                strValue = strUNASSIGNED;
                            }

                            retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), UserId, credentials.tokenDetails[i].status, cat, subcat);

                            if (retValue == 0) {
                                aStatus.error = "SUCCESS";
                                aStatus.errorcode = 0;
                            } else if (retValue == -6) {
                                aStatus.error = "Status your trying to change is same as current status";
                                aStatus.errorcode = -6;
                            }

                            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                    channel.getName(),
                                    session.getLoginid(), session.getLoginid(),
                                    new Date(),
                                    "CHANGE STATUS", aStatus.error, aStatus.errorcode,
                                    "Token Management",
                                    "Current Status=" + oldValue,
                                    "New Status=" + strValue,
                                    "OTPTOKENS", UserId);

                        }

                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.HARDWARE_TOKEN) {

                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_HARDWARE) != 0) {
                            aStatus = new AxiomStatus();
                            aStatus.error = "This feature is not available in this license!!!";
                            aStatus.errorcode = -101;
                            return aStatus;
                        }
                        int cat = OTP_TOKEN_HARDWARE;
                        int subcat = 0;
                        if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_HARDWARE_CR) {
                            subcat = OTP_TOKEN_HARDWARE_CR;
                        } else {
                            subcat = OTP_TOKEN_HARDWARE_MINI;
                        }
                        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                        TokenStatusDetails tokenSelected = oManagement.getToken(sessionid, session.getChannelid(), UserId, cat);

                        if (tokenSelected == null) {
                            aStatus.errorcode = -2;
                            aStatus.error = "Desired Token is not assigned";
                            return aStatus;
                        } else {
                            if (tokenSelected.Status == TOKEN_STATUS_ACTIVE) {
                                oldValue = strACTIVE;
                            } else if (tokenSelected.Status == TOKEN_STATUS_SUSPENDED) {
                                oldValue = strSUSPENDED;
                            } else if (tokenSelected.Status == TOKEN_STATUS_UNASSIGNED) {
                                oldValue = strUNASSIGNED;
                            }

                            if (credentials.tokenDetails[i].status == TOKEN_STATUS_ACTIVE) {
                                strValue = strACTIVE;
                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_SUSPENDED) {
                                strValue = strSUSPENDED;
                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_UNASSIGNED) {
                                strValue = strUNASSIGNED;
                            }

                            retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), UserId, credentials.tokenDetails[i].status, cat, credentials.tokenDetails[i].subcategory);

                            if (retValue == 0) {
                                aStatus.error = "SUCCESS";
                                aStatus.errorcode = 0;
                            } else if (retValue == -6) {
                                aStatus.error = "Status your trying to change is same as current status";
                                aStatus.errorcode = -6;
                            }

                            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                    channel.getName(),
                                    session.getLoginid(), session.getLoginid(),
                                    new Date(),
                                    "CHANGE STATUS", aStatus.error, aStatus.errorcode,
                                    "Token Management",
                                    "Current Status=" + oldValue,
                                    "New Status=" + strValue,
                                    "OTPTOKENS", UserId);
                        }

                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.OUTOFBOUND_TOKEN) {
                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_OOB) != 0) {
                            aStatus = new AxiomStatus();
                            aStatus.error = "This feature is not available in this license!!!";
                            aStatus.errorcode = -101;
                            return aStatus;
                        }
                        int cat = OTP_TOKEN_OUTOFBAND;
                        int subcat = 0;
                        if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_OUTOFBAND_SMS) {
                            subcat = OTP_TOKEN_OUTOFBAND_SMS;
                        }
                        if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_OUTOFBAND_EMAIL) {
                            subcat = OTP_TOKEN_OUTOFBAND_EMAIL;
                        }
                        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                        TokenStatusDetails tokenSelected = oManagement.getToken(sessionid, session.getChannelid(), UserId, cat);

                        if (tokenSelected == null) {
                            aStatus.errorcode = -2;
                            aStatus.error = "Desired Token is not assigned";
                            return aStatus;
                        } else {
                            if (tokenSelected.Status == TOKEN_STATUS_ACTIVE) {
                                oldValue = strACTIVE;
                            } else if (tokenSelected.Status == TOKEN_STATUS_SUSPENDED) {
                                oldValue = strSUSPENDED;
                            } else if (tokenSelected.Status == TOKEN_STATUS_UNASSIGNED) {
                                oldValue = strUNASSIGNED;
                            }

                            if (credentials.tokenDetails[i].status == TOKEN_STATUS_ACTIVE) {
                                strValue = strACTIVE;
                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_SUSPENDED) {
                                strValue = strSUSPENDED;
                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_UNASSIGNED) {
                                strValue = strUNASSIGNED;
                            }

                            retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), UserId, credentials.tokenDetails[i].status, cat, credentials.tokenDetails[i].subcategory);

                            if (retValue == 0) {
                                aStatus.error = "SUCCESS";
                                aStatus.errorcode = 0;
                            } else if (retValue == -6) {
                                aStatus.error = "Status your trying to change is same as current status";
                                aStatus.errorcode = -6;
                            }

                            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                    channel.getName(),
                                    session.getLoginid(), session.getLoginid(),
                                    new Date(),
                                    "CHANGE STATUS", aStatus.error, aStatus.errorcode,
                                    "Token Management",
                                    "Current Status=" + oldValue,
                                    "New Status=" + strValue,
                                    "OTPTOKENS", UserId);
                        }

                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.PKI_SOFTWARE_TOKEN) {
                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.PKI_SOFTWARE) != 0) {
                            aStatus = new AxiomStatus();
                            aStatus.error = "This feature is not available in this license!!!";
                            aStatus.errorcode = -101;
                            return aStatus;
                        }

                        int cat = PKITokenManagement.SOFTWARE_TOKEN;
                        int subcat = 0;

                        PKITokenManagement pManagement = new PKITokenManagement();
                        TokenStatusDetails tokenSelected = pManagement.getToken(sessionid, session.getChannelid(), UserId, cat);

                        if (tokenSelected == null) {
                            aStatus.errorcode = -2;
                            aStatus.error = "Desired Token is not assigned";
                            return aStatus;
                        } else {
                            if (tokenSelected.Status == TOKEN_STATUS_ACTIVE) {
                                oldValue = strACTIVE;
                            } else if (tokenSelected.Status == TOKEN_STATUS_SUSPENDED) {
                                oldValue = strSUSPENDED;
                            } else if (tokenSelected.Status == TOKEN_STATUS_UNASSIGNED) {
                                oldValue = strUNASSIGNED;
                            }

                            if (credentials.tokenDetails[i].status == TOKEN_STATUS_ACTIVE) {
                                strValue = strACTIVE;
                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_SUSPENDED) {
                                strValue = strSUSPENDED;
                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_UNASSIGNED) {
                                strValue = strUNASSIGNED;
                            }

                            retValue = pManagement.ChangeStatus(sessionid, session.getChannelid(), UserId, credentials.tokenDetails[i].status, cat);

                            if (retValue == 0) {
                                aStatus.error = "SUCCESS";
                                aStatus.errorcode = 0;
                            } else if (retValue == -6) {
                                aStatus.error = "Status your trying to change is same as current status";
                                aStatus.errorcode = -6;
                            }

                            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                    channel.getName(),
                                    session.getLoginid(), session.getLoginid(),
                                    new Date(),
                                    "CHANGE STATUS", aStatus.error, aStatus.errorcode,
                                    "PKI Token Management",
                                    "Current Status=" + oldValue,
                                    "New Status=" + strValue,
                                    "PKITOKEN", UserId);

                        }

                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.PKI_HARDWARE_TOKEN) {
                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.PKI_HARDWARE) != 0) {
                            aStatus = new AxiomStatus();
                            aStatus.error = "This feature is not available in this license!!!";
                            aStatus.errorcode = -101;
                            return aStatus;
                        }

                        int cat = PKITokenManagement.HARDWARE_TOKEN;
                        int subcat = 0;

                        PKITokenManagement pManagement = new PKITokenManagement();
                        TokenStatusDetails tokenSelected = pManagement.getToken(sessionid, session.getChannelid(), UserId, cat);

                        if (tokenSelected == null) {
                            aStatus.errorcode = -2;
                            aStatus.error = "Desired Token is not assigned";
                            return aStatus;
                        } else {
                            if (tokenSelected.Status == TOKEN_STATUS_ACTIVE) {
                                oldValue = strACTIVE;
                            } else if (tokenSelected.Status == TOKEN_STATUS_SUSPENDED) {
                                oldValue = strSUSPENDED;
                            } else if (tokenSelected.Status == TOKEN_STATUS_UNASSIGNED) {
                                oldValue = strUNASSIGNED;
                            }

                            if (credentials.tokenDetails[i].status == TOKEN_STATUS_ACTIVE) {
                                strValue = strACTIVE;
                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_SUSPENDED) {
                                strValue = strSUSPENDED;
                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_UNASSIGNED) {
                                strValue = strUNASSIGNED;
                            }

                            retValue = pManagement.ChangeStatus(sessionid, session.getChannelid(), UserId, credentials.tokenDetails[i].status, cat);

                            if (retValue == 0) {
                                aStatus.error = "SUCCESS";
                                aStatus.errorcode = 0;
                            } else if (retValue == -6) {
                                aStatus.error = "Status your trying to change is same as current status";
                                aStatus.errorcode = -6;
                            }

                            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                    channel.getName(),
                                    session.getLoginid(), session.getLoginid(),
                                    new Date(),
                                    "CHANGE STATUS", aStatus.error, aStatus.errorcode,
                                    "PKI Token Management",
                                    "Current Status=" + oldValue,
                                    "New Status=" + strValue,
                                    "PKITOKEN", UserId);

                        }

                    } //nilesh
                    else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.SECURE_PHRASE) {
                        SecurePhraseManagment sMngt = new SecurePhraseManagment();

                        Securephrase Securephrase = sMngt.GetSecurePhrase(sessionid, session.getChannelid(), UserId);

//                        byte[] _securephraseID = UtilityFunctions.SHA1("" + credentials.tokenDetails[i].securePhraseImage + credentials.tokenDetails[i].xCoordinate + credentials.tokenDetails[i].yCoordinate);
//                        String securephraseID = new String(Base64.encode(_securephraseID));
                        if (Securephrase == null) {
                            retValue = sMngt.AddSecurePhrase(sessionid, session.getChannelid(), UserId,
                                    credentials.tokenDetails[i].securePhraseImage, credentials.tokenDetails[i].xCoordinate, credentials.tokenDetails[i].yCoordinate, credentials.tokenDetails[i].securePhraseID);
//                            retValue = -45;

                        } else {
                            retValue = sMngt.EditSecurePhrase(sessionid, session.getChannelid(), UserId,
                                    credentials.tokenDetails[i].securePhraseImage, credentials.tokenDetails[i].xCoordinate, credentials.tokenDetails[i].yCoordinate, credentials.tokenDetails[i].securePhraseID);
                        }
                        if (retValue == 0) {
                            MobileTrustManagment mManagment = new MobileTrustManagment();
                            if (credentials.tokenDetails[i].securePhraseID != null) {
                                try {
                                    TrustDeviceManagement trustDevice = new TrustDeviceManagement();
                                    Trusteddevice trust = trustDevice.getTrusteddeviceByDeviceId(sessionid, channelid, UserId, credentials.tokenDetails[i].securePhraseID);

                                    if (trust == null) {
                                        retValue = mManagment.addTrustedDevice(sessionid, session.getChannelid(), UserId, credentials.tokenDetails[i].securePhraseID, credentials.errorMsg, "", credentials.tokenDetails[i].securePhraseID, true, "", new Date(), new Date());
                                    } else {
                                        retValue = 0;
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                            aStatus.error = "SUCCESS";
                            aStatus.errorcode = 0;
                        } else if (retValue == -1) {
                            aStatus.error = "Failed to change security phrase";
                            aStatus.errorcode = -1;
                        } else if (retValue == -45) {
                            aStatus.error = "Registartion code is not generated!!";
                            aStatus.errorcode = -45;
                        }
                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                channel.getName(),
                                session.getLoginid(), session.getLoginid(),
                                new Date(),
                                "CHANGE SECURITY PHRASE", aStatus.error, aStatus.errorcode,
                                "SECURITY PHRASE Management",
                                "Current Status=" + oldValue,
                                "New Status=" + strValue,
                                "SECURITYPHRASE", UserId);

                    }
                    //nilesh

                }
            }

        }
        return aStatus;
    }

    @Override
    public AxiomStatus AssignCredential(String sessionid, RSSUserCerdentials credentials, String integrityCheckString, String trustPayLoad) {

        String strDebug = null;

        try {
            System.out.println("AssignCredential::sessionId::" + sessionid);
            System.out.println("AssignCredential::userid::" + credentials.rssUser.userId);
            for (int i = 0; i < credentials.tokenDetails.length; i++) {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {

                    System.out.println("AssignCredential::type::" + credentials.tokenDetails[i].category);
                    System.out.println("AssignCredential::subtype::" + credentials.tokenDetails[i].subcategory);
                    System.out.println("AssignCredential::Password::" + credentials.tokenDetails[i].Password);
                    System.out.println("AssignCredential::Attempts::" + credentials.tokenDetails[i].attempts);
                    System.out.println("AssignCredential::User Certificate" + credentials.tokenDetails[i].base64Cert);
                    System.out.println("AssignCredential::Serial Number" + credentials.tokenDetails[i].serialnumber);
                    System.out.println("AssignCredential::AxiomQuestionAnserChallenge" + credentials.tokenDetails[i].qas);

                }
            }
        } catch (Exception ex) {
        }
        String UserId = credentials.rssUser.userId;
//        nilesh
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "Licence is invalid";
//            aStatus.errorcode = -100;
//            return aStatus;
        }

        AxiomStatus aStatus = new AxiomStatus();

        byte[] SHA1hash = null;
        if (trustPayLoad == null) {
            SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.rssUser.emailid + credentials.rssUser.userId + credentials.rssUser.phoneNumber + credentials.rssUser.userName);
        } else {
            SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.rssUser.emailid + credentials.rssUser.userId + credentials.rssUser.phoneNumber + credentials.rssUser.userName + trustPayLoad);
        }
//        String integritycheck = new String(Base64.encode(SHA1hash));
        String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//        String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java
        if (integrityCheckString != null) {
            if (integrityCheckString.equals(integritycheck) != true) {
                aStatus.error = "Input Data is Tampered!!!";
                aStatus.errorcode = -76;
                return aStatus;

            }
        }
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);

        if (session == null) {
            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
            return aStatus;
        }

        if (sessionid == null || credentials.rssUser.userId == null
                || sessionid.isEmpty() == true || credentials.rssUser.userId.isEmpty() == true
                || credentials.tokenDetails.length < 0) {
            //AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Invalid Data";
            aStatus.errorcode = -11;
            return aStatus;
        }

        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(session.getChannelid());
        if (channel == null) {
            aStatus.error = "Invalid Channel";
            aStatus.errorcode = -15;
            return aStatus;
        }
        SettingsManagement setManagement = new SettingsManagement();
        ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channel.getChannelid(), SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
        if (channelProfileObj != null) {
            int retVal = sManagement.getServerStatus(channelProfileObj);
            if (retVal != 0) {

                aStatus = new AxiomStatus();
                aStatus.error = "Server Down for maintainance,Please try later!!!";
                aStatus.errorcode = -48;
                return aStatus;
            }
        }
        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
        if (session != null) {

            String channelid = session.getChannelid();

            if (channel == null) {
                return null;
            }
            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        OperatorsManagement oManagementObj = new OperatorsManagement();
                        Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }

                        suTemplate.close();
                        sTemplate.close();
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                    aStatus.error = "INVALID IP REQUEST";
                    aStatus.errorcode = -8;
                    return aStatus;
                }
            }

            int retValue = -1;
            boolean bSentToUser = true;
            String strCategory = "";
            String strSubCategory = "";
            int subcategory = 1;
            if (trustPayLoad != null) {
                MobileTrustSettings mSettings = null;

                Object obj = setManagement.getSetting(sessionid, channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
                if (obj != null) {
                    if (obj instanceof MobileTrustSettings) {
                        mSettings = (MobileTrustSettings) obj;
                    }
                }

                if (mSettings == null) {
                    aStatus.error = "Mobile Trust Setting is not configured!!!";
                    aStatus.errorcode = -23;
//                  
                    return aStatus;
                }

                if (mSettings.bGeoFencing == true) {

                    GeoLocationManagement gManagement = new GeoLocationManagement();
                    AXIOMStatus status = gManagement.validateGeoLocation(sessionid, channelid, credentials.rssUser.userId, trustPayLoad);
                    if (status == null) {
                        aStatus.error = "Location Not Found!!!";
                        aStatus.errorcode = -115;
                    } else if (status.iStatus != 0) {

                        aStatus.error = status.strStatus;
                        aStatus.errorcode = status.iStatus;
                        //     String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
                        audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                                session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorcode,
                                "Token Management", "Invalid Location", null, itemtype,
                                session.getLoginid());

                        return aStatus;
                    }
                }
            }
            PKITokenManagement pManagement = new PKITokenManagement();
            for (int i = 0; i < credentials.tokenDetails.length; i++) {
                if (credentials.tokenDetails[i] != null) {
                    if (credentials.tokenDetails[i].category == AxiomCredentialDetails.PASSWORD) {
                        UserManagement um = new UserManagement();
                        retValue = um.AssignPassword(sessionid, session.getChannelid(), UserId, credentials.tokenDetails[i].Password);
                        if (retValue == 0) {
                            if (bSentToUser == true) {
                                SendNotification send = new SendNotification();
                                AuthUser aUser = um.getUser(sessionid, session.getChannelid(), UserId);
                                Templates temp = null;
                                TemplateManagement tObj = new TemplateManagement();
                                if (subcategory == 1) // sms
                                {
                                    temp = tObj.LoadbyName(sessionid, session.getChannelid(), TemplateNames.MOBILE_USER_PASSWORD_TEMPLATE);
                                }
                                if (temp == null) {
                                    aStatus.error = "Message Template is missing";
                                    aStatus.errorcode = -2;
                                    //return aStatus;
                                }

                                ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
                                String templatebody = (String) TemplateUtils.deserializeFromObject(bais);

                                templatebody = templatebody.replaceAll("#name#", aUser.userName);
                                String date = String.valueOf(new Date());
                                templatebody = templatebody.replaceAll("#channel#", channel.getName());
                                String pass = credentials.tokenDetails[i].Password;
//                           int index = pass.indexOf('$');
                                pass = pass.replace("$", "\\$");

                                templatebody = templatebody.replaceAll("#password#", pass);
                                templatebody = templatebody.replaceAll("#datetime#", date);

                                com.mollatech.axiom.connector.communication.AXIOMStatus axiomStatus = null;

                                int iProductType = Integer.valueOf(LoadSettings.g_sSettings.getProperty("product.type")).intValue();

                                if (temp.getStatus() == tObj.ACTIVE_STATUS) {
                                    axiomStatus = send.SendOnMobile(session.getChannelid(), aUser.phoneNo, templatebody, subcategory, iProductType);
                                }
                                if (axiomStatus != null) {
                                    aStatus.error = axiomStatus.strStatus;
                                    aStatus.errorcode = axiomStatus.iStatus;
                                }
                            } else {
                                aStatus.error = "SUCCESS";
                                aStatus.errorcode = 0;

                            }
                        }
                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                channel.getName(),
                                session.getLoginid(), session.getLoginid(),
                                new Date(),
                                "Assign Password", aStatus.error, aStatus.errorcode,
                                "User Management",
                                "Old Password=******",
                                "New Password=******",
                                "PASSWORD", credentials.rssUser.userId);

                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.CERTIFICATE) {
                        CertificateManagement cm = new CertificateManagement();
                        String cert = cm.GenerateCertificate(sessionid, session.getChannelid(), UserId);
                        if (cert != null) {
                            aStatus.error = "SUCCESS";
                            aStatus.errorcode = 0;
                        } else {
                            aStatus.error = "Failed to issue Certificate";
                            aStatus.errorcode = -9;
                        }

                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                req.getRemoteAddr(),
                                //ipaddress,
                                channel.getName(),
                                session.getLoginid(), session.getLoginid(), new Date(), "Generate Certificate", aStatus.error, aStatus.errorcode,
                                "Certificate Management", "", " ",
                                "PKITOKEN", credentials.rssUser.userId);

                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.CHALLAEGE_RESPONSE) {
                        ChallengeResponsemanagement chManagment = new ChallengeResponsemanagement();
                        AxiomQuestionsAndAnswers axiomQAndA = new AxiomQuestionsAndAnswers();
                        if (credentials.tokenDetails[i].qas.webQAndA == null) {
                            aStatus.error = "No question anser provided";
                            aStatus.errorcode = -7;
                        } else {
                            axiomQAndA.webQAndA = credentials.tokenDetails[i].qas.webQAndA;
                            Questionsandanswers qA = chManagment.getRegisterUser(sessionid, session.getChannelid(), UserId);

                            if (qA != null) {
                                aStatus.error = "User already Registered...";
                                aStatus.errorcode = -6;
                                retValue = -6;
                                return aStatus;
                            } else {

                                retValue = chManagment.RegisterUser(sessionid, session.getChannelid(), UserId, axiomQAndA);
                                if (retValue == 0) {
                                    aStatus.error = "Success";
                                    aStatus.errorcode = 0;
                                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                            req.getRemoteAddr(),
                                            channel.getName(),
                                            session.getLoginid(), session.getLoginid(), new Date(),
                                            "Assign Q&A",
                                            aStatus.error, aStatus.errorcode,
                                            "Q&A Management",
                                            "",
                                            "Added answers for " + axiomQAndA.webQAndA.length + " questions.",
                                            "CHALLENGERESPONSE", UserId);
                                    return aStatus;
                                }
                            }
                        }
                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.SOFTWARE_TOKEN) {
                        strCategory = "SOFTWARE_TOKEN";
                        int cat = OTP_TOKEN_SOFTWARE;
                        int subcat = 0;
                        if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_SOFTWARE_MOBILE) {
                            subcat = OTP_TOKEN_SOFTWARE_MOBILE;
                        } else if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_SOFTWARE_PC) {
                            subcat = OTP_TOKEN_SOFTWARE_PC;
                        } else if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_SOFTWARE_WEB) {
                            subcat = OTP_TOKEN_SOFTWARE_WEB;
                        }

                        //new addtion for license enforcement 
                        int tokenCountCurrentDBState = oManagement.getTokenCountForLicenseCheck(sessionid, channel.getChannelid(), OTP_TOKEN_SOFTWARE);
                        int licensecount = 0; // Axiom Protect fuction call here 
                        //nilesh 28-1
//                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
//                            aStatus.error = "Software Token is not available in this license!!!";
//                            aStatus.errorcode = -100;
//                            return aStatus;
//                        }
//                        licensecount = AxiomProtect.GetSoftwareTokensAllowed();
//                        if (licensecount == -998) {
//                            //unlimited licensing 
//                        } else {
//                            if (tokenCountCurrentDBState >= licensecount) {
//                                aStatus.error = "SOFTWARE Token already reached its limit as per this license!!!";
//                                aStatus.errorcode = -100;
//                                return aStatus;
//                            }
//                        }
                        //niles end 28-1
                        //end of new addition

                        retValue = oManagement.AssignToken(sessionid, session.getChannelid(), credentials.rssUser.userId, cat, subcat, credentials.tokenDetails[i].serialnumber);
                        if (retValue == 0) {
                            aStatus.error = "SUCCESS";
                            aStatus.errorcode = 0;
                            AxiomStatus aStatus1 = this.SendRegistrationCode(session, channel, UserId, OTP_TOKEN, req);
                            if (aStatus1 != null && aStatus1.regCodeMessage != null) {
                                aStatus.regCodeMessage = aStatus1.regCodeMessage;
                            }
                        } else if (retValue == -4) {
                            aStatus.error = "Token already assigned!!!";
                            aStatus.errorcode = -4;
                        } else if (retValue == -3) {
                            aStatus.error = "Empty Serial Number!!!";
                            aStatus.errorcode = -3;
                        } else if (retValue == -2) {
                            aStatus.error = "Invalid Serial Number!!!";
                            aStatus.errorcode = -3;
                        } else if (retValue == -6) {
                            aStatus.error = "Hardware Token could not be assigned!!!";
                            aStatus.errorcode = -6;
                        }

                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.HARDWARE_TOKEN) {

                        int cat = OTP_TOKEN_HARDWARE;
                        int subcat = 0;
                        if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_HARDWARE_CR) {
                            subcat = OTP_TOKEN_HARDWARE_CR;
                        } else if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_HARDWARE_MINI) {
                            subcat = OTP_TOKEN_HARDWARE_MINI;
                        }

                        //new addtion for license enforcement 
                        int tokenCountCurrentDBState = oManagement.getTokenCountForLicenseCheck(sessionid, channel.getChannelid(), OTP_TOKEN_HARDWARE);
                        int licensecount = 0; // Axiom Protect fuction call here 
                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_HARDWARE) != 0) {
                            aStatus.error = "Hardware Token is not available in this license!!!";
                            aStatus.errorcode = -100;
                            return aStatus;
                        }
                        licensecount = AxiomProtect.GetHardwareTokensAllowed();
                        if (licensecount == -998) {
                            //unlimited licensing 
                        } else if (tokenCountCurrentDBState >= licensecount) {
                            aStatus.error = "Hardware Token already reached its limit as per this license!!!";
                            aStatus.errorcode = -100;
                            return aStatus;
                        }
                        //end of new addition

                        retValue = oManagement.AssignToken(sessionid, session.getChannelid(), credentials.rssUser.userId, cat, credentials.tokenDetails[i].subcategory, credentials.tokenDetails[i].serialnumber);
                        if (retValue == 0) {
                            aStatus.error = "SUCCESS";
                            aStatus.errorcode = 0;
                        } else if (retValue == -4) {
                            aStatus.error = "Token already assigned!!!";
                            aStatus.errorcode = -4;
                        } else if (retValue == -3) {
                            aStatus.error = "Empty Serial Number!!!";
                            aStatus.errorcode = -3;
                        } else if (retValue == -2) {
                            aStatus.error = "Invalid Serial Number!!!";
                            aStatus.errorcode = -3;
                        } else if (retValue == -6) {
                            aStatus.error = "Hardware Token could not be assigned!!!";
                            aStatus.errorcode = -6;
                        }
                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.OUTOFBOUND_TOKEN) {

                        int cat = OTP_TOKEN_OUTOFBAND;
                        int subcat = OTP_TOKEN_OUTOFBAND_SMS;
                        if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_OUTOFBAND_SMS) {
                            subcat = OTP_TOKEN_OUTOFBAND_SMS;
                        } else if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_OUTOFBAND_EMAIL) {
                            subcat = OTP_TOKEN_OUTOFBAND_EMAIL;
                        } else if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_OUTOFBAND_USSD) {
                            subcat = OTP_TOKEN_OUTOFBAND_USSD;
                        }

                        //new addtion for license enforcement 
                        int tokenCountCurrentDBState = oManagement.getTokenCountForLicenseCheck(sessionid, channel.getChannelid(), OTP_TOKEN_OUTOFBAND);
                        int licensecount = 0; // Axiom Protect fuction call here 
                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_OOB) != 0) {
                            aStatus.error = "OOB Token is not available in this license!!!";
                            aStatus.errorcode = -100;
                            return aStatus;

                        }

                        licensecount = AxiomProtect.GetOOBTokensAllowed();

                        if (licensecount == -998) {
                            //unlimited licensing 
                        } else if (tokenCountCurrentDBState >= licensecount) {
                            aStatus.error = "OOB Token already reached its limit as per this license!!!";
                            aStatus.errorcode = -100;
                            return aStatus;
                        }
                        //end of new addition

                        retValue = oManagement.AssignToken(sessionid, session.getChannelid(), credentials.rssUser.userId, cat, subcat, credentials.tokenDetails[i].serialnumber);
                        if (retValue == 0) {
                            aStatus.error = "SUCCESS";
                            aStatus.errorcode = 0;
                        } else if (retValue == -4) {
                            aStatus.error = "Token already assigned!!!";
                            aStatus.errorcode = -4;
                        } else if (retValue == -3) {
                            aStatus.error = "Empty Serial Number!!!";
                            aStatus.errorcode = -3;
                        } else if (retValue == -2) {
                            aStatus.error = "Invalid Serial Number!!!";
                            aStatus.errorcode = -3;
                        } else if (retValue == -6) {
                            aStatus.error = "Hardware Token could not be assigned!!!";
                            aStatus.errorcode = -6;
                        }

                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.PKI_SOFTWARE_TOKEN) {
                        strCategory = "SOFTWARE_PKI_TOKEN";
                        int cat = PKITokenManagement.SOFTWARE_TOKEN;

                        //new addtion for license enforcement 
                        //   int tokenCountCurrentDBState = oManagement.getTokenCountForLicenseCheck(sessionid, channel.getChannelid(), OTP_TOKEN_SOFTWARE);
                        int licensecount = 0; // Axiom Protect fuction call here 
                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.PKI_SOFTWARE) != 0) {
                            aStatus.error = "PKI Software Token is not available in this license!!!";
                            aStatus.errorcode = -100;
                            return aStatus;
                        }
                        retValue = pManagement.AssignToken(sessionid, session.getChannelid(), credentials.rssUser.userId, cat, credentials.tokenDetails[i].serialnumber);
                        if (retValue == 0) {
                            aStatus.error = "SUCCESS";
                            aStatus.errorcode = 0;
                        } else if (retValue == -4) {
                            aStatus.error = "PKI Token already assigned!!!";
                            aStatus.errorcode = -4;
                        } else if (retValue == -3) {
                            aStatus.error = "Empty Serial Number!!!";
                            aStatus.errorcode = -3;
                        } else if (retValue == -2) {
                            aStatus.error = "Invalid Serial Number!!!";
                            aStatus.errorcode = -3;
                        } else if (retValue == -6) {
                            aStatus.error = "Hardware Token could not be assigned!!!";
                            aStatus.errorcode = -6;
                        }

                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                req.getRemoteAddr(),
                                channel.getName(),
                                session.getLoginid(), session.getLoginid(), new Date(), "Assign Token", aStatus.error, aStatus.errorcode,
                                "PKI TOKEN Management", "", "Category = " + strCategory
                                + " Serial No =" + credentials.tokenDetails[i].serialnumber,
                                "PKITOKEN", credentials.rssUser.userId);

                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.HARDWARE_TOKEN) {

                        int cat = PKITokenManagement.HARDWARE_TOKEN;

                        //new addtion for license enforcement 
                        int tokenCountCurrentDBState = oManagement.getTokenCountForLicenseCheck(sessionid, channel.getChannelid(), OTP_TOKEN_HARDWARE);
                        int licensecount = 0; // Axiom Protect fuction call here 
                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.PKI_HARDWARE) != 0) {
                            aStatus.error = "PKI Hardware Token is not available in this license!!!";
                            aStatus.errorcode = -100;
                            return aStatus;
                        }

                        retValue = pManagement.AssignToken(sessionid, session.getChannelid(), credentials.rssUser.userId, cat, credentials.tokenDetails[i].serialnumber);
                        if (retValue == 0) {
                            aStatus.error = "SUCCESS";
                            aStatus.errorcode = 0;
                        } else if (retValue == -4) {
                            aStatus.error = "PKI Token already assigned!!!";
                            aStatus.errorcode = -4;
                        } else if (retValue == -3) {
                            aStatus.error = "Empty Serial Number!!!";
                            aStatus.errorcode = -3;
                        } else if (retValue == -2) {
                            aStatus.error = "Invalid Serial Number!!!";
                            aStatus.errorcode = -3;
                        } else if (retValue == -6) {
                            aStatus.error = "PKI Hardware Token could not be assigned!!!";
                            aStatus.errorcode = -6;
                        }

                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                req.getRemoteAddr(),
                                channel.getName(),
                                session.getLoginid(), session.getLoginid(), new Date(), "Assign Token", aStatus.error, aStatus.errorcode,
                                "PKI TOKEN Management", "", "Category = " + strCategory
                                + " Serial No =" + credentials.tokenDetails[i].serialnumber,
                                "PKITOKEN", credentials.rssUser.userId);
                    }
                }

                if (credentials.tokenDetails[i].category == OTP_TOKEN_SOFTWARE) {
                    strCategory = "SOFTWARE_TOKEN";
                } else if (credentials.tokenDetails[i].category == OTP_TOKEN_HARDWARE) {
                    strCategory = "HARDWARE_TOKEN";
                } else if (credentials.tokenDetails[i].category == OTP_TOKEN_OUTOFBAND) {
                    strCategory = "OOB_TOKEN";
                }

//                
                if (credentials.tokenDetails[i].subcategory == OTP_TOKEN_OUTOFBAND_SMS) {
                    strSubCategory = "OOB__SMS_TOKEN";
                } else if (credentials.tokenDetails[i].subcategory == OTP_TOKEN_OUTOFBAND_USSD) {
                    strSubCategory = "OOB__USSD_TOKEN";
                } else if (credentials.tokenDetails[i].subcategory == OTP_TOKEN_OUTOFBAND_VOICE) {
                    strSubCategory = "OOB__VOICE_TOKEN";
                } else if (credentials.tokenDetails[i].subcategory == OTP_TOKEN_OUTOFBAND_EMAIL) {
                    strSubCategory = "OOB__EMAIL_TOKEN";
                } else if (credentials.tokenDetails[i].subcategory == OTP_TOKEN_SOFTWARE_WEB) {
                    strSubCategory = "SW_WEB_TOKEN";
                } else if (credentials.tokenDetails[i].subcategory == OTP_TOKEN_SOFTWARE_MOBILE) {
                    strSubCategory = "SW_MOBILE_TOKEN";
                } else if (credentials.tokenDetails[i].subcategory == OTP_TOKEN_SOFTWARE_PC) {
                    strSubCategory = "SW_PC_TOKEN";
                } else if (credentials.tokenDetails[i].subcategory == OTP_TOKEN_HARDWARE_CR) {
                    strSubCategory = "HW_CR_TOKEN";
                } else if (credentials.tokenDetails[i].subcategory == OTP_TOKEN_HARDWARE_MINI) {
                    strSubCategory = "HW_MINI_TOKEN";
                } else {
                    //AxiomStatus aStatus = new AxiomStatus();
                    aStatus.error = "Invalid subcategory!!!";
                    aStatus.errorcode = -13;
                    return aStatus;
                }
            }

            //SessionManagement sManagement = new SessionManagement();
            //AuditManagement audit = new AuditManagement();
            //MessageContext mc = wsContext.getMessageContext();
            //HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            //Sessions session = sManagement.getSessionById(sessionId);
            //AxiomStatus aStatus = new AxiomStatus();
            // int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;

            if (session == null) {
                aStatus.error = "Invalid/Expired Session";
                aStatus.errorcode = -14;
                return aStatus;
            }

            if (retValue == 0) {

                aStatus.error = "SUCCESS";
                aStatus.errorcode = 0;
            } else if (retValue == -4) {
                aStatus.error = "Token already assigned!!!";
                aStatus.errorcode = -4;
            } else if (retValue == -3) {
                aStatus.error = "Empty Serial Number!!!";
                aStatus.errorcode = -3;
            } else if (retValue == -2) {
                aStatus.error = "Invalid Serial Number!!!";
                aStatus.errorcode = -3;
            } else if (retValue == -6) {
                aStatus.error = "Hardware Token could not be assigned!!!";
                aStatus.errorcode = -6;
            }

        }

        return aStatus;

    }

//    @Override
//    public AxiomStatus AssignCredential(String sessionid, RSSUserCerdentials credentials, String integrityCheckString, String trustPayLoad) {
//
//        String strDebug = null;
//
//        try {
//            System.out.println("AssignCredential::sessionId::" + sessionid);
//            System.out.println("AssignCredential::userid::" + credentials.rssUser.userId);
//            for (int i = 0; i < credentials.tokenDetails.length; i++) {
//                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//
//                    System.out.println("AssignCredential::type::" + credentials.tokenDetails[i].category);
//                    System.out.println("AssignCredential::subtype::" + credentials.tokenDetails[i].subcategory);
//                    System.out.println("AssignCredential::Password::" + credentials.tokenDetails[i].Password);
//                    System.out.println("AssignCredential::Attempts::" + credentials.tokenDetails[i].attempts);
//                    System.out.println("AssignCredential::User Certificate" + credentials.tokenDetails[i].base64Cert);
//                    System.out.println("AssignCredential::Serial Number" + credentials.tokenDetails[i].serialnumber);
//                    System.out.println("AssignCredential::AxiomQuestionAnserChallenge" + credentials.tokenDetails[i].qas);
//
//                }
//            }
//        } catch (Exception ex) {
//        }
//        String UserId = credentials.rssUser.userId;
////        nilesh
//        int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "Licence is invalid";
//            aStatus.errorcode = -100;
//            return aStatus;
//        }
//
//        AxiomStatus aStatus = new AxiomStatus();
//
//        byte[] SHA1hash = null;
//        if (trustPayLoad == null) {
//            SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.rssUser.emailid + credentials.rssUser.userId + credentials.rssUser.phoneNumber + credentials.rssUser.userName);
//        } else {
//            SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.rssUser.emailid + credentials.rssUser.userId + credentials.rssUser.phoneNumber + credentials.rssUser.userName + trustPayLoad);
//        }
////        String integritycheck = new String(Base64.encode(SHA1hash));
//        String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
////        String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java
//        if (integrityCheckString != null) {
//            if (integrityCheckString.equals(integritycheck) != true) {
//                aStatus.error = "Input Data is Tampered!!!";
//                aStatus.errorcode = -76;
//                return aStatus;
//
//            }
//        }
//        SessionManagement sManagement = new SessionManagement();
//        AuditManagement audit = new AuditManagement();
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        Sessions session = sManagement.getSessionById(sessionid);
//
//        if (session == null) {
//            aStatus.error = "Invalid Session";
//            aStatus.errorcode = -14;
//            return aStatus;
//        }
//
//        if (sessionid == null || credentials.rssUser.userId == null
//                || sessionid.isEmpty() == true || credentials.rssUser.userId.isEmpty() == true
//                || credentials.tokenDetails.length < 0) {
//            //AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "Invalid Data";
//            aStatus.errorcode = -11;
//            return aStatus;
//        }
//
//        ChannelManagement cManagement = new ChannelManagement();
//        Channels channel = cManagement.getChannelByID(session.getChannelid());
//        if (channel == null) {
//            aStatus.error = "Invalid Channel";
//            aStatus.errorcode = -15;
//            return aStatus;
//        }
//        SettingsManagement setManagement = new SettingsManagement();
//        ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channel.getChannelid(), SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
//        if (channelProfileObj != null) {
//            int retVal = sManagement.getServerStatus(channelProfileObj);
//            if (retVal != 0) {
//
//                aStatus = new AxiomStatus();
//                aStatus.error = "Server Down for maintainance,Please try later!!!";
//                aStatus.errorcode = -48;
//                return aStatus;
//            }
//        }
//        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//        if (session != null) {
//
//            String channelid = session.getChannelid();
//
//            if (channel == null) {
//                return null;
//            }
//            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//            if (ipobj != null) {
//                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                int checkIp = 1;
//                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                } else {
//                    checkIp = 1;
//                }
//                if (iObj.ipstatus == 0 && checkIp != 1) {
//                    if (iObj.ipalertstatus == 0) {
//                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                        Session sTemplate = suTemplate.openSession();
//                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                        OperatorsManagement oManagementObj = new OperatorsManagement();
//                        Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
//                        if (aOperator != null) {
//                            String[] emailList = new String[aOperator.length - 1];
//                            for (int i = 1; i < aOperator.length; i++) {
//                                emailList[i - 1] = aOperator[i].getEmailid();
//                            }
//                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                String strsubject = templatesObj.getSubject();
//                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                if (strmessageBody != null) {
//                                    // Date date = new Date();
//                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                }
//
//                                SendNotification send = new SendNotification();
//                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
//                            aStatus.error = "INVALID IP REQUEST";
//                            aStatus.errorcode = -8;
//                            return aStatus;
//                        }
//
//                        suTemplate.close();
//                        sTemplate.close();
//                        aStatus.error = "INVALID IP REQUEST";
//                        aStatus.errorcode = -8;
//                        return aStatus;
//                    }
//                    aStatus.error = "INVALID IP REQUEST";
//                    aStatus.errorcode = -8;
//                    return aStatus;
//                }
//            }
//
//            int retValue = -1;
//            boolean bSentToUser = true;
//            String strCategory = "";
//            String strSubCategory = "";
//            int subcategory = 1;
//            if (trustPayLoad != null) {
//                MobileTrustSettings mSettings = null;
//
//                Object obj = setManagement.getSetting(sessionid, channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
//                if (obj != null) {
//                    if (obj instanceof MobileTrustSettings) {
//                        mSettings = (MobileTrustSettings) obj;
//                    }
//                }
//
//                if (mSettings == null) {
//                    aStatus.error = "Mobile Trust Setting is not configured!!!";
//                    aStatus.errorcode = -23;
////                  
//                    return aStatus;
//                }
//
//                if (mSettings.bGeoFencing == true) {
//
//                    GeoLocationManagement gManagement = new GeoLocationManagement();
//                    AXIOMStatus status = gManagement.validateGeoLocation(sessionid, channelid, credentials.rssUser.userId, trustPayLoad);
//                    if (status == null) {
//                        aStatus.error = "Location Not Found!!!";
//                        aStatus.errorcode = -115;
//                    } else if (status.iStatus != 0) {
//
//                        aStatus.error = status.strStatus;
//                        aStatus.errorcode = status.iStatus;
//                        //     String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
//                        audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
//                                session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorcode,
//                                "Token Management", "Invalid Location", null, itemtype,
//                                session.getLoginid());
//
//                        return aStatus;
//                    }
//                }
//            }
//            PKITokenManagement pManagement = new PKITokenManagement();
//            for (int i = 0; i < credentials.tokenDetails.length; i++) {
//                if (credentials.tokenDetails[i] != null) {
//                    if (credentials.tokenDetails[i].category == AxiomCredentialDetails.PASSWORD) {
//                        UserManagement um = new UserManagement();
//                        retValue = um.AssignPassword(sessionid, session.getChannelid(), UserId, credentials.tokenDetails[i].Password);
//                        if (retValue == 0) {
//                            if (bSentToUser == true) {
//                                SendNotification send = new SendNotification();
//                                AuthUser aUser = um.getUser(sessionid, session.getChannelid(), UserId);
//                                Templates temp = null;
//                                TemplateManagement tObj = new TemplateManagement();
//                                if (subcategory == 1) // sms
//                                {
//                                    temp = tObj.LoadbyName(sessionid, session.getChannelid(), TemplateNames.MOBILE_USER_PASSWORD_TEMPLATE);
//                                }
//                                if (temp == null) {
//                                    aStatus.error = "Message Template is missing";
//                                    aStatus.errorcode = -2;
//                                    //return aStatus;
//                                }
//
//                                ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
//                                String templatebody = (String) TemplateUtils.deserializeFromObject(bais);
//
//                                templatebody = templatebody.replaceAll("#name#", aUser.userName);
//                                String date = String.valueOf(new Date());
//                                templatebody = templatebody.replaceAll("#channel#", channel.getName());
//                                String pass = credentials.tokenDetails[i].Password;
////                           int index = pass.indexOf('$');
//                                pass = pass.replace("$", "\\$");
//
//                                templatebody = templatebody.replaceAll("#password#", pass);
//                                templatebody = templatebody.replaceAll("#datetime#", date);
//
//                                com.mollatech.axiom.connector.communication.AXIOMStatus axiomStatus = null;
//
//                                int iProductType = Integer.valueOf(LoadSettings.g_sSettings.getProperty("product.type")).intValue();
//
//                                if (temp.getStatus() == tObj.ACTIVE_STATUS) {
//                                    axiomStatus = send.SendOnMobile(session.getChannelid(), aUser.phoneNo, templatebody, subcategory, iProductType);
//                                }
//                                if (axiomStatus != null) {
//                                    aStatus.error = axiomStatus.strStatus;
//                                    aStatus.errorcode = axiomStatus.iStatus;
//                                }
//                            } else {
//                                aStatus.error = "SUCCESS";
//                                aStatus.errorcode = 0;
//
//                            }
//                        }
//                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
//                                channel.getName(),
//                                session.getLoginid(), session.getLoginid(),
//                                new Date(),
//                                "Assign Password", aStatus.error, aStatus.errorcode,
//                                "User Management",
//                                "Old Password=******",
//                                "New Password=******",
//                                "PASSWORD", credentials.rssUser.userId);
//
//                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.CERTIFICATE) {
//                        CertificateManagement cm = new CertificateManagement();
//                        String cert = cm.GenerateCertificate(sessionid, session.getChannelid(), UserId);
//                        if (cert != null) {
//                            aStatus.error = "SUCCESS";
//                            aStatus.errorcode = 0;
//                        } else {
//                            aStatus.error = "Failed to issue Certificate";
//                            aStatus.errorcode = -9;
//                        }
//
//                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                                req.getRemoteAddr(),
//                                //ipaddress,
//                                channel.getName(),
//                                session.getLoginid(), session.getLoginid(), new Date(), "Generate Certificate", aStatus.error, aStatus.errorcode,
//                                "Certificate Management", "", " ",
//                                "PKITOKEN", credentials.rssUser.userId);
//
//                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.CHALLAEGE_RESPONSE) {
//                        ChallengeResponsemanagement chManagment = new ChallengeResponsemanagement();
//                        AxiomQuestionsAndAnswers axiomQAndA = new AxiomQuestionsAndAnswers();
//                        if (credentials.tokenDetails[i].qas.webQAndA == null) {
//                            aStatus.error = "No question anser provided";
//                            aStatus.errorcode = -7;
//                        } else {
//                            axiomQAndA.webQAndA = credentials.tokenDetails[i].qas.webQAndA;
//                            Questionsandanswers qA = chManagment.getRegisterUser(sessionid, session.getChannelid(), UserId);
//
//                            if (qA != null) {
//                                aStatus.error = "User already Registered...";
//                                aStatus.errorcode = -6;
//                                retValue = -6;
//                                return aStatus;
//                            } else {
//
//                                retValue = chManagment.RegisterUser(sessionid, session.getChannelid(), UserId, axiomQAndA);
//                                if (retValue == 0) {
//                                    aStatus.error = "Success";
//                                    aStatus.errorcode = 0;
//                                    return aStatus;
//                                }
//                            }    //  retValue = uManagement.CreateUser(sessionId, session.getChannelid(), fullname, phone, email);
//                        }//  retValue = uManagement.CreateUser(sessionId, session.getChannelid(), fullname, phone, email);
//
//                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.SOFTWARE_TOKEN) {
//                        strCategory = "SOFTWARE_TOKEN";
//                        int cat = OTP_TOKEN_SOFTWARE;
//                        int subcat = 0;
//                        if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_SOFTWARE_MOBILE) {
//                            subcat = OTP_TOKEN_SOFTWARE_MOBILE;
//                        } else if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_SOFTWARE_PC) {
//                            subcat = OTP_TOKEN_SOFTWARE_PC;
//                        } else if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_SOFTWARE_WEB) {
//                            subcat = OTP_TOKEN_SOFTWARE_WEB;
//                        }
//
//                        //new addtion for license enforcement 
//                        int tokenCountCurrentDBState = oManagement.getTokenCountForLicenseCheck(sessionid, channel.getChannelid(), OTP_TOKEN_SOFTWARE);
//                        int licensecount = 0; // Axiom Protect fuction call here 
//                        //nilesh 28-1
////                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
////                            aStatus.error = "Software Token is not available in this license!!!";
////                            aStatus.errorcode = -100;
////                            return aStatus;
////                        }
////                        licensecount = AxiomProtect.GetSoftwareTokensAllowed();
////                        if (licensecount == -998) {
////                            //unlimited licensing 
////                        } else {
////                            if (tokenCountCurrentDBState >= licensecount) {
////                                aStatus.error = "SOFTWARE Token already reached its limit as per this license!!!";
////                                aStatus.errorcode = -100;
////                                return aStatus;
////                            }
////                        }
//                        //niles end 28-1
//                        //end of new addition
//
//                        retValue = oManagement.AssignToken(sessionid, session.getChannelid(), credentials.rssUser.userId, cat, subcat, credentials.tokenDetails[i].serialnumber);
//                        if (retValue == 0) {
//                            aStatus.error = "SUCCESS";
//                            aStatus.errorcode = 0;
//                            AxiomStatus aStatus1 = this.SendRegistrationCode(session, channel, UserId, OTP_TOKEN, req);
//                            if (aStatus1 != null && aStatus1.regCodeMessage != null) {
//                                aStatus.regCodeMessage = aStatus.regCodeMessage;
//                            }
//                        } else {
//                            if (retValue == -4) {
//                                aStatus.error = "Token already assigned!!!";
//                                aStatus.errorcode = -4;
//                            } else if (retValue == -3) {
//                                aStatus.error = "Empty Serial Number!!!";
//                                aStatus.errorcode = -3;
//                            } else if (retValue == -2) {
//                                aStatus.error = "Invalid Serial Number!!!";
//                                aStatus.errorcode = -3;
//                            } else if (retValue == -6) {
//                                aStatus.error = "Hardware Token could not be assigned!!!";
//                                aStatus.errorcode = -6;
//                            }
//                        }
//
//                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.HARDWARE_TOKEN) {
//
//                        int cat = OTP_TOKEN_HARDWARE;
//                        int subcat = 0;
//                        if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_HARDWARE_CR) {
//                            subcat = OTP_TOKEN_HARDWARE_CR;
//                        } else if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_HARDWARE_MINI) {
//                            subcat = OTP_TOKEN_HARDWARE_MINI;
//                        }
//
//                        //new addtion for license enforcement 
//                        int tokenCountCurrentDBState = oManagement.getTokenCountForLicenseCheck(sessionid, channel.getChannelid(), OTP_TOKEN_HARDWARE);
//                        int licensecount = 0; // Axiom Protect fuction call here 
//                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_HARDWARE) != 0) {
//                            aStatus.error = "Hardware Token is not available in this license!!!";
//                            aStatus.errorcode = -100;
//                            return aStatus;
//                        }
//                        licensecount = AxiomProtect.GetHardwareTokensAllowed();
//                        if (licensecount == -998) {
//                            //unlimited licensing 
//                        } else {
//                            if (tokenCountCurrentDBState >= licensecount) {
//                                aStatus.error = "Hardware Token already reached its limit as per this license!!!";
//                                aStatus.errorcode = -100;
//                                return aStatus;
//                            }
//                        }
//                        //end of new addition
//
//                        retValue = oManagement.AssignToken(sessionid, session.getChannelid(), credentials.rssUser.userId, cat, credentials.tokenDetails[i].subcategory, credentials.tokenDetails[i].serialnumber);
//                        if (retValue == 0) {
//                            aStatus.error = "SUCCESS";
//                            aStatus.errorcode = 0;
//                        } else {
//                            if (retValue == -4) {
//                                aStatus.error = "Token already assigned!!!";
//                                aStatus.errorcode = -4;
//                            } else if (retValue == -3) {
//                                aStatus.error = "Empty Serial Number!!!";
//                                aStatus.errorcode = -3;
//                            } else if (retValue == -2) {
//                                aStatus.error = "Invalid Serial Number!!!";
//                                aStatus.errorcode = -3;
//                            } else if (retValue == -6) {
//                                aStatus.error = "Hardware Token could not be assigned!!!";
//                                aStatus.errorcode = -6;
//                            }
//
//                        }
//                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.OUTOFBOUND_TOKEN) {
//
//                        int cat = OTP_TOKEN_OUTOFBAND;
//                        int subcat = OTP_TOKEN_OUTOFBAND_SMS;
//                        if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_OUTOFBAND_SMS) {
//                            subcat = OTP_TOKEN_OUTOFBAND_SMS;
//                        } else if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_OUTOFBAND_EMAIL) {
//                            subcat = OTP_TOKEN_OUTOFBAND_EMAIL;
//                        } else if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_OUTOFBAND_USSD) {
//                            subcat = OTP_TOKEN_OUTOFBAND_USSD;
//                        }
//
//                        //new addtion for license enforcement 
//                        int tokenCountCurrentDBState = oManagement.getTokenCountForLicenseCheck(sessionid, channel.getChannelid(), OTP_TOKEN_OUTOFBAND);
//                        int licensecount = 0; // Axiom Protect fuction call here 
//                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_OOB) != 0) {
//                            aStatus.error = "OOB Token is not available in this license!!!";
//                            aStatus.errorcode = -100;
//                            return aStatus;
//
//                        }
//
//                        licensecount = AxiomProtect.GetOOBTokensAllowed();
//
//                        if (licensecount == -998) {
//                            //unlimited licensing 
//                        } else {
//                            if (tokenCountCurrentDBState >= licensecount) {
//                                aStatus.error = "OOB Token already reached its limit as per this license!!!";
//                                aStatus.errorcode = -100;
//                                return aStatus;
//                            }
//                        }
//                        //end of new addition
//
//                        retValue = oManagement.AssignToken(sessionid, session.getChannelid(), credentials.rssUser.userId, cat, subcat, credentials.tokenDetails[i].serialnumber);
//                        if (retValue == 0) {
//                            aStatus.error = "SUCCESS";
//                            aStatus.errorcode = 0;
//                        } else {
//                            if (retValue == -4) {
//                                aStatus.error = "Token already assigned!!!";
//                                aStatus.errorcode = -4;
//                            } else if (retValue == -3) {
//                                aStatus.error = "Empty Serial Number!!!";
//                                aStatus.errorcode = -3;
//                            } else if (retValue == -2) {
//                                aStatus.error = "Invalid Serial Number!!!";
//                                aStatus.errorcode = -3;
//                            } else if (retValue == -6) {
//                                aStatus.error = "Hardware Token could not be assigned!!!";
//                                aStatus.errorcode = -6;
//                            }
//
//                        }
//
//                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.PKI_SOFTWARE_TOKEN) {
//                        strCategory = "SOFTWARE_PKI_TOKEN";
//                        int cat = PKITokenManagement.SOFTWARE_TOKEN;
//
//                        //new addtion for license enforcement 
//                        //   int tokenCountCurrentDBState = oManagement.getTokenCountForLicenseCheck(sessionid, channel.getChannelid(), OTP_TOKEN_SOFTWARE);
//                        int licensecount = 0; // Axiom Protect fuction call here 
//                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.PKI_SOFTWARE) != 0) {
//                            aStatus.error = "PKI Software Token is not available in this license!!!";
//                            aStatus.errorcode = -100;
//                            return aStatus;
//                        }
//                        retValue = pManagement.AssignToken(sessionid, session.getChannelid(), credentials.rssUser.userId, cat, credentials.tokenDetails[i].serialnumber);
//                        if (retValue == 0) {
//                            aStatus.error = "SUCCESS";
//                            aStatus.errorcode = 0;
//                        } else {
//                            if (retValue == -4) {
//                                aStatus.error = "PKI Token already assigned!!!";
//                                aStatus.errorcode = -4;
//                            } else if (retValue == -3) {
//                                aStatus.error = "Empty Serial Number!!!";
//                                aStatus.errorcode = -3;
//                            } else if (retValue == -2) {
//                                aStatus.error = "Invalid Serial Number!!!";
//                                aStatus.errorcode = -3;
//                            } else if (retValue == -6) {
//                                aStatus.error = "Hardware Token could not be assigned!!!";
//                                aStatus.errorcode = -6;
//                            }
//                        }
//
//                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                                req.getRemoteAddr(),
//                                channel.getName(),
//                                session.getLoginid(), session.getLoginid(), new Date(), "Assign Token", aStatus.error, aStatus.errorcode,
//                                "PKI TOKEN Management", "", "Category = " + strCategory
//                                + " Serial No =" + credentials.tokenDetails[i].serialnumber,
//                                "PKITOKEN", credentials.rssUser.userId);
//
//                    } else if (credentials.tokenDetails[i].category == AxiomCredentialDetails.HARDWARE_TOKEN) {
//
//                        int cat = PKITokenManagement.HARDWARE_TOKEN;
//
//                        //new addtion for license enforcement 
//                        int tokenCountCurrentDBState = oManagement.getTokenCountForLicenseCheck(sessionid, channel.getChannelid(), OTP_TOKEN_HARDWARE);
//                        int licensecount = 0; // Axiom Protect fuction call here 
//                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.PKI_HARDWARE) != 0) {
//                            aStatus.error = "PKI Hardware Token is not available in this license!!!";
//                            aStatus.errorcode = -100;
//                            return aStatus;
//                        }
//
//                        retValue = pManagement.AssignToken(sessionid, session.getChannelid(), credentials.rssUser.userId, cat, credentials.tokenDetails[i].serialnumber);
//                        if (retValue == 0) {
//                            aStatus.error = "SUCCESS";
//                            aStatus.errorcode = 0;
//                        } else {
//                            if (retValue == -4) {
//                                aStatus.error = "PKI Token already assigned!!!";
//                                aStatus.errorcode = -4;
//                            } else if (retValue == -3) {
//                                aStatus.error = "Empty Serial Number!!!";
//                                aStatus.errorcode = -3;
//                            } else if (retValue == -2) {
//                                aStatus.error = "Invalid Serial Number!!!";
//                                aStatus.errorcode = -3;
//                            } else if (retValue == -6) {
//                                aStatus.error = "PKI Hardware Token could not be assigned!!!";
//                                aStatus.errorcode = -6;
//                            }
//
//                        }
//
//                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
//                                req.getRemoteAddr(),
//                                channel.getName(),
//                                session.getLoginid(), session.getLoginid(), new Date(), "Assign Token", aStatus.error, aStatus.errorcode,
//                                "PKI TOKEN Management", "", "Category = " + strCategory
//                                + " Serial No =" + credentials.tokenDetails[i].serialnumber,
//                                "PKITOKEN", credentials.rssUser.userId);
//                    }
//                }
//
//                if (credentials.tokenDetails[i].category == OTP_TOKEN_SOFTWARE) {
//                    strCategory = "SOFTWARE_TOKEN";
//                } else if (credentials.tokenDetails[i].category == OTP_TOKEN_HARDWARE) {
//                    strCategory = "HARDWARE_TOKEN";
//                } else if (credentials.tokenDetails[i].category == OTP_TOKEN_OUTOFBAND) {
//                    strCategory = "OOB_TOKEN";
//                }
//
////                
//                if (credentials.tokenDetails[i].subcategory == OTP_TOKEN_OUTOFBAND_SMS) {
//                    strSubCategory = "OOB__SMS_TOKEN";
//                } else if (credentials.tokenDetails[i].subcategory == OTP_TOKEN_OUTOFBAND_USSD) {
//                    strSubCategory = "OOB__USSD_TOKEN";
//                } else if (credentials.tokenDetails[i].subcategory == OTP_TOKEN_OUTOFBAND_VOICE) {
//                    strSubCategory = "OOB__VOICE_TOKEN";
//                } else if (credentials.tokenDetails[i].subcategory == OTP_TOKEN_OUTOFBAND_EMAIL) {
//                    strSubCategory = "OOB__EMAIL_TOKEN";
//                } else if (credentials.tokenDetails[i].subcategory == OTP_TOKEN_SOFTWARE_WEB) {
//                    strSubCategory = "SW_WEB_TOKEN";
//                } else if (credentials.tokenDetails[i].subcategory == OTP_TOKEN_SOFTWARE_MOBILE) {
//                    strSubCategory = "SW_MOBILE_TOKEN";
//                } else if (credentials.tokenDetails[i].subcategory == OTP_TOKEN_SOFTWARE_PC) {
//                    strSubCategory = "SW_PC_TOKEN";
//                } else if (credentials.tokenDetails[i].subcategory == OTP_TOKEN_HARDWARE_CR) {
//                    strSubCategory = "HW_CR_TOKEN";
//                } else if (credentials.tokenDetails[i].subcategory == OTP_TOKEN_HARDWARE_MINI) {
//                    strSubCategory = "HW_MINI_TOKEN";
//                } else {
//                    //AxiomStatus aStatus = new AxiomStatus();
//                    aStatus.error = "Invalid subcategory!!!";
//                    aStatus.errorcode = -13;
//                    return aStatus;
//                }
//            }
//
//            //SessionManagement sManagement = new SessionManagement();
//            //AuditManagement audit = new AuditManagement();
//            //MessageContext mc = wsContext.getMessageContext();
//            //HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//            //Sessions session = sManagement.getSessionById(sessionId);
//            //AxiomStatus aStatus = new AxiomStatus();
//            // int retValue = -1;
//            aStatus.error = "ERROR";
//            aStatus.errorcode = retValue;
//
//            if (session == null) {
//                aStatus.error = "Invalid/Expired Session";
//                aStatus.errorcode = -14;
//                return aStatus;
//            }
//
//            if (retValue == 0) {
//
//                aStatus.error = "SUCCESS";
//                aStatus.errorcode = 0;
//            } else {
//                if (retValue == -4) {
//                    aStatus.error = "Token already assigned!!!";
//                    aStatus.errorcode = -4;
//                } else if (retValue == -3) {
//                    aStatus.error = "Empty Serial Number!!!";
//                    aStatus.errorcode = -3;
//                } else if (retValue == -2) {
//                    aStatus.error = "Invalid Serial Number!!!";
//                    aStatus.errorcode = -3;
//                } else if (retValue == -6) {
//                    aStatus.error = "Hardware Token could not be assigned!!!";
//                    aStatus.errorcode = -6;
//                }
//            }
//
//        }
//
//        return aStatus;
//
//    }
    @Override
    public AxiomChallengeResponse getQuestionsForRegistration(String sessionid, int noOfQuestion, String integrityCheckString) throws AxiomException {

        try {

//            nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
               // throw new AxiomException("Licence is invalid");
            }

            AxiomStatus aStatus = new AxiomStatus();

            if (integrityCheckString != null) {
                byte[] SHA1hash = UtilityFunctions.SHA1(sessionid + noOfQuestion);
                String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
                if (integrityCheckString.equals(integritycheck) != true) {
                    aStatus.error = "Input Data is Tampered!!!";
                    aStatus.errorcode = -76;
                    throw new AxiomException("Input Data is Tampered!!!");

                }
            }

            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.USER_CHALLENGERESPONSE) != 0) {
                throw new AxiomException("Challenge Response feature is not available in this license!!!");
            }

            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionid);
            if (session == null) {

                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                //return aStatus;
                throw new AxiomException("Invalid Session!!!");
            }

            UserManagement uManagement = new UserManagement();
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            AxiomQuestionsAndAnswers questions = null;
            AxiomChallengeResponse webQuestions = null;

            if (session != null) {

                //System.out.println("Client IP = " + req.getRemoteAddr());        
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    throw new AxiomException("Invalid Channel!!!");

                }

                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {

                        throw new AxiomException("Server Down for maintainance,Please try later!!!");

                    }
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                throw new AxiomException("Invalid Ip!!!");
                            }

                            suTemplate.close();
                            sTemplate.close();
                            throw new AxiomException("Invalid IP!!!");
                        }
                        throw new AxiomException("Invalid IP!!!");
                    }
                }
                //getUserfrom QAnd A table 
                QuestionsManagement qManagment = new QuestionsManagement();
                questions = qManagment.getRandomQuestions(sessionid, session.getChannelid(), noOfQuestion);

                if (questions.webQAndA == null) {
                    throw new AxiomException("No Questions found!!!!");
                }
                if (questions != null) {
                    webQuestions = new AxiomChallengeResponse();
                    webQuestions.webQAndA = questions.webQAndA;
                    retValue = 0;
                }
                if (retValue == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                }
                //return aStatus;
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            //AuditManagement audit = new AuditManagement();
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                    req.getRemoteAddr(), channel.getName(),
                    session.getLoginid(), session.getLoginid(), new Date(), "GetQuestionForRegistration",
                    aStatus.error, aStatus.errorcode,
                    "Get Random Questions",
                    "",
                    "Sending back count = " + noOfQuestion + " for random questions.",
                    "CHALLENGERESPONSE", ""
            );
            return webQuestions;
        } catch (Exception e) {
            e.printStackTrace();
            throw new AxiomException(e.getMessage());
        }

    }

    @Override
    public AxiomStatus VerifyCredential(String sessionid, String userid, VerifyRequest verifyRequest, String integrityCheckString, String trustPayLoad) throws AxiomException {
        String strDebug = null;
        String strException = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");

            if (verifyRequest == null) {
                AxiomStatus aData = new AxiomStatus();
                aData.errorcode = -11;
                aData.error = "Invalid Data";
            }
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                System.out.println("VerifyCredential::sessionid::" + sessionid);
                System.out.println("VerifyCredential::userid::" + userid);
                System.out.println("VerifyCredential::otp::" + verifyRequest.category);
//                    System.out.println("VerifyOTPAndSignTransaction::rsInfo::" + );
//                    System.out.println("VerifyOTPAndSignTransaction::bNotifyUser::" + bNotifyUser);

            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {

//                AxiomStatus aData = new AxiomStatus();
//                aData.error = "Licence is invalid";
//                aData.errorcode = -100;
//                return aData;
                //throw new AxiomException("Licence is invalid");
            }

            AxiomStatus aData = new AxiomStatus();

            byte[] SHA1hash = null;
            if (trustPayLoad == null) {
                SHA1hash = UtilityFunctions.SHA1(sessionid + userid + verifyRequest.credential + verifyRequest.category);
            } else {
                SHA1hash = UtilityFunctions.SHA1(sessionid + userid + verifyRequest.credential + verifyRequest.category + trustPayLoad);
            }
//            String integritycheck = new String(Base64.encode(SHA1hash));
            String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//            String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java
//            System.out.println("integrity check =" + sessionid + userid + verifyRequest.credential + verifyRequest.category + trustPayLoad);
            if (integrityCheckString != null) {
                if (integrityCheckString.equals(integritycheck) != true) {
                    aData.error = "Input Data is Tampered!!!";
                    aData.errorcode = -76;
                    return aData;

                }
            }

            SessionManagement sManagement = new SessionManagement();
            UserManagement uManagement = new UserManagement();

            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionid);

            int retValue = -1;
            aData.error = "ERROR";
            aData.errorcode = retValue;
            if (session == null) {
                return aData;
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                return aData;
            }
            AuthenticationPackage authPackage = new AuthenticationPackage();
            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
            if (session != null) {
                AuthUser aUser = uManagement.getUser(sessionid, channel.getChannelid(), userid);
                if (aUser == null) {
                    aData.error = "INVALID USER";
                    aData.errorcode = -10;
                    return aData;
                }
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();

                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {

                        aData.error = "Server Down for maintainance,Please try later!!!";
                        aData.errorcode = -48;
                        return aData;
                    }
                }

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                        authPackage.IpCheck = SecurePhraseManagment.SUCCESS_AUTH_PACKAGE;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagementObj = new OperatorsManagement();
                            Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                authPackage.IpCheck = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                                AxiomStatus aStatus = new AxiomStatus();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            authPackage.IpCheck = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                            AxiomStatus aStatus = new AxiomStatus();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        authPackage.IpCheck = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                        AxiomStatus aStatus = new AxiomStatus();
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }

                if (trustPayLoad != null) {
                    MobileTrustSettings mSettings = null;

                    Object obj = setManagement.getSetting(sessionid, channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
                    if (obj != null) {
                        if (obj instanceof MobileTrustSettings) {
                            mSettings = (MobileTrustSettings) obj;
                        }
                    }

                    AxiomStatus aStatus = new AxiomStatus();

                    if (mSettings == null) {
                        aStatus.error = "Mobile Trust Setting is not configured!!!";
                        aStatus.errorcode = -23;                  
                        return aStatus;
                    }

                    if (mSettings.bGeoFencing == true) {

                        GeoLocationManagement gManagement = new GeoLocationManagement();
                        AXIOMStatus status = gManagement.validateGeoLocation(sessionid, channelid, userid, trustPayLoad);
                        if (status == null) {
                            authPackage.GeoCheck = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                            aStatus.error = "Location Not Found!!!";
                            aStatus.errorcode = -115;
                        } else if (status.iStatus == 0) {
                            authPackage.GeoCheck = SecurePhraseManagment.SUCCESS_AUTH_PACKAGE;
                        } else if (status.iStatus != 0) {
                            authPackage.GeoCheck = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                            aStatus.error = status.strStatus;
                            aStatus.errorcode = status.iStatus;
                            //     String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
                            audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                                    session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorcode,
                                    "Token Management", "Invalid Location", null,
                                    itemtype,
                                    session.getLoginid());

                            return aStatus;
                        }
                    }else{
                        authPackage.GeoCheck = SecurePhraseManagment.DISABLE_AUTH_PACKAGE;
                    }
                }else{
                    authPackage.GeoCheck = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                }
                if (verifyRequest.category == VerifyRequest.OTP) {
                    Date dateOfverifyOtp = new Date();
                    retValue = oManagement.VerifyOTP(session.getChannelid(), userid, sessionid, verifyRequest.credential);
                    PDFSigningManagement pManagement = new PDFSigningManagement();
                    String plain = userid + session.getChannelid() + new Date();
                    String archiveid = UtilityFunctions.Bas64SHA1(plain);
                    aData.authPackage = authPackage;
                    if (retValue == 0) {
                        aData.error = "SUCCESS";
                        aData.errorcode = 0;
                    } else if (retValue == -9) {
                        aData.error = "One Time Password is expired...";
                        aData.errorcode = -9;
                    } //added for new error codes 2nd april 2014
                    else if (retValue == -8) {
                        aData.error = "One Time Password is already consumed!!!";
                        aData.errorcode = -16;

                    } else if (retValue == -17) {
                        aData.error = "User/Token is not found!!!";
                        aData.errorcode = -17;

                    } else if (retValue == -22) {
                        aData.error = "Token is locked!!!";
                        aData.errorcode = -22;
                    }

                    //end of addition
                    //System.out.println("VerifyOTP result::" + retValue + " for OTP::" + otp);
                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                            channel.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(),
                            "VERIFY OTP", aData.error, aData.errorcode,
                            "Token Management",
                            "OTP= *******",
                            "",
                            "OTPTOKENS", userid);
                    return aData;
                } else if (verifyRequest.category == VerifyRequest.Password) {
                    AuthUser auserPass = new AuthUser();

                    auserPass = uManagement.verifyPassword(sessionid, session.getChannelid(), userid, verifyRequest.credential);

                    if (auserPass != null) {
                        retValue = 0;
                    } else {
                        retValue = -1;
                    }
                    if (retValue == 0) {
                        aData.error = "SUCCESS";
                        aData.errorcode = 0;
                    } else {
                        aData.error = "Verification Failed";
                        aData.errorcode = -9;
                    }

                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                            channel.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(),
                            "Verify Password", aData.error, aData.errorcode,
                            "User Management",
                            "",
                            "",
                            "PASSWORD", userid);

                    return aData;

                } else if (verifyRequest.category == AxiomCredentialDetails.CHALLAEGE_RESPONSE) {

                    AxiomQuestionsAndAnswers axiomQandA = new AxiomQuestionsAndAnswers();
                    axiomQandA.webQAndA = verifyRequest.qas.webQAndA;

                    ChallengeResponsemanagement chManagment = new ChallengeResponsemanagement();
                    retValue = chManagment.ValidateUserAnswers(sessionid, session.getChannelid(), userid, axiomQandA);

                    if (retValue == 0) {
                        aData.error = "SUCCESS";
                        aData.errorcode = 0;
                    }
                    //return aStatus;

                    //AuditManagement audit = new AuditManagement();
                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(), "ValidateUserAnswers", aData.error, aData.errorcode,
                            "ChallengeResponse Management", "userid =" + userid,
                            "userid = " + userid,
                            "ValidateUserAnswers", "");
                    return aData;

                } else if (verifyRequest.category == VerifyRequest.SignatureOTP) {

                    retValue = oManagement.VerifySignatureOTP(session.getChannelid(), userid, sessionid, verifyRequest.signingData, verifyRequest.credential);

                    if (retValue == 0) {
                        aData.error = "SUCCESS";
                        aData.errorcode = 0;
                    } else if (retValue == -9) {
                        aData.error = "Signature One Time Password is expired";
                        aData.errorcode = -9;
                    } else if (retValue == -2) {
                        aData.error = "Internal Error";
                        aData.errorcode = -2;
                    } else if (retValue == -14) {
                        aData.error = "Invalid Session";
                        aData.errorcode = -14;
                    } else if (retValue == -8) {
                        aData.error = "One Time Password is already consumed!!!";
                        aData.errorcode = -16;

                    } else if (retValue == -17) {
                        aData.error = "User/Token is not found!!!";
                        aData.errorcode = -17;

                    } else if (retValue == -22) {
                        aData.error = "Token is locked!!!";
                        aData.errorcode = -22;
                    }

                    SessionFactoryUtil suSettings = new SessionFactoryUtil(SessionFactoryUtil.settings);
                    Session sSettings = suSettings.openSession();
                    SettingsUtil setUtil = new SettingsUtil(suSettings, sSettings);
                    TokenSettings tSettings = (TokenSettings) setUtil.getSetting(session.getChannelid(), SettingsUtil.Token, 1);
                    sSettings.close();
                    suSettings.close();

                    String txFrom = null;
                    String txTo = null;
                    //System.out.println(tSettings.isbEnforceMasking());
                    if (tSettings.isbEnforceMasking() == true) {
                        txTo = UtilityFunctions.MaskData(verifyRequest.signingData[1]);
                        txFrom = UtilityFunctions.MaskData(verifyRequest.signingData[2]);
                        //txFrom = uFunctions.masking(data[2]);
                    } else {
                        txTo = verifyRequest.signingData[1];
                        txFrom = verifyRequest.signingData[2];
                    }

                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                            channel.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(),
                            "VERIFY SIGNATURE OTP", aData.error, aData.errorcode,
                            "Token Management",
                            "SOTP=" + "********" + ",From=" + txFrom + ",To=" + txTo,
                            "",
                            "OTPTOKENS", userid);

                    return aData;

                } else if (verifyRequest.category == VerifyRequest.verifyData) {
                    CertificateManagement certMngt = new CertificateManagement();
                    Certificates certObj = certMngt.getCertificate(sessionid, channelid, userid);
                    String cert = null;
                    if (certObj != null) {
                        try {
                            cert = certObj.getCertificate();
                            ByteArrayInputStream fis = new ByteArrayInputStream(Base64.decode(certObj.getPfx()));
                            KeyStore ks = KeyStore.getInstance("PKCS12");
                            ks.load(fis, certObj.getPfxpassword().toCharArray());
                            for (Enumeration num = ks.aliases(); num.hasMoreElements();) {
                                String alias = (String) num.nextElement();
                                if (ks.isKeyEntry(alias)) {
                                    CryptoManager cmObj = new CryptoManager();
                                    //nilesh
                                    char[] aliasPassPhrase = certObj.getPfxpassword().toCharArray();
                                    KeyPair keyPair = getPrivateKey(ks, alias, aliasPassPhrase);
                                    String signature = verifyRequest.pkisignData;
                                    String strData = verifyRequest.pkiplainData;
                                    signature = signature.replaceAll("(\\r|\\n|\\t)", "");
                                    byte[] arrSigndata = null;
                                    boolean bResult = false;
                                    try {
                                        arrSigndata = hexStringToByteArray(signature);
                                        bResult = cmObj.verifyDataRSA1(strData.getBytes(), arrSigndata, keyPair.getPublic(), CryptoManager.SHA1WithRSA);
                                    } catch (Exception ex) {
                                    }
                                    //end nilesh
                                    if (bResult != true) {
                                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                                req.getRemoteAddr(), channel.getName(),
                                                session.getLoginid(), session.getLoginid(), new Date(),
                                                "PKI Sign Data", "Failed To Sign", -1,
                                                "PKI Token Management",
                                                "",
                                                "Failed To Sign Data",
                                                "PKITOKEN", userid);
                                        AxiomStatus aStatus1 = new AxiomStatus();
                                        aStatus1.error = "Invalid Data!!!";
                                        aStatus1.errorcode = -99;
                                        return aStatus1;
                                    } else {
                                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                                                req.getRemoteAddr(), channel.getName(),
                                                session.getLoginid(), session.getLoginid(), new Date(),
                                                "PKI Sign Data", "Success To Sign", 0,
                                                "PKI Token Management",
                                                "",
                                                "Data Sign Successfully",
                                                "PKITOKEN", userid);
                                        AxiomStatus aStatus1 = new AxiomStatus();
                                        aStatus1.error = "Valid Data!!!";
                                        aStatus1.errorcode = 0;
                                        return aStatus1;
                                    }
                                }
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            strException = ex.getMessage();

        }
        AxiomStatus aStatus1 = new AxiomStatus();
        aStatus1.error = strException;
        aStatus1.errorcode = -99;
        return aStatus1;
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public KeyPair getPrivateKey(KeyStore keystore, String alias, char[] password) {
        try {
            // Get private key
            Key key = keystore.getKey(alias, password);
            if (key instanceof PrivateKey) {
                // Get certificate of public key
                Certificate cert = keystore.getCertificate(alias);
                // Get public key
                PublicKey publicKey = cert.getPublicKey();
                // Return a key pair
                System.out.println("(PrivateKey) key" + (PrivateKey) key);
                return new KeyPair(publicKey, (PrivateKey) key);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public AxiomData VerifySignature(String sessionid, String userid, String dataToSign, String envelopedData, int docType, String integrityCheckString, String trustPayLoad) {

        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                System.out.println("VerifySignature::sessionid::" + sessionid);
                System.out.println("VerifySignature::userid::" + userid);
                System.out.println("VerifySignature::SignedData::" + dataToSign);
                System.out.println("VerifySignature::Signature::" + envelopedData);
                System.out.println("VerifySignature::docType::" + docType);
            }
        } catch (Exception ex) {
        }
//nilesh
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
//                AxiomStatus aStatus = new AxiomStatus();
//                aStatus.error = "Licence is invalid";
//                aStatus.errorcode = -100;
//            AxiomData aData = new AxiomData();
//            aData.sErrorMessage = "Licence is invalid";
//            aData.iErrorCode = -100;
//            return aData;
            //throw new AxiomException("Licence is invalid");
        }

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.PKI_CERTIFICATE) != 0) {
            AxiomData aData = new AxiomData();
            aData.sErrorMessage = "Digital Certificate feature is not available in this license!!!";
            aData.iErrorCode = -100;
            //aStatus.error = "Digital Certificate feature is not available in this license!!!";
            //aStatus.errorcode = -101;
            return aData;
        }

        AxiomData aData = new AxiomData();

        byte[] SHA1hash = null;
        if (trustPayLoad == null) {
            SHA1hash = UtilityFunctions.SHA1(sessionid + userid + dataToSign + envelopedData + docType);
        } else {
            SHA1hash = UtilityFunctions.SHA1(sessionid + userid + dataToSign + envelopedData + docType + trustPayLoad);
        }

//        String integritycheck = new String(Base64.encode(SHA1hash));
        String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//        String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java
        if (integrityCheckString != null) {
            if (integrityCheckString.equals(integritycheck) != true) {
                aData.sErrorMessage = "Input Data is Tampered!!!";
                aData.iErrorCode = -76;
                return aData;

            }
        }

        try {

            SessionManagement sManagement = new SessionManagement();
            UserManagement uManagement = new UserManagement();
            CryptoManagement cyManagement = new CryptoManagement();

            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionid);

            int retValue = -1;
            aData.sErrorMessage = "ERROR";
            aData.iErrorCode = retValue;
            if (session == null) {
                aData.sErrorMessage = "Invalid Session";
                aData.iErrorCode = -14;
                return aData;
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                aData.sErrorMessage = "Invalid Channel";
                aData.iErrorCode = -15;
                return aData;
            }

            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
            if (channelProfileObj != null) {
                int retVal = sManagement.getServerStatus(channelProfileObj);
                if (retVal != 0) {

                    aData.sErrorMessage = "Server Down for maintainance,Please try later!!!";
                    aData.iErrorCode = -48;
                    return aData;
                }
            }

            if (session != null) {
                AuthUser aUser = uManagement.getUser(sessionid, channel.getChannelid(), userid);
                if (aUser == null) {
                    aData.sErrorMessage = "INVALID USER";
                    aData.iErrorCode = -10;
                    return aData;
                }

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                AxiomData aStatus = new AxiomData();
                                aStatus.sErrorMessage = "INVALID IP REQUEST";
                                aStatus.iErrorCode = -8;
                                return aStatus;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            AxiomData aStatus = new AxiomData();
                            aStatus.sErrorMessage = "INVALID IP REQUEST";
                            aStatus.iErrorCode = -8;
                            return aStatus;
                        }
                        AxiomData aStatus = new AxiomData();
                        aStatus.sErrorMessage = "INVALID IP REQUEST";
                        aStatus.iErrorCode = -8;
                        return aStatus;
                    }
                }

                if (trustPayLoad != null) {
                    MobileTrustSettings mSettings = null;

                    Object obj = setManagement.getSetting(sessionid, channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
                    if (obj != null) {
                        if (obj instanceof MobileTrustSettings) {
                            mSettings = (MobileTrustSettings) obj;
                        }
                    }

                    AxiomData aStatus = new AxiomData();

                    if (mSettings == null) {
                        aStatus.sErrorMessage = "Mobile Trust Setting is not configured!!!";
                        aStatus.iErrorCode = -23;
//                  
                        return aStatus;
                    }

                    if (mSettings.bGeoFencing == true) {

                        GeoLocationManagement gManagement = new GeoLocationManagement();
                        AXIOMStatus status = gManagement.validateGeoLocation(sessionid, channelid, userid, trustPayLoad);
                        if (status == null) {
                            aStatus.sErrorMessage = "Location Not Found!!!";
                            aStatus.iErrorCode = -115;
                        } else if (status.iStatus != 0) {

                            aStatus.sErrorMessage = status.strStatus;
                            aStatus.iErrorCode = status.iStatus;
                            //     String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
                            audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                                    session.getLoginid(), new Date(), "Change Token Status", aStatus.sErrorMessage, aStatus.iErrorCode,
                                    "Token Management", "Invalid Location", null, itemtype,
                                    session.getLoginid());

                            return aStatus;
                        }
                    }
                }
                CertificateManagement certManagement = new CertificateManagement();
                Certificates cert = certManagement.getCertificate(sessionid, session.getChannelid(), userid);
                if (cert == null) {
                    aData.sErrorMessage = "Certificate Not Issued!!";
                    aData.iErrorCode = -7;
                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(), "Get Certificate", aData.sErrorMessage, aData.iErrorCode,
                            "Certificate Management", "", "Certificate Not Found",
                            "Get Certificate", userid);
                    return aData;
                }

                retValue = cyManagement.VerifySignature1(dataToSign, envelopedData, cert.getCertificate());
                if (retValue == 0) {
                    aData.sErrorMessage = "Success";
                    aData.iErrorCode = retValue;
                }

            }
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                    session.getLoginid(), session.getLoginid(), new Date(), "Verify Signature ", aData.sErrorMessage, aData.iErrorCode,
                    "Verify Signature", "signature = ******", " signature = ******",
                    "Verify Signature", userid);
            return aData;

        } catch (Exception e) {
            e.printStackTrace();
            aData.sErrorMessage = "Exception::" + e.getMessage();
            aData.iErrorCode = -20;
            return aData;
        }

    }

    @Override
    public AxiomData ValidateUserAndSignTransaction(String sessionid, String userid, VerifyRequest verifyRequest, RemoteSigningInfo rsInfo, boolean bNotifyUser, String integrityCheckString, String trustPayLoad) throws AxiomException {
        String strDebug = null;
        String strException = null;
        String ratio = null;
        String referenceid = null;

        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");

            if (verifyRequest == null) {
                AxiomData aData = new AxiomData();
                aData.iErrorCode = -11;
                aData.sErrorMessage = "Invalid Data";
            }
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                System.out.println("VerifyCredential::sessionid::" + sessionid);
                System.out.println("VerifyCredential::userid::" + userid);
                System.out.println("VerifyCredential::otp::" + verifyRequest.category);
//                    System.out.println("VerifyOTPAndSignTransaction::rsInfo::" + );
//                    System.out.println("VerifyOTPAndSignTransaction::bNotifyUser::" + bNotifyUser);

            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
//                AxiomStatus aStatus = new AxiomStatus();
//                aStatus.error = "Licence is invalid";
//                aStatus.errorcode = -100;
//                AxiomData aData = new AxiomData();
//                aData.sErrorMessage = "Licence is invalid";
//                aData.iErrorCode = -100;
//                return aData;
                //throw new AxiomException("Licence is invalid");
            }

            AxiomData aData = new AxiomData();

            byte[] SHA1hash = null;
            if (trustPayLoad == null) {
                SHA1hash = UtilityFunctions.SHA1(sessionid + userid + verifyRequest.credential + verifyRequest.category + rsInfo.clientLocation
                        + rsInfo.dataToSign + rsInfo.designation + rsInfo.name + rsInfo.reason + rsInfo.referenceId + rsInfo.bAddTimestamp
                        + rsInfo.bNotifyUser + rsInfo.bReturnSignedDocument + rsInfo.type + bNotifyUser);
            } else {
                SHA1hash = UtilityFunctions.SHA1(sessionid + userid + verifyRequest.credential + verifyRequest.category + rsInfo.clientLocation
                        + rsInfo.dataToSign + rsInfo.designation + rsInfo.name + rsInfo.reason + rsInfo.referenceId + rsInfo.bAddTimestamp
                        + rsInfo.bNotifyUser + rsInfo.bReturnSignedDocument + rsInfo.type + bNotifyUser + trustPayLoad);
            }

//            String integritycheck = new String(Base64.encode(SHA1hash));
            String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//            String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java
            if (integrityCheckString != null) {
                if (integrityCheckString.equals(integritycheck) == false) {
                    aData.sErrorMessage = "Input Data is Tampered!!!";
                    aData.iErrorCode = -76;
                    return aData;

                }
            }
            SessionManagement sManagement = new SessionManagement();
            UserManagement uManagement = new UserManagement();

            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionid);

            int retValue = -1;
            aData.sErrorMessage = "ERROR";
            aData.iErrorCode = retValue;
            if (session == null) {
                return aData;
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                return aData;
            }

            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
            if (channelProfileObj != null) {
                int retVal = sManagement.getServerStatus(channelProfileObj);
                if (retVal != 0) {

                    aData.sErrorMessage = "Server Down for maintainance,Please try later!!!";
                    aData.iErrorCode = -48;
                    return aData;
                }
            }

            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
            if (session != null) {
                AuthUser aUser = uManagement.getUser(sessionid, channel.getChannelid(), userid);
                if (aUser == null) {
                    aData.sErrorMessage = "INVALID USER";
                    aData.iErrorCode = -10;
                    return aData;
                }

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagementObj = new OperatorsManagement();
                            Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                AxiomData aStatus = new AxiomData();
                                aStatus.sErrorMessage = "INVALID IP REQUEST";
                                aStatus.iErrorCode = -8;
                                return aStatus;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            AxiomData aStatus = new AxiomData();
                            aStatus.sErrorMessage = "INVALID IP REQUEST";
                            aStatus.iErrorCode = -8;
                            return aStatus;
                        }
                        AxiomData aStatus = new AxiomData();
                        aStatus.sErrorMessage = "INVALID IP REQUEST";
                        aStatus.iErrorCode = -8;
                        return aStatus;
                    }
                }

                if (trustPayLoad != null) {
                    MobileTrustSettings mSettings = null;

                    Object obj = setManagement.getSetting(sessionid, channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
                    if (obj != null) {
                        if (obj instanceof MobileTrustSettings) {
                            mSettings = (MobileTrustSettings) obj;
                        }
                    }

                    AxiomData aStatus = new AxiomData();

                    if (mSettings == null) {
                        aStatus.sErrorMessage = "Mobile Trust Setting is not configured!!!";
                        aStatus.iErrorCode = -23;
//                  
                        return aStatus;
                    }

                    if (mSettings.bGeoFencing == true) {

                        GeoLocationManagement gManagement = new GeoLocationManagement();
                        AXIOMStatus status = gManagement.validateGeoLocation(sessionid, channelid, userid, trustPayLoad);
                        if (status == null) {
                            aStatus.sErrorMessage = "Location Not Found!!!";
                            aStatus.iErrorCode = -115;
                        } else if (status.iStatus != 0) {

                            aStatus.sErrorMessage = status.strStatus;
                            aStatus.iErrorCode = status.iStatus;
                            //     String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
                            audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                                    session.getLoginid(), new Date(), "Change Token Status", aStatus.sErrorMessage, aStatus.iErrorCode,
                                    "Token Management", "Invalid Location", null, itemtype,
                                    session.getLoginid());

                            return aStatus;
                        }
                    }
                }
                if (verifyRequest.category == VerifyRequest.OTP) {
                    Date dateOfverifyOtp = new Date();
                    AuthUser auth = new AuthUser();
                    retValue = oManagement.VerifyOTP(channelid, userid, sessionid, verifyRequest.credential);

                    if (retValue == 0) {
                        aData.sErrorMessage = "SUCCESS";
                        aData.iErrorCode = 0;
                    } else if (retValue == -9) {
                        aData.sErrorMessage = "One Time Password is expired...";
                        aData.iErrorCode = -9;
                    } //added for new error codes 2nd april 2014
                    else if (retValue == -8) {
                        aData.sErrorMessage = "One Time Password is already consumed!!!";
                        aData.iErrorCode = -16;

                    } else if (retValue == -17) {
                        aData.sErrorMessage = "User/Token is not found!!!";
                        aData.iErrorCode = -17;

                    } else if (retValue == -22) {
                        aData.sErrorMessage = "Token is locked!!!";
                        aData.iErrorCode = -22;
                    }

                    //end of addition
                    //System.out.println("VerifyOTP result::" + retValue + " for OTP::" + otp);
                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                            channel.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(),
                            "VERIFY OTP", aData.sErrorMessage, aData.iErrorCode,
                            "Token Management",
                            "OTP= *******",
                            "",
                            "OTPTOKENS", userid);

                } else if (verifyRequest.category == VerifyRequest.Password) {

                    aUser = uManagement.verifyPassword(sessionid, session.getChannelid(), userid, verifyRequest.credential);

                    if (retValue == 0) {
                        aData.sErrorMessage = "SUCCESS";
                        aData.iErrorCode = 0;
                    } else {
                        aData.sErrorMessage = "Verification Failed";
                        aData.iErrorCode = -9;
                    }

                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                            channel.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(),
                            "Verify Password", aData.sErrorMessage, aData.iErrorCode,
                            "User Management",
                            "",
                            "",
                            "PASSWORD", userid);

                } else if (verifyRequest.category == VerifyRequest.ChallengeAndResponse) {

                    AxiomQuestionsAndAnswers axiomQandA = new AxiomQuestionsAndAnswers();
                    axiomQandA.webQAndA = verifyRequest.qas.webQAndA;

                    ChallengeResponsemanagement chManagment = new ChallengeResponsemanagement();
                    retValue = chManagment.ValidateUserAnswers(sessionid, session.getChannelid(), userid, axiomQandA);
                    ratio = chManagment.getCRValidationRatio(sessionid, session.getChannelid(), userid, axiomQandA);
                    if (retValue == 0) {
                        aData.sErrorMessage = "SUCCESS";
                        aData.iErrorCode = 0;
                    }
                    //return aStatus;

                    //AuditManagement audit = new AuditManagement();
                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(), "ValidateUserAnswers", aData.sErrorMessage, aData.iErrorCode,
                            "ChallengeResponse Management", "userid =" + userid,
                            "userid = " + userid,
                            "ValidateUserAnswers", "");

                } else if (verifyRequest.category == VerifyRequest.SignatureOTP) {

                    retValue = oManagement.VerifySignatureOTP(session.getChannelid(), userid, sessionid, verifyRequest.signingData, verifyRequest.credential);

                    if (retValue == 0) {
                        aData.sErrorMessage = "SUCCESS";
                        aData.iErrorCode = 0;
                    } else if (retValue == -9) {
                        aData.sErrorMessage = "Signature One Time Password is expired";
                        aData.iErrorCode = -9;
                    } else if (retValue == -2) {
                        aData.sErrorMessage = "Internal Error";
                        aData.iErrorCode = -2;
                    } else if (retValue == -14) {
                        aData.sErrorMessage = "Invalid Session";
                        aData.iErrorCode = -14;
                    } else if (retValue == -8) {
                        aData.sErrorMessage = "One Time Password is already consumed!!!";
                        aData.iErrorCode = -16;

                    } else if (retValue == -17) {
                        aData.sErrorMessage = "User/Token is not found!!!";
                        aData.iErrorCode = -17;

                    } else if (retValue == -22) {
                        aData.sErrorMessage = "Token is locked!!!";
                        aData.iErrorCode = -22;
                    }

                    SessionFactoryUtil suSettings = new SessionFactoryUtil(SessionFactoryUtil.settings);
                    Session sSettings = suSettings.openSession();
                    SettingsUtil setUtil = new SettingsUtil(suSettings, sSettings);
                    TokenSettings tSettings = (TokenSettings) setUtil.getSetting(session.getChannelid(), SettingsUtil.Token, 1);
                    sSettings.close();
                    suSettings.close();

                    String txFrom = null;
                    String txTo = null;
                    //System.out.println(tSettings.isbEnforceMasking());
                    if (tSettings.isbEnforceMasking() == true) {
                        txTo = UtilityFunctions.MaskData(verifyRequest.signingData[1]);
                        txFrom = UtilityFunctions.MaskData(verifyRequest.signingData[2]);
                        //txFrom = uFunctions.masking(data[2]);
                    } else {
                        txTo = verifyRequest.signingData[1];
                        txFrom = verifyRequest.signingData[2];
                    }

                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                            channel.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(),
                            "VERIFY SIGNATURE OTP", aData.sErrorMessage, aData.iErrorCode,
                            "Token Management",
                            "SOTP=" + "********" + ",From=" + txFrom + ",To=" + txTo,
                            "",
                            "OTPTOKENS", userid);

                }

                PDFSigningManagement pManagement = new PDFSigningManagement();
                String plain = userid + session.getChannelid() + new Date();
                String archiveid = UtilityFunctions.Bas64SHA1(plain);
                //  referenceid="REF102030401";
                if (rsInfo.referenceId == null) {
                    String refData = plain + archiveid + new Date();
                    referenceid = UtilityFunctions.Bas64SHA1(refData);

                } else {
                    referenceid = rsInfo.referenceId;
                }

//                    PDFSigningManagement pManagement = new PDFSigningManagement();
//                String plain = userid + session.getChannelid() + new Date();
//                String archiveid = UtilityFunctions.Bas64SHA1(plain);
                if (retValue == 0) {
                    CertificateManagement certManagement = new CertificateManagement();
                    Certificates cert = certManagement.getCertificate(sessionid, session.getChannelid(), userid);

                    if (cert == null) {
                        aData.sErrorMessage = "Certificate Not Issued!!";
                        aData.iErrorCode = -27;

                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                                session.getLoginid(), session.getLoginid(), new Date(), "Validate User And Sign Transaction ", aData.sErrorMessage, aData.iErrorCode,
                                "Validate User And Sign Transaction Management", "data = ******, questions = ******", " signdata = ******,questions = ******",
                                "Validate User TOKEN And Sign Transaction", userid);
                        return aData;
                    }

                    String pfxFile = cert.getPfx();
                    String pfxPassword = cert.getPfxpassword();
                    CryptoManagement cyManagement = new CryptoManagement();
                    // String pfxFile = certManagement.sendPFXFile(sessionid, session.getChannelid(), userid);
                    //String pfxPassword = certManagement.getPFXPassWord(sessionid, session.getChannelid(), userid);
                    //call signData() here 
                    boolean sResult = false;
                    String signature = null;
                    int result = -1;
                    if (rsInfo.type == RemoteSigningInfo.RAW_DATA) {
                        signature = cyManagement.SignData(rsInfo.dataToSign, pfxFile, pfxPassword); // signData();
                        if (signature != null) {
                            result = 0;
                        }
                        if (verifyRequest.category == VerifyRequest.ChallengeAndResponse) {
                            retValue = pManagement.addRemoteSignature(sessionid, session.getChannelid(), userid, archiveid, ratio, retValue, new Date(), rsInfo.type, rsInfo.dataToSign, signature, result, new Date(), referenceid);
                        } else {
                            retValue = pManagement.addRemoteSignature(sessionid, session.getChannelid(), userid, archiveid, verifyRequest.credential, retValue, new Date(), rsInfo.type, rsInfo.dataToSign, signature, result, new Date(), referenceid);
                        }

                        if (retValue != 0) {

                        }
                    } else if (rsInfo.type == RemoteSigningInfo.PDF) {

                        Date d = new Date();
                        String sourceFileName = userid + d.getTime();
                        String filepath = LoadSettings.g_sSettings.getProperty("remote.sign.archive") + sourceFileName + ".pdf";
                        sResult = pManagement.Base64ToPDF(rsInfo.dataToSign, filepath);
                        // String destpath = 
                        String destFileName = LoadSettings.g_sSettings.getProperty("remote.sign.archive") + sourceFileName + "-sign.pdf";

                        sResult = pManagement.signPdf(userid, filepath, destFileName, pfxFile, pfxPassword, rsInfo, null);
                        if (sResult == true) {
                            signature = pManagement.fileToBase64(destFileName);
                        }

                        if (verifyRequest.category == VerifyRequest.ChallengeAndResponse) {
                            pManagement.addRemoteSignature(sessionid, session.getChannelid(), userid, archiveid, ratio, retValue, new Date(), rsInfo.type, filepath, destFileName, result, new Date(), referenceid);
                        } else {
                            pManagement.addRemoteSignature(sessionid, session.getChannelid(), userid, archiveid, verifyRequest.credential, retValue, new Date(), rsInfo.type, filepath, destFileName, result, new Date(), referenceid);
                        }
                    }
                    aData.archiveid = archiveid;
                    aData.signature = signature;
                    aData.referenceid = referenceid;
                    aData.sErrorMessage = "SUCCESS";
                    aData.iErrorCode = 0;
                } else if (verifyRequest.category == VerifyRequest.ChallengeAndResponse) {
                    //  pManagement.addRemoteSignature(sessionid, session.getChannelid(), userid, archiveid, ratio, retValue, new Date(), rsInfo.type, rsInfo.dataToSign, null, 0, null,referenceid);
                } else {
                    //   pManagement.addRemoteSignature(sessionid, session.getChannelid(), userid, archiveid, verifyRequest.credential, retValue, new Date(), rsInfo.type, rsInfo.dataToSign, null, 0, null,referenceid);
                }
            }

            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                    session.getLoginid(), session.getLoginid(), new Date(), "Validate User And Sign Transaction ", aData.sErrorMessage, aData.iErrorCode,
                    "Validate User And Sign Transaction Management", "data = ******, questions = ******", " signdata = ******,questions = ******",
                    "Validate User TOKEN And Sign Transaction", userid);

            SendNotification sNotification = new SendNotification();
            PushNotificationDeviceManagement pManagement = new PushNotificationDeviceManagement();
            String pushMessage = null;
            if (aData.iErrorCode == 0) {
                pushMessage = "You have transfered amount from your account " + verifyRequest.signingData[1] + "  to  beneficiary's account : " + verifyRequest.signingData[2] + " for amount of " + verifyRequest.signingData[0];
            } else {
                pushMessage = "Your transtion has failed from your account " + verifyRequest.signingData[1] + "  to  beneficiary's account : " + verifyRequest.signingData[2] + " for amount of " + verifyRequest.signingData[0];
                //pushMessage = "You are transferring amount from the specified account no : " + verifyRequest.signingData[1] + "  to the destination account : " + verifyRequest.signingData[2] + " and the amount you specified for transaction is :  " + verifyRequest.signingData[0] + " is failed to transferred. ";;
            }

            Registerdevicepush rDevice = pManagement.GetRegisterPushDeviceByUserID(sessionid, channelid, userid);
            if (rDevice != null) {
                sNotification.SendOnPush(channelid, rDevice.getGoogleregisterid(), rDevice.getGooglemeailid(), pushMessage, ANDROID);
            }

            return aData;

        } catch (Exception ex) {
            ex.printStackTrace();
            strException = ex.getMessage();

        }
        AxiomData aStatus1 = new AxiomData();
        aStatus1.sErrorMessage = strException;
        aStatus1.iErrorCode = -99;
        return aStatus1;
    }

    @Override
    public String GenerateSignatureCode(String sessionid, String[] data, String integrityCheckString) throws AxiomException {

        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                System.out.println("GenerateSignatureCode::sessionid::" + sessionid);
                System.out.println("GenerateSignatureCode::data::" + data);
            }
        } catch (Exception ex) {
        }
//nilesh
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Licence invalid";
            aStatus.errorcode = -100;
            throw new AxiomException("Licence invalid");

        }

        AxiomStatus aStatus = new AxiomStatus();
        String strData = "";
        for (int i = 0; i < data.length; i++) {
            if (strData == null) {
                strData = data[i];
            } else {
                strData = strData + data[i];
            }
        }
        byte[] SHA1hash = UtilityFunctions.SHA1(sessionid + strData);
//        String integritycheck = new String(Base64.encode(SHA1hash));
        String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//        String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java

        if (integrityCheckString != null) {
            if (integrityCheckString.equals(integritycheck) != true) {
                aStatus.error = "Input Data is Tampered!!!";
                aStatus.errorcode = -76;
                throw new AxiomException("Input Data is Tampered!!!");

            }
        }

        try {

            int retValue = -10;

            aStatus.error = "Invalid Parameters!!!";
            aStatus.errorcode = retValue;

            if (sessionid == null) {
                throw new AxiomException("Invalid Parameters!!!");
            }

            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionid);
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            if (session == null) {
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -104;
                throw new AxiomException("Invalid Session");
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                aStatus.error = "Invalid Channel";
                aStatus.errorcode = -3;
                throw new AxiomException("Invalid Channel");
            }

            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
            if (channelProfileObj != null) {
                int retVal = sManagement.getServerStatus(channelProfileObj);
                if (retVal != 0) {

                    aStatus = new AxiomStatus();
                    aStatus.error = "Server Down for maintainance,Please try later!!!";
                    aStatus.errorcode = -48;

                    throw new AxiomException("Server Down for maintainance,Please try later!!!");
                }
            }

            if (session != null) {

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagementObj = new OperatorsManagement();
                            Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP";
                                aStatus.errorcode = -108;
                                throw new AxiomException("INVALID IP");

                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP";
                            aStatus.errorcode = -108;
                            throw new AxiomException("INVALID IP");
                        }
                        aStatus.error = "INVALID IP";
                        aStatus.errorcode = -108;
                        throw new AxiomException("INVALID IP");
                    }
                }

                RSSUtils rUtils = new RSSUtils();
                String concatData = null;
                if (data != null) {
                    for (int i = 0; i < data.length; i++) {
                        if (concatData != null) {
                            concatData = concatData + data[i];
                        } else {
                            concatData = data[i];

                        }
                    }
                    long signaturecode = 00;
                    if (concatData != null) {
                        signaturecode = rUtils.CRC32CheckSum(concatData.getBytes());
                    }
                    String signature = signaturecode + "";

                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(), "RSS Generate Signature Code", aStatus.error, aStatus.errorcode,
                            "Security Management", aStatus.error, " ",
                            itemtype, "");
                    return signature;

                    //return aStatus;
                    //end of addition
                } else {

                    aStatus.error = "No Data Available";
                    aStatus.errorcode = -55;
                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(), "RSS Generate Signature Code", aStatus.error, aStatus.errorcode,
                            "Security Management", aStatus.error, " ",
                            itemtype, "");
                }

            }

        } catch (Exception e) {
            throw new AxiomException(e.getMessage());
        }
        return null;

    }

    @Override
    public String EnforceSecurity(String sessionid, String userid, String data, String integrityCheckString, String trustPayLoad) throws AxiomException {

        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                System.out.println("EnforceSecurity::sessionid::" + sessionid);
                System.out.println("EnforceSecurity::userid::" + userid);
                System.out.println("EnforceSecurity::data::" + data);
            }
        } catch (Exception ex) {
        }
//nilesh
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Licence invalid";
            aStatus.errorcode = -100;
            throw new AxiomException("Licence invalid");

        }

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.MOBILETRUST) != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Mobile Trust feature is not available in this license!!!";
            aStatus.errorcode = -101;
            throw new AxiomException("Mobile Trust feature is not available in this license!!!");
        }

        try {

            int retValue = -10;
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Invalid Parameters!!!";
            aStatus.errorcode = retValue;

            byte[] SHA1hash = null;
            if (trustPayLoad == null) {
                SHA1hash = UtilityFunctions.SHA1(sessionid + userid + data);
            } else {
                SHA1hash = UtilityFunctions.SHA1(sessionid + userid + data + trustPayLoad);
            }
//            String integritycheck = new String(Base64.encode(SHA1hash));
            String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//            String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java

            if (integrityCheckString != null) {
                if (integrityCheckString.equals(integritycheck) != true) {
                    aStatus.error = "Input Data is Tampered!!!";
                    aStatus.errorcode = -76;
                    throw new AxiomException("Input Data is Tampered!!!");

                }
            }
            if (sessionid == null || userid == null) {
                throw new AxiomException("Invalid Parameters!!!");
            }

            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionid);
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            if (session == null) {
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -104;
                throw new AxiomException("Invalid Session");
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                aStatus.error = "Invalid Channel";
                aStatus.errorcode = -3;
                throw new AxiomException("Invalid Channel");
            }

            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
            if (channelProfileObj != null) {
                int retVal = sManagement.getServerStatus(channelProfileObj);
                if (retVal != 0) {

                    aStatus = new AxiomStatus();
                    aStatus.error = "Server Down for maintainance,Please try later!!!";
                    aStatus.errorcode = -48;
                    throw new AxiomException("Server Down for maintainance,Please try later!!!");

                }
            }

            if (session != null) {

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagementObj = new OperatorsManagement();
                            Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP";
                                aStatus.errorcode = -108;
                                throw new AxiomException("INVALID IP");

                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP";
                            aStatus.errorcode = -108;
                            throw new AxiomException("INVALID IP");
                        }
                        aStatus.error = "INVALID IP";
                        aStatus.errorcode = -108;
                        throw new AxiomException("INVALID IP");
                    }
                }

                if (trustPayLoad != null) {
                    MobileTrustSettings mSettings = null;

                    Object obj = setManagement.getSetting(sessionid, channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
                    if (obj != null) {
                        if (obj instanceof MobileTrustSettings) {
                            mSettings = (MobileTrustSettings) obj;
                        }
                    }

                    if (mSettings == null) {
                        aStatus.error = "Mobile Trust Setting is not configured!!!";
                        aStatus.errorcode = -23;
//                  
                        throw new AxiomException(aStatus.error);
                    }

                    if (mSettings.bGeoFencing == true) {

                        GeoLocationManagement gManagement = new GeoLocationManagement();
                        AXIOMStatus status = gManagement.validateGeoLocation(sessionid, channelid, userid, trustPayLoad);
                        if (status == null) {
                            aStatus.error = "Location Not Found!!!";
                            aStatus.errorcode = -115;
                        } else if (status.iStatus != 0) {

                            aStatus.error = status.strStatus;
                            aStatus.errorcode = status.iStatus;

                            //     String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
                            audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                                    session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorcode,
                                    "Token Management", "Invalid Location", null, itemtype,
                                    session.getLoginid());

                            throw new AxiomException(aStatus.error);
                        }
                    }
                }
                MobileTrustManagment mManagment = new MobileTrustManagment();
                JSONObject jsonObj = new JSONObject(data);
                String device = jsonObj.getString("_deviceType");
                int devicetype = 1;
                if (device.equals("ANDROID")) {
                    devicetype = ANDROID;
                } else if (device.equals("IOS")) {
                    devicetype = IOS;
                }
                String did = new String(Base64.decode(jsonObj.getString("did")));

                if (data != null) {
                    String encryptedData = mManagment.EnforceSecrity(sessionid, channel, userid, data, devicetype);

                    if (encryptedData != null) {
                        aStatus.error = "Success";
                        aStatus.errorcode = 0;
                    } else {
                        aStatus.error = "Failed";
                        aStatus.errorcode = 0;
                    }
                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(), "Mobile Trust Enforce Security", aStatus.error, aStatus.errorcode,
                            "Security Management", aStatus.error, " ",
                            itemtype, userid);
                    return new String(Base64.encode(encryptedData.getBytes()));

                    //return aStatus;
                    //end of addition
                } else {

                    aStatus.error = "No Data Available";
                    aStatus.errorcode = -55;
                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(), "Mobile Trust Enforce Security", aStatus.error, aStatus.errorcode,
                            "Security Management", aStatus.error, " ",
                            itemtype, userid);
                }

            }

        } catch (Exception e) {
            throw new AxiomException(e.getMessage());
        }
        return null;
    }

    @Override
    public String ConsumeSecurity(String sessionid, String userid, String data, String integrityCheckString, String trustPayLoad) throws AxiomException {

        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                System.out.println("EnforceSecurity::sessionid::" + sessionid);
                System.out.println("EnforceSecurity::userid::" + userid);
                System.out.println("EnforceSecurity::data::" + data);
            }
        } catch (Exception ex) {
        }
//nilesh
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Licence invalid";
            aStatus.errorcode = -100;
            throw new AxiomException("Licence invalid");

        }

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.MOBILETRUST) != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Mobile Trust feature is not available in this license!!!";
            aStatus.errorcode = -101;
            throw new AxiomException("Mobile Trust feature is not available in this license!!!");
        }

        try {

            int retValue = -10;
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Invalid Parameters!!!";
            aStatus.errorcode = retValue;

            byte[] SHA1hash = null;
            if (trustPayLoad == null) {
                SHA1hash = UtilityFunctions.SHA1(sessionid + userid + data);
            } else {
                SHA1hash = UtilityFunctions.SHA1(sessionid + userid + data + trustPayLoad);
            }
//            String integritycheck = new String(Base64.encode(SHA1hash));
            String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//            String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java

            if (integrityCheckString != null) {
                if (integrityCheckString.equals(integritycheck) != true) {
                    aStatus.error = "Input Data is Tampered!!!";
                    aStatus.errorcode = -76;
                    throw new AxiomException("Input Data is Tampered!!!");

                }
            }

            if (sessionid == null || userid == null) {
                throw new AxiomException("Invalid Parameters!!!");
            }
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionid);
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            if (session == null) {
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -104;
                throw new AxiomException("Invalid Session");
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                aStatus.error = "Invalid Channel";
                aStatus.errorcode = -3;
                throw new AxiomException("Invalid Channel");
            }

            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
            if (channelProfileObj != null) {
                int retVal = sManagement.getServerStatus(channelProfileObj);
                if (retVal != 0) {

                    aStatus = new AxiomStatus();
                    aStatus.error = "Server Down for maintainance,Please try later!!!";
                    aStatus.errorcode = -48;
                    throw new AxiomException("Server Down for maintainance,Please try later!!!");
                }
            }

            if (session != null) {

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagementObj = new OperatorsManagement();
                            Operators[] aOperator = oManagementObj.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP";
                                aStatus.errorcode = -108;
                                throw new AxiomException("INVALID IP");

                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP";
                            aStatus.errorcode = -108;
                            throw new AxiomException("INVALID IP");
                        }
                        aStatus.error = "INVALID IP";
                        aStatus.errorcode = -108;
                        throw new AxiomException("INVALID IP");
                    }
                }
                if (trustPayLoad != null) {
                    MobileTrustSettings mSettings = null;

                    Object obj = setManagement.getSetting(sessionid, channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
                    if (obj != null) {
                        if (obj instanceof MobileTrustSettings) {
                            mSettings = (MobileTrustSettings) obj;
                        }
                    }

                    if (mSettings == null) {
                        aStatus.error = "Mobile Trust Setting is not configured!!!";
                        aStatus.errorcode = -23;
//                  
                        throw new AxiomException(aStatus.error);
                    }

                    if (mSettings.bGeoFencing == true) {

                        GeoLocationManagement gManagement = new GeoLocationManagement();
                        AXIOMStatus status = gManagement.validateGeoLocation(sessionid, channelid, userid, trustPayLoad);
                        if (status == null) {
                            aStatus.error = "Location Not Found!!!";
                            aStatus.errorcode = -115;
                        } else if (status.iStatus != 0) {

                            aStatus.error = status.strStatus;
                            aStatus.errorcode = status.iStatus;
                            //     String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
                            audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                                    session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorcode,
                                    "Token Management", "Invalid Location", null, itemtype,
                                    session.getLoginid());

                            throw new AxiomException(aStatus.error);
                        }
                    }
                }
                MobileTrustManagment mManagment = new MobileTrustManagment();
                JSONObject jsonObj = new JSONObject(data);
                String device = jsonObj.getString("_deviceType");
                int devicetype = 1;
                if (device.equals("ANDROID")) {
                    devicetype = ANDROID;
                } else if (device.equals("IOS")) {
                    devicetype = IOS;
                }
                String did = new String(Base64.decode(jsonObj.getString("did")));

                if (data != null) {
                    String plainData = mManagment.ConsumeSecurity(sessionid, channel, userid, data, devicetype);

                    if (plainData != null) {
                        aStatus.error = "Success";
                        aStatus.errorcode = 0;
                    } else {
                        aStatus.error = "Failed";
                        aStatus.errorcode = 0;
                    }
                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(), "Mobile Trust Enforce Security", aStatus.error, aStatus.errorcode,
                            "Security Management", aStatus.error, " ",
                            itemtype, userid);
                    return new String(Base64.encode(plainData.getBytes()));

                    //return aStatus;
                    //end of addition
                } else {

                    aStatus.error = "No Data Available";
                    aStatus.errorcode = -55;
                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(), "Mobile Trust Consume Security", aStatus.error, aStatus.errorcode,
                            "Security Management", aStatus.error, " ",
                            itemtype, userid);
                }

            }

        } catch (Exception e) {
            throw new AxiomException(e.getMessage());
        }
        return null;
    }

    @Override
    public String OpenSession(String channelid, String loginid, String loginpassword, String integrityCheckString) {
        try {

            String strDebug = null;
            SettingsManagement setManagement = new SettingsManagement();
            SessionManagement sManagement = new SessionManagement();
            try {

                ChannelProfile channelprofileObj = null;
                Object channelpobj = setManagement.getSettingInner(channelid, SettingsManagement.CHANNELPROFILE_SETTING, 1);

                if (channelpobj == null) {
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                } else {
                    channelprofileObj = (ChannelProfile) channelpobj;
                    LoadSettings.LoadChannelProfile(channelprofileObj);
                }
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("OpenSession::loginpassword::" + loginpassword);
                    System.out.println("OpenSession::channelid::" + channelid);
                    System.out.println("OpenSession::loginid::" + loginid);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                System.out.println("OpenSession::License Error::" + iResult);
//                return null;
//            }
            byte[] SHA1hash = UtilityFunctions.SHA1(channelid + loginid + loginpassword);
//            String integritycheck = new String(Base64.encode(SHA1hash));
            String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//            String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java

            if (integrityCheckString != null) {
                if (integrityCheckString.equals(integritycheck) != true) {

                    throw new AxiomException("Input Data is Tampered!!!");

                }
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(channelid);
            if (channel == null) {
                return null;
            }

            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
            if (channelProfileObj != null) {
                int retVal = sManagement.getServerStatus(channelProfileObj);
                if (retVal != 0) {

                    System.out.println("OpenSession::Server Down for maintainance,Please try later!!!    -48");
                    return null;

                }
            }

            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            //System.out.println("Client IP = " + req.getRemoteAddr());
            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        SessionFactoryUtil suOperators = new SessionFactoryUtil(SessionFactoryUtil.operators);
                        OperatorsManagement oManagement = new OperatorsManagement();
                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            return null;
                        }

                        suTemplate.close();
                        sTemplate.close();
                        return null;
                    }
                    return null;
                }
            }

            String resultStr = "Failure";
            int retValue = -1;

            String sessionId = sManagement.OpenSessionForWS(channelid, loginid, loginpassword, req.getSession().getId());

            AuditManagement audit = new AuditManagement();
            if (sessionId != null) {
                retValue = 0;
                resultStr = "Success";
                audit.AddAuditTrail(sessionId, channelid, loginid,
                        req.getRemoteAddr(),
                        channel.getName(), loginid,
                        loginid, new Date(), "Open Session", resultStr, retValue,
                        "Login", "", "Open Session successfully with Session Id =" + sessionId, "SESSION",
                        loginid);

            } else if (sessionId == null) {
                audit.AddAuditTrail(sessionId, channel.getChannelid(), loginid, req.getRemoteAddr(), channel.getName(), loginid,
                        loginid, new Date(), "Open Session", resultStr, retValue,
                        "Login", "", "Failed To Open Session", "SESSION",
                        loginid);

            }

            return sessionId;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public AxiomStatus CloseSession(String sessionid, String integrityCheckString) {
        try {

            String strDebug = null;
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("CloseSession::sessionid::" + sessionid);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Licence is invalid";
                aStatus.errorcode = -100;
                return aStatus;
            }

            byte[] SHA1hash = UtilityFunctions.SHA1(sessionid);
//            String integritycheck = new String(Base64.encode(SHA1hash));
            String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//            String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java

            AxiomStatus aStatus = new AxiomStatus();
            if (integrityCheckString != null) {
                if (integrityCheckString.equals(integritycheck) != true) {
                    aStatus.error = "Input Data is Tampered!!!";
                    aStatus.errorcode = -76;
                    return aStatus;

                }
            }
            SessionManagement sManagement = new SessionManagement();
            Sessions session = sManagement.getSessionById(sessionid);
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);

            aStatus.error = "ERROR";
            aStatus.errorcode = -2;

            if (session != null) {

                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    return null;
                }

                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {

                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
                        return aStatus;
                    }
                }

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }
                int retValue = sManagement.CloseSession(sessionid);

                aStatus.error = "ERROR";
                aStatus.errorcode = retValue;
                if (retValue == 0) {
                    aStatus.error = "SUCCESS";
                    aStatus.errorcode = 0;
                }

            } else if (session == null) {
                return aStatus;
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                return aStatus;
            }
            AuditManagement audit = new AuditManagement();
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                    session.getLoginid(), new Date(), "Close Session", aStatus.error, aStatus.errorcode,
                    "Login", "", "Close Session successfully with Session Id =" + sessionid, "SESSION",
                    session.getLoginid());
            return aStatus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public AxiomMessage[] SendMessages(String sessionId, AxiomMessage[] msgs, boolean bCheckContent, int speedType, int type) throws AxiomException {
//       int iResult = AxiomProtect.ValidateLicense();   iResult=0;
//        if (iResult != 0) {
//            return null;
//        }

        int iResult = AxiomProtect.ValidateLicense();
        iResult = 0;
        if (iResult != 0) {
            //AxiomStatus aStatus = new AxiomStatus();
            //aStatus.error = "Licence is invalid";
            //aStatus.errorcode = -100;
            //return aStatus;
            throw new AxiomException("Licence is invalid");
        }

        int SMS = 1;
        int USSD = 2;
        int VOICE = 3;
        int EMAIL = 4;
        try {

            if (msgs == null || msgs.length == 0) {
                throw new AxiomException("Invalid Messages!!!");
            }

            if (type == EMAIL) {
                if (AxiomProtect.CheckEnforcementFor(AxiomProtect.GATEWAY_EMAIL) != 0) {
                    throw new AxiomException("Email Messaging is not available in this license!!!");
                }
            } else if (type == USSD) {
                if (AxiomProtect.CheckEnforcementFor(AxiomProtect.GATEWAY_USSD) != 0) {
                    throw new AxiomException("USSD Messaging is not available in this license!!!");
                }
            } else if (type == VOICE) {
                if (AxiomProtect.CheckEnforcementFor(AxiomProtect.GATEWAY_VOICE) != 0) {
                    throw new AxiomException("Voice Messaging is not available in this license!!!");
                }
            } else if (type == SMS) {
                if (AxiomProtect.CheckEnforcementFor(AxiomProtect.GATEWAY_SMS) != 0) {
                    throw new AxiomException("SMS Messaging is not available in this license!!!");
                }
            }

            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                throw new AxiomException("Invalid Session");
            }

            BulkMSGManagement bMSGManagement = new BulkMSGManagement();
//            SessionManagement sManagement = new SessionManagement();
//            AuditManagement audit = new AuditManagement();
//            MessageContext mc = wsContext.getMessageContext();
//            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//            Sessions session = sManagement.getSessionById(sessionId);
            int check = 0;
            //AxiomMessage aMessage = null;
//            if (session == null) {
//                throw new AxiomException("Invalid Session");
//            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                throw new AxiomException("Invalid Channel");
            }

            if (session != null) {

                SettingsManagement setManagement = new SettingsManagement();
//                int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
//                if (result != 1) {
//                    throw new AxiomException("Invalid IP");
//                }

                String channelid = session.getChannelid();

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                throw new AxiomException("Invalid IP");
                            }

                            suTemplate.close();
                            sTemplate.close();
                            throw new AxiomException("Invalid IP");
                        }
                        throw new AxiomException("Invalid IP");
                    }
                }
                int result = -1;
                int speed = 0;
                Object settingsObj = setManagement.getSetting(sessionId, session.getChannelid(), SettingsManagement.GlobalSettings, SettingsManagement.PREFERENCE_ONE);
                GlobalChannelSettings globalObj = null;
                if (settingsObj != null) {
                    globalObj = (GlobalChannelSettings) settingsObj;
                    if (speedType == GlobalChannelSettings._SLOW) {
                        if (type == EMAIL) {
                            speed = globalObj.emailsettingobj._slowPaceEMAIL;
                        } else if (type == SMS) {
                            speed = globalObj.smssettingobj._slowPaceSMS;
                        } else if (type == VOICE) {
                            speed = globalObj.voicesettingobj._slowPaceVOICE;
                        } else if (type == USSD) {
                            speed = globalObj.ussdsettingobj._slowPaceUSSD;
                        }
                    } else if (speedType == GlobalChannelSettings._NORMAL) {
                        if (type == EMAIL) {
                            speed = globalObj.emailsettingobj._normalPaceEMAIL;
                        } else if (type == SMS) {
                            speed = globalObj.smssettingobj._normalPaceSMS;
                        } else if (type == VOICE) {
                            speed = globalObj.voicesettingobj._normalPaceVOICE;
                        } else if (type == USSD) {
                            speed = globalObj.ussdsettingobj._normalPaceUSSD;
                        }
                    } else if (speedType == GlobalChannelSettings._FAST) {
                        if (type == EMAIL) {
                            speed = globalObj.emailsettingobj._fastPaceEMAIL;
                        } else if (type == SMS) {
                            speed = globalObj.smssettingobj._fastPaceSMS;
                        } else if (type == VOICE) {
                            speed = globalObj.voicesettingobj._fastPaceVOICE;
                        } else if (type == USSD) {
                            speed = globalObj.ussdsettingobj._fastPaceUSSD;
                        }
                    } else if (speedType == GlobalChannelSettings._HYPER) {
                        if (type == EMAIL) {
                            speed = globalObj.emailsettingobj._hyperPaceEMAIL;
                        } else if (type == SMS) {
                            speed = globalObj.smssettingobj._hyperPaceSMS;
                        } else if (type == VOICE) {
                            speed = globalObj.voicesettingobj._hyperPaceVOICE;
                        } else if (type == USSD) {
                            speed = globalObj.ussdsettingobj._hyperPaceUSSD;
                        }
                    }
                } else {
                    throw new AxiomException("Configuraiton Missing!!!");
                }

                //if (msgs != null && msgs.length > 0) {
                com.mollatech.axiom.nucleus.db.operation.AxiomMessage[] aMsg = new com.mollatech.axiom.nucleus.db.operation.AxiomMessage[msgs.length];

                String res = null;
                //int msgid = 0;
                //AuthUser[] aUsers = new AuthUser[msgs.length];
                //add all device info and get new unique number

                String uniqueid = req.getSession().getId() + channel.getChannelid() + new Date().getTime() + sessionId;
                String uniqueidforbulk = new String(Base64.encode(UtilityFunctions.SHA1(uniqueid)));

                for (int i = 0; i < msgs.length; i++) {

                    if ((msgs[i] == null || msgs[i].message == null || msgs[i].number == null
                            || msgs[i].message.isEmpty() == true || msgs[i].number.isEmpty() == true)
                            && (msgs[i] == null || msgs[i].message == null || msgs[i].emailid == null || msgs[i].subject == null
                            || msgs[i].message.isEmpty() == true || msgs[i].emailid.isEmpty() == true || msgs[i].subject.isEmpty() == true)) {
                        msgs[i].status = new AxiomStatus();
                        msgs[i].status.error = SEND_MESSAGE_INVALID_DATA_STRING;
                        msgs[i].status.errorcode = SEND_MESSAGE_INVALID_DATA;
                        check = -1;
                        continue;
                    }

                    if (bCheckContent == true) {
                        res = setManagement.checkcontent(session.getChannelid(), msgs[i].message, globalObj);
                        if (res == null) { //good to send out

                            try {
                                aMsg[i] = new com.mollatech.axiom.nucleus.db.operation.AxiomMessage();
                                aMsg[i].phone = msgs[i].number;
                                aMsg[i].message = msgs[i].message;
                                aMsg[i].emailid = msgs[i].emailid;
                                aMsg[i].subject = msgs[i].subject;

                                msgs[i].status = new AxiomStatus();
                                msgs[i].status.error = SEND_MESSAGE_PENDING_STATE_STRING;
                                //msgs[i].status.errorcode = SEND_MESSAGE_PENDING_STATE;
                                msgs[i].status.errorcode = 0;
                                int retValue = bMSGManagement.ADDAxiomMessage(sessionId, session.getChannelid(), null, aMsg[i], SEND_MESSAGE_PENDING_STATE, new Date(), new Date(), type, speed, uniqueidforbulk);
                                msgs[i].msgid = "" + retValue;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else { //to be blocked

                            try {
                                aMsg[i] = new com.mollatech.axiom.nucleus.db.operation.AxiomMessage();
                                aMsg[i].phone = msgs[i].number;
                                aMsg[i].message = msgs[i].message;
                                aMsg[i].emailid = msgs[i].emailid;
                                aMsg[i].subject = msgs[i].subject;

                                msgs[i].status = new AxiomStatus();
                                msgs[i].status.error = SEND_MESSAGE_BLOCKED_STATE_STRING;
                                msgs[i].status.errorcode = SEND_MESSAGE_BLOCKED_STATE;
                                int retValue = bMSGManagement.ADDAxiomMessage(sessionId, session.getChannelid(), null, aMsg[i], SEND_MESSAGE_BLOCKED_STATE, new Date(), new Date(), type, speed, uniqueidforbulk);
                                msgs[i].msgid = "" + retValue;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        try {
                            aMsg[i] = new com.mollatech.axiom.nucleus.db.operation.AxiomMessage();
                            aMsg[i].phone = msgs[i].number;
                            aMsg[i].message = msgs[i].message;
                            aMsg[i].emailid = msgs[i].emailid;
                            aMsg[i].subject = msgs[i].subject;
                            msgs[i].status = new AxiomStatus();
                            msgs[i].status.error = SEND_MESSAGE_PENDING_STATE_STRING;
                            //msgs[i].status.errorcode = SEND_MESSAGE_PENDING_STATE;
                            msgs[i].status.errorcode = 0;
                            int retValue = bMSGManagement.ADDAxiomMessage(sessionId, session.getChannelid(), null, aMsg[i], SEND_MESSAGE_PENDING_STATE, new Date(), new Date(), type, speed, uniqueidforbulk);
                            msgs[i].msgid = "" + retValue;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (check == 0) {
                    result = bMSGManagement.SendMSG(sessionId, session.getChannelid(), type, speed, uniqueidforbulk);
                }
                //return msgs;

                //}
                String strTYPE = null;

                if (type == 1) {
                    strTYPE = "SMS";
                } else if (type == 2) {
                    strTYPE = "USSD";
                } else if (type == 3) {
                    strTYPE = "VOICE";
                } else if (type == 4) {
                    strTYPE = "EMAIL";
                } else if (type == 5) {

                }

                String resultstr = "Failure";
                if (result == 0) {

                    resultstr = "Success";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                            channel.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(),
                            "BULK SEND", resultstr, result,
                            "Messages",
                            "Total Count=" + msgs.length,
                            "",
                            strTYPE,
                            channel.getChannelid());

//                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), 
//                        req.getRemoteAddr(), channel.getName(),
//                        session.getLoginid(), session.getLoginid(), new Date(), 
//                        "Add and Send Axiom MSG...!", resultstr, result,
//                        "Axiom Mesaage Management", "", "Bulk Msg Send Successfully", 
//                        "AXIOM MESSAGE", session.getLoginid());
                } else if (result != 0) {
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                            channel.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(),
                            "BULK SEND", resultstr, result,
                            "Messages",
                            "Total Count=" + msgs.length,
                            "",
                            strTYPE,
                            channel.getChannelid());
//                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                        session.getLoginid(), session.getLoginid(), new Date(), "Add and Send Axiom MSG...!", resultstr, result,
//                        "Axiom Mesaage Management", "", "Failed To Send Axiom MSG", "AXIOM MESSAGE",
//                        session.getLoginid());
                }
            }
            return msgs;

        } catch (Exception e) {
            throw new AxiomException("System Internal Exception::" + e.getMessage());
            //return null;
        }
    }

    @Override
    public AxiomStatus sendNotification(String sessionid, String userid, int type, String[] data, String integrityCheckString, String trustPayLoad) {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                System.out.println("sendNotification::sessionid::" + sessionid);
                System.out.println("sendNotification::userid::" + userid);
                System.out.println("sendNotification::type::" + type);
            }
        } catch (Exception ex) {
        }
//nilesh
        int iResult = AxiomProtect.ValidateLicense();
        iResult = 0;
        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "Licence is invalid";
//            aStatus.errorcode = -100;
//            return aStatus;
            //throw new AxiomException("Licence is invalid");
        }
        AxiomStatus aStatus = new AxiomStatus();
        byte[] SHA1hash = null;
        if (trustPayLoad == null) {
            SHA1hash = UtilityFunctions.SHA1(sessionid + userid + type);
        } else {
            SHA1hash = UtilityFunctions.SHA1(sessionid + userid + type + trustPayLoad);
        }
//        String integritycheck = new String(Base64.encode(SHA1hash));
        String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//        String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java
        if (integrityCheckString != null) {
            if (integrityCheckString.equals(integritycheck) != true) {
                aStatus.error = "Input Data is Tampered!!!";
                aStatus.errorcode = -76;
                return aStatus;
            }
        }
        try {
            SessionManagement sManagement = new SessionManagement();
            UserManagement uManagement = new UserManagement();
            CryptoManagement cyManagement = new CryptoManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionid);
            int retValue = -1;
            aStatus.error = "ERROR";
            aStatus.errorcode = retValue;
            if (session == null) {
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
                return aStatus;
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(session.getChannelid());
            if (channel == null) {
                aStatus.error = "Invalid Channel";
                aStatus.errorcode = -15;
                return aStatus;
            }
            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
            if (channelProfileObj != null) {
                int retVal = sManagement.getServerStatus(channelProfileObj);
                if (retVal != 0) {
                    aStatus.error = "Server Down for maintainance,Please try later!!!";
                    aStatus.errorcode = -48;
                    return aStatus;
                }
            }
            if (session != null) {
                AuthUser aUser = uManagement.getUser(sessionid, channel.getChannelid(), userid);
                if (aUser == null) {
                    aStatus.error = "INVALID USER";
                    aStatus.errorcode = -10;
                    return aStatus;
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                return aStatus;
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                }
                if (trustPayLoad != null) {
                    MobileTrustSettings mSettings = null;
                    Object obj = setManagement.getSetting(sessionid, channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
                    if (obj != null) {
                        if (obj instanceof MobileTrustSettings) {
                            mSettings = (MobileTrustSettings) obj;
                        }
                    }
                    if (mSettings == null) {
                        aStatus.error = "Mobile Trust Setting is not configured!!!";
                        aStatus.errorcode = -23;
                        return aStatus;
                    }
                    if (mSettings.bGeoFencing == true) {
                        GeoLocationManagement gManagement = new GeoLocationManagement();
                        AXIOMStatus status = gManagement.validateGeoLocation(sessionid, channelid, userid, trustPayLoad);
                        if (status == null) {
                            aStatus.error = "Location Not Found!!!";
                            aStatus.errorcode = -115;
                        } else if (status.iStatus != 0) {
                            aStatus.error = status.strStatus;
                            aStatus.errorcode = status.iStatus;
                            //     String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
                            audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                                    session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorcode,
                                    "Token Management", "Invalid Location", null, itemtype,
                                    session.getLoginid());
                            return aStatus;
                        }
                    }
                }
                //here to add code
                if (type == USER_PASSWORD) {
                    aStatus = this.SendPassword(session, channel, aUser, req);
                    return aStatus;
                } else if (type == OTP_TOKEN_REGCODE) {
                    aStatus = this.SendRegistrationCode(session, channel, userid, OTP_TOKEN, req);
                    return aStatus;
                } else if (type == OTP_TOKEN_OTP) {
                    aStatus = this.SendOTP(session, channel, userid, req);
                    return aStatus;
                } else if (type == OTP_TOKEN_SOTP) {
                    aStatus = this.SendSignatureOTP(session, channel, userid, data, req);
                    return aStatus;
                } else if (type == PKI_TOKEN_REGCODE) {
                    aStatus = this.SendRegistrationCode(session, channel, userid, PKI_TOKEN, req);
                    return aStatus;
                } else if (type == PFX_PASSWORD) {
                    aStatus = this.SendPfxPassword(session, channel, aUser, req);
                    return aStatus;
                } else if (type == PFX_FILE) {
                    aStatus = this.SendPfxFile(session, channel, aUser, req);
                    return aStatus;
                } else if (type == CERT_FILE) {
                    aStatus = this.SendCertificateFile(session, channel, aUser, req);
                    return aStatus;
                } else if (type == IMG_AUTH_REGCODE) {
                    aStatus = this.SendRegistrationCode(session, channel, userid, IMG_AUTH, req);
                    return aStatus;
                } else if (type == EMAIL_PDF_SIGNING_TEMPLATE) {
                    aStatus = this.SendEmailPdfTemplate(session, channel, userid, EMAIL_PDF_SIGNING_TEMPLATE, data);
                    return aStatus;
                } else if (type == EPIN) {
//                    for EPIN added by amol
                    Registerdevicepush registerdevicepush = new PushNotificationDeviceManagement().GetRegisterPushDeviceByUserID(sessionid, channel.getChannelid(), userid);
                    if (registerdevicepush == null) {
                        aStatus.error = "USER NOT REGISTERED FOR PUSH";
                        aStatus.errorcode = -63;
                        return aStatus;
//                        throw new AxiomException(aStatus.error);
                    } else {
//                        InitTransactionPackage package1=new InitTransactionPackage();                        
                        Txdetails txdetail = new Txdetails();
                        txdetail.setCreatedOn(new Date());
                        txdetail.setExpmin(3);
                        txdetail.setUserid(userid);
                        txdetail.setSessionid(sessionid);
                        txdetail.setChannelid(channel.getChannelid());
                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.MINUTE, 3);
                        txdetail.setExpiredOn(calendar.getTime());
                        txdetail.setStatus(Txdetails.PENDING);
                        txdetail.setQnAStatus(Txdetails.PENDING);
                        txdetail.setMobileno(aUser.phoneNo);
                        txdetail.setChannelid(channelid);
//                         set EPIN ecrypted message here
                        EPINManagement epinmngt = new EPINManagement();
                        PINDeliverySetting epin_setting = null;
                        SettingsManagement s = new SettingsManagement();
                        epin_setting = (PINDeliverySetting) s.getSetting(sessionid, channelid, 9, 1);
                        String epin = epinmngt.GeneratePIN(channel.getChannelid(), epin_setting, aUser.getUserId());
                        if (epin != null) {
                            CryptoManager cm = new CryptoManager();
                            System.out.println("EPIN is : " + epin);
//                           Encrypt EPIN
                            JSONObject jdata = new JSONObject();
                            jdata.put("_EPIN", epin);
                            CertificateManagement c = new CertificateManagement();
                            Certificates cert = c.getCertificate(channel.getChannelid(), userid);
                            TrustDeviceManagement tm = new TrustDeviceManagement();
                            Trusteddevice tdevice = tm.getTrusteddevice(sessionid, channelid, userid);
                            String epin_protected = cm.EnforceMobileTrust(jdata, tdevice.getDeviceid(), cert.getPublickey(), channel.getHiddenkey(), channel.getVisiblekey(), registerdevicepush.getType(), userid, channelid, "");
                            System.out.println("EPIN is : " + epin_protected);
                            txdetail.setTxmsg(epin_protected);
                            txdetail.setQuestion("");
                            Random randomGenerator = new Random();
                            int randomInt = randomGenerator.nextInt(10000000);
                            byte[] authId = Base64.encode(("" + randomInt).getBytes());
                            String strauthId = new String(authId);
                            txdetail.setTransactionId(strauthId);
                            int res = new TxManagement().addTxDetails(txdetail);
                            JSONObject json = new JSONObject();
                            json.put("message", "EPIN");
                            json.put("title", "Axiom Protect");
                            json.put("_txid", txdetail.getTransactionId());
                            AXIOMStatus status = new SendNotification().SendOnPush(channelid, registerdevicepush.getGoogleregisterid(), aUser.email, json.toString(), ANDROID);
                            aStatus.error = status.strStatus;
                            aStatus.errorcode = status.iStatus;
                            return aStatus;
                        } else {
                            aStatus.errorcode = -1;
                            aStatus.error = "Error in Generating EPIN!!";
                            return aStatus;
                        }
                    }
//                    amol EPIN end here
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            aStatus.error = "Exception::" + e.getMessage();
            aStatus.errorcode = -20;
            return aStatus;
        }
        return null;
    }

    private AxiomStatus SendRegistrationCode(Sessions session, Channels channel, String userid, int type, HttpServletRequest req) {
        String strDebug = null;

        if (type == OTP_TOKEN) {

            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "OTP Token feature is not available in this license!!!";
                aStatus.errorcode = -101;
                return aStatus;
            }
        } else if (type == PKI_TOKEN) {

            if (AxiomProtect.CheckEnforcementFor(AxiomProtect.PKI_SOFTWARE) != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "PKI Token feature is not available in this license!!!";
                aStatus.errorcode = -101;
                return aStatus;
            }
        } else if (type == IMG_AUTH) {

        }

        AuditManagement audit = new AuditManagement();

        AxiomStatus aStatus = new AxiomStatus();
        int retValue = -1;
        aStatus.error = "ERROR";
        aStatus.errorcode = retValue;

        if (session == null) {
            aStatus.error = "Session Invalid!!!";
            aStatus.errorcode = -14;
            return aStatus;
        }

        String strCategory = "SOFTWARE_TOKEN";

        if (channel == null) {
            aStatus.error = "Invalid Channel";
            aStatus.errorcode = -3;
            return aStatus;
        }

        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
        if (session != null) {
            SettingsManagement setManagement = new SettingsManagement();
            //if (tSettings.isbSilentCall() == false) {
            String regCode = null;
            Otptokens oToken = null;
            Pkitokens pToken = null;
            Securephrase sPhrase = null;
            int subtype = 0;
            PKITokenManagement pManagement = new PKITokenManagement();
            SecurePhraseManagment sManagment = new SecurePhraseManagment();
            TokenSettings token = (TokenSettings) setManagement.getSetting(session.getSessionid(), session.getChannelid(), SettingsManagement.Token, SettingsManagement.PREFERENCE_ONE);
            if (token == null) {
                aStatus.error = "OTP Token Settings is not configured!!!";
                aStatus.errorcode = -17;
                return aStatus;
            }
            SimpleDateFormat sdfExpiry = new SimpleDateFormat("hh:mm");
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MINUTE, token.getRegistrationValidity());
            Date expiry = cal.getTime();
            if (type == OTP_TOKEN) {

                oToken = oManagement.getOtpObjByUserId(session.getSessionid(), session.getChannelid(), userid, OTPTokenManagement.SOFTWARE_TOKEN);
                if (oToken == null) {
                    aStatus.error = "Desired Software Token is not assigned!!!";
                    aStatus.errorcode = -22;

                    audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(), "Send Registration Code ", aStatus.error, aStatus.errorcode,
                            "Token Management", "", "Category = " + strCategory + "regcode =" + "******",
                            itemtype, userid);
                    return aStatus;
                }
                subtype = oToken.getSubcategory();
                regCode = oManagement.generateRegistrationCode(session.getSessionid(), session.getChannelid(), userid, OTP_TOKEN_SOFTWARE);
                String strSWOTPType = LoadSettings.g_sSettings.getProperty("sw.otp.type");
                if (strSWOTPType != null && strSWOTPType.compareToIgnoreCase("simple") == 0) { // we are forcing it to be time based for mobile token.
                    regCode = "1" + regCode;

                }
                System.out.println("REGCODE =" + regCode);

            } else if (type == PKI_TOKEN) {
                pToken = pManagement.getPkiObjByUserid(session.getChannelid(), userid, PKITokenManagement.SOFTWARE_TOKEN);
                if (pToken == null) {
                    aStatus.error = "Desired Software Token is not assigned!!!";
                    aStatus.errorcode = -22;

                    audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(), "Send Registration Code ", aStatus.error, aStatus.errorcode,
                            "PKI Token Management", "", "Category = " + strCategory + "regcode =" + "******",
                            itemtype, userid);
                    return aStatus;

                }
                subtype = pToken.getCategory();
                regCode = pManagement.generateRegistrationCode(session.getSessionid(), session.getChannelid(), userid, PKITokenManagement.SOFTWARE_TOKEN);
            } else if (type == IMG_AUTH) {
                UserManagement uManagement = new UserManagement();
                AuthUser aUser = uManagement.getUser(session.getSessionid(), session.getChannelid(), userid);
                if (aUser == null) {
                    aStatus.error = "User Not Found!!!";
                    aStatus.errorcode = -22;

                    audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(), "Send Registration Code ", aStatus.error, aStatus.errorcode,
                            "SecurePhrase Management", "", "regcode =" + "******",
                            "IMAGEAUTH", userid);
                    return aStatus;

                }
//                subtype = pToken.getCategory();
                regCode = sManagment.generateRegistrationCode(session.getSessionid(), session.getChannelid(), userid);
            }

            if (regCode != null) {

                UserManagement userObj = new UserManagement();
                AuthUser user = null;
                user = userObj.getUser(session.getSessionid(), channel.getChannelid(), userid);

                Templates temp = null;
                TemplateManagement tObj = new TemplateManagement();
                if (type == OTP_TOKEN || type == PKI_TOKEN) {
                    temp = tObj.LoadbyName(session.getSessionid(), channel.getChannelid(), TemplateNames.MOBILE_SOFTWARE_TOKEN_REGISTER_TEMPLATE);
                } else if (type == IMG_AUTH) {
                    temp = tObj.LoadbyName(session.getSessionid(), channel.getChannelid(), TemplateNames.IMAGE_AUTH_REGISTER_TEMPLATE);
                }
                ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                Date d = new Date();
                String tmessage = (String) UtilityFunctions.deserializeFromObject(bais);
                tmessage = tmessage.replaceAll("#name#", user.getUserName());
                tmessage = tmessage.replaceAll("#channel#", channel.getName());
                tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                tmessage = tmessage.replaceAll("#regcode#", regCode);
                tmessage = tmessage.replaceAll("#expiry#", sdfExpiry.format(expiry));

                if (subtype == OTPTokenManagement.SW_WEB_TOKEN) {
                    tmessage = tmessage.replaceAll("#tokentype#", "WEB");

                }

                if (type == OTP_TOKEN) {
                    if (subtype == OTPTokenManagement.SW_MOBILE_TOKEN) {
                        tmessage = tmessage.replaceAll("#tokentype#", "MOBILE");

                    }
                    if (subtype == OTPTokenManagement.SW_PC_TOKEN) {
                        tmessage = tmessage.replaceAll("#tokentype#", "PC");

                    }
                }
                if (type == IMG_AUTH) {
                    if (subtype == OTPTokenManagement.SW_MOBILE_TOKEN) {
                        tmessage = tmessage.replaceAll("#tokentype#", "MOBILE");

                    }
                    if (subtype == OTPTokenManagement.SW_PC_TOKEN) {
                        tmessage = tmessage.replaceAll("#tokentype#", "PC");

                    }
                } else {
                    tmessage = tmessage.replaceAll("#tokentype#", "");
                }
                SendNotification send = new SendNotification();
                AXIOMStatus status = new AXIOMStatus();
                if (temp.getStatus() == tObj.ACTIVE_STATUS) {
//                    status.iStatus = SEND_MESSAGE_PENDING_STATE;
//                    status.strStatus = "PENDING";
//                    status.regcodeMsg = tmessage;
//                    status.regcode = regCode;
                    aStatus.errorcode = SEND_MESSAGE_PENDING_STATE;
                    aStatus.error = "PENDING";
                    aStatus.regCodeMessage = tmessage;
                    aStatus.regcode = regCode;

                    status = send.SendOnMobileNoWaiting(channel.getChannelid(), user.phoneNo, tmessage, 1, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                    //ashish
                    send.SendEmail(channel.getChannelid(), user.getEmail(), "REGISTRATION CODE", tmessage, null, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                    //end ashish
                }

                if (aStatus != null) {
                    if (aStatus.errorcode == SEND_MESSAGE_PENDING_STATE) {
                        RegistrationCodeTrailManagement rManagement = new RegistrationCodeTrailManagement();
                        String registrationcode = UtilityFunctions.Bas64SHA1(regCode);
                        rManagement.addRegCodeTrail(session.getChannelid(), userid, registrationcode, RegistrationCodeTrailManagement.SENT, RegistrationCodeTrailManagement.OTPTOKEN);
                    }
                }
                retValue = 0;
                aStatus.error = "SUCCESS";
                aStatus.errorcode = 0;
                aStatus.regCodeMessage = tmessage;
                aStatus.regcode = regCode;
                if (type == IMG_AUTH) {
                    audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getChannelid(),
                            req.getRemoteAddr(), channel.getName(),
                            session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code",
                            "success", 0, "IMAGE AUTHENTICATION", "", "Registration Code = ******" /*strRegCode*/, "IMAGAUTH", userid);
                } else {
                    audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getChannelid(),
                            req.getRemoteAddr(), channel.getName(),
                            session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code",
                            "success", 0, "Token Management", "", "Registration Code = ******" /*strRegCode*/, itemtype, userid);
                }
            } else if (regCode == null) {
                aStatus.error = "Failed To generate registration code";
                aStatus.errorcode = -24;
                if (type == IMG_AUTH) {
                    audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getChannelid(),
                            req.getRemoteAddr(), channel.getName(),
                            session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code",
                            "success", -1, "IMAGE AUTHENTICATION", "", "Registration Code = ******" /*strRegCode*/, "IMAGAUTH", userid);
                } else {

                    audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channel.getName(),
                            session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code",
                            "failed", -1, "Token Management", "", "Failed To generate Registration Code", itemtype, userid);
                }

            }

        } else {
            aStatus.error = "SUCCESS";
            aStatus.errorcode = 0;
        }

        if (type == IMG_AUTH) {
            audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getChannelid(),
                    req.getRemoteAddr(), channel.getName(),
                    session.getChannelid(), session.getChannelid(), new Date(), "Generate Registration Code",
                    aStatus.error, aStatus.errorcode, "IMAGE AUTHENTICATION", "", "Registration Code = ******" /*strRegCode*/, "IMAGAUTH", userid);
        } else {
            audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                    session.getLoginid(), session.getLoginid(), new Date(), "Send Registration Code", aStatus.error, aStatus.errorcode,
                    "Token Management", "", "Category = " + strCategory + "regcode =" + "******",
                    itemtype, userid);
        }
        return aStatus;
    }

    private AxiomStatus SendEmailPdfTemplate(Sessions session, Channels channel, String userid, int type, String[] data) {
        String strDebug = null;
        SendNotification send = new SendNotification();
        TemplateManagement tManagement = new TemplateManagement();
        Templates templates = tManagement.LoadbyName(session.getSessionid(), channel.getChannelid(), TemplateNames.EMAIL_PDF_SIGNING_TEMPLATE);
        AXIOMStatus axStatus = null;
        AxiomStatus astatus = new AxiomStatus();
        if (templates.getStatus() == tManagement.ACTIVE_STATUS) {
            ByteArrayInputStream bais = new ByteArrayInputStream(templates.getTemplatebody());
            String tmessage = (String) TemplateUtils.deserializeFromObject(bais);
            String subject = templates.getSubject();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
            UserManagement userObj = new UserManagement();
            AuthUser Users = null;
            Users = userObj.getUser(session.getSessionid(), channel.getChannelid(), userid);
            String _filepath = data[0];
            String referenceid = data[1];
            File file = new File(_filepath);
//            request.getLocalAddr();

            if (tmessage != null) {

                tmessage = tmessage.replaceAll("#name#", Users.getUserName());
                tmessage = tmessage.replaceAll("#channel#", "Face");
                tmessage = tmessage.replaceAll("#email#", Users.getEmail());
                tmessage = tmessage.replaceAll("#datetime#", sdf.format(new Date()));
                tmessage = tmessage.replaceAll("#fileName#", file.getName());
                tmessage = tmessage.replaceAll("#referenceid#", referenceid);
            }

            if (subject != null) {

                subject = subject.replaceAll("#channel#", "Face");
                subject = subject.replaceAll("#datetime#", sdf.format(new Date()));
            }
            //end of addition

            String fileNames[] = new String[1];
//            fileNames[0] = destFileName;
            fileNames[0] = data[0];
            String mimeTypes[] = new String[1];
            mimeTypes[0] = "application/pdf";
            try {
                axStatus = send.SendEmail(channel.getChannelid(), Users.getEmail(), subject, tmessage,
                        null, null, fileNames, mimeTypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));

                if (axStatus != null) {
                    astatus.errorcode = axStatus.iStatus;
                    astatus.error = axStatus.strStatus;

                }

                return astatus;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return astatus;
    }

    private AxiomStatus SendOTP(Sessions session, Channels channel, String userid, HttpServletRequest req) {

        AuditManagement audit = new AuditManagement();

        AxiomStatus aStatus = new AxiomStatus();
        String strCategory = "OOB_TOKEN";

        String strSubCategory = "";
        int retValue = -1;
        aStatus.error = "ERROR";
        aStatus.errorcode = retValue;

        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
        if (session != null) {
            //System.out.println("Client IP = " + req.getRemoteAddr());        
            SettingsManagement setManagement = new SettingsManagement();

            TokenStatusDetails[] tokens = oManagement.getTokenList(session.getSessionid(), session.getChannelid(), userid);
            TokenStatusDetails tokenSelected = null;
            if (tokens == null) {
                aStatus.error = "No Token Assigned to this user!!!";
                aStatus.errorcode = -2;
                return aStatus;
            }
            for (int i = 0; i < tokens.length; i++) {
                if (tokens[i].Catrgory == OTPTokenManagement.OOB_TOKEN) {
                    tokenSelected = tokens[i];
                    break;
                }
            }

            if (tokenSelected == null) {
                aStatus.errorcode = -12;
                aStatus.error = "Desired OOB Token is not assigned";
                return aStatus;
            }
            if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_SMS) {
                strSubCategory = "OOB__SMS_TOKEN";
            } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_USSD) {
                strSubCategory = "OOB__USSD_TOKEN";
            } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_VOICE) {
                strSubCategory = "OOB__VOICE_TOKEN";
            } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_EMAIL) {
                strSubCategory = "OOB__EMAIL_TOKEN";
            }

            AXIOMStatus aStatusResponse = oManagement.SendOTP(session.getSessionid(), session.getChannelid(), userid, OTP_TOKEN_OUTOFBAND, tokenSelected.SubCategory);
            aStatus.error = aStatusResponse.strStatus;
            aStatus.errorcode = aStatusResponse.iStatus;

//            if (retValue == 0) {
//                aStatus.error = "SUCCESS";
//                aStatus.errorcode = 0;
//            }
        }

        audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                channel.getName(),
                session.getLoginid(), session.getLoginid(),
                new Date(),
                "SEND OTP", aStatus.error, aStatus.errorcode,
                "Token Management",
                "",
                "Category=" + strCategory + ",Subcategory=" + strSubCategory,
                "OTPTOKENS", userid);

        return aStatus;
    }

    private AxiomStatus SendPassword(Sessions session, Channels channel, AuthUser user, HttpServletRequest req) {

        AuditManagement audit = new AuditManagement();

        AxiomStatus aStatus = new AxiomStatus();

        int retValue = -1;
        aStatus.error = "ERROR";
        aStatus.errorcode = retValue;

        UserManagement uManagement = new UserManagement();
        if (session != null) {
            //System.out.println("Client IP = " + req.getRemoteAddr());        
            TemplateManagement tManagement = new TemplateManagement();
            String strPassword = uManagement.GetPassword(session.getSessionid(), channel.getChannelid(), user.userId);

            if (strPassword != null) {
                Templates templates = tManagement.LoadbyName(session.getSessionid(), channel.getChannelid(), TemplateNames.MOBILE_USER_PASSWORD_TEMPLATE);
                if (templates.getStatus() == tManagement.ACTIVE_STATUS) {
                    ByteArrayInputStream bais = new ByteArrayInputStream(templates.getTemplatebody());
                    String tmessage = (String) TemplateUtils.deserializeFromObject(bais);
                    //String tsubject = templates.getSubject();
                    ChannelManagement cmObj = new ChannelManagement();
                    //Channels channel = cmObj.getChannelByID(channelId);
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                    if (tmessage != null) {
                        Date d = new Date();
                        tmessage = tmessage.replaceAll("#name#", user.getUserName());
                        tmessage = tmessage.replaceAll("#channel#", channel.getName());
                        tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                        tmessage = tmessage.replaceAll("#password#", strPassword);
                    }

                    //end of addition
                    SendNotification send = new SendNotification();

                    AXIOMStatus status = send.SendOnMobileNoWaiting(channel.getChannelid(),
                            user.getPhoneNo(),
                            tmessage,
                            1,
                            Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                    String resultString = "failed";
                    if (status != null) {
                        if (status.iStatus == 2 || status.iStatus == 0) {
                            aStatus.error = "Success";
                            aStatus.errorcode = 0;
                            resultString = "Success";
                            audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                    channel.getName(),
                                    session.getLoginid(), session.getLoginid(), new Date(), "Resend User Password", resultString, retValue,
                                    "User Management", "Old Password =*****", "New Password =*****",
                                    "USERPASSWORD", user.userId);

                        } else {
                            aStatus.error = status.strStatus;
                            aStatus.errorcode = status.iStatus;
                            audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                    channel.getName(),
                                    session.getLoginid(), session.getLoginid(), new Date(), "Resend User Password", resultString, retValue,
                                    "User Management", "Old Password =*****", "Failed To Send Password",
                                    "USERPASSWORD", user.userId);
                        }
                    } else {
                        aStatus.error = "Failed To send Password";
                        aStatus.errorcode = -1;
                        audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                channel.getName(),
                                session.getLoginid(), session.getLoginid(), new Date(), "Resend User Password", resultString, retValue,
                                "User Management", "Old Password =*****", "Failed To Send Password",
                                "USERPASSWORD", user.getUserId());
                    }
                    return aStatus;
                }

//            if (retValue == 0) {
//                aStatus.error = "SUCCESS";
//                aStatus.errorcode = 0;
//            }
            }

        }
        return aStatus;
    }

    private AxiomStatus SendPfxFile(Sessions session, Channels channel, AuthUser user, HttpServletRequest req) {

        AxiomStatus aStatus = new AxiomStatus();
        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.PKI_CERTIFICATE) != 0) {

            aStatus.error = "Digital Certificate feature is not available in this license!!!";
            aStatus.errorcode = -100;

            return aStatus;
        }
        AuditManagement audit = new AuditManagement();

        int retValue = -1;
        aStatus.error = "ERROR";
        aStatus.errorcode = retValue;

        if (session != null) {
            //System.out.println("Client IP = " + req.getRemoteAddr());        
            String PfxPassword = null;
            CertificateManagement cManagement = new CertificateManagement();
            String filepath = cManagement.getPFXFile(session.getSessionid(), channel.getChannelid(), user.getUserId());
            if (filepath != null) {
                retValue = 0;

                Templates temp = null;
                TemplateManagement tObj = new TemplateManagement();
                temp = tObj.LoadbyName(session.getSessionid(), channel.getChannelid(), TemplateNames.EMAIL_HARDWARE_PFX_FILE_TEMPLATE);
                if (temp.getStatus() == tObj.ACTIVE_STATUS) {
                    ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                    Date d = new Date();
                    String tmessage = (String) TemplateUtils.deserializeFromObject(bais);
                    String subject = temp.getSubject();
                    tmessage = tmessage.replaceAll("#name#", user.getUserName());
                    tmessage = tmessage.replaceAll("#channel#", channel.getName());
                    tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                    tmessage = tmessage.replaceAll("#email#", user.getEmail());
                    subject = subject.replaceAll("#name#", user.getUserName());
                    subject = subject.replaceAll("#channel#", channel.getName());
                    subject = subject.replaceAll("#datetime#", sdf.format(d));

                    String[] files = new String[1];
                    files[0] = filepath;

                    String[] filemimetypes = new String[1];
                    filemimetypes[0] = "application/x-pkcs12";

                    SendNotification send = new SendNotification();
                    AXIOMStatus status = send.SendEmail(channel.getChannelid(), user.getEmail(), subject, tmessage, null, null, files, filemimetypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                    String resultString = "Failed";
                    if (status != null) {
                        if (status.iStatus == 0 || status.iStatus == 2) { // for success and pending 
                            aStatus.error = "Success";
                            aStatus.errorcode = 0;
                            resultString = "Success";
                            audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(),
                                    req.getRemoteAddr(),
                                    //ipaddress, 
                                    channel.getName(),
                                    session.getLoginid(), session.getLoginid(), new Date(), "SendPFXFile",
                                    resultString, retValue, "PKI TOKEN Management", "", "Send pfx file to =" + user.getUserName(), "PKITOKEN", user.getUserId());
                        } else {
                            resultString = "Failure";
                            aStatus.error = status.strStatus;
                            aStatus.errorcode = status.iStatus;
                            audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(),
                                    req.getRemoteAddr(),
                                    //ipaddress, 
                                    channel.getName(),
                                    session.getLoginid(), session.getLoginid(), new Date(), "SendPFXFile",
                                    resultString, retValue, "PKI TOKEN Management", "", "Send pfx file to =" + user.getUserName(), "PKITOKEN", user.getUserId());

                        }
                    } else {
                        resultString = "Failure";
                        aStatus.error = "Failed To send pfx file";
                        aStatus.errorcode = -1;
                        audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(),
                                req.getRemoteAddr(),
                                //ipaddress, 
                                channel.getName(),
                                session.getLoginid(), session.getLoginid(), new Date(), "SendPFXFile",
                                resultString, retValue, "PKI TOKEN Management", "", "Send pfx file to =" + user.getUserName(), "PKITOKEN", user.getUserId());

                    }
                }
            }
        }

        return aStatus;
    }

    private AxiomStatus SendPfxPassword(Sessions session, Channels channel, AuthUser user, HttpServletRequest req) {
        AxiomStatus aStatus = new AxiomStatus();
        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.PKI_CERTIFICATE) != 0) {

            aStatus.error = "Digital Certificate feature is not available in this license!!!";
            aStatus.errorcode = -101;
            return aStatus;
        }

        AuditManagement audit = new AuditManagement();

        int retValue = -1;
        aStatus.error = "ERROR";
        aStatus.errorcode = retValue;

        if (session != null) {
            //System.out.println("Client IP = " + req.getRemoteAddr());        
            String PfxPassword = null;
            CertificateManagement pManagement = new CertificateManagement();

            PfxPassword = pManagement.getPFXPassWord(session.getSessionid(), channel.getChannelid(), user.getUserId());

            if (PfxPassword != null) {
                retValue = 0;

                Templates temp = null;
                TemplateManagement tObj = new TemplateManagement();
                temp = tObj.LoadbyName(session.getSessionid(), channel.getChannelid(), TemplateNames.MOBILE_HARDWARE_PFX_PASSWORD_TEMPLATE);

                if (temp.getStatus() == tObj.ACTIVE_STATUS) {
                    ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                    Date d = new Date();
                    String tmessage = (String) TemplateUtils.deserializeFromObject(bais);
                    tmessage = tmessage.replaceAll("#name#", user.getUserName());
                    tmessage = tmessage.replaceAll("#channel#", channel.getName());
                    tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                    tmessage = tmessage.replaceAll("#password#", PfxPassword);

                    SendNotification send = new SendNotification();
                    AXIOMStatus status = send.SendOnMobileNoWaiting(channel.getChannelid(), user.getPhoneNo(), tmessage, 1, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                    String resultString = "Failed";
                    if (status != null) {
                        if (status.iStatus == 0 || status.iStatus == 2) { // for success and pending 
                            aStatus.error = "Success";
                            aStatus.errorcode = 0;
                            resultString = "Success";
                            audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(),
                                    req.getRemoteAddr(),
                                    //ipaddress, 
                                    channel.getName(),
                                    session.getLoginid(), session.getLoginid(), new Date(), "SendPFXPassword",
                                    resultString, retValue, "PKI TOKEN Management", "", "Send PFX Password to=" + user.getUserName() + "******", "PKITOKEN", user.getUserId());
                        } else {
                            resultString = "Failure";
                            aStatus.error = status.strStatus;
                            aStatus.errorcode = status.iStatus;
                            audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(),
                                    req.getRemoteAddr(),
                                    //ipaddress, 
                                    channel.getName(),
                                    session.getLoginid(), session.getLoginid(), new Date(), "SendPFXPassword",
                                    resultString, retValue, "PKI TOKEN Management", "", "Failed to send pfx password", "PKITOKEN", user.getUserId());

                        }
                    } else {
                        resultString = "Failure";
                        aStatus.error = "Failed To send Password";
                        aStatus.errorcode = -1;
                        audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(),
                                req.getRemoteAddr(),
                                //ipaddress, 
                                channel.getName(),
                                session.getLoginid(), session.getLoginid(), new Date(), "SendPFXPassword",
                                resultString, retValue, "PKI TOKEN Management", "", "Failed to send pfx password", "PKITOKEN", user.getUserId());

                    }
                }
            }
        }

        return aStatus;
    }

    private AxiomStatus SendCertificateFile(Sessions session, Channels channel, AuthUser user, HttpServletRequest req) {

        AxiomStatus aStatus = new AxiomStatus();
        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.PKI_CERTIFICATE) != 0) {

            aStatus.error = "Digital Certificate feature is not available in this license!!!";
            aStatus.errorcode = -100;

            return aStatus;
        }
        AuditManagement audit = new AuditManagement();

        int retValue = -1;
        aStatus.error = "ERROR";
        aStatus.errorcode = retValue;

        if (session != null) {
            //System.out.println("Client IP = " + req.getRemoteAddr());        
            String PfxPassword = null;
            CertificateManagement cManagement = new CertificateManagement();

            String filepath = cManagement.getCertFile(session.getSessionid(), channel.getChannelid(), user.getUserId());
            if (filepath != null) {
                retValue = 0;

                Templates temp = null;
                TemplateManagement tObj = new TemplateManagement();
                temp = tObj.LoadbyName(session.getSessionid(), channel.getChannelid(), TemplateNames.EMAIL_CERTIFICATE_FILE_TEMPLATE);
                if (temp.getStatus() == tObj.ACTIVE_STATUS) {
                    ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                    Date d = new Date();
                    String tmessage = (String) TemplateUtils.deserializeFromObject(bais);
                    String subject = temp.getSubject();
                    tmessage = tmessage.replaceAll("#name#", user.getUserName());
                    tmessage = tmessage.replaceAll("#channel#", channel.getName());
                    tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                    tmessage = tmessage.replaceAll("#email#", user.getEmail());
                    subject = subject.replaceAll("#name#", user.getUserName());
                    subject = subject.replaceAll("#channel#", channel.getName());
                    subject = subject.replaceAll("#datetime#", sdf.format(d));

                    String[] files = new String[1];
                    files[0] = filepath;

                    String[] filemimetypes = new String[1];
                    filemimetypes[0] = "application/pkix-cert";

                    SendNotification send = new SendNotification();
                    AXIOMStatus status = send.SendEmail(channel.getChannelid(), user.getEmail(), subject, tmessage, null, null, files, filemimetypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                    String resultString = "Failed";
                    if (status != null) {
                        if (status.iStatus == 0 || status.iStatus == 2) { // for success and pending 
                            aStatus.error = "Success";
                            aStatus.errorcode = 0;
                            resultString = "Success";

                            audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(),
                                    req.getRemoteAddr(),
                                    //ipaddress, 
                                    channel.getName(),
                                    session.getLoginid(), session.getLoginid(), new Date(), "SendPFXFile",
                                    resultString, retValue, "PKI TOKEN Management", "User Name=" + user.getUserName() + ",Email=" + user.getEmail() + ",Phone" + user.getPhoneNo(), "Cert send successfully to " + user.getUserName(), "PKITOKEN", user.getUserId());
                        } else {
                            resultString = "Failure";
                            aStatus.error = status.strStatus;
                            aStatus.errorcode = status.iStatus;
                            audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(),
                                    req.getRemoteAddr(),
                                    //ipaddress, 
                                    channel.getName(),
                                    session.getLoginid(), session.getLoginid(), new Date(), "SendPFXFile",
                                    resultString, retValue, "PKI TOKEN Management", "User Name=" + user.getUserName() + ",Email=" + user.getEmail() + ",Phone" + user.getPhoneNo(),
                                    "Failed to send certificate to =" + user.getUserName(), "PKITOKEN", user.getUserId());
                        }
                    } else {
                        resultString = "Failure";
                        aStatus.error = "Failed To send certificate file";
                        aStatus.errorcode = -1;
                        audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(),
                                req.getRemoteAddr(),
                                //ipaddress, 
                                channel.getName(),
                                session.getLoginid(), session.getLoginid(), new Date(), "SendPFXFile",
                                resultString, retValue, "PKI TOKEN Management", "User Name=" + user.getUserName() + ",Email=" + user.getEmail() + ",Phone" + user.getPhoneNo(),
                                "Failed to send certificate to =" + user.getUserName(), "PKITOKEN", user.getUserId());

                    }
                }
            }
        }

        return aStatus;
    }

    private AxiomStatus SendSignatureOTP(Sessions session, Channels channel, String userid, String[] data, HttpServletRequest req) {

        AuditManagement audit = new AuditManagement();

        AxiomStatus aStatus = new AxiomStatus();

        int retValue = -1;
        aStatus.error = "ERROR";
        aStatus.errorcode = retValue;

        String strCategory = "OOB_TOKEN";
        String strSubCategory = "";

        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
        if (session != null) {

            SettingsManagement setManagement = new SettingsManagement();

            TokenStatusDetails[] tokens = oManagement.getTokenList(session.getSessionid(), session.getChannelid(), userid);
            if (tokens == null) {
                aStatus.error = "No Token Assigned to this user!!!";
                aStatus.errorcode = -2;
                return aStatus;
            }

            TokenStatusDetails tokenSelected = null;
            for (int i = 0; i < tokens.length; i++) {
                if (tokens[i].Catrgory == OTPTokenManagement.OOB_TOKEN) {
                    tokenSelected = tokens[i];
                    break;
                }
            }

            if (tokenSelected == null) {
                aStatus.errorcode = -12;
                aStatus.error = "Desired OOB Token is not assigned!!!";
                return aStatus;
            }
            if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_SMS) {
                strSubCategory = "OOB__SMS_TOKEN";
            } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_USSD) {
                strSubCategory = "OOB__USSD_TOKEN";
            } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_VOICE) {
                strSubCategory = "OOB__VOICE_TOKEN";
            } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_EMAIL) {
                strSubCategory = "OOB__EMAIL_TOKEN";
            }

            retValue = oManagement.SendSignatureOTP(session.getChannelid(), userid, session.getSessionid(), data, OTP_TOKEN_OUTOFBAND, tokenSelected.SubCategory);

            if (retValue == 0 || retValue == 2) { //pending and success
                aStatus.error = "SUCCESS";
                aStatus.errorcode = 0;
            } else if (retValue == -22) {
                aStatus.error = "Token is locked!!!";
                aStatus.errorcode = retValue;
            } else {
                aStatus.error = "Signautre OTP message could not sent...";
                aStatus.errorcode = retValue;
            }

        }

        audit.AddAuditTrail(session.getSessionid(), channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                channel.getName(),
                session.getLoginid(), session.getLoginid(),
                new Date(),
                "SEND SIGNATURE OTP", aStatus.error, aStatus.errorcode,
                "Token Management",
                "",
                "Category=" + strCategory + ",Subcategory=" + strSubCategory,
                "OTPTOKENS", userid);

        return aStatus;
    }

    @Override
    public AxiomStatus IsRegistered(String sessionid, RSSUserCerdentials credentials, String integrityCheckString, String trustPayLoad) {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");

            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                System.out.println("IsRegistered::sessionid::" + sessionid);
                System.out.println("IsRegistered::userid::" + credentials.rssUser.userId);
                for (int i = 0; i < credentials.tokenDetails.length; i++) {
                    System.out.println("Categorey" + credentials.tokenDetails[i].category);
                    System.out.println("SubCategory" + credentials.tokenDetails[i].subcategory);
                }
            }
        } catch (Exception ex) {
        }
//nilesh
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Licence is invalid";
            aStatus.errorcode = -100;
            return aStatus;
        }
        AxiomStatus aStatus = new AxiomStatus();

        byte[] SHA1hash = null;
        if (trustPayLoad == null) {
            SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.rssUser.emailid + credentials.rssUser.userId + credentials.rssUser.phoneNumber + credentials.rssUser.userName);
        } else {
            SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.rssUser.emailid + credentials.rssUser.userId + credentials.rssUser.phoneNumber + credentials.rssUser.userName + trustPayLoad);
        }
//        String integritycheck = new String(Base64.encode(SHA1hash));
        String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//        String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java

        if (integrityCheckString != null) {
            if (integrityCheckString.equals(integritycheck) != true) {
                aStatus.error = "Input Data is Tampered!!!";
                aStatus.errorcode = -76;
                return aStatus;
            }
        }

        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);
        if (session == null) {
            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
            return aStatus;
        }
        aStatus.error = "ERROR";
        aStatus.errorcode = -2;
        String oldValue = "";
        String strValue = "";
        String UserId = credentials.rssUser.userId;
        int retValue = -1;
        if (session != null) {
            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(channelid);
            if (channel == null) {
                return null;
            }
            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
            if (channelProfileObj != null) {
                int retVal = sManagement.getServerStatus(channelProfileObj);
                if (retVal != 0) {
                    aStatus = new AxiomStatus();
                    aStatus.error = "Server Down for maintainance,Please try later!!!";
                    aStatus.errorcode = -48;
                    return aStatus;
                }
            }

            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        OperatorsManagement oManagement = new OperatorsManagement();
                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
                            return aStatus;
                        }

                        suTemplate.close();
                        sTemplate.close();
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        return aStatus;
                    }
                    aStatus.error = "INVALID IP REQUEST";
                    aStatus.errorcode = -8;
                    return aStatus;
                }
            }
            boolean bSentToUser = true;
            int subcategory = 1;
            if (trustPayLoad != null) {
                MobileTrustSettings mSettings = null;

                Object obj = setManagement.getSetting(sessionid, channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
                if (obj != null) {
                    if (obj instanceof MobileTrustSettings) {
                        mSettings = (MobileTrustSettings) obj;
                    }
                }
                if (mSettings == null) {
                    aStatus.error = "Mobile Trust Setting is not configured!!!";
                    aStatus.errorcode = -23;
                    return aStatus;
                }
                if (mSettings.bGeoFencing == true) {

                    GeoLocationManagement gManagement = new GeoLocationManagement();
                    AXIOMStatus status = gManagement.validateGeoLocation(sessionid, channelid, UserId, trustPayLoad);
                    if (status == null) {
                        aStatus.error = "Location Not Found!!!";
                        aStatus.errorcode = -115;
                    } else if (status.iStatus != 0) {

                        aStatus.error = status.strStatus;
                        aStatus.errorcode = status.iStatus;
                        audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                                session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorcode,
                                "Token Management", "Invalid Location", null, itemtype,
                                session.getLoginid());

                        return aStatus;
                    }
                }
            }
            for (int i = 0; i < credentials.tokenDetails.length; i++) {
                if (credentials.tokenDetails[i] != null) {
                    //nilesh
                    if (credentials.tokenDetails[i].category == AxiomCredentialDetails.SECURE_PHRASE) {
                        try {
                            String jsonString = new String(Base64.decode(trustPayLoad));
                            JSONObject json = new JSONObject(jsonString);
                            String ipAddress = json.getString("ip");
                            String longi = json.getString("longi");
                            String latti = json.getString("latti");
                            String strTxType = json.getString("txType");
                            MobileTrustManagment mManagment = new MobileTrustManagment();
                            Location srvLoc = null;
//                             srvLoc = mManagment.getLocationByLongAndLat(longi, latti);
                            SecurePhraseManagment sMngt = new SecurePhraseManagment();
                            Securephrase Securephrase = sMngt.GetSecurePhrase(sessionid, session.getChannelid(), UserId);
                            if (Securephrase == null) {
                                srvLoc = mManagment.getLocationByLongAndLat(longi, latti);
                                sMngt.AddSecureTrap(sessionid, channelid, UserId,
                                        -1, -1, null, SecurePhraseManagment.IS_NOT_REGISTERED);
                                aStatus.error = "Secure Phrase Not Set!!!";
                                aStatus.errorcode = -1;
                                return aStatus;
                            } else {
                                srvLoc = mManagment.getLocationByLongAndLat(longi, latti);
                                sMngt.AddSecureTrap(sessionid, channelid, UserId,
                                        -1, -1, null, SecurePhraseManagment.IS_REGISTERED);
                                aStatus.error = "SUCCESS";
                                aStatus.errorcode = 0;
                                return aStatus;
                            }
                        } //nilesh
                        catch (Exception ex) {
                            Logger.getLogger(RSSCoreInterfaceImpl.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }

        }
        return aStatus;
    }

    @Override
    public String CheckUser(String sessionid, RSSUserCerdentials credentials, String cookie, String integrityCheckString, String trustPayLoad) throws AxiomException {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");

            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                System.out.println("CheckUser::sessionid::" + sessionid);
                System.out.println("CheckUser::userid::" + credentials.rssUser.userId);
                for (int i = 0; i < credentials.tokenDetails.length; i++) {
                    System.out.println("Categorey" + credentials.tokenDetails[i].category);
                    System.out.println("SubCategory" + credentials.tokenDetails[i].subcategory);
                }
            }
        } catch (Exception ex) {
        }
//nilesh
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Licence is invalid";
            aStatus.errorcode = -100;
//            return aStatus;
            throw new AxiomException(aStatus.error);
        }
        AxiomStatus aStatus = new AxiomStatus();

        byte[] SHA1hash = null;
        if (trustPayLoad == null) {
            SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.rssUser.emailid + credentials.rssUser.userId + credentials.rssUser.phoneNumber + credentials.rssUser.userName + cookie);
        } else {
            SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.rssUser.emailid + credentials.rssUser.userId + credentials.rssUser.phoneNumber + credentials.rssUser.userName + cookie + trustPayLoad);
        }
//        String integritycheck = new String(Base64.encode(SHA1hash));
        String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//        String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java

        if (integrityCheckString != null) {
            if (integrityCheckString.equals(integritycheck) != true) {
//                aStatus.error = "Input Data is Tampered!!!";
//                aStatus.errorcode = -76;
//                return aStatus;
                throw new AxiomException("Input Data is Tampered!!!");
            }
        }

        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);
        if (session == null) {
//            aStatus.error = "Invalid Session";
//            aStatus.errorcode = -14;
//            return aStatus;
            throw new AxiomException("Invalid Session");
        }
        aStatus.error = "ERROR";
        aStatus.errorcode = -2;
        String oldValue = "";
        String strValue = "";
        String UserId = credentials.rssUser.userId;
        int retValue = -1;
        if (session != null) {
            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(channelid);
            if (channel == null) {
                return null;
            }
            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
            if (channelProfileObj != null) {
                int retVal = sManagement.getServerStatus(channelProfileObj);
                if (retVal != 0) {
//                    aStatus = new AxiomStatus();
//                    aStatus.error = "Server Down for maintainance,Please try later!!!";
//                    aStatus.errorcode = -48;
//                    return aStatus;
                    throw new AxiomException("Server Down for maintainance,Please try later!!!");
                }
            }

            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        OperatorsManagement oManagement = new OperatorsManagement();
                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
//                            aStatus.error = "INVALID IP REQUEST";
//                            aStatus.errorcode = -8;
//                            return aStatus;
                            throw new AxiomException("INVALID IP REQUEST");
                        }

                        suTemplate.close();
                        sTemplate.close();
//                        aStatus.error = "INVALID IP REQUEST";
//                        aStatus.errorcode = -8;
//                        return aStatus;
                        throw new AxiomException("INVALID IP REQUEST");
                    }
//                    aStatus.error = "INVALID IP REQUEST";
//                    aStatus.errorcode = -8;
//                    return aStatus;
                    throw new AxiomException("INVALID IP REQUEST");
                }
            }
            boolean bSentToUser = true;
            int subcategory = 1;
            if (trustPayLoad != null) {
                MobileTrustSettings mSettings = null;

                Object obj = setManagement.getSetting(sessionid, channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
                if (obj != null) {
                    if (obj instanceof MobileTrustSettings) {
                        mSettings = (MobileTrustSettings) obj;
                    }
                }
                if (mSettings == null) {
//                    aStatus.error = "Mobile Trust Setting is not configured!!!";
//                    aStatus.errorcode = -23;
//                    return aStatus;
                    throw new AxiomException("Mobile Trust Setting is not configured!!!");
                }
                if (mSettings.bGeoFencing == true) {

                    GeoLocationManagement gManagement = new GeoLocationManagement();
                    AXIOMStatus status = gManagement.validateGeoLocation(sessionid, channelid, UserId, trustPayLoad);
                    if (status == null) {
                        aStatus.error = "Location Not Found!!!";
                        aStatus.errorcode = -115;
                    } else if (status.iStatus != 0) {

                        aStatus.error = status.strStatus;
                        aStatus.errorcode = status.iStatus;
                        audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                                session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorcode,
                                "Token Management", "Invalid Location", null, itemtype,
                                session.getLoginid());

//                        return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }
            }
            for (int i = 0; i < credentials.tokenDetails.length; i++) {
                if (credentials.tokenDetails[i] != null) {
                    //nilesh
                    if (credentials.tokenDetails[i].category == AxiomCredentialDetails.SECURE_PHRASE) {
                        try {
                            SecurePhraseManagment sMngt = new SecurePhraseManagment();
                            String ipAddress = null;
                            String longi = null;
                            String latti = null;
                            if (trustPayLoad != null) {
                                String jsonString = new String(Base64.decode(trustPayLoad));
                                JSONObject json = new JSONObject(jsonString);
                                ipAddress = json.getString("ip");
                                longi = json.getString("longi");
                                latti = json.getString("latti");
                                String strTxType = json.getString("txType");
                            }
                            MobileTrustManagment mManagment = new MobileTrustManagment();
                            Location srvLoc = null;

                            Securephrase Securephrase = sMngt.GetSecurePhrase(sessionid, session.getChannelid(), UserId);
                            if (cookie == null && Securephrase != null) {
                                if (trustPayLoad != null) {
                                    srvLoc = mManagment.getLocationByLongAndLat(longi, latti);
                                    sMngt.EditSecureTrap(sessionid, channelid, UserId,
                                            -1, -1, null, SecurePhraseManagment.IS_NOT_REGISTERED_DEVICE);
                                } else {
                                    sMngt.EditSecureTrap(sessionid, channelid, UserId,
                                            -1, -1, null, SecurePhraseManagment.IS_NOT_REGISTERED_DEVICE);
                                }

                                aStatus.error = "Not a registered device!!!";
                                aStatus.errorcode = -1;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            } else {
                                Securephrase securePhraseObj = sMngt.CheckUser(sessionid, channelid, UserId, cookie);
                                if (securePhraseObj == null) {

                                    if (trustPayLoad != null) {
                                        srvLoc = mManagment.getLocationByLongAndLat(longi, latti);
                                        sMngt.EditSecureTrap(sessionid, channelid, UserId,
                                                -1, -1, null, SecurePhraseManagment.IS_NOT_REGISTERED_DEVICE);
                                    } else {
                                        sMngt.EditSecureTrap(sessionid, channelid, UserId,
                                                -1, -1, null, SecurePhraseManagment.IS_NOT_REGISTERED_DEVICE);
                                    }
                                    aStatus.error = "Record Not Found!!!";
                                    aStatus.errorcode = -1;
//                            return aStatus;
                                    throw new AxiomException(aStatus.error);
                                } else {

                                    if (trustPayLoad != null) {
                                        srvLoc = mManagment.getLocationByLongAndLat(longi, latti);
                                        sMngt.EditSecureTrap(sessionid, channelid, UserId,
                                                -1, -1, null, SecurePhraseManagment.REGISTERED_DEVICE);
                                    } else {
                                        sMngt.EditSecureTrap(sessionid, channelid, UserId,
                                                -1, -1, null, SecurePhraseManagment.REGISTERED_DEVICE);
                                    }
                                    aStatus.error = "Record Not Found!!!";
                                    aStatus.errorcode = -1;
//                            return aStatus;
//                              throw new AxiomException(aStatus.error);
                                    return securePhraseObj.getSecurephraseid();
                                }

                            }
                        } //nilesh
                        catch (Exception ex) {
                            Logger.getLogger(RSSCoreInterfaceImpl.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }

        }
//        return aStatus;
        throw new AxiomException(aStatus.error);
    }

    @Override
    public Securephrase GetImage(String sessionid, RSSUserCerdentials credentials, String cookie, String authToken, String integrityCheckString, String trustPayLoad) throws AxiomException {
        String strDebug = null;
        System.out.println("GetImage Request Receivd At = " + new Date());
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");

            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                System.out.println("GetImage::sessionid::" + sessionid);
                System.out.println("GetImage::userid::" + credentials.rssUser.userId);
                for (int i = 0; i < credentials.tokenDetails.length; i++) {
                    System.out.println("Categorey" + credentials.tokenDetails[i].category);
                    System.out.println("SubCategory" + credentials.tokenDetails[i].subcategory);
                }
            }
        } catch (Exception ex) {
        }
//nilesh
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Licence is invalid";
            aStatus.errorcode = -100;
//            return aStatus;
            throw new AxiomException(aStatus.error);
        }
        AxiomStatus aStatus = new AxiomStatus();

        byte[] SHA1hash = null;
        if (trustPayLoad == null) {
            SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.rssUser.emailid + credentials.rssUser.userId + credentials.rssUser.phoneNumber + credentials.rssUser.userName);
        } else {
            SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.rssUser.emailid + credentials.rssUser.userId + credentials.rssUser.phoneNumber + credentials.rssUser.userName + trustPayLoad);
        }
        String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//        String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java
        if (integrityCheckString != null) {
            if (integrityCheckString.equals(integritycheck) != true) {
//                aStatus.error = "Input Data is Tampered!!!";
//                aStatus.errorcode = -76;
//                return aStatus;
                throw new AxiomException("Input Data is Tampered!!!");
            }
        }

        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);
        if (session == null) {
//            aStatus.error = "Invalid Session";
//            aStatus.errorcode = -14;
//            return aStatus;
            throw new AxiomException("Invalid Session");
        }
        aStatus.error = "ERROR";
        aStatus.errorcode = -2;
        String oldValue = "";
        String strValue = "";
        String UserId = credentials.rssUser.userId;
        int retValue = -1;
        if (session != null) {
            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(channelid);
            if (channel == null) {
                return null;
            }
            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
            if (channelProfileObj != null) {
                int retVal = sManagement.getServerStatus(channelProfileObj);
                if (retVal != 0) {
//                    aStatus = new AxiomStatus();
//                    aStatus.error = "Server Down for maintainance,Please try later!!!";
//                    aStatus.errorcode = -48;
//                    return aStatus;
                    throw new AxiomException("Server Down for maintainance,Please try later!!!");
                }
            }

            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        OperatorsManagement oManagement = new OperatorsManagement();
                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
//                        
                            throw new AxiomException("INVALID IP REQUEST");
                        }

                        suTemplate.close();
                        sTemplate.close();
//                        
                        throw new AxiomException("INVALID IP REQUEST");
                    }
//                  
                    throw new AxiomException("INVALID IP REQUEST");
                }
            }
            boolean bSentToUser = true;
            int subcategory = 1;
            ImageSettings iSettings = null;
            Object obj1 = setManagement.getSetting(sessionid, channelid, SettingsManagement.IMAGE_SETTINGS, SettingsManagement.PREFERENCE_ONE);
            if (obj1 != null) {
                if (obj1 instanceof ImageSettings) {
                    iSettings = (ImageSettings) obj1;
                }
            }
            if (trustPayLoad != null) {
                MobileTrustSettings mSettings = null;

                Object obj = setManagement.getSetting(sessionid, channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
                if (obj != null) {
                    if (obj instanceof MobileTrustSettings) {
                        mSettings = (MobileTrustSettings) obj;
                    }
                }
                if (mSettings == null) {

                    throw new AxiomException("Mobile Trust Setting is not configured!!!");
                }
                if (iSettings != null) {
                    if (iSettings._geoCheck == 1) {//1- Active ; 0 - inActive
                        if (mSettings.bGeoFencing == true) {

                            GeoLocationManagement gManagement = new GeoLocationManagement();
                            AXIOMStatus status = gManagement.validateGeoLocation(sessionid, channelid, UserId, trustPayLoad);
                            if (status == null) {
                                aStatus.error = "Location Not Found!!!";
                                aStatus.errorcode = -115;
                            } else if (status.iStatus == -24) {
                                try {
                                    byte[] byteTrustPayLoad = Base64.decode(trustPayLoad);
                                    JSONObject jObj = new JSONObject(new String(byteTrustPayLoad));
                                    String ipAddress = jObj.getString("ip");
                                    String longitude = jObj.getString("longi");
                                    String lattitude = jObj.getString("latti");
                                    int txType = jObj.getInt("txType");
                                    String locationChunk = jObj.getString("locationChunk");
                                    int locationType = jObj.getInt("locationType");
                                    MobileTrustManagment tSettings = new MobileTrustManagment();
                                    byte[] bytelocationChunk = Base64.decode(locationChunk.getBytes());
                                    Location loc = tSettings.getLocationFromJSON(new String(bytelocationChunk), locationType);

                                    if (loc != null) {
//                                        aStatus.iStatus = -77;
//                                        aStatus.strStatus = "User Location Not Found!!!";
//                                        return aStatus;
                                        SessionFactoryUtil geouSession = new SessionFactoryUtil(SessionFactoryUtil.geotrack);
                                        Session geoSession = geouSession.openSession();
                                        GeoTrackingUtils geoUtil = new GeoTrackingUtils(geouSession, geoSession);
                                        int val = geoUtil.addGeotrack(channelid, UserId, longitude, lattitude,
                                                ipAddress, loc.city, loc.state, loc.short_name, loc.zipcode, txType, new Date());
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }

                            } else {

                                aStatus.error = status.strStatus;
                                aStatus.errorcode = status.iStatus;
//                                audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
//                                        session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorcode,
//                                        "Token Management", "Invalid Location", null, itemtype,
//                                        session.getLoginid());

                            }
                        }
                    }
                }
            }
            for (int i = 0; i < credentials.tokenDetails.length; i++) {
                if (credentials.tokenDetails[i] != null) {
                    //nilesh
                    if (credentials.tokenDetails[i].category == AxiomCredentialDetails.SECURE_PHRASE) {

                        System.out.println("GetImage > Start Generating Image = " + new Date());

                        SecurePhraseManagment sMngt = new SecurePhraseManagment();
                        Securephrase securePhraseObj = null;
                        String sampleText = null;
                        int icolor = 0;
                        int randomNum = 0;
                        int randomNumC = 0;
                        if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.SECURE_PHRASE_INVALIDIMAGE) {
                            if (iSettings._honeyTrap == 0) {//0 -disable ;1- Active
                                return null;
                            }

                            Phrase ph = new Phrase();
                            String[] arrPhrase = ph.getPhrase();
                            randomNum = 0 + (int) (Math.random() * arrPhrase.length);
                            sampleText = ph.getCode("" + randomNum);
                            sampleText = "  " + sampleText + "   ";
                            String[] arrColor = ph.getColor();
                            randomNumC = 0 + (int) (Math.random() * arrColor.length);
//                            String sColor = arrColor[randomNumC];
//                            if (sColor != null) {
//                                icolor = Integer.parseInt(sColor);
//                            }
                            icolor = randomNumC;
//                            sMngt.EditSecureTrap(sessionid, channelid, UserId,
//                                    -1, -1, null, SecurePhraseManagment.INVALID_IMAGE);
                            sMngt.AddSecureTrap(sessionid, channelid, UserId, -1, -1, "",
                                    SecurePhraseManagment.INVALID_IMAGE);
                        } else if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.SECURE_PHRASE_VALIDIMAGE) {
                            sampleText = credentials.tokenDetails[i].securePhrase;
                            sampleText = "  " + sampleText + "   ";
                            sMngt.EditSecureTrap(sessionid, channelid, UserId,
                                    -1, -1, null, SecurePhraseManagment.VALID_IMAGE);
                        }

                        try {
                            aStatus.error = "Record Not Found!!!";
                            aStatus.errorcode = -1;
//                            Font font = new Font("Arial", Font.BOLD, 26);
                            Font font = new Font("Arial", Font.BOLD | Font.ITALIC, 26);
                            FontRenderContext frc = new FontRenderContext(null, true, true);
                            java.awt.geom.Rectangle2D bounds = font.getStringBounds(sampleText, frc);
                            int w = (int) bounds.getWidth();
                            int h = (int) bounds.getHeight();
                            BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
                            Graphics2D g = image.createGraphics();

                            if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.SECURE_PHRASE_INVALIDIMAGE) {
                                if (icolor == SecurePhraseManagment.BLUE) {
                                    g.setColor(Color.BLUE);
                                } else if (icolor == SecurePhraseManagment.GREEN) {
                                    g.setColor(Color.GREEN);
                                } else if (icolor == SecurePhraseManagment.RED) {
                                    g.setColor(Color.RED);
                                } else if (icolor == SecurePhraseManagment.GRAY) {
                                    g.setColor(Color.GRAY);
                                } else if (icolor == SecurePhraseManagment.ORANGE) {
                                    g.setColor(Color.ORANGE);
                                } else if (icolor == SecurePhraseManagment.YELLOW) {
                                    g.setColor(Color.YELLOW);
                                } else if (icolor == SecurePhraseManagment.MAGENTA) {
                                    g.setColor(Color.MAGENTA);
                                }
                                g.fillRect(0, 0, w, h);
                                g.setColor(Color.WHITE);
                            } else if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.SECURE_PHRASE_VALIDIMAGE) {

                                Color c = SecurePhraseManagment.hex2Rgb(credentials.tokenDetails[i].securePhraseColor);
                                g.setColor(c);
                                g.fillRect(0, 0, w, h);
                                if (credentials.tokenDetails[i].securePhraseColor.equals("#AAAAAA")
                                        || credentials.tokenDetails[i].securePhraseColor.equals("#BBBBBB")
                                        || credentials.tokenDetails[i].securePhraseColor.equals("#CCCCCC")
                                        || credentials.tokenDetails[i].securePhraseColor.equals("#DDDDDD")
                                        || credentials.tokenDetails[i].securePhraseColor.equals("#EEEEEE")
                                        || credentials.tokenDetails[i].securePhraseColor.equals("#FFFFFF")) {
                                    g.setColor(Color.BLACK);
                                } else {
                                    g.setColor(Color.WHITE);
                                }

                            }
                            g.setFont(font);
                            g.drawString(sampleText, (float) bounds.getX(), (float) -bounds.getY());
                            g.dispose();
                            String savepath = System.getProperty("catalina.home");
                            if (savepath == null) {
                                savepath = System.getenv("catalina.home");
                            }
                            savepath += System.getProperty("file.separator");
                            savepath += "axiomv2-settings";
                            savepath += System.getProperty("file.separator");
                            savepath += "user-phrase";
                            savepath += System.getProperty("file.separator");
                            long LongTime = new Date().getTime() / 1000;
                            File outputfile = new File(savepath + randomNum + randomNumC + LongTime + ".png");
                            ImageIO.write(image, "png", outputfile);

                            ByteArrayOutputStream baos = new ByteArrayOutputStream(1000);
                            BufferedImage img = ImageIO.read(new File(savepath + randomNum + randomNumC + LongTime + ".png"));
                            ImageIO.write(img, "png", baos);
                            baos.flush();

                            byte[] base64Image = Base64.encode(baos.toByteArray());
                            baos.close();
//                            byte[] plainImage = Base64.decode(base64Image);
                            File f = new File(savepath + randomNum + randomNumC + LongTime + ".png");
                            f.delete();

//                                        srvLoc = mManagment.getLocationByLongAndLat(longi, latti);
                            securePhraseObj = new Securephrase();
//                            securePhraseObj.setImageentry(plainImage);
                            securePhraseObj.setImageentry(base64Image);
                            System.out.println("GetImage > Image Created and return = " + new Date());
                            return securePhraseObj;
//                                        throw new AxiomException(aStatus.error);
                        } catch (Exception ex) {
                            Logger.getLogger(RSSCoreInterfaceImpl.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }

        }
//        return aStatus;
        throw new AxiomException(aStatus.error);
    }

    @Override
    public AuthenticationPackage GenerateAuthenticationPackage(String sessionid, RSSUserCerdentials credentials, String _deviceId, String authToken, String integrityCheckString, String trustPayLoad) throws AxiomException {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");

            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                System.out.println("GenerateAuthenticationPackage::sessionid::" + sessionid);
                System.out.println("GenerateAuthenticationPackage::userid::" + credentials.rssUser.userId);
                for (int i = 0; i < credentials.tokenDetails.length; i++) {
                    System.out.println("Categorey" + credentials.tokenDetails[i].category);
                    System.out.println("SubCategory" + credentials.tokenDetails[i].subcategory);
                }
            }
        } catch (Exception ex) {
        }
//nilesh
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "Licence is invalid";
//            aStatus.errorcode = -100;
////            return aStatus;            
//            throw new AxiomException(aStatus.error);
        }
        AxiomStatus aStatus = new AxiomStatus();

        byte[] SHA1hash = null;
        if (trustPayLoad == null) {
            SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.rssUser.emailid + credentials.rssUser.userId + credentials.rssUser.phoneNumber + credentials.rssUser.userName);
        } else {
            SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.rssUser.emailid + credentials.rssUser.userId + credentials.rssUser.phoneNumber + credentials.rssUser.userName + trustPayLoad);
        }
//        String integritycheck = new String(Base64.encode(SHA1hash));
        String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//        String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java
        if (integrityCheckString != null) {
            if (integrityCheckString.equals(integritycheck) != true) {
                throw new AxiomException("Input Data is Tampered!!!");
            }
        }

        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);
        if (session == null) {
            throw new AxiomException("Invalid Session");
        }
        aStatus.error = "ERROR";
        aStatus.errorcode = -2;
        String oldValue = "";
        String strValue = "";
        String UserId = credentials.rssUser.userId;
        int retValue = -1;
        if (session != null) {
            SettingsManagement setManagement = new SettingsManagement();
            String channelid = session.getChannelid();
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(channelid);
            if (channel == null) {
                return null;
            }
            AuthenticationPackage authPackage = new AuthenticationPackage();
            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                    authPackage.IpCheck = SecurePhraseManagment.SUCCESS_AUTH_PACKAGE;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        OperatorsManagement oManagement = new OperatorsManagement();
                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
//                            throw new AxiomException("INVALID IP REQUEST");
                            authPackage.IpCheck = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                        }

                        suTemplate.close();
                        sTemplate.close();
                        authPackage.IpCheck = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
//                        throw new AxiomException("INVALID IP REQUEST");
                    }
                    authPackage.IpCheck = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
//                    throw new AxiomException("INVALID IP REQUEST");
                }
            } else {
                authPackage.IpCheck = SecurePhraseManagment.DISABLE_AUTH_PACKAGE;
            }
            boolean bSentToUser = true;
            int subcategory = 1;

            AXIOMStatus status = null;
            ImageSettings iSettings = null;
            Object obj1 = setManagement.getSetting(sessionid, channelid, SettingsManagement.IMAGE_SETTINGS, SettingsManagement.PREFERENCE_ONE);
            if (obj1 != null) {
                if (obj1 instanceof ImageSettings) {
                    iSettings = (ImageSettings) obj1;
                }
            }

            if (iSettings._geoCheck == 1) {//1-Active ; 0- Suspended // check for geo

                if (trustPayLoad != null) {
                    MobileTrustSettings mSettings = null;

                    Object obj = setManagement.getSetting(sessionid, channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
                    if (obj != null) {
                        if (obj instanceof MobileTrustSettings) {
                            mSettings = (MobileTrustSettings) obj;
                        }
                    }
                    if (mSettings == null) {
                        audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                                session.getLoginid(), new Date(), "Generate Authentication Package", "Mobile Trust Setting is not configured!!!", aStatus.errorcode,
                                "Authentication Package", "", null, itemtype,
                                session.getLoginid());
                        throw new AxiomException("Mobile Trust Setting is not configured!!!");
                    }
                    if (mSettings.bGeoFencing == true) {

                        GeoLocationManagement gManagement = new GeoLocationManagement();
                        status = gManagement.validateGeoLocation(sessionid, channelid, UserId, trustPayLoad);
                        if (status == null) {
                            aStatus.error = "Location Not Found!!!";
                            aStatus.errorcode = -115;
//                            audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
//                                    session.getLoginid(), new Date(), "Generate Authentication Package", aStatus.error, aStatus.errorcode,
//                                    "Authentication Package", "", null, itemtype,
//                                    session.getLoginid());
                        } else if (status.iStatus == 0) {
                            authPackage.GeoCheck = SecurePhraseManagment.SUCCESS_AUTH_PACKAGE;
//                            authPackage.IpCheck = SecurePhraseManagment.SUCCESS_AUTH_PACKAGE;
                        } else if (status.iStatus != 0) {
                            authPackage.GeoCheck = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
//                            authPackage.IpCheck = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                            aStatus.error = status.strStatus;
                            aStatus.errorcode = status.iStatus;
                            audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                                    session.getLoginid(), new Date(), "Generate Authentication Package", aStatus.error, aStatus.errorcode,
                                    "Authentication Package", "", null, itemtype,
                                    session.getLoginid());
                            throw new AxiomException(aStatus.error);
                        }

                    }
                } else {
                    authPackage.GeoCheck = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
//                    authPackage.IpCheck = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                }
            } else {
                authPackage.GeoCheck = SecurePhraseManagment.DISABLE_AUTH_PACKAGE;
//                authPackage.IpCheck = SecurePhraseManagment.DISABLE_AUTH_PACKAGE;
            }

            if (iSettings._deviceProfile == 1) { // 1 - Active 0 - in Active
                if (_deviceId != null) {
                    TrustDeviceManagement deviceMngt = new TrustDeviceManagement();
                    Trusteddevice device = deviceMngt.getTrusteddeviceByDeviceId(sessionid, channelid, UserId, _deviceId);
                    if (device != null) {
                        if (device.getStatus() == 0 || device.getStatus() == 1) {
                            authPackage.RegisteredDevice = SecurePhraseManagment.SUCCESS_AUTH_PACKAGE;
                        } else {
                            authPackage.RegisteredDevice = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                        }
                    } else {
                        authPackage.RegisteredDevice = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                    }
                } else {
                    authPackage.RegisteredDevice = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                }
            } else {
                authPackage.RegisteredDevice = SecurePhraseManagment.DISABLE_AUTH_PACKAGE;
            }

//            authPackage.GeoCheck ==
            for (int i = 0; i < credentials.tokenDetails.length; i++) {
                if (credentials.tokenDetails[i] != null) {
                    //nilesh
                    if (credentials.tokenDetails[i].category == AxiomCredentialDetails.SECURE_PHRASE) {
                        try {
                            SecurePhraseManagment sMngt = new SecurePhraseManagment();
                            Securephrase securePhraseObj = sMngt.GetSecurePhrase(sessionid, channelid, UserId);
                            if (securePhraseObj == null) {
                                authPackage.RegisteredUser = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                                authPackage.GeoCheck = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                                authPackage.GetValidImage = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                                authPackage.IpCheck = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                                authPackage.RegisteredDevice = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                                authPackage.SweetSpot = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                                        session.getLoginid(), new Date(), "Generate Authentication Package", "Auth Package failed", aStatus.errorcode,
                                        "Authentication Package", "", "", itemtype,
                                        session.getLoginid());
                                return authPackage;

                            } else {
                                authPackage.RegisteredUser = SecurePhraseManagment.SUCCESS_AUTH_PACKAGE;
                                authPackage.GetValidImage = SecurePhraseManagment.SUCCESS_AUTH_PACKAGE;
                                if (iSettings._sweetSpot == 1) {//1- Active ;0-In-Active
                                    if (iSettings._sweetSpotDeviation == 0) {
                                        if (securePhraseObj.getXcoordinate() == credentials.tokenDetails[i].xCoordinate && securePhraseObj.getYcoordinate() == credentials.tokenDetails[i].yCoordinate) {
                                            authPackage.SweetSpot = SecurePhraseManagment.SUCCESS_AUTH_PACKAGE;
                                            aStatus.error = "Success";
                                            aStatus.errorcode = 0;
                                        } else {
                                            authPackage.SweetSpot = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                                        }
                                    } else if (iSettings._sweetSpotDeviation == 1) {
                                        int PhraseXcoordinate = securePhraseObj.getXcoordinate();
                                        int PhraseYcoordinate = securePhraseObj.getYcoordinate();
                                        int userXcoordinate = credentials.tokenDetails[i].xCoordinate;
                                        int userYcoordinate = credentials.tokenDetails[i].yCoordinate;
                                        int _xDeviation = iSettings._xDeviation;
                                        int _yDeviation = iSettings._yDeviation;
                                        if (PhraseXcoordinate >= userXcoordinate && PhraseYcoordinate >= userYcoordinate) {
                                            int xDiff = PhraseXcoordinate - (userXcoordinate + _xDeviation);
                                            int yDiff = PhraseYcoordinate - (userYcoordinate + _yDeviation);
                                            String xnumber = String.valueOf(xDiff);
                                            if (xnumber.contains("-")) {
                                                xDiff = (xDiff * -1);
                                            }
                                            String ynumber = String.valueOf(yDiff);
                                            if (ynumber.contains("-")) {
                                                yDiff = (yDiff * -1);
                                            }
                                            if (xDiff <= _xDeviation && yDiff <= _yDeviation) {
                                                authPackage.SweetSpot = SecurePhraseManagment.SUCCESS_AUTH_PACKAGE;
                                                aStatus.error = "Success";
                                                aStatus.errorcode = 0;
                                            } else {
                                                authPackage.SweetSpot = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                                            }
                                        } else if (PhraseXcoordinate <= userXcoordinate && PhraseYcoordinate <= userYcoordinate) {
                                            int xDiff = userXcoordinate - (PhraseXcoordinate + _xDeviation);
                                            int yDiff = userYcoordinate - (PhraseYcoordinate + _yDeviation);
                                            String xnumber = String.valueOf(xDiff);
                                            if (xnumber.contains("-")) {
                                                xDiff = (xDiff * -1);
                                            }
                                            String ynumber = String.valueOf(yDiff);
                                            if (ynumber.contains("-")) {
                                                yDiff = (yDiff * -1);
                                            }
                                            if (xDiff <= _xDeviation && yDiff <= _yDeviation) {
                                                authPackage.SweetSpot = SecurePhraseManagment.SUCCESS_AUTH_PACKAGE;
                                                aStatus.error = "Success";
                                                aStatus.errorcode = 0;
                                            } else {
                                                authPackage.SweetSpot = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                                            }
                                        } else if (PhraseXcoordinate <= userXcoordinate && PhraseYcoordinate >= userYcoordinate) {
                                            int xDiff = userXcoordinate - (PhraseXcoordinate + _xDeviation);
                                            int yDiff = PhraseYcoordinate - (userYcoordinate + _yDeviation);
                                            String xnumber = String.valueOf(xDiff);
                                            if (xnumber.contains("-")) {
                                                xDiff = (xDiff * -1);
                                            }
                                            String ynumber = String.valueOf(yDiff);
                                            if (ynumber.contains("-")) {
                                                yDiff = (yDiff * -1);
                                            }
                                            if (xDiff <= _xDeviation && yDiff <= _yDeviation) {
                                                authPackage.SweetSpot = SecurePhraseManagment.SUCCESS_AUTH_PACKAGE;
                                                aStatus.error = "Success";
                                                aStatus.errorcode = 0;
                                            } else {
                                                authPackage.SweetSpot = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                                            }
                                        } else if (PhraseXcoordinate >= userXcoordinate && PhraseYcoordinate <= userYcoordinate) {
                                            int xDiff = PhraseXcoordinate - (userXcoordinate + _xDeviation);
                                            int yDiff = userYcoordinate - (PhraseYcoordinate + _yDeviation);
                                            String xnumber = String.valueOf(xDiff);
                                            if (xnumber.contains("-")) {
                                                xDiff = (xDiff * -1);
                                            }
                                            String ynumber = String.valueOf(yDiff);
                                            if (ynumber.contains("-")) {
                                                yDiff = (yDiff * -1);
                                            }
                                            if (xDiff <= _xDeviation && yDiff <= _yDeviation) {
                                                authPackage.SweetSpot = SecurePhraseManagment.SUCCESS_AUTH_PACKAGE;
                                                aStatus.error = "Success";
                                                aStatus.errorcode = 0;
                                            } else {
                                                authPackage.SweetSpot = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                                            }
                                        } else {
                                            authPackage.SweetSpot = SecurePhraseManagment.FAILED_AUTH_PACKAGE;
                                        }
                                    }
                                } else {
                                    authPackage.SweetSpot = SecurePhraseManagment.DISABLE_AUTH_PACKAGE;
                                }
                            }
                            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), 
                                    session.getLoginid(), session.getLoginid(), new Date(), 
                                    "Generate Authentication Package", aStatus.error, aStatus.errorcode,
                                    "SECURITY PHRASE Management","", "Registered User: " + authPackage.RegisteredUser + ", GeoCheck: " + authPackage.GeoCheck + ", GetValidImage: " + authPackage.GetValidImage
                                    + ", IpCheck: " + authPackage.IpCheck + ", RegisteredDevice: " + authPackage.RegisteredDevice + ", SweetSpot: " + authPackage.SweetSpot, "SECURITYPHRASE", credentials.rssUser.userId);
                            return authPackage;
                        } //nilesh
                        catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }

        }
//        return aStatus;
        throw new AxiomException(aStatus.error);
    }

    @Override
    public AxiomTokenData ActivateToken(String sessionid, RSSUserCerdentials credentials, String regcode, String integrityCheckString, String trustPayLoad) throws AxiomException {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                System.out.println("ActivateToken::sessionid::" + sessionid);
                System.out.println("ActivateToken::userid::" + credentials.rssUser.userId);
                for (int i = 0; i < credentials.tokenDetails.length; i++) {
                    System.out.println("Categorey" + credentials.tokenDetails[i].category);
                    System.out.println("SubCategory" + credentials.tokenDetails[i].subcategory);
                }
            }
        } catch (Exception ex) {
        }
//nilesh
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "Licence is invalid";
//            aStatus.errorcode = -100;
////            return aStatus;
//            throw new AxiomException(aStatus.error);
        }
        AxiomStatus aStatus = new AxiomStatus();
        if (regcode == null) {
            aStatus.error = "Input Data is Null!!!";
            aStatus.errorcode = -1;
            throw new AxiomException(aStatus.error);
        }
        byte[] SHA1hash = null;
        if (trustPayLoad == null) {
            SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.rssUser.userId + regcode);
        } else {
            SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.rssUser.userId + regcode + trustPayLoad);
        }
//        String integritycheck = new String(Base64.encode(SHA1hash));
        String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//        String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java
        if (integrityCheckString != null) {
            if (integrityCheckString.equals(integritycheck) != true) {
                aStatus.error = "Input Data is Tampered!!!";
                aStatus.errorcode = -76;
//                return aStatus;
                throw new AxiomException(aStatus.error);

            }
        }

        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);
        if (session == null) {

            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
//            return aStatus;
            throw new AxiomException(aStatus.error);
        }
        aStatus.error = "ERROR";
        aStatus.errorcode = -2;
        String oldValue = "";
        String strValue = "";
        String UserId = credentials.rssUser.userId;
        int retValue = -1;
        if (session != null) {

            SettingsManagement setManagement = new SettingsManagement();

            String channelid = session.getChannelid();
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(channelid);
            if (channel == null) {
                return null;
            }

            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
            if (channelProfileObj != null) {
                int retVal = sManagement.getServerStatus(channelProfileObj);
                if (retVal != 0) {

                    aStatus = new AxiomStatus();
                    aStatus.error = "Server Down for maintainance,Please try later!!!";
                    aStatus.errorcode = -48;
//                    return aStatus;
                    throw new AxiomException(aStatus.error);
                }
            }

            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        OperatorsManagement oManagement = new OperatorsManagement();
                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
//                            return aStatus;
                            throw new AxiomException(aStatus.error);
                        }

                        suTemplate.close();
                        sTemplate.close();
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                        return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                    aStatus.error = "INVALID IP REQUEST";
                    aStatus.errorcode = -8;
//                    return aStatus;
                    throw new AxiomException(aStatus.error);
                }
            }
            boolean bSentToUser = true;
            int subcategory = 1;
            if (trustPayLoad != null) {
                MobileTrustSettings mSettings = null;

                Object obj = setManagement.getSetting(sessionid, channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
                if (obj != null) {
                    if (obj instanceof MobileTrustSettings) {
                        mSettings = (MobileTrustSettings) obj;
                    }
                }

                if (mSettings == null) {
                    aStatus.error = "Mobile Trust Setting is not configured!!!";
                    aStatus.errorcode = -23;
//                  
//                    return aStatus;
                    throw new AxiomException(aStatus.error);
                }

                if (mSettings.bGeoFencing == true) {

                    GeoLocationManagement gManagement = new GeoLocationManagement();
                    AXIOMStatus status = gManagement.validateGeoLocation(sessionid, channelid, UserId, trustPayLoad);
                    if (status == null) {
                        aStatus.error = "Location Not Found!!!";
                        aStatus.errorcode = -115;
                    } else if (status.iStatus != 0) {

                        aStatus.error = status.strStatus;
                        aStatus.errorcode = status.iStatus;

                        //     String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
                        audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                                session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorcode,
                                "Token Management", "Invalid Location", null, itemtype,
                                session.getLoginid());

//                        return aStatus;
                        throw new AxiomException(aStatus.error);

                    }
                }
            }
            for (int i = 0; i < credentials.tokenDetails.length; i++) {
                if (credentials.tokenDetails[i] != null) {
                    if (credentials.tokenDetails[i].category == AxiomCredentialDetails.SOFTWARE_TOKEN) {
                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
                            aStatus = new AxiomStatus();
                            aStatus.error = "This feature is not available in this license!!!";
                            aStatus.errorcode = -101;
//                            return aStatus;
                            throw new AxiomException(aStatus.error);
                        }

                        int cat = OTP_TOKEN_SOFTWARE;
                        int subcat = 0;

                        if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_SOFTWARE_WEB) {
                            subcat = OTP_TOKEN_SOFTWARE_WEB;
                            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//                            TokenStatusDetails tokenSelected = oManagement.getToken(sessionid, session.getChannelid(), UserId, cat);
                            Otptokens tokenSelected = oManagement.getOtpObjByUserId(sessionid, session.getChannelid(), UserId, cat);
                            if (tokenSelected == null) {
                                aStatus.errorcode = -2;
                                aStatus.error = "Desired Token is not assigned";
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            } else {
                                if (tokenSelected.getStatus() == TOKEN_STATUS_ACTIVE) {
                                    oldValue = strACTIVE;
                                } else if (tokenSelected.getStatus() == TOKEN_STATUS_SUSPENDED) {
                                    oldValue = strSUSPENDED;
                                } else if (tokenSelected.getStatus() == TOKEN_STATUS_UNASSIGNED) {
                                    oldValue = strUNASSIGNED;
                                }

                                if (credentials.tokenDetails[i].status == TOKEN_STATUS_ACTIVE) {
                                    strValue = strACTIVE;
                                } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_SUSPENDED) {
                                    strValue = strSUSPENDED;
                                } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_UNASSIGNED) {
                                    strValue = strUNASSIGNED;
                                }
                                int res = -1;
                                String secret = null;
                                AxiomTokenData tokenData = new AxiomTokenData();
//                                System.out.println("REGCODE =" + tokenSelected.getRegcode());
                                SettingsManagement settObj = new SettingsManagement();
                                Object obj = settObj.getSetting(sessionid, channelid, SettingsManagement.Token, SettingsManagement.PREFERENCE_ONE);
                                TokenSettings tSettingObj = null;
                                if (obj != null) {
                                    tSettingObj = (TokenSettings) obj;
                                }

                                if (tSettingObj == null) {
                                    aStatus.error = "OTP Token Setting not Found!!!";
                                    aStatus.errorcode = -101;
                                    throw new AxiomException(aStatus.error);
                                }
                                if (regcode.contains(tokenSelected.getRegcode())) {
                                    if (tokenSelected.getSecret() != null) {

                                        secret = tokenSelected.getSecret();
                                        System.out.println("SECRET =" + secret);
                                        res = oManagement.changeOtptokens(channelid, UserId, tokenSelected, cat);
                                    } else {
                                        CryptoManagement cryptoObj = new CryptoManagement();
                                        secret = cryptoObj.generateOTPTokenSecretSimple(tokenSelected);
                                        tokenSelected.setSecret(secret);
                                        System.out.println("SECRET =" + secret);
                                        res = oManagement.changeOtptokens(channelid, UserId, tokenSelected, cat);
                                    }
                                } else {
                                    aStatus.error = "Registration code not Matched!!!";
                                    aStatus.errorcode = -101;
                                    throw new AxiomException(aStatus.error);
                                }
//                              
                                if (res != 0) {
                                    return null;
                                } else {

                                    retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), UserId, credentials.tokenDetails[i].status, cat, subcat);
//                                    retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), UserId, OTPTokenManagement.TOKEN_STATUS_ACTIVE, cat, subcat);
                                    if (retValue == 0 || retValue == -6) {
                                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                                channel.getName(),
                                                session.getLoginid(), session.getLoginid(),
                                                new Date(),
                                                "CHANGE STATUS", aStatus.error, aStatus.errorcode,
                                                "Token Management",
                                                "Current Status=" + oldValue,
                                                "New Status=" + strValue,
                                                "OTPTOKENS", UserId);
//                                        return secret;

                                        CertificateManagement certMngt = new CertificateManagement();
                                        Certificates certObj = certMngt.getCertificate(sessionid, channelid, UserId);
                                        String cert = null;
                                        String hiddenKey = null;
                                        String visibleKey = null;
                                        String privateKeyString = null;
                                        if (certObj != null) {

                                            cert = certObj.getCertificate();

                                            ByteArrayInputStream fis = new ByteArrayInputStream(Base64.decode(certObj.getPfx()));

                                            try {
                                                KeyStore ks = KeyStore.getInstance("PKCS12");
                                                ks.load(fis, certObj.getPfxpassword().toCharArray());
                                                for (Enumeration num = ks.aliases(); num.hasMoreElements();) {
                                                    String alias = (String) num.nextElement();
                                                    if (ks.isKeyEntry(alias)) {
                                                        PrivateKey priKey = (PrivateKey) ks.getKey(alias, certObj.getPfxpassword().toCharArray());
                                                        hiddenKey = new String(Base64.encode(priKey.getEncoded()));
                                                        RSAPrivateCrtKey privk = (RSAPrivateCrtKey) priKey;
                                                        RSAPublicKeySpec publicKeySpec = new java.security.spec.RSAPublicKeySpec(privk.getModulus(), privk.getPublicExponent());
                                                        RSAPrivateKeySpec privateKeySpec = new java.security.spec.RSAPrivateKeySpec(privk.getModulus(), privk.getPrivateExponent());
                                                        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                                                        PublicKey myPublicKey = keyFactory.generatePublic(publicKeySpec);

                                                        byte[] privkey = Base64.decode(hiddenKey);
                                                        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privkey);
                                                        RSAPrivateCrtKey privKey = (RSAPrivateCrtKey) keyFactory.generatePrivate(keySpec);

                                                        //nilesh
                                                        char[] aliasPassPhrase = certObj.getPfxpassword().toCharArray();
                                                        KeyPair keyPair = getPrivateKey(ks, alias, aliasPassPhrase);
                                                        StringWriter stringWriter = new StringWriter();
                                                        PEMWriter pemWriter = new PEMWriter(stringWriter);
                                                        pemWriter.writeObject(keyPair.getPrivate());
                                                        pemWriter.close();
                                                        privateKeyString = stringWriter.toString();
                                                        System.out.println("privateKeyString :" + privateKeyString);
                                                        //end nilesh

                                                        visibleKey = new String(Base64.encode(myPublicKey.getEncoded()));
                                                        PrivateKey myPrivateKey = keyFactory.generatePrivate(privateKeySpec);
                                                        hiddenKey = new String(Base64.encode(myPrivateKey.getEncoded()));

                                                        String strprivKey = new String(Base64.encode(privKey.getEncoded()));
                                                        System.out.println("privKey :" + strprivKey);

                                                    }
                                                }
                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                            }
                                        }
                                        tokenData.OTPLength = tSettingObj.getOtpLengthSWToken();
                                        tokenData.OTPSecret = secret;
                                        tokenData.OTPduration = tSettingObj.getOtpDurationInSeconds();

                                        tokenData.PublicWebTokenExpiryTime = tSettingObj.getiSWWebTokenExpiryTime();
                                        tokenData.invalidPinAttempt = tSettingObj.getiSWWebTokenPinAttempt();
                                        tokenData.certificate = cert;
//                                        tokenData.certificatePassword = certObj.getPfxpassword();
                                        tokenData.certificatePrivateKey = privateKeyString;
//                                        tokenData.certificatePublicKey = visibleKey;

                                        return tokenData;
                                    } else {
                                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                                channel.getName(),
                                                session.getLoginid(), session.getLoginid(),
                                                new Date(),
                                                "CHANGE STATUS", aStatus.error, aStatus.errorcode,
                                                "Token Management",
                                                "Current Status=" + oldValue,
                                                "New Status=" + strValue,
                                                "OTPTOKENS", UserId);
                                        return null;
                                    }

                                }

                            }
                        }
                    }

                    if (credentials.tokenDetails[i].category == AxiomCredentialDetails.HARDWARE_TOKEN) {
                        int cat1 = OTP_TOKEN_HARDWARE;
                        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//                            TokenStatusDetails tokenSelected = oManagement.getToken(sessionid, session.getChannelid(), UserId, cat);
                        Otptokens tokenSelected = oManagement.getOtpObjByUserId(sessionid, session.getChannelid(), UserId, cat1);
                        if (tokenSelected == null) {
                            aStatus.errorcode = -2;
                            aStatus.error = "Desired Token is not assigned";
//                            return aStatus;
                            throw new AxiomException(aStatus.error);
                        } else {
                            tokenSelected.setStatus(tokenSelected.getStatus());
                            if (tokenSelected.getStatus() == TOKEN_STATUS_ACTIVE) {
                                oldValue = strACTIVE;
                            } else if (tokenSelected.getStatus() == TOKEN_STATUS_SUSPENDED) {
                                oldValue = strSUSPENDED;
                            } else if (tokenSelected.getStatus() == TOKEN_STATUS_UNASSIGNED) {
                                oldValue = strUNASSIGNED;
                            }

                            if (credentials.tokenDetails[i].status == TOKEN_STATUS_ACTIVE) {
                                strValue = strACTIVE;
                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_SUSPENDED) {
                                strValue = strSUSPENDED;
                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_UNASSIGNED) {
                                strValue = strUNASSIGNED;
                            }
                            int res = -1;
                            String secret = null;
                            AxiomTokenData tokenData = new AxiomTokenData();
//                                System.out.println("REGCODE =" + tokenSelected.getRegcode());
                            SettingsManagement settObj = new SettingsManagement();
                            Object obj = settObj.getSetting(sessionid, channelid, SettingsManagement.Token, SettingsManagement.PREFERENCE_ONE);
                            TokenSettings tSettingObj = null;
                            if (obj != null) {
                                tSettingObj = (TokenSettings) obj;
                            }

                            if (tSettingObj == null) {
                                aStatus.error = "OTP Token Setting not Found!!!";
                                aStatus.errorcode = -101;
                                throw new AxiomException(aStatus.error);
                            }
                            res = oManagement.changeOtptokens(channelid, UserId, tokenSelected, cat1);

                            if (res == 0) {
                                retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), UserId, credentials.tokenDetails[i].status, cat1, OTP_TOKEN_HARDWARE_MINI);
                            }
                            if (retValue == 0 || retValue == -6) {
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                        channel.getName(),
                                        session.getLoginid(), session.getLoginid(),
                                        new Date(),
                                        "CHANGE STATUS", aStatus.error, aStatus.errorcode,
                                        "Token Management",
                                        "Current Status=" + oldValue,
                                        "New Status=" + strValue,
                                        "OTPTOKENS", UserId);
//                                        return secret;
                                tokenData.OTPLength = tSettingObj.getOtpLengthSWToken();
                                tokenData.OTPSecret = secret;
                                tokenData.OTPduration = tSettingObj.getOtpDurationInSeconds();
                                tokenData.PublicWebTokenExpiryTime = tSettingObj.getiSWWebTokenExpiryTime();
                                tokenData.invalidPinAttempt = tSettingObj.getiSWWebTokenPinAttempt();
                                return tokenData;
                            } else {
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                        channel.getName(),
                                        session.getLoginid(), session.getLoginid(),
                                        new Date(),
                                        "CHANGE STATUS", aStatus.error, aStatus.errorcode,
                                        "Token Management",
                                        "Current Status=" + oldValue,
                                        "New Status=" + strValue,
                                        "OTPTOKENS", UserId);
                                return null;
                            }

                        }

                    }
                    if (credentials.tokenDetails[i].category == AxiomCredentialDetails.OUTOFBOUND_TOKEN) {
                        int cat1 = OTP_TOKEN_OUTOFBAND;
                        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
//                            TokenStatusDetails tokenSelected = oManagement.getToken(sessionid, session.getChannelid(), UserId, cat);
                        Otptokens tokenSelected = oManagement.getOtpObjByUserId(sessionid, session.getChannelid(), UserId, cat1);
                        if (tokenSelected == null) {
                            aStatus.errorcode = -2;
                            aStatus.error = "Desired Token is not assigned";
//                            return aStatus;
                            throw new AxiomException(aStatus.error);
                        } else {
                            tokenSelected.setStatus(tokenSelected.getStatus());
                            if (tokenSelected.getStatus() == TOKEN_STATUS_ACTIVE) {
                                oldValue = strACTIVE;
                            } else if (tokenSelected.getStatus() == TOKEN_STATUS_SUSPENDED) {
                                oldValue = strSUSPENDED;
                            } else if (tokenSelected.getStatus() == TOKEN_STATUS_UNASSIGNED) {
                                oldValue = strUNASSIGNED;
                            }

                            if (credentials.tokenDetails[i].status == TOKEN_STATUS_ACTIVE) {
                                strValue = strACTIVE;
                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_SUSPENDED) {
                                strValue = strSUSPENDED;
                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_UNASSIGNED) {
                                strValue = strUNASSIGNED;
                            }
                            int res = -1;
                            String secret = null;
                            AxiomTokenData tokenData = new AxiomTokenData();
//                                System.out.println("REGCODE =" + tokenSelected.getRegcode());
                            SettingsManagement settObj = new SettingsManagement();
                            Object obj = settObj.getSetting(sessionid, channelid, SettingsManagement.Token, SettingsManagement.PREFERENCE_ONE);
                            TokenSettings tSettingObj = null;
                            if (obj != null) {
                                tSettingObj = (TokenSettings) obj;
                            }

                            if (tSettingObj == null) {
                                aStatus.error = "OTP Token Setting not Found!!!";
                                aStatus.errorcode = -101;
                                throw new AxiomException(aStatus.error);
                            }
                            res = oManagement.changeOtptokens(channelid, UserId, tokenSelected, cat1);

                            if (res == 0) {
                                retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), UserId, credentials.tokenDetails[i].status, cat1, OTP_TOKEN_HARDWARE_MINI);
                            }
                            if (retValue == 0 || retValue == -6) {
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                        channel.getName(),
                                        session.getLoginid(), session.getLoginid(),
                                        new Date(),
                                        "CHANGE STATUS", aStatus.error, aStatus.errorcode,
                                        "Token Management",
                                        "Current Status=" + oldValue,
                                        "New Status=" + strValue,
                                        "OTPTOKENS", UserId);
//                                        return secret;
                                tokenData.OTPLength = tSettingObj.getOtpLengthSWToken();
                                tokenData.OTPSecret = secret;
                                tokenData.OTPduration = tSettingObj.getOtpDurationInSeconds();
                                tokenData.PublicWebTokenExpiryTime = tSettingObj.getiSWWebTokenExpiryTime();
                                tokenData.invalidPinAttempt = tSettingObj.getiSWWebTokenPinAttempt();
                                return tokenData;
                            } else {
                                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                        channel.getName(),
                                        session.getLoginid(), session.getLoginid(),
                                        new Date(),
                                        "CHANGE STATUS", aStatus.error, aStatus.errorcode,
                                        "Token Management",
                                        "Current Status=" + oldValue,
                                        "New Status=" + strValue,
                                        "OTPTOKENS", UserId);
                                return null;
                            }

                        }
                    }
                    if (credentials.tokenDetails[i].category == AxiomCredentialDetails.PKI_SOFTWARE_TOKEN) {
                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.PKI_SOFTWARE) != 0) {
                            aStatus = new AxiomStatus();
                            aStatus.error = "This feature is not available in this license!!!";
                            aStatus.errorcode = -101;
//                            return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
//                        subcat = AxiomCredentialDetails.PKI_SOFTWARE_TOKEN;
                        int cat = 1;
                        int subcat = 0;
                        PKITokenManagement oManagement = new PKITokenManagement();
                        Pkitokens tokenSelected = oManagement.getPkiObjByUserid(channelid, UserId, cat);
                        if (tokenSelected == null) {
                            aStatus.errorcode = -2;
                            aStatus.error = "Desired Token is not assigned";
                            throw new AxiomException(aStatus.error);
                        } else {
                            if (tokenSelected.getStatus() == TOKEN_STATUS_ACTIVE) {
                                oldValue = strACTIVE;
                            } else if (tokenSelected.getStatus() == TOKEN_STATUS_SUSPENDED) {
                                oldValue = strSUSPENDED;
                            } else if (tokenSelected.getStatus() == TOKEN_STATUS_UNASSIGNED) {
                                oldValue = strUNASSIGNED;
                            }

                            if (credentials.tokenDetails[i].status == TOKEN_STATUS_ACTIVE) {
                                strValue = strACTIVE;
                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_SUSPENDED) {
                                strValue = strSUSPENDED;
                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_UNASSIGNED) {
                                strValue = strUNASSIGNED;
                            }
                            int res = -1;
                            String secret = null;
                            AxiomTokenData tokenData = new AxiomTokenData();
                            SettingsManagement settObj = new SettingsManagement();
                            Object obj = settObj.getSetting(sessionid, channelid, SettingsManagement.Token, SettingsManagement.PREFERENCE_ONE);
                            TokenSettings tSettingObj = null;
                            if (obj != null) {
                                tSettingObj = (TokenSettings) obj;
                            }
                            if (tSettingObj == null) {
                                aStatus.error = "OTP Token Setting not Found!!!";
                                aStatus.errorcode = -101;
                                throw new AxiomException(aStatus.error);
                            }
                            String regcodeDB = AxiomProtect.AccessData(tokenSelected.getRegcode());
                            if (regcode.contains(regcodeDB)) {
                                CertificateManagement certMngt = new CertificateManagement();
                                Certificates certObj = certMngt.getCertificate(sessionid, channelid, UserId);
                                String cert = null;
                                String hiddenKey = null;
                                String visibleKey = null;
                                String privateKeyString = null;
                                if (certObj != null) {
                                    try {
                                        cert = certObj.getCertificate();
//                                        byte[] pfxAsBytes = Base64.decode(certObj.getPfx());
//                                        ByteArrayInputStream fis = new ByteArrayInputStream(Base64.decode(pfxAsBytes));
                                        ByteArrayInputStream fis = new ByteArrayInputStream(Base64.decode(certObj.getPfx()));
                                        KeyStore ks = KeyStore.getInstance("PKCS12");
                                        ks.load(fis, certObj.getPfxpassword().toCharArray());
                                        for (Enumeration num = ks.aliases(); num.hasMoreElements();) {
                                            String alias = (String) num.nextElement();
                                            if (ks.isKeyEntry(alias)) {
//                                                PrivateKey priKey = (PrivateKey) ks.getKey(alias, certObj.getPfxpassword().toCharArray());
//                                                hiddenKey = new String(Base64.encode(priKey.getEncoded()));
//                                                RSAPrivateCrtKey privk = (RSAPrivateCrtKey) priKey;
//                                                RSAPublicKeySpec publicKeySpec = new java.security.spec.RSAPublicKeySpec(privk.getModulus(), privk.getPublicExponent());
//                                                KeyFactory keyFactory = KeyFactory.getInstance("RSA");
//                                                PublicKey myPublicKey = keyFactory.generatePublic(publicKeySpec);
//                                                visibleKey = new String(Base64.encode(myPublicKey.getEncoded()));

                                                PrivateKey priKey = (PrivateKey) ks.getKey(alias, certObj.getPfxpassword().toCharArray());
                                                hiddenKey = new String(Base64.encode(priKey.getEncoded()));
                                                RSAPrivateCrtKey privk = (RSAPrivateCrtKey) priKey;
                                                RSAPublicKeySpec publicKeySpec = new java.security.spec.RSAPublicKeySpec(privk.getModulus(), privk.getPublicExponent());
                                                RSAPrivateKeySpec privateKeySpec = new java.security.spec.RSAPrivateKeySpec(privk.getModulus(), privk.getPrivateExponent());
                                                KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                                                PublicKey myPublicKey = keyFactory.generatePublic(publicKeySpec);

                                                byte[] privkey = Base64.decode(hiddenKey);
                                                PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privkey);
                                                RSAPrivateCrtKey privKey = (RSAPrivateCrtKey) keyFactory.generatePrivate(keySpec);

                                                //nilesh
                                                char[] aliasPassPhrase = certObj.getPfxpassword().toCharArray();
                                                KeyPair keyPair = getPrivateKey(ks, alias, aliasPassPhrase);
                                                StringWriter stringWriter = new StringWriter();
                                                PEMWriter pemWriter = new PEMWriter(stringWriter);
                                                pemWriter.writeObject(keyPair.getPrivate());
                                                pemWriter.close();
                                                privateKeyString = stringWriter.toString();
                                                System.out.println("privateKeyString :" + privateKeyString);
                                                //end nilesh

                                                visibleKey = new String(Base64.encode(myPublicKey.getEncoded()));
                                                PrivateKey myPrivateKey = keyFactory.generatePrivate(privateKeySpec);
                                                hiddenKey = new String(Base64.encode(myPrivateKey.getEncoded()));

                                                String strprivKey = new String(Base64.encode(privKey.getEncoded()));
                                                System.out.println("privKey :" + strprivKey);
                                            }
                                        }
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                } else {
                                    aStatus.error = "Registration code not Matched!!!";
                                    aStatus.errorcode = -101;
                                    throw new AxiomException(aStatus.error);
                                }
                                if (cert != null && hiddenKey != null && visibleKey != null) {
                                    retValue = oManagement.ChangeStatus(sessionid, channelid, UserId, PKITokenManagement.TOKEN_STATUS_ACTIVE, cat);
                                    if (retValue == 0 || retValue == -6) {
                                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                                channel.getName(),
                                                session.getLoginid(), session.getLoginid(),
                                                new Date(),
                                                "CHANGE STATUS", aStatus.error, aStatus.errorcode,
                                                "Token Management",
                                                "Current Status=" + oldValue,
                                                "New Status=" + "Active",
                                                "PKITOKEN", UserId);
                                        tokenData.certificate = cert;
//                                        tokenData.certificatePassword = certObj.getPfxpassword();
                                        tokenData.certificatePrivateKey = privateKeyString;
//                                        tokenData.certificatePublicKey = visibleKey;
                                        tokenData.PublicWebTokenExpiryTime = tSettingObj.getiSWWebTokenExpiryTime();
                                        tokenData.invalidPinAttempt = tSettingObj.getiSWWebTokenPinAttempt();
                                        return tokenData;
                                    } else {
                                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                                                channel.getName(),
                                                session.getLoginid(), session.getLoginid(),
                                                new Date(),
                                                "CHANGE STATUS", aStatus.error, aStatus.errorcode,
                                                "Token Management",
                                                "Current Status=" + oldValue,
                                                "New Status= Failed to change status.",
                                                "OTPTOKENS", UserId);
                                        return null;
                                    }
                                }

                            }

                        }
                    }
//                    }

                }

            }
//            return null;
            throw new AxiomException(aStatus.error);
        }
        return null;
    }

//    @Override
//    public AxiomTokenData ActivateToken(String sessionid, RSSUserCerdentials credentials, String regcode, String integrityCheckString, String trustPayLoad) throws AxiomException {
//        String strDebug = null;
//        try {
//            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
//            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
//                System.out.println("ActivateToken::sessionid::" + sessionid);
//                System.out.println("ActivateToken::userid::" + credentials.rssUser.userId);
//                for (int i = 0; i < credentials.tokenDetails.length; i++) {
//                    System.out.println("Categorey" + credentials.tokenDetails[i].category);
//                    System.out.println("SubCategory" + credentials.tokenDetails[i].subcategory);
//                }
//            }
//        } catch (Exception ex) {
//        }
////nilesh
//        int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "Licence is invalid";
//            aStatus.errorcode = -100;
////            return aStatus;
//            throw new AxiomException(aStatus.error);
//        }
//        AxiomStatus aStatus = new AxiomStatus();
//        if (regcode == null) {
//            aStatus.error = "Input Data is Null!!!";
//            aStatus.errorcode = -1;
//            throw new AxiomException(aStatus.error);
//        }
//        byte[] SHA1hash = null;
//        if (trustPayLoad == null) {
//            SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.rssUser.userId + regcode);
//        } else {
//            SHA1hash = UtilityFunctions.SHA1(sessionid + credentials.rssUser.userId + regcode + trustPayLoad);
//        }
////        String integritycheck = new String(Base64.encode(SHA1hash));
//        String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
////        String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java
//        if (integrityCheckString != null) {
//            if (integrityCheckString.equals(integritycheck) != true) {
//                aStatus.error = "Input Data is Tampered!!!";
//                aStatus.errorcode = -76;
////                return aStatus;
//                throw new AxiomException(aStatus.error);
//
//            }
//        }
//
//        SessionManagement sManagement = new SessionManagement();
//        AuditManagement audit = new AuditManagement();
//        MessageContext mc = wsContext.getMessageContext();
//        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
//        Sessions session = sManagement.getSessionById(sessionid);
//        if (session == null) {
//
//            aStatus.error = "Invalid Session";
//            aStatus.errorcode = -14;
////            return aStatus;
//            throw new AxiomException(aStatus.error);
//        }
//        aStatus.error = "ERROR";
//        aStatus.errorcode = -2;
//        String oldValue = "";
//        String strValue = "";
//        String UserId = credentials.rssUser.userId;
//        int retValue = -1;
//        if (session != null) {
//
//            SettingsManagement setManagement = new SettingsManagement();
//
//            String channelid = session.getChannelid();
//            ChannelManagement cManagement = new ChannelManagement();
//            Channels channel = cManagement.getChannelByID(channelid);
//            if (channel == null) {
//                return null;
//            }
//
//            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
//            if (channelProfileObj != null) {
//                int retVal = sManagement.getServerStatus(channelProfileObj);
//                if (retVal != 0) {
//
//                    aStatus = new AxiomStatus();
//                    aStatus.error = "Server Down for maintainance,Please try later!!!";
//                    aStatus.errorcode = -48;
////                    return aStatus;
//                    throw new AxiomException(aStatus.error);
//                }
//            }
//
//            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
//            if (ipobj != null) {
//                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
//                int checkIp = 1;
//                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
//                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
//                } else {
//                    checkIp = 1;
//                }
//                if (iObj.ipstatus == 0 && checkIp != 1) {
//                    if (iObj.ipalertstatus == 0) {
//                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
//                        Session sTemplate = suTemplate.openSession();
//                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
//                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
//                        OperatorsManagement oManagement = new OperatorsManagement();
//                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
//                        if (aOperator != null) {
//                            String[] emailList = new String[aOperator.length - 1];
//                            for (int i = 1; i < aOperator.length; i++) {
//                                emailList[i - 1] = aOperator[i].getEmailid();
//                            }
//                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
//                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
//                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
//                                String strsubject = templatesObj.getSubject();
//                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//                                if (strmessageBody != null) {
//                                    // Date date = new Date();
//                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
//                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
//                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
//                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
//                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
//                                }
//
//                                SendNotification send = new SendNotification();
//                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
//                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                            }
//
//                            suTemplate.close();
//                            sTemplate.close();
//                            aStatus.error = "INVALID IP REQUEST";
//                            aStatus.errorcode = -8;
////                            return aStatus;
//                            throw new AxiomException(aStatus.error);
//                        }
//
//                        suTemplate.close();
//                        sTemplate.close();
//                        aStatus.error = "INVALID IP REQUEST";
//                        aStatus.errorcode = -8;
////                        return aStatus;
//                        throw new AxiomException(aStatus.error);
//                    }
//                    aStatus.error = "INVALID IP REQUEST";
//                    aStatus.errorcode = -8;
////                    return aStatus;
//                    throw new AxiomException(aStatus.error);
//                }
//            }
//            boolean bSentToUser = true;
//            int subcategory = 1;
//            if (trustPayLoad != null) {
//                MobileTrustSettings mSettings = null;
//
//                Object obj = setManagement.getSetting(sessionid, channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
//                if (obj != null) {
//                    if (obj instanceof MobileTrustSettings) {
//                        mSettings = (MobileTrustSettings) obj;
//                    }
//                }
//
//                if (mSettings == null) {
//                    aStatus.error = "Mobile Trust Setting is not configured!!!";
//                    aStatus.errorcode = -23;
////                  
////                    return aStatus;
//                    throw new AxiomException(aStatus.error);
//                }
//
//                if (mSettings.bGeoFencing == true) {
//
//                    GeoLocationManagement gManagement = new GeoLocationManagement();
//                    AXIOMStatus status = gManagement.validateGeoLocation(sessionid, channelid, UserId, trustPayLoad);
//                    if (status == null) {
//                        aStatus.error = "Location Not Found!!!";
//                        aStatus.errorcode = -115;
//                    } else if (status.iStatus != 0) {
//
//                        aStatus.error = status.strStatus;
//                        aStatus.errorcode = status.iStatus;
//
//                        //     String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
//                        audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
//                                session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorcode,
//                                "Token Management", "Invalid Location", null, itemtype,
//                                session.getLoginid());
//
////                        return aStatus;
//                        throw new AxiomException(aStatus.error);
//
//                    }
//                }
//            }
//            for (int i = 0; i < credentials.tokenDetails.length; i++) {
//                if (credentials.tokenDetails[i] != null) {
//                    if (credentials.tokenDetails[i].category == AxiomCredentialDetails.SOFTWARE_TOKEN) {
//                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
//                            aStatus = new AxiomStatus();
//                            aStatus.error = "This feature is not available in this license!!!";
//                            aStatus.errorcode = -101;
////                            return aStatus;
//                            throw new AxiomException(aStatus.error);
//                        }
//
//                        int cat = OTP_TOKEN_SOFTWARE;
//                        int subcat = 0;
//
//                        if (credentials.tokenDetails[i].subcategory == AxiomCredentialDetails.OTP_TOKEN_SOFTWARE_WEB) {
//                            subcat = OTP_TOKEN_SOFTWARE_WEB;
//                            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
////                            TokenStatusDetails tokenSelected = oManagement.getToken(sessionid, session.getChannelid(), UserId, cat);
//                            Otptokens tokenSelected = oManagement.getOtpObjByUserId(sessionid, session.getChannelid(), UserId, cat);
//                            if (tokenSelected == null) {
//                                aStatus.errorcode = -2;
//                                aStatus.error = "Desired Token is not assigned";
////                            return aStatus;
//                                throw new AxiomException(aStatus.error);
//                            } else {
//                                if (tokenSelected.getStatus() == TOKEN_STATUS_ACTIVE) {
//                                    oldValue = strACTIVE;
//                                } else if (tokenSelected.getStatus() == TOKEN_STATUS_SUSPENDED) {
//                                    oldValue = strSUSPENDED;
//                                } else if (tokenSelected.getStatus() == TOKEN_STATUS_UNASSIGNED) {
//                                    oldValue = strUNASSIGNED;
//                                }
//
//                                if (credentials.tokenDetails[i].status == TOKEN_STATUS_ACTIVE) {
//                                    strValue = strACTIVE;
//                                } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_SUSPENDED) {
//                                    strValue = strSUSPENDED;
//                                } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_UNASSIGNED) {
//                                    strValue = strUNASSIGNED;
//                                }
//                                int res = -1;
//                                String secret = null;
//                                AxiomTokenData tokenData = new AxiomTokenData();
////                                System.out.println("REGCODE =" + tokenSelected.getRegcode());
//                                SettingsManagement settObj = new SettingsManagement();
//                                Object obj = settObj.getSetting(sessionid, channelid, SettingsManagement.Token, SettingsManagement.PREFERENCE_ONE);
//                                TokenSettings tSettingObj = null;
//                                if (obj != null) {
//                                    tSettingObj = (TokenSettings) obj;
//                                }
//
//                                if (tSettingObj == null) {
//                                    aStatus.error = "OTP Token Setting not Found!!!";
//                                    aStatus.errorcode = -101;
//                                    throw new AxiomException(aStatus.error);
//                                }
//                                if (regcode.contains(tokenSelected.getRegcode())) {
//                                    if (tokenSelected.getSecret() != null) {
//
//                                        secret = tokenSelected.getSecret();
//                                        System.out.println("SECRET =" + secret);
//                                        res = oManagement.changeOtptokens(channelid, UserId, tokenSelected, cat);
//                                    } else {
//                                        CryptoManagement cryptoObj = new CryptoManagement();
//                                        secret = cryptoObj.generateOTPTokenSecretSimple(tokenSelected);
//                                        tokenSelected.setSecret(secret);
//                                        System.out.println("SECRET =" + secret);
//                                        res = oManagement.changeOtptokens(channelid, UserId, tokenSelected, cat);
//                                    }
//                                } else {
//                                    aStatus.error = "Registration code not Matched!!!";
//                                    aStatus.errorcode = -101;
//                                    throw new AxiomException(aStatus.error);
//                                }
////                              
//                                if (res != 0) {
//                                    return null;
//                                } else {
//
//                                    retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), UserId, credentials.tokenDetails[i].status, cat, subcat);
////                                    retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), UserId, OTPTokenManagement.TOKEN_STATUS_ACTIVE, cat, subcat);
//                                    if (retValue == 0 || retValue == -6) {
//                                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
//                                                channel.getName(),
//                                                session.getLoginid(), session.getLoginid(),
//                                                new Date(),
//                                                "CHANGE STATUS", aStatus.error, aStatus.errorcode,
//                                                "Token Management",
//                                                "Current Status=" + oldValue,
//                                                "New Status=" + strValue,
//                                                "OTPTOKENS", UserId);
////                                        return secret;
//                                        tokenData.OTPLength = tSettingObj.getOtpLengthSWToken();
//                                        tokenData.OTPSecret = secret;
//                                        tokenData.OTPduration = tSettingObj.getOtpDurationInSeconds();
//                                        tokenData.PublicWebTokenExpiryTime = tSettingObj.getiSWWebTokenExpiryTime();
//                                        tokenData.invalidPinAttempt = tSettingObj.getiSWWebTokenPinAttempt();
//                                        return tokenData;
//                                    } else {
//                                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
//                                                channel.getName(),
//                                                session.getLoginid(), session.getLoginid(),
//                                                new Date(),
//                                                "CHANGE STATUS", aStatus.error, aStatus.errorcode,
//                                                "Token Management",
//                                                "Current Status=" + oldValue,
//                                                "New Status=" + strValue,
//                                                "OTPTOKENS", UserId);
//                                        return null;
//                                    }
//
//                                }
//
//                            }
//                        }
//                    }
//                    if (credentials.tokenDetails[i].category == AxiomCredentialDetails.PKI_SOFTWARE_TOKEN) {
//                        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.PKI_SOFTWARE) != 0) {
//                            aStatus = new AxiomStatus();
//                            aStatus.error = "This feature is not available in this license!!!";
//                            aStatus.errorcode = -101;
////                            return aStatus;
//                            throw new AxiomException(aStatus.error);
//                        }
////                        subcat = AxiomCredentialDetails.PKI_SOFTWARE_TOKEN;
//                        int cat = 1;
//                        int subcat = 0;
//                        PKITokenManagement oManagement = new PKITokenManagement();
//                        Pkitokens tokenSelected = oManagement.getPkiObjByUserid(channelid, UserId, cat);
//                        if (tokenSelected == null) {
//                            aStatus.errorcode = -2;
//                            aStatus.error = "Desired Token is not assigned";
//                            throw new AxiomException(aStatus.error);
//                        } else {
//                            if (tokenSelected.getStatus() == TOKEN_STATUS_ACTIVE) {
//                                oldValue = strACTIVE;
//                            } else if (tokenSelected.getStatus() == TOKEN_STATUS_SUSPENDED) {
//                                oldValue = strSUSPENDED;
//                            } else if (tokenSelected.getStatus() == TOKEN_STATUS_UNASSIGNED) {
//                                oldValue = strUNASSIGNED;
//                            }
//
//                            if (credentials.tokenDetails[i].status == TOKEN_STATUS_ACTIVE) {
//                                strValue = strACTIVE;
//                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_SUSPENDED) {
//                                strValue = strSUSPENDED;
//                            } else if (credentials.tokenDetails[i].status == TOKEN_STATUS_UNASSIGNED) {
//                                strValue = strUNASSIGNED;
//                            }
//                            int res = -1;
//                            String secret = null;
//                            AxiomTokenData tokenData = new AxiomTokenData();
//                            SettingsManagement settObj = new SettingsManagement();
//                            Object obj = settObj.getSetting(sessionid, channelid, SettingsManagement.Token, SettingsManagement.PREFERENCE_ONE);
//                            TokenSettings tSettingObj = null;
//                            if (obj != null) {
//                                tSettingObj = (TokenSettings) obj;
//                            }
//                            if (tSettingObj == null) {
//                                aStatus.error = "OTP Token Setting not Found!!!";
//                                aStatus.errorcode = -101;
//                                throw new AxiomException(aStatus.error);
//                            }
//                            String regcodeDB = AxiomProtect.AccessData(tokenSelected.getRegcode());
//                            if (regcode.contains(regcodeDB)) {
//                                CertificateManagement certMngt = new CertificateManagement();
//                                Certificates certObj = certMngt.getCertificate(sessionid, channelid, UserId);
//                                String cert = null;
//                                String hiddenKey = null;
//                                String visibleKey = null;
//                                String privateKeyString = null;
//                                if (certObj != null) {
//                                    try {
//                                        cert = certObj.getCertificate();
////                                        byte[] pfxAsBytes = Base64.decode(certObj.getPfx());
////                                        ByteArrayInputStream fis = new ByteArrayInputStream(Base64.decode(pfxAsBytes));
//                                        ByteArrayInputStream fis = new ByteArrayInputStream(Base64.decode(certObj.getPfx()));
//                                        KeyStore ks = KeyStore.getInstance("PKCS12");
//                                        ks.load(fis, certObj.getPfxpassword().toCharArray());
//                                        for (Enumeration num = ks.aliases(); num.hasMoreElements();) {
//                                            String alias = (String) num.nextElement();
//                                            if (ks.isKeyEntry(alias)) {
////                                                PrivateKey priKey = (PrivateKey) ks.getKey(alias, certObj.getPfxpassword().toCharArray());
////                                                hiddenKey = new String(Base64.encode(priKey.getEncoded()));
////                                                RSAPrivateCrtKey privk = (RSAPrivateCrtKey) priKey;
////                                                RSAPublicKeySpec publicKeySpec = new java.security.spec.RSAPublicKeySpec(privk.getModulus(), privk.getPublicExponent());
////                                                KeyFactory keyFactory = KeyFactory.getInstance("RSA");
////                                                PublicKey myPublicKey = keyFactory.generatePublic(publicKeySpec);
////                                                visibleKey = new String(Base64.encode(myPublicKey.getEncoded()));
//
//                                                PrivateKey priKey = (PrivateKey) ks.getKey(alias, certObj.getPfxpassword().toCharArray());
//                                                hiddenKey = new String(Base64.encode(priKey.getEncoded()));
//                                                RSAPrivateCrtKey privk = (RSAPrivateCrtKey) priKey;
//                                                RSAPublicKeySpec publicKeySpec = new java.security.spec.RSAPublicKeySpec(privk.getModulus(), privk.getPublicExponent());
//                                                RSAPrivateKeySpec privateKeySpec = new java.security.spec.RSAPrivateKeySpec(privk.getModulus(), privk.getPrivateExponent());
//                                                KeyFactory keyFactory = KeyFactory.getInstance("RSA");
//                                                PublicKey myPublicKey = keyFactory.generatePublic(publicKeySpec);
//
//                                                byte[] privkey = Base64.decode(hiddenKey);
//                                                PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privkey);
//                                                RSAPrivateCrtKey privKey = (RSAPrivateCrtKey) keyFactory.generatePrivate(keySpec);
//
//                                                //nilesh
//                                                char[] aliasPassPhrase = certObj.getPfxpassword().toCharArray();
//                                                KeyPair keyPair = getPrivateKey(ks, alias, aliasPassPhrase);
//                                                StringWriter stringWriter = new StringWriter();
//                                                PEMWriter pemWriter = new PEMWriter(stringWriter);
//                                                pemWriter.writeObject(keyPair.getPrivate());
//                                                pemWriter.close();
//                                                privateKeyString = stringWriter.toString();
//                                                System.out.println("privateKeyString :" + privateKeyString);
//                                                //end nilesh
//
//                                                visibleKey = new String(Base64.encode(myPublicKey.getEncoded()));
//                                                PrivateKey myPrivateKey = keyFactory.generatePrivate(privateKeySpec);
//                                                hiddenKey = new String(Base64.encode(myPrivateKey.getEncoded()));
//
//                                                String strprivKey = new String(Base64.encode(privKey.getEncoded()));
//                                                System.out.println("privKey :" + strprivKey);
//                                            }
//                                        }
//                                    } catch (Exception ex) {
//                                        ex.printStackTrace();
//                                    }
//                                } else {
//                                    aStatus.error = "Registration code not Matched!!!";
//                                    aStatus.errorcode = -101;
//                                    throw new AxiomException(aStatus.error);
//                                }
//                                if (cert != null && hiddenKey != null && visibleKey != null) {
//                                    retValue = oManagement.ChangeStatus(sessionid, channelid, UserId, PKITokenManagement.TOKEN_STATUS_ACTIVE, cat);
//                                    if (retValue == 0 || retValue == -6) {
//                                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
//                                                channel.getName(),
//                                                session.getLoginid(), session.getLoginid(),
//                                                new Date(),
//                                                "CHANGE STATUS", aStatus.error, aStatus.errorcode,
//                                                "Token Management",
//                                                "Current Status=" + oldValue,
//                                                "New Status=" + "Active",
//                                                "PKITOKEN", UserId);
//                                        tokenData.certificate = cert;
////                                        tokenData.certificatePassword = certObj.getPfxpassword();
//                                        tokenData.certificatePrivateKey = privateKeyString;
////                                        tokenData.certificatePublicKey = visibleKey;
//                                        tokenData.PublicWebTokenExpiryTime = tSettingObj.getiSWWebTokenExpiryTime();
//                                        tokenData.invalidPinAttempt = tSettingObj.getiSWWebTokenPinAttempt();
//                                        return tokenData;
//                                    } else {
//                                        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
//                                                channel.getName(),
//                                                session.getLoginid(), session.getLoginid(),
//                                                new Date(),
//                                                "CHANGE STATUS", aStatus.error, aStatus.errorcode,
//                                                "Token Management",
//                                                "Current Status=" + oldValue,
//                                                "New Status= Failed to change status.",
//                                                "OTPTOKENS", UserId);
//                                        return null;
//                                    }
//                                }
//
//                            }
//
//                        }
//                    }
////                    }
//
//                }
//
//            }
////            return null;
//            throw new AxiomException(aStatus.error);
//        }
//        return null;
//    }
    //manoj
    @Override
    public AxiomStatus ALertEvent(String sessionid, String userid, String signData, int type, String integrityCheckString) throws AxiomException {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                System.out.println("ALertEvent::sessionid::" + sessionid);

            }

        } catch (Exception ex) {
        }
//nilesh
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Licence is invalid";
            aStatus.errorcode = -100;
//            return aStatus;
            throw new AxiomException(aStatus.error);
        }
        AxiomStatus aStatus = new AxiomStatus();
//        if (signData == null) {
//            aStatus.error = "Input Data is Null!!!";
//            aStatus.errorcode = -1;
//            throw new AxiomException(aStatus.error);
//        }
        byte[] SHA1hash = null;
        if (signData == null) {
            SHA1hash = UtilityFunctions.SHA1(sessionid + userid + type);
        } else {
            SHA1hash = UtilityFunctions.SHA1(sessionid + userid + type + signData);
        }
//        String integritycheck = new String(Base64.encode(SHA1hash));
        String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//        String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java
        if (integrityCheckString != null) {
            if (integrityCheckString.equals(integritycheck) != true) {
                aStatus.error = "Input Data is Tampered!!!";
                aStatus.errorcode = -76;
//                return aStatus;
                throw new AxiomException(aStatus.error);

            }
        }

        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);
        if (session == null) {

            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
//            return aStatus;
            throw new AxiomException(aStatus.error);
        }
        aStatus.error = "ERROR";
        aStatus.errorcode = -2;
        String oldValue = "";
        String strValue = "";

        int retValue = -1;
        if (session != null) {

            SettingsManagement setManagement = new SettingsManagement();

            String channelid = session.getChannelid();
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(channelid);
            if (channel == null) {
                return null;
            }

            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
            if (channelProfileObj != null) {
                int retVal = sManagement.getServerStatus(channelProfileObj);
                if (retVal != 0) {

                    aStatus = new AxiomStatus();
                    aStatus.error = "Server Down for maintainance,Please try later!!!";
                    aStatus.errorcode = -48;
//                    return aStatus;
                    throw new AxiomException(aStatus.error);
                }
            }

            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        OperatorsManagement oManagement = new OperatorsManagement();
                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
//                            return aStatus;
                            throw new AxiomException(aStatus.error);
                        }

                        suTemplate.close();
                        sTemplate.close();
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                        return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                    aStatus.error = "INVALID IP REQUEST";
                    aStatus.errorcode = -8;
//                    return aStatus;
                    throw new AxiomException(aStatus.error);
                }
            }
            boolean bSentToUser = true;
            int subcategory = 1;

            MobileTrustManagment mManagment = new MobileTrustManagment();
            if (type == AxiomCredentialDetails.SOFTWARE_TOKEN) {

                if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
                    aStatus = new AxiomStatus();
                    aStatus.error = "This feature is not available in this license!!!";
                    aStatus.errorcode = -101;
                    return aStatus;
                }

                int cat = OTP_TOKEN_SOFTWARE;
                int subcat = OTP_TOKEN_SOFTWARE_WEB;

                OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                TokenStatusDetails tokenSelected = oManagement.getToken(sessionid, session.getChannelid(), userid, cat);

                if (tokenSelected == null) {
                    aStatus.errorcode = -2;
                    aStatus.error = "Desired Token is not assigned";
                    return aStatus;
                } else {
                    if (tokenSelected.Status == TOKEN_STATUS_ACTIVE) {
                        oldValue = strACTIVE;
                    } else if (tokenSelected.Status == TOKEN_STATUS_SUSPENDED) {
                        oldValue = strSUSPENDED;
                    } else if (tokenSelected.Status == TOKEN_STATUS_UNASSIGNED) {
                        oldValue = strUNASSIGNED;
                    }

                    strValue = "LOCKED";

                    retValue = oManagement.ChangeStatus(sessionid, session.getChannelid(), userid, OTPTokenManagement.TOKEN_STATUS_LOCKEd, cat, subcat);

                    if (retValue == 0) {
                        aStatus.error = "SUCCESS";
                        aStatus.errorcode = 0;
                    } else if (retValue == -6) {
                        aStatus.error = "Token is already locked";
                        aStatus.errorcode = -6;
                    }

                    audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                            channel.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(),
                            "CHANGE STATUS", aStatus.error, aStatus.errorcode,
                            "Token Management",
                            "Current Status=" + oldValue,
                            "New Status=" + strValue,
                            "OTPTOKENS", userid);

                    return aStatus;

                }

            } else if (type == OTP_TOKEN_SOFTWARE_MOBILE) {

            }

        }

        throw new AxiomException(aStatus.error);
    }

    private byte[] SHA1(String message) {
        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            byte[] array = sha1.digest(message.getBytes());
            return array;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public AxiomStatus initTransaction(InitTransactionPackage package1, String integrityCheckString, String trustPayLoad) throws AxiomException {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                System.out.println("ALertEvent::sessionid::" + package1.getSessionid());

            }

        } catch (Exception ex) {

        }

        try {
//nilesh
//        int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "Licence is invalid";
//            aStatus.errorcode = -100;
////            return aStatus;
//            throw new AxiomException(aStatus.error);
//        }
//
//        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.TWO_WAY_AUTH) != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "This feature is not available in this license!!!";
//            aStatus.errorcode = -100;
////            return aStatus;
//            throw new AxiomException(aStatus.error);
//        }

            AxiomStatus aStatus = new AxiomStatus();
//        if (signData == null) {
//            aStatus.error = "Input Data is Null!!!";
//            aStatus.errorcode = -1;
//            throw new AxiomException(aStatus.error);
//        }
            byte[] SHA1hash = null;
            if (trustPayLoad == null) {
                SHA1hash = UtilityFunctions.SHA1(package1.getSessionid() + package1.getUserid() + package1.getAuthid());
            } else {
                SHA1hash = UtilityFunctions.SHA1(package1.getSessionid() + package1.getUserid() + package1.getAuthid() + trustPayLoad);
            }
//        String integritycheck = new String(Base64.encode(SHA1hash));
            String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//        String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java
            if (integrityCheckString != null) {
                if (integrityCheckString.equals(integritycheck) != true) {
                    aStatus.error = "Input Data is Tampered!!!";
                    aStatus.errorcode = -76;
//                return aStatus;
                    throw new AxiomException(aStatus.error);

                }
            }

            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(package1.getSessionid());
            if (session == null) {

                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
//            return aStatus;
                throw new AxiomException(aStatus.error);
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";

            int retValue = -1;
            if (session != null) {

                SettingsManagement setManagement = new SettingsManagement();

                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    return null;
                }

                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {

                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                        return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }

                if (trustPayLoad != null) {
                    MobileTrustSettings mSettings = null;

                    Object obj = setManagement.getSetting(package1.getSessionid(), channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
                    if (obj != null) {
                        if (obj instanceof MobileTrustSettings) {
                            mSettings = (MobileTrustSettings) obj;
                        }
                    }

                    if (mSettings == null) {
                        aStatus.error = "Mobile Trust Setting is not configured!!!";
                        aStatus.errorcode = -23;
//                    throw new Exception(aStatus.error);
                        return aStatus;
                    }

                    if (mSettings.bGeoFencing == true) {
                        GeoLocationManagement gManagement = new GeoLocationManagement();
                        AXIOMStatus axioms = gManagement.validateGeoLocation(package1.getSessionid(), channelid, package1.getUserid(), trustPayLoad);
                        if (axioms == null) {
                            aStatus.error = "Location Not Found!!!";
                            aStatus.errorcode = -115;
                        } else if (axioms.iStatus != 0) {

                            aStatus.error = axioms.strStatus;
                            aStatus.errorcode = axioms.iStatus;
                            //     String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
                            audit.AddAuditTrail(package1.getSessionid(), channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                                    session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorcode,
                                    "Token Management", "Invalid Location", null, itemtype,
                                    session.getLoginid());

//                        throw new Exception(aStatus.error);
                            return aStatus;
                        }
                    }
                }

                String sessionId = session.getSessionid();
                AuthUser user = new UserManagement().getUser(sessionId, channelid, package1.getUserid());
                if (user == null) {
                    aStatus.error = "INVALID USERID";
                    aStatus.errorcode = -63;
//                    return aStatus;
                    throw new AxiomException(aStatus.error);
                } else {
                    ///////////////////////////////////////////////////////////////////////

                    Twowayauth twowayauth = new TwowayauthManagement().getAuthDetailsByUserId(channelid, user.getUserId(), sessionId);
                    Txdetails txdetail = new Txdetails();
                    txdetail.setType(package1.getType());
                    if (twowayauth == null) {
                        aStatus.error = "USER NOT REGISTERED FOR INTERNET BANKING";
                        aStatus.errorcode = -63;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    } else {
                        try {
                            JSONObject json1 = new JSONObject(twowayauth.getType());
                            String status = json1.getString(txdetail.getTypeinString(txdetail.getType()));
                            if (status.equalsIgnoreCase("Suspended")) {
                                aStatus.error = "USER NOT REGISTERED FOR THIS TYPE OF INTERNET BANKING";
                                aStatus.errorcode = -63;
                                return aStatus;
//                            throw new AxiomException(aStatus.error);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    }

                    txdetail.setCreatedOn(new Date());
                    txdetail.setExpmin(package1.getExpirytimeInMins());
                    txdetail.setUserid(package1.getUserid());
                    txdetail.setSessionid(package1.getSessionid());
                    txdetail.setChannelid(package1.getChannelid());
                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.MINUTE, package1.getExpirytimeInMins());
                    txdetail.setExpiredOn(calendar.getTime());
                    txdetail.setStatus(Txdetails.PENDING);
                    txdetail.setQnAStatus(Txdetails.PENDING);
                    txdetail.setMobileno(user.phoneNo);
                    txdetail.setChannelid(channelid);
                    txdetail.setTxmsg(package1.getMessage());
                    txdetail.setQuestion(package1.getQuestion());
                    txdetail.setTransactionId(package1.getAuthid());
                    int res = new TxManagement().addTxDetails(txdetail);
                    if (txdetail.getType() == PUSH) {
                        Registerdevicepush registerdevicepush = new PushNotificationDeviceManagement().GetRegisterPushDeviceByUserID(sessionId, channelid, txdetail.getUserid());
                        if (registerdevicepush == null) {
                            aStatus.error = "USER NOT REGISTERED FOR PUSH";
                            aStatus.errorcode = -63;
                            return aStatus;
//                        throw new AxiomException(aStatus.error);
                        } else {
                            JSONObject json = new JSONObject();
//                        json.put("_msg", txdetail.getTxmsg());
                            json.put("message", package1.getFriendlyMsg());
                            json.put("title", "Axiom Protect");
                            json.put("_txid", txdetail.getTransactionId());
                            AXIOMStatus status = new SendNotification().SendOnPush(channelid, registerdevicepush.getGoogleregisterid(), user.email, json.toString(), ANDROID);
                            aStatus.error = status.strStatus;
                            aStatus.errorcode = status.iStatus;
                            return aStatus;
                        }
                    } else if (txdetail.getType() == SMS) {
                        AXIOMStatus status = new SendNotification().SendOnMobile(channelid, user.phoneNo, txdetail.getTxmsg() + "\n" + txdetail.getQuestion(), SendNotification.SMS, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                        aStatus.error = status.strStatus;
                        aStatus.errorcode = status.iStatus;
                        return aStatus;
                    } else if (txdetail.getType() == VOICE) {
                        AXIOMStatus status = new SendNotification().SendOnMobileByPreference(channelid, user.phoneNo, txdetail.getTxmsg() + "\n" + txdetail.getQuestion(), SendNotification.VOICE, SettingsManagement.PREFERENCE_ONE, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                        aStatus.error = status.strStatus;
                        aStatus.errorcode = status.iStatus;
                        return aStatus;
                    } else if (txdetail.getType() == MISSEDCALL) {
//                    AXIOMStatus status = new SendNotification().SendOnMobile(channelid, user.phoneNo, txdetails.getMsg(), 1, 1);
                        aStatus.error = "completed";
                        aStatus.errorcode = 0;
                        return aStatus;
                    }
                }
            }

            throw new AxiomException(aStatus.error);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public TransactionStatus getStatus(String sessionId, String txId, String integrityCheckString, String trustPayLoad) throws AxiomException {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                System.out.println("ALertEvent::sessionid::" + sessionId);

            }

        } catch (Exception ex) {
        }
//nilesh
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            aStatus.error = "Licence is invalid";
//            aStatus.errorcode = -100;
////            return aStatus;
//            throw new AxiomException(aStatus.error);
        }
        AxiomStatus aStatus = new AxiomStatus();
//        if (signData == null) {
//            aStatus.error = "Input Data is Null!!!";
//            aStatus.errorcode = -1;
//            throw new AxiomException(aStatus.error);
//        }
        byte[] SHA1hash = null;
        if (trustPayLoad == null) {
            SHA1hash = UtilityFunctions.SHA1(sessionId + txId);
        } else {
            SHA1hash = UtilityFunctions.SHA1(sessionId + txId + trustPayLoad);
        }
//        String integritycheck = new String(Base64.encode(SHA1hash));
        String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//        String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java
        if (integrityCheckString != null) {
            if (integrityCheckString.equals(integritycheck) != true) {
                aStatus.error = "Input Data is Tampered!!!";
                aStatus.errorcode = -76;
//                return aStatus;
                throw new AxiomException(aStatus.error);

            }
        }

        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionId);
        if (session == null) {

            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
//            return aStatus;
            throw new AxiomException(aStatus.error);
        }
        aStatus.error = "ERROR";
        aStatus.errorcode = -2;
        String oldValue = "";
        String strValue = "";

        int retValue = -1;
        if (session != null) {

            SettingsManagement setManagement = new SettingsManagement();

            String channelid = session.getChannelid();
            ChannelManagement cManagement = new ChannelManagement();
            Channels channel = cManagement.getChannelByID(channelid);
            if (channel == null) {
                return null;
            }

            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
            if (channelProfileObj != null) {
                int retVal = sManagement.getServerStatus(channelProfileObj);
                if (retVal != 0) {

                    aStatus = new AxiomStatus();
                    aStatus.error = "Server Down for maintainance,Please try later!!!";
                    aStatus.errorcode = -48;
//                    return aStatus;
                    throw new AxiomException(aStatus.error);
                }
            }

            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        OperatorsManagement oManagement = new OperatorsManagement();
                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
//                            return aStatus;
                            throw new AxiomException(aStatus.error);
                        }

                        suTemplate.close();
                        sTemplate.close();
                        aStatus.error = "INVALID IP REQUEST";
//                        return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                    aStatus.error = "INVALID IP REQUEST";
                    aStatus.errorcode = -8;
//                    return aStatus;
                    throw new AxiomException(aStatus.error);
                }
            }

            Txdetails txdetails = new TxManagement().getTxDetailsByTxId(channelid, txId, sessionId);
            if (txdetails != null) {
                TransactionStatus status = new TransactionStatus();
                status.setTransactionStatus(txdetails.getStatusinString(txdetails.getStatus()));
                status.setQuestionAndAnswerstatus(txdetails.getStatusinString(txdetails.getQnAStatus()));
                status.setResponse(txdetails.getResponse());
                return status;
            } else {
                aStatus.error = "INVALID Tx. Id";
                aStatus.errorcode = -63;
//                    return aStatus;
                throw new AxiomException(aStatus.error);
            }
        }
        throw new AxiomException(aStatus.error);
    }

    @Override
    public AxiomStatus changeAuthStatus(String sessionId, String userid, int type, int status, String integrityCheckString, String trustPayLoad) throws AxiomException {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                System.out.println("ALertEvent::sessionid::" + sessionId);

            }

        } catch (Exception ex) {
        }
//nilesh
        try {
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Licence is invalid";
                aStatus.errorcode = -100;
//            return aStatus;
                throw new AxiomException(aStatus.error);
            }
            AxiomStatus aStatus = new AxiomStatus();
//        if (signData == null) {
//            aStatus.error = "Input Data is Null!!!";
//            aStatus.errorcode = -1;
//            throw new AxiomException(aStatus.error);
//        }
            byte[] SHA1hash = null;
            if (trustPayLoad == null) {
                SHA1hash = UtilityFunctions.SHA1(sessionId + userid + type);
            } else {
                SHA1hash = UtilityFunctions.SHA1(sessionId + userid + type + trustPayLoad);
            }
//        String integritycheck = new String(Base64.encode(SHA1hash));
            String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//        String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java
            if (integrityCheckString != null) {
                if (integrityCheckString.equals(integritycheck) != true) {
                    aStatus.error = "Input Data is Tampered!!!";
                    aStatus.errorcode = -76;
//                return aStatus;
                    throw new AxiomException(aStatus.error);

                }
            }

            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {

                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
//            return aStatus;
                throw new AxiomException(aStatus.error);
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";

            int retValue = -1;
            if (session != null) {

                SettingsManagement setManagement = new SettingsManagement();

                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                Channels channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    return null;
                }

                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {

                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                        return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }

                if (trustPayLoad != null) {
                    MobileTrustSettings mSettings = null;

                    Object obj = setManagement.getSetting(sessionId, channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
                    if (obj != null) {
                        if (obj instanceof MobileTrustSettings) {
                            mSettings = (MobileTrustSettings) obj;
                        }
                    }

                    if (mSettings == null) {
                        aStatus.error = "Mobile Trust Setting is not configured!!!";
                        aStatus.errorcode = -23;
//                    throw new Exception(aStatus.error);
                        return aStatus;
                    }

                    if (mSettings.bGeoFencing == true) {
                        GeoLocationManagement gManagement = new GeoLocationManagement();
                        AXIOMStatus axioms = gManagement.validateGeoLocation(sessionId, channelid, userid, trustPayLoad);
                        if (axioms == null) {
                            aStatus.error = "Location Not Found!!!";
                            aStatus.errorcode = -115;
                        } else if (axioms.iStatus != 0) {

                            aStatus.error = axioms.strStatus;
                            aStatus.errorcode = axioms.iStatus;
                            //     String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
                            audit.AddAuditTrail(sessionId, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
                                    session.getLoginid(), new Date(), "Change Token Status", aStatus.error, aStatus.errorcode,
                                    "Token Management", "Invalid Location", null, itemtype,
                                    session.getLoginid());

//                        throw new Exception(aStatus.error);
                            return aStatus;
                        }
                    }
                }

                Twowayauth twowayauth = new TwowayauthManagement().getAuthDetailsByUserId(channelid, userid, sessionId);
                JSONObject json = new JSONObject();
                if (twowayauth == null) {
                    twowayauth = new Twowayauth();
                    twowayauth.setAttempts(0);
                    json.put("SMS", "Suspended");
                    json.put("Callback", "Suspended");
                    json.put("Missed", "Suspended");
                    json.put("Push", "Suspended");
                    if (type == Txdetails.SMS) {
                        if (status == 0) {
                            json.put("SMS", "Active");
                        }
                    } else if (type == Txdetails.VOICE) {
                        if (status == 0) {
                            json.put("Callback", "Active");
                        }
                    } else if (type == Txdetails.MISSEDCALL) {
                        if (status == 0) {
                            json.put("Missed", "Active");
                        }
                    } else if (type == Txdetails.PUSH) {
                        if (status == 0) {
                            json.put("Push", "Active");
                        }
                    }
                    twowayauth.setChannelid(channelid);
                    twowayauth.setCreationdatetime(new Date());
                    twowayauth.setLastaccessdatetime(new Date());
                    twowayauth.setType(json.toString());
                    twowayauth.setUserid(userid);
                    int res = new TwowayauthManagement().addAuthDetails(twowayauth, sessionId);
                    aStatus.errorcode = res;
                    return aStatus;
                } else {
                    twowayauth.setAttempts(0);
                    try {
                        json = new JSONObject(twowayauth.getType());
                    } catch (Exception pe) {
                        pe.printStackTrace();
                    }
                    if (type == Txdetails.SMS) {
                        if (status == 0) {
                            json.put("SMS", "Active");
                        } else {
                            json.put("SMS", "Suspended");
                        }
                    } else if (type == Txdetails.VOICE) {
                        if (status == 0) {
                            json.put("Callback", "Active");
                        } else {
                            json.put("Callback", "Suspended");
                        }
                    } else if (type == Txdetails.MISSEDCALL) {
                        if (status == 0) {
                            json.put("Missed", "Active");
                        } else {
                            json.put("Missed", "Suspended");
                        }
                    } else if (type == Txdetails.PUSH) {
                        if (status == 0) {
                            json.put("Push", "Active");
                        } else {
                            json.put("Push", "Suspended");
                        }
                    }
                    twowayauth.setChannelid(channelid);
                    twowayauth.setLastaccessdatetime(new Date());
                    twowayauth.setType(json.toString());
                    int res = new TwowayauthManagement().editAuthDetails(twowayauth, sessionId);
                    aStatus.errorcode = res;
                    return aStatus;
                }
            }
            throw new AxiomException(aStatus.error);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public SSOData getSSODetails(String sessionId, String userid, String integrityCheckString) throws AxiomException {
        String strDebug = null;
        Channels channel = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                System.out.println("ALertEvent::sessionid::" + sessionId);

            }

        } catch (Exception ex) {
        }
//
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Licence is invalid";
            aStatus.errorcode = -100;
//            return aStatus;
            throw new AxiomException(aStatus.error);
        }
        AxiomStatus aStatus = new AxiomStatus();
//        if (signData == null) {
//            aStatus.error = "Input Data is Null!!!";
//            aStatus.errorcode = -1;
//            throw new AxiomException(aStatus.error);
//        }
        byte[] SHA1hash = null;
        SHA1hash = UtilityFunctions.SHA1(sessionId + userid);
//        String integritycheck = new String(Base64.encode(SHA1hash));
        String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//        String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java
        if (integrityCheckString != null) {
            if (integrityCheckString.equals(integritycheck) != true) {
                aStatus.error = "Input Data is Tampered!!!";
                aStatus.errorcode = -76;
//                return aStatus;
                throw new AxiomException(aStatus.error);

            }
        }

        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionId);
        if (session == null) {

            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
//            return aStatus;
            throw new AxiomException(aStatus.error);
        }
        aStatus.error = "ERROR";
        aStatus.errorcode = -2;
        String oldValue = "";
        String strValue = "";

        int retValue = -1;
        if (session != null) {

            SettingsManagement setManagement = new SettingsManagement();

            String channelid = session.getChannelid();
            ChannelManagement cManagement = new ChannelManagement();
            channel = cManagement.getChannelByID(channelid);
            if (channel == null) {
                return null;
            }

            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
            if (channelProfileObj != null) {
                int retVal = sManagement.getServerStatus(channelProfileObj);
                if (retVal != 0) {

                    aStatus = new AxiomStatus();
                    aStatus.error = "Server Down for maintainance,Please try later!!!";
                    aStatus.errorcode = -48;
//                    return aStatus;
                    throw new AxiomException(aStatus.error);
                }
            }

            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        OperatorsManagement oManagement = new OperatorsManagement();
                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
//                            return aStatus;
                            throw new AxiomException(aStatus.error);
                        }

                        suTemplate.close();
                        sTemplate.close();
                        aStatus.error = "INVALID IP REQUEST";
//                        return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                    aStatus.error = "INVALID IP REQUEST";
                    aStatus.errorcode = -8;
//                    return aStatus;
                    throw new AxiomException(aStatus.error);
                }
            }

        }
        byte[] SHA1hashh = SHA1("" + new Date() + userid);
        String ssoId = new String(Base64.encode(SHA1hashh));
        Ssodetails ssodetails = new Ssodetails();
        ssodetails.setChannelId(channel.getChannelid());
        ssodetails.setCreatedOn(new Date());
        ssodetails.setLastUpdateOn(new Date());
        ssodetails.setSsoId(ssoId);
        ssodetails.setStatus(1);
        ssodetails.setUserId(userid);
        ssodetails.setAppId("");
        int res = new SSOManagement().addDetails(sessionId, ssodetails);
        if (res == -1) {
            aStatus.error = "Error in Adding SSO Details";
            aStatus.errorcode = -1;
//                    return aStatus;
            throw new AxiomException(aStatus.error);
        } else {
            SSOData oData = new SSOData();
            oData.error = "success";
            oData.errorcode = 0;
            oData.ssoId = ssoId;
            oData.resDetails = new ArrayList<ResourceDetails>();
            Webresource[] ws = new ResourceManagement().getAllResources(sessionId, channel.getChannelid());
            if (ws != null) {
                for (int i = 0; i < ws.length; i++) {
                    ResourceDetails details = new ResourceDetails();
                    details.url = ws[i].getWeburl() + "?ssoid=" + ssoId;
                    File file = new File(ws[i].getImageUrl());
                    if (file.exists()) {
                        try {
                            FileInputStream fis = new FileInputStream(file);
                            ByteArrayOutputStream bos = new ByteArrayOutputStream();
                            byte[] buf = new byte[1024];

                            for (int readNum; (readNum = fis.read(buf)) != -1;) {
                                bos.write(buf, 0, readNum);
                            }
                            byte[] bytes = bos.toByteArray();
                            details.image = new String(Base64.encode(bytes));

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                    details.resId = new String(Base64.encode((ws[i].getResourceid() + ":" + ws[i].getResourcename()).getBytes()));
                    details.resName = ws[i].getResourcename();
                    oData.resDetails.add(details);
                }

            }
            return oData;
        }

    }

    @Override
    public SSOResponse validateSSODetails(String sessionId, String ssoId, String apId, String integrityCheckString) throws AxiomException {
        String strDebug = null;
        Channels channel = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                System.out.println("ALertEvent::sessionid::" + sessionId);

            }

        } catch (Exception ex) {
        }
//nilesh
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            aStatus.error = "Licence is invalid";
            aStatus.errorcode = -100;
//            return aStatus;
            throw new AxiomException(aStatus.error);
        }
        AxiomStatus aStatus = new AxiomStatus();
//        if (signData == null) {
//            aStatus.error = "Input Data is Null!!!";
//            aStatus.errorcode = -1;
//            throw new AxiomException(aStatus.error);
//        }
        byte[] SHA1hash = null;
        SHA1hash = UtilityFunctions.SHA1(sessionId + ssoId);
//        String integritycheck = new String(Base64.encode(SHA1hash));
        String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//        String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java
        if (integrityCheckString != null) {
            if (integrityCheckString.equals(integritycheck) != true) {
                aStatus.error = "Input Data is Tampered!!!";
                aStatus.errorcode = -76;
//                return aStatus;
                throw new AxiomException(aStatus.error);

            }
        }

        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionId);
        if (session == null) {

            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
//            return aStatus;
            throw new AxiomException(aStatus.error);
        }
        aStatus.error = "ERROR";
        aStatus.errorcode = -2;
        String oldValue = "";
        String strValue = "";

        int retValue = -1;
        if (session != null) {

            SettingsManagement setManagement = new SettingsManagement();

            String channelid = session.getChannelid();
            ChannelManagement cManagement = new ChannelManagement();
            channel = cManagement.getChannelByID(channelid);
            if (channel == null) {
                return null;
            }

            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
            if (channelProfileObj != null) {
                int retVal = sManagement.getServerStatus(channelProfileObj);
                if (retVal != 0) {

                    aStatus = new AxiomStatus();
                    aStatus.error = "Server Down for maintainance,Please try later!!!";
                    aStatus.errorcode = -48;
//                    return aStatus;
                    throw new AxiomException(aStatus.error);
                }
            }

            Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
            if (ipobj != null) {
                GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                int checkIp = 1;
                if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                    checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                } else {
                    checkIp = 1;
                }
                if (iObj.ipstatus == 0 && checkIp != 1) {
                    if (iObj.ipalertstatus == 0) {
                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                        Session sTemplate = suTemplate.openSession();
                        TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                        Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                        OperatorsManagement oManagement = new OperatorsManagement();
                        Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                        if (aOperator != null) {
                            String[] emailList = new String[aOperator.length - 1];
                            for (int i = 1; i < aOperator.length; i++) {
                                emailList[i - 1] = aOperator[i].getEmailid();
                            }
                            if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                String strsubject = templatesObj.getSubject();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                if (strmessageBody != null) {
                                    // Date date = new Date();
                                    strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                    strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                    strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                    strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                    strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                }

                                SendNotification send = new SendNotification();
                                AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                        emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            aStatus.errorcode = -8;
//                            return aStatus;
                            throw new AxiomException(aStatus.error);
                        }

                        suTemplate.close();
                        sTemplate.close();
                        aStatus.error = "INVALID IP REQUEST";
//                        return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                    aStatus.error = "INVALID IP REQUEST";
                    aStatus.errorcode = -8;
//                    return aStatus;
                    throw new AxiomException(aStatus.error);
                }
            }

        }
        Ssodetails ssodetails = new SSOManagement().getDetails(sessionId, ssoId, channel.getChannelid());
        String strapId = new String(Base64.decode(apId));
        int id = Integer.parseInt(strapId.split(":")[0]);
        Webresource webresource = new ResourceManagement().getResourcebyIds(channel.getChannelid(), id);
        if (ssodetails == null || webresource == null) {
            SSOResponse response = new SSOResponse();
            response.errorcode = -1;
            response.message = "No Data Found";
            return response;
        } else {
            SSOResponse response = new SSOResponse();
            response.errorcode = 0;
            response.message = "success";
            AuthUser user = new UserManagement().getUser(sessionId, channel.getChannelid(), ssodetails.getUserId());
            try {
                Map map = (Map) UtilityFunctions.deserialize(user.sso);
                Object obj = map.get(apId);
                if (obj == null) {
                    response.result = null;
                } else {
                    JSONObject jsonU = new JSONObject(obj.toString());
                    JSONObject jsonW = new JSONObject(webresource.getSsofields());
                    DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
                    Document doc = docBuilder.newDocument();
                    Element rootElement = doc.createElement("response");
                    doc.appendChild(rootElement);
                    if (jsonW.get("_field1") != null) {
                        if (!jsonW.get("_field1").toString().isEmpty()) {
                            Element element = doc.createElement(jsonW.get("_field1").toString());
                            element.appendChild(doc.createTextNode(jsonU.get("_field1").toString()));
                            rootElement.appendChild(element);
                        }
                    }
                    if (jsonW.get("_field2") != null) {
                        if (!jsonW.get("_field2").toString().isEmpty()) {
                            Element element = doc.createElement(jsonW.get("_field2").toString());
                            element.appendChild(doc.createTextNode(jsonU.get("_field2").toString()));
                            rootElement.appendChild(element);
                        }
                    }
                    if (jsonW.get("_field3") != null) {
                        if (!jsonW.get("_field3").toString().isEmpty()) {
                            Element element = doc.createElement(jsonW.get("_field3").toString());
                            element.appendChild(doc.createTextNode(jsonU.get("_field3").toString()));
                            rootElement.appendChild(element);
                        }
                    }
                    if (jsonW.get("_field4") != null) {
                        if (!jsonW.get("_field4").toString().isEmpty()) {
                            Element element = doc.createElement(jsonW.get("_field4").toString());
                            element.appendChild(doc.createTextNode(jsonU.get("_field4").toString()));
                            rootElement.appendChild(element);
                        }
                    }
                    if (jsonW.get("_field5") != null) {
                        if (!jsonW.get("_field5").toString().isEmpty()) {
                            Element element = doc.createElement(jsonW.get("_field5").toString());
                            element.appendChild(doc.createTextNode(jsonU.get("_field5").toString()));
                            rootElement.appendChild(element);
                        }
                    }
                    if (jsonW.get("_field6") != null) {
                        if (!jsonW.get("_field6").toString().isEmpty()) {
                            Element element = doc.createElement(jsonW.get("_field6").toString());
                            element.appendChild(doc.createTextNode(jsonU.get("_field6").toString()));
                            rootElement.appendChild(element);
                        }
                    }
                    if (jsonW.get("_field7") != null) {
                        if (!jsonW.get("_field7").toString().isEmpty()) {
                            Element element = doc.createElement(jsonW.get("_field7").toString());
                            element.appendChild(doc.createTextNode(jsonU.get("_field7").toString()));
                            rootElement.appendChild(element);
                        }
                    }
                    if (jsonW.get("_field8") != null) {
                        if (!jsonW.get("_field8").toString().isEmpty()) {
                            Element element = doc.createElement(jsonW.get("_field8").toString());
                            element.appendChild(doc.createTextNode(jsonU.get("_field8").toString()));
                            rootElement.appendChild(element);
                        }
                    }
                    if (jsonW.get("_field9") != null) {
                        if (!jsonW.get("_field9").toString().isEmpty()) {
                            Element element = doc.createElement(jsonW.get("_field9").toString());
                            element.appendChild(doc.createTextNode(jsonU.get("_field9").toString()));
                            rootElement.appendChild(element);
                        }
                    }
                    if (jsonW.get("_field10") != null) {
                        if (!jsonW.get("_field10").toString().isEmpty()) {
                            Element element = doc.createElement(jsonW.get("_field10").toString());
                            element.appendChild(doc.createTextNode(jsonU.get("_field10").toString()));
                            rootElement.appendChild(element);
                        }
                    }
                    TransformerFactory transformerFactory = TransformerFactory.newInstance();
                    Transformer transformer = transformerFactory.newTransformer();
                    DOMSource source = new DOMSource(doc);
                    OutputStream os = new ByteArrayOutputStream(4096 * 4);
                    StreamResult consoleResult = new StreamResult(os);
                    transformer.transform(source, consoleResult);
                    response.result = new String(os.toString());
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return response;
        }
    }

    //start amol 20-10-15 addition for partner portal
    @Override
    public RSSUserDetails[] getUserByOrganisationName(String sessionId, String oraganization) throws AxiomException {

        String strDebug = null;
        Channels channel = null;
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("ALertEvent::sessionid::" + sessionId);

                }

            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Licence is invalid";
                aStatus.errorcode = -100;
//            return aStatus;
                throw new AxiomException(aStatus.error);
            }
            AxiomStatus aStatus = new AxiomStatus();
//        if (signData == null) {
//            aStatus.error = "Input Data is Null!!!";
//            aStatus.errorcode = -1;
//            throw new AxiomException(aStatus.error);
//        }
            //  byte[] SHA1hash = null;
//        SHA1hash = UtilityFunctions.SHA1(sessionId + ssoId);
//        String integritycheck = new String(Base64.encode(SHA1hash));
            //    String integritycheck = new String(Base64.encode(SHA1hash));//bouncy castle
//        String integritycheck = new String(com.sun.org.apache.xerces.internal.impl.dv.util.Base64.encode(SHA1hash));//java
//        if (integrityCheckString != null) {
//            if (integrityCheckString.equals(integritycheck) != true) {
//                aStatus.error = "Input Data is Tampered!!!";
//                aStatus.errorcode = -76;
////                return aStatus;
//                throw new AxiomException(aStatus.error);
//
//            }
//        }

            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {

                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
//            return aStatus;
                throw new AxiomException(aStatus.error);
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";

            int retValue = -1;
            if (session != null) {

                SettingsManagement setManagement = new SettingsManagement();

                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    return null;
                }

                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {

                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                        return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }
                AuthUser[] Ausers = null;
                RSSUserDetails[] rssUsers = null;

                UserManagement umgmt = new UserManagement();

                Ausers = umgmt.getUsersByOrganization(sessionId, channelid, oraganization);
                if (Ausers != null) {
                    if (Ausers.length != 0) {
                        rssUsers = new RSSUserDetails[Ausers.length];

                        for (int i = 0; i < Ausers.length; i++) {
                            String x = Ausers[i].getUserId();
                            rssUsers[i] = new RSSUserDetails();
                            rssUsers[i].userId = Ausers[i].getUserId();
                            rssUsers[i].userName = Ausers[i].getUserName();
                            rssUsers[i].emailid = Ausers[i].getEmail();
                            rssUsers[i].status = Ausers[i].getStatus();
                            rssUsers[i].phoneNumber = Ausers[i].getPhoneNo();
                            rssUsers[i].createOn = Ausers[i].getlCreatedOn();
                            rssUsers[i].lastaccessOn = Ausers[i].getlLastAccessOn();
                            rssUsers[i].organisation = Ausers[i].getOrganisation();
                            rssUsers[i].organisationUnit = Ausers[i].getOrganisationUnit();
                            rssUsers[i].country = Ausers[i].getCountry();
                            rssUsers[i].location = Ausers[i].getLocation();
                            rssUsers[i].street = Ausers[i].getStreet();
                            rssUsers[i].designation = Ausers[i].getDesignation();
                        }
                        return rssUsers;
                    }

                } else {
                    return null;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String GetPassword(String sessionId, String userid) throws AxiomException {

        String strDebug = null;
        String password = null;
        Channels channel = null;
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("ALertEvent::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Licence is invalid";
                aStatus.errorcode = -100;
                throw new AxiomException(aStatus.error);
            }
            AxiomStatus aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {

                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
//            return aStatus;
                throw new AxiomException(aStatus.error);
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";

            int retValue = -1;
            if (session != null) {

                SettingsManagement setManagement = new SettingsManagement();

                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    return null;
                }

                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {

                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                        return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }

                UserManagement umgmt = new UserManagement();
                password = umgmt.GetPassword(sessionId, channelid, userid);
                return password;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public RSSUserDetails getUser(String sessionId, String userid) throws AxiomException {
        RSSUserDetails user = null;
        String strDebug = null;
        Channels channel = null;
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("ALertEvent::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Licence is invalid";
                aStatus.errorcode = -100;
                throw new AxiomException(aStatus.error);
            }
            AxiomStatus aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {

                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
//            return aStatus;
                throw new AxiomException(aStatus.error);
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";

            int retValue = -1;
            if (session != null) {

                SettingsManagement setManagement = new SettingsManagement();

                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    return null;
                }

                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {

                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                        return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }

                UserManagement umgmt = new UserManagement();
                AuthUser au = umgmt.getUser(channelid, userid);
                if (au != null) {
                    user = new RSSUserDetails();
                    user.userId = au.userId;
                    user.userName = au.userName;
                    user.status = au.status;
                    user.userIdentity = au.userIdentity;
                    user.country = au.country;
                    user.designation = au.designation;
                    user.createOn = au.lCreatedOn;
                    user.emailid = au.email;
                    user.groupid = au.groupid;
                    user.lastaccessOn = au.lLastAccessOn;
                    user.organisation = au.organisation;
                    user.organisationUnit = au.organisationUnit;
                    user.phoneNumber = au.phoneNo;
                }
                return user;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String GeneratePassword(String sessionId, String strPassword) throws AxiomException {
        String strDebug = null;
        String password = null;
        Channels channel = null;
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("ALertEvent::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Licence is invalid";
                aStatus.errorcode = -100;
                throw new AxiomException(aStatus.error);
            }
            AxiomStatus aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {

                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
//            return aStatus;
                throw new AxiomException(aStatus.error);
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";

            int retValue = -1;
            if (session != null) {

                SettingsManagement setManagement = new SettingsManagement();

                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    return null;
                }

                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {

                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                        return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }
                PasswordTrailManagement ptm = new PasswordTrailManagement();
                password = ptm.GeneratePassword(channelid, strPassword);
                return password;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public PasswordPolicySetting getSettingInner(String sessionId) throws AxiomException {
        PasswordPolicySetting pps = null;
        String strDebug = null;
//        String password = null;
        Channels channel = null;
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Licence is invalid";
                aStatus.errorcode = -100;
                throw new AxiomException(aStatus.error);
            }
            AxiomStatus aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {

                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
//            return aStatus;
                throw new AxiomException(aStatus.error);
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";

            int retValue = -1;
            if (session != null) {

                SettingsManagement setManagement = new SettingsManagement();

                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    return null;
                }

                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {

                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                        return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }
                Object obj = setManagement.getSettingInner(channelid, SettingsManagement.PASSWORD_POLICY_SETTING, SettingsManagement.PREFERENCE_ONE);
                pps = (PasswordPolicySetting) obj;
                return pps;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public ChannelProfile getSetting(String sessionId) throws AxiomException {
        String strDebug = null;
        ChannelProfile profile = null;

        Channels channel = null;
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("ALertEvent::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Licence is invalid";
                aStatus.errorcode = -100;
                throw new AxiomException(aStatus.error);
            }
            AxiomStatus aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {

                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
//            return aStatus;
                throw new AxiomException(aStatus.error);
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";

            int retValue = -1;
            if (session != null) {

                SettingsManagement setManagement = new SettingsManagement();

                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    return null;
                }

                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {

                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                        return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }
                SettingsManagement stm = new SettingsManagement();
                Object obj = stm.getSetting(sessionId, channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                profile = (ChannelProfile) obj;
                return profile;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Operators getOperatorById(String sessionId, String id) throws AxiomException {
        String strDebug = null;
        Operators operator = null;
        Channels channel = null;
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("ALertEvent::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Licence is invalid";
                aStatus.errorcode = -100;
                throw new AxiomException(aStatus.error);
            }
            AxiomStatus aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {

                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
//            return aStatus;
                throw new AxiomException(aStatus.error);
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";

            int retValue = -1;
            if (session != null) {

                SettingsManagement setManagement = new SettingsManagement();

                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    return null;
                }

                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {

                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                        return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }

                OperatorsManagement omgmt = new OperatorsManagement();
                operator = omgmt.getOperatorById(channelid, id);
                return operator;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public int AssignPassword(String sessionId, String userid, String strPassword) throws AxiomException {
        String strDebug = null;
        int result = -1;
        Channels channel = null;
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("ALertEvent::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Licence is invalid";
                aStatus.errorcode = -100;
                throw new AxiomException(aStatus.error);
            }
            AxiomStatus aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {

                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
//            return aStatus;
                throw new AxiomException(aStatus.error);
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";

            int retValue = -1;
            if (session != null) {

                SettingsManagement setManagement = new SettingsManagement();

                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    return result;
                }

                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {

                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                        return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }

                UserManagement umgmt = new UserManagement();
                result = umgmt.AssignPassword(sessionId, channelid, userid, strPassword);
                return result;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    @Override
    public Templates LoadbyName(String sessionId, int type) throws AxiomException {
        Templates templates = null;
        String strDebug = null;
        Channels channel = null;
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("ALertEvent::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Licence is invalid";
                aStatus.errorcode = -100;
                throw new AxiomException(aStatus.error);
            }
            AxiomStatus aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {

                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
//            return aStatus;
                throw new AxiomException(aStatus.error);
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";

            int retValue = -1;
            if (session != null) {

                SettingsManagement setManagement = new SettingsManagement();

                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    return templates;
                }

                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {

                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }

                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                        return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }
                TemplateManagement tmngt = new TemplateManagement();
                if (type == RESET) {
                    templates = tmngt.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.MOBILE_USER_PASSWORD_TEMPLATE);
                } else if (type == SEND) {
                    templates = tmngt.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.MOBILE_USER_SEND_PASSWORD_TEMPLATE);
                }
                return templates;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return templates;
    }

    public int SendEmail(String sessionId, String emailId, String subject, String message, String[] arrCC, String[] arrBCC, String[] files, String[] mimetypes, int productType) throws AxiomException {
        AXIOMStatus astatus = null;
        String strDebug = null;
        Channels channel = null;
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("ALertEvent::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Licence is invalid";
                aStatus.errorcode = -100;
                throw new AxiomException(aStatus.error);
            }
            AxiomStatus aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {

                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
//            return aStatus;
                throw new AxiomException(aStatus.error);
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";
            int retValue = -1;
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    return astatus.iStatus;
                }
                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {
                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
                        throw new AxiomException(aStatus.error);
                    }
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                        return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }
                SendNotification send = new SendNotification();
                astatus = send.SendEmail(channelid, emailId, subject, message, arrCC, arrBCC, files, mimetypes, productType);
                return astatus.iStatus;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return astatus.iStatus;
    }

    @Override
    public String MD5HashPassword(String sessionId, String passwordToHash) throws AxiomException {
        AXIOMStatus astatus = null;
        String strDebug = null;
        Channels channel = null;
        String result = null;
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("ALertEvent::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Licence is invalid";
                aStatus.errorcode = -100;
                throw new AxiomException(aStatus.error);
            }
            AxiomStatus aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {

                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
//            return aStatus;
                throw new AxiomException(aStatus.error);
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";
            int retValue = -1;
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {
                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
                        throw new AxiomException(aStatus.error);
                    }
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                        return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }
                PasswordTrailManagement ptmngt = new PasswordTrailManagement();
                result = ptmngt.MD5HashPassword(passwordToHash);
                return result;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    @Override
    public void AddPasswordTrail(String sessionId, String operatorid, String password) throws AxiomException {

        AXIOMStatus astatus = null;
        String strDebug = null;
        Channels channel = null;
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("ALertEvent::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Licence is invalid";
                aStatus.errorcode = -100;
                throw new AxiomException(aStatus.error);
            }
            AxiomStatus aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {

                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";
            int retValue = -1;
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {
                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
                        throw new AxiomException(aStatus.error);
                    }
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                        return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }
                PasswordTrailManagement ptmngt = new PasswordTrailManagement();
                ptmngt.AddPasswordTrail(channelid, operatorid, password);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    @Override
    public int ValidatePassword(String sessionId, String strPassword, PasswordPolicySetting passwordSetting) throws AxiomException {
        String strDebug = null;
        Channels channel = null;
        int result = -1;
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("ALertEvent::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Licence is invalid";
                aStatus.errorcode = -100;
                throw new AxiomException(aStatus.error);
            }
            AxiomStatus aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {

                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";
            int retValue = -1;
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {
                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
                        throw new AxiomException(aStatus.error);
                    }
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                        return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }
                PasswordTrailManagement ptmngt = new PasswordTrailManagement();
                result = ptmngt.ValidatePassword(channelid, strPassword, passwordSetting);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public int SendEmailOperator(String sessionId, String emailId, String subject, String message, String[] arrCC, String[] arrBCC, String[] files, String[] mimetypes, int productType) throws AxiomException {
        AXIOMStatus astatus = null;
        String strDebug = null;
        Channels channel = null;
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("ALertEvent::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Licence is invalid";
                aStatus.errorcode = -100;
                throw new AxiomException(aStatus.error);
            }
            AxiomStatus aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {

                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
//            return aStatus;
                throw new AxiomException(aStatus.error);
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";
            int retValue = -1;
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                if (channel == null) {
                    return astatus.iStatus;
                }
                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {
                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
                        throw new AxiomException(aStatus.error);
                    }
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                        return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }
                SendNotification send = new SendNotification();
                astatus = send.SendEmailOperator(channelid, emailId, subject, message, arrCC, arrBCC, files, mimetypes, productType);
                return astatus.iStatus;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return astatus.iStatus;
    }

    //end amol 20-10-15 addition for partner portal
    //nilesh esigner 291015
    @Override
    public Srtracking[] getAllEsignerTrackingDetails(String sessionId, String userId, int status, int type, String startDate, String endDate) throws AxiomException {
        String strDebug = null;
        Channels channel = null;
        int result = -1;
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("ALertEvent::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                AxiomStatus aStatus = new AxiomStatus();
                aStatus.error = "Licence is invalid";
                aStatus.errorcode = -100;
                throw new AxiomException(aStatus.error);
            }
            AxiomStatus aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {

                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";
            int retValue = -1;
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {
                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
                        throw new AxiomException(aStatus.error);
                    }
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                        return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }
//                PasswordTrailManagement ptmngt = new PasswordTrailManagement();
//                result = ptmngt.ValidatePassword(channelid, strPassword, passwordSetting);
                SignReqTrackingManagement srtm = new SignReqTrackingManagement();
                Srtracking[] srtrack = null;
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                if (type == SignReqTrackingManagement.GET_ALL_TRACKING_BY_DATE) {
                    Date startDate1 = sdf.parse(startDate);
                    Date endDate1 = sdf.parse(endDate);
                    srtrack = srtm.getAllSRTrackingDetailsbyUserIdbyDate(channelid, userId, startDate1, endDate1);
                } else if (type == SignReqTrackingManagement.GET_ALL_TRACKING_BY_DATE_STATUS) {
                    Date startDate1 = sdf.parse(startDate);
                    srtrack = srtm.getCountSRTrackingbystatusbyDate1(channelid, userId, 1, startDate1);
                } else if (type == SignReqTrackingManagement.GET_ALL_TRACKING_BY_USERID_DOCID) {
                    srtrack = srtm.getAllSRTrackingDetailsbyUserIddocId1(channelid, userId, status);
                } else if (type == SignReqTrackingManagement.GET_ALL_TRACKING_BY_DOCID) {
                    srtrack = srtm.getAllSRTrackingDetailsbydocId(sessionId, channelid, status);
                } else {
                    srtrack = srtm.getCountSRTrackingbystatus1(channelid, userId, status);
                }

                return srtrack;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //end 291015
    @Override
    public AxiomStatus SignPdf(String sessionId, String filename, String useremailId, String emailId, String otp, String referenceId, String lattitude, String longitude) throws AxiomException {
//    public AxiomStatus SignPdf(String sessionId, String filename, String emailId, String otp, String referenceId, String trustPayLoad) throws AxiomException{

        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        String strDebug = null;
        Channels channel = null;
        AxiomStatus aStatus = null;
        int result = -1;
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("ALertEvent::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
//                aStatus = new AxiomStatus();
//                aStatus.error = "Licence is invalid";
//                aStatus.errorcode = -100;
//                throw new AxiomException(aStatus.error);
            }
            aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {

                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";
            int retValue = -1;
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {
                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
                        throw new AxiomException(aStatus.error);
                    }
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                            return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }

//            String user_name = _user_name.substring(_user_name.indexOf("src"), _user_name.length());
//            String finalstring = user_name.substring(user_name.indexOf(",") + 1, user_name.length() - 2);
                String imagepath = LoadSettings.g_sSettings.getProperty("remote.sign.archive") + "imagefile.png";
////            byte[] data = Base64.decode(finalstring);
//            try {
//                FileOutputStream fos = new FileOutputStream(imagepath);
////                fos.write(data);
//                fos.close();
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }

                String filepath = LoadSettings.g_sSettings.getProperty("remote.sign.archive") + filename;
                PdfReader reader;
                try {
                    reader = new PdfReader(filepath);
                } catch (IOException ex) {
                    Logger.getLogger(RSSCoreInterfaceImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
//            int pages = reader.getNumberOfPages();
                audit = new AuditManagement();
                if (emailId == null || otp == null) {
                    aStatus.error = "fill all details!!";
                    throw new AxiomException(aStatus.error);

                }

                int retval = -1;
                UserManagement uManagement = new UserManagement();
                String userFlag = LoadSettings.g_sSettings.getProperty("check.user");
                AuthUser aUser = null;
//            if (userFlag.equals("1")) {
//                aUser = uManagement.CheckUserByType(sessionId, channel.getChannelid(), _emailid, 4);
//            } else {
                aUser = uManagement.CheckUserByType(sessionId, channel.getChannelid(), useremailId, 3);
                //}
                if (aUser == null) {
                    aStatus.error = "fill all details!!";
                    throw new AxiomException(aStatus.error);
//                json.put("_result", result);
//                json.put("_message", message);
//                out.print(json);
//                out.flush();
                }

//            if (trustPayLoad != null) {
//                    MobileTrustSettings mSettings = null;
//
//                    Object obj = setManagement.getSetting(sessionid, channelid, SettingsManagement.MOBILETRUST_SETTING, 1);
//                    if (obj != null) {
//                        if (obj instanceof MobileTrustSettings) {
//                            mSettings = (MobileTrustSettings) obj;
//                        }
//                    }
//
//                    AxiomData aStatus = new AxiomData();
//
//                    if (mSettings == null) {
//                        aStatus.sErrorMessage = "Mobile Trust Setting is not configured!!!";
//                        aStatus.iErrorCode = -23;
////                  
//                        return aStatus;
//                    }
//
//                    if (mSettings.bGeoFencing == true) {
//
//                        GeoLocationManagement gManagement = new GeoLocationManagement();
//                        AXIOMStatus status = gManagement.validateGeoLocation(sessionid, channelid, userid, trustPayLoad);
//                        if (status == null) {
//                            aStatus.sErrorMessage = "Location Not Found!!!";
//                            aStatus.iErrorCode = -115;
//                        } else if (status.iStatus != 0) {
//
//                            aStatus.sErrorMessage = status.strStatus;
//                            aStatus.iErrorCode = status.iStatus;
//                            //     String errorLocation = "longitude=" + longi + ",lattitude=" + latti + ",country=" + srvLoc.country + ",city= " + srvLoc.city + ",state=" + srvLoc.state + ",zipcode=" + srvLoc.zipcode;
//                            audit.AddAuditTrail(sessionid, channel.getChannelid(), "", req.getRemoteAddr(), channel.getName(), session.getLoginid(),
//                                    session.getLoginid(), new Date(), "Change Token Status", aStatus.sErrorMessage, aStatus.iErrorCode,
//                                    "Token Management", "Invalid Location", null, itemtype,
//                                    session.getLoginid());
//
//                            return aStatus;
//                        }
//                    }
//                }
                OTPTokenManagement oManagement = new OTPTokenManagement(channel.getChannelid());
                Date dateOfverifyOtp = new Date();
                retValue = oManagement.VerifyOTP(channel.getChannelid(), aUser.userId, sessionId, otp);
//                retValue = 0;
                PDFSigningManagement pManagement = new PDFSigningManagement();
                String plain = aUser.userId + channel.getChannelid() + new Date();
                String archiveid = UtilityFunctions.Bas64SHA1(plain);
                AxiomPDFSignerWrapper apWrapper = new AxiomPDFSignerWrapper();
                String referenceid = null;
                if (referenceId == null || referenceId.isEmpty()) {
                    String refData = plain + archiveid + new Date();
                    referenceid = UtilityFunctions.Bas64SHA1(refData);

                } else {
                    referenceid = referenceId;
                }
                String pdfFileB64 = null;
                try {
                    pdfFileB64 = pManagement.fileToBase64(filepath);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                String strLocation = "";
                String strReason = "Testing";
                boolean sResult = false;
                int res = -1;
                aStatus.error = "pdf Signed Successful";
                String signature = "";
                RemoteSigningInfo rsInfo = new RemoteSigningInfo();
                //rsInfo.setDataToSign("To be signed data");
                rsInfo.dataToSign = pdfFileB64;
                rsInfo.reason = strReason;
                rsInfo.type = 2; // PDF
                rsInfo.name = aUser.userName;
                rsInfo.designation = aUser.designation;
                String _location = lattitude.replace(".", "") + ", " + longitude.replace(".", "");
                rsInfo.clientLocation = _location;

                if (retValue == 0) {
                    CertificateManagement certManagement = new CertificateManagement();
                    Certificates cert = certManagement.getCertificate(sessionId, channel.getChannelid(), aUser.userId);

                    if (cert == null) {

                        aStatus.error = "Certificate Not Issued!!";
                        aStatus.errorcode = -7;

                        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                                session.getLoginid(), session.getLoginid(), new Date(), "Verify OTP ", aStatus.error, aStatus.errorcode,
                                "Token Management", "OTP = ******", " OTP = ******",
                                "Verify OTP TOKEN", aUser.userId);
                        return aStatus;
                    }                                        
                    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                    String certbase64 = cert.getCertificate();
                    //CertificateFactory cf = CertificateFactory.getInstance("X.509");
                    //Certificate certificate = cf.generateCertificate(new ByteArrayInputStream(Base64.decode(certbase64)));
                    byte[] certBytes = Base64.decode(certbase64);
                    javax.security.cert.X509Certificate certifts = javax.security.cert.X509Certificate.getInstance(certBytes);
                    String strCertExDate = sdf1.format(cert.getExpirydatetime());
                    Date certExpiryDate = sdf1.parse(strCertExDate);
                    String strCurrentDate = sdf1.format(new Date());
                    Date currentDate = sdf1.parse(strCurrentDate);
                    if(certExpiryDate.before(currentDate)){
                        aStatus.error = "Document sign failed PDF due to certificate Expired!!";
                        aStatus.errorcode = -8;

                        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                                session.getLoginid(), session.getLoginid(), new Date(), "Verify OTP ", aStatus.error, aStatus.errorcode,
                                "Token Management", "OTP = ******", " OTP = ******",
                                "Verify OTP TOKEN", aUser.userId);
                        return aStatus;
                    }                    
                    String pfxFile = cert.getPfx();
                    String pfxPassword = cert.getPfxpassword();
                    CryptoManagement cyManagement = new CryptoManagement();

                    Date d = new Date();
                    String sourceFileName = aUser.userId + d.getTime();

                    String destFileName = LoadSettings.g_sSettings.getProperty("remote.sign.archive") + sourceFileName + "-sign.pdf";
                    try {
                        String uname = aUser.userName;
                        uname = uname.replace(".", "");
                        sResult = apWrapper.signpdf(uname, 1, 1, filepath, destFileName, pfxFile, pfxPassword, rsInfo, imagepath);
                        //sResult = pManagement.signPdf(uname, filepath, destFileName, pfxFile, pfxPassword, rsInfo, imagepath);
                    } catch (Exception ex) {
                        Logger.getLogger(RSSCoreInterfaceImpl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (sResult == true) {
                        signature = pManagement.fileToBase64(destFileName);
                        res = 0;
                    }
                    retval = pManagement.addRemoteSignature(sessionId, channel.getChannelid(), aUser.userId, archiveid, otp, retValue, dateOfverifyOtp, rsInfo.type, filepath, destFileName, res, new Date(), referenceid);
                    if (retval != 0) {

                        aStatus.error = "Add Remote Signature Failed!!";
                        aStatus.errorcode = -1;

//                    json.put("_result", result);
//                    json.put("_message", message);
//                    out.print(json);
//                    out.flush();
                        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                                session.getLoginid(), session.getLoginid(), new Date(), "Verify OTP ", aStatus.error, aStatus.errorcode,
                                "Token Management", "Add Remote Signature Failed", " Add Remote Signature Failed",
                                "Add Remote Signature", aUser.userId);
                        return aStatus;
                    }
                    SendNotification send = new SendNotification();
                    TemplateManagement tManagement = new TemplateManagement();
                    Templates templates = tManagement.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.EMAIL_PDF_SIGNING_TEMPLATE);

                    if (templates.getStatus() == tManagement.ACTIVE_STATUS) {
                        ByteArrayInputStream bais = new ByteArrayInputStream(templates.getTemplatebody());
                        String tmessage = (String) TemplateUtils.deserializeFromObject(bais);
                        String subject = templates.getSubject();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                        File file = new File(filepath);
                        req.getLocalAddr();

                        if (tmessage != null) {

                            tmessage = tmessage.replaceAll("#name#", aUser.getUserName());
                            tmessage = tmessage.replaceAll("#channel#", channel.getName());
                            tmessage = tmessage.replaceAll("#email#", aUser.getEmail());
                            tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                            tmessage = tmessage.replaceAll("#fileName#", filename);
                            tmessage = tmessage.replaceAll("#referenceid#", referenceid);
                        }

                        if (subject != null) {

                            subject = subject.replaceAll("#channel#", channel.getName());
                            subject = subject.replaceAll("#datetime#", sdf.format(d));
                        }
                        String fileNames[] = new String[1];
                        fileNames[0] = destFileName;
                        String mimeTypes[] = new String[1];
                        mimeTypes[0] = "application/pdf";

                        //logic added to send signed pdf to multiple users
                        String[] recipientEmail = emailId.split(",");

                        for (int i = 0; i < recipientEmail.length; i++) {
                            AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), recipientEmail[i], subject, tmessage,
                                    null, null,
                                    fileNames, mimeTypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                        }

//                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), emailId, subject, tmessage,
//                            null, null, fileNames, mimeTypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
//                    
                        aStatus.error = "Success";
                        aStatus.errorcode = 0;
                        aStatus.regCodeMessage = destFileName;

//                    json.put("_result", result);
//                    json.put("_message", message);
//                    json.put("_pdffilename", destFileName);
                    }
                } else {

                    aStatus.error = "Document Signature Failed Due to Verify OTP Failed!!";
                    aStatus.errorcode = -1;
                    retval = pManagement.addRemoteSignature(sessionId, channel.getChannelid(), aUser.userId, archiveid, otp, retValue, dateOfverifyOtp, rsInfo.type, filepath, null, 0, null, referenceid);

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(), "Verify OTP ", aStatus.error, aStatus.errorcode,
                            "Token Management", "Add Remote Signature Failed", " Add Remote Signature Failed",
                            "Add Remote Signature", aUser.userId);

//                json.put("_result", result);
//                json.put("_message", message);
//                out.print(json);
//                out.flush();
                    return aStatus;
                }
                if (res == 0) {
//                String resultString = "success";
                    aStatus.error = "Success";
                    aStatus.errorcode = 0;
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(), "Verify OTP And Sign Transaction ", aStatus.error, aStatus.errorcode,
                            "Verify Otp And Sign Transaction Management", "data = ******, otp = ******", " signdata = ******,otp = ******",
                            "Verify OTP TOKEN And Sign Transaction", aUser.userId);

//                out.print(json);
//                out.flush();
                    return aStatus;

                } else {
//                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
//                                                channel.getName(),
//                                                session.getLoginid(), session.getLoginid(),
//                                                new Date(),
//                                                "CHANGE STATUS", aStatus.error, aStatus.errorcode,
//                                                "Token Management",
//                                                "Current Status=" + oldValue,
//                                                "New Status=" + "Active",
//                                                "PKITOKEN", UserId);
                    aStatus.errorcode = -1;
                    aStatus.error = "Failed to sign PDF!!";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(), "Verify OTP And Sign Transaction ", aStatus.error, aStatus.errorcode,
                            "Verify Otp And Sign Transaction Management", "data = ******, otp = ******", " signdata = ******,otp = ******",
                            "Verify OTP TOKEN And Sign Transaction", aUser.userId);

//                json.put("_result", result);
//                json.put("_message", message);
//                out.print(json);
//                out.flush();
                    return aStatus;

                }

            }
        } catch (AxiomException ex) {
            Logger.getLogger(RSSCoreInterfaceImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RSSCoreInterfaceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }catch(Exception ex){
            Logger.getLogger(RSSCoreInterfaceImpl.class.getName()).log(Level.SEVERE, null, ex);
        } 
        finally {

        }
        return aStatus;
    }

    @Override
    public Remotesignature[] GetAllSignaturebyUserId(String sessionId, String userid, int type) {
        Remotesignature[] remoteSign = null;
        try {
            String strDebug = null;
            Channels channel = null;
            AxiomStatus aStatus = null;
            int result = -1;
            String sep = System.getProperty("file.separator");

            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("ALertEvent::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                aStatus = new AxiomStatus();
                aStatus.error = "Licence is invalid";
                aStatus.errorcode = -100;
                throw new AxiomException(aStatus.error);
            }
            aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {

                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";
            int retValue = -1;
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {
                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
                        throw new AxiomException(aStatus.error);
                    }
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                            return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.remotesign);
                sManagement = new SessionManagement();
                session = sManagement.getSessionById(sessionId);
                String channelId = session.getChannelid();
                Session sTemplate = suTemplate.openSession();
                RemoteSignatureUtils remoteSignUtil = new RemoteSignatureUtils(suTemplate, sTemplate);
                remoteSign = remoteSignUtil.getRemoteSignObj(channelId, userid, type);
                if (remoteSign != null) {
                    UserManagement uManagement = new UserManagement();
                    AuthUser oldUser = uManagement.getUser(sessionId, session.getChannelid(), remoteSign[0].getUserid());
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(), "Create User",
                            aStatus.error, aStatus.errorcode,
                            "Remote Server Signing", "GetSign Details",
                            "Name=" + oldUser.getUserId() + ",Phone=" + oldUser.getPhoneNo() + ",Email=" + oldUser.getEmail() + ",State=active",
                            "Result", aStatus.error);
                    return remoteSign;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;

    }

    @Override
    public AxiomStatus UpdateRemoteSignaturesByRequest(String sessionId, String deviceId, String refid, String deviceDetails) {
        int updateRemoteSign = 0;
        Channels channel = null;
        AxiomStatus aStatus = null;
        String strDebug = "";
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("ALertEvent::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                aStatus = new AxiomStatus();
                aStatus.error = "Licence is invalid";
                aStatus.errorcode = -100;
                throw new AxiomException(aStatus.error);
            }
            aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {

                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";
            int retValue = -1;
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {
                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
                        throw new AxiomException(aStatus.error);
                    }
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                            return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }
                Remotesignature remoteSignOld = null;
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.remotesign);
                sManagement = new SessionManagement();
                session = sManagement.getSessionById(sessionId);
                String channelId = session.getChannelid();
                Session sTemplate = suTemplate.openSession();
                RemoteSignatureUtils remoteSignUtil = new RemoteSignatureUtils(suTemplate, sTemplate);
                Remotesignature[] res = remoteSignUtil.getRemoteSignObj(channelId, refid, 2);
                HashMap deviceDetailsMap = null;

                if (res.length != 0) {
                    remoteSignOld = res[0];
                    if (remoteSignOld.getAllowedUsers() != null) {
                        deviceDetailsMap = new HashMap();
                        deviceDetailsMap = (HashMap) SettingsUtil.deserializeFromObject(new ByteArrayInputStream(remoteSignOld.getAllowedUsers()));
                    }
                    if (deviceDetailsMap != null) {
                        deviceDetailsMap.put(deviceId, deviceDetails);
                    } else {
                        deviceDetailsMap = new HashMap();
                        deviceDetailsMap.put(deviceId, deviceDetails);
                    }
                } else {
                    aStatus.errorcode = -1;
                    aStatus.error = "No Signing Details to update for this User!!!";
                    return aStatus;
                }

                byte[] serializedMap = SettingsUtil.serializeToObject(deviceDetailsMap);

                remoteSignOld.setAllowedUsers(serializedMap);

                updateRemoteSign = remoteSignUtil.changeRemotesignatureWithQR(refid, remoteSignOld);
                aStatus.errorcode = updateRemoteSign;
                if (updateRemoteSign == 0) {
                    aStatus.error = "SignatureDetails Successfully Updated!!!";
                } else {
                    aStatus.error = "Excep occured while updating!!!";
                }
                UserManagement uManagement = new UserManagement();
                AuthUser oldUser = uManagement.getUser(sessionId, session.getChannelid(), remoteSignOld.getUserid());
                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
                        req.getRemoteAddr(), channel.getName(),
                        session.getLoginid(), session.getLoginid(),
                        new Date(), "Create User",
                        aStatus.error, aStatus.errorcode,
                        "Remote Server Signing", "Update SignDetails",
                        "Name=" + remoteSignOld.getUserid() + ",Phone=" + oldUser.getPhoneNo() + ",Email=" + oldUser.getEmail() + ",State=active",
                        "Result", aStatus.error);

                return aStatus;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private AxiomStatus CheckIPAndSendAlert(Channels channel, String cip) {

        AxiomStatus aStatus = new AxiomStatus();
        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();

        SettingsManagement setManagement = new SettingsManagement();

        Object ipobj = setManagement.getSettingInner(channel.getChannelid(), SettingsManagement.GlobalSettings, 1);
        if (ipobj != null) {
            GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
            int checkIp = 1;
            if (cip.compareTo("127.0.0.1") != 0) {
                checkIp = setManagement.checkIP(channel.getChannelid(), cip);
            } else {
                checkIp = 1;
            }
            String channelid = GetChannelID();
            if (iObj.ipstatus == 0 && checkIp != 1) {
                if (iObj.ipalertstatus == 0) {
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                    Session sTemplate = suTemplate.openSession();
                    TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                    Templates templatesObj = tUtil.loadbyName(channel.getChannelid(), TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                    OperatorsManagement oManagement = new OperatorsManagement();
                    Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                    if (aOperator != null) {
                        String[] emailList = new String[aOperator.length - 1];
                        for (int i = 1; i < aOperator.length; i++) {
                            emailList[i - 1] = aOperator[i].getEmailid();
                        }
                        if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                            ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                            String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                            String strsubject = templatesObj.getSubject();
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            if (strmessageBody != null) {
                                // Date date = new Date();
                                strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                strmessageBody = strmessageBody.replaceAll("#filterword#", cip);
                            }

                            SendNotification send = new SendNotification();
                            AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                    emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                        }

                        suTemplate.close();
                        sTemplate.close();
//                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;

                        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(channelid, aStatus.errorcode);
                        aStatus.error = errmsg.getUsermessage();
                        return aStatus;
                    }

                    suTemplate.close();
                    sTemplate.close();
//                    aStatus.error = "INVALID IP REQUEST";
                    aStatus.errorcode = -8;
                    Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(channelid, aStatus.errorcode);
                    aStatus.error = errmsg.getUsermessage();
                    return aStatus;
                }
//                aStatus.error = "INVALID IP REQUEST";
                aStatus.errorcode = -8;
                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(channelid, aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }
        }
        return aStatus;
    }

    @Override
    public AxiomStatus SignPdfWithQR(String sessionId, String filename, String emailId, String otp, String referenceId, String latitude, String longitude, String qrcodedataUrl, String allowedPhonesAndCountry) throws AxiomException {

        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        String strDebug = null;
        Channels channel = null;
        AxiomStatus aStatus = null;
        int result = -1;
        String sep = System.getProperty("file.separator");
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    System.out.println("ALertEvent::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
                aStatus = new AxiomStatus();
                aStatus.error = "Licence is invalid";
                aStatus.errorcode = -100;
                throw new AxiomException(aStatus.error);
            }
            aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {

                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";
            int retValue = -1;
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {
                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
                        throw new AxiomException(aStatus.error);
                    }
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }

                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }

                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                            return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }

//            String user_name = _user_name.substring(_user_name.indexOf("src"), _user_name.length());
//            String finalstring = user_name.substring(user_name.indexOf(",") + 1, user_name.length() - 2);
                String imagepath = LoadSettings.g_sSettings.getProperty("remote.sign.archive") + sep + "imagefile.png";
////            byte[] data = Base64.decode(finalstring);
//            try {
//                FileOutputStream fos = new FileOutputStream(imagepath);
////                fos.write(data);
//                fos.close();
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }

                String filepath = LoadSettings.g_sSettings.getProperty("remote.sign.archive") + sep + filename;
                PdfReader reader;
                try {
                    reader = new PdfReader(filepath);
                } catch (IOException ex) {
                    Logger.getLogger(RSSCoreInterfaceImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
//            int pages = reader.getNumberOfPages();
                audit = new AuditManagement();
                if (emailId == null || otp == null) {
                    aStatus.error = "fill all details!!";
                    throw new AxiomException(aStatus.error);

                }

                int retval = -1;
                UserManagement uManagement = new UserManagement();
                String userFlag = LoadSettings.g_sSettings.getProperty("check.user");
                AuthUser aUser = null;
//            if (userFlag.equals("1")) {
//                aUser = uManagement.CheckUserByType(sessionId, channel.getChannelid(), _emailid, 4);
//            } else {
                aUser = uManagement.CheckUserByType(sessionId, channel.getChannelid(), emailId, 3);
                //}
                if (aUser == null) {
                    aStatus.error = "fill all details!!";
                    throw new AxiomException(aStatus.error);
//                json.put("_result", result);
//                json.put("_message", message);
//                out.print(json);
//                out.flush();
                }
                OTPTokenManagement oManagement = new OTPTokenManagement(channel.getChannelid());
                Date dateOfverifyOtp = new Date();
                retValue = oManagement.VerifyOTP(channel.getChannelid(), aUser.userId, sessionId, otp);

                PDFSigningManagement pManagement = new PDFSigningManagement();
                String plain = aUser.userId + channel.getChannelid() + new Date();
                String archiveid = UtilityFunctions.Bas64SHA1(plain);

                String referenceid = null;
                if (referenceId == null || referenceId.isEmpty()) {
                    String refData = plain + archiveid + new Date();
                    referenceid = UtilityFunctions.Bas64SHA1(refData);

                } else {
                    referenceid = referenceId;
                }
                String pdfFileB64 = null;
                try {
                    pdfFileB64 = pManagement.fileToBase64(filepath);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                String strLocation = "";
                String strReason = "Testing";
                boolean sResult = false;
                int res = -1;
                aStatus.error = "pdf Signed Successful";
                String signature = "";
                RemoteSigningInfo rsInfo = new RemoteSigningInfo();
                //rsInfo.setDataToSign("To be signed data");
                rsInfo.dataToSign = pdfFileB64;
                rsInfo.reason = strReason;
                rsInfo.type = 2; // PDF
                rsInfo.name = aUser.userName;
                rsInfo.designation = aUser.designation;
                rsInfo.clientLocation = latitude + ", " + longitude;
                rsInfo.qrCodeData = qrcodedataUrl + "?refid=" + URLEncoder.encode(referenceid, "UTF-8");
                if (retValue == 0) {

                    CertificateManagement certManagement = new CertificateManagement();
                    Certificates cert = certManagement.getCertificate(sessionId, channel.getChannelid(), aUser.userId);

                    if (cert == null) {

                        aStatus.error = "Certificate Not Issued!!";
                        aStatus.errorcode = -7;

                        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                                session.getLoginid(), session.getLoginid(), new Date(), "Verify OTP ", aStatus.error, aStatus.errorcode,
                                "Token Management", "OTP = ******", " OTP = ******",
                                "Verify OTP TOKEN", aUser.userId);
                        return aStatus;
                    }
                    String pfxFile = cert.getPfx();
                    String pfxPassword = cert.getPfxpassword();
                    CryptoManagement cyManagement = new CryptoManagement();

                    Date d = new Date();
                    String sourceFileName = aUser.userId + d.getTime();

                    String destFileName = LoadSettings.g_sSettings.getProperty("remote.sign.archive") + System.getProperty("file.separator") + sourceFileName + "-sign.pdf";
                    try {
                        sResult = pManagement.signPdfWithQR(aUser.userId, filepath, destFileName, pfxFile, pfxPassword, rsInfo, imagepath);
                    } catch (Exception ex) {
                        Logger.getLogger(RSSCoreInterfaceImpl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (sResult == true) {
                        signature = pManagement.fileToBase64(destFileName);
                        res = 0;
                    }
                    retval = pManagement.addRemoteSignatureWithQR(sessionId, channel.getChannelid(), aUser.userId,
                            archiveid, otp, retValue, dateOfverifyOtp, rsInfo.type, filepath, destFileName,
                            res, new Date(), referenceid,
                            null, allowedPhonesAndCountry);
                    if (retval != 0) {

                        aStatus.error = "Add Remote Signature Failed!!";
                        aStatus.errorcode = -1;

//                    json.put("_result", result);
//                    json.put("_message", message);
//                    out.print(json);
//                    out.flush();
                        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                                session.getLoginid(), session.getLoginid(), new Date(), "Verify OTP ", aStatus.error, aStatus.errorcode,
                                "Token Management", "Add Remote Signature Failed", " Add Remote Signature Failed",
                                "Add Remote Signature", aUser.userId);
                        return aStatus;
                    }
                    SendNotification send = new SendNotification();
                    TemplateManagement tManagement = new TemplateManagement();
                    Templates templates = tManagement.LoadbyName(sessionId, channel.getChannelid(), TemplateNames.EMAIL_PDF_SIGNING_TEMPLATE);

                    if (templates.getStatus() == tManagement.ACTIVE_STATUS) {
                        ByteArrayInputStream bais = new ByteArrayInputStream(templates.getTemplatebody());
                        String tmessage = (String) TemplateUtils.deserializeFromObject(bais);
                        String subject = templates.getSubject();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                        File file = new File(filepath);
                        req.getLocalAddr();

                        if (tmessage != null) {

                            tmessage = tmessage.replaceAll("#name#", aUser.getUserName());
                            tmessage = tmessage.replaceAll("#channel#", channel.getName());
                            tmessage = tmessage.replaceAll("#email#", aUser.getEmail());
                            tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                            tmessage = tmessage.replaceAll("#fileName#", filename);
                            tmessage = tmessage.replaceAll("#referenceid#", referenceid);
                        }

                        if (subject != null) {

                            subject = subject.replaceAll("#channel#", channel.getName());
                            subject = subject.replaceAll("#datetime#", sdf.format(d));
                        }
                        String fileNames[] = new String[1];
                        fileNames[0] = destFileName;
                        String mimeTypes[] = new String[1];
                        mimeTypes[0] = "application/pdf";
                        AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), emailId, subject, tmessage,
                                null, null, fileNames, mimeTypes, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));

                        aStatus.error = "Success";
                        aStatus.errorcode = 0;
                        aStatus.regCodeMessage = "Please Check your Email for Details";

//                    json.put("_result", result);
//                    json.put("_message", message);
//                    json.put("_pdffilename", destFileName);
                    }
                } else {

                    aStatus.error = "Add Remote Signature Failed Due to Verify OTP Failed!!";
                    aStatus.errorcode = -1;
                    retval = pManagement.addRemoteSignature(sessionId, channel.getChannelid(), aUser.userId, archiveid,
                            otp, retValue, dateOfverifyOtp, rsInfo.type, filepath, null, 0, null,
                            referenceid);

                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(), "Verify OTP ", aStatus.error, aStatus.errorcode,
                            "Token Management", "Add Remote Signature Failed", " Add Remote Signature Failed",
                            "Add Remote Signature", aUser.userId);

//                json.put("_result", result);
//                json.put("_message", message);
//                out.print(json);
//                out.flush();
                    return aStatus;
                }
                if (res == 0) {
//                String resultString = "success";
                    aStatus.error = "Success";
                    aStatus.errorcode = 0;
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(), "Verify OTP And Sign Transaction ", aStatus.error, aStatus.errorcode,
                            "Verify Otp And Sign Transaction Management", "data = ******, otp = ******", " signdata = ******,otp = ******",
                            "Verify OTP TOKEN And Sign Transaction", aUser.userId);

//                out.print(json);
//                out.flush();
                    return aStatus;

                } else {
//                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
//                                                channel.getName(),
//                                                session.getLoginid(), session.getLoginid(),
//                                                new Date(),
//                                                "CHANGE STATUS", aStatus.error, aStatus.errorcode,
//                                                "Token Management",
//                                                "Current Status=" + oldValue,
//                                                "New Status=" + "Active",
//                                                "PKITOKEN", UserId);
                    aStatus.errorcode = -1;
                    aStatus.error = "Failed to sign PDF!!";
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(), "Verify OTP And Sign Transaction ", aStatus.error, aStatus.errorcode,
                            "Verify Otp And Sign Transaction Management", "data = ******, otp = ******", " signdata = ******,otp = ******",
                            "Verify OTP TOKEN And Sign Transaction", aUser.userId);

//                json.put("_result", result);
//                json.put("_message", message);
//                out.print(json);
//                out.flush();
                    return aStatus;

                }

            }
        } catch (AxiomException ex) {
            Logger.getLogger(RSSCoreInterfaceImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RSSCoreInterfaceImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
        return aStatus;
    }

    //added by parimal and virkam for Demo portal - 3/2/2016
    //static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(RSSCoreInterfaceImpl.class.getName());
    private static String g_channelID = null;

    private String GetChannelID() {

        if (g_channelID == null) {

            SessionFactoryUtil suChannel = null;// new SessionFactoryUtil(SessionFactoryUtil.channels);
            Session sChannel = null; //suChannel.openSession();

            try {
                suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
                sChannel = suChannel.openSession();

                MessageContext mc = wsContext.getMessageContext();
                ServletContext sc = (ServletContext) mc.get(MessageContext.SERVLET_CONTEXT);
                String _channelName = sc.getContextPath();
                _channelName = _channelName.replaceAll("/", "");

                if (_channelName.compareTo("core") == 0) {
                    _channelName = "face";
                }

                ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
                Channels channel = cUtil.getChannel(_channelName);
                g_channelID = channel.getChannelid();
                sChannel.close();
                suChannel.close();
            } catch (Exception e) {
                sChannel.close();
                suChannel.close();
                e.printStackTrace();
            }
        }

        return g_channelID;
    }

    private AxiomStatus VerifyPlusFactor(String sessionid, Channels channel, String userid, String sotp, String sotpPlus) {

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.MOBILETRUST) != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//            aStatus.error = "Mobile Trust feature is not available in this license!!!";
            aStatus.errorcode = -106;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();

            return aStatus;
        }

        if (AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_OOB) != 0
                && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_HARDWARE) != 0
                && AxiomProtect.CheckEnforcementFor(AxiomProtect.OTP_SOFTWARE) != 0) {
            AxiomStatus aStatus = new AxiomStatus();
            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//            aStatus.error = "OTP Token feature is not available in this license!!!";
            aStatus.errorcode = -104;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            return aStatus;
        }

        int retValue = -10;
        AxiomStatus aStatus = new AxiomStatus();
        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//        aStatus.error = "Invalid Parameters!!!";
        aStatus.errorcode = -66;
        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
        aStatus.error = errmsg.getUsermessage();

        MobileTrustManagment mManagment = new MobileTrustManagment();
        int check = -9;
        JSONObject jsonObj = null;
        int devicetype = 0;
        try {
            jsonObj = new JSONObject(sotpPlus);
            String device = jsonObj.getString("_deviceType");
            if (device.equals("ANDROID")) {
                devicetype = MobileTrustManagment.ANDROID;
            } else if (device.equals("IOS")) {
                devicetype = MobileTrustManagment.IOS;
            } else {
//                aStatus.error = "Invalid Device Type (Android and iOS) is only supported";
                aStatus.errorcode = -95;
                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                return aStatus;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
//            aStatus.error = "Mobile Trust Plus Payload is invalid!!!";
            aStatus.errorcode = -96;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            return aStatus;
        }

        retValue = mManagment.CheckPlusComponent(channel, userid, sessionid, sotp, sotpPlus, devicetype);
        if (retValue == -6) {
//            aStatus.error = "Trusted device not found";
            aStatus.errorcode = -92;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
        } else if (retValue == -4) {
//            aStatus.error = "OTP is invalid";
            aStatus.errorcode = -97;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
        } else if (retValue == -5) {
//            aStatus.error = "Trusted Device mismatch";

            aStatus.errorcode = -94;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
        } else if (retValue == -7) {
//            aStatus.error = "Invalid Location, out of home country!!!";
            aStatus.errorcode = -98;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
        } else if (retValue == -8) {
//            aStatus.error = "Mobile Trust Setting is not configured!!!";
            aStatus.errorcode = -87;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
        } else if (retValue == -9) {
//            aStatus.error = "Timestamp is null";
            aStatus.errorcode = -141;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
        } else if (retValue == -10) {
//            aStatus.error = "TimeStamp mistmatch!!!";
            aStatus.errorcode = -142;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
        } else if (retValue == -11) {
//            aStatus.error = "TimeStamp has expired!!!";
            aStatus.errorcode = -143;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
        } else if (retValue == -12) {
//            aStatus.error = "Invalid Location, out of home country!!!";
            aStatus.errorcode = -98;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
        } else if (retValue == -13) {
//            aStatus.error = "Invalid Location, roaming country is not allowed!!!";
            aStatus.errorcode = -99;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
        } else if (retValue == -14) {
//            aStatus.error = "Roaming duration is over";
            aStatus.errorcode = -10;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
        } else if (retValue == -15) {
            // //aStatus.error = "TimeStamp Updated failed";
//            aStatus.error = "SUCCESS";
            aStatus.errorcode = 0;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
        } else if (retValue == -2) {
//            aStatus.error = "Session has expired";
            aStatus.errorcode = -115;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
        } else if (retValue == -1) {
//            aStatus.error = "General Exception";
            aStatus.errorcode = -999;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
        } else if (retValue == 0) {
//            aStatus.error = "SUCCESS";
            aStatus.errorcode = 0;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
        }
        return aStatus;
    }

    @Override
    public AxiomData VerifyCredentialsAndSignTransactionRSS(String sessionid, String userid, String SOTP, String challenge, String[] Data, int type, String plusFactor, String clientip) {

        log.info("Entered VerifyCredentialsAndSignTransaction()");

        String strDebug = null;
        String strException = null;
        String ratio = null;

        //try {
        strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");

        if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
            log.info("VerifyCredentialsAndSignTransaction::sessionid::" + sessionid);
            log.info("VerifyCredentialsAndSignTransaction::userid::" + userid);
            log.info("VerifyCredentialsAndSignTransaction::SOTP::" + SOTP);
            log.info("VerifyCredentialsAndSignTransaction::Data::" + Data);
            log.info("VerifyCredentialsAndSignTransaction::Data.length::" + Data.length);
            log.info("VerifyCredentialsAndSignTransaction::challenge::" + challenge);
            log.info("VerifyCredentialsAndSignTransaction::typetoVerify::" + type);
            log.info("VerifyCredentialsAndSignTransaction::plusFactor::" + plusFactor);
            log.info("VerifyCredentialsAndSignTransaction::clientip::" + clientip);
        }

        int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//            AxiomData aData = new AxiomData();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
//            aData.iErrorCode = -100;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
//            aData.sErrorMessage = errmsg.getUsermessage();
//
//            return aData;
//        }

        AxiomData aData = new AxiomData();
        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();

        byte[] SHA1hash = null;

        SessionManagement sManagement = new SessionManagement();
        UserManagement uManagement = new UserManagement();

        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);

        if (session == null) {
//            aData.sErrorMessage = "Sessio is  null!! ";
            aData.iErrorCode = -113;

            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
            aData.sErrorMessage = errmsg.getUsermessage();
            return aData;
        }

        int retValue = -1;
//            aData.sErrorMessage = "ERROR";
//            aData.iErrorCode = retValue;
//            if (session == null) {
//                return aData;
//            }
        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(session.getChannelid());
        if (channel == null) {
//            aData.sErrorMessage = "Channel is not configured or null!! ";
            aData.iErrorCode = -102;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
            aData.sErrorMessage = errmsg.getUsermessage();

            return aData;
        }

        SettingsManagement setManagement = new SettingsManagement();
        String channelid = session.getChannelid();
        ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
        if (channelProfileObj == null) {
//            aData.sErrorMessage = "Channel Profile is not properly configured !!!";
            aData.iErrorCode = -80;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
            aData.sErrorMessage = errmsg.getUsermessage();

            return aData;
        }

        int retVal = sManagement.getServerStatus(channelProfileObj);
        if (retVal != 0) {
//            aData.sErrorMessage = "Acces is not available as per channel profile restriction!!!";

            aData.iErrorCode = -84;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
            aData.sErrorMessage = errmsg.getUsermessage();
            return aData;
        }

        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());

        AuthUser aUser = uManagement.getUser(sessionid, channel.getChannelid(), userid);
        if (aUser == null) {
//            aData.sErrorMessage = "INVALID USER";
            aData.iErrorCode = -12;

            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
            aData.sErrorMessage = errmsg.getUsermessage();
            return aData;
        }

        Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
        if (ipobj == null) {
//            aData.sErrorMessage = "Channel Global Settings is not properly configured !!!";
            aData.iErrorCode = -83;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
            aData.sErrorMessage = errmsg.getUsermessage();
            return aData;
        }

        AxiomStatus as = CheckIPAndSendAlert(channel, req.getRemoteAddr());
        if (as.errorcode != 0) {
            AxiomData aStatus = new AxiomData();
//            aStatus.sErrorMessage = as.error;
            aStatus.iErrorCode = as.errorcode;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
            aData.sErrorMessage = errmsg.getUsermessage();
            return aStatus;
        }

        //we need to check the Data with the chanlen is the data is same or not for challenge
        String allData = "";
        if (Data != null && Data.length != 0) {

            for (int i = 0; i < Data.length; i++) {
                allData += Data[i];
            }

            if (challenge != null && challenge.isEmpty() == false) {
                RSSUtils rs = new RSSUtils();
                long lChallenge = rs.CRC32CheckSum(allData.getBytes());
                String calculatedChallenge = "" + lChallenge;

                //System.out.println(calculatedChallenge);
                //System.out.println(challenge);
                if (calculatedChallenge.compareTo(challenge) != 0) {
                    //challenge is not from this data so error 
                    AxiomData aStatus = new AxiomData();
//                aStatus.sErrorMessage = "Challenge does not represent the data provided!!!";
                    aStatus.iErrorCode = -18;
                    Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.iErrorCode);
                    aStatus.sErrorMessage = errmsg.getUsermessage();
                    return aStatus;
                }

            }

        } else {
//            AxiomData aStatus = new AxiomData();
//            aStatus.sErrorMessage = "Data is empty. It cannot be empty. Please recheck.";
//            aStatus.iErrorCode = -19;
//            return aStatus;
            allData = challenge;
        }

        AxiomData aStatus = new AxiomData();
        String cip = req.getRemoteAddr();
        if (clientip == null || clientip.isEmpty() == true) {
        } else {
            cip = clientip;
        }

        if (plusFactor != null && plusFactor.isEmpty() == false) {
            AxiomStatus aStatus1 = VerifyPlusFactor(sessionid, channel, userid, SOTP, plusFactor);
            if (aStatus1.errorcode < 0) {
//                aStatus.sErrorMessage = aStatus1.error;
                aStatus.iErrorCode = aStatus1.errorcode;
                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.iErrorCode);
                aStatus.sErrorMessage = errmsg.getUsermessage();
                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), cip, channel.getName(),
                        session.getLoginid(), session.getLoginid(),
                        new Date(),
                        "Verify Plus Component",
                        "ERROR",
                        aData.iErrorCode,
                        "Mobile Trust Management",
                        "",
                        errmsg.getErrorMessage(),
                        "MOBILETRUST",
                        userid);

                log.info("VerifyPlusFactor failed = " + aStatus1.errorcode);
                log.info("Exiting VerifyCredentialsAndSignTransaction");
                return aStatus;
            } else {
                aStatus.iErrorCode = aStatus1.errorcode;
                Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.iErrorCode);
                aStatus.sErrorMessage = errmsg.getUsermessage();
                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), cip, channel.getName(),
                        session.getLoginid(), session.getLoginid(),
                        new Date(),
                        "Verify Plus Component",
                        "SUCCESS",
                        aData.iErrorCode,
                        "Mobile Trust Management",
                        "",
                        "MobileTrust Plus (Geo, Decice, Timestamp) are veirifed",
                        "MOBILETRUST",
                        userid);

                log.info("VerifyPlusFactor result = " + aStatus1.errorcode);
                //log.info("Exiting VerifyCredentialsAndSignTransaction");
            }
        }

        if (challenge == null || challenge.isEmpty() == true) { //OTP
            if (type != 0) // check specific type of OTP token
            {
                retValue = oManagement.VerifyOTPByType(channelid, userid, sessionid, SOTP, type);
            } else //check all OTP tokens.
            {
                retValue = oManagement.VerifyOTP(channelid, userid, sessionid, SOTP);
            }
        } else { //SOTP
            String[] challengeData = new String[1];
            challengeData[0] = challenge;

            if (type != 0) // check specific type of OTP token
            {
                retValue = oManagement.VerifySignatureOTPByType(session.getChannelid(), userid, sessionid, challengeData, SOTP, type);
            } else //check all OTP tokens.
            {
                retValue = oManagement.VerifySignatureOTP(session.getChannelid(), userid, sessionid, challengeData, SOTP);
            }
        }

//        if (retValue == 0) {
//                aStatus.error = "SUCCESS";
//                aStatus.errorcode = 0;
//            } else if (retValue == -22) {
//                aStatus.error = "Signature OTP Token is locked";
//                aStatus.errorcode = -22;
//            } else if (retValue == -8) {
//                aStatus.error = "Signature OTP is already consumed";
//                aStatus.errorcode = -8;
//            } else if (retValue == -17) {
//                aStatus.error = "User/Token is not found";
//                aStatus.errorcode = -17;
//            } else if (retValue == -9) {
//                aStatus.error = "Signature OTP is expired";
//                aStatus.errorcode = -9;
//            } else if (retValue == -2) {
//                aStatus.error = "Session has expired";
//                aStatus.errorcode = -2;
//            } else if (retValue == -1) {
//                aStatus.error = "General Error";
//                aStatus.errorcode = -1;
//            }
        if (retValue == 0) {
//            aData.sErrorMessage = "SUCCESS";
            aData.iErrorCode = 0;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
            aData.sErrorMessage = errmsg.getUsermessage();
        } else if (retValue == -9) {
//            aData.sErrorMessage = "One Time Password/Signature One Time Password was expired";
            aData.iErrorCode = -9;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
            aData.sErrorMessage = errmsg.getUsermessage();
        } else if (retValue == -2) {
//            aData.sErrorMessage = "Internal Error";
            aData.iErrorCode = -2;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
            aData.sErrorMessage = errmsg.getUsermessage();
        } else if (retValue == -14) {
            aData.sErrorMessage = "Invalid Session";
            aData.iErrorCode = -114;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
            aData.sErrorMessage = errmsg.getUsermessage();
        } else if (retValue == -8) {
//            aData.sErrorMessage = "One Time Password/Signature One Time Password was expired";
            aData.iErrorCode = -9;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
            aData.sErrorMessage = errmsg.getUsermessage();
        } else if (retValue == -17) {
//            aData.sErrorMessage = "User/Token is not found!!!";

            aData.iErrorCode = -17;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
            aData.sErrorMessage = errmsg.getUsermessage();
        } else if (retValue == -22) {
//            aData.sErrorMessage = "Token is locked!!!";
            aData.iErrorCode = -22;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
            aData.sErrorMessage = errmsg.getUsermessage();
        } else if (retValue == -1) {
            aData.iErrorCode = -97;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
            aData.sErrorMessage = errmsg.getUsermessage();
        }

        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
        String strERRORMesg = "ERROR";
        if (aData.iErrorCode >= 0) {
            strERRORMesg = "SUCCESS";
        }

        if (type == OTP_TOKEN) {
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), cip,
                    channel.getName(),
                    session.getLoginid(), session.getLoginid(),
                    new Date(),
                    "VERIFY OTP/SIGNATURE OTP", strERRORMesg, aData.iErrorCode,
                    "Token Management",
                    "",
                    "OTP/Siganture OTP=" + SOTP + " for challenge =" + challenge + " failed with =" + errmsg.getErrorMessage(),
                    "OTPTOKENS", userid);
        }

        if (retValue != 0) {
            log.info("VerifySignatureOTP/VerifyOTP failed = " + retValue);
            log.info("Exiting VerifyCredentialsAndSignTransaction");
            return aData;
        }

        RemoteSigningInfo rsInfo = new RemoteSigningInfo();
        int result = -1;

        CertificateManagement certManagement = new CertificateManagement();
        Certificates cert = certManagement.getCertificate(sessionid, session.getChannelid(), userid);

        if (cert == null) {
//            aData.sErrorMessage = "Certificate is not issued to the user!!";
            aData.iErrorCode = -7;

            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
            aData.sErrorMessage = errmsg.getUsermessage();
            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), cip, channel.getName(),
                    session.getLoginid(), session.getLoginid(),
                    new Date(),
                    "Sign Transaction",
                    "ERROR",
                    aData.iErrorCode,
                    "Certificate Management",
                    "",
                    errmsg.getErrorMessage(),
                    "PKITOKEN", userid);

            log.info("getCertificate failed = -7");
            log.info("Exiting VerifyCredentialsAndSignTransaction");

            return aData;
        }

        String pfxFile = cert.getPfx();
        String pfxPassword = cert.getPfxpassword();

        if (pfxFile == null || pfxPassword == null) {
//            aData.sErrorMessage = "Certificate is not present for this user!!";
            aData.iErrorCode = -6;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
            aData.sErrorMessage = errmsg.getUsermessage();

            audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(), cip, channel.getName(),
                    session.getLoginid(), session.getLoginid(),
                    new Date(),
                    "Sign Transaction",
                    "ERROR",
                    aData.iErrorCode,
                    "Certificate Management",
                    "",
                    errmsg.getErrorMessage(),
                    "PKITOKEN", userid);

            log.info("getPfx failed = -6");
            log.info("Exiting VerifyCredentialsAndSignTransaction");

            return aData;

        }

        CryptoManagement cyManagement = new CryptoManagement();
        //boolean sResult = false;
        String signature = null;

        //this is raw signing...
        rsInfo.type = RemoteSigningInfo.RAW_DATA;
        rsInfo.dataToSign = allData;
        rsInfo.clientLocation = null;
        rsInfo.reason = null;
        rsInfo.name = null;
        rsInfo.designation = null;
        rsInfo.qrCodeData = null;
        rsInfo.bNotifyUser = false;
        rsInfo.bReturnSignedDocument = false;
        rsInfo.bAddTimestamp = false;
        rsInfo.referenceId = null;
        String referenceid = null;
        String plain = userid + session.getChannelid() + new Date();
        String archiveid = UtilityFunctions.Bas64SHA1(plain);

        String refData = plain + archiveid + new Date();
        referenceid = UtilityFunctions.Bas64SHA1(refData);

        if (rsInfo.type == RemoteSigningInfo.RAW_DATA) {
            String strSignApproach = LoadSettings.g_sSettings.getProperty("pkcs.signature.version");
            if (strSignApproach != null && strSignApproach.isEmpty() == false) {
                if (strSignApproach.compareToIgnoreCase("pkcs7") == 0) {
                    //PKCS7 enveloped sgnature with data and certificate and signature
                    signature = cyManagement.SignDataPKCS7(rsInfo.dataToSign, pfxFile, pfxPassword);
                } else {

                    signature = cyManagement.SignData(rsInfo.dataToSign, pfxFile, pfxPassword); // signData();
                }
            } else {
                //default do PKCS 1
                signature = cyManagement.SignData(rsInfo.dataToSign, pfxFile, pfxPassword); // signData();
            }

            if (signature != null) {
                result = 0;
                aData.archiveid = archiveid;
                aData.signature = signature;
                aData.referenceid = referenceid;
//                aData.sErrorMessage = "SUCCESS";
                aData.iErrorCode = 0;
                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
                aData.sErrorMessage = errmsg.getUsermessage();

                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                        cip, channel.getName(),
                        session.getLoginid(), session.getLoginid(),
                        new Date(),
                        "Sign Transaction",
                        "SUCCESS",
                        aData.iErrorCode,
                        "Certificate Management",
                        "",
                        "Challenge=" + challenge + ",Archiveid=" + aData.archiveid + ", Referenceid="
                        + aData.referenceid,
                        "PKITOKEN", userid);

                //added for dumping into remote service  signature 
                PDFSigningManagement pManagement = new PDFSigningManagement();
                retValue = pManagement.addRemoteSignature(sessionid, session.getChannelid(),
                        userid,
                        archiveid,
                        SOTP,
                        0,
                        new Date(),
                        rsInfo.type,
                        rsInfo.dataToSign,
                        signature,
                        0, //success 
                        new Date(),
                        referenceid);
                //end of addition

                log.info("archiveid = " + archiveid + " and referenceid=" + referenceid + " and result is " + result);

                log.info("Exiting VerifyCredentialsAndSignTransaction");

                return aData;

            } else {
                result = -11;
                aData.archiveid = archiveid;
                aData.signature = "";
                aData.referenceid = referenceid;
//                aData.sErrorMessage = "ERROR";
                aData.iErrorCode = -2;
                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
                aData.sErrorMessage = errmsg.getUsermessage();

                audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                        cip, channel.getName(),
                        session.getLoginid(), session.getLoginid(),
                        new Date(),
                        "Sign Transaction",
                        "ERROR",
                        aData.iErrorCode,
                        "Certificate Management",
                        "",
                        "Challenge=" + challenge + ",Archiveid=" + aData.archiveid + ", Referenceid="
                        + aData.referenceid,
                        "PKITOKEN", userid);

                //added for dumping into remote service  signature 
                PDFSigningManagement pManagement = new PDFSigningManagement();
                retValue = pManagement.addRemoteSignature(sessionid, session.getChannelid(),
                        userid, archiveid,
                        SOTP,
                        0,
                        new Date(),
                        rsInfo.type,
                        rsInfo.dataToSign,
                        signature,
                        result, //success 
                        new Date(),
                        referenceid);
                //end of addition

                log.info("archiveid = " + archiveid + " and referenceid=" + referenceid + " and error  result is " + result);

                log.info("Exiting VerifyCredentialsAndSignTransaction");

                return aData;

            }
        } else {
            aData.iErrorCode = -13;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aData.iErrorCode);
            aData.sErrorMessage = errmsg.getUsermessage();
            log.info("Exiting VerifyCredentialsAndSignTransaction");
            return aData;
        }
    }

    @Override
    public String GenerateSignatureCodeRSS(String sessionid, String userid,
            String[] Data, String clientip) throws AxiomException {

        log.info("Exiting GenerateSignatureCode");

        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.info("GenerateSignatureCode::sessionid::" + sessionid);
                log.info("GenerateSignatureCode::userid::" + userid);
            }
        } catch (Exception ex) {
        }

        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
//            AxiomStatus aStatus = new AxiomStatus();
//            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
////            aStatus.error = "Licence invalid";
//            aStatus.errorcode = -100;
//            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
//            aStatus.error = errmsg.getUsermessage();
//            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
        }

        if (sessionid == null) {
            AxiomStatus aStatus = new AxiomStatus();
            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
            aStatus.errorcode = -113;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
//            throw new AxiomException("Invalid Parameters!!!");
        }

        if (Data == null) {
            AxiomStatus aStatus = new AxiomStatus();
            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
            aStatus.errorcode = -67;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
//            throw new AxiomException("Data is empty.");
        }

        if (Data.length == 0) {
            AxiomStatus aStatus = new AxiomStatus();
            ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
            aStatus.errorcode = -68;
            Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
//            throw new AxiomException("Data Arrray is empty.");
        }

        AxiomStatus aStatus = new AxiomStatus();
        ErrorMessageManagement errmsgObj = new ErrorMessageManagement();
        int retValue = -10;
//        aStatus.error = "Invalid Parameters!!!";
        aStatus.errorcode = -10;

        Errormessages errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
        aStatus.error = errmsg.getUsermessage();

        String strData = "";
        for (int i = 0; i < Data.length; i++) {
            strData += Data[i];
        }

        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(sessionid);
//        aStatus.error = "ERROR";
        aStatus.errorcode = -2;
        errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
        aStatus.error = errmsg.getUsermessage();
        if (session == null) {
//            aStatus.error = "Invalid Session";
            aStatus.errorcode = -114;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
        }
        ChannelManagement cManagement = new ChannelManagement();
        Channels channel = cManagement.getChannelByID(session.getChannelid());
        if (channel == null) {
//            aStatus.error = "Invalid Channel";
            aStatus.errorcode = -102;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
//            throw new AxiomException("Invalid Channel");
            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
        }

        SettingsManagement setManagement = new SettingsManagement();
        String channelid = session.getChannelid();
        ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
        if (channelProfileObj != null) {
            int retVal = sManagement.getServerStatus(channelProfileObj);
            if (retVal != 0) {

                aStatus = new AxiomStatus();
//                aStatus.error = "Channel Profile is not properly configured.";

                aStatus.errorcode = -82;
                errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
                aStatus.error = errmsg.getUsermessage();
                throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");

            }
        }

        Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
        if (ipobj == null) {
//            throw new AxiomException("Channel Global Setting is not configured properly");
            aStatus.errorcode = -83;
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), aStatus.errorcode);
            aStatus.error = errmsg.getUsermessage();
//                throw new AxiomException("Channel Setting is not configured properly!!!");
            throw new AxiomException(aStatus.error + " (" + aStatus.errorcode + ")");
        }

        AxiomStatus as = CheckIPAndSendAlert(channel, req.getRemoteAddr());
        if (as.errorcode != 0) {
            errmsg = errmsgObj.LoadAuditErrorUsingErrorCode(GetChannelID(), as.errorcode);
            as.error = errmsg.getUsermessage();
            throw new AxiomException(as.error);
        }

        String concatData = "";
        for (int i = 0; i < Data.length; i++) {
            concatData += Data[i];
        }

        long signaturecode = 00;
        RSSUtils rUtils = new RSSUtils();
        signaturecode = rUtils.CRC32CheckSum(concatData.getBytes());

        String signature = "" + signaturecode;

        String cip = req.getRemoteAddr();
        if (clientip == null || clientip.isEmpty() == true) {
        } else {
            cip = clientip;
        }

        aStatus.errorcode = 0;

        audit.AddAuditTrail(sessionid, channel.getChannelid(), session.getLoginid(),
                cip, channel.getName(),
                session.getLoginid(),
                session.getLoginid(),
                new Date(), "Generate Signature Code",
                "SUCCESS", aStatus.errorcode,
                "Security Management",
                "",
                errmsg.getErrorMessage(),
                "PKITOKEN", userid);

        return signature;
    }

    @Override
    public AxiomStatus AddDownloadRequest(String sessionId, String deviceId, String refid, String deviceDetails) {
        int updateRemoteSign = 0;
        Channels channel = null;
        AxiomStatus aStatus = null;
        String strDebug = "";
        log.info("#AddDownloadRequest Method Started");
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    log.info("ALertEvent::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
//                aStatus = new AxiomStatus();
//                aStatus.error = "Licence is invalid";
//                aStatus.errorcode = -100;
//                throw new AxiomException(aStatus.error);
            }
            aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";
            int retValue = -1;
            if (session != null) {
                SettingsManagement setManagement = new SettingsManagement();
                String channelid = session.getChannelid();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {
                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
                        throw new AxiomException(aStatus.error);
                    }
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                            return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }
                Remotesignature remoteSignOld = null;
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.remotesign);
                sManagement = new SessionManagement();
                session = sManagement.getSessionById(sessionId);
                String channelId = session.getChannelid();
                Session sTemplate = suTemplate.openSession();
                RemoteSignatureUtils remoteSignUtil = new RemoteSignatureUtils(suTemplate, sTemplate);
                Remotesignature[] res = remoteSignUtil.getRemoteSignObj(channelId, refid, 2);
                HashMap deviceDetailsMap = null;
                String downloadReq = "";
                if (res.length != 0) {
                    remoteSignOld = res[0];
                    HashMap DeviceRequests = null;
                    if (res.length != 0) {
                        remoteSignOld = res[0];
                        if (remoteSignOld.getDownloadrequests() != null) {
                            DeviceRequests = new HashMap();
                            DeviceRequests = (HashMap) SettingsUtil.deserializeFromObject(new ByteArrayInputStream(remoteSignOld.getDownloadrequests()));
                        }
                        if (DeviceRequests != null) {
                            DeviceRequests.put(deviceId, deviceDetails);
                        } else {
                            DeviceRequests = new HashMap();
                            DeviceRequests.put(deviceId, deviceDetails);
                        }
                        byte[] serializedMap = SettingsUtil.serializeToObject(DeviceRequests);
                        remoteSignOld.setDownloadrequests(serializedMap);
                    }
                } else {
                    aStatus.errorcode = -1;
                    aStatus.error = "No Signing Details to update for this User!!!";
                    log.info("No signing details available for this user");
                    return aStatus;
                }
                updateRemoteSign = remoteSignUtil.changeRemotesignatureWithQR(refid, remoteSignOld);
                aStatus.errorcode = updateRemoteSign;
                if (updateRemoteSign == 0) {
                    aStatus.error = "SignatureDetails Successfully Updated!!!";
                    aStatus.errorcode = 0;
                } else {
                    aStatus.error = "Excep occured while updating!!!";
                    aStatus.errorcode = updateRemoteSign;
                }
                UserManagement uManagement = new UserManagement();
                AuthUser oldUser = uManagement.getUser(sessionId, session.getChannelid(), remoteSignOld.getUserid());
                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
                        req.getRemoteAddr(), channel.getName(),
                        session.getLoginid(), session.getLoginid(),
                        new Date(), "Create User",
                        aStatus.error, aStatus.errorcode,
                        "Remote Server Signing", "Update SignDetails",
                        "Name=" + remoteSignOld.getUserid() + ",Phone=" + oldUser.getPhoneNo() + ",Email=" + oldUser.getEmail() + ",State=active",
                        "Result", aStatus.error);
                return aStatus;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public EasyCheckInSessions addEasyLoginRequest(String sessionId, EasyCheckInSessions easychecksession) {
        int updateRemoteSign = 0;
        Channels channel = null;
        AxiomStatus aStatus = null;
        String strDebug = "";
        log.info("#addEasyLoginRequest Method Started");
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    log.info("ALertEvent::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
//                aStatus = new AxiomStatus();
//                aStatus.error = "Licence is invalid";
//                aStatus.errorcode = -100;
//                throw new AxiomException(aStatus.error);
            }
            aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";
            int retValue = -1;
            if (session != null) {
                String channelid = session.getChannelid();
                SettingsManagement setManagement = new SettingsManagement();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {
                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
                        throw new AxiomException(aStatus.error);
                    }
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                            return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }

//                System.out.println("getAgent : " + eloginSession.getAgent);
//                System.out.println("getAppChannelid : " + eloginSession.getAppChannelid());
//                System.out.println("getAppIp : " + eloginSession.getAppIp());
//                System.out.println("getAppSesssionId : " + eloginSession.getAppSesssionId());
//                System.out.println("getAppUserid : " + eloginSession.getAppUserid());
//                System.out.println("getBrowser : " + eloginSession.getBrowser());
//                System.out.println("getEasyloginId : " + eloginSession.getEasyloginId());
//                System.out.println("getErrormsg : " + eloginSession.getErrormsg());
//                System.out.println("getLattitude : " + eloginSession.getLattitude());
//                System.out.println("getLocation : " + eloginSession.getLocation());
//                System.out.println("getLongitude : " + eloginSession.getLongitude());
//                System.out.println("getCreatedon : " + eloginSession.getCreatedon());
//                System.out.println("getElsid : " + eloginSession.getElsid());
//                System.out.println("getRequesterIndex : " + eloginSession.getRequesterIndex());
//                System.out.println("getStatus : " + eloginSession.getStatus());
                int r;
                if (easychecksession != null) {
                    ApEasyloginsession easysession = new ApEasyloginsession();
                    EasyLoginSessionManagement elsm = new EasyLoginSessionManagement();
                    easysession.setAgent(easychecksession.getAgent());
                    easysession.setAppChannelid(easychecksession.getAppChannelid());
                    easysession.setAppIp(easychecksession.getAppIp());
                    easysession.setAppSesssionId(easychecksession.getAppSesssionId());
                    easysession.setAppUserid(easychecksession.getAppUserid());
                    easysession.setBrowser(easychecksession.getBrowser());
                    easysession.setCreatedon(easychecksession.getCreatedon());
                    easysession.setEasyloginId(easychecksession.getEasyloginId());
                   // easysession.setElsid(easychecksession.getElsid());
                    easysession.setErrormsg(easychecksession.getErrormsg());    
                    easysession.setLattitude(easychecksession.getLattitude());
                    easysession.setLocation(easychecksession.getLocation());
                    easysession.setLongitude(easychecksession.getLongitude());
                    easysession.setRequesterIndex(easychecksession.getRequesterIndex());
                    easysession.setStatus(easychecksession.getStatus());
                    r = elsm.addEasyLoginSessionDetails(sessionId, channelid, easysession);
                } else {
                    r = -1;
                }
                if(r == 0){
                    return easychecksession;
                }
//                if (r == 0) {
//                    Registerdevicepush registerdevicepush = new PushNotificationDeviceManagement().GetRegisterPushDeviceByUserID(sessionId, channelid, eloginSession.getAppUserid());
//                    if (registerdevicepush != null) {
//                        UserManagement umngt = new UserManagement();
//                        AuthUser auser = umngt.getUser(sessionId, channelid, eloginSession.getAppUserid());
//                        JSONObject json = new JSONObject();
//                        json.put("message", "Easy Login Request from : " + eloginSession.getAppIp());
//                        json.put("title", "Axiom Protect");
//                        AXIOMStatus status = new SendNotification().SendOnPush(channelid, registerdevicepush.getGoogleregisterid(), auser.email, json.toString(), ANDROID);
//                    }
//                }
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public EasyCheckInSessions getEasyLoginRequest(String sessionId, String userid) {
        Channels channel = null;
        AxiomStatus aStatus = null;
        String strDebug = "";
        log.info("#AddDownloadRequest Method Started");
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    log.info("ALertEvent::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
//                aStatus = new AxiomStatus();
//                aStatus.error = "Licence is invalid";
//                aStatus.errorcode = -100;
//                throw new AxiomException(aStatus.error);
            }
            aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";
            int retValue = -1;
            if (session != null) {
                String channelid = session.getChannelid();
                SettingsManagement setManagement = new SettingsManagement();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {
                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
                        throw new AxiomException(aStatus.error);
                    }
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                            return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }
                EasyLoginSessionManagement elsm = new EasyLoginSessionManagement();
                ApEasyloginsession easysession = elsm.getEasyLoginSessionByUser(sessionId, channelid, userid);
//                System.out.println(" The result is :" + r);
//                if (r != -1) {
//                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
//                        req.getRemoteAddr(), channel.getName(),
//                        session.getLoginid(), session.getLoginid(),
//                        new Date(), "Create User",
//                        aStatus.error, aStatus.errorcode,
//                        "Remote Server Signing", "Update SignDetails",
//                        "Name=" + remoteSignOld.getUserid() + ",Phone=" + oldUser.getPhoneNo() + ",Email=" + oldUser.getEmail() + ",State=active",
//                        "Result", aStatus.error);
//                return aStatus;

                if (easysession != null) {
                    EasyCheckInSessions easycheckinsession = new EasyCheckInSessions();
                    easycheckinsession.setAgent(easysession.getAgent());
                    easycheckinsession.setAppChannelid(easysession.getAppChannelid());
                    easycheckinsession.setAppIp(easysession.getAppIp());
                    easycheckinsession.setAppSesssionId(easysession.getAppSesssionId());
                    easycheckinsession.setAppUserid(easysession.getAppUserid());
                    easycheckinsession.setBrowser(easysession.getBrowser());
                    easycheckinsession.setCreatedon(easysession.getCreatedon());
                    easycheckinsession.setEasyloginId(easysession.getEasyloginId());
                    easycheckinsession.setElsid(easysession.getElsid());
                    easycheckinsession.setErrormsg(easysession.getErrormsg());
                    easycheckinsession.setLattitude(easysession.getLattitude());
                    easycheckinsession.setLocation(easysession.getLocation());
                    easycheckinsession.setLongitude(easysession.getLongitude());
                    easycheckinsession.setRequesterIndex(easysession.getRequesterIndex());
                    easycheckinsession.setRequesterIndex(easysession.getStatus());
                    return easycheckinsession;
                } else {
                    return null;
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public int editEasyLoginRequest(String sessionId, EasyCheckInSessions easychecksession) {
        int updateRemoteSign = 0;
        Channels channel = null;
        AxiomStatus aStatus = null;
        String strDebug = "";
        log.info("#AddDownloadRequest Method Started");
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    log.info("ALertEvent::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
            }
//nilesh
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
//                aStatus = new AxiomStatus();
//                aStatus.error = "Licence is invalid";
//                aStatus.errorcode = -100;
//                throw new AxiomException(aStatus.error);
            }
            aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";
            int retValue = -1;
            if (session != null) {
                String channelid = session.getChannelid();
                SettingsManagement setManagement = new SettingsManagement();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {
                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
                        throw new AxiomException(aStatus.error);
                    }
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                            return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }
                int result;
                EasyLoginSessionManagement elsm = new EasyLoginSessionManagement();
                ApEasyloginsession easysession = new ApEasyloginsession();
                if (easychecksession != null) {
                    easysession.setAgent(easychecksession.getAgent());
                    easysession.setAppChannelid(easychecksession.getAppChannelid());
                    easysession.setAppIp(easychecksession.getAppIp());
                    easysession.setAppSesssionId(easychecksession.getAppSesssionId());
                    easysession.setAppUserid(easychecksession.getAppUserid());
                    easysession.setBrowser(easychecksession.getBrowser());
                    easysession.setCreatedon(easychecksession.getCreatedon());
                    easysession.setEasyloginId(easychecksession.getEasyloginId());
                    easysession.setElsid(easychecksession.getElsid());
                    easysession.setErrormsg(easychecksession.getErrormsg());
                    easysession.setLattitude(easychecksession.getLattitude());
                    easysession.setLocation(easychecksession.getLocation());
                    easysession.setLongitude(easychecksession.getLongitude());
                    easysession.setRequesterIndex(easychecksession.getRequesterIndex());
                    easysession.setStatus(easychecksession.getStatus());
                    result = elsm.editEasyLoginSessionDetails(sessionId, channelid, easysession);
                } else {
                    result = -1;
                }
//                        getEasyLoginSessionByUser(sessionId, channelid, userid);
//                System.out.println(" The result is :" + r);
//                if (r != -1) {
//                audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(),
//                        req.getRemoteAddr(), channel.getName(),
//                        session.getLoginid(), session.getLoginid(),
//                        new Date(), "Create User",
//                        aStatus.error, aStatus.errorcode,
//                        "Remote Server Signing", "Update SignDetails",
//                        "Name=" + remoteSignOld.getUserid() + ",Phone=" + oldUser.getPhoneNo() + ",Email=" + oldUser.getEmail() + ",State=active",
//                        "Result", aStatus.error);
//                return aStatus;
                return result;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return -1;
    }

    @Override
    public AxiomStatus verifyEasyLoginRequest(String sessionId, String userid, String lattitude, String longitude) {
        Channels channel = null;
        AxiomStatus aStatus = null;
        String strDebug = "";
        log.info("#verifyEasyLoginRequest Method Started");
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    log.info("ALertEvent::sessionid::" + sessionId);
                    log.info("ALertEvent::userid::" + userid);
                    log.info("ALertEvent::lattitude::" + lattitude);
                    log.info("ALertEvent::longitude::" + longitude);
                }
            } catch (Exception ex) {
            }
            System.out.println("sessionid::" + sessionId);
            System.out.println("userid::" + userid);
            System.out.println("lattitude::" + lattitude);
            System.out.println("longitude::" + longitude);
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
//                aStatus = new AxiomStatus();
//                aStatus.error = "Licence is invalid";
//                aStatus.errorcode = -100;
//                throw new AxiomException(aStatus.error);
            }
            aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";
            int retValue = -1;
            if (session != null) {
                String channelid = session.getChannelid();
                SettingsManagement setManagement = new SettingsManagement();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {
                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
                        throw new AxiomException(aStatus.error);
                    }
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                            return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }
                EasyLoginManagement elm = new EasyLoginManagement();
                EasyLoginSessionManagement elsm = new EasyLoginSessionManagement();
                ApEasylogin elogin = elm.getEasyLoginByUser(sessionId, channelid, userid);
                ApEasyloginsession lsession = elsm.getPendingEasyLoginSessionByUser(sessionId, channelid, userid);
                if (lsession != null) {
                    if (elogin != null) {
                        System.err.println("getEasyLoginByUser is : " + elogin.getUserName());
                        lsession.setEasyloginId(elogin.getEasyloginId());
                    }
                    elsm.editEasyLoginSessionDetails(sessionId, channelid, lsession);
                    //                verify the expiry time
                    if (elogin != null) {
                        if (elogin.getExpireson().after(new Date())) {
//                        verify number of reuqest for the check in
                            if (elogin.getCounter() < 3) {
                                elogin.setCounter(elogin.getCounter() + 1);
                                elm.editEasyLoginRequest(sessionId, channelid, elogin);
                                lsession.setRequesterIndex(elogin.getCounter());
//                verify the geolocation
                                if (elogin.getLongitude() == null || elogin.getLattitude() == null) {
                                    aStatus.errorcode = -110;
                                    aStatus.error = "Please Turn on your device GPS!!!";
                                    return aStatus;
                                } else if (elogin.getLongitude().isEmpty() || elogin.getLattitude().isEmpty()) {
                                    aStatus.errorcode = -110;
                                    aStatus.error = "Please turn on your device GPS!!!";
                                    return aStatus;
                                }
                                double longitude1 = Double.parseDouble(elogin.getLongitude());
                                double latittude1 = Double.parseDouble(elogin.getLattitude());
                                double longitude2 = Double.parseDouble(longitude);
                                double latittude2 = Double.parseDouble(lattitude);
                                double geodistanse = UtilityFunctions.findDistance(latittude1, longitude1, latittude2, longitude2);
                                if (geodistanse <= 5000) {
//                        check the device details
                                    TrustDeviceManagement tdm = new TrustDeviceManagement();
                                    Trusteddevice devicedetails = tdm.getTrusteddevice(sessionId, channelid, userid);
                                    if (devicedetails.getDeviceid().equals(elogin.getDeviceid())) {
//                       check the OTP
                                        OTPTokenManagement omngt = new OTPTokenManagement(channelid);
                                        int success = omngt.VerifyOTP(channelid, userid, sessionId, elogin.getOtp());
                                        if (success == 0) {
//                        check the data intrity
                                            String integrity = userid + channel.getChannelid() + elogin.getDeviceid() + elogin.getOtp();
                                            String authsign = new String(Base64.encode(integrity.getBytes()));
                                            if (elogin.getAuthsign().equals(authsign)) {
                                                lsession.setStatus(ELS_APPROVED_STATUS);
                                                lsession.setErrormsg("User Details is verified");
                                                elsm.editEasyLoginSessionDetails(sessionId, channelid, lsession);
                                                aStatus.errorcode = 0;
                                                aStatus.error = "User Details is verified!!!";
                                                return aStatus;
                                            } else {
                                                lsession.setStatus(ELS_REJECTED_STATUS);
                                                lsession.setErrormsg("Data integrity is not matched");
                                                elsm.editEasyLoginSessionDetails(sessionId, channelid, lsession);
                                                aStatus.errorcode = -115;
                                                aStatus.error = "Data integrity is not matched!!!";
                                                return aStatus;
                                            }
                                        } else {
                                            lsession.setStatus(ELS_REJECTED_STATUS);
                                            lsession.setErrormsg("OTP is not Verified");
                                            elsm.editEasyLoginSessionDetails(sessionId, channelid, lsession);
                                            aStatus.errorcode = -114;
                                            aStatus.error = "OTP is not Verified!!!";
                                            return aStatus;
                                        }
                                    } else {
                                        lsession.setStatus(ELS_REJECTED_STATUS);
                                        lsession.setErrormsg("Device details are not Matched");
                                        elsm.editEasyLoginSessionDetails(sessionId, channelid, lsession);
                                        aStatus.errorcode = -114;
                                        aStatus.error = "Device details are not Matched!!!";
                                        return aStatus;
                                    }
                                } else {
                                    lsession.setStatus(ELS_REJECTED_STATUS);
                                    lsession.setErrormsg("Geolocation is not Matched");
                                    elsm.editEasyLoginSessionDetails(sessionId, channelid, lsession);
                                    aStatus.errorcode = -113;
                                    aStatus.error = "Geolocation is not Matched!!!";
                                    return aStatus;
                                }
                            } else {
                                lsession.setStatus(ELS_REJECTED_STATUS);
                                lsession.setErrormsg("Maximum Attempts Reached");
                                elsm.editEasyLoginSessionDetails(sessionId, channelid, lsession);
                                aStatus.errorcode = -112;
                                aStatus.error = "Maximum Attempts Reached!!!";
                                return aStatus;
                            }
                        } else {
                            elogin.setStatus(EL_EXPIRED_STATUS);
                            elm.editEasyLoginRequest(sessionId, channelid, elogin);
                            lsession.setStatus(ELS_EXPIRED_STATUS);
                            lsession.setErrormsg(" Either Your Request is not instantiated or Expired");
                            elsm.editEasyLoginSessionDetails(sessionId, channelid, lsession);
                            aStatus.errorcode = -111;
                            aStatus.error = "Request Session Expired!!!";
                            return aStatus;
                        }
                    } else {
                        lsession.setStatus(ELS_NA_STATUS);
                        lsession.setErrormsg(" Either Your Request is not Instantiated");
                        aStatus.errorcode = -110;
                        aStatus.error = "Request is not instantiated!!!";
                        return aStatus;
                    }
                } else {
                    aStatus.errorcode = -109;
                    aStatus.error = "Request is not instantiated!!!";
                    return aStatus;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return aStatus;
    }

    @Override
    public EasyCheckIn getEasyLogin(String sessionId, String EasyLoginId) {
        Channels channel = null;
        AxiomStatus aStatus = null;
        String strDebug = "";
        log.info("#getEasyLogin Method Started");
        EasyCheckIn easycheckin = null;
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    log.info("ALertEvent::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
            }
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
//                aStatus = new AxiomStatus();
//                aStatus.error = "Licence is invalid";
//                aStatus.errorcode = -100;
//                throw new AxiomException(aStatus.error);
            }
            aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";
            int retValue = -1;
            if (session != null) {
                String channelid = session.getChannelid();
                SettingsManagement setManagement = new SettingsManagement();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, SettingsManagement.CHANNELPROFILE_SETTING, SettingsManagement.PREFERENCE_ONE);
                if (channelProfileObj != null) {
                    int retVal = sManagement.getServerStatus(channelProfileObj);
                    if (retVal != 0) {
                        aStatus = new AxiomStatus();
                        aStatus.error = "Server Down for maintainance,Please try later!!!";
                        aStatus.errorcode = -48;
                        throw new AxiomException(aStatus.error);
                    }
                }
                Object ipobj = setManagement.getSettingInner(channelid, SettingsManagement.GlobalSettings, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    if (req.getRemoteAddr().compareTo("127.0.0.1") != 0) {
                        checkIp = setManagement.checkIP(channelid, req.getRemoteAddr());
                    } else {
                        checkIp = 1;
                    }
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.templates);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_IP_BLOCKED_ALERT);
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; i++) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == tUtil.ACTIVE_STATUS) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject(baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        // Date date = new Date();
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }

                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody,
                                            emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
//                            return aStatus;
                                throw new AxiomException(aStatus.error);
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
//                            return aStatus;
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
//                    return aStatus;
                        throw new AxiomException(aStatus.error);
                    }
                }
                EasyLoginManagement elm = new EasyLoginManagement();
                ApEasylogin elogin = elm.getEasyLoginByLoginId(sessionId, channel.getChannelid(), EasyLoginId);
                if (elogin != null) {
                    easycheckin = new EasyCheckIn();
                    easycheckin.setAuthsign(elogin.getAuthsign());
                    easycheckin.setChannelid(elogin.getChannelid());
                    easycheckin.setCounter(elogin.getCounter());
                    easycheckin.setCreatedon(elogin.getCreatedon());
                    easycheckin.setDeviceid(elogin.getDeviceid());
                    easycheckin.setDeviceinfo(elogin.getDeviceinfo());
                    easycheckin.setEasyloginId(elogin.getEasyloginId());
                    easycheckin.setElid(elogin.getElid());
                    easycheckin.setErrormsg(elogin.getErrormsg());
                    easycheckin.setExpireson(elogin.getExpireson());
                    easycheckin.setIp(elogin.getIp());
                    easycheckin.setLattitude(elogin.getLattitude());
                    easycheckin.setLongitude(elogin.getLongitude());
                    easycheckin.setOtp(elogin.getOtp());
                    easycheckin.setStatus(elogin.getStatus());
                    easycheckin.setUserName(elogin.getUserName());
                    easycheckin.setUserid(elogin.getUserid());
                } else {
                    easycheckin = null;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return easycheckin;
    }

    public AxiomStatus getQrImageWithRegCode(String sessionId, String userid, int type) {
        Channels channel = null;
        AxiomStatus aStatus = null;
        String strDebug = "";
        log.info("#getQrImageWithRegCode Method Started");
        Object elogin = null;
        try {
            try {
                strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
                if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                    log.info("getQrImageWithRegCode::sessionid::" + sessionId);
                }
            } catch (Exception ex) {
                // empty catch block
            }
            int iResult = AxiomProtect.ValidateLicense();
            if (iResult != 0) {
//                aStatus = new AxiomStatus();
//                aStatus.error = "Licence is invalid";
//                aStatus.errorcode = -100;
//                throw new AxiomException(aStatus.error);
            }
            aStatus = new AxiomStatus();
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = this.wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
            int portNumber = req.getServerPort();
            String ipAddress = LoadSettings.g_sSettings.getProperty("mobileresgistration.serverip");
            Sessions session = sManagement.getSessionById(sessionId);
            if (session == null) {
                aStatus.error = "Invalid Session";
                aStatus.errorcode = -14;
            }
            aStatus.error = "ERROR";
            aStatus.errorcode = -2;
            String oldValue = "";
            String strValue = "";
            int retValue = -1;
            if (session != null) {
                int retVal;
                String channelid = session.getChannelid();
                SettingsManagement setManagement = new SettingsManagement();
                ChannelManagement cManagement = new ChannelManagement();
                channel = cManagement.getChannelByID(channelid);
                ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, 20, 1);
                if (channelProfileObj != null && (retVal = sManagement.getServerStatus(channelProfileObj)) != 0) {
                    aStatus = new AxiomStatus();
                    aStatus.error = "Server Down for maintainance,Please try later!!!";
                    aStatus.errorcode = -48;
                    throw new AxiomException(aStatus.error);
                }
                Object ipobj = setManagement.getSettingInner(channelid, 10, 1);
                if (ipobj != null) {
                    GlobalChannelSettings iObj = (GlobalChannelSettings) ipobj;
                    int checkIp = 1;
                    checkIp = req.getRemoteAddr().compareTo("127.0.0.1") != 0 ? setManagement.checkIP(channelid, req.getRemoteAddr()) : 1;
                    if (iObj.ipstatus == 0 && checkIp != 1) {
                        if (iObj.ipalertstatus == 0) {
                            SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                            Session sTemplate = suTemplate.openSession();
                            TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                            Templates templatesObj = tUtil.loadbyName(channelid, "email.ipfilter");
                            OperatorsManagement oManagement = new OperatorsManagement();
                            Operators[] aOperator = oManagement.getAdminOperator(channel.getChannelid());
                            if (aOperator != null) {
                                String[] emailList = new String[aOperator.length - 1];
                                for (int i = 1; i < aOperator.length; ++i) {
                                    emailList[i - 1] = aOperator[i].getEmailid();
                                }
                                if (templatesObj.getStatus() == 1) {
                                    ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                                    String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                                    String strsubject = templatesObj.getSubject();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    if (strmessageBody != null) {
                                        strmessageBody = strmessageBody.replaceAll("#name#", aOperator[0].getName());
                                        strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                                        strmessageBody = strmessageBody.replaceAll("#email#", aOperator[0].getEmailid());
                                        strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                                        strmessageBody = strmessageBody.replaceAll("#filterword#", req.getRemoteAddr());
                                    }
                                    SendNotification send = new SendNotification();
                                    AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), aOperator[0].getEmailid(), strsubject, strmessageBody, emailList, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
                                }
                                suTemplate.close();
                                sTemplate.close();
                                aStatus.error = "INVALID IP REQUEST";
                                aStatus.errorcode = -8;
                                throw new AxiomException(aStatus.error);
                            }
                            suTemplate.close();
                            sTemplate.close();
                            aStatus.error = "INVALID IP REQUEST";
                            throw new AxiomException(aStatus.error);
                        }
                        aStatus.error = "INVALID IP REQUEST";
                        aStatus.errorcode = -8;
                        throw new AxiomException(aStatus.error);
                    }
                }
                OTPTokenManagement oManagement = new OTPTokenManagement(channel.getChannelid());
                String registrationCode = oManagement.generateRegistrationCode(sessionId, channel.getChannelid(), userid, type);
                RegistrationCodeTrailManagement regcodeMgmt = new RegistrationCodeTrailManagement();
                String registrationcode = UtilityFunctions.Bas64SHA1((String) registrationCode);
                regcodeMgmt.addRegCodeTrail(channel.getChannelid(), userid, registrationcode, 0, 1);
                JSONObject jsonData = new JSONObject();
                jsonData.put("regcode", (Object) registrationCode);
                if (ipAddress != null) {
                    jsonData.put("ip", (Object) ipAddress);
                } else {
                    jsonData.put("ip", (Object) "127.0.0.1");
                }
                jsonData.put("port", portNumber);
                jsonData.put("conn", (Object) "https");
                PDFSigningManagement pdfSmgmt = new PDFSigningManagement();
                String bae64ImageQR = pdfSmgmt.getQrCodeImage(jsonData);
                if (bae64ImageQR != null) {
                    aStatus = new AxiomStatus();
                    aStatus.regCodeMessage = bae64ImageQR;
                    aStatus.error = "Success";
                    aStatus.errorcode = 0;
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "getQrImageWithRegCode", aStatus.error, aStatus.errorcode, "SoftWare token Registration", "Embedding regcode in QRCODE", "Name=" + userid + ",Phone=" + userid + ",Email=" + userid + ",State=active", "Result", aStatus.error);
                } else {
                    aStatus = new AxiomStatus();
                    aStatus.regCodeMessage = "";
                    aStatus.error = "Error";
                    aStatus.errorcode = -1;
                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(), session.getLoginid(), session.getLoginid(), new Date(), "getQrImageWithRegCode", aStatus.error, aStatus.errorcode, "SoftWare token Registration", "Embedding regcode in QRCODE", "Name=" + userid + ",Phone=" + userid + ",Email=" + userid + ",State=active", "Result", aStatus.error);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return aStatus;
    }

    @Override
    public AxiomStatus ping() {
        String strDebug = null;
        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                Date d = new Date();
                //log.debug(d + ">>" + "ping::" + new Date());
            }
        } catch (Exception ex) {
        }
        AxiomStatus response = new AxiomStatus();
        response.error = "SUCCESS";
        response.errorcode = 0;
        return response;
    }

    public AxiomStatus GetGoogleAuthToken(String sessionid, String userid, boolean bSendEmail, boolean bReturnB64QRCode) {

        Channels channel = null;
        AxiomStatus aStatus = null;
        String strDebug = "";
        HttpServletRequest req;
        log.info("#GetGoogleAuthToken Method Started");

        try {
            strDebug = LoadSettings.g_sSettings.getProperty("reserved.5");
            if (strDebug != null && strDebug.compareToIgnoreCase("yes") == 0) {
                log.info("GetGoogleAuthToken::sessionid::" + sessionid);
                log.info("GetGoogleAuthToken::userid::" + userid);
                log.info("GetGoogleAuthToken::bSendEmail::" + bSendEmail);
                log.info("GetGoogleAuthToken::bReturnB64QRCode::" + bReturnB64QRCode);
            }
        } catch (Exception ex) {
            // empty catch block
        }
        int iResult = AxiomProtect.ValidateLicense();
        if (iResult != 0) {
//            aStatus = new AxiomStatus();
//            aStatus.error = "Licence is invalid";
//            aStatus.errorcode = -100;
//            try {
//                throw new AxiomException(aStatus.error);
//            } catch (AxiomException ex) {
//                Logger.getLogger(RSSCoreInterfaceImpl.class.getName()).log(Level.SEVERE, null, ex);
//            }
        }
        aStatus = new AxiomStatus();
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = this.wsContext.getMessageContext();
        req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
        Sessions session = sManagement.getSessionById(sessionid);
        if (session == null) {
            aStatus.error = "Invalid Session";
            aStatus.errorcode = -14;
        }
        aStatus.error = "ERROR";
        aStatus.errorcode = -2;
        String oldValue = "";
        String strValue = "";
        int retValue = -1;
        if (session != null) {
            int retVal;
            String channelid = session.getChannelid();
            SettingsManagement setManagement = new SettingsManagement();
            ChannelManagement cManagement = new ChannelManagement();
            channel = cManagement.getChannelByID(channelid);
            ChannelProfile channelProfileObj = (ChannelProfile) setManagement.getSetting(channelid, 20, 1);
            if (channelProfileObj != null && (retVal = sManagement.getServerStatus(channelProfileObj)) != 0) {
                aStatus = new AxiomStatus();
                aStatus.error = "Server Down for maintainance,Please try later!!!";
                aStatus.errorcode = -48;
                try {
                    throw new AxiomException(aStatus.error);
                } catch (AxiomException ex) {
                    Logger.getLogger(RSSCoreInterfaceImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            //
            OTPTokenManagement otp = new OTPTokenManagement(channelid);
            int status = otp.getStatus(sessionid, channelid, userid, 1, 1);

            if (status == 1 || status == 0) {
                if (status == 0) {
                    int changedStatus = otp.ChangeStatus(channelid, userid, 1, 1, 1);
                    if (changedStatus != 0) {
                        aStatus.errorcode = -2;
                        aStatus.error = " Unable To change Users Software Status to active .!!";
                        return aStatus;
                    }
                }

//            if (status == 1) {
                Otptokens details = otp.getOtpObjByUserId(sessionid, channelid, userid, 1);
                String secret = details.getSecret();
                System.out.println("Secret>> " + secret);

                byte[] secret1 = CryptoManager.hexStringToByteArray(secret);

                byte[] decoded = org.apache.commons.codec.binary.Base64.decodeBase64(secret.getBytes());
                String secrestforQR = base32().encode(secret1);
                String url = "otpauth://totp/" + channel.getName() + ":" + userid + "?secret=" + secrestforQR + "&issuer=" + channel.getName();
                System.out.println("url is :" + url);
                QrcodeGenerator Qrimage = new QrcodeGenerator();
                String image = Qrimage.CreateQRCode(url);

                if (bSendEmail == true) {
                    UserManagement userobj = new UserManagement();
                    AuthUser userDetails = userobj.getUser(channelid, userid);
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(18);
                    Session sTemplate = suTemplate.openSession();
                    TemplateUtils tUtil = new TemplateUtils(suTemplate, sTemplate);
                    Templates templatesObj = tUtil.loadbyName(channelid, TemplateNames.EMAIL_GOOGLE_AUTH_TEMPLATE);
                    OperatorsManagement oManagement = new OperatorsManagement();

                    if (templatesObj.getStatus() == 1) {
                        ByteArrayInputStream baisobj = new ByteArrayInputStream(templatesObj.getTemplatebody());
                        String strmessageBody = (String) TemplateUtils.deserializeFromObject((ByteArrayInputStream) baisobj);
                        String strsubject = templatesObj.getSubject();
                        String ip = null;
                        String port = null;
                        String https = null;
                        //added
                        Properties prop = new Properties();
                        InputStream input = null;
                        try {

                            ip = g_sSettings.getProperty("googauthenticator.ipaddress"); //  prop.getProperty("mobiletrust.ipaddress");
                            String http = g_sSettings.getProperty("googauthenticator.secured");
                            port = g_sSettings.getProperty("googauthenticator.port");
                            if (http.equals("yes")) {
                                https = "https";
                            } else {
                                https = "http";
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        //https://sgenrich.mollatech.com:7443/face

                        String QRurl = https + "://" + ip + ":" + port + "/face/GoogleAuthQRImage.jsp?userid=" + userid;
                        System.out.println("QRurl" + QRurl);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        if (strmessageBody != null) {
                            strmessageBody = strmessageBody.replaceAll("#name#", userDetails.getUserName());
                            strmessageBody = strmessageBody.replaceAll("#channel#", channel.getName());
                            strmessageBody = strmessageBody.replaceAll("#email#", userDetails.getEmail());
                            strmessageBody = strmessageBody.replaceAll("#datetime#", sdf.format(new Date()));
                            strmessageBody = strmessageBody.replaceAll("#image#", QRurl);
                            strmessageBody = strmessageBody + "\n\n<br/><img src=\"data:image/png;base64," + image + "\"alt=\" Encoded Image\"/>";

// attach the image file
                            System.out.println("Base64 Image   data:image/png;base64" + image);
                        }
                        SendNotification send = new SendNotification();
                        AXIOMStatus axStatus = send.SendEmail(channel.getChannelid(), userDetails.getEmail(), strsubject, strmessageBody, null, null, null, null, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));

                        aStatus.error = axStatus.strStatus;
                        aStatus.errorcode = axStatus.iStatus;
                        aStatus.regCodeMessage = "Email sent successfully.";
                        return aStatus;
                    }
                } else if (bReturnB64QRCode == true) {
                    aStatus.regcode = image;
                    aStatus.error = "Success";
                    aStatus.errorcode = 0;
                    aStatus.regCodeMessage = "Base64 QR code image";
                    return aStatus;

                } else {
                    aStatus.error = "Please select any one valid option";

                    return aStatus;
                }

            } else {
                aStatus.error = "User  Software Status Is not active";

                return aStatus;
            }

        } else {
            aStatus.error = "Invalid Session";
            return aStatus;
        }
        return null;

    }

}
