/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ibanking.acleda.proj.apac.ps.hid.com;


import com.mollatech.axiom.connector.user.AuthUser;
import com.mollatech.axiom.connector.user.TokenStatusDetails;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.crypto.LoadSettings;
import com.mollatech.axiom.nucleus.db.Channels;
import com.mollatech.axiom.nucleus.db.Sessions;
import com.mollatech.axiom.nucleus.db.Templates;
import com.mollatech.axiom.nucleus.db.connector.TemplateUtils;
import com.mollatech.axiom.nucleus.db.connector.management.AuditManagement;
import com.mollatech.axiom.nucleus.db.connector.management.ChannelManagement;
import com.mollatech.axiom.nucleus.db.connector.management.OTPTokenManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SessionManagement;
import com.mollatech.axiom.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.axiom.nucleus.db.connector.management.TemplateManagement;
import com.mollatech.axiom.nucleus.db.connector.management.UserManagement;
import com.mollatech.axiom.nucleus.db.operation.TemplateNames;
import com.mollatech.axiom.nucleus.settings.SendNotification;
import java.io.ByteArrayInputStream;
import java.util.Date;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

/**
 *
 * @author vikramsareen
 */
@WebService(serviceName =  "FTRESSServiceWrapperService",endpointInterface = "ibanking.acleda.proj.apac.ps.hid.com.FtressWrapperInterface",targetNamespace =  "http://com.hid.ps.apac.proj.acleda.ibanking",portName = "FTRESSServiceWrapperPort")
public class FtressWrapperInterfaceImpl implements FtressWrapperInterface {
    
    private int GETUSER_BY_USERID = 1;
    private int GETUSER_BY_PHONENUMBER = 2;
    private int GETUSER_BY_EMAILID = 3;
    private int GETUSER_BY_FULLNAME = 4;
    private int RESET_USER_PASSWORD = 1;
    private int RESET_USER_TOKEN_OOB = 2;
    private int RESET_USER_TOKEN_SOFTWARE = 3;
    private int RESET_USER_TOKEN_HARDWARE = 4;
    private int OTP_TOKEN_OUTOFBAND = 3;
    private int OTP_TOKEN_SOFTWARE = 1;
    private int OTP_TOKEN_HARDWARE = 2;
    private int OTP_TOKEN_OUTOFBAND_SMS = 1;
    private int OTP_TOKEN_OUTOFBAND_VOICE = 2;
    private int OTP_TOKEN_OUTOFBAND_USSD = 3;
    private int OTP_TOKEN_OUTOFBAND_EMAIL = 4;
    private int OTP_TOKEN_SOFTWARE_MOBILE = 2;
    private int OTP_TOKEN_SOFTWARE_PC = 3;
    private int OTP_TOKEN_SOFTWARE_WEB = 1;
    private int OTP_TOKEN_HARDWARE_MINI = 1;
    private int OTP_TOKEN_HARDWARE_CR = 2;
//    private int MESSAGE_AS_SMS = 1;
//    private int MESSAGE_AS_VOICE = 2;
//    private int MESSAGE_AS_USSD = 3;
//    private int MESSAGE_AS_MAIL = 4;
    private int TOKEN_STATUS_UNASSIGNED = -10;
    private int TOKEN_STATUS_ACTIVE = 1;
    private int TOKEN_STATUS_SUSPENDED = -2;
    //private int TOKEN_STATUS_PENDING = 0;
    //public int MESSAGES_BLOCKED= -5;
    private String strACTIVE = "ACTIVE";
    private String strSUSPENDED = "SUSPENDED";
    //private String strPENDING = "PENDING";
    //public String strBLOCKED = "BLOCKED";    
    private String strUNASSIGNED = "UNASSIGNED";
    //private String USER_ASSIGN_PASSWORD_TEMPLATE_NAME = "User Password";
    private int SEND_MESSAGE_PENDING_STATE = 2;
    private String SEND_MESSAGE_PENDING_STATE_STRING = "PENDING";
    private int SEND_MESSAGE_BLOCKED_STATE = -5;
    private String SEND_MESSAGE_BLOCKED_STATE_STRING = "BLOCKED";
    private int SEND_MESSAGE_INVALID_DATA = -4;
    private String SEND_MESSAGE_INVALID_DATA_STRING = "INVALIDDATA";
    private int INVALID_IP = -3;
    private int INVALID_REMOTEUSER = -4;
    @Resource
    WebServiceContext wsContext;

    
   
  

    
   
   
    @Override
    public AuthenticationResponse verifyEBankingPassword(String systemSessionId, String userid, String password, int fds_options, String channel, String domain) {
      //        int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//            AuthenticationResponse aStatus = new AuthenticationResponse();
//            aStatus.errorMessage = "Licence Not Valid";
//            aStatus.status = -10;
//        aStatus.session = systemSessionId;
//            return aStatus;
//        }
        UserManagement uManagement = new UserManagement();
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(systemSessionId);
        AuthenticationResponse aStatus = new AuthenticationResponse();
        int retValue = -1;
        aStatus.errorMessage = "ERROR";
        aStatus.status = "" + retValue;
        aStatus.session = systemSessionId;

        if (session == null) {
            return aStatus;
        }
        ChannelManagement cManagement = new ChannelManagement();
        Channels channels = cManagement.getChannelByID(session.getChannelid());
        if (channels == null) {
            return aStatus;
        }
        if (session != null) {

            SettingsManagement setManagement = new SettingsManagement();
            int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
            if (result != 0) {
                aStatus.errorMessage = "INVALID IP REQUEST";
                aStatus.status = "" + -8;
                aStatus.session = systemSessionId;
                //  return aStatus;
            }

            AuthUser aUser = uManagement.verifyPassword(systemSessionId, session.getChannelid(), userid, password);

            if (aUser != null) {
                aStatus.errorMessage = "SUCCESS";
                aStatus.status = "" + 0;
                aStatus.session = systemSessionId;
            }

        }

        audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                channels.getName(),
                session.getLoginid(), session.getLoginid(),
                new Date(),
                "Verify Password", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                "User Management",
                "",
                "",
                "PASSWORD", userid);

//        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                session.getLoginid(), session.getLoginid(), new Date(), "Verify User's Password", aStatus.error, aStatus.errorcode,
//                "User Management", " Password = ******", " Password = ******", "USERPASSWORD", userid);
        return aStatus;
    }

    @Override
    public Response resetUser(String systemSessionId, String authenticationType, String userid, String channel, String domain) {
        try {
//            int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                Response aStatus = new Response();
//                aStatus.errorMessage = "Licence Not Valid";
//                aStatus.status = "" +-10;
//                return aStatus;
//            }
            SessionManagement sManagement = new SessionManagement();
            Sessions session = sManagement.getSessionById(systemSessionId);
            AuditManagement audit = new AuditManagement();
            Response aStatus = new Response();
            aStatus.errorMessage = "ERROR";
            aStatus.status = "" + -9;

            int type = Integer.parseInt(authenticationType);
            int retValue = -1;
            String strCategory = "";
            if (type == RESET_USER_TOKEN_SOFTWARE) {
                strCategory = "SOFTWARE_TOKEN";
            } else if (type == RESET_USER_TOKEN_HARDWARE) {
                strCategory = "HARDWARE_TOKEN";
            } else if (type == RESET_USER_TOKEN_OOB) {
                strCategory = "OOB_TOKEN";
            }
            String strSubCategory = "";
            if (session == null) {
                return aStatus;
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channels = cManagement.getChannelByID(session.getChannelid());
//        if (channels == null) {
//            return aStatus;
//        }
            if (session != null) {
                MessageContext mc = wsContext.getMessageContext();
                HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
                //System.out.println("Client IP = " + req.getRemoteAddr());        
                SettingsManagement setManagement = new SettingsManagement();
                int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
                if (result != 0) {
                    aStatus.errorMessage = "INVALID IP REQUEST";
                    aStatus.status = "" + -8;
                    // return aStatus;
                }
                if (type == RESET_USER_PASSWORD) {
                    UserManagement uManagement = new UserManagement();
                    retValue = uManagement.resetPassword(systemSessionId, session.getChannelid(), userid);

                    if (retValue == 0) {
                        aStatus.errorMessage = "SUCCESS";
                        aStatus.status = "" + 0;
                        //return aStatus;
                    } else {
                        aStatus.status = "" + -1;
                        // return aStatus;
                    }

                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channels.getName(),
                            session.getLoginid(), session.getLoginid(),
                            new Date(), "RESET",
                            aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "User Management",
                            "Old Password = ******",
                            "New Password = ******",
                            "PASSWORD", userid);
                    return aStatus;
                } else if (type == RESET_USER_TOKEN_OOB) {

                    OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                    TokenStatusDetails[] tokens = oManagement.getTokenList(systemSessionId, session.getChannelid(), userid);
                    TokenStatusDetails tokenSelected = null;
                    for (int i = 0; i < tokens.length; i++) {
                        if (tokens[i].Catrgory == OTPTokenManagement.OOB_TOKEN) {
                            tokenSelected = tokens[i];
                            break;
                        }
                    }

                    if (tokenSelected == null) {
                        //return ERROR;
                        aStatus.status = "" + -2;
                        aStatus.errorMessage = "Desired OOB Token is not assigned";
                        return aStatus;
                    }

                    if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_SMS) {
                        strSubCategory = "OOB__SMS_TOKEN";
                    } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_USSD) {
                        strSubCategory = "OOB__USSD_TOKEN";
                    } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_VOICE) {
                        strSubCategory = "OOB__VOICE_TOKEN";
                    } else if (tokenSelected.SubCategory == OTP_TOKEN_OUTOFBAND_EMAIL) {
                        strSubCategory = "OOB__EMAIL_TOKEN";
                    }

                    retValue = oManagement.ChangeStatus(systemSessionId, userid, session.getChannelid(), TOKEN_STATUS_UNASSIGNED, OTPTokenManagement.OOB_TOKEN, tokenSelected.SubCategory);

                    if (retValue == 0) {
                        aStatus.errorMessage = "SUCCESS";
                        aStatus.status = "" + 0;
                    } else {
                        aStatus.status = "" + -2;
                        //   return aStatus;
                    }

                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channels.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(),
                            "RESET", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "Token Management",
                            "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                            "New Status =" + strUNASSIGNED,
                            "OOBTOKEN", userid);
                } else if (type == RESET_USER_TOKEN_SOFTWARE) {

                    OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                    TokenStatusDetails[] tokens = oManagement.getTokenList(systemSessionId, session.getChannelid(), userid);
                    TokenStatusDetails tokenSelected = null;
                    for (int i = 0; i < tokens.length; i++) {
                        if (tokens[i].Catrgory == OTPTokenManagement.SOFTWARE_TOKEN) {
                            tokenSelected = tokens[i];
                            break;
                        }
                    }

                    if (tokenSelected == null) {
                        //return ERROR;
                        aStatus.status = "" + -2;
                        aStatus.errorMessage = "Desired Software Token is not assigned";
                        return aStatus;
                    }

                    if (tokenSelected.SubCategory == OTP_TOKEN_SOFTWARE_WEB) {
                        strSubCategory = "SW_WEB_TOKEN";
                    } else if (tokenSelected.SubCategory == OTP_TOKEN_SOFTWARE_MOBILE) {
                        strSubCategory = "SW_MOBILE_TOKEN";
                    } else if (tokenSelected.SubCategory == OTP_TOKEN_SOFTWARE_PC) {
                        strSubCategory = "SW_PC_TOKEN";
                    }

                    retValue = oManagement.ChangeStatus(systemSessionId, userid, session.getChannelid(), TOKEN_STATUS_UNASSIGNED, OTPTokenManagement.SOFTWARE_TOKEN, tokenSelected.SubCategory);

                    if (retValue == 0) {
                        aStatus.errorMessage = "SUCCESS";
                        aStatus.status = "" + 0;
                    } else {
                        aStatus.status = "" + -2;
                        //  return aStatus;
                    }

                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channels.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(),
                            "RESET", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "Token Management",
                            "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                            "New Status =" + strUNASSIGNED,
                            "SOFTWARETOKEN", userid);

//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                            session.getLoginid(), session.getLoginid(), new Date(), "Change OTP Status", aStatus.error, aStatus.errorcode,
//                            "Token Management", "Category =" + strCategory + "SubCategory =" + strSubCategory + "Old Status =" + tokenSelected.Status,
//                            "Category =" + strCategory + "New Status =" + strUNASSIGNED,
//                            "RESET TOKEN", userid);
                } else if (type == RESET_USER_TOKEN_HARDWARE) {

                    OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
                    TokenStatusDetails[] tokens = oManagement.getTokenList(systemSessionId, session.getChannelid(), userid);
                    TokenStatusDetails tokenSelected = null;
                    for (int i = 0; i < tokens.length; i++) {
                        if (tokens[i].Catrgory == OTPTokenManagement.HARDWARE_TOKEN) {
                            tokenSelected = tokens[i];
                            break;
                        }
                    }

                    if (tokenSelected == null) {
                        //return ERROR;
                        aStatus.status = "" + -2;
                        aStatus.errorMessage = "Desired Hardware Token is not assigned";
                        return aStatus;
                    }

                    if (tokenSelected.SubCategory == OTP_TOKEN_HARDWARE_CR) {
                        strSubCategory = "HW_CR_TOKEN";
                    } else if (tokenSelected.SubCategory == OTP_TOKEN_HARDWARE_MINI) {
                        strSubCategory = "HW_MINI_TOKEN";
                    }
                    retValue = oManagement.ChangeStatus(systemSessionId, userid, session.getChannelid(), TOKEN_STATUS_UNASSIGNED, OTPTokenManagement.HARDWARE_TOKEN, tokenSelected.SubCategory);

                    if (retValue == 0) {
                        aStatus.errorMessage = "SUCCESS";
                        aStatus.status = "" + 0;
                    } else {
                        aStatus.status = "" + -2;
                        //  return aStatus;
                    }

                    audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                            req.getRemoteAddr(), channels.getName(),
                            session.getLoginid(), session.getLoginid(), new Date(),
                            "RESET", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                            "Token Management",
                            "Category=" + strCategory + ",Subcategory=" + strSubCategory + ",Current Status=" + tokenSelected.Status,
                            "New Status =" + strUNASSIGNED,
                            "HARDWARETOKEN", userid);

//                    audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                            session.getLoginid(), session.getLoginid(), new Date(), "Change OTP Status", aStatus.error, aStatus.errorcode,
//                            "Token Management", "Category =" + strCategory + "SubCategory =" + strSubCategory + "Old Status =" + tokenSelected.Status,
//                            "Category =" + strCategory + "New Status =" + strUNASSIGNED,
//                            "RESET TOKEN", userid);
                }
            }

            return aStatus;
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            return null;
        }
    }
   
    
    @Override
    public AuthenticationResponse changeEBankingPassword(String systemSessionId, String userid, String oldpassword, String newpassword, String channel, String domain)  {
       try {

          
           
            if (systemSessionId == null || userid == null || oldpassword == null || newpassword == null
                    || systemSessionId.isEmpty() == true || userid.isEmpty() == true || oldpassword.isEmpty() == true || newpassword.isEmpty() == true) {
                AuthenticationResponse aStatus = new AuthenticationResponse();
                aStatus.errorMessage = "Invalid Data";
                aStatus.status = "" + -11;
                aStatus.session = systemSessionId;
                return aStatus;
            }


//            int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                Response aStatus = new Response();
//                aStatus.errorMessage = "Invalid License";
//                aStatus.status = "" +-10;
//                return aStatus;
//            }
            UserManagement uManagement = new UserManagement();
            SessionManagement sManagement = new SessionManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(systemSessionId);
            AuthenticationResponse aStatus = new AuthenticationResponse();
            int retValue = -1;
            aStatus.errorMessage = "ERROR";
            aStatus.status = "" + retValue;
            aStatus.session = systemSessionId;

            if (session != null) {

                //System.out.println("Client IP = " + req.getRemoteAddr());        
                SettingsManagement setManagement = new SettingsManagement();
                int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
                if (result != 0) {
                    aStatus.errorMessage = "INVALID IP REQUEST";
                    aStatus.status = "" + -8;
                     aStatus.session = systemSessionId;
                    //  return aStatus;
                }

                retValue = uManagement.changePassword(systemSessionId, session.getChannelid(), userid, oldpassword, newpassword);
                if (retValue == 0) {
                    aStatus.errorMessage = "SUCCESS";
                    aStatus.status = "" + 0;
                     aStatus.session = systemSessionId;
                }else{
                     aStatus.errorMessage = "Failed to change Password/Old Password is Wrong";
                    aStatus.status = "" + retValue;
                     aStatus.session = systemSessionId;
                }
                //return aStatus;
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channels = cManagement.getChannelByID(session.getChannelid());
            AuditManagement audit = new AuditManagement();
            audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                    req.getRemoteAddr(), channels.getName(),
                    session.getLoginid(), session.getLoginid(),
                    new Date(), "Change Password",
                    aStatus.errorMessage, Integer.parseInt(aStatus.status),
                    "User Management", "",
                    "OldPassword=" + oldpassword + ",newPassword=" + newpassword + ",Userid=" + userid,
                    "USER", "");
            return aStatus;
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public Response assignPwdAuthenticationType(String systemSessionId, String userid, String password, String channel, String domain)  {
           AuditManagement audit = new AuditManagement();
        ChannelManagement cManagement = new ChannelManagement();
        SessionManagement sManagement = new SessionManagement();
        Sessions session = sManagement.getSessionById(systemSessionId);
        Channels channels = cManagement.getChannelByID(session.getChannelid());
        Response aStatus = new Response();
        int retValue = -1;
        aStatus.errorMessage = "ERROR";
        aStatus.status = "" + retValue;
        if (channels == null) {
            return aStatus;
        }

        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);

        try {
//            int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                //Response aStatus = new Response();
//                aStatus.errorMessage = "Licence Not Valid";
//                aStatus.status = "" +-10;
//                return aStatus;
//            }
            UserManagement uManagement = new UserManagement();

            if (session == null) {
                return aStatus;
            }

            if (session != null) {

                //System.out.println("Client IP = " + req.getRemoteAddr());        
                SettingsManagement setManagement = new SettingsManagement();
                int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
                if (result != 0) {
                    aStatus.errorMessage = "INVALID IP REQUEST";
                    aStatus.status = "" + -8;
                    //   return aStatus;
                }

                retValue = uManagement.AssignPassword(systemSessionId, session.getChannelid(), userid, password);

                if (retValue == 0) {
                    boolean bSentToUser = true;
                    if (bSentToUser == true) {
                        SendNotification send = new SendNotification();
                        AuthUser aUser = uManagement.getUser(systemSessionId, session.getChannelid(), userid);
                        Templates temp = null;
                        TemplateManagement tObj = new TemplateManagement();
                        temp = tObj.LoadbyName(systemSessionId, session.getChannelid(), TemplateNames.MOBILE_USER_PASSWORD_TEMPLATE);

                        if (temp == null) {
                            aStatus.errorMessage = "Message Template is missing";
                            aStatus.status = "" + -2;
                            //return aStatus;
                        }

                        ByteArrayInputStream bais = new ByteArrayInputStream(temp.getTemplatebody());
                        String templatebody = (String) TemplateUtils.deserializeFromObject(bais);

                        templatebody = templatebody.replaceAll("#name#", aUser.userName);
                        String date = String.valueOf(new Date());
                        //templatebody = templatebody.replaceAll("#Sender#", "Axiom");
                        templatebody = templatebody.replaceAll("#date#", date);

                        com.mollatech.axiom.connector.communication.AXIOMStatus axiomStatus = null;

                        int iProductType = Integer.valueOf(LoadSettings.g_sSettings.getProperty("product.type")).intValue();

                        axiomStatus = send.SendOnMobile(session.getChannelid(), aUser.phoneNo, templatebody, SendNotification.SMS, iProductType);
                        if (axiomStatus != null) {
                            aStatus.errorMessage = axiomStatus.strStatus;
                            aStatus.status = "" + axiomStatus.iStatus;
                        }
                    } else {
                        aStatus.errorMessage = "SUCCESS";
                        aStatus.status = "" + 0;
                    }
                }
            }
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
        } finally {
            audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                    channels.getName(),
                    session.getLoginid(), session.getLoginid(),
                    new Date(),
                    "Assign Password", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                    "User Management",
                    "Old Password=******",
                    "New Password=******",
                    "PASSWORD", userid);
        }

        return aStatus;
    }


    @Override
    public AuthenticationResponse verifyEBankingOTP(String systemSessionId, String userid, String otptoken, String password, String channel, String domain)  {
      
        try {

//            int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                 AuthenticationResponse aStatus = new AuthenticationResponse();
//            aStatus.errorMessage = "Licence Not Valid";
//            aStatus.status = -10;
//        aStatus.session = systemSessionId;
//                return aStatus;
//            }
            if (systemSessionId == null || userid == null
                    || systemSessionId.isEmpty() == true || userid.isEmpty() == true
                    || otptoken == null || otptoken.isEmpty() == true) {
                AuthenticationResponse aStatus = new AuthenticationResponse();

                aStatus.errorMessage = "Invalid Data";
                aStatus.status = "" + -11;
                aStatus.session = systemSessionId;
                return aStatus;
            }

            
            SessionManagement sManagement = new SessionManagement();
            AuditManagement audit = new AuditManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(systemSessionId);
            AuthenticationResponse aStatus = new AuthenticationResponse();
            int retValue = -1;
            aStatus.errorMessage = "ERROR";
            aStatus.status = "" +retValue;
             aStatus.session = systemSessionId;
            if (session == null) {
                aStatus.errorMessage = "Invalid Session";
                aStatus.status = "" +-14;
                 aStatus.session = systemSessionId;
                return aStatus;
            }
            ChannelManagement cManagement = new ChannelManagement();
            Channels channels = cManagement.getChannelByID(session.getChannelid());
            if (channels == null) {
                aStatus.errorMessage = "Invalid Channel";
                aStatus.status = "" +-15;
                 aStatus.session = systemSessionId;
                return aStatus;
            }
            OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
            
            if (session != null) {

                SettingsManagement setManagement = new SettingsManagement();
                int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
                if (result != 0) {
                    aStatus.errorMessage = "INVALID IP REQUEST";
                    aStatus.status = "" +-8;
                     aStatus.session = systemSessionId;
                    return aStatus;
                }

                //System.out.println("Before VerifyOTP >> " + otp);
                retValue = oManagement.VerifyOTP(session.getChannelid(), userid, systemSessionId, otptoken);

                if (retValue == 0) {
                    aStatus.errorMessage = "SUCCESS";
                    aStatus.status = "" +0;
                     aStatus.session = systemSessionId;
                } else if (retValue == -9) {
                    aStatus.errorMessage = "One Time Password is expired...";
                    aStatus.status = "" +-9;
                     aStatus.session = systemSessionId;
                }

                //System.out.println("VerifyOTP result::" + retValue + " for OTP::" + otp);
            }

            audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                    channels.getName(),
                    session.getLoginid(), session.getLoginid(),
                    new Date(),
                    "VERIFY OTP", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                    "Token Management",
                    "OTP=" + password,
                    "",
                    "OTPTOKENS", userid);

//            audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                    session.getLoginid(), session.getLoginid(), new Date(), "Verify OTP ", aStatus.error, aStatus.errorcode,
//                    "Token Management", "OTP = ******", " OTP = ******",
//                    "Verify OTP TOKEN", userid);
            return aStatus;

        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Response createUser(String systemSessionId, String userid, String firstname, String lastname, String postcode, String address1, String address2, String city, String mobileno, String channel, String domain){
       try {

            String fullname = firstname + " " + lastname;
            if (mobileno == null) {
                mobileno = "1234567890";
            }
            if (systemSessionId == null || fullname == null
                    || systemSessionId.isEmpty() == true || fullname.isEmpty() == true) {
                Response aStatus = new Response();
                aStatus.errorMessage = "Invalid Data";
                aStatus.status = "" + -11;
                return aStatus;
            }

            if (userid != null && userid.isEmpty() == true) {
                Response aStatus = new Response();
                aStatus.errorMessage = "Setting userid is not supported!!!";
                aStatus.status = "" + -12;
                return aStatus;
            }

//            int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                Response aStatus = new Response();
//                aStatus.errorMessage = "Invalid License";
//                aStatus.status = "" +-10;
//                return aStatus;
//            }
            UserManagement uManagement = new UserManagement();
            SessionManagement sManagement = new SessionManagement();
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Sessions session = sManagement.getSessionById(systemSessionId);
            Response aStatus = new Response();
            int retValue = -1;
            aStatus.errorMessage = "ERROR";
            aStatus.status = "" + retValue;

            if (session != null) {

                //System.out.println("Client IP = " + req.getRemoteAddr());        
                SettingsManagement setManagement = new SettingsManagement();
                int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
                if (result != 0) {
                    aStatus.errorMessage = "INVALID IP REQUEST";
                    aStatus.status = "" + -8;
                    //  return aStatus;
                }

//                retValue = uManagement.CreateUser(systemSessionId, session.getChannelid(), fullname, mobileno,null,userid,0);
                retValue = uManagement.CreateUser(systemSessionId, session.getChannelid(), fullname, mobileno,null,userid,0,null,null,null,null,null,null,null,null);
                if (retValue == 0) {
                    aStatus.errorMessage = "SUCCESS";
                    aStatus.status = "" + 0;
                }
                //return aStatus;
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channels = cManagement.getChannelByID(session.getChannelid());
            AuditManagement audit = new AuditManagement();
            audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                    req.getRemoteAddr(), channels.getName(),
                    session.getLoginid(), session.getLoginid(),
                    new Date(), "Create User",
                    aStatus.errorMessage, Integer.parseInt(aStatus.status),
                    "User Management", "",
                    "Name=" + fullname + ",Phone=" + mobileno + ",Userid=" + userid + ",State=Inactive",
                    "USER", "");
            return aStatus;
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Response systemLogout(String systemSessionId, String channel, String domain) {
        try {
//            int iResult = AxiomProtect.ValidateLicense();
//            if (iResult != 0) {
//                Response aResponse = new AxiomStatus();
//                aResponse.errorMessage = "Licence Not Valid";
//                aResponse.status = "" +-10;
//                return aResponse;
//            }
            SessionManagement sManagement = new SessionManagement();
            Sessions session = sManagement.getSessionById(systemSessionId);
            MessageContext mc = wsContext.getMessageContext();
            HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
            Response aResponse = new Response();
            aResponse.errorMessage = "ERROR";
            aResponse.status = "" + -2;

            if (session != null) {

                //System.out.println("Client IP = " + req.getRemoteAddr());        
                SettingsManagement setManagement = new SettingsManagement();
                int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
                if (result != 0) {

                    aResponse.errorMessage = "INVALID IP REQUEST";
                    aResponse.status = "" + -8;

                }
                int retValue = sManagement.CloseSession(systemSessionId);

                aResponse.errorMessage = "ERROR";
                aResponse.status = "" + retValue;

                if (retValue == 0) {
                    aResponse.errorMessage = "SUCCESS";
                    aResponse.status = "" + 0;

                }
                //return aStatus;
            } else if (session == null) {
                return aResponse;
            }

            ChannelManagement cManagement = new ChannelManagement();
            Channels channels = cManagement.getChannelByID(session.getChannelid());
            if (channels == null) {
                return aResponse;
            }
            AuditManagement audit = new AuditManagement();

            audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(),
                    req.getRemoteAddr(),
                    channels.getName(), session.getLoginid(),
                    session.getLoginid(), new Date(),
                    "CLOSE",
                    aResponse.errorMessage, Integer.parseInt(aResponse.status),
                    "SESSION",
                    "Session ID=" + systemSessionId,
                    "",
                    "SESSION",
                    channels.getChannelid());
            return aResponse;
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    @Override
    public AuthenticationResponse systemLogin(String userid, String password, String channel, String domain) {
        
            try {
                AuthenticationResponse aResponse = new AuthenticationResponse();
                aResponse.errorMessage = "ERROR";
                aResponse.status = "-1";
                aResponse.session = null;
//            int iResult = AxiomProtect.ValidateLicense();
//
//            if (iResult != 0) {
//                aResponse.errorMessage = "Licence Not Valid";
//             aResponse.status = "-10";
//             aResponse.session = null;
//            }
                MessageContext mc = wsContext.getMessageContext();
                HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
                //System.out.println("Client IP = " + req.getRemoteAddr());

                SettingsManagement setManagement = new SettingsManagement();
                int result = setManagement.checkIP(channel, req.getRemoteAddr());
                if (result != 0) {
                    aResponse.errorMessage = "INVALID IP REQUEST";
                    aResponse.status = "-8";
                    aResponse.session = null;
                }
                String resultStr = "Failure";
                int retValue = -1;
                SessionManagement sManagement = new SessionManagement();
                String sessionId = sManagement.OpenSessionForWS(channel, userid, password, req.getSession().getId());
                ChannelManagement cManagement = new ChannelManagement();
                Channels channels = cManagement.getChannelByID(sessionId, channel);
                AuditManagement audit = new AuditManagement();
                if (sessionId != null) {
                    retValue = 0;
                    resultStr = "success";
                    audit.AddAuditTrail(sessionId, channel, userid,
                            req.getRemoteAddr(),
                            channels.getName(), userid,
                            userid, new Date(),
                            "OPEN",
                            resultStr, retValue,
                            "SESSION",
                            "",
                            "Session ID=" + sessionId,
                            "SESSION",
                            channel);
                    aResponse.errorMessage = "SUCCESS";
                    aResponse.status = "0";
                    aResponse.session = sessionId;
                } else if (sessionId == null) {
                    resultStr = "failed";
                    audit.AddAuditTrail(sessionId, channel, userid,
                            req.getRemoteAddr(),
                            channels.getName(), userid,
                            userid, new Date(),
                            "OPEN",
                            resultStr,
                            retValue,
                            "SESSION",
                            "",
                            "",
                            "SESSION",
                            channel);
                    aResponse.errorMessage = "INVALID IP REQUEST";
                    aResponse.status = "" + -4;
                    aResponse.session = sessionId;
                }

                return aResponse;
            } catch (java.lang.Exception e) {
                e.printStackTrace();
                return null;
            }
        
    }


    @Override
    public Response assignOtpAuthenticationType(String systemSessionId, String userid, String otptoken, String channel, String domain)  {
         int iResult = AxiomProtect.ValidateLicense();
//        if (iResult != 0) {
//            Response aStatus = new Response();
//            aStatus.errorMessage = "Licence Not Valid";
//            aStatus.status = "" +-10;
//            return aStatus;
//        }


        if (systemSessionId == null || userid == null 
                || systemSessionId.isEmpty() == true || userid.isEmpty() == true ) {
            Response aStatus = new Response();
            aStatus.errorMessage = "Invalid Data";
            aStatus.status =  "" +-11;
            return aStatus;
        }
        int type = 0;
        int subtype = 0;
        
        if(otptoken == null){
            type = 3;
            subtype = 1;
       }else {
            type = 2;
            subtype = 1;
        }
        
        
//        if(otptoken.equals("11")){
//            type = 1;
//            subtype = 1;
//        }else if(otptoken.equals("12")){
//            type = 1;
//            subtype = 2;
//        }else if(otptoken.equals("13")){
//            type = 1;
//            subtype = 3;
//        }else if(otptoken.equals("21")){
//            type = 2;
//            subtype = 1;
//        }else if(otptoken.equals("22")){
//            type = 2;
//            subtype = 2;
//        }else if(otptoken.equals("31")){
//            type = 3;
//            subtype = 1;
//        }else if(otptoken.equals("32")){
//            type = 3;
//            subtype = 2;
//        }else if(otptoken.equals("33")){
//            type = 3;
//            subtype = 3;
//        }else if(otptoken.equals("34")){
//            type = 3;
//            subtype = 4;
//        }

        String strCategory = "";
        if (type == RESET_USER_TOKEN_SOFTWARE) {
            strCategory = "SOFTWARE_TOKEN";
        } else if (type == RESET_USER_TOKEN_HARDWARE) {
            strCategory = "HARDWARE_TOKEN";
        } else if (type == RESET_USER_TOKEN_OOB) {
            strCategory = "OOB_TOKEN";
        }


        if (type != RESET_USER_TOKEN_SOFTWARE && type != RESET_USER_TOKEN_HARDWARE
                && type != RESET_USER_TOKEN_OOB) {
            Response aStatus = new Response();
            aStatus.errorMessage = "Invalid category!!!";
            aStatus.status = "" +-12;
            return aStatus;
        }


        String strSubCategory = "";
        if (subtype == OTP_TOKEN_OUTOFBAND_SMS) {
            strSubCategory = "OOB__SMS_TOKEN";
        } else if (subtype == OTP_TOKEN_OUTOFBAND_USSD) {
            strSubCategory = "OOB__USSD_TOKEN";
        } else if (subtype == OTP_TOKEN_OUTOFBAND_VOICE) {
            strSubCategory = "OOB__VOICE_TOKEN";
        } else if (subtype == OTP_TOKEN_OUTOFBAND_EMAIL) {
            strSubCategory = "OOB__EMAIL_TOKEN";
        } else if (subtype == OTP_TOKEN_SOFTWARE_WEB) {
            strSubCategory = "SW_WEB_TOKEN";
        } else if (subtype == OTP_TOKEN_SOFTWARE_MOBILE) {
            strSubCategory = "SW_MOBILE_TOKEN";
        } else if (subtype == OTP_TOKEN_SOFTWARE_PC) {
            strSubCategory = "SW_PC_TOKEN";
        } else if (subtype == OTP_TOKEN_HARDWARE_CR) {
            strSubCategory = "HW_CR_TOKEN";
        } else if (subtype == OTP_TOKEN_HARDWARE_MINI) {
            strSubCategory = "HW_MINI_TOKEN";
        } else {
            Response aStatus = new Response();
            aStatus.errorMessage = "Invalid subcategory!!!";
            aStatus.status = "" +-13;
            return aStatus;
        }

        
        SessionManagement sManagement = new SessionManagement();
        AuditManagement audit = new AuditManagement();
        MessageContext mc = wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
        Sessions session = sManagement.getSessionById(systemSessionId);
        Response aStatus = new Response();
        int retValue = -1;
        aStatus.errorMessage = "ERROR";
        aStatus.status = "" +retValue;

        if (session == null) {
            aStatus.errorMessage = "Invalid/Expired Session";
            aStatus.status = "" +-14;
            return aStatus;
        }
        ChannelManagement cManagement = new ChannelManagement();
        Channels channels = cManagement.getChannelByID(session.getChannelid());
        if (channels == null) {
            aStatus.errorMessage = "Invalid Channel";
            aStatus.status = ""+-15;
            return aStatus;
        }
        
        OTPTokenManagement oManagement = new OTPTokenManagement(session.getChannelid());
        if (session != null) {

            SettingsManagement setManagement = new SettingsManagement();
            int result = setManagement.checkIP(session.getChannelid(), req.getRemoteAddr());
            if (result != 0) {
                aStatus.errorMessage = "INVALID IP REQUEST";
                aStatus.status = "" +-8;
                // return aStatus;
            }

            retValue = oManagement.AssignToken(systemSessionId, session.getChannelid(), userid, type, subtype,otptoken);
            if(retValue == 0){
                retValue = oManagement.ChangeStatus(systemSessionId, session.getChannelid(), userid, OTPTokenManagement.TOKEN_STATUS_ACTIVE,type, subtype);
            if(retValue != 0){
                retValue = -15;
                 aStatus.errorMessage = "Failed in Activation";
                aStatus.status = "" +retValue;
                
            }
            
            }
            if (retValue == 0) {
                aStatus.errorMessage = "SUCCESS";
                aStatus.status = "" +0;
            } else {
                if (retValue == -4) {
                    aStatus.errorMessage = "Token already assigned!!!";
                    aStatus.status = "" +-4;
                } else if (retValue == -3) {
                    aStatus.errorMessage = "Empty Serial Number!!!";
                    aStatus.status = "" +-3;
                } else if (retValue == -2) {
                    aStatus.errorMessage = "Invalid Serial Number!!!";
                    aStatus.status = ""+-3;
                } else if (retValue == -6) {
                    aStatus.errorMessage = "Hardware Token could not be assigned!!!";
                    aStatus.status = ""+-6;
                }
            }

        }

        audit.AddAuditTrail(systemSessionId, channels.getChannelid(), session.getLoginid(), req.getRemoteAddr(),
                channels.getName(),
                session.getLoginid(), session.getLoginid(),
                new Date(),
                "Assign OTP Token", aStatus.errorMessage, Integer.parseInt(aStatus.status),
                "Token Management",
                "",
                "Category=" + strCategory + ",Subcategory=" + strSubCategory,
                "OTPTOKENS", userid);

//        audit.AddAuditTrail(sessionId, channel.getChannelid(), session.getLoginid(), req.getRemoteAddr(), channel.getName(),
//                session.getLoginid(), session.getLoginid(), new Date(), "Assign Token", aStatus.error, aStatus.errorcode,
//                "Token Management", "", 
//                "Category = " + strCategory + " Sub Category =" + strSubCategory,
//                "OTPTOKENS", userid);
        return aStatus;
    }


    @Override
    public Response ping() {
        Response response = new Response();
        response.errorMessage = "SUCCESS";
        response.status = "" + 0;
        return response;
    }

}
